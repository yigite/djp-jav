package com.lounge.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
/*
 * this is a test controller for lounge which calls the controller in the core module
 * the lounge url is the controller url in the core module
 * it calls a function in that controller called /there;
 * */
@Controller
public class LoungeHomeController {

//	@Value("${core.lounge.url}")
//	String loungeUrl;


	/* below function is for test purpose only
	/ if you want it to work just uncomment it
	 and add the specified end point in core module of lounge*/
//	@GetMapping
//	@ResponseBody
//	public String callEndPoint(){
//		final String uri = loungeUrl+"/there";
//		RestTemplate restTemplate = new RestTemplate();
//		String result = restTemplate.getForObject(uri, String.class);
//		return result;
//	}
}
