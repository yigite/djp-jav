package com.webservices.handler;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


import java.io.IOException;

public class LoginAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginAuthenticationFailureHandler.class);
    private String noAuthUrl;
    private String noActiveUrl;


    public void setNoAuthUrl(String noAuthUrl) {
        this.noAuthUrl = noAuthUrl;
    }

    public void setNoActiveUrl(String noActiveUrl) {
        this.noActiveUrl = noActiveUrl;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        HttpSession session = httpServletRequest.getSession(false);
        if (session != null) {

        }

    }

    public String getNoAuthUrl() {
        return this.noAuthUrl;
    }
    public String getNoActiveUrl() {
        return this.noActiveUrl;
    }
}
