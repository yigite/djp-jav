package com.webservices.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;

public class CrossScriptingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //no need to do anything
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
    }

    @Override
    public void destroy() {
        //no need to do anything
    }
}
