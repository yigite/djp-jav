package com.webservices.controller;


import com.djp.response.FioriStatusResponse;
import com.djp.response.ManuelRecordsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
/*
* this is a test controller for fasttrack which calls the controller in the core module
* the fasttrack url is the controller url in the core module
* it calls a function in that controller called /there;
* */

@RestController
public class FasttrackControllerDemo {

	/* below function is for test purpose only
	/ if you want it to work just uncomment it
	 and add the specified end point in core module of fasttrack*/


    @GetMapping("/webservices")
	public ResponseEntity<ManuelRecordsResponse> hello()
	{
		ManuelRecordsResponse response = new ManuelRecordsResponse();
		FioriStatusResponse serviceResult = new FioriStatusResponse();
		serviceResult.setMessage("getManuelRecords HATA OLUŞTU!");
		response.setServiceResult(serviceResult);
		return  ResponseEntity.of(Optional.of(response));
	}
}
