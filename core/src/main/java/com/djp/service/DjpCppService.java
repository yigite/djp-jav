package com.djp.service;

import com.djp.core.model.DjpCppTxnModel;
import com.djp.data.DjpCppTxnData;

public interface DjpCppService {

    String initPayment(DjpCppTxnModel djpCppTxnModel, DjpCppTxnData djpCppTxnData);
    DjpCppTxnModel getDjpCppTxnDataByToken(String djpCppToken);
}
