package com.djp.service;

import com.djp.core.enums.QrType;
import com.djp.core.enums.UsageMethod;
import com.djp.core.model.*;
import com.djp.data.DjpGuestData;
import com.djp.dto.UserWsDto;
import com.djp.request.*;
import com.djp.response.FioriStatusResponse;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface DjpBoardingService {
    FioriStatusResponse updateBoardingPasses(FioriStatusResponse response,UpdateBoardingPassRequest updateBoardingPassRequest,DjpBoardingPassModel boardingPassModel,DjpAccessBoardingModel accessBoardingModel);

    DjpBoardingPassModel getCouldReadDjpBoardingPassWithouthDayLimit(String pnrCode, String whereUsed);

    DjpAccessBoardingModel getCouldReadDjpAccessBoardingWithoutDateLimit(String pnrCode, String whereUsed);

    void checkIfUsageByBoardingPass(QrForFioriRequest qrForFioriRequest, AccessBoardingPassRequest accBoardingPassRequest, BoardingPassRequest boardingPassRequest, DjpGuestData djpGuestData);

    int getDiffBetweenToDate(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData) throws ParseException;

    String checkIfBoardingForLocationIATA(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData, DjpPassLocationsModel djpPassLocation);

    boolean checkIfBoardingFromLocationIATA(QrForFioriRequest qrForFioriRequest,BoardingPassRequest boardingPassRequest,AccessBoardingPassRequest accessBoardingPassRequest,DjpGuestData djpGuestData,DjpPassLocationsModel djpPassLocation);

    String checkInternationalStatusForBp(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData, QrType whereUsed) throws ParseException;

    String saveDjpBoardingPass(BoardingPassRequest boardingPassRequest);

    DjpMiddleWareBoardingPassStatusModel getStatusForMiddleWare(DjpMiddleWareBoardingPassStatusModel statusModel, Boolean couldNotReadBP, String fromIATA, Date flightDate, String flightNumber, QrType whereUsed);

    boolean ifHasSameBp(BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpMobilePassRequest djpMobilePassRequest, boolean checkOwner);

    DjpBoardingPassModel getDjpBoardingPassByToken(String token);

    List<DjpBoardingPassModel> getDjpBoardingPassesByTokenAndSapOrderID(String qrKey, String sapOrderID);

    List<DjpBoardingPassModel> getDjpBoardingPassByTokenAndUsage(String qrKey,QrType whereUsed,Date sub48HoursCurrentDate);

    DjpBoardingPassModel getDjpBoardingPassByTokenAndSapOrderID(String token,String sapOrderID);

    DjpBoardingPassModel getCouldReadDjpBoardingPass(String pnrCode, String whereUsed);

    DjpMobilePassModel getCouldReadDjpMobilePass(String pnrCode,String whereUsed, String deviceId);

    DjpAccessBoardingModel getCouldReadDjpAccessBoarding(String pnrCode,String whereUsed);

    void checkIfHaveDjpPassAndGenerateBoardingPass(QrForFioriRequest qrForFioriRequest, UserWsDto userWs, DjpPassLocationsModel igaPassLocation, UsageMethod usageMethod);

    void saveAccessBoardingPassForQuickServiceUsage(QrForFioriRequest qrForFioriRequest, String contractId, String className, DjpPassLocationsModel djpPassLocation, UsageMethod usageMethod);

    ContractedCompaniesModel getContractedCompaniesViaPass(String contractedId);

    List<Object> getAllDjpBoardingSellList(Date startDate, Date endDate, Boolean isNewSale, String whereUsed, DjpPassLocationsModel djpPassLocation);

    FioriStatusResponse checkIfValidBoardingPasses(FioriStatusResponse response, UpdateBoardingPassRequest updateBoardingPassRequest) throws ParseException;

    DjpAccessBoardingModel getAccessBoardingPass(QrForFioriRequest qrForFioriRequest,AccessBoardingPassRequest accBoardingPassRequest, DjpGuestData igaGuestData, String whereUsedBoardingPass);

    DjpAccessBoardingModel findDjpAccessBoarding(UpdateBoardingPassRequest updateBoardingPassRequest,QrForFioriRequest qrForFioriRequest);
    DjpBoardingPassModel getDjpBpByQrOrFiori(UpdateBoardingPassRequest updateBoardingPassRequest,QrForFioriRequest qrForFioriRequest);

}
