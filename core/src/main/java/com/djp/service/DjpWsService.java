package com.djp.service;

import com.djp.data.login.CheckLoginResponse;
import com.djp.user.CurrentUser;
import com.djp.dto.UserWsDto;
import com.djp.request.QrForFioriRequest;
public interface DjpWsService {

    CheckLoginResponse checkLogin(final String username, final String password);
    CurrentUser getUserDetails(String name);
    String getAuthorizationToken() throws Exception;
    UserWsDto getOrderList(QrForFioriRequest orderListRequest);

}
