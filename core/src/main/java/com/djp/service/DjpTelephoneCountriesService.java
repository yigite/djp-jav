package com.djp.service;

import com.djp.core.model.DjpTelCountriesModel;

import java.util.List;

public interface DjpTelephoneCountriesService {

    List<DjpTelCountriesModel> findCountriesTelephone();
}
