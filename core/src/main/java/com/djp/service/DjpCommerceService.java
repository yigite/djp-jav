package com.djp.service;

import com.djp.core.enums.QrType;
import com.djp.core.model.DjpAccessBoardingModel;
import com.djp.core.model.DjpBoardingPassModel;

public interface DjpCommerceService {
    void sendMiddleWareBPInfo(DjpBoardingPassModel boardingPassModel, DjpAccessBoardingModel djpAccessBoardingModel, QrType whereUsed);
}
