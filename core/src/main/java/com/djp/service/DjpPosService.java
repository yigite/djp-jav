package com.djp.service;

import com.djp.rest.data.DjpPosLogDataRequest;
import com.djp.rest.data.DjpPosLogDataResponse;

public interface DjpPosService {

    DjpPosLogDataResponse getPosLogData(DjpPosLogDataRequest request);
}
