package com.djp.service;

import com.djp.core.model.DjpManuelRecordsModel;
import com.djp.data.DjpGuestData;
import com.djp.data.ManuelRecordsListData;
import com.djp.rest.data.ContractedCompaniesResponseData;
import com.djp.request.NewManuelRecordRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.rest.data.ServiceResultWsData;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface DjpManuelRecordService {

    List<ManuelRecordsListData> getManuelRecords(String date, String whereUsed);
    DjpManuelRecordsModel checkIfUsageByManuelBoarding(QrForFioriRequest qrForFioriRequest, NewManuelRecordRequest newManuelRecordRequest, DjpGuestData igaGuestData);
    int getDiffBetweenToDate(QrForFioriRequest qrForFioriRequest, NewManuelRecordRequest newManuelRecordRequest, DjpGuestData djpGuestData, String flightNumber, Date flightDate) throws  ParseException;
    ServiceResultWsData saveNewManuelRecord(NewManuelRecordRequest newManuelRecordRequest);

    List<ContractedCompaniesResponseData> getContractedCompaniesForService(String qrType);

}
