package com.djp.service;


import com.djp.core.model.UserModel;

public interface UserService {
    UserModel getUserForUID(final String userUid);
}
