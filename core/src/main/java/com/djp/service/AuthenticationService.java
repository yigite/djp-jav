package com.djp.service;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.djp.core.model.UserModel;
import org.apache.http.auth.InvalidCredentialsException;

public interface AuthenticationService {
    UserModel checkCredentials(String var1, String var2) throws InvalidCredentialsException;

    UserModel login(String var1, String var2) throws InvalidCredentialsException;

    void logout();
}

