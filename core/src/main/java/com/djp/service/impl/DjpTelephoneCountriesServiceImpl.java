package com.djp.service.impl;


import com.djp.core.dao.DjpTelephoneCountriesDao;

import com.djp.core.model.DjpTelCountriesModel;
import com.djp.service.DjpTelephoneCountriesService;

import jakarta.annotation.Resource;
import java.util.List;

public class DjpTelephoneCountriesServiceImpl implements DjpTelephoneCountriesService {

    @Resource
    private DjpTelephoneCountriesDao djpTelephoneCountriesDao;
    @Override
    public List<DjpTelCountriesModel> findCountriesTelephone() {
        return djpTelephoneCountriesDao.findCountriesTelephone();
    }
}
