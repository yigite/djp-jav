package com.djp.service.impl;

import com.djp.core.dao.DjpPosDao;
import com.djp.core.model.DjpPosLogModel;
import com.djp.rest.data.DjpPosLogDataRequest;
import com.djp.rest.data.DjpPosLogDataResponse;
import com.djp.service.DjpPosService;
import org.apache.commons.lang.StringUtils;

import jakarta.annotation.Resource;
import java.util.Objects;

public class DjpPosServiceImpl implements DjpPosService {

    @Resource
    private DjpPosDao djpPosDao;

    @Override
    public DjpPosLogDataResponse getPosLogData(DjpPosLogDataRequest request){
        DjpPosLogDataResponse response = new DjpPosLogDataResponse();
        if(Objects.nonNull(request)){
            if (StringUtils.isNotBlank(request.getRequestId())){
                DjpPosLogModel posLogModel = djpPosDao.getPosLogModel(request.getRequestId());
                if (Objects.nonNull(posLogModel)){
                    response.setRequestId(posLogModel.getRequestId());
                    response.setResponseBody(posLogModel.getResponseBody());
                    response.setType(posLogModel.getType());
                    response.setErrorResponseMessage(posLogModel.getErrorResponseMessage());
                }
            }
        }
        return response;
    }
}
