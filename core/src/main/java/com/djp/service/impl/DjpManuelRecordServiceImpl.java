package com.djp.service.impl;

import com.djp.core.dao.DjpManuelRecordDao;
import com.djp.core.enums.ContractedType;
import com.djp.core.enums.QrType;
import com.djp.core.model.DjpManuelRecordsModel;
import com.djp.data.DjpGuestData;
import com.djp.data.FioriFlightDetailData;
import com.djp.data.ManuelRecordsListData;
import com.djp.enums.HttpStatusCodes;
import com.djp.facades.DjpFioriFlightFacade;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.rest.data.ContractedCompaniesResponseData;
import com.djp.request.NewManuelRecordRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.rest.data.ServiceResultWsData;
import com.djp.service.DjpManuelRecordService;
import com.djp.util.DjpServiceUtil;
import com.djp.util.DjpUtil;
import com.djp.util.impl.Converters;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.*;
import java.text.ParseException;
import java.util.*;

public class DjpManuelRecordServiceImpl implements DjpManuelRecordService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpManuelRecordServiceImpl.class);

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private DjpManuelRecordDao djpManuelRecordDao;

    @Resource
    private DjpFioriFlightFacade djpFioriFlightFacade;

    @Resource
    private ModelService modelService;

    private Converter<DjpManuelRecordsModel, ManuelRecordsListData> djpManuelRecordsConverter;

    @Override
    public List<ManuelRecordsListData> getManuelRecords(String date, String whereUsed) {
        try {
            Date startDate = DateUtil.convertStartDate(date);
            Date endDate = DateUtil.addOneDay(startDate);
            return Converters.convertAll(djpManuelRecordDao.getManuelRecords(startDate,endDate, QrType.valueOf(qrTypeEnumHashMap.get(whereUsed))),djpManuelRecordsConverter);
        }catch (Exception e){
            LOGGER.error("has an error getManuelRecords:",e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ContractedCompaniesResponseData> getContractedCompaniesForService(String qrType) {
        List<ContractedCompaniesResponseData> contractedCompaniesResponseDataList = new ArrayList<>();
        try {
            String qrCode = qrTypeEnumHashMap.get(qrType);
            ArrayList resultList = djpManuelRecordDao.getContractedCompaniesForService(qrCode);

            for (int i = 0; i < resultList.size(); i++) {
                ContractedCompaniesResponseData contractedCompaniesResponseData = new ContractedCompaniesResponseData();

                contractedCompaniesResponseData.setCompanyName((((ArrayList) resultList.get(i)).get(0)).toString());
                if (Objects.nonNull(((((ArrayList) resultList.get(i)).get(1))))) {
                    contractedCompaniesResponseData.setCompanyContractedType((((ArrayList) resultList.get(i)).get(1)).toString());
                }else{
                    contractedCompaniesResponseData.setCompanyContractedType("");
                }
                if (Objects.nonNull((((ArrayList) resultList.get(i)).get(2)))) {
                    contractedCompaniesResponseData.setClassNames(Arrays.asList((((ArrayList) resultList.get(i)).get(2)).toString().split(",")));
                }else{
                    contractedCompaniesResponseData.setClassNames(new ArrayList<>());
                }
                contractedCompaniesResponseDataList.add(contractedCompaniesResponseData);
            }

            return contractedCompaniesResponseDataList;
        }catch (Exception e){
            LOGGER.error("has an error getContractedCompaniesForService:",e);
            return null;
        }

    }



    @Override
    public DjpManuelRecordsModel checkIfUsageByManuelBoarding(QrForFioriRequest qrForFioriRequest, NewManuelRecordRequest newManuelRecordRequest, DjpGuestData djpGuestData) {
        DjpManuelRecordsModel djpManuelRecord = null;
        boolean couldNotReadBoardingPass = false;
        String pnrCode = null;
        String whereUsed = "";
        try {
            if (Objects.isNull(djpGuestData)) {
                if (Objects.nonNull(qrForFioriRequest)) {
                    couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                    pnrCode = !couldNotReadBoardingPass ? qrForFioriRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                    whereUsed = qrForFioriRequest.getBoardingType();
                } else {
                    if (Objects.nonNull(newManuelRecordRequest)) {
                        couldNotReadBoardingPass = Objects.nonNull(newManuelRecordRequest.getCouldNotReadManuelBoarding()) && newManuelRecordRequest.getCouldNotReadManuelBoarding();
                        pnrCode = !couldNotReadBoardingPass ? newManuelRecordRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                        whereUsed = newManuelRecordRequest.getWhereUsed();
                    }
                }
            } else {
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                pnrCode = !couldNotReadBoardingPass ? djpGuestData.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                whereUsed = Objects.nonNull(newManuelRecordRequest) ? newManuelRecordRequest.getWhereUsed() : newManuelRecordRequest.getWhereUsed();
            }

            Date flightDate = null;
            String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
            String currentPassangerName = null;
            if (StringUtils.isBlank(pnrCode)) {
                currentPassangerName = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(newManuelRecordRequest) ? "" : newManuelRecordRequest.getManuelNameSurname().toUpperCase() : qrForFioriRequest.getPassengerName().toUpperCase() : djpGuestData.getName() + djpGuestData.getSurname();
                flightDate = Objects.isNull(djpGuestData) ? DateUtil.convertStartDate(Objects.isNull(qrForFioriRequest) ? Objects.isNull(newManuelRecordRequest) ? "" : newManuelRecordRequest.getManuelFlightDate() : qrForFioriRequest.getDate()) : DateUtil.convertStartDate(djpGuestData.getDate());
                flightNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(newManuelRecordRequest) ? "" : newManuelRecordRequest.getManuelFlightNo().toUpperCase().substring(2, newManuelRecordRequest.getManuelFlightNo().length()) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length()) : djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                seatNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(newManuelRecordRequest) ? "" : newManuelRecordRequest.getManuelSeatNumber().toUpperCase() : qrForFioriRequest.getSeatCode().toUpperCase() : djpGuestData.getSeatCode().toUpperCase();
                if (seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
            }

            if (StringUtils.isNotBlank(pnrCode)) {
                currentPassangerName = pnrCode.substring(2, 22).toUpperCase();

                flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
                seatNumber = pnrCode.substring(48, 52);
                djpManuelRecord = djpManuelRecordDao.getManuelRecord(pnrCode, QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));

                if (Objects.isNull(djpManuelRecord)) {
                    //checkIfManuelBoarding(qrForFioriRequest, djpManuelRecord, newManuelRecordRequest,currentPassangerName);
                }
            } else {
                djpManuelRecord = djpManuelRecordDao.getManuelRecord(pnrCode, QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));

                if (Objects.isNull(djpManuelRecord)) {
                    // checkIfManuelBoarding(qrForFioriRequest, djpManuelRecord, newManuelRecordRequest, currentPassangerName);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("checkIfUsageByManuelBoarding has an error : ", ex);
        }
        return djpManuelRecord;
    }

    @Override
    public int getDiffBetweenToDate(QrForFioriRequest qrForFioriRequest, NewManuelRecordRequest newManuelRecordRequest, DjpGuestData djpGuestData, String flightNumber, Date flightDate) throws ParseException {
        try {
            boolean couldNotReadBoardingPass = false;
            String getFlightDateByFlightNumber;
            int diffBetweenToDate;
            if (Objects.nonNull(newManuelRecordRequest)) {
                FioriFlightDetailData fioriFlightDetailData = djpFioriFlightFacade.getFioriFlightDetail(flightDate.toString(), flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            } else if (Objects.nonNull(djpGuestData)) {
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? djpGuestData.getFlightNumber().toUpperCase() : djpGuestData.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(djpGuestData.getDate()) : DateUtil.convertJulianDateToDate(djpGuestData.getPnrCode().substring(44, 47));
                FioriFlightDetailData fioriFlightDetailData = djpFioriFlightFacade.getFioriFlightDetail(flightDate.toString(), flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            } else {
                couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? qrForFioriRequest.getFlightNumber().toUpperCase() : qrForFioriRequest.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(qrForFioriRequest.getDate()) : DateUtil.convertJulianDateToDate(qrForFioriRequest.getPnrCode().substring(44, 47));
                FioriFlightDetailData fioriFlightDetailData = djpFioriFlightFacade.getFioriFlightDetail(flightDate.toString(), flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( fioriFlightDetailData.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            }
        }catch (Exception ex){
            LOGGER.error("getDiffBetweenToDate has an error : ",ex);
            return 0;
        }
    }
    @Override
    public ServiceResultWsData saveNewManuelRecord(NewManuelRecordRequest newManuelRecordRequest) {
        try {
            DjpManuelRecordsModel manuelRecord = new DjpManuelRecordsModel();

            ServiceResultWsData serviceResultWsData = validateRequiredFields(newManuelRecordRequest);
            if (serviceResultWsData != null){
                return serviceResultWsData;
            }
            setManuelRecordFields(newManuelRecordRequest,manuelRecord);
            modelService.saveOrUpdate(manuelRecord);
            return DjpServiceUtil.generateSuccessServiceResult();
        }catch (Exception e){
            LOGGER.error("has an error saveNewManuelRecord:",e);
            return DjpServiceUtil.generateErrorServiceResult(e.getMessage());
        }
    }

    private ServiceResultWsData validateRequiredFields(NewManuelRecordRequest newManuelRecordRequest) {
        if (StringUtils.isBlank(newManuelRecordRequest.getInputMethod())) {
            return DjpServiceUtil.generateErrorServiceResult("Giriş Yöntemi gereklidir", HttpStatusCodes.BAD_REQUEST.name());
        } else if (Arrays.stream(ContractedType.values()).anyMatch((contractedType -> contractedType.getCode().equals(newManuelRecordRequest.getInputMethod()))) && StringUtils.isBlank(newManuelRecordRequest.getClientName())) {
            return DjpServiceUtil.generateErrorServiceResult("Müşteri Adı gereklidir", HttpStatusCodes.BAD_REQUEST.name());
        } else if (StringUtils.isBlank(newManuelRecordRequest.getNote())) {
            return DjpServiceUtil.generateErrorServiceResult("Neden Girlmedi / Not gereklidir", HttpStatusCodes.BAD_REQUEST.name());
        } else if (newManuelRecordRequest.getIsManuelFormOpen()) {
            if (StringUtils.isBlank(newManuelRecordRequest.getManuelNameSurname()) || StringUtils.isBlank(newManuelRecordRequest.getManuelFlightDate())
                    || StringUtils.isBlank(newManuelRecordRequest.getManuelFlightCode()) || StringUtils.isBlank(newManuelRecordRequest.getManuelFlightNo())
                    || StringUtils.isBlank(newManuelRecordRequest.getManuelSeatNumber()) || StringUtils.isBlank(newManuelRecordRequest.getManuelFromAirport())
                    || StringUtils.isBlank(newManuelRecordRequest.getManuelToAirport()) || StringUtils.isBlank(newManuelRecordRequest.getManuelClassCode())
            ) {
                return DjpServiceUtil.generateErrorServiceResult("Manuel Giriş Zorunlu alanları doldurunuz", HttpStatusCodes.BAD_REQUEST.name());
            }

        } else {
            if (StringUtils.isBlank(newManuelRecordRequest.getPnrCode())) {
                return DjpServiceUtil.generateErrorServiceResult("Biniş Kartı gereklidir", HttpStatusCodes.BAD_REQUEST.name());
            }
        }
        return null;
    }
    private void setManuelRecordFields(NewManuelRecordRequest newManuelRecordRequest,DjpManuelRecordsModel manuelRecord){
        manuelRecord.setInputMethod(newManuelRecordRequest.getInputMethod());
        manuelRecord.setClientName(newManuelRecordRequest.getClientName());
        manuelRecord.setNote(newManuelRecordRequest.getNote());
        if (newManuelRecordRequest.getIsManuelFormOpen()){
            StringBuilder sb = new StringBuilder();
            sb.append(newManuelRecordRequest.getManuelNameSurname());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelClassCode());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelFromAirport());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelToAirport());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelFlightDate());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelFlightCode());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelFlightNo());
            sb.append("/");
            sb.append(newManuelRecordRequest.getManuelSeatNumber());
            manuelRecord.setPnrCode(sb.toString());
        } else {
            manuelRecord.setPnrCode(newManuelRecordRequest.getPnrCode());
        }
        manuelRecord.setSubMembership(newManuelRecordRequest.getSubMembership());
        manuelRecord.setPhoneNumber(newManuelRecordRequest.getPhoneNumber());
        manuelRecord.setName(newManuelRecordRequest.getName());
        manuelRecord.setProgramNo(newManuelRecordRequest.getProgram());
        manuelRecord.setWhoseGuest(newManuelRecordRequest.getWhoseGuest());
        manuelRecord.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(newManuelRecordRequest.getWhereUsed())));
        manuelRecord.setManuelPnr(newManuelRecordRequest.getIsManuelFormOpen());
        manuelRecord.setPersonalName(newManuelRecordRequest.getPersonalName());
    }
}
