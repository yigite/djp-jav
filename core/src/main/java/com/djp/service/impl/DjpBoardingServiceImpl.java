package com.djp.service.impl;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.dao.DjpBoardingDao;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.enums.TourniquetBoardingStatus;
import com.djp.core.enums.UsageMethod;
import com.djp.core.model.*;
import com.djp.data.DjpGuestData;
import com.djp.data.NurusServiceCreateReservationRequestAttendeeData;
import com.djp.dto.UserWsDto;
import com.djp.event.DjpBoardingPassSendToMwEvent;
import com.djp.facades.DjpRemainingUsageFacade;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.request.*;
import com.djp.response.FioriStatusResponse;
import com.djp.response.NurusServiceCreateReservationResponse;
import com.djp.response.NurusServiceLoginResponse;
import com.djp.service.*;
import com.djp.util.DjpUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import com.djp.core.constant.*;

import static com.djp.core.constant.DjpCommerceservicesConstants.QR_FOR_CARD;

public class DjpBoardingServiceImpl implements DjpBoardingService {

    private static final Logger LOG = LoggerFactory.getLogger(DjpBoardingServiceImpl.class);

    @Value("${project.nurus.createReservationUrl}")
    private String nurusCreateReservationServiceUrl;

    @Value("${project.nurus.service.login.email}")
    private String nurusLoginEmail;

    @Value("${project.nurus.service.login.password}")
    private String nurusLoginPassword;

    @Value("${project.nurus.loginServicesUrl}")
    private String nurusLoginServiceUrl;

    @Value("${project.businesPod.url}")
    private String businessPodTabletServiceUrl;

    @Resource
    private DjpBoardingDao djpBoardingDao;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private DjpQRService djpQRService;

    @Resource
    private List<String> internationalUsagePoint;

    @Resource
    private List<String> domesticUsagePoint;

    @Resource
    private List<String> willNotCheckStatusOfPoint;

    @Resource
    private ModelService modelService;
    
    @Resource
    private HashMap<String, String> passengerTypeMap;

    @Resource
    private HashMap<String, String> middleWareUsageMap;

    @Resource
    private UserService userService;

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private DjpCustomerService djpCustomerService;
    @Resource
    private DjpRemainingUsageFacade djpRemainingUsageFacade;

    @Resource
    private ApplicationEventPublisher applicationEventPublisher;

    @Resource
    private ContractedCompanyService contractedCompanyService;

    @Resource
    private MessageSource messageSource;

    @Resource
    private DjpFioriFlightService djpFioriFlightService;

    @Value("${fiori.host.type}")
    private String fioriHostType;

    @Value("${server.host.type}")
    private String serverHostType;

    @Override
    public DjpBoardingPassModel getCouldReadDjpBoardingPassWithouthDayLimit(String pnrCode, String whereUsed) {
        Date flightDate = null , sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        boolean checkIfWhereUsed = false;
        boolean checkIfNewSale = false;
        DjpBoardingPassModel djpBoardingPass = null;
        if(StringUtils.isNotBlank(pnrCode)){
            operatingCarrierDesignator = pnrCode.substring(36,39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39,44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44,47));
            seatNumber = pnrCode.substring(48,52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            QrType boardingUsageEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
            djpBoardingPass = getDjpBoardingPassModelWithoutDateLimit(flightDate, sub48HoursCurrentDate, flightNumber, seatNumber, operatingCarrierDesignator, boardingUsageEnum);
            if (Objects.nonNull(djpBoardingPass)) {

                checkIfNewSale = Objects.nonNull(djpBoardingPass.getNewSale()) ? djpBoardingPass.getNewSale() : true;

                if (checkIfNewSale) {
                    checkIfWhereUsed = boardingUsageEnum.equals(djpBoardingPass.getBoardingUsage());
                    if (checkIfWhereUsed) {
                        return djpBoardingPass;
                    } else {
                        return null;
                    }
                } else {

                    QrForBankModel qrForBankModel = null;
                    QrForVoucherModel qrForVoucherModel = null;
                    QrForFioriModel qrForFioriModel = null;
                    QrForMobileModel qrForMobileModel = null;
                    QrForMeetModel qrForMeetModel = null;
                    qrForMobileModel = djpQRService.validQrCodeForMobile(djpBoardingPass.getToken().getCode());
                    if (Objects.isNull(qrForMobileModel)) {
                        qrForVoucherModel = djpQRService.validQrCodeForDjp(djpBoardingPass.getToken().getCode());
                        if (Objects.isNull(qrForVoucherModel)) {
                            qrForFioriModel = djpQRService.validQrCodeForFiori(djpBoardingPass.getToken().getCode());
                            if (Objects.isNull(qrForFioriModel)) {
                                qrForBankModel = djpQRService.validQrCodeForBank(djpBoardingPass.getToken().getCode());
                                if (Objects.isNull(qrForBankModel)) {
                                    qrForMeetModel = djpQRService.getQRForMeetByTokenKey(djpBoardingPass.getToken().getCode());
                                    if (Objects.isNull(qrForMeetModel)) {
                                        return djpBoardingPass;
                                    } else {
                                        if (Objects.isNull(qrForMeetModel.getWhereUsed())) {
                                            return djpBoardingPass;
                                        }
                                        checkIfWhereUsed = qrForMeetModel.getWhereUsed().equals(boardingUsageEnum);
                                        if (checkIfWhereUsed) {
                                            return djpBoardingPass;
                                        } else {
                                            return null;
                                        }
                                    }
                                } else {
                                    if (Objects.isNull(qrForBankModel.getWhereUsed())) {
                                        return djpBoardingPass;
                                    }
                                    checkIfWhereUsed = qrForBankModel.getWhereUsed().equals(boardingUsageEnum);
                                    if (checkIfWhereUsed) {
                                        return djpBoardingPass;
                                    } else {
                                        return null;
                                    }
                                }
                            } else {
                                if (Objects.isNull(qrForFioriModel.getWhereUsed())) {
                                    return djpBoardingPass;
                                }
                                checkIfWhereUsed = qrForFioriModel.getWhereUsed().equals(boardingUsageEnum);
                                if (checkIfWhereUsed) {
                                    return djpBoardingPass;
                                } else {
                                    return null;
                                }
                            }
                        } else {
                            if (Objects.isNull(qrForVoucherModel.getWhereUsed())) {
                                return djpBoardingPass;
                            }
                            checkIfWhereUsed = qrForVoucherModel.getWhereUsed().contains(boardingUsageEnum);
                            if (checkIfWhereUsed) {
                                return djpBoardingPass;
                            } else {
                                return null;
                            }
                        }
                    } else {
                        if (Objects.isNull(qrForMobileModel.getWhereUsed())) {
                            return djpBoardingPass;
                        }
                        checkIfWhereUsed = qrForMobileModel.getWhereUsed().equals(boardingUsageEnum);
                        if (checkIfWhereUsed) {
                            return djpBoardingPass;
                        } else {
                            return null;
                        }
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    @Override
    public DjpAccessBoardingModel getCouldReadDjpAccessBoardingWithoutDateLimit(String pnrCode, String whereUsed) {
        Date flightDate = null ,sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        boolean checkIfWhereUsed = false;
        if(StringUtils.isNotBlank(pnrCode)){
            operatingCarrierDesignator = pnrCode.substring(36,39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39,44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44,47));
            seatNumber = pnrCode.substring(48,52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            QrType boardingUsageEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
            DjpAccessBoardingModel djpAccessBoarding = djpBoardingDao.getDjpAccessBoardingWithoutDateLimit(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, boardingUsageEnum);
            if(Objects.nonNull(djpAccessBoarding)){
                checkIfWhereUsed = djpAccessBoarding.getBoardingUsages().contains(boardingUsageEnum);
                if(checkIfWhereUsed){
                    return djpAccessBoarding;
                }else {
                    return null;
                }
            }else {
                return null;
            }
        }else {
            return null;
        }
    }

    @Override
    public void checkIfUsageByBoardingPass(QrForFioriRequest qrForFioriRequest, AccessBoardingPassRequest accBoardingPassRequest, BoardingPassRequest boardingPassRequest, DjpGuestData djpGuestData) {
        DjpAccessBoardingModel djpAccessBoarding = null;
        DjpBoardingPassModel djpBoardingPass = null;
        boolean couldNotReadBoardingPass = false;
        String pnrCode = null;
        String whereUsed = "";
        try {


            if (Objects.isNull(djpGuestData)) {
                if (Objects.nonNull(qrForFioriRequest)) {
                    couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                    pnrCode = !couldNotReadBoardingPass ? qrForFioriRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                    whereUsed = qrForFioriRequest.getBoardingType();
                } else {
                    if (Objects.nonNull(accBoardingPassRequest)) {
                        couldNotReadBoardingPass = Objects.nonNull(accBoardingPassRequest.getCouldNotReadBoardingPass()) && accBoardingPassRequest.getCouldNotReadBoardingPass();
                        pnrCode = !couldNotReadBoardingPass ? accBoardingPassRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                        whereUsed = accBoardingPassRequest.getBoardingType();
                    } else {
                        couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                        pnrCode = !couldNotReadBoardingPass ? boardingPassRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                        whereUsed = boardingPassRequest.getWhereUsed();
                    }
                }
            } else {
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                pnrCode = !couldNotReadBoardingPass ? djpGuestData.getPnrCode().toUpperCase() : StringUtils.EMPTY;
                whereUsed = Objects.nonNull(accBoardingPassRequest) ? accBoardingPassRequest.getBoardingType() : boardingPassRequest.getWhereUsed();
            }

            Date flightDate = null, sub48HoursCurrentDate = null;
            String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
            String currentPassangerName = null;
            if (StringUtils.isBlank(pnrCode)) {
                currentPassangerName = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accBoardingPassRequest.getPassengerName().toUpperCase() : boardingPassRequest.getPassengerName().toUpperCase() : qrForFioriRequest.getPassengerName().toUpperCase() : djpGuestData.getName() + djpGuestData.getSurname();
                flightDate = Objects.isNull(djpGuestData) ? DateUtil.convertStartDate(Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accBoardingPassRequest.getDate() : boardingPassRequest.getDate() : qrForFioriRequest.getDate()) : DateUtil.convertStartDate(djpGuestData.getDate());
                operatingCarrierDesignator = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : boardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2) : djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                flightNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, accBoardingPassRequest.getFlightNumber().length()) : boardingPassRequest.getFlightNumber().toUpperCase().substring(2, boardingPassRequest.getFlightNumber().length()) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length()) : djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                seatNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accBoardingPassRequest.getSeatCode().toUpperCase() : boardingPassRequest.getSeatCode().toUpperCase() : qrForFioriRequest.getSeatCode().toUpperCase() : djpGuestData.getSeatCode().toUpperCase();
                if (seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            }

            if (StringUtils.isNotBlank(pnrCode)) {
                currentPassangerName = pnrCode.substring(2,22).toUpperCase();
                operatingCarrierDesignator = pnrCode.substring(36, 39);
                operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ","") : operatingCarrierDesignator;
                flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
                seatNumber = pnrCode.substring(48, 52);
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
                djpAccessBoarding = djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));

                if (Objects.isNull(djpAccessBoarding)) {
                    djpBoardingPass = djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));
                    checkIfBoardingPass(qrForFioriRequest, djpBoardingPass, null, boardingPassRequest, accBoardingPassRequest,currentPassangerName);
                } else {
                    checkIfBoardingPass(qrForFioriRequest, null, djpAccessBoarding, boardingPassRequest, accBoardingPassRequest,currentPassangerName);
                }
            } else {
                djpAccessBoarding = djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));

                if (Objects.isNull(djpAccessBoarding)) {
                    djpBoardingPass = djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));
                    checkIfBoardingPass(qrForFioriRequest, djpBoardingPass, null, boardingPassRequest, accBoardingPassRequest,currentPassangerName);
                } else {
                    checkIfBoardingPass(qrForFioriRequest, null, djpAccessBoarding, boardingPassRequest, accBoardingPassRequest,currentPassangerName);
                }
            }
        }catch (Exception ex){
            LOG.error("checkIfUsageByBoardingPass has an error : ",ex);
        }
    }

    @Override
    public int getDiffBetweenToDate(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData) throws ParseException {
        try {
            boolean couldNotReadBoardingPass = false;
            String flightNumber;
            Date flightDate;
            String getFlightDateByFlightNumber;
            int diffBetweenToDate;
            if (Objects.nonNull(boardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? boardingPassRequest.getFlightNumber().toUpperCase() : boardingPassRequest.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(boardingPassRequest.getDate()) : DateUtil.convertJulianDateToDate(boardingPassRequest.getPnrCode().substring(44, 47));
                FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern((Date) flightInformationModel.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( flightInformationModel.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            } else if (Objects.nonNull(accessBoardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? accessBoardingPassRequest.getFlightNumber().toUpperCase() : accessBoardingPassRequest.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(accessBoardingPassRequest.getDate()) : DateUtil.convertJulianDateToDate(accessBoardingPassRequest.getPnrCode().substring(44, 47));
                FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern((Date) flightInformationModel.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( flightInformationModel.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            } else if (Objects.nonNull(djpGuestData)) {
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? djpGuestData.getFlightNumber().toUpperCase() : djpGuestData.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(djpGuestData.getDate()) : DateUtil.convertJulianDateToDate(djpGuestData.getPnrCode().substring(44, 47));
                FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern((Date) flightInformationModel.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( flightInformationModel.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            } else {
                couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                flightNumber = couldNotReadBoardingPass ? qrForFioriRequest.getFlightNumber().toUpperCase() : qrForFioriRequest.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(qrForFioriRequest.getDate()) : DateUtil.convertJulianDateToDate(qrForFioriRequest.getPnrCode().substring(44, 47));
                FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                getFlightDateByFlightNumber = DjpUtil.toDatePattern((Date) flightInformationModel.getFlightDate(), "dd/MM/yyyy") + " " + DjpUtil.toDatePattern( flightInformationModel.getFlightDate(), "HH:mm:ss");
                diffBetweenToDate = DateUtil.differenceHoursBetweenTwoDate(new Date(), DateUtil.stringToDate(getFlightDateByFlightNumber));
                return diffBetweenToDate;
            }
        }catch (Exception ex){
            LOG.error("getDiffBetweenToDate has an error : ",ex);
            return 0;
        }
    }

    @Override
    public String checkIfBoardingForLocationIATA(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData, DjpPassLocationsModel djpPassLocation) {
        try {
            boolean couldNotReadBoardingPass = Boolean.FALSE;
            boolean checkIfFastTrackArrivalUsaged = Boolean.FALSE;
            boolean checkIfFastTrackTransferUsaged = Boolean.FALSE;
            boolean checkIfISTFromCity = Boolean.FALSE;
            boolean checkIfISTToCity = Boolean.FALSE;
            boolean isNotValidArrivalUsage = Boolean.FALSE;
            boolean isNotValidTransferUsage = Boolean.FALSE;
            String fromCityAirportCode = null;
            String toCityAirportCode = null;
            String whereUsed = null;

            if (Objects.nonNull(djpGuestData)) {
                whereUsed = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getWhereUsed() : accessBoardingPassRequest.getBoardingType();
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                checkIfFastTrackArrivalUsaged = isCheckIfFastTrackArrival(whereUsed);
                checkIfFastTrackTransferUsaged = isCheckIfFastTrackTransfer(whereUsed);
                fromCityAirportCode = couldNotReadBoardingPass ? djpGuestData.getFromAirportIATA().toUpperCase() : djpGuestData.getPnrCode().substring(30, 33).toUpperCase();
                toCityAirportCode = couldNotReadBoardingPass ? djpGuestData.getToAirportIATA().toUpperCase() : djpGuestData.getPnrCode().substring(33, 36).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                checkIfISTToCity = toCityAirportCode.equals(djpPassLocation.getIata());
                isNotValidArrivalUsage = checkIfFastTrackArrivalUsaged && !checkIfISTToCity;
                isNotValidTransferUsage = (checkIfFastTrackTransferUsaged && !(checkIfISTFromCity || checkIfISTToCity));
            } else if (Objects.nonNull(boardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackArrivalUsaged = isCheckIfFastTrackArrival(boardingPassRequest.getWhereUsed());
                checkIfFastTrackTransferUsaged = isCheckIfFastTrackTransfer(boardingPassRequest.getWhereUsed());
                fromCityAirportCode = couldNotReadBoardingPass ? boardingPassRequest.getFromAirportIATA().toUpperCase() : boardingPassRequest.getPnrCode().substring(30, 33).toUpperCase();
                toCityAirportCode = couldNotReadBoardingPass ? boardingPassRequest.getToAirportIATA().toUpperCase() : boardingPassRequest.getPnrCode().substring(33, 36).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                checkIfISTToCity = toCityAirportCode.equals(djpPassLocation.getIata());
                isNotValidArrivalUsage = checkIfFastTrackArrivalUsaged && !checkIfISTToCity;
                isNotValidTransferUsage = (checkIfFastTrackTransferUsaged && !(checkIfISTFromCity || checkIfISTToCity));
            } else if (Objects.nonNull(accessBoardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackArrivalUsaged = isCheckIfFastTrackArrival(accessBoardingPassRequest.getBoardingType());
                checkIfFastTrackTransferUsaged = isCheckIfFastTrackTransfer(accessBoardingPassRequest.getBoardingType());
                fromCityAirportCode = couldNotReadBoardingPass ? accessBoardingPassRequest.getFromAirportIATA().toUpperCase() : accessBoardingPassRequest.getPnrCode().substring(30, 33).toUpperCase();
                toCityAirportCode = couldNotReadBoardingPass ? accessBoardingPassRequest.getToAirportIATA().toUpperCase() : accessBoardingPassRequest.getPnrCode().substring(33, 36).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                checkIfISTToCity = toCityAirportCode.equals(djpPassLocation.getIata());
                isNotValidArrivalUsage = checkIfFastTrackArrivalUsaged && !checkIfISTToCity;
                isNotValidTransferUsage = (checkIfFastTrackTransferUsaged && !(checkIfISTFromCity || checkIfISTToCity));
            } else {
                couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackArrivalUsaged = isCheckIfFastTrackArrival(qrForFioriRequest.getBoardingType());
                checkIfFastTrackTransferUsaged = isCheckIfFastTrackTransfer(qrForFioriRequest.getBoardingType());
                fromCityAirportCode = couldNotReadBoardingPass ? qrForFioriRequest.getFromAirportIATA().toUpperCase() : qrForFioriRequest.getPnrCode().substring(30, 33).toUpperCase();
                toCityAirportCode = couldNotReadBoardingPass ? qrForFioriRequest.getToAirportIATA().toUpperCase() : qrForFioriRequest.getPnrCode().substring(33, 36).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                checkIfISTToCity = toCityAirportCode.equals(djpPassLocation.getIata());
                isNotValidArrivalUsage = checkIfFastTrackArrivalUsaged && !checkIfISTToCity;
                isNotValidTransferUsage = (checkIfFastTrackTransferUsaged && !(checkIfISTFromCity || checkIfISTToCity));
            }
            if (Boolean.TRUE.equals(isNotValidArrivalUsage)) {
                return messageSource.getMessage(
                        "fast.track.invalid.arrival.usage.text",
                        new Object[]{djpPassLocation.getName(),Objects.nonNull(djpGuestData) ? messageSource.getMessage("guest.detail.text",null, LocaleContextHolder.getLocale()) : messageSource.getMessage("passenger.detail.text",null,LocaleContextHolder.getLocale())}
                        ,LocaleContextHolder.getLocale());
            } else if (Boolean.TRUE.equals(isNotValidTransferUsage)) {
                return messageSource.getMessage(
                        "fast.track.invalid.transfer.usage.text", new Object[]{djpPassLocation.getName(),Objects.nonNull(djpGuestData) ? messageSource.getMessage("guest.detail.text",null,LocaleContextHolder.getLocale()) :  messageSource.getMessage("passenger.detail.text",null,LocaleContextHolder.getLocale()),LocaleContextHolder.getLocale()}
                        ,LocaleContextHolder.getLocale());
            } else {
                return StringUtils.EMPTY;
            }
        } catch (Exception ex) {
            LOG.error("checkIfBoardingForLocationIATA has an error : ", ex);
            return StringUtils.EMPTY;
        }
    }

    @Override
    public boolean checkIfBoardingFromLocationIATA(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData, DjpPassLocationsModel djpPassLocation) {         
        try {
            boolean couldNotReadBoardingPass = false;
            boolean checkIfFastTrackDepartureUsaged = false;
            boolean checkIfISTFromCity = false;
            String fromCityAirportCode = null;
            String whereUsed = null;
            if (Objects.nonNull(djpGuestData)) {
                whereUsed = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getWhereUsed() : accessBoardingPassRequest.getBoardingType();
                couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                checkIfFastTrackDepartureUsaged = isCheckIfFastTrackDepartureUsaged(whereUsed);
                fromCityAirportCode = couldNotReadBoardingPass ? djpGuestData.getFromAirportIATA().toUpperCase() : djpGuestData.getPnrCode().substring(30, 33).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                return checkIfFastTrackDepartureUsaged && !checkIfISTFromCity;
            } else if (Objects.nonNull(boardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackDepartureUsaged = isCheckIfFastTrackDepartureUsaged(boardingPassRequest.getWhereUsed());
                fromCityAirportCode = couldNotReadBoardingPass ? boardingPassRequest.getFromAirportIATA().toUpperCase() : boardingPassRequest.getPnrCode().substring(30, 33).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                return checkIfFastTrackDepartureUsaged && !checkIfISTFromCity;
            } else if (Objects.nonNull(accessBoardingPassRequest)) {
                couldNotReadBoardingPass = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackDepartureUsaged = isCheckIfFastTrackDepartureUsaged(accessBoardingPassRequest.getBoardingType());
                fromCityAirportCode = couldNotReadBoardingPass ? accessBoardingPassRequest.getFromAirportIATA().toUpperCase() : accessBoardingPassRequest.getPnrCode().substring(30, 33).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                return checkIfFastTrackDepartureUsaged && !checkIfISTFromCity;
            } else {
                couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                checkIfFastTrackDepartureUsaged = isCheckIfFastTrackDepartureUsaged(qrForFioriRequest.getBoardingType());
                fromCityAirportCode = couldNotReadBoardingPass ? qrForFioriRequest.getFromAirportIATA().toUpperCase() : qrForFioriRequest.getPnrCode().substring(30, 33).toUpperCase();
                checkIfISTFromCity = fromCityAirportCode.equals(djpPassLocation.getIata());
                return checkIfFastTrackDepartureUsaged && !checkIfISTFromCity;
            }
        }catch (Exception ex){
            LOG.error("checkIfBoardingFromLocationIATA has an error : ",ex);
            return Boolean.FALSE;
        }
    }

    @Override
    public String checkInternationalStatusForBp(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpGuestData djpGuestData, QrType whereUsed) throws ParseException {
        try {
            boolean couldNotReadBoardingPass = false;
            boolean isStartWithM1 = false;
            String flightNumber;
            Date flightDate;
            FlightInformationModel flightInformationModel = null;
            String whereUsedCode = Objects.nonNull(whereUsed) ? whereUsed.getCode() : StringUtils.EMPTY;
            boolean isCheckStatus = StringUtils.isNotBlank(whereUsedCode) && !willNotCheckStatusOfPoint.contains(whereUsedCode);
            if(Boolean.TRUE.equals(isCheckStatus)) {
                if (Objects.nonNull(boardingPassRequest)) {
                    couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                    isStartWithM1 = couldNotReadBoardingPass ? Boolean.TRUE : boardingPassRequest.getPnrCode().toUpperCase().startsWith(DjpCommerceservicesConstants.StatusForFlightHours.M1);
                    if(Boolean.FALSE.equals(isStartWithM1)){
                        return StringUtils.EMPTY;
                    }
                    flightNumber = couldNotReadBoardingPass ? boardingPassRequest.getFlightNumber().toUpperCase() : boardingPassRequest.getPnrCode().substring(36, 43).toUpperCase();
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(boardingPassRequest.getDate()) : DateUtil.convertJulianDateToDate(boardingPassRequest.getPnrCode().substring(44, 47));
                    flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                } else if (Objects.nonNull(accessBoardingPassRequest)) {
                    couldNotReadBoardingPass = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();
                    isStartWithM1 = couldNotReadBoardingPass ? Boolean.TRUE : accessBoardingPassRequest.getPnrCode().toUpperCase().startsWith(DjpCommerceservicesConstants.StatusForFlightHours.M1);
                    if(Boolean.FALSE.equals(isStartWithM1)){
                        return StringUtils.EMPTY;
                    }
                    flightNumber = couldNotReadBoardingPass ? accessBoardingPassRequest.getFlightNumber().toUpperCase() : accessBoardingPassRequest.getPnrCode().substring(36, 43).toUpperCase();
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(accessBoardingPassRequest.getDate()) : DateUtil.convertJulianDateToDate(accessBoardingPassRequest.getPnrCode().substring(44, 47));
                    flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                } else if (Objects.nonNull(djpGuestData)) {
                    couldNotReadBoardingPass = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                    isStartWithM1 = couldNotReadBoardingPass ? Boolean.TRUE : djpGuestData.getPnrCode().toUpperCase().startsWith(DjpCommerceservicesConstants.StatusForFlightHours.M1);
                    if(Boolean.FALSE.equals(isStartWithM1)){
                        return StringUtils.EMPTY;
                    }
                    flightNumber = couldNotReadBoardingPass ? djpGuestData.getFlightNumber().toUpperCase() : djpGuestData.getPnrCode().substring(36, 43).toUpperCase();
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(djpGuestData.getDate()) : DateUtil.convertJulianDateToDate(djpGuestData.getPnrCode().substring(44, 47));
                    flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                } else {
                    couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
                    isStartWithM1 = couldNotReadBoardingPass ? Boolean.TRUE : qrForFioriRequest.getPnrCode().toUpperCase().startsWith(DjpCommerceservicesConstants.StatusForFlightHours.M1);
                    if(Boolean.FALSE.equals(isStartWithM1)){
                        return StringUtils.EMPTY;
                    }
                    flightNumber = couldNotReadBoardingPass ? qrForFioriRequest.getFlightNumber().toUpperCase() : qrForFioriRequest.getPnrCode().substring(36, 43).toUpperCase();
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    flightDate = couldNotReadBoardingPass ? DateUtil.convertStartDate(qrForFioriRequest.getDate()) : DateUtil.convertJulianDateToDate(qrForFioriRequest.getPnrCode().substring(44, 47));
                    flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                }
                String internationalStatus = flightInformationModel.getInternationalStatus();
                boolean isInternationalFlight = DjpCommerceservicesConstants.StatusForFlightHours.INTERNATIONAL.equals(internationalStatus);
                if (Boolean.TRUE.equals(isInternationalFlight)) {
                    boolean useTheInternational = internationalUsagePoint.contains(whereUsedCode);
                    if (Boolean.TRUE.equals(useTheInternational)) {
                        return StringUtils.EMPTY;
                    } else {
                        return DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC;
                    }
                } else {
                    boolean useTheDomestic = domesticUsagePoint.contains(whereUsedCode);
                    if (Boolean.TRUE.equals(useTheDomestic)) {
                        return StringUtils.EMPTY;
                    } else {
                        return DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL;
                    }
                }
            }else {
                return StringUtils.EMPTY;
            }
        }catch (Exception ex){
            LOG.error("checkInternationalStatusForBp has an error : ",ex);
            return DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP;
        }
    }

    @Override
    public String saveDjpBoardingPass(BoardingPassRequest boardingPassRequest) {
        try {
            DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(boardingPassRequest.getLocationID());
            QrForVoucherModel qrForVoucherModel = null;
            QrForBankModel qrForBankModel = null;
            QrForExperienceCenterModel qrForExperienceCenterModel = null;
            String pnrCode = null;
            DjpBoardingPassModel boardingPass = null;
            if(StringUtils.isNotBlank(boardingPassRequest.getPnrCode())) {
                pnrCode = boardingPassRequest.getPnrCode().toUpperCase().replaceAll("\\.","/");
                pnrCode = pnrCode.replaceAll("&","/");
                boardingPassRequest.setPnrCode(pnrCode);
            }
            if (ifHasSameBp(boardingPassRequest,null,null,true)){
                return "hasSameBp";
            }
            if (ifHasSameBp(boardingPassRequest,null,null,false)){
                return "hasSameGuestBp";
            }
            //DjpBoardingPassModel boardingPass = getBoardingPass(boardingPassRequest,null,boardingPassRequest.getWhereUsed());
            checkIfUsageByBoardingPass(null,null,boardingPassRequest,null);
            if(!boardingPassRequest.getCanUseService()){
                return "hasBp";
            }
            boolean hasGuestBp = boardingPassRequest.getGuests().stream().anyMatch(djpGuestData -> {
                checkIfUsageByBoardingPass(null,null, boardingPassRequest,djpGuestData);
                return !boardingPassRequest.getCanUseService();
            });

            if(hasGuestBp && !boardingPassRequest.getCanUseService()){
                return "hasGuestBp";
            }else{
                boardingPass = new DjpBoardingPassModel();
            }

            DjpUniqueTokenModel tokenModel=djpTokenService.encodeCustomerFromToken(boardingPassRequest.getTokenKey());
            if(Objects.isNull(tokenModel)){
                QrForCardModel cardByTokenKey = djpQRService.getQRForCardByTokenKey(boardingPassRequest.getTokenKey());
                if(Objects.nonNull(cardByTokenKey)){
                    tokenModel = djpTokenService.generateTokenForDjp(cardByTokenKey.getCardId(),QR_FOR_CARD);
                    cardByTokenKey.setToken(tokenModel);
                    modelService.saveOrUpdate(cardByTokenKey);
                }
            }else {
                qrForVoucherModel = djpQRService.validQrCodeForDjp(boardingPassRequest.getTokenKey());
                qrForBankModel = djpQRService.validQrCodeForBank(boardingPassRequest.getTokenKey());
                boolean checkBankOrVoucher = Objects.nonNull(qrForBankModel) || Objects.nonNull(qrForVoucherModel);
                boolean isForSomeBody = Objects.nonNull(qrForVoucherModel) && Boolean.TRUE.equals(qrForVoucherModel.getVoucherForRemainingUsage());
                boolean isForExternal = Objects.nonNull(qrForVoucherModel) && Boolean.TRUE.equals(qrForVoucherModel.getVoucherForDjpExternalCustomer());
                if(checkBankOrVoucher && StringUtils.isBlank(boardingPassRequest.getContractID()) && !isForSomeBody && !isForExternal){
                    return "400";
                }
            }
            if(tokenModel != null){
                EmployeeModel employeeModel = null;
                try {
                    employeeModel = (EmployeeModel) userService.getUserForUID(boardingPassRequest.getPersonalID());
                    tokenModel.setPersonalID(StringUtils.isNotBlank(boardingPassRequest.getPersonalID()) ? boardingPassRequest.getPersonalID() : null);
                    tokenModel.setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                } catch (Exception e){
                    LOG.error("Cannot find user with uid : " + boardingPassRequest.getPersonalID());
                }
            }

            qrForExperienceCenterModel = djpQRService.getQrForExperienceCenter(boardingPassRequest.getTokenKey());
            if (Objects.nonNull(qrForExperienceCenterModel)){
                qrForExperienceCenterModel.setIsPaymentSuccess(true);
                modelService.saveOrUpdate(qrForExperienceCenterModel);
            }
            boardingPass.setToken(tokenModel);
            boardingPass.setProductCode(StringUtils.isNotBlank(boardingPassRequest.getProductCode()) ? boardingPassRequest.getProductCode() : null);
            boardingPass.setPassportNumber(StringUtils.isNotBlank(boardingPassRequest.getPassNumber()) ? boardingPassRequest.getPassNumber() : null);
            if(!CollectionUtils.isEmpty(boardingPassRequest.getGuests())){
                List<DjpGuestModel> guests=new ArrayList<>();
                boardingPassRequest.getGuests().forEach(djpGuest -> {
                    DjpGuestModel djpGuestModel = new DjpGuestModel();
                    djpGuestModel.setName(djpGuest.getName());
                    djpGuestModel.setPassport(djpGuest.getPassport());
                    djpGuestModel.setSurname(djpGuest.getSurname());
                    djpGuestModel.setPhoneNumber(djpGuest.getPhoneNumber());
                    djpGuestModel.setTckn(djpGuest.getTckn());
                    djpGuestModel.setBirthDate(djpGuest.getBirthDate());
                    djpGuestModel.setEmail(djpGuest.getEmail());
                    djpGuestModel.setGender(djpGuest.getGender());
                    djpGuestModel.setNationality(djpGuest.getNationality());
                    djpGuestModel.setPnrCode(djpGuest.getPnrCode().toUpperCase());
                    djpGuestModel.setKvkkCheck(djpGuest.getKvkkCheck());
                    djpGuestModel.setWifiCheck(djpGuest.getWifiCheck());
                    modelService.saveOrUpdate(djpGuestModel);
                    guests.add(djpGuestModel);
                });
                boardingPass.setGuests(guests);
            }
            boolean couldNotReadBoardingPass = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
            if(couldNotReadBoardingPass){
                Date dateOfFlight = DateUtil.convertStartDate(boardingPassRequest.getDate());
                String operatingCarrierDesignator =  boardingPassRequest.getFlightNumber().toUpperCase().substring(0,2);
                String flightNumber = boardingPassRequest.getFlightNumber().toUpperCase().substring(2,boardingPassRequest.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ","") : flightNumber;
                String seatNumber = boardingPassRequest.getSeatCode().toUpperCase();
                String fromCityAirportCode = boardingPassRequest.getFromAirportIATA().toUpperCase();
                String toCityAirportCode = boardingPassRequest.getToAirportIATA().toUpperCase();
                String compartmentCode = boardingPassRequest.getClassCode().toUpperCase();
                String passengerName = boardingPassRequest.getPassengerName();
                if(seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
                boardingPass.setDateOfFlight(dateOfFlight);
                boardingPass.setFlightNumber(flightNumber);
                boardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                boardingPass.setSeatNumber(seatNumber);
                boardingPass.setFromCityAirportCode(fromCityAirportCode);
                boardingPass.setToCityAirportCode(toCityAirportCode);
                boardingPass.setCompartmentCode(compartmentCode);
                boardingPass.setPassengerName(passengerName);
                boardingPass.setPnrCode(passengerName + "/" + compartmentCode + "/" + fromCityAirportCode + "/" + toCityAirportCode + "/" + DateUtil.toDatePattern(dateOfFlight,"dd.MM.yyyy") +"/"+operatingCarrierDesignator+ "/" + flightNumber + "/" + seatNumber);
                boardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPass);
                boardingPass.setSapOrderID(boardingPassRequest.getOrderID());
            }else {
                boardingPass.setPnrCode(pnrCode);
                boardingPass.setFormatCode(pnrCode.substring(0, 1));
                boardingPass.setNumberOfLegsEncoded(pnrCode.substring(1,2));
                boardingPass.setPassengerName(pnrCode.substring(2,22));
                boardingPass.setElectronicTicketIndicator(pnrCode.substring(22,23));
                boardingPass.setOperatingCarrierPnrCode(pnrCode.substring(23,30));
                boardingPass.setFromCityAirportCode(pnrCode.substring(30,33));
                boardingPass.setToCityAirportCode(pnrCode.substring(33,36));
                String operatingCarrierDesignator = pnrCode.substring(36,39);
                operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ","") : operatingCarrierDesignator;
                boardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                String flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                boardingPass.setFlightNumber(flightNumber);
                boardingPass.setDateOfFlight(DateUtil.convertJulianDateToDate(pnrCode.substring(44,47)));
                boardingPass.setCompartmentCode(pnrCode.substring(47,48));
                boardingPass.setSeatNumber(pnrCode.substring(48,52));
                boardingPass.setCheckInSequenceNumber(pnrCode.substring(52,57));
                boardingPass.setPassengerStatus(pnrCode.substring(57,58));
                boardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPass);
                boardingPass.setSapOrderID(boardingPassRequest.getOrderID());
            }

            boardingPass.setWifiPassword(boardingPassRequest.getWifiKey());
            boardingPass.setContractID(boardingPassRequest.getContractID());
            boardingPass.setNewSale(boardingPassRequest.getNewSale());
            boardingPass.setUsageMethod(UsageMethod.DESK);
            boardingPass.setPersonalID(StringUtils.isNotBlank(boardingPassRequest.getPersonalID()) ? boardingPassRequest.getPersonalID() : null);
            EmployeeModel employeeModel = null;
            try {
                employeeModel = (EmployeeModel) userService.getUserForUID(boardingPassRequest.getPersonalID());
            } catch (Exception e){
                LOG.error("Cannot find user with uid : " + boardingPassRequest.getPersonalID());
            }
            boardingPass.setPersonalName(employeeModel != null ? employeeModel.getName() : null);
            QrType whereUsedEnum = QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()));
            boardingPass.setBoardingUsage(whereUsedEnum);
            boardingPass.setWhichLocation(djpPassLocation);
            boardingPass.setPassengerType(PassengerType.valueOf(passengerTypeMap.get(boardingPassRequest.getPassengerType())));
            boardingPass.setMiddleWareDetail(getStatusForMiddleWare(boardingPass.getMiddleWareDetail(),boardingPass.getCouldNotReadBoardingPass(),boardingPass.getFromCityAirportCode(),boardingPass.getDateOfFlight(),boardingPass.getOperatingCarrierDesignator()+boardingPass.getFlightNumber(),whereUsedEnum));
            QrForCabinetModel cabinetModel = null;
            if (boardingPass.getBoardingUsage().equals(QrType.BUSINESS_CABINET_INTERNATIONAL)){
                if(StringUtils.isNotBlank(boardingPassRequest.getQrCode())){
                    cabinetModel = djpQRService.getQrForCabinet(boardingPassRequest.getQrCode());
                    if(cabinetModel != null){
                        boardingPass.setToken(cabinetModel.getTokenKey());
                        cabinInfoSendToTable(cabinetModel, boardingPassRequest.getQrCode());
                        String tokenKey = getNurusLoginTokenKey();
                        if(StringUtils.isNotBlank(tokenKey)){
                            postNurusCreateReservation(cabinetModel,tokenKey);
                        }
                    }
                }
            }
            if(CollectionUtils.isNotEmpty(boardingPassRequest.getCampaignNames())){
                boardingPass.setCampaignName(boardingPassRequest.getCampaignNames());
            }
            modelService.saveOrUpdate(boardingPass);
            if (boardingPass.getBoardingUsage().equals(QrType.LOUNGE_DOMESTIC) || boardingPass.getBoardingUsage().equals(QrType.LOUNGE_INTERNATIONAL)){
                BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                if(Objects.nonNull(baseStoreModel)){
                    Integer lengthOfStay = baseStoreModel.getIndividualLengthOfStayHour();
                    if (lengthOfStay > 0){
                        generateServicePlaningData(null,boardingPass,null,lengthOfStay);
                    }
                }
            }
            applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),boardingPass,null,whereUsedEnum));
            DjpBoardingPassModel superBoardingPass = boardingPass;
            boardingPassRequest.getGuests().forEach(djpGuestData -> {
                DjpBoardingPassModel guestBoardingPass = getBoardingPass(boardingPassRequest,null, djpGuestData,boardingPassRequest.getWhereUsed());
                if(Objects.isNull(guestBoardingPass)) {
                    guestBoardingPass = new DjpBoardingPassModel();
                    List<DjpBoardingPassModel> superBoarding = new ArrayList<DjpBoardingPassModel>();
                    superBoarding.add(superBoardingPass);
                    guestBoardingPass.setSuperBoarding(superBoarding);
                    QrForCabinetModel guestCabin = null;
                    if (superBoardingPass.getBoardingUsage().equals(QrType.BUSINESS_CABINET_INTERNATIONAL)){
                        if(StringUtils.isNotBlank(djpGuestData.getQrCode())){
                            guestCabin = djpQRService.getQrForCabinet(djpGuestData.getQrCode());
                            if(guestCabin != null){
                                guestBoardingPass.setToken(guestCabin.getTokenKey());
                                cabinInfoSendToTable(guestCabin, guestCabin.getTokenKey().getCode());
                                String tokenKey = getNurusLoginTokenKey();
                                if(StringUtils.isNotBlank(tokenKey)){
                                    postNurusCreateReservation(guestCabin,tokenKey);
                                }
                            }
                        }
                    }else {
                        guestBoardingPass.setToken(superBoardingPass.getToken());
                    }
                    boolean couldNotReadBoardingPassForGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                    if (couldNotReadBoardingPassForGuest) {
                        Date dateOfFlight = DateUtil.convertStartDate(djpGuestData.getDate());
                        String operatingCarrierDesignator = djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                        String flightNumber = djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                        flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                        String seatNumber = djpGuestData.getSeatCode().toUpperCase();
                        String fromCityAirportCode = djpGuestData.getFromAirportIATA().toUpperCase();
                        String toCityAirportCode = djpGuestData.getToAirportIATA().toUpperCase();
                        String compartmentCode = djpGuestData.getClassCode().toUpperCase();
                        String passengerName = djpGuestData.getName() + djpGuestData.getSurname();
                        if(seatNumber.length() < 4) {
                            do {
                                String zero = "0";
                                String blank = " ";
                                if (seatNumber.startsWith("NS")) {
                                    seatNumber = seatNumber + blank;
                                } else {
                                    seatNumber = zero + seatNumber;
                                }
                            } while (seatNumber.length() != 4);
                        }
                        guestBoardingPass.setDateOfFlight(dateOfFlight);
                        guestBoardingPass.setFlightNumber(flightNumber);
                        guestBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                        guestBoardingPass.setSeatNumber(seatNumber);
                        guestBoardingPass.setFromCityAirportCode(fromCityAirportCode);
                        guestBoardingPass.setToCityAirportCode(toCityAirportCode);
                        guestBoardingPass.setCompartmentCode(compartmentCode);
                        guestBoardingPass.setPassengerName(passengerName);
                        guestBoardingPass.setPnrCode(passengerName + "/" + compartmentCode + "/" + fromCityAirportCode + "/" + toCityAirportCode + "/" + DateUtil.toDatePattern(dateOfFlight,"dd.MM.yyyy") +"/"+operatingCarrierDesignator+ "/" + flightNumber + "/" + seatNumber);
                        guestBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                        guestBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                        guestBoardingPass.setNewSale(superBoardingPass.getNewSale());
                    } else {
                        String guestDataPnrCode = djpGuestData.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                        guestDataPnrCode = guestDataPnrCode.replaceAll("&", "/");
                        guestBoardingPass.setPnrCode(guestDataPnrCode);
                        guestBoardingPass.setFormatCode(guestDataPnrCode.substring(0, 1));
                        guestBoardingPass.setNumberOfLegsEncoded(guestDataPnrCode.substring(1, 2));
                        guestBoardingPass.setPassengerName(guestDataPnrCode.substring(2, 22));
                        guestBoardingPass.setElectronicTicketIndicator(guestDataPnrCode.substring(22, 23));
                        guestBoardingPass.setOperatingCarrierPnrCode(guestDataPnrCode.substring(23, 30));
                        guestBoardingPass.setFromCityAirportCode(guestDataPnrCode.substring(30, 33));
                        guestBoardingPass.setToCityAirportCode(guestDataPnrCode.substring(33, 36));
                        String operatingCarrierDesignator = guestDataPnrCode.substring(36,39);
                        operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ","") : operatingCarrierDesignator;
                        guestBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                        String flightNumber = guestDataPnrCode.substring(39, 44);
                        flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                        guestBoardingPass.setFlightNumber(flightNumber);
                        guestBoardingPass.setDateOfFlight(DateUtil.convertJulianDateToDate(guestDataPnrCode.substring(44, 47)));
                        guestBoardingPass.setCompartmentCode(guestDataPnrCode.substring(47, 48));
                        guestBoardingPass.setSeatNumber(guestDataPnrCode.substring(48, 52));
                        guestBoardingPass.setCheckInSequenceNumber(guestDataPnrCode.substring(52, 57));
                        guestBoardingPass.setPassengerStatus(guestDataPnrCode.substring(57, 58));
                        guestBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                        guestBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                        guestBoardingPass.setNewSale(superBoardingPass.getNewSale());
                    }
                }
                List<DjpBoardingPassModel> superBoarding = null;
                if(CollectionUtils.isNotEmpty(guestBoardingPass.getSuperBoarding())){
                    superBoarding = new ArrayList<>(guestBoardingPass.getSuperBoarding());
                }else {
                    superBoarding = new ArrayList<>();
                }
                superBoarding.add(superBoardingPass);
                guestBoardingPass.setSuperBoarding(superBoarding);
                guestBoardingPass.setWifiPassword(djpGuestData.getWifiKey());
                guestBoardingPass.setContractID(superBoardingPass.getContractID());
                guestBoardingPass.setPersonalID(superBoardingPass.getPersonalID());
                guestBoardingPass.setPersonalName(superBoardingPass.getPersonalName());
                guestBoardingPass.setBoardingUsage(superBoardingPass.getBoardingUsage());
                guestBoardingPass.setWhichLocation(superBoardingPass.getWhichLocation());
                guestBoardingPass.setPassengerType(PassengerType.valueOf(passengerTypeMap.get(djpGuestData.getPassengerType())));
                guestBoardingPass.setMiddleWareDetail(getStatusForMiddleWare(guestBoardingPass.getMiddleWareDetail(),guestBoardingPass.getCouldNotReadBoardingPass(),guestBoardingPass.getFromCityAirportCode(),guestBoardingPass.getDateOfFlight(),guestBoardingPass.getOperatingCarrierDesignator()+guestBoardingPass.getFlightNumber(),whereUsedEnum));
                guestBoardingPass.setUsageMethod(UsageMethod.DESK);
                modelService.saveOrUpdate(guestBoardingPass);
                applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),guestBoardingPass,null,whereUsedEnum));
            });
            return Objects.nonNull(boardingPass) ? "200" : "202";
        }catch (Exception exc){
            LOG.error("error on save DjpBoardingPass : ",exc);
            return "202";
        }
    }
    @Override
    public  DjpMiddleWareBoardingPassStatusModel getStatusForMiddleWare(DjpMiddleWareBoardingPassStatusModel statusModel,Boolean couldNotReadBP, String fromIATA, Date flightDate, String flightNumber, QrType whereUsed) {

        try {
            boolean middleWareStatusShouldBeDetermined = middleWareUsageMap.containsKey(whereUsed.getCode());
            if (Boolean.TRUE.equals(middleWareStatusShouldBeDetermined)) {
                if (Objects.isNull(statusModel)) {
                    statusModel = new DjpMiddleWareBoardingPassStatusModel();
                }
                if (Boolean.TRUE.equals(couldNotReadBP)) {
                    statusModel.setStatusForMiddleWare(2);
                } else {
                    if (!DjpCommerceservicesConstants.DJP_IATA.equals(fromIATA)) {
                        statusModel.setStatusForMiddleWare(3);
                    } else {
                        FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                        String operational_status = flightInformationModel.getOperationalStatus(); // OP değilse iptal olmuş uçuş.
                        if (!DjpCommerceservicesConstants.StatusForFlightHours.OP.equals(operational_status)) {
                            statusModel.setStatusForMiddleWare(5);
                        } else {
                            String international_status = flightInformationModel.getInternationalStatus(); // iç mi dış mı değerini dönüyor.
                            boolean isInternationalFlight = DjpCommerceservicesConstants.StatusForFlightHours.INTERNATIONAL.equals(international_status);
                            if (Boolean.TRUE.equals(isInternationalFlight)) {
                                boolean useTheInternational = internationalUsagePoint.contains(whereUsed.getCode());
                                if (Boolean.TRUE.equals(useTheInternational)) {
                                    statusModel.setStatusForMiddleWare(9);
                                } else {
                                    statusModel.setStatusForMiddleWare(8);
                                }
                            } else {
                                boolean useTheDomestic = domesticUsagePoint.contains(whereUsed.getCode());
                                if (Boolean.TRUE.equals(useTheDomestic)) {
                                    statusModel.setStatusForMiddleWare(9);
                                } else {
                                    statusModel.setStatusForMiddleWare(7);
                                }
                            }
                        }
                    }
                }
                modelService.saveOrUpdate(statusModel);
                return statusModel;
            } else {
                return statusModel;
            }
        } catch (Exception ex) {
            LOG.error("getStatusForMiddleWare has an error:", ex);
            statusModel.setStatusForMiddleWare(-2);
            modelService.saveOrUpdate(statusModel);
            return statusModel;
        }
    }

    @Override
    public boolean ifHasSameBp(BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, DjpMobilePassRequest djpMobilePassRequest, boolean checkOwner) {
        try {
            Date dateOfFlight = null, dateOffFlightGuest = null, dateOffFlightGuesto1 = null, dateOffFlightGuesto2 = null;
            String operatingCarrierDesignator = null, operatingCarrierDesignatorGuest = null, operatingCarrierDesignatorGuesto1 = null, operatingCarrierDesignatorGuesto2 = null;
            String flightNumber = null, flightNumberGuest = null, flightNumberGuesto1 = null, flightNumberGuesto2 = null;
            String seatNumber = null, seatNumberGuest = null, seatNumberGuesto1 = null, seatNumberGuesto2 = null;
            if (checkOwner) {
                List<DjpGuestData> djpGuestDataList = new ArrayList<>();
                if (Objects.nonNull(boardingPassRequest)) {
                    boardingPassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataList.add(djpGuestData);
                    });
                } else if(Objects.nonNull(accessBoardingPassRequest)){
                    accessBoardingPassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataList.add(djpGuestData);
                    });
                }else {
                    djpMobilePassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataList.add(djpGuestData);
                    });
                }
                boolean couldNotReadBoardingPassForOwner = false;
                if(Objects.isNull(djpMobilePassRequest)) {
                    if (Objects.nonNull(boardingPassRequest)) {
                        couldNotReadBoardingPassForOwner = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
                    } else {
                        couldNotReadBoardingPassForOwner = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();
                    }
                }

                if (couldNotReadBoardingPassForOwner) {
                    dateOfFlight = DateUtil.convertStartDate(Objects.isNull(boardingPassRequest) ? accessBoardingPassRequest.getDate() : boardingPassRequest.getDate());
                    operatingCarrierDesignator = Objects.isNull(boardingPassRequest) ? accessBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : boardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2);
                    flightNumber = Objects.isNull(boardingPassRequest) ? accessBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, accessBoardingPassRequest.getFlightNumber().length()) : boardingPassRequest.getFlightNumber().toUpperCase().substring(2, boardingPassRequest.getFlightNumber().length());
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    seatNumber = Objects.isNull(boardingPassRequest) ? accessBoardingPassRequest.getSeatCode().toUpperCase() : boardingPassRequest.getSeatCode().toUpperCase();
                    if(seatNumber.length() < 4) {
                        do {
                            String zero = "0";
                            String blank = " ";
                            if (seatNumber.startsWith("NS")) {
                                seatNumber = seatNumber + blank;
                            } else {
                                seatNumber = zero + seatNumber;
                            }
                        } while (seatNumber.length() != 4);
                    }
                } else {
                    dateOfFlight = DateUtil.convertJulianDateToDate(Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getPnrCode().toUpperCase().substring(44, 47) : Objects.nonNull(accessBoardingPassRequest) ? accessBoardingPassRequest.getPnrCode().toUpperCase().substring(44, 47) : djpMobilePassRequest.getOwnerPnrCode().toUpperCase().substring(44, 47));
                    operatingCarrierDesignator = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getPnrCode().toUpperCase().substring(36, 39) : Objects.nonNull(accessBoardingPassRequest) ? accessBoardingPassRequest.getPnrCode().toUpperCase().substring(36, 39) : djpMobilePassRequest.getOwnerPnrCode().toUpperCase().substring(36, 39);
                    operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
                    flightNumber = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getPnrCode().toUpperCase().substring(39, 44) : Objects.nonNull(accessBoardingPassRequest) ? accessBoardingPassRequest.getPnrCode().toUpperCase().substring(39, 44) : djpMobilePassRequest.getOwnerPnrCode().toUpperCase().substring(39, 44);
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    seatNumber = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getPnrCode().toUpperCase().substring(48, 52) : Objects.nonNull(accessBoardingPassRequest) ? accessBoardingPassRequest.getPnrCode().toUpperCase().substring(48, 52) : djpMobilePassRequest.getOwnerPnrCode().toUpperCase().substring(48, 52);
                }
                for (DjpGuestData djpGuestData : djpGuestDataList) {
                    boolean couldNotReadBoardingPassForGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                    if (!couldNotReadBoardingPassForGuest) {
                        dateOffFlightGuest = DateUtil.convertJulianDateToDate(djpGuestData.getPnrCode().toUpperCase().substring(44, 47));
                        operatingCarrierDesignatorGuest = djpGuestData.getPnrCode().toUpperCase().substring(36, 39);
                        operatingCarrierDesignatorGuest = operatingCarrierDesignatorGuest.contains(" ") ? operatingCarrierDesignatorGuest.replace(" ", "") : operatingCarrierDesignatorGuest;
                        flightNumberGuest = djpGuestData.getPnrCode().toUpperCase().substring(39, 44);
                        flightNumberGuest = flightNumberGuest.contains(" ") ? flightNumberGuest.replace(" ", "") : flightNumberGuest;
                        seatNumberGuest = djpGuestData.getPnrCode().toUpperCase().substring(48, 52);
                    } else {
                        dateOffFlightGuest = DateUtil.convertStartDate(djpGuestData.getDate());
                        operatingCarrierDesignatorGuest = djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                        flightNumberGuest = djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                        flightNumberGuest = flightNumberGuest.contains(" ") ? flightNumberGuest.replace(" ", "") : flightNumberGuest;
                        seatNumberGuest = djpGuestData.getSeatCode().toUpperCase();
                        if(seatNumberGuest.length() < 4) {
                            do {
                                String zero = "0";
                                String blank = " ";
                                if (seatNumberGuest.startsWith("NS")) {
                                    seatNumberGuest = seatNumberGuest + blank;
                                } else {
                                    seatNumberGuest = zero + seatNumberGuest;
                                }
                            } while (seatNumberGuest.length() != 4);
                        }
                    }
                    boolean ifEqualsDateOfFlight = dateOfFlight.equals(dateOffFlightGuest);
                    boolean ifEqualsOperatingCarrierDesignator = operatingCarrierDesignator.equals(operatingCarrierDesignatorGuest);
                    boolean ifEqualsFlightNumber = flightNumber.equals(flightNumberGuest);
                    boolean ifEqualsSeatNumber = seatNumber.equals(seatNumberGuest);
                    if (ifEqualsDateOfFlight && ifEqualsOperatingCarrierDesignator && ifEqualsFlightNumber && ifEqualsSeatNumber) {
                        return true;
                    }
                }
            } else {
                List<DjpGuestData> djpGuestDataListo1 = new ArrayList<>();
                if (Objects.nonNull(boardingPassRequest)) {
                    boardingPassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataListo1.add(djpGuestData);
                    });
                } else if(Objects.nonNull(accessBoardingPassRequest)){
                    accessBoardingPassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataListo1.add(djpGuestData);
                    });
                }else {
                    djpMobilePassRequest.getGuests().stream().forEach(djpGuestData -> {
                        djpGuestDataListo1.add(djpGuestData);
                    });
                }
                if (!djpGuestDataListo1.isEmpty() && djpGuestDataListo1.size() > 1) {
                    for (int i = 0; i < djpGuestDataListo1.size() - 1; i++) {
                        DjpGuestData djpGuestDatao1 = djpGuestDataListo1.get(i);
                        boolean couldNotReadBoardingPassForGuesto1 = Objects.nonNull(djpGuestDatao1.getCouldNotReadBoardingPass()) && djpGuestDatao1.getCouldNotReadBoardingPass();
                        if (!couldNotReadBoardingPassForGuesto1) {
                            dateOffFlightGuesto1 = DateUtil.convertJulianDateToDate(djpGuestDatao1.getPnrCode().substring(44, 47));
                            operatingCarrierDesignatorGuesto1 = djpGuestDatao1.getPnrCode().substring(36, 39);
                            operatingCarrierDesignatorGuesto1 = operatingCarrierDesignatorGuesto1.contains(" ") ? operatingCarrierDesignatorGuesto1.replace(" ", "") : operatingCarrierDesignatorGuesto1;
                            flightNumberGuesto1 = djpGuestDatao1.getPnrCode().substring(39, 44);
                            flightNumberGuesto1 = flightNumberGuesto1.contains(" ") ? flightNumberGuesto1.replace(" ", "") : flightNumberGuesto1;
                            seatNumberGuesto1 = djpGuestDatao1.getPnrCode().substring(48, 52);
                        } else {
                            dateOffFlightGuesto1 = DateUtil.convertJulianDateToDate(djpGuestDatao1.getDate());
                            operatingCarrierDesignatorGuesto1 = djpGuestDatao1.getFlightNumber().toUpperCase().substring(0, 2);
                            flightNumberGuesto1 = djpGuestDatao1.getFlightNumber().substring(2, djpGuestDatao1.getFlightNumber().length());
                            flightNumberGuesto1 = flightNumberGuesto1.contains(" ") ? flightNumberGuesto1.replace(" ", "") : flightNumberGuesto1;
                            seatNumberGuesto1 = djpGuestDatao1.getSeatCode().toUpperCase();
                            if(seatNumberGuesto1.length() < 4) {
                                do {
                                    String zero = "0";
                                    String blank = " ";
                                    if (seatNumberGuesto1.startsWith("NS")) {
                                        seatNumberGuesto1 = seatNumberGuesto1 + blank;
                                    } else {
                                        seatNumberGuesto1 = zero + seatNumberGuesto1;
                                    }
                                } while (seatNumberGuesto1.length() != 4);
                            }
                        }
                        for (int y = i + 1; y <= djpGuestDataListo1.size() - 1; y++) {
                            DjpGuestData djpGuestDatao2 = djpGuestDataListo1.get(y);
                            boolean couldNotReadBoardingPassForGuesto2 = Objects.nonNull(djpGuestDatao2.getCouldNotReadBoardingPass()) && djpGuestDatao2.getCouldNotReadBoardingPass();
                            if (!couldNotReadBoardingPassForGuesto2) {
                                dateOffFlightGuesto2 = DateUtil.convertJulianDateToDate(djpGuestDatao2.getPnrCode().substring(44, 47));
                                operatingCarrierDesignatorGuesto2 = djpGuestDatao2.getPnrCode().substring(36, 39);
                                operatingCarrierDesignatorGuesto2 = operatingCarrierDesignatorGuesto2.contains(" ") ? operatingCarrierDesignatorGuesto2.replace(" ", "") : operatingCarrierDesignatorGuesto2;
                                flightNumberGuesto2 = djpGuestDatao2.getPnrCode().substring(39, 44);
                                flightNumberGuesto2 = flightNumberGuesto2.contains(" ") ? flightNumberGuesto2.replace(" ", "") : flightNumberGuesto2;
                                seatNumberGuesto2 = djpGuestDatao2.getPnrCode().substring(48, 52);
                            } else {
                                dateOffFlightGuesto2 = DateUtil.convertStartDate(djpGuestDatao2.getDate());
                                operatingCarrierDesignatorGuesto2 = djpGuestDatao2.getFlightNumber().toUpperCase().substring(0, 2);
                                flightNumberGuesto2 = djpGuestDatao2.getFlightNumber().substring(2, djpGuestDatao2.getFlightNumber().length());
                                flightNumberGuesto2 = flightNumberGuesto2.contains(" ") ? flightNumberGuesto2.replace(" ", "") : flightNumberGuesto2;
                                seatNumberGuesto2 = djpGuestDatao2.getSeatCode().toUpperCase();
                                if(seatNumberGuesto2.length() < 4) {
                                    do {
                                        String zero = "0";
                                        String blank = " ";
                                        if (seatNumberGuesto2.startsWith("NS")) {
                                            seatNumberGuesto2 = seatNumberGuesto2 + blank;
                                        } else {
                                            seatNumberGuesto2 = zero + seatNumberGuesto2;
                                        }
                                    } while (seatNumberGuesto2.length() != 4);
                                }
                            }
                            boolean ifEqualsDateOfFlight = dateOffFlightGuesto1.equals(dateOffFlightGuesto2);
                            boolean ifEqualsOperatingCarrierDesignator = operatingCarrierDesignatorGuesto1.equals(operatingCarrierDesignatorGuesto2);
                            boolean ifEqualsFlightNumber = flightNumberGuesto1.equals(flightNumberGuesto2);
                            boolean ifEqualsSeatNumber = seatNumberGuesto1.equals(seatNumberGuesto2);
                            if (ifEqualsDateOfFlight && ifEqualsOperatingCarrierDesignator && ifEqualsFlightNumber && ifEqualsSeatNumber) {
                                return true;
                            }

                        }
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            LOG.error("has error getBoardingPass:", e);
        }
        return false;
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByToken(String token) {
        return djpBoardingDao.getDjpBoardingPassByToken(token);
    }

    @Override
    public List<DjpBoardingPassModel> getDjpBoardingPassesByTokenAndSapOrderID(String qrKey, String sapOrderID) {
        return djpBoardingDao.getDjpBoardingPassesByTokenAndSapOrderID(qrKey,sapOrderID);
    }

    @Override
    public List<DjpBoardingPassModel> getDjpBoardingPassByTokenAndUsage(String qrKey, QrType whereUsed, Date sub48HoursCurrentDate) {
        return djpBoardingDao.getDjpBoardingPassByTokenAndUsage(qrKey,whereUsed,sub48HoursCurrentDate);
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByTokenAndSapOrderID(String token, String sapOrderID) {
        return djpBoardingDao.getDjpBoardingPassByTokenAndSapOrderID(token,sapOrderID);
    }
    @Override
    public DjpBoardingPassModel getCouldReadDjpBoardingPass(String pnrCode, String whereUsed) {
        Date flightDate = null , sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        boolean checkIfWhereUsed = false;
        boolean checkIfNewSale = false;
        DjpBoardingPassModel djpBoardingPass = null;
        if(StringUtils.isNotBlank(pnrCode)){
            operatingCarrierDesignator = pnrCode.substring(36,39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39,44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44,47));
            seatNumber = pnrCode.substring(48,52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            QrType boardingUsageEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
            djpBoardingPass = getDjpBoardingPassModel(flightDate, sub48HoursCurrentDate, flightNumber, seatNumber, operatingCarrierDesignator, boardingUsageEnum);
            if (Objects.nonNull(djpBoardingPass)) {

                checkIfNewSale = Objects.nonNull(djpBoardingPass.getNewSale()) ? djpBoardingPass.getNewSale() : true;

                if (checkIfNewSale) {
                    checkIfWhereUsed = boardingUsageEnum.equals(djpBoardingPass.getBoardingUsage());
                    if (checkIfWhereUsed) {
                        return djpBoardingPass;
                    } else {
                        return null;
                    }
                } else {

                    QrForBankModel qrForBankModel = null;
                    QrForVoucherModel qrForVoucherModel = null;
                    QrForFioriModel qrForFioriModel = null;
                    QrForMobileModel qrForMobileModel = null;
                    QrForMeetModel qrForMeetModel = null;
                    qrForMobileModel = djpQRService.validQrCodeForMobile(djpBoardingPass.getToken().getCode());
                    if (Objects.isNull(qrForMobileModel)) {
                        qrForVoucherModel = djpQRService.validQrCodeForDjp(djpBoardingPass.getToken().getCode());
                        if (Objects.isNull(qrForVoucherModel)) {
                            qrForFioriModel = djpQRService.validQrCodeForFiori(djpBoardingPass.getToken().getCode());
                            if (Objects.isNull(qrForFioriModel)) {
                                qrForBankModel = djpQRService.validQrCodeForBank(djpBoardingPass.getToken().getCode());
                                if (Objects.isNull(qrForBankModel)) {
                                    qrForMeetModel = djpQRService.getQRForMeetByTokenKey(djpBoardingPass.getToken().getCode());
                                    if (Objects.isNull(qrForMeetModel)) {
                                        return djpBoardingPass;
                                    } else {
                                        if (Objects.isNull(qrForMeetModel.getWhereUsed())) {
                                            return djpBoardingPass;
                                        }
                                        checkIfWhereUsed = qrForMeetModel.getWhereUsed().equals(boardingUsageEnum);
                                        if (checkIfWhereUsed) {
                                            return djpBoardingPass;
                                        } else {
                                            return null;
                                        }
                                    }
                                } else {
                                    if (Objects.isNull(qrForBankModel.getWhereUsed())) {
                                        return djpBoardingPass;
                                    }
                                    checkIfWhereUsed = qrForBankModel.getWhereUsed().equals(boardingUsageEnum);
                                    if (checkIfWhereUsed) {
                                        return djpBoardingPass;
                                    } else {
                                        return null;
                                    }
                                }
                            } else {
                                if (Objects.isNull(qrForFioriModel.getWhereUsed())) {
                                    return djpBoardingPass;
                                }
                                checkIfWhereUsed = qrForFioriModel.getWhereUsed().equals(boardingUsageEnum);
                                if (checkIfWhereUsed) {
                                    return djpBoardingPass;
                                } else {
                                    return null;
                                }
                            }
                        } else {
                            if (Objects.isNull(qrForVoucherModel.getWhereUsed())) {
                                return djpBoardingPass;
                            }
                            checkIfWhereUsed = qrForVoucherModel.getWhereUsed().contains(boardingUsageEnum);
                            if (checkIfWhereUsed) {
                                return djpBoardingPass;
                            } else {
                                return null;
                            }
                        }
                    } else {
                        if (Objects.isNull(qrForMobileModel.getWhereUsed())) {
                            return djpBoardingPass;
                        }
                        checkIfWhereUsed = qrForMobileModel.getWhereUsed().equals(boardingUsageEnum);
                        if (checkIfWhereUsed) {
                            return djpBoardingPass;
                        } else {
                            return null;
                        }
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public DjpMobilePassModel getCouldReadDjpMobilePass(String pnrCode,String whereUsed, String deviceId) {
        Date flightDate = null , sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        boolean checkIfWhereUsed = false;
        boolean checkIfNewSale = false;
        DjpMobilePassModel djpMobilePass = null;
        if(StringUtils.isNotBlank(pnrCode)){
            operatingCarrierDesignator = pnrCode.substring(36,39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39,44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44,47));
            seatNumber = pnrCode.substring(48,52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            QrType boardingUsageEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
            djpMobilePass = getDjpMobilePassModel(flightDate, sub48HoursCurrentDate, flightNumber, seatNumber, operatingCarrierDesignator, boardingUsageEnum);
            if (Objects.nonNull(djpMobilePass)) {
                QrForBankModel qrForBankModel = null;
                QrForVoucherModel qrForVoucherModel = null;
                QrForFioriModel qrForFioriModel = null;
                QrForMobileModel qrForMobileModel = null;
                QrForMeetModel qrForMeetModel = null;
                qrForMobileModel = djpQRService.validQrCodeForMobile(djpMobilePass.getToken().getCode());
                if (Objects.isNull(qrForMobileModel)) {
                    qrForVoucherModel = djpQRService.validQrCodeForDjp(djpMobilePass.getToken().getCode());
                    if (Objects.isNull(qrForVoucherModel)) {
                        qrForFioriModel = djpQRService.validQrCodeForFiori(djpMobilePass.getToken().getCode());
                        if (Objects.isNull(qrForFioriModel)) {
                            qrForBankModel = djpQRService.validQrCodeForBank(djpMobilePass.getToken().getCode());
                            if (Objects.isNull(qrForBankModel)) {
                                qrForMeetModel = djpQRService.getQRForMeetByTokenKey(djpMobilePass.getToken().getCode());
                                if (Objects.isNull(qrForMeetModel)) {
                                    return djpMobilePass;
                                } else {
                                    if (Objects.isNull(qrForMeetModel.getWhereUsed())) {
                                        return djpMobilePass;
                                    }
                                    checkIfWhereUsed = qrForMeetModel.getWhereUsed().equals(boardingUsageEnum);
                                    if (checkIfWhereUsed) {
                                        return djpMobilePass;
                                    } else {
                                        return null;
                                    }
                                }
                            } else {
                                if (Objects.isNull(qrForBankModel.getWhereUsed())) {
                                    return djpMobilePass;
                                }
                                checkIfWhereUsed = qrForBankModel.getWhereUsed().equals(boardingUsageEnum);
                                if (checkIfWhereUsed) {
                                    return djpMobilePass;
                                } else {
                                    return null;
                                }
                            }
                        } else {
                            if (Objects.isNull(qrForFioriModel.getWhereUsed())) {
                                return djpMobilePass;
                            }
                            checkIfWhereUsed = qrForFioriModel.getWhereUsed().equals(boardingUsageEnum);
                            if (checkIfWhereUsed) {
                                return djpMobilePass;
                            } else {
                                return null;
                            }
                        }
                    } else {
                        if (Objects.isNull(qrForVoucherModel.getWhereUsed())) {
                            return djpMobilePass;
                        }
                        checkIfWhereUsed = qrForVoucherModel.getWhereUsed().contains(boardingUsageEnum);
                        if (checkIfWhereUsed) {
                            return djpMobilePass;
                        } else {
                            return null;
                        }
                    }
                } else {
                    if (Objects.nonNull(qrForMobileModel.getWhereUsed())) {
                        checkIfWhereUsed = qrForMobileModel.getWhereUsed().equals(boardingUsageEnum);
                        if (checkIfWhereUsed){
                            return djpMobilePass;
                        } else {
                            return null;
                        }
                    } else {
                        djpMobilePass.setBoardingUsage(QrType.valueOf(whereUsed));
                        djpMobilePass.setUsageMethod(deviceId.equals("T") ? UsageMethod.TOURNIQUET : UsageMethod.DESK);
                        djpMobilePass.setNewSale(Boolean.FALSE);
                        qrForMobileModel.setUsageDate(new Date());
                        qrForMobileModel.setWhereUsed(QrType.valueOf(whereUsed));
                        modelService.saveOrUpdate(djpMobilePass);
                        modelService.saveOrUpdate(qrForMobileModel);
                        return djpMobilePass;
                    }
                }
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public DjpAccessBoardingModel getCouldReadDjpAccessBoarding(String pnrCode,String whereUsed) {
        Date flightDate = null ,sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        boolean checkIfWhereUsed = false;
        if(StringUtils.isNotBlank(pnrCode)){
            operatingCarrierDesignator = pnrCode.substring(36,39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39,44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44,47));
            seatNumber = pnrCode.substring(48,52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            QrType boardingUsageEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
            DjpAccessBoardingModel djpAccessBoarding = djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, boardingUsageEnum);
            if(Objects.nonNull(djpAccessBoarding)){
                checkIfWhereUsed = djpAccessBoarding.getBoardingUsages().contains(boardingUsageEnum);
                if(checkIfWhereUsed){
                    return djpAccessBoarding;
                }else {
                    return null;
                }
            }else {
                return null;
            }
        }else {
            return null;
        }
    }

    @Override
    public void checkIfHaveDjpPassAndGenerateBoardingPass(QrForFioriRequest qrForFioriRequest, UserWsDto userWs, DjpPassLocationsModel djpPassLocation, UsageMethod usageMethod) {
        Date date = new Date();
        FioriStatusResponse status = new FioriStatusResponse();
        DjpBoardingPassModel djpBoardingPassModel = null;
        boolean couldNotReadBoardingPass = false;
        String pnrCode = "";
        String boardingType = StringUtils.isNotBlank(qrForFioriRequest.getBoardingType()) ? qrForFioriRequest.getBoardingType() : DjpCommerceservicesConstants.LOUNGE_INTERNATIONAL_POINT_CODE;

        couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
        pnrCode = !couldNotReadBoardingPass ? qrForFioriRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;


        Date flightDate = null, sub48HoursCurrentDate = null;
        String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
        String currentPassangerName = null;
        if (StringUtils.isBlank(pnrCode)) {
            currentPassangerName = qrForFioriRequest.getPassengerName().toUpperCase();
            flightDate = DateUtil.convertStartDate(qrForFioriRequest.getDate());
            operatingCarrierDesignator = qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2);
            flightNumber = qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length());
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            seatNumber = qrForFioriRequest.getSeatCode().toUpperCase();
            if(seatNumber.length() < 4) {
                do {
                    String zero = "0";
                    String blank = " ";
                    if (seatNumber.startsWith("NS")) {
                        seatNumber = seatNumber + blank;
                    } else {
                        seatNumber = zero + seatNumber;
                    }
                } while (seatNumber.length() != 4);
            }
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
        }

        if (StringUtils.isNotBlank(pnrCode)) {
            currentPassangerName = pnrCode.substring(2, 22).toUpperCase();
            operatingCarrierDesignator = pnrCode.substring(36, 39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39, 44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
            seatNumber = pnrCode.substring(48, 52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
        }
        DjpMobilePassModel djpMobilePassModel = djpBoardingDao.getDjpMobilePass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate);
        djpBoardingPassModel = getDjpBoardingPassModel(flightDate, sub48HoursCurrentDate, flightNumber, seatNumber, operatingCarrierDesignator, null);
        if(Objects.nonNull(djpMobilePassModel) && Objects.isNull(djpBoardingPassModel)){
            boolean isOwner = djpMobilePassModel.getSuperBoarding().isEmpty();
            if (isOwner) {
                qrForFioriRequest.setIsGuest(!isOwner);
                cloneMobilePassWithGuest(qrForFioriRequest, status, boardingType, djpMobilePassModel,usageMethod,date);
            }else {
                qrForFioriRequest.setIsGuest(isOwner);
                DjpUniqueTokenModel guestToken = djpMobilePassModel.getToken();
                Optional<DjpBoardingPassModel> ownerOptional = djpMobilePassModel.getSuperBoarding().stream().filter(superPassModel -> Objects.nonNull(superPassModel.getToken()) && superPassModel.getToken().equals(guestToken)).findFirst();
                if(ownerOptional.isPresent()){
                    cloneMobilePassWithGuest(qrForFioriRequest, status, boardingType, (DjpMobilePassModel) ownerOptional.get(),usageMethod,date);
                }
            }
            boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
            if (!hasErrorOnResponse) {
                userWs.setQuickServiceUsage(Boolean.TRUE);
                if(qrForFioriRequest.getIsGuest()){
                    status.setMessage("Yolcu iGA Pass yolcusunun misafiridir, kaydı Istanbul Airport Uygulumasından yapılmıştır. Lütfen yanında iGA Passli yolcu olduğunu kontrol ediniz, sonrasında turnikeden direk geçebilir");
                }else {
                    status.setMessage("Yolcu iGA Pass yolcusudur ve boarding kaydı Istanbul Airport Uygulumasından yapılmıştır. Direk turnikeden geçiş yapabilir.");
                }
                status.setType("S");
                userWs.setResult(status);
                qrForFioriRequest.setHaveDjpPass(Boolean.TRUE);
                qrForFioriRequest.setCanUseService(Boolean.TRUE);
            }else {
                qrForFioriRequest.setHaveDjpPass(Boolean.FALSE);
            }
        }

        if (Objects.nonNull(djpBoardingPassModel)) {
            QrType whereUsage = QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()));
            checkIfBoardingPass(qrForFioriRequest, djpBoardingPassModel, null, null, null,currentPassangerName);
            if(Objects.nonNull(djpBoardingPassModel.getToken()) && Objects.nonNull(qrForFioriRequest.getCanUseService()) && Boolean.TRUE.equals(qrForFioriRequest.getCanUseService())){
                Boolean checkHaveDjpPass = djpCustomerService.checkHaveDjpPass(djpBoardingPassModel.getToken().getUserUid());
                DjpBoardingPassModel djpBoardingPassByUsage = getDjpBoardingPassModel(flightDate, sub48HoursCurrentDate, flightNumber, seatNumber, operatingCarrierDesignator, whereUsage);
                if(checkHaveDjpPass && Objects.isNull(djpBoardingPassByUsage)){
                    try {
                        boolean isOwner = djpBoardingPassModel.getSuperBoarding().isEmpty();
                        if (isOwner) {
                            qrForFioriRequest.setIsGuest(!isOwner);
                            cloneBpWithGuests(qrForFioriRequest, status, djpBoardingPassModel,djpPassLocation,usageMethod,null,sub48HoursCurrentDate,date);
                        }else {
                            qrForFioriRequest.setIsGuest(!isOwner);
                            DjpUniqueTokenModel guestToken = djpBoardingPassModel.getToken();
                            Optional<DjpBoardingPassModel> ownerOptional = djpBoardingPassModel.getSuperBoarding().stream().filter(superPassModel -> Objects.nonNull(superPassModel.getToken()) && superPassModel.getToken().equals(guestToken)).findFirst();
                            if(ownerOptional.isPresent()){
                                cloneBpWithGuests(qrForFioriRequest,status,ownerOptional.get(),djpPassLocation,usageMethod,djpBoardingPassModel,sub48HoursCurrentDate,date);
                            }
                        }
                        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
                        if (!hasErrorOnResponse) {
                            userWs.setQuickServiceUsage(checkHaveDjpPass);
                            if(qrForFioriRequest.getIsGuest()){
                                status.setMessage("Yolcu iGA Pass yolcusunun misafiridir, kaydı Fast Track noktasında alınmıştır. Lütfen yanında iGA Passli yolcu olduğunu kontrol ediniz, sonrasında turnikeden direk geçebilir");
                            }else {
                                status.setMessage("Yolcu iGA Pass yolcusudur ve boarding kaydı Fast Track noktasında alınmıştır. Direk turnikeden geçiş yapabilir.");
                            }
                            status.setType("S");
                            userWs.setResult(status);
                            qrForFioriRequest.setHaveDjpPass(checkHaveDjpPass);
                            qrForFioriRequest.setCanUseService(Boolean.TRUE);
                        }else {
                            qrForFioriRequest.setHaveDjpPass(Boolean.FALSE);
                        }
                    } catch (Exception e) {
                        LOG.error("generateQrForFiori has error:", e);
                    }
                }
            }
        }
    }

    @Override
    public void saveAccessBoardingPassForQuickServiceUsage(QrForFioriRequest qrForFioriRequest, String contractId, String boardingClass, DjpPassLocationsModel djpPassLocation, UsageMethod usageMethod) {
        try {
            DjpAccessBoardingModel accBoardingPass = new DjpAccessBoardingModel();
            boolean couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
            if (couldNotReadBoardingPass) {
                Date dateOfFlight = DateUtil.convertStartDate(qrForFioriRequest.getDate());
                String operatingCarrierDesignator = qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2);
                String flightNumber = qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                String seatNumber = qrForFioriRequest.getSeatCode().toUpperCase();
                if(seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
                String fromCityAirportCode = qrForFioriRequest.getFromAirportIATA().toUpperCase();
                String toCityAirportCode = qrForFioriRequest.getToAirportIATA().toUpperCase();
                String compartmentCode = qrForFioriRequest.getClassCode().toUpperCase();
                String passengerName = qrForFioriRequest.getPassengerName();
                accBoardingPass.setDateOfFlight(dateOfFlight);
                accBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                accBoardingPass.setFlightNumber(flightNumber);
                accBoardingPass.setSeatNumber(seatNumber);
                accBoardingPass.setFromCityAirportCode(fromCityAirportCode);
                accBoardingPass.setToCityAirportCode(toCityAirportCode);
                accBoardingPass.setCompartmentCode(compartmentCode);
                accBoardingPass.setPassengerName(passengerName);
                accBoardingPass.setPnrCode(passengerName + "/" + compartmentCode + "/" + fromCityAirportCode + "/" + toCityAirportCode + "/" + DateUtil.toDatePattern(dateOfFlight,"dd.MM.yyyy") +"/"+operatingCarrierDesignator+ "/" + flightNumber + "/" + seatNumber);
                accBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPass);
            } else {
                String pnrCode = qrForFioriRequest.getPnrCode();
                accBoardingPass.setPnrCode(pnrCode);
                accBoardingPass.setFormatCode(pnrCode.substring(0, 1));
                accBoardingPass.setNumberOfLegsEncoded(pnrCode.substring(1, 2));
                accBoardingPass.setPassengerName(pnrCode.substring(2, 22));
                accBoardingPass.setElectronicTicketIndicator(pnrCode.substring(22, 23));
                accBoardingPass.setOperatingCarrierPnrCode(pnrCode.substring(23, 30));
                accBoardingPass.setFromCityAirportCode(pnrCode.substring(30, 33));
                accBoardingPass.setToCityAirportCode(pnrCode.substring(33, 36));
                String operatingCarrierDesignator = pnrCode.substring(36, 39);
                operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
                accBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                String flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                accBoardingPass.setFlightNumber(flightNumber);
                accBoardingPass.setDateOfFlight(DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47)));
                accBoardingPass.setCompartmentCode(pnrCode.substring(47, 48));
                accBoardingPass.setSeatNumber(pnrCode.substring(48, 52));
                accBoardingPass.setCheckInSequenceNumber(pnrCode.substring(52, 57));
                accBoardingPass.setPassengerStatus(pnrCode.substring(57, 58));
                accBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPass);
            }
            Set<QrType> whereUsedBoarding = new HashSet<>();
            QrType whereUsed = QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()));
            whereUsedBoarding.add(whereUsed);
            accBoardingPass.setBoardingUsages(whereUsedBoarding);
            accBoardingPass.setBoardingClass(boardingClass);
            accBoardingPass.setContractID(contractId);
            accBoardingPass.setNewSale(Objects.nonNull(qrForFioriRequest.getNewSale()) ? qrForFioriRequest.getNewSale() : Boolean.FALSE);
            accBoardingPass.setPersonalID(StringUtils.isNotBlank(qrForFioriRequest.getPersonalID()) ? qrForFioriRequest.getPersonalID() : null);
            EmployeeModel employeeModel = null;
            try {
                employeeModel = (EmployeeModel) userService.getUserForUID(qrForFioriRequest.getPersonalID());
            } catch (Exception e){
                LOG.error("Cannot find user with uid : " + qrForFioriRequest.getPersonalID());
            }
            accBoardingPass.setPersonalName(employeeModel != null ? employeeModel.getName() : null);
            accBoardingPass.setWhichLocation(djpPassLocation);
            accBoardingPass.setMiddleWareDetail(getStatusForMiddleWare(accBoardingPass.getMiddleWareDetail(),accBoardingPass.getCouldNotReadBoardingPass(),accBoardingPass.getFromCityAirportCode(),accBoardingPass.getDateOfFlight(),accBoardingPass.getOperatingCarrierDesignator()+accBoardingPass.getFlightNumber(),whereUsed));
            accBoardingPass.setUsageMethod(usageMethod);
            modelService.saveOrUpdate(accBoardingPass);
            boolean isLoungeDomestic = whereUsedBoarding.stream().anyMatch(x -> x.equals(QrType.LOUNGE_DOMESTIC));
            if (StringUtils.isNotBlank(qrForFioriRequest.getBoardingType())){
                if (isLoungeDomestic){
                    if (StringUtils.isNotBlank(contractId)){
                        ContractedCompaniesModel contractedCompaniesModel = contractedCompanyService.getContractedCompany(contractId);
                        if (Objects.nonNull(contractedCompaniesModel)){
                            generateServicePlaningData(contractedCompaniesModel,null,accBoardingPass,0);
                        }
                    }
                }
            }
            applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),null,accBoardingPass,whereUsed));
//            TODO: this line needs to be un commented when adding the business process infrastructure
            //            djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrForFioriRequest.getBoardingType(), null, accBoardingPass, null,null,null);
        } catch (Exception ex) {
            LOG.error("has an error saveAccessBoardingPassForQuickServiceUsage:", ex);
        }
    }

    @Override
    public ContractedCompaniesModel getContractedCompaniesViaPass(String contractedId) {
        return djpBoardingDao.getContractedCompaniesViaPass(contractedId);
    }

    @Override
    public List<Object> getAllDjpBoardingSellList(Date startDate, Date endDate, Boolean isNewSale, String whereUsed, DjpPassLocationsModel djpPassLocation) {
        return djpBoardingDao.getAllDjpBoardingSellList(startDate,endDate,isNewSale,QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)),djpPassLocation);

    }

    private boolean checkIfBoardingPass(QrForFioriRequest qrForFioriRequest,DjpBoardingPassModel djpBoardingPass,DjpAccessBoardingModel djpAccessBoardingModel,BoardingPassRequest boardingPassRequest,AccessBoardingPassRequest accessBoardingPassRequest,String currentPassengerName) {
        if(StringUtils.isNotBlank(currentPassengerName)){
            currentPassengerName = currentPassengerName.contains(" ") ? currentPassengerName.replace(" ","") : currentPassengerName;
        }
        String whereUsedBoardingPass = Objects.isNull(qrForFioriRequest) ? Objects.isNull(boardingPassRequest) ? accessBoardingPassRequest.getBoardingType() : boardingPassRequest.getWhereUsed() : qrForFioriRequest.getBoardingType();
        String passengersNameUsingTheService = null;
        boolean checkIfWhereUsed = false;
        boolean checkIfNewSale = false;
        if (Objects.nonNull(djpBoardingPass)) {
            checkIfNewSale = Objects.nonNull(djpBoardingPass.getNewSale()) ? djpBoardingPass.getNewSale() : true;
            passengersNameUsingTheService = djpBoardingPass.getPassengerName();
            if(StringUtils.isNotBlank(passengersNameUsingTheService)){
                passengersNameUsingTheService = passengersNameUsingTheService.contains(" ") ? passengersNameUsingTheService.replace(" ","") : passengersNameUsingTheService;
            }
            if (checkIfNewSale) {
                checkIfWhereUsed = QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)).equals(djpBoardingPass.getBoardingUsage());
                if (checkIfWhereUsed) {
                    if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                        if (!currentPassengerName.contains("/")) {
                            if(passengersNameUsingTheService.contains("/")) {
                                String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                            }
                        }
                        checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                    }
                }
                fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, null, null);
                return checkIfWhereUsed;
            } else {
                QrForBankModel qrForBankModel = null;
                QrForVoucherModel qrForVoucherModel = null;
                QrForFioriModel qrForFioriModel = null;
                QrForMobileModel qrForMobileModel = null;
                QrForMeetModel qrForMeetModel = null;
                qrForMobileModel = djpQRService.validQrCodeForMobile(djpBoardingPass.getToken().getCode());
                if (Objects.isNull(qrForMobileModel)) {
                    qrForVoucherModel = djpQRService.validQrCodeForDjp(djpBoardingPass.getToken().getCode());
                    if (Objects.isNull(qrForVoucherModel)) {
                        qrForFioriModel = djpQRService.validQrCodeForFiori(djpBoardingPass.getToken().getCode());
                        if (Objects.isNull(qrForFioriModel)) {
                            qrForBankModel = djpQRService.validQrCodeForBank(djpBoardingPass.getToken().getCode());
                            if (Objects.isNull(qrForBankModel)) {
                                qrForMeetModel = djpQRService.getQRForMeetByTokenKey(djpBoardingPass.getToken().getCode());
                                if (Objects.isNull(qrForMeetModel)) {
                                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, null, null);
                                    return false;
                                } else {
                                    if (Objects.isNull(qrForMeetModel.getWhereUsed())) {
                                        fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                                        return true;
                                    }
                                    checkIfWhereUsed = qrForMeetModel.getWhereUsed().equals(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                                    if (checkIfWhereUsed) {
                                        if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                                            if (!currentPassengerName.contains("/")) {
                                                if(passengersNameUsingTheService.contains("/")) {
                                                    String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                                    passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                                                }
                                            }
                                            checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                                        }
                                    }
                                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                                    return checkIfWhereUsed;
                                }
                            } else {
                                if (Objects.isNull(qrForBankModel.getWhereUsed())) {
                                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                                    return true;
                                }
                                checkIfWhereUsed = qrForBankModel.getWhereUsed().equals(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                                if (checkIfWhereUsed) {
                                    if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                                        if (!currentPassengerName.contains("/")) {
                                            if(passengersNameUsingTheService.contains("/")) {
                                                String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                                passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                                            }
                                        }
                                        checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                                    }
                                }
                                fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                                return checkIfWhereUsed;
                            }
                        } else {
                            if (Objects.isNull(qrForFioriModel.getWhereUsed())) {
                                fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                                return true;
                            }
                            checkIfWhereUsed = qrForFioriModel.getWhereUsed().equals(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                            if (checkIfWhereUsed) {
                                if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                                    if (!currentPassengerName.contains("/")) {
                                        if(passengersNameUsingTheService.contains("/")) {
                                            String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                            passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                                        }
                                    }
                                    checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                                }
                            }
                            fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                            //qrForFioriRequest.setCanUseService(Boolean.TRUE);
                            return checkIfWhereUsed;
                        }
                    } else {
                        if (Objects.isNull(qrForVoucherModel.getWhereUsed())) {
                            fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                            return true;
                        }
                        checkIfWhereUsed = qrForVoucherModel.getWhereUsed().contains(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                        if (checkIfWhereUsed) {
                            if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                                if (!currentPassengerName.contains("/")) {
                                    if(passengersNameUsingTheService.contains("/")) {
                                        String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                        passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                                    }
                                }
                                checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                            }
                        }
                        fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                        return checkIfWhereUsed;
                    }
                } else {
                    if (Objects.isNull(qrForMobileModel.getWhereUsed())) {
                        fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                        return true;
                    }
                    checkIfWhereUsed = qrForMobileModel.getWhereUsed().equals(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                    if (checkIfWhereUsed) {
                        if (StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                            if (!currentPassengerName.contains("/")) {
                                if(passengersNameUsingTheService.contains("/")) {
                                    String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                    passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                                }
                            }
                            checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                        }
                    }
                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed, djpBoardingPass, null);
                    return checkIfWhereUsed;
                }
            }
        }else {
            if(Objects.nonNull(djpAccessBoardingModel)) {
                passengersNameUsingTheService = djpAccessBoardingModel.getPassengerName();
                if(StringUtils.isNotBlank(passengersNameUsingTheService)){
                    passengersNameUsingTheService = passengersNameUsingTheService.contains(" ") ? passengersNameUsingTheService.replace(" ","") : passengersNameUsingTheService;
                }
                boolean usedBoardingPass = djpAccessBoardingModel.getBoardingUsages().contains(QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
                if (usedBoardingPass) {
                    checkIfWhereUsed = true;
                    if(StringUtils.isNotBlank(passengersNameUsingTheService) && StringUtils.isNotBlank(currentPassengerName)) {
                        if(!currentPassengerName.contains("/")){
                            if(passengersNameUsingTheService.contains("/")) {
                                String[] splitPassengerName = passengersNameUsingTheService.split("/");
                                passengersNameUsingTheService = splitPassengerName[1] + splitPassengerName[0];
                            }
                        }
                        checkIfWhereUsed = passengersNameUsingTheService.equals(currentPassengerName);
                    }
                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, !checkIfWhereUsed,null,djpAccessBoardingModel);
                    return checkIfWhereUsed;
                } else {
                    fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, Boolean.TRUE,null,djpAccessBoardingModel);
                    return true;
                }
            }else {
                fillCanUserService(qrForFioriRequest, boardingPassRequest, accessBoardingPassRequest, Boolean.TRUE,null,null);
                return false;
            }
        }
    }
    private void fillCanUserService(QrForFioriRequest qrForFioriRequest, BoardingPassRequest boardingPassRequest, AccessBoardingPassRequest accessBoardingPassRequest, Boolean value,DjpBoardingPassModel djpBoardingPassModel,DjpAccessBoardingModel djpAccessBoardingModel) {
        if (Objects.nonNull(accessBoardingPassRequest)) {
            accessBoardingPassRequest.setCanUseService(value);
        } else if (Objects.nonNull(boardingPassRequest)) {
            boardingPassRequest.setCanUseService(value);
        } else {
            qrForFioriRequest.setCanUseService(value);
        }
    }
    private DjpBoardingPassModel getDjpBoardingPassModelWithoutDateLimit(Date flightDate, Date sub48HoursCurrentDate, String flightNumber, String seatNumber, String operatingCarrierDesignator, QrType whereUsed) {
        DjpBoardingPassModel djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByQrWithoutDateLimit(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
        if (Objects.isNull(djpBoardingPassModel)) {
            djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByFioriWithoutDateLimit(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            if (Objects.isNull(djpBoardingPassModel)) {
                djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassWithoutDate(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            }
        }
        return djpBoardingPassModel;
    }
    private boolean isCheckIfFastTrackTransfer(String whereUsed) {
        return qrTypeEnumHashMap.get(whereUsed).startsWith(DjpCommerceservicesConstants.FAST_TRACK_TRANSFER_STARTWITH);
    }
    private boolean isCheckIfFastTrackDepartureUsaged(String whereUsed) {
        return QrType.FAST_TRACK_DEPARTURE_DOMESTIC.getCode().equals(qrTypeEnumHashMap.get(whereUsed))
                || QrType.FAST_TRACK_DEPARTURE_INTERNATIONAL.getCode().equals(qrTypeEnumHashMap.get(whereUsed));
    }
    private boolean isCheckIfFastTrackArrival(String whereUsed) {
        return QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_C1.getCode().equals(qrTypeEnumHashMap.get(whereUsed))
                || QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_E1.getCode().equals(qrTypeEnumHashMap.get(whereUsed));
    }
    private Boolean postNurusCreateReservation(QrForCabinetModel qrForCabinetModel, String userToken){
        try{
            NurusServiceCreateReservationRequest reservationRequest = new NurusServiceCreateReservationRequest();
            reservationRequest.setRDate(getReservationDate(qrForCabinetModel.getCabinStartTime()));
            reservationRequest.setBeginTime(getReservationTime(qrForCabinetModel.getCabinStartTime()));
            reservationRequest.setEndTime(getReservationTime(qrForCabinetModel.getExpireDate()));
            reservationRequest.setSubject(qrForCabinetModel.getCabinInfo());
            reservationRequest.setUserToken(userToken);
            reservationRequest.setIsCheck_In(0);
            reservationRequest.setRoomID(qrForCabinetModel.getCabinID());
            List<NurusServiceCreateReservationRequestAttendeeData> list = new ArrayList<>();
            NurusServiceCreateReservationRequestAttendeeData attendeeData = new NurusServiceCreateReservationRequestAttendeeData();
            attendeeData.setName(qrForCabinetModel.getNameSurname());
            attendeeData.setPnrCode(qrForCabinetModel.getPnrCode());
            attendeeData.setQRCode(qrForCabinetModel.getQr().getCode());
            attendeeData.setEmail("");
            list.add(attendeeData);

            ObjectMapper mapper = getObjectMapper();
            String jsonData = mapper.writeValueAsString(reservationRequest);
            String response = getNurusServiceResponse(jsonData,nurusCreateReservationServiceUrl);
            Boolean result = false;
            if(StringUtils.isNotBlank(response)){
                NurusServiceCreateReservationResponse reservationResponse = mapper.readValue(response,NurusServiceCreateReservationResponse.class);
                if (reservationResponse != null){
                    if(reservationResponse.getResponse() != null){
                        result = reservationResponse.getResponse();
                    }
                }
            }
            return result;
        } catch (Exception e){
            LOG.error("Nurus create reservation exception :" + e);
            return false;
        }
    }
    private String getNurusServiceResponse(String json,String serviceEndpoint){
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost(serviceEndpoint);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", MediaType.APPLICATION_JSON_VALUE);
            httpPost.setHeader("Content-type", MediaType.APPLICATION_JSON_VALUE);
            CloseableHttpResponse response = client.execute(httpPost);
            return EntityUtils.toString(response.getEntity());
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
    private ObjectMapper getObjectMapper(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES,true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        return objectMapper;
    }
    private String getReservationDate(Date date){
        return new SimpleDateFormat("dd/MM/yyyy").format(date) + "T00:00:00";
    }

    private String getReservationTime(Date date){
        return new SimpleDateFormat("HH:mm").format(date);
    }
    private String dateToStringFormatForCabinetTablet(Date date){
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
    }
    private String getNurusLoginTokenKey(){
        try {
            NurusServiceLoginRequest loginRequest = new NurusServiceLoginRequest();
            loginRequest.setEmail(nurusLoginEmail);
            loginRequest.setPassword(nurusLoginPassword);
            ObjectMapper mapper = getObjectMapper();
            String jsonData = mapper.writeValueAsString(loginRequest);
            String response = getNurusServiceResponse(jsonData,nurusLoginServiceUrl);
            String tokenKey = "";
            if (StringUtils.isNotBlank(response)){
                NurusServiceLoginResponse result = mapper.readValue(response,NurusServiceLoginResponse.class);
                if(result !=null){
                    if(result.getResponse()){
                        if (!result.getUser().isEmpty()){
                            tokenKey = result.getUser().get(0).getTokenKey();
                        }
                    }
                }
            }
            return tokenKey;
        } catch (Exception e){
            LOG.error("Nurus login exception :" + e);
            return null;
        }
    }
    private void cabinInfoSendToTable(QrForCabinetModel cabinetModel, String tokenKey){
        try{
            DjpCabinetTabletRequest cabinetTabletRequest = new DjpCabinetTabletRequest();
            cabinetTabletRequest.setPassengerName(cabinetModel.getNameSurname());
            cabinetTabletRequest.setCabinetId(cabinetModel.getMacAddress());
            cabinetTabletRequest.setStartDate(dateToStringFormatForCabinetTablet(cabinetModel.getCabinStartTime()));
            cabinetTabletRequest.setEndDate(dateToStringFormatForCabinetTablet(cabinetModel.getExpireDate()));

            restTemplate.postForEntity(businessPodTabletServiceUrl,cabinetTabletRequest,null);
        } catch (Exception e){
            LOG.error("Not started cabin tablet : " + tokenKey);
        }
    }
    private DjpBoardingPassModel getBoardingPass(BoardingPassRequest boardingPassRequest,UpdateBoardingPassRequest updateBoardingPassRequest, DjpGuestData djpGuestData, String whereUsedBoardingPass) {
        try {
            DjpBoardingPassModel djpBoardingPass = null;
            String pnrCode = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? boardingPassRequest.getPnrCode().toUpperCase() : updateBoardingPassRequest.getPnrCode().toUpperCase() : djpGuestData.getPnrCode().toUpperCase();
            boolean couldNotReadBoardingPass = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? Boolean.TRUE.equals(boardingPassRequest.getCouldNotReadBoardingPass()) :  Boolean.TRUE.equals(updateBoardingPassRequest.getCouldNotReadBoardingPass()) : Boolean.TRUE.equals(djpGuestData.getCouldNotReadBoardingPass());
            Date flightDate = null, sub48HoursCurrentDate = null;
            String flightNumber = null, seatNumber = null, operatingCarrierDesignator = null;
            String currentPassangerName = null;
            if (StringUtils.isBlank(pnrCode) && Boolean.TRUE.equals(couldNotReadBoardingPass)) {
                currentPassangerName = Objects.isNull(djpGuestData) ? Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getPassengerName().toUpperCase() : updateBoardingPassRequest.getPassengerName().toUpperCase() : djpGuestData.getName().toUpperCase() + " " + djpGuestData.getSurname().toUpperCase();
                flightDate = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? DateUtil.convertStartDate(boardingPassRequest.getDate()) : DateUtil.convertStartDate(updateBoardingPassRequest.getDate()) : DateUtil.convertStartDate(djpGuestData.getDate());
                operatingCarrierDesignator = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? boardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                flightNumber = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? boardingPassRequest.getFlightNumber().toUpperCase().substring(2, boardingPassRequest.getFlightNumber().length()) : updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, updateBoardingPassRequest.getFlightNumber().length()) : djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                seatNumber = Objects.isNull(djpGuestData) ? Objects.isNull(updateBoardingPassRequest) ? boardingPassRequest.getSeatCode().toUpperCase() : updateBoardingPassRequest.getSeatCode().toUpperCase() : djpGuestData.getSeatCode().toUpperCase();
                if(seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            }
            if (StringUtils.isNotBlank(pnrCode) && Boolean.FALSE.equals(couldNotReadBoardingPass)) {
                currentPassangerName = pnrCode.substring(2,22).toUpperCase();
                operatingCarrierDesignator = pnrCode.substring(36, 39);
                operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
                flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
                seatNumber = pnrCode.substring(48, 52);
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
                djpBoardingPass = djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
            } else {
                djpBoardingPass = djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));

            }
            if (checkIfBoardingPass(null, djpBoardingPass, null, boardingPassRequest, updateBoardingPassRequest, currentPassangerName)) {
                return djpBoardingPass;
            } else {
                return null;
            }
        } catch (Exception e) {
            LOG.error("has error getBoardingPass:", e);
            return null;
        }
    }
    private void generateServicePlaningData(ContractedCompaniesModel companiesModel, DjpBoardingPassModel boardingPassModel, DjpAccessBoardingModel accessBoardingModel, Integer stayHour){
        //Company TRUE Individual FALSE
        boolean infoModelStat = Objects.nonNull(boardingPassModel) ? Objects.isNull(boardingPassModel.getBoardingPassUsingData()) : Objects.isNull(accessBoardingModel.getBoardingPassUsingData());
        if (infoModelStat){
            DjpBoardingPassUsingInfoModel infoModel = new DjpBoardingPassUsingInfoModel();
            infoModel.setCompanyOrIndividual(Objects.nonNull(companiesModel) ? Boolean.TRUE : Boolean.FALSE);
            infoModel.setCompany(Objects.nonNull(companiesModel) ? companiesModel : null);
            infoModel.setIndexUsingData(1);
            Integer stayHourValue = Objects.nonNull(companiesModel) ? companiesModel.getLengthOfStayHour() : stayHour;
            if (Objects.nonNull(boardingPassModel)){
                infoModel.setScheduledOutStartDate(boardingPassModel.getCreationtime());
                infoModel.setScheduledOutEndDate(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(boardingPassModel.getCreationtime(), stayHourValue)));
                infoModel.setIsTimeOk(new Date().after(infoModel.getScheduledOutEndDate()));
                infoModel.setBoardingPass(boardingPassModel);
                infoModel.setStatus(TourniquetBoardingStatus.USE);
                infoModel.setStayHourLimit(stayHourValue);
                modelService.saveOrUpdate(infoModel);
                boardingPassModel.setBoardingPassUsingData(infoModel);
                modelService.saveOrUpdate(boardingPassModel);
            } else {
                infoModel.setScheduledOutStartDate(accessBoardingModel.getCreationtime());
                infoModel.setScheduledOutEndDate(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(accessBoardingModel.getCreationtime(), stayHourValue)));
                infoModel.setIsTimeOk(new Date().after(infoModel.getScheduledOutEndDate()));
                infoModel.setAccBoardingPass(accessBoardingModel);
                infoModel.setStatus(TourniquetBoardingStatus.USE);
                modelService.saveOrUpdate(infoModel);
                accessBoardingModel.setBoardingPassUsingData(infoModel);
                infoModel.setStayHourLimit(stayHourValue);
                modelService.saveOrUpdate(accessBoardingModel);
            }
        }
    }

    private DjpBoardingPassModel getDjpBoardingPassModel(Date flightDate, Date sub48HoursCurrentDate, String flightNumber, String seatNumber, String operatingCarrierDesignator, QrType whereUsed) {
        DjpBoardingPassModel djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByQr(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
        if (Objects.isNull(djpBoardingPassModel)) {
            djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByFiori(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            if (Objects.isNull(djpBoardingPassModel)) {
                djpBoardingPassModel = djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            }
        }
        return djpBoardingPassModel;
    }

    private DjpMobilePassModel getDjpMobilePassModel(Date flightDate, Date sub48HoursCurrentDate, String flightNumber, String seatNumber, String operatingCarrierDesignator, QrType whereUsed) {
        DjpMobilePassModel djpMobilePassModel = djpBoardingDao.getDjpMobilePassByQr(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
        if (Objects.isNull(djpMobilePassModel)) {
            djpMobilePassModel = djpBoardingDao.getDjpMobilePassByFiori(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            if (Objects.isNull(djpMobilePassModel)) {
                djpMobilePassModel = djpBoardingDao.getDjpMobilePass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, whereUsed);
            }
        }
        return djpMobilePassModel;
    }

    private void cloneMobilePassWithGuest(QrForFioriRequest qrForFioriRequest, FioriStatusResponse status, String boardingType, DjpMobilePassModel djpMobilePassModel, UsageMethod usageMethod, Date creationTime) {
        QrForFioriModel qrForFioriModel = null;
        DjpBoardingPassModel cloneMobilePass = modelService.clone(djpMobilePassModel,DjpBoardingPassModel.class);
        CustomerModel customerByUid = djpCustomerService.findCustomerByUid(cloneMobilePass.getToken().getUserUid());
        qrForFioriModel = djpQRService.generateQrForFiori(cloneMobilePass.getToken().getUserUid(), customerByUid, "");
        String boardingTypeCode = qrForFioriRequest.getDevice().equals("T") ? boardingType : null;
        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,customerByUid.getUid(), qrForFioriRequest.getProductCategory(), null, null,  false,null, boardingTypeCode);
        Boolean statusType = Objects.isNull(status.getType()) ? true : status.getType().equals("E") ? false : true;
        if (!qrForFioriRequest.getDevice().equals("T") || (qrForFioriRequest.getDevice().equals("T") && statusType)) {
            Set<QrType> qrTypeEnumSet = new HashSet<>();
            QrType whereUsed = QrType.valueOf(qrTypeEnumHashMap.get(boardingType));
            qrTypeEnumSet.add(whereUsed);
            qrForFioriModel.setWhereIsValidQr(qrTypeEnumSet);
            qrForFioriModel.setUsageDate(new Date());
            qrForFioriModel.setWhereUsed(whereUsed);
            modelService.saveOrUpdate(qrForFioriModel);
            cloneMobilePass.setCreationtime(creationTime);
            cloneMobilePass.setToken(qrForFioriModel.getTokenKey());
            cloneMobilePass.setBoardingUsage(whereUsed);
            cloneMobilePass.setMiddleWareDetail(getStatusForMiddleWare(cloneMobilePass.getMiddleWareDetail(),cloneMobilePass.getCouldNotReadBoardingPass(),cloneMobilePass.getFromCityAirportCode(),cloneMobilePass.getDateOfFlight(),cloneMobilePass.getFlightNumber(),whereUsed));
            cloneMobilePass.setUsageMethod(usageMethod);
            modelService.saveOrUpdate(cloneMobilePass);
            applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),cloneMobilePass,null,whereUsed));
            if (!cloneMobilePass.getDjpBoardingPass().isEmpty()) {
                cloneMobilePass.getDjpBoardingPass().forEach(guest -> {
                    DjpBoardingPassModel cloneGuestBp = modelService.clone(guest,DjpBoardingPassModel.class);
                    cloneGuestBp.setCreationtime(creationTime);
                    cloneGuestBp.setToken(cloneMobilePass.getToken());
                    cloneGuestBp.setBoardingUsage(cloneMobilePass.getBoardingUsage());
                    cloneGuestBp.setMiddleWareDetail(getStatusForMiddleWare(cloneGuestBp.getMiddleWareDetail(),cloneGuestBp.getCouldNotReadBoardingPass(),cloneGuestBp.getFromCityAirportCode(),cloneGuestBp.getDateOfFlight(),cloneGuestBp.getFlightNumber(),whereUsed));
                    cloneGuestBp.setUsageMethod(usageMethod);
                    List<DjpBoardingPassModel> superBoarding = new ArrayList<DjpBoardingPassModel>();
                    superBoarding.add(cloneMobilePass);
                    cloneGuestBp.setSuperBoarding(superBoarding);
                    modelService.saveOrUpdate(cloneGuestBp);
                    applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),cloneGuestBp,null,whereUsed));
                });
            }
        }

    }

    private void cloneBpWithGuests(QrForFioriRequest qrForFioriRequest, FioriStatusResponse status, DjpBoardingPassModel djpBoardingPassModel,DjpPassLocationsModel djpPassLocation, UsageMethod usageMethod,DjpBoardingPassModel guestBoarding,Date sub48HoursCurrentDate,Date creationTime) {
        CustomerModel customerByUid = djpCustomerService.findCustomerByUid(djpBoardingPassModel.getToken().getUserUid());
        String boardingTypeCode = qrForFioriRequest.getDevice().equals("T") ? Objects.nonNull(qrForFioriRequest.getBoardingType()) ? qrForFioriRequest.getBoardingType() : null : null;
        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status, customerByUid.getUid(), qrForFioriRequest.getProductCategory(), null, null, false, null, boardingTypeCode);
        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
        if (!hasErrorOnResponse) {
            boolean isGuest = Objects.nonNull(guestBoarding);
            QrType whereUsed = QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()));
            if(Boolean.TRUE.equals(isGuest)){
                DjpBoardingPassModel djpOwnerBoardingPassByUsage = getDjpBoardingPassModel(djpBoardingPassModel.getDateOfFlight(), sub48HoursCurrentDate, djpBoardingPassModel.getFlightNumber(), djpBoardingPassModel.getSeatNumber(), djpBoardingPassModel.getOperatingCarrierDesignator(), whereUsed);
                if(Objects.isNull(djpOwnerBoardingPassByUsage)){
                    cloneBpForUsage(djpBoardingPassModel, djpPassLocation, usageMethod, customerByUid, whereUsed,creationTime);
                }else {
                    DjpBoardingPassModel cloneGuestBp = modelService.clone(guestBoarding);
                    fillAndSaveGuestBoardingPass(djpPassLocation, usageMethod, whereUsed, djpOwnerBoardingPassByUsage, cloneGuestBp,creationTime);
                }
            }else {
                cloneBpForUsage(djpBoardingPassModel, djpPassLocation, usageMethod, customerByUid, whereUsed,creationTime);
            }
        }

    }

    private void cloneBpForUsage(DjpBoardingPassModel djpBoardingPassModel, DjpPassLocationsModel djpPassLocation, UsageMethod usageMethodEnum, CustomerModel customerByUid, QrType whereUsed,Date creationTime) {
        QrForFioriModel qrForFioriModel = djpQRService.generateQrForFiori(customerByUid.getUid(), customerByUid, "");
        Set<QrType> qrTypeEnumSet = new HashSet<>();
        qrTypeEnumSet.add(whereUsed);
        qrForFioriModel.setWhereIsValidQr(qrTypeEnumSet);
        qrForFioriModel.setUsageDate(new Date());
        qrForFioriModel.setWhereUsed(whereUsed);
        qrForFioriModel.setWhichLocation(djpPassLocation);
        modelService.saveOrUpdate(qrForFioriModel);
        DjpBoardingPassModel cloneOwnerBp = modelService.clone(djpBoardingPassModel);
        cloneOwnerBp.setCreationtime(creationTime);
        cloneOwnerBp.setToken(qrForFioriModel.getTokenKey());
        cloneOwnerBp.setBoardingUsage(whereUsed);
        cloneOwnerBp.setWhichLocation(djpPassLocation);
        cloneOwnerBp.setMiddleWareDetail(getStatusForMiddleWare(cloneOwnerBp.getMiddleWareDetail(), cloneOwnerBp.getCouldNotReadBoardingPass(), cloneOwnerBp.getFromCityAirportCode(), cloneOwnerBp.getDateOfFlight(), cloneOwnerBp.getFlightNumber(), whereUsed));
        cloneOwnerBp.setUsageMethod(usageMethodEnum);
        modelService.saveOrUpdate(cloneOwnerBp);
        applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(), cloneOwnerBp, null, whereUsed));
        if (!cloneOwnerBp.getDjpBoardingPass().isEmpty()) {
            Collection<DjpBoardingPassModel> djpBoardingPass = cloneOwnerBp.getDjpBoardingPass();
            cloneOwnerBp.setDjpBoardingPass(null);
            modelService.saveOrUpdate(cloneOwnerBp);
            djpBoardingPass.forEach(guest -> {
                DjpBoardingPassModel cloneGuestBp = modelService.clone(guest);
                fillAndSaveGuestBoardingPass(djpPassLocation, usageMethodEnum, whereUsed, cloneOwnerBp, cloneGuestBp,creationTime);
            });
        }
    }

    private void fillAndSaveGuestBoardingPass(DjpPassLocationsModel djpPassLocation, UsageMethod usageMethodEnum, QrType whereUsed, DjpBoardingPassModel ownerBp, DjpBoardingPassModel cloneGuestBp,Date creationTime) {
        cloneGuestBp.setCreationtime(creationTime);
        cloneGuestBp.setToken(ownerBp.getToken());
        cloneGuestBp.setBoardingUsage(ownerBp.getBoardingUsage());
        cloneGuestBp.setWhichLocation(djpPassLocation);
        cloneGuestBp.setMiddleWareDetail(getStatusForMiddleWare(cloneGuestBp.getMiddleWareDetail(), cloneGuestBp.getCouldNotReadBoardingPass(), cloneGuestBp.getFromCityAirportCode(), cloneGuestBp.getDateOfFlight(), cloneGuestBp.getFlightNumber(), whereUsed));
        cloneGuestBp.setUsageMethod(usageMethodEnum);
        List<DjpBoardingPassModel> superBoarding = new ArrayList<DjpBoardingPassModel>();
        superBoarding.add(ownerBp);
        cloneGuestBp.setSuperBoarding(superBoarding);
        modelService.saveOrUpdate(cloneGuestBp);
        applicationEventPublisher.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(), cloneGuestBp, null, whereUsed));
    }

    private DjpBoardingPassSendToMwEvent initializeDjpBoardingPassSendToMwEvent(final DjpBoardingPassSendToMwEvent event, final DjpBoardingPassModel djpBoardingPassModel,final DjpAccessBoardingModel djpAccessBoardingModel,final QrType whereUsed) {
        event.setDjpAccessBoardingModel(djpAccessBoardingModel);
        event.setDjpBoardingPassModel(djpBoardingPassModel);
        event.setWhereUsed(whereUsed);
        return event;

    }
    @Override
    public FioriStatusResponse checkIfValidBoardingPasses(FioriStatusResponse response, UpdateBoardingPassRequest updateBoardingPassRequest) throws ParseException {
        DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(updateBoardingPassRequest.getLocationID());
        int diffBetweenToDate;
        String pnr = null;
        boolean checkIfStartWithBp =false;
        if (!updateBoardingPassRequest.getGuests().isEmpty()) {
            for (DjpGuestData djpGuestData : updateBoardingPassRequest.getGuests()) {
                if(!djpGuestData.getCouldNotReadBoardingPass() && StringUtils.isNotBlank(djpGuestData.getPnrCode())){
                    pnr = djpGuestData.getPnrCode().toUpperCase();
                    checkIfStartWithBp = !pnr.startsWith("M1") && !pnr.startsWith("M2") && !pnr.startsWith("M3");
                    if (checkIfStartWithBp) {
                        break;
                    }
                }
                if(checkIfStartWithBp) {
                    response.setMessage("Misafir Boarding Kart M1 / M2 / M3 Karakterleriyle Başlamalıdır!");
                    response.setType("E");
                    LOG.error("responseForFiori:" + response.getType());
                    return response;
                }
                boolean couldNotReadBpGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                if (couldNotReadBpGuest) {
                    if (djpGuestData.getFromAirportIATA().length() != 3) {
                        response.setMessage("Misafirin FromAirportIATA 3 Karakter Olmalı!");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    }
                    if (djpGuestData.getToAirportIATA().length() != 3) {
                        response.setMessage("Misafirin ToAirportIATA 3 Karakter Olmalı!");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    }
                }
                String messageForCheckIfBoardingForLocationIATA = StringUtils.EMPTY;
                if ("PROD".equals(fioriHostType) || "TEST".equals(serverHostType)) {
                    diffBetweenToDate = getDiffBetweenToDate(null, null, null, djpGuestData);
                    if (diffBetweenToDate > 36 || diffBetweenToDate < -36) {
                        response.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                        LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    } else {
                        messageForCheckIfBoardingForLocationIATA = checkIfBoardingForLocationIATA(null, null, updateBoardingPassRequest, djpGuestData,djpPassLocation);
                        if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                            response.setMessage(messageForCheckIfBoardingForLocationIATA);
                            response.setType("E");
                            LOG.error("responseForFiori:" + response.getType());
                            return response;
                        } else if (checkIfBoardingFromLocationIATA(null, null, updateBoardingPassRequest, djpGuestData,djpPassLocation)) {
                            response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Misafirin hizmet kullanım hakkı yoktur");
                            response.setType("E");
                            LOG.error("responseForFiori:" + response.getType());
                            return response;
                        }
                    }
                } else {
                    messageForCheckIfBoardingForLocationIATA = checkIfBoardingForLocationIATA(null, null, updateBoardingPassRequest, djpGuestData,djpPassLocation);
                    if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                        response.setMessage(messageForCheckIfBoardingForLocationIATA);
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    } else if (checkIfBoardingFromLocationIATA(null, null, updateBoardingPassRequest, djpGuestData,djpPassLocation)) {
                        response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Misafirin hizmet kullanım hakkı yoktur");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    }
                }
                if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
                    try {
                        String status = checkInternationalStatusForBp(null, null, null, djpGuestData, QrType.valueOf(qrTypeEnumHashMap.get(updateBoardingPassRequest.getBoardingType())));
                        if (StringUtils.isNotBlank(status)) {
                            if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(status)) {
                                response.setMessage("Misafire ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                                response.setType("E");
                                LOG.error("responseForFiori:" + response.getType());
                                return response;
                            } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(status)) {
                                response.setMessage("Misafire ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                                response.setType("E");
                                LOG.error("responseForFiori:" + response.getType());
                                return response;
                            }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(status)){
                                response.setMessage("Uçuş djp da planlanmamaktadır.");
                                response.setType("E");
                                LOG.error("responseForFiori:" + response.getType());
                                return response;
                            }
                        }
                    } catch (Exception ex) {
                        LOG.error("checkInternationalStatusForBp has an error:", ex);
                        response.setMessage("Uçuş djp da planlanmamaktadır.");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                        return response;
                    }
                }

                String pnrCode = null;
                Boolean hasGuestBp = null;
                if (StringUtils.isBlank(updateBoardingPassRequest.getContractID()) && StringUtils.isBlank(updateBoardingPassRequest.getTokenKey())) {
                    response.setMessage("ContractID Boş Olmamalı!");
                    response.setType("E");
                    return response;
                }
                if (StringUtils.isNotBlank(updateBoardingPassRequest.getPnrCode())) {
                    pnrCode = updateBoardingPassRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                    pnrCode = pnrCode.replaceAll("&", "/");
                    updateBoardingPassRequest.setPnrCode(pnrCode);
                }
                if (StringUtils.isNotBlank(djpGuestData.getPnrCode())) {
                    pnrCode = djpGuestData.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                    pnrCode = pnrCode.replaceAll("&", "/");
                    djpGuestData.setPnrCode(pnrCode);
                }
                if (ifHasSameBp(null, updateBoardingPassRequest,null,true)) {
                    response.setMessage("Yolcu ile misafirinin boarding kartı aynıdır! Lütfen misafirin kendi boarding kartını sisteme okutun.!");
                    response.setType("E");
                    return response;
                }
                if (ifHasSameBp(null, updateBoardingPassRequest,null,false)) {
                    response.setMessage("Misafirlerin boarding kartları aynıdır! Lütfen her bir misafirin kendi boarding kartını okutun!");
                    response.setType("E");
                    return response;
                }
                if(StringUtils.isBlank(updateBoardingPassRequest.getTokenKey())) {
                    if (!ifSameCompanyForGuest(updateBoardingPassRequest,djpPassLocation)) {
                        response.setMessage("Owner BoardingPass ile Guest BoardingPass Aynı Havayolu Olmalı!");
                        response.setType("E");
                        return response;
                    }
                }
                hasGuestBp = updateBoardingPassRequest.getGuests().stream().anyMatch(djpGuest -> {
                    checkIfUsageByBoardingPass(null,updateBoardingPassRequest, null,djpGuest);
                    return !updateBoardingPassRequest.getCanUseService();
                });

                if (hasGuestBp && !updateBoardingPassRequest.getCanUseService()) {
                    response.setMessage("Bu misafir yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(updateBoardingPassRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(updateBoardingPassRequest.getBoardingType()))+" hakkından yararlanabilir.");
                    response.setType("E");
                    return response;
                }
            }
        }
        return response;
    }
    private boolean ifSameCompanyForGuest(AccessBoardingPassRequest accessBoardingPassRequest,DjpPassLocationsModel djpPassLocation) {
        try {
            boolean couldNotReadBoardingPass = false;
            DjpAccessRightsModel djpAccessRights = null;
            String pnrCode = null;
            String className = null;
            String airlineCode = null;
            String guestIATA = null;
            boolean sameCompanyBoardingPass = true;
            List<DjpGuestData> djpGuestDataList = new ArrayList<>();
            if (!accessBoardingPassRequest.getGuests().isEmpty()) {
                djpGuestDataList.addAll(accessBoardingPassRequest.getGuests());

                couldNotReadBoardingPass = Objects.nonNull(accessBoardingPassRequest.getCouldNotReadBoardingPass()) && accessBoardingPassRequest.getCouldNotReadBoardingPass();

                if (!couldNotReadBoardingPass) {
                    pnrCode = accessBoardingPassRequest.getPnrCode();
                    className = accessBoardingPassRequest.getBoardingClass();
                    if (StringUtils.isNotBlank(pnrCode) && pnrCode.length() > 50) {
                        airlineCode = pnrCode.substring(36, 39);
                        airlineCode = airlineCode.contains(" ") ? airlineCode.replace(" ", "") : airlineCode;
                        djpAccessRights = djpTokenService.getAccessRightsByAirlineCodeAndClassName(airlineCode, className, djpPassLocation);
                    } else if (pnrCode.length() < 50) {
                        djpAccessRights = djpTokenService.getAccessRightsByAirlineCodeAndClassName(pnrCode.substring(0, 2).toUpperCase(), className, djpPassLocation);
                    }
                } else {
                    airlineCode = accessBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2);
                    className = accessBoardingPassRequest.getBoardingClass();
                    djpAccessRights = djpTokenService.getAccessRightsByAirlineCodeAndClassName(airlineCode, className, djpPassLocation);
                }
                if(Objects.nonNull(djpAccessRights)) {
                    if (Boolean.TRUE.equals(djpAccessRights.getRequiredSameCompanyForGuest())) {
                        for (DjpGuestData djpGuestData : djpGuestDataList) {
                            boolean couldNotReadBoardingPassForGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();

                            if (!couldNotReadBoardingPassForGuest) {
                                guestIATA = djpGuestData.getPnrCode().substring(36, 39);
                                guestIATA = guestIATA.contains(" ") ? guestIATA.replace(" ", "") : guestIATA;
                                if (!airlineCode.equals(guestIATA)) {
                                    sameCompanyBoardingPass = false;
                                    break;
                                }

                            } else {
                                guestIATA = djpGuestData.getFlightNumber().substring(0, 2);
                                if (!airlineCode.equals(guestIATA)) {
                                    sameCompanyBoardingPass = false;
                                    break;
                                }
                            }
                        }
                    }
                }else {
                    if(StringUtils.isNotBlank(accessBoardingPassRequest.getRefNumber())){
                        List<String> membershipCodes = djpTokenService.getMembershipCodes();
                        String membershipCode = accessBoardingPassRequest.getRefNumber().substring(0,2).toUpperCase();
                        boolean isServiceUsageWithMembership = membershipCodes.contains(membershipCode);
                        return Boolean.TRUE.equals(isServiceUsageWithMembership);
                    }
                    return Boolean.TRUE;

                }
            }
            return sameCompanyBoardingPass;
        } catch (Exception e) {
            LOG.error("has error ifSameCompanyForGuest:", e);
            return false;
        }
    }
    @Override
    public FioriStatusResponse updateBoardingPasses(FioriStatusResponse response, UpdateBoardingPassRequest updateBoardingPassRequest, DjpBoardingPassModel boardingPassModel, DjpAccessBoardingModel accessBoardingModel) {
        QrType whereUsed = QrType.valueOf(qrTypeEnumHashMap.get(updateBoardingPassRequest.getBoardingType()));
        if(Objects.nonNull(boardingPassModel)){
            List<DjpBoardingPassModel> guestOnBoardingPass = new ArrayList<>(boardingPassModel.getDjpBoardingPass());

            if(!CollectionUtils.isEmpty(updateBoardingPassRequest.getGuests())){
                List<DjpGuestModel> guests=new ArrayList<>(boardingPassModel.getGuests());
                updateBoardingPassRequest.getGuests().forEach(djpGuest -> {
                    DjpGuestModel djpGuestModel = new DjpGuestModel();
                    djpGuestModel.setName(djpGuest.getName());
                    djpGuestModel.setPassport(djpGuest.getPassport());
                    djpGuestModel.setSurname(djpGuest.getSurname());
                    djpGuestModel.setPhoneNumber(djpGuest.getPhoneNumber());
                    djpGuestModel.setTckn(djpGuest.getTckn());
                    djpGuestModel.setBirthDate(djpGuest.getBirthDate());
                    djpGuestModel.setEmail(djpGuest.getEmail());
                    djpGuestModel.setGender(djpGuest.getGender());
                    djpGuestModel.setNationality(djpGuest.getNationality());
                    djpGuestModel.setPnrCode(djpGuest.getPnrCode().toUpperCase());
                    djpGuestModel.setKvkkCheck(djpGuest.getKvkkCheck());
                    djpGuestModel.setWifiCheck(djpGuest.getWifiCheck());
                    modelService.saveOrUpdate(djpGuestModel);
                    guests.add(djpGuestModel);
                });
                boardingPassModel.setGuests(guests);
                DjpBoardingPassModel superBoardingPass = boardingPassModel;
                updateBoardingPassRequest.getGuests().forEach(djpGuestData -> {
                    DjpBoardingPassModel guestBoardingPass = getBoardingPass(null,updateBoardingPassRequest, djpGuestData,updateBoardingPassRequest.getBoardingType());
                    if(Objects.isNull(guestBoardingPass)) {
                        guestBoardingPass = new DjpBoardingPassModel();
                        List<DjpBoardingPassModel> superBoarding = new ArrayList<DjpBoardingPassModel>();
                        superBoarding.add(superBoardingPass);
                        guestBoardingPass.setSuperBoarding(superBoarding);
                        guestBoardingPass.setToken(superBoardingPass.getToken());
                        boolean couldNotReadBoardingPassForGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                        if (couldNotReadBoardingPassForGuest) {
                            Date dateOfFlight = DateUtil.convertStartDate(djpGuestData.getDate());
                            String operatingCarrierDesignator = djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                            String flightNumber = djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                            String seatNumber = djpGuestData.getSeatCode().toUpperCase();
                            String fromCityAirportCode = djpGuestData.getFromAirportIATA().toUpperCase();
                            String toCityAirportCode = djpGuestData.getToAirportIATA().toUpperCase();
                            String compartmentCode = djpGuestData.getClassCode().toUpperCase();
                            String passengerName = djpGuestData.getName() + djpGuestData.getSurname();
                            if(seatNumber.length() < 4) {
                                do {
                                    String zero = "0";
                                    String blank = " ";
                                    if (seatNumber.startsWith("NS")) {
                                        seatNumber = seatNumber + blank;
                                    } else {
                                        seatNumber = zero + seatNumber;
                                    }
                                } while (seatNumber.length() != 4);
                            }
                            guestBoardingPass.setDateOfFlight(dateOfFlight);
                            guestBoardingPass.setFlightNumber(flightNumber);
                            guestBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                            guestBoardingPass.setSeatNumber(seatNumber);
                            guestBoardingPass.setFromCityAirportCode(fromCityAirportCode);
                            guestBoardingPass.setToCityAirportCode(toCityAirportCode);
                            guestBoardingPass.setCompartmentCode(compartmentCode);
                            guestBoardingPass.setPassengerName(passengerName);
                            guestBoardingPass.setPnrCode(passengerName + "/" + compartmentCode + "/" + fromCityAirportCode + "/" + toCityAirportCode + "/" + DateUtil.toDatePattern(dateOfFlight,"dd.MM.yyyy") +"/"+operatingCarrierDesignator+ "/" + flightNumber + "/" + seatNumber);
                            guestBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                            guestBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                            guestBoardingPass.setNewSale(superBoardingPass.getNewSale());
                        } else {
                            String guestDataPnrCode = djpGuestData.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                            guestDataPnrCode = guestDataPnrCode.replaceAll("&", "/");
                            guestBoardingPass.setPnrCode(guestDataPnrCode);
                            guestBoardingPass.setFormatCode(guestDataPnrCode.substring(0, 1));
                            guestBoardingPass.setNumberOfLegsEncoded(guestDataPnrCode.substring(1, 2));
                            guestBoardingPass.setPassengerName(guestDataPnrCode.substring(2, 22));
                            guestBoardingPass.setElectronicTicketIndicator(guestDataPnrCode.substring(22, 23));
                            guestBoardingPass.setOperatingCarrierPnrCode(guestDataPnrCode.substring(23, 30));
                            guestBoardingPass.setFromCityAirportCode(guestDataPnrCode.substring(30, 33));
                            guestBoardingPass.setToCityAirportCode(guestDataPnrCode.substring(33, 36));
                            String operatingCarrierDesignator = guestDataPnrCode.substring(36,39);
                            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ","") : operatingCarrierDesignator;
                            guestBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                            String flightNumber = guestDataPnrCode.substring(39, 44);
                            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                            guestBoardingPass.setFlightNumber(flightNumber);
                            guestBoardingPass.setDateOfFlight(DateUtil.convertJulianDateToDate(guestDataPnrCode.substring(44, 47)));
                            guestBoardingPass.setCompartmentCode(guestDataPnrCode.substring(47, 48));
                            guestBoardingPass.setSeatNumber(guestDataPnrCode.substring(48, 52));
                            guestBoardingPass.setCheckInSequenceNumber(guestDataPnrCode.substring(52, 57));
                            guestBoardingPass.setPassengerStatus(guestDataPnrCode.substring(57, 58));
                            guestBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                            guestBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                            guestBoardingPass.setNewSale(superBoardingPass.getNewSale());
                        }
                    }
                    List<DjpBoardingPassModel> superBoarding = null;
                    if(CollectionUtils.isNotEmpty(guestBoardingPass.getSuperBoarding())){
                        superBoarding = new ArrayList<>(guestBoardingPass.getSuperBoarding());
                    }else {
                        superBoarding = new ArrayList<>();
                    }
                    superBoarding.add(superBoardingPass);
                    guestBoardingPass.setSuperBoarding(superBoarding);
                    guestBoardingPass.setWifiPassword(djpGuestData.getWifiKey());
                    guestBoardingPass.setPersonalID(superBoardingPass.getPersonalID());
                    guestBoardingPass.setPersonalName(superBoardingPass.getPersonalName());
                    guestBoardingPass.setBoardingUsage(whereUsed);
                    guestBoardingPass.setWhichLocation(superBoardingPass.getWhichLocation());
                    guestBoardingPass.setMiddleWareDetail(getStatusForMiddleWare(guestBoardingPass.getMiddleWareDetail(),guestBoardingPass.getCouldNotReadBoardingPass(),guestBoardingPass.getFromCityAirportCode(),guestBoardingPass.getDateOfFlight(),guestBoardingPass.getOperatingCarrierDesignator()+guestBoardingPass.getFlightNumber(),whereUsed));
                    guestBoardingPass.setUsageMethod(UsageMethod.DESK);
                    modelService.saveOrUpdate(guestBoardingPass);
                   // eventService.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),guestBoardingPass,null,whereUsed));
                    guestOnBoardingPass.add(guestBoardingPass);
                    boardingPassModel.setDjpBoardingPass(guestOnBoardingPass);
                    modelService.saveOrUpdate(boardingPassModel);
                });
            }
            response.setType("S");
            response.setMessage("Misafir Eklenmiştir");
            return response;
        }else {
            List<DjpAccessBoardingModel> guestOnAccBoarding = new ArrayList<>(accessBoardingModel.getSuperAccBoarding());
            if (!CollectionUtils.isEmpty(updateBoardingPassRequest.getGuests())) {
                List<DjpGuestModel> guests = new ArrayList<>(accessBoardingModel.getGuests());
                updateBoardingPassRequest.getGuests().forEach(djpGuest -> {
                    DjpGuestModel djpGuestModel = new DjpGuestModel();
                    djpGuestModel.setName(djpGuest.getName());
                    djpGuestModel.setPassport(djpGuest.getPassport());
                    djpGuestModel.setSurname(djpGuest.getSurname());
                    djpGuestModel.setPhoneNumber(djpGuest.getPhoneNumber());
                    djpGuestModel.setTckn(djpGuest.getTckn());
                    djpGuestModel.setBirthDate(djpGuest.getBirthDate());
                    djpGuestModel.setEmail(djpGuest.getEmail());
                    djpGuestModel.setGender(djpGuest.getGender());
                    djpGuestModel.setNationality(djpGuest.getNationality());
                    djpGuestModel.setPnrCode(djpGuest.getPnrCode().toUpperCase());
                    djpGuestModel.setKvkkCheck(djpGuest.getKvkkCheck());
                    djpGuestModel.setWifiCheck(djpGuest.getWifiCheck());
                    modelService.saveOrUpdate(djpGuestModel);
                    guests.add(djpGuestModel);
                });
                accessBoardingModel.setGuests((Set<DjpGuestModel>) guests);
            }
            modelService.saveOrUpdate(accessBoardingModel);
            DjpAccessBoardingModel superBoardingPass = accessBoardingModel;
            updateBoardingPassRequest.getGuests().forEach(djpGuestData -> {
                DjpAccessBoardingModel guestAccBoardingPass = getAccessBoardingPass(null,updateBoardingPassRequest,djpGuestData,updateBoardingPassRequest.getBoardingType());
                if (Objects.isNull(guestAccBoardingPass)) {
                    guestAccBoardingPass = new DjpAccessBoardingModel();

                    List<DjpAccessBoardingModel> superBoarding = new ArrayList<DjpAccessBoardingModel>();
                    superBoarding.add(superBoardingPass);
                    guestAccBoardingPass.setSuperAccBoarding(superBoarding);
                    boolean couldNotReadBoardingPassForGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                    if (couldNotReadBoardingPassForGuest) {

                        Date dateOfFlight = DateUtil.convertStartDate(djpGuestData.getDate());
                        String operatingCarrierDesignator = djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                        String flightNumber = djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                        flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                        String seatNumber = djpGuestData.getSeatCode().toUpperCase();
                        String fromCityAirportCode = djpGuestData.getFromAirportIATA().toUpperCase();
                        String toCityAirportCode = djpGuestData.getToAirportIATA().toUpperCase();
                        String compartmentCode = djpGuestData.getClassCode().toUpperCase();
                        String passengerName = djpGuestData.getName() + djpGuestData.getSurname();
                        if(seatNumber.length() < 4) {
                            do {
                                String zero = "0";
                                String blank = " ";
                                if (seatNumber.startsWith("NS")) {
                                    seatNumber = seatNumber + blank;
                                } else {
                                    seatNumber = zero + seatNumber;
                                }
                            } while (seatNumber.length() != 4);
                        }
                        guestAccBoardingPass.setDateOfFlight(dateOfFlight);
                        guestAccBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                        guestAccBoardingPass.setFlightNumber(flightNumber);
                        guestAccBoardingPass.setSeatNumber(seatNumber);
                        guestAccBoardingPass.setFromCityAirportCode(fromCityAirportCode);
                        guestAccBoardingPass.setToCityAirportCode(toCityAirportCode);
                        guestAccBoardingPass.setCompartmentCode(compartmentCode);
                        guestAccBoardingPass.setPassengerName(passengerName);
                        guestAccBoardingPass.setPnrCode(passengerName + "/" + compartmentCode + "/" + fromCityAirportCode + "/" + toCityAirportCode + "/" + DateUtil.toDatePattern(dateOfFlight,"dd.MM.yyyy") +"/"+operatingCarrierDesignator+ "/" + flightNumber + "/" + seatNumber);
                        guestAccBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                        guestAccBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                        guestAccBoardingPass.setNewSale(superBoardingPass.getNewSale());
                    } else {
                        String guestPnrCode = djpGuestData.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                        guestPnrCode = guestPnrCode.replaceAll("&", "/");
                        guestAccBoardingPass.setPnrCode(guestPnrCode);
                        guestAccBoardingPass.setFormatCode(guestPnrCode.substring(0, 1));
                        guestAccBoardingPass.setNumberOfLegsEncoded(guestPnrCode.substring(1, 2));
                        guestAccBoardingPass.setPassengerName(guestPnrCode.substring(2, 22));
                        guestAccBoardingPass.setElectronicTicketIndicator(guestPnrCode.substring(22, 23));
                        guestAccBoardingPass.setOperatingCarrierPnrCode(guestPnrCode.substring(23, 30));
                        guestAccBoardingPass.setFromCityAirportCode(guestPnrCode.substring(30, 33));
                        guestAccBoardingPass.setToCityAirportCode(guestPnrCode.substring(33, 36));
                        String operatingCarrierDesignator = guestPnrCode.substring(36,39);
                        operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ","") : operatingCarrierDesignator;
                        guestAccBoardingPass.setOperatingCarrierDesignator(operatingCarrierDesignator);
                        String flightNumber = guestPnrCode.substring(39, 44);
                        flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                        guestAccBoardingPass.setFlightNumber(flightNumber);
                        guestAccBoardingPass.setDateOfFlight(DateUtil.convertJulianDateToDate(guestPnrCode.substring(44, 47)));
                        guestAccBoardingPass.setCompartmentCode(guestPnrCode.substring(47, 48));
                        guestAccBoardingPass.setSeatNumber(guestPnrCode.substring(48, 52));
                        guestAccBoardingPass.setCheckInSequenceNumber(guestPnrCode.substring(52, 57));
                        guestAccBoardingPass.setPassengerStatus(guestPnrCode.substring(57, 58));
                        guestAccBoardingPass.setCouldNotReadBoardingPass(couldNotReadBoardingPassForGuest);
                        guestAccBoardingPass.setSapOrderID(superBoardingPass.getSapOrderID());
                        guestAccBoardingPass.setNewSale(superBoardingPass.getNewSale());
                    }
                }
                List<DjpAccessBoardingModel> superBoarding = new ArrayList<DjpAccessBoardingModel>(guestAccBoardingPass.getSuperAccBoarding());
                superBoarding.add(superBoardingPass);
                guestAccBoardingPass.setSuperAccBoarding(superBoarding);
                guestAccBoardingPass.setWifiPassword(djpGuestData.getWifiKey());
                Set<QrType> boardingUsages = null;
                if(Objects.nonNull(guestAccBoardingPass.getBoardingUsages())) {
                    boardingUsages = new HashSet<>(guestAccBoardingPass.getBoardingUsages());
                }else {
                    boardingUsages = new HashSet<>();
                }
                boardingUsages.add(whereUsed);
                guestAccBoardingPass.setSuperAccBoarding(superBoarding);
                guestAccBoardingPass.setBoardingUsages(boardingUsages);
                guestAccBoardingPass.setContractID(superBoardingPass.getContractID());
                guestAccBoardingPass.setBoardingClass(superBoardingPass.getBoardingClass());
                List<String> guestModifableList = CollectionUtils.isEmpty(guestAccBoardingPass.getRefNumberList()) ? new ArrayList<>() : (List<String>) guestAccBoardingPass.getRefNumberList();
                guestModifableList.addAll(CollectionUtils.isEmpty(superBoardingPass.getRefNumberList()) ? Collections.emptyList() : superBoardingPass.getRefNumberList());
                guestAccBoardingPass.setRefNumberList((List<String>) guestModifableList);
                guestAccBoardingPass.setPersonalID(superBoardingPass.getPersonalID());
                guestAccBoardingPass.setPersonalName(superBoardingPass.getPersonalName());
                guestAccBoardingPass.setWhichLocation(superBoardingPass.getWhichLocation());
                guestAccBoardingPass.setMiddleWareDetail(getStatusForMiddleWare(guestAccBoardingPass.getMiddleWareDetail(),guestAccBoardingPass.getCouldNotReadBoardingPass(),guestAccBoardingPass.getFromCityAirportCode(),guestAccBoardingPass.getDateOfFlight(),guestAccBoardingPass.getOperatingCarrierDesignator()+guestAccBoardingPass.getFlightNumber(),whereUsed));
                guestAccBoardingPass.setUsageMethod(UsageMethod.DESK);
                modelService.saveOrUpdate(guestAccBoardingPass);
                //eventService.publishEvent(initializeDjpBoardingPassSendToMwEvent(new DjpBoardingPassSendToMwEvent(),null,guestAccBoardingPass,whereUsed));
                guestOnAccBoarding.add(guestAccBoardingPass);
                accessBoardingModel.setSuperAccBoarding(guestOnAccBoarding);
                modelService.saveOrUpdate(accessBoardingModel);
                // TODO : guest : M , child : C , baby : B for boardingtype
                djpRemainingUsageFacade.startDjpUsageSendToErpProcess(updateBoardingPassRequest.getBoardingType(), null, guestAccBoardingPass, "M",null,null);
            });
            response.setType("S");
            response.setMessage("Misafir Eklenmiştir");
            return response;
        }

    }
    @Override
    public DjpAccessBoardingModel getAccessBoardingPass(QrForFioriRequest qrForFioriRequest,AccessBoardingPassRequest accBoardingPassRequest, DjpGuestData djpGuestData, String whereUsedBoardingPass) {
        try {
            DjpAccessBoardingModel djpAccessBoardingModel = null;
            String pnrCode = Objects.isNull(djpGuestData) ? Objects.nonNull(qrForFioriRequest) ? qrForFioriRequest.getPnrCode().toUpperCase() : accBoardingPassRequest.getPnrCode().toUpperCase() : djpGuestData.getPnrCode().toUpperCase();
            boolean couldNotReadBoardingPass = Objects.isNull(djpGuestData) ? Objects.nonNull(qrForFioriRequest) ? Boolean.TRUE.equals(qrForFioriRequest.getCouldNotReadBoardingPass()) : Boolean.TRUE.equals(accBoardingPassRequest.getCouldNotReadBoardingPass()) : Boolean.TRUE.equals(djpGuestData.getCouldNotReadBoardingPass());
            Date flightDate = null, sub48HoursCurrentDate = null;
            String flightNumber = null, seatNumber = null, fromAirportIATA = null, toAirportIATA = null, classCode = null, operatingCarrierDesignator = null;
            String currentPassangerName = null;
            if (StringUtils.isBlank(pnrCode) && Boolean.TRUE.equals(couldNotReadBoardingPass)) {
                currentPassangerName = Objects.isNull(djpGuestData) ? Objects.nonNull(qrForFioriRequest) ? qrForFioriRequest.getPassengerName().toUpperCase() : accBoardingPassRequest.getPassengerName().toUpperCase() : djpGuestData.getName().toUpperCase() + " " + djpGuestData.getSurname().toUpperCase();
                flightDate = Objects.isNull(djpGuestData) ? DateUtil.convertStartDate(Objects.isNull(qrForFioriRequest) ? accBoardingPassRequest.getDate() : qrForFioriRequest.getDate()) : DateUtil.convertStartDate(djpGuestData.getDate());
                operatingCarrierDesignator = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? accBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2) : djpGuestData.getFlightNumber().toUpperCase().substring(0, 2);
                flightNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? accBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, accBoardingPassRequest.getFlightNumber().length()) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length()) : djpGuestData.getFlightNumber().toUpperCase().substring(2, djpGuestData.getFlightNumber().length());
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                seatNumber = Objects.isNull(djpGuestData) ? Objects.isNull(qrForFioriRequest) ? accBoardingPassRequest.getSeatCode().toUpperCase() : qrForFioriRequest.getSeatCode().toUpperCase() : djpGuestData.getSeatCode().toUpperCase();
                if(seatNumber.length() < 4) {
                    do {
                        String zero = "0";
                        String blank = " ";
                        if (seatNumber.startsWith("NS")) {
                            seatNumber = seatNumber + blank;
                        } else {
                            seatNumber = zero + seatNumber;
                        }
                    } while (seatNumber.length() != 4);
                }
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
            }

            if (StringUtils.isNotBlank(pnrCode) && Boolean.FALSE.equals(couldNotReadBoardingPass)) {
                currentPassangerName = pnrCode.substring(2,22).toUpperCase();
                operatingCarrierDesignator = pnrCode.substring(36, 39);
                operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
                flightNumber = pnrCode.substring(39, 44);
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
                seatNumber = pnrCode.substring(48, 52);
                sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
                djpAccessBoardingModel = djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
            } else {
                djpAccessBoardingModel = djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(whereUsedBoardingPass)));
            }
            if (checkIfBoardingPass(qrForFioriRequest, null, djpAccessBoardingModel, null, accBoardingPassRequest, currentPassangerName)) {
                return djpAccessBoardingModel;
            } else {
                return null;
            }
        }catch (Exception e){
            LOG.error("has error getAccessBoardingPass:",e);
            return null;
        }
    }

    @Override
    public DjpAccessBoardingModel findDjpAccessBoarding(UpdateBoardingPassRequest updateBoardingPassRequest,QrForFioriRequest qrForFioriRequest) {
        Date flightDate = null;
        Date sub48HoursCurrentDate = null;
        String flightNumber = null;
        String seatNumber = null;
        String operatingCarrierDesignator = null;

        boolean couldNotReadBoardingPass = false;
        String pnrCode = "";
        String boardingType = "";

        if(Objects.nonNull(updateBoardingPassRequest)){
            couldNotReadBoardingPass = Objects.nonNull(updateBoardingPassRequest.getCouldNotReadBoardingPass()) && updateBoardingPassRequest.getCouldNotReadBoardingPass();
            pnrCode = !couldNotReadBoardingPass ? updateBoardingPassRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
            boardingType = updateBoardingPassRequest.getBoardingType();
        }else {
            couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
            pnrCode = !couldNotReadBoardingPass ? qrForFioriRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
            boardingType = qrForFioriRequest.getBoardingType();
        }


        String currentPassangerName = null;
        if (StringUtils.isBlank(pnrCode)) {
            currentPassangerName = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getPassengerName().toUpperCase() : qrForFioriRequest.getPassengerName().toUpperCase();
            flightDate = Objects.nonNull(updateBoardingPassRequest) ? DateUtil.convertStartDate(updateBoardingPassRequest.getDate()) :DateUtil.convertStartDate(qrForFioriRequest.getDate());
            operatingCarrierDesignator = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2);
            flightNumber = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, updateBoardingPassRequest.getFlightNumber().length()) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length());
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            seatNumber = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getSeatCode().toUpperCase() : qrForFioriRequest.getSeatCode().toUpperCase();
            if(seatNumber.length() < 4) {
                do {
                    String zero = "0";
                    String blank = " ";
                    if (seatNumber.startsWith("NS")) {
                        seatNumber = seatNumber + blank;
                    } else {
                        seatNumber = zero + seatNumber;
                    }
                } while (seatNumber.length() != 4);
            }
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
        }

        if (StringUtils.isNotBlank(pnrCode)) {
            currentPassangerName = pnrCode.substring(2, 22).toUpperCase();
            operatingCarrierDesignator = pnrCode.substring(36, 39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39, 44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
            seatNumber = pnrCode.substring(48, 52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());


        }
        return djpBoardingDao.getDjpAccessBoarding(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(boardingType)));
    }
    @Override
    public DjpBoardingPassModel getDjpBpByQrOrFiori(UpdateBoardingPassRequest updateBoardingPassRequest,QrForFioriRequest qrForFioriRequest){
        Date flightDate = null;
        Date sub48HoursCurrentDate = null;
        String flightNumber = null;
        String seatNumber = null;
        String operatingCarrierDesignator = null;

        boolean couldNotReadBoardingPass = false;
        String pnrCode = "";
        String boardingType = "";

        if(Objects.nonNull(updateBoardingPassRequest)){
            couldNotReadBoardingPass = Objects.nonNull(updateBoardingPassRequest.getCouldNotReadBoardingPass()) && updateBoardingPassRequest.getCouldNotReadBoardingPass();
            pnrCode = !couldNotReadBoardingPass ? updateBoardingPassRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
            boardingType = updateBoardingPassRequest.getBoardingType();
        }else {
            couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
            pnrCode = !couldNotReadBoardingPass ? qrForFioriRequest.getPnrCode().toUpperCase() : StringUtils.EMPTY;
            boardingType = qrForFioriRequest.getBoardingType();
        }


        String currentPassangerName = null;
        if (StringUtils.isBlank(pnrCode)) {
            currentPassangerName = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getPassengerName().toUpperCase() : qrForFioriRequest.getPassengerName().toUpperCase();
            flightDate = Objects.nonNull(updateBoardingPassRequest) ? DateUtil.convertStartDate(updateBoardingPassRequest.getDate()) :DateUtil.convertStartDate(qrForFioriRequest.getDate());
            operatingCarrierDesignator = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(0, 2) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2);
            flightNumber = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getFlightNumber().toUpperCase().substring(2, updateBoardingPassRequest.getFlightNumber().length()) : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length());
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            seatNumber = Objects.nonNull(updateBoardingPassRequest) ? updateBoardingPassRequest.getSeatCode().toUpperCase() : qrForFioriRequest.getSeatCode().toUpperCase();
            if(seatNumber.length() < 4) {
                do {
                    String zero = "0";
                    String blank = " ";
                    if (seatNumber.startsWith("NS")) {
                        seatNumber = seatNumber + blank;
                    } else {
                        seatNumber = zero + seatNumber;
                    }
                } while (seatNumber.length() != 4);
            }
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());
        }

        if (StringUtils.isNotBlank(pnrCode)) {
            currentPassangerName = pnrCode.substring(2, 22).toUpperCase();
            operatingCarrierDesignator = pnrCode.substring(36, 39);
            operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
            flightNumber = pnrCode.substring(39, 44);
            flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
            flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
            seatNumber = pnrCode.substring(48, 52);
            sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(new Date());


        }
        DjpBoardingPassModel djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByQr(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate,QrType.valueOf(qrTypeEnumHashMap.get(boardingType)));
        if (Objects.nonNull(djpBoardingPassModel)) {
            return djpBoardingPassModel;
        } else {
            djpBoardingPassModel = djpBoardingDao.getDjpBoardingPassByFiori(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, QrType.valueOf(qrTypeEnumHashMap.get(boardingType)));
            if (Objects.nonNull(djpBoardingPassModel)) {
                return djpBoardingPassModel;
            } else {
                return djpBoardingDao.getDjpBoardingPass(flightDate, operatingCarrierDesignator, flightNumber, seatNumber, sub48HoursCurrentDate, QrType.valueOf(qrTypeEnumHashMap.get(boardingType)));
            }
        }
    }

}
