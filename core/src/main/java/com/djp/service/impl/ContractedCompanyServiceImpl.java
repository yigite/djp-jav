package com.djp.service.impl;

import com.djp.core.dao.DjpContractedCompanyDao;
import com.djp.core.model.ContractedCompaniesModel;
import com.djp.service.ContractedCompanyService;

import jakarta.annotation.Resource;

public class ContractedCompanyServiceImpl implements ContractedCompanyService {

    @Resource(name = "djpContractedCompanyDao")
    private DjpContractedCompanyDao contractedCompanyDao;
    @Override
    public ContractedCompaniesModel getContractedCompany(String contractID) {
        return contractedCompanyDao.getContractedCompany(contractID);
    }
}

