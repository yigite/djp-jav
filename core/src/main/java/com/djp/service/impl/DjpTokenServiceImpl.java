package com.djp.service.impl;

import com.djp.core.dao.DjpTokenDao;
import com.djp.core.model.DjpAccessRightsModel;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.data.login.TokenResponse;
import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.core.model.DjpUniqueTokenModel;
import com.djp.modelservice.ModelService;
import com.djp.service.DjpRemainingUsageService;
import com.djp.service.DjpTokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.collections.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

public class DjpTokenServiceImpl implements DjpTokenService {
    private static final Logger LOGGER = LogManager.getLogger(DjpTokenServiceImpl.class);

    @Resource
    private DjpTokenDao djpTokenDao;

    @Resource
    private DjpRemainingUsageService djpRemainingUsageService;

    @Resource
    private ModelService modelService;

    @Override
    public DjpUniqueTokenModel generateToken(String customerUid, String orderCode, String type, Date expireDate) {
        String token = null;
        DjpUniqueTokenModel tokenFromKey = null;
        List<String> membershipCodes = getMembershipCodes();
        List<String> boardingPassFormatCodes = Arrays.asList("M1","M2","M3");
        do{
            token = generateRandomString(new Random(), 6);
            if(orderCode!=null
                    && orderCode.startsWith("campaign")){
                token="DJP"+token.substring(3,6);
            }
            tokenFromKey = djpTokenDao.getTokenFromKey(token);
        }while (tokenFromKey != null || boardingPassFormatCodes.contains(token.substring(0,2).toUpperCase()) || membershipCodes.contains(token.substring(0,2).toUpperCase()));

        DjpUniqueTokenModel tokenModel = new DjpUniqueTokenModel();
        tokenModel.setCode(token);
        tokenModel.setExpireDate(expireDate);
        tokenModel.setUserUid(customerUid);
        tokenModel.setOrderCode(orderCode);
        tokenModel.setType(type);
        try {
            if (org.springframework.util.StringUtils.hasText(customerUid)
                    && (!org.springframework.util.StringUtils.hasText(orderCode) || "0".equals(orderCode))){
                List<DjpRemainingUsageModel> djpRemainingUsageModels = djpRemainingUsageService.getCustomerUsages(customerUid);
                if(!CollectionUtils.isEmpty(djpRemainingUsageModels)){
                    tokenModel.setOrderCode(djpRemainingUsageModels.iterator().next().getOrderCode());
                }
            }
        }catch (Exception e){

        }
        modelService.saveOrUpdate(tokenModel);
        return tokenModel;
    }

    @Override
    public List<String> getMembershipCodes() {
        List<DjpAccessRightsModel> serviceUsageWithMembership = djpTokenDao.getServiceUsageWithMembership();
        List<String> membershipcodes = new ArrayList<>();
        if(!serviceUsageWithMembership.isEmpty()){
            membershipcodes = serviceUsageWithMembership.stream().map(DjpAccessRightsModel::getAirlineCode).collect(Collectors.toList());
        }
        return membershipcodes;
    }

    @Override
    public DjpUniqueTokenModel generateTokenForDjp(String companyId, String type) {
        String tokenForDjp = null;
        DjpUniqueTokenModel tokenFromKey = null;
        List<String> membershipCodes = getMembershipCodes();
        List<String> boardingPassFormatCodes = Arrays.asList("M1","M2","M3");
        do{
            tokenForDjp = generateRandomString(new Random(), 6);
            tokenFromKey = djpTokenDao.getTokenFromKey(tokenForDjp);
        }while (tokenFromKey != null || boardingPassFormatCodes.contains(tokenForDjp.substring(0,2).toUpperCase()) || membershipCodes.contains(tokenForDjp.substring(0,2).toUpperCase()));

        DjpUniqueTokenModel tokenModel = new DjpUniqueTokenModel();

        tokenModel.setCode(tokenForDjp);
        tokenModel.setUserUid(companyId+"-"+tokenForDjp);
        tokenModel.setType(type);

        modelService.saveOrUpdate(tokenModel);

        return tokenModel;
    }

    @Value("${external.auth.clientid}")
    private String clientid;

    @Value("${external.auth.client_secret}")
    private String clientSecret;

    @Value("${airportrest.grant_type}")
    private String grantType;

    @Value("${external.authurl}")
    private String authUrl;

    @Resource
    private RestTemplate airportServicesRestTemplate;

    @Override
    public List<DjpAccessRightsModel> getAccessRights(String airport, DjpPassLocationsModel djpPassLocation, Boolean isServiceUsageWithMembership) {
        return djpTokenDao.getAccessRightsByAirlineCode(airport,djpPassLocation,isServiceUsageWithMembership);
    }

    @Override
    public DjpPassLocationsModel getDjpPassLocationByLocationID(String locationID) {
        return djpTokenDao.getDjpPassLocationByLocationID(locationID);
    }

    @Override
    public DjpUniqueTokenModel encodeCustomerFromToken(String token) {
        DjpUniqueTokenModel tokenModel = djpTokenDao.getTokenFromKey(token);

        return tokenModel;
    }

    private static String generateRandomString(Random random, int length) {
        return random.ints(48, 122)
                .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                .mapToObj(i -> (((char) i)+"").toUpperCase(new Locale("en")))
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    @Override
    public String getAuthToken() {
        LOGGER.info("----------------------------------------------------------------------------------------------------\n\n");
        LOGGER.info("START || getAuthToken()");
        try{
            final MultiValueMap<String, String> postHeadersMap = new LinkedMultiValueMap<>();
            postHeadersMap.add("client_id",clientid);
            postHeadersMap.add("client_secret",clientSecret);
            postHeadersMap.add("grant_type",grantType);

            HttpHeaders headers2 = new HttpHeaders();
            headers2.setContentType(MediaType.APPLICATION_FORM_URLENCODED);


            final HttpEntity<MultiValueMap<String, String>> posEntity = new HttpEntity<>(postHeadersMap, headers2);
            ResponseEntity<TokenResponse> postResponse = airportServicesRestTemplate.postForEntity(authUrl, posEntity, TokenResponse.class);
            LOGGER.info("SUCC || getAuthToken Response : Token Type : " + postResponse.getBody().getTokenType()+" Access Token :  "+postResponse.getBody().getAccessToken());
            LOGGER.info("----------------------------------------------------------------------------------------------------\n\n");
            return postResponse.getBody().getTokenType()+" "+postResponse.getBody().getAccessToken();

        } catch (Exception e){
            LOGGER.error("ERR ||getAuthToken() : ", e);
            LOGGER.info("----------------------------------------------------------------------------------------------------\n\n");
            return "";
        }
    }
    @Override
    public DjpAccessRightsModel getAccessRightsByAirlineCodeAndClassName(String airport, String className,DjpPassLocationsModel igaPassLocation) {
        return djpTokenDao.getAccessRightsByAirlineCodeAndClassName(airport,className,igaPassLocation);
    }
}
