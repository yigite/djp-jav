package com.djp.service.impl;

import com.djp.core.dao.DjpExternalCompanyDao;
import com.djp.core.model.DjpExternalCustomerModel;
import com.djp.service.DjpExternalCompanyService;
import com.djp.core.model.DjpExternalCompanyOrderTableModel;
import com.djp.service.DjpExternalCompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;

public class DjpExternalCompanyServiceImpl implements DjpExternalCompanyService {
    private static final Logger LOG = LoggerFactory.getLogger(DjpExternalCompanyServiceImpl.class);

    @Resource
    private DjpExternalCompanyDao djpExternalCompanyDao;

    @Override
    public DjpExternalCustomerModel findDjpExternalCustomerByID(String djpExternalCustomerID, String djpExternalCompanyClientID) {
        String companyID = djpExternalCompanyClientID.split("_")[0];
        return djpExternalCompanyDao.findDjpExternalCustomerByID(djpExternalCustomerID,companyID);
    }
    @Override
    public DjpExternalCompanyOrderTableModel findDjpExternalOrderByIdAndCustomer(String orderCode, String customerID) {
        try {
            return djpExternalCompanyDao.findDjpExternalOrderByIdAndCustomer(orderCode,customerID);
        }catch (Exception e){
            LOG.error("findDjpExternalOrderByIdAndCustomer has an error");
            return null;
        }
    }
}
