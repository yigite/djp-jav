package com.djp.service.impl;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.QrType;
import com.djp.core.model.DjpAccessBoardingModel;
import com.djp.core.model.DjpBoardingPassModel;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.service.DjpCommerceService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import com.djp.core.model.DjpMiddleWareBoardingPassStatusModel;


import jakarta.annotation.Resource;
import java.util.HashMap;
import java.util.Objects;


public class DjpCommerceServiceImpl implements DjpCommerceService {

    private static final Logger LOG =  LoggerFactory.getLogger(DjpCommerceServiceImpl.class);

    @Resource
    private HashMap<String, String> middleWareUsageMap;

    @Value("${middle.ware.bp.service.posturl}")
    private String middleWareBpServicePostUrl;
    
    @Resource
    private ModelService modelService;

    @Override
    public void sendMiddleWareBPInfo(DjpBoardingPassModel boardingPassModel, DjpAccessBoardingModel djpAccessBoardingModel, QrType whereUsed) {
        String pnrCode = "";
        Integer status = null;
        String deviceID = "";
        boolean shouldItBeSentToMiddleWare = middleWareUsageMap.containsKey(whereUsed.getCode());
        if (Boolean.TRUE.equals(shouldItBeSentToMiddleWare)) {
            DjpMiddleWareBoardingPassStatusModel statusModel = null;
            if (Objects.nonNull(boardingPassModel)) {
                pnrCode = fillPnrCode(boardingPassModel,null);
                status = boardingPassModel.getMiddleWareDetail().getStatusForMiddleWare();
                statusModel = boardingPassModel.getMiddleWareDetail();
            }
            if (Objects.nonNull(djpAccessBoardingModel)) {
                pnrCode = fillPnrCode(null,djpAccessBoardingModel);
                status = djpAccessBoardingModel.getMiddleWareDetail().getStatusForMiddleWare();
                statusModel = djpAccessBoardingModel.getMiddleWareDetail();
            }

            int cycle = 0;
            String result = "";
            do {
                cycle = cycle + 1;
                JSONObject middleWareRequest = new JSONObject();
                middleWareRequest.put(DjpCoreConstants.StatusForFlightHours.PNR_CODE, pnrCode);
                deviceID = middleWareUsageMap.get(whereUsed.getCode());
                middleWareRequest.put(DjpCoreConstants.StatusForFlightHours.DEVICE_ID, deviceID);
                middleWareRequest.put(DjpCoreConstants.StatusForFlightHours.STATUS, status);
                middleWareRequest.put(DjpCoreConstants.StatusForFlightHours.PASSED, status.toString().equals("9") ? 1 : null);


                try (CloseableHttpClient client = HttpClients.createDefault()) {

                    String postUrl = middleWareBpServicePostUrl;
                    LOG.info("middle ware bp post url:" + postUrl);
                    HttpPost httpPost = createPostRequestForGlobalBilgi(middleWareRequest, postUrl);
                    LOG.info("middleWareRequest:" + EntityUtils.toString(httpPost.getEntity()));
                    CloseableHttpResponse httpResponse = client.execute(httpPost);

                    result = EntityUtils.toString(httpResponse.getEntity());
                    LOG.info("middleWareResponse:" + result);
                } catch (Exception ex) {
                    LOG.error("sendMiddleWareBPInfo has an error:", ex);
                }
            } while ((StringUtils.isBlank(result) && cycle != 3) || (DjpCoreConstants.StatusForFlightHours.MIDDLE_WARE_ERROR_STATUS.equals(result) && cycle != 3));

            try {
                fillAndSetStatusMiddleWare(boardingPassModel, djpAccessBoardingModel, whereUsed, statusModel, result);
            } catch (Exception ex) {
                LOG.error("saveOrUpdate middle ware detail has an error:", ex);
                fillAndSetStatusMiddleWare(boardingPassModel, djpAccessBoardingModel, whereUsed, statusModel, DjpCoreConstants.StatusForFlightHours.MIDDLE_WARE_ERROR_STATUS);
            }

        }
    }

    private String fillPnrCode(DjpBoardingPassModel boardingPassModel,DjpAccessBoardingModel djpAccessBoardingModel) {
        boolean couldNotReadBoardingPass = Boolean.FALSE;
        String passengerName = "";
        String pnrCode = "";
        couldNotReadBoardingPass = Objects.nonNull(boardingPassModel) ? boardingPassModel.getCouldNotReadBoardingPass() : djpAccessBoardingModel.getCouldNotReadBoardingPass();
        passengerName = Objects.nonNull(boardingPassModel) ? boardingPassModel.getPassengerName() : djpAccessBoardingModel.getPassengerName();
        if(Boolean.TRUE.equals(couldNotReadBoardingPass) && Objects.nonNull(boardingPassModel)){
            if(passengerName.length() > 20){
                pnrCode = passengerName.substring(0,20) +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getCompartmentCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getFromCityAirportCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getToCityAirportCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        DateUtil.toDatePattern(boardingPassModel.getDateOfFlight(),"dd.MM.yyyy") +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getOperatingCarrierDesignator() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getFlightNumber() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        boardingPassModel.getSeatNumber();
            }else {
                pnrCode = boardingPassModel.getPnrCode();
            }
        }else if(Boolean.TRUE.equals(couldNotReadBoardingPass) && Objects.nonNull(djpAccessBoardingModel)) {
            if (passengerName.length() > 20) {
                pnrCode = passengerName.substring(0, 20) +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getCompartmentCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getFromCityAirportCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getToCityAirportCode() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        DateUtil.toDatePattern(djpAccessBoardingModel.getDateOfFlight(), "dd.MM.yyyy") +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getOperatingCarrierDesignator() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getFlightNumber() +
                        DjpCoreConstants.SpecialCharacters.DIVISION_MARK +
                        djpAccessBoardingModel.getSeatNumber();
            } else {
                pnrCode = djpAccessBoardingModel.getPnrCode();
            }
        }else {
            pnrCode = Objects.nonNull(boardingPassModel) ? boardingPassModel.getPnrCode() : djpAccessBoardingModel.getPnrCode();
        }
        return pnrCode;
    }

    private HttpPost createPostRequestForGlobalBilgi(JSONObject requestObject, String url){
        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = new StringEntity(requestObject.toString(), ContentType.APPLICATION_JSON);
        httpPost.setEntity(entity);
        return httpPost;
    }

    private void fillAndSetStatusMiddleWare(DjpBoardingPassModel boardingPassModel, DjpAccessBoardingModel djpAccessBoardingModel, QrType whereUsed, DjpMiddleWareBoardingPassStatusModel statusModel, String result) {
        String dbResult=result;
        if(org.springframework.util.StringUtils.hasText(result) && result.length()>255){
            dbResult=result.substring(0,254);
        }
        if (Objects.nonNull(boardingPassModel)) {
            statusModel.setStatusFromMiddleWare(dbResult);
            statusModel.setWhereUsed(whereUsed);
            modelService.saveOrUpdate(statusModel);
            boardingPassModel.setMiddleWareDetail(statusModel);
            modelService.saveOrUpdate(boardingPassModel);
        }
        if (Objects.nonNull(djpAccessBoardingModel)) {
            statusModel.setStatusFromMiddleWare(dbResult);
            statusModel.setWhereUsed(whereUsed);
            modelService.saveOrUpdate(statusModel);
            djpAccessBoardingModel.setMiddleWareDetail(statusModel);
            modelService.saveOrUpdate(djpAccessBoardingModel);
        }
    }
}
