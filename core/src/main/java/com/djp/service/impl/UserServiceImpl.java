package com.djp.service.impl;


import com.djp.core.dao.UserDao;
import com.djp.core.model.UserModel;
import com.djp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;

public class UserServiceImpl implements UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource
    private UserDao userDao;

    @Override
    public UserModel getUserForUID(String userUid) {
        return userDao.getUserForUID(userUid);
    }
}
