package com.djp.service.impl;


import com.djp.core.dao.DjpOrderDao;
import com.djp.core.dao.DjpRemainingUsageDao;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.modelservice.ModelService;
import com.djp.core.model.*;
import com.djp.service.BaseStoreService;
import com.djp.service.DjpRemainingUsageService;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.*;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.dao.DjpRemainingUsageDao;
import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.service.DjpCustomerService;
import com.djp.service.DjpRemainingUsageService;
import org.apache.commons.lang.StringUtils;

import jakarta.annotation.Resource;
import java.util.List;

public class DjpRemainingUsageServiceImpl implements DjpRemainingUsageService {

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private DjpOrderDao djpOrderDao;

    @Resource
    private DjpRemainingUsageDao djpRemainingUsageDao;

    @Resource
    private DjpCustomerService djpCustomerService;

    @Override
    public OrderModel getOrderByToken(String token) {
        BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        return djpOrderDao.getOrderByToken(token,currentBaseStore);
    }

    @Override
    public List<DjpRemainingUsageModel> getCustomerUsages(String customerUid) {
        return djpRemainingUsageDao.getCustomerUsages(customerUid);
    }

    @Override
    public List<DjpRemainingUsageModel> getCustomerUsages(String customerUid, String productCategory, String productType) {
        return djpRemainingUsageDao.getCustomerUsages(customerUid, productCategory, productType);
    }

    @Override
    public List<DjpRemainingUsageModel> getExternalCustomerUsages(String externalUid, String productCategory, String productType) {
        return djpRemainingUsageDao.getExternalCustomerUsages(externalUid, productCategory, productType);
    }

    @Override
    public List<DjpRemainingUsageModel> getDjpRemainingUsageForOrderCode(String orderCode) {
        return djpRemainingUsageDao.getDjpRemainingUsageForOrderCode(orderCode);
    }

    @Override
    public List<DjpRemainingUsageModel> getDailyPackageProducts(String customerUid, String orderCode, String packageProductCode, Date date,Boolean isExternalCustomer) {
        return djpRemainingUsageDao.getDailyPackageProducts(customerUid,orderCode,packageProductCode,date,isExternalCustomer);
    }
    @Override
    public String getPackageWithoutParkingByUid(String uid) {
        if(!DjpCoreConstants.ANONYMOUS.equals(uid) && StringUtils.isNotBlank(uid)) {
            CustomerModel customerByUid = djpCustomerService.findCustomerByUid(uid);
            if (Objects.isNull(customerByUid)) {
                return StringUtils.EMPTY;
            } else {
                List<DjpRemainingUsageModel> djpRemainingUsageForPackageProductByUser = djpRemainingUsageDao.findDjpRemainingUsageForPackageProductByUser(customerByUid);
                boolean hasCarParking = djpRemainingUsageForPackageProductByUser
                        .stream()
                        .anyMatch(remainingUsageModel -> remainingUsageModel
                                .getProduct()
                                .getSupercategories()
                                .stream()
                                .anyMatch(categoryModel -> DjpCoreConstants.CAR_PARK_CATEGORY.equals(categoryModel.getCode())));
                return Boolean.FALSE.equals(hasCarParking) ? djpRemainingUsageForPackageProductByUser.iterator().next().getPackagedProduct().getName() : StringUtils.EMPTY;
            }
        }else {
            return StringUtils.EMPTY;
        }
    }

    @Override
    public void startDjpUsageSendToErpProcess(String whereUsed, ItemModel qr, ItemModel boarding, String boardingType, String sapOrderID, Double paidPrice) {
        String processID = null;
        // TODO: id or pk not exist in item model
//        if(Objects.nonNull(qr)){
//            processID = qr.getPk().toString();
//        }
//        if(Objects.nonNull(boarding)){
//            processID = boarding.getPk().toString();
//        }
        // TODO: business process service
//        final DjpUsageSendToErpProcessModel djpUsedSendToErpProcess = businessProcessService.createProcess(
//                "djpUsedSendToErpProcess-" + processID + System.currentTimeMillis(),
//                "djpUsedSendToErpProcess");
//        if(StringUtils.isNotBlank(whereUsed)) {
//            QrType qrUsedEnum = QrType.valueOf(qrTypeEnumHashMap.get(whereUsed));
//            djpUsedSendToErpProcess.setWhereUsed(qrUsedEnum);
//        }
//        djpUsedSendToErpProcess.setQr(qr);
//        djpUsedSendToErpProcess.setBoarding(boarding);
//        djpUsedSendToErpProcess.setBoardingType(boardingType);
//        djpUsedSendToErpProcess.setSapOrderID(sapOrderID);
//        djpUsedSendToErpProcess.setPaidPrice(paidPrice);
//        modelService.saveOrUpdate(djpUsedSendToErpProcess);
//        businessProcessService.startProcess(djpUsedSendToErpProcess);
    }

    @Override
    public void startDjpUsedSendToExternalCompanyProcess(ItemModel qr) {
        // TODO: need information
//        final DjpUsageSendToExternalCompanyProcessModel djpUsageSendToExternalCompanyProcessModel = businessProcessService.createProcess(
//                "djpUsedSendToExternalCompanyProcess-" + System.currentTimeMillis(),
//                "djpUsedSendToExternalCompanyProcess");
//        djpUsageSendToExternalCompanyProcessModel.setQr(qr);
//        modelService.saveOrUpdate(djpUsageSendToExternalCompanyProcessModel);
//        businessProcessService.startProcess(djpUsageSendToExternalCompanyProcessModel);
    }

    @Override
    public DjpRemainingUsageModel getDjpRemainingByExternal(String customerId, String orderCode) {
        return djpRemainingUsageDao.getDjpRemainingByExternal(customerId,orderCode);
    }
}
