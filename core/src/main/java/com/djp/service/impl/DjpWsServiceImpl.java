package com.djp.service.impl;

import com.djp.data.login.CheckLoginRequest;
import com.djp.data.login.CheckLoginResponse;
import com.djp.dto.UserWsDto;
import com.djp.request.QrForFioriRequest;
import com.djp.data.login.ExternalAccessToken;
import com.djp.dto.UserWsDto;
import com.djp.request.QrForFioriRequest;
import com.djp.response.UserDetailRequest;
import com.djp.response.UserDetailResponse;
import com.djp.service.DjpTokenService;
import com.djp.service.DjpWsService;
import com.djp.user.CurrentUser;
import com.djp.util.ProjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;

public class DjpWsServiceImpl implements DjpWsService {

    private static final Logger LOG = LoggerFactory.getLogger(DjpWsServiceImpl.class);
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "bearer ";

    @Value("${external.authurl}")
    private String authUrl;

    @Value("${external.auth.clientid}")
    private String clientid;

    @Value("${external.auth.client_secret}")
    private String clientsecret;

    @Value("${external.auth.grant_type}")
    private String granttype;

    @Value("${external.service.getUserDetailsUrl}")
    private String getUserDetailsUrl;
    @Resource
    private RestTemplate airportServicesRestTemplate;

    @Value("${external.server.rest.host}/djpstore/userController/loginCheck")
    private String checkLoginUrl;
    @Resource
    private DjpTokenService djpTokenService;

    @Override
    public UserWsDto getOrderList(QrForFioriRequest orderListRequest) {
        try {
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        } catch (Exception e) {
            //LOG.error("ERR >>>> getOrderList() : " + airportProjectUtil.convertToJsonString(getOrderListRequest), e);
        }
        return null;
    }
    @Resource
    private ProjectUtil projectUtil;
    @Override
    public CheckLoginResponse checkLogin(String username, String password) {
        LOG.info("----------------------------------------------------------------------------------------------------\n\n");
        LOG.info("START >>>> checkLogin() : Username = " + username);
        try {
            final CheckLoginRequest checkLoginRequest = new CheckLoginRequest();
            checkLoginRequest.setPassword(password);
            checkLoginRequest.setUserName(username);
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

            headers.add(CONTENT_TYPE, APPLICATION_JSON);
            headers.add(AUTHORIZATION, djpTokenService.getAuthToken());

            final HttpEntity<CheckLoginRequest> postEntity = new HttpEntity<>(checkLoginRequest, headers);
            ResponseEntity<CheckLoginResponse> postResponse = airportServicesRestTemplate.postForEntity(checkLoginUrl, postEntity, CheckLoginResponse.class);
           // LOG.info("SUCC >>>> checkLogin() response : " + projectUtil.convertToJsonString(postResponse.getBody()) + "\n");
            LOG.info("----------------------------------------------------------------------------------------------------\n\n");
            return postResponse.getBody();
        } catch (Exception e) {
            LOG.error("ERR >>>> checkLogin()  : Username = " + username, e);
            LOG.info("----------------------------------------------------------------------------------------------------\n\n");
            return null;
        }
    }

    @Override
    public CurrentUser getUserDetails(String name) {
        LOG.info("----------------------------------------------------------------------------------------------------\n\n");
        LOG.info("START >>>> getUserDetails() request ");
        try {
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

            headers.add(CONTENT_TYPE, APPLICATION_JSON);
            headers.add(AUTHORIZATION, BEARER + getAuthorizationToken());
            headers.add("lang", "tr");

            UserDetailRequest userDetailRequest = new UserDetailRequest();
            userDetailRequest.setUserUid(name);

            final HttpEntity<UserDetailRequest> postEntity = new HttpEntity<>(userDetailRequest, headers);
            ResponseEntity<UserDetailResponse> postResponse = airportServicesRestTemplate.postForEntity(getUserDetailsUrl, postEntity, UserDetailResponse.class);
            LOG.info("SUCC >>>> getUserDetails() response : " + projectUtil.convertToJsonString(postResponse.getBody()) + "\n");
            LOG.info("----------------------------------------------------------------------------------------------------\n\n");
            return postResponse.getBody().getUserDetail();
        } catch (Exception e) {
            LOG.error("ERR >>>> getUserDetails() : ", e);
            LOG.info("----------------------------------------------------------------------------------------------------\n\n");
            return new CurrentUser();
        }
    }
    @Override
    public String getAuthorizationToken() throws Exception {
        try {
            final MultiValueMap<String, String> postHeadersMap = new LinkedMultiValueMap<>();
            postHeadersMap.add("client_id", clientid);
            postHeadersMap.add("client_secret", clientsecret);
            postHeadersMap.add("grant_type", granttype);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            final HttpEntity<MultiValueMap<String, String>> posEntity = new HttpEntity<>(postHeadersMap, headers);
            ResponseEntity<ExternalAccessToken> postResponse = airportServicesRestTemplate.postForEntity(authUrl, posEntity, ExternalAccessToken.class);
            return postResponse.getBody().getAccessToken();

        } catch (Exception e) {
            LOG.error("ERR >>>>getAuthorizationToken() : ", e);
            return "";
        }
    }
    @PostConstruct
    public void disableSSL() {
       /* try {
            final SSLContext sslc = SSLContext.getInstance("TLS");
            final TrustManager trustAllManager = new TrustAllManager();
            sslc.init(null, org.apache.commons.lang3.ArrayUtils.toArray(trustAllManager), null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new TrustAllHostnameVerifier());
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

}
