package com.djp.service.impl;

import com.djp.core.dao.DjpFlightInformationDao;
import com.djp.core.model.FlightInformationModel;
import com.djp.service.DjpFioriFlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.util.Date;

public class DjpFioriFlightServiceImpl implements DjpFioriFlightService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpFioriFlightServiceImpl.class);

    @Resource
    private DjpFlightInformationDao djpFlightInformationDao;

    @Override
    public FlightInformationModel getFlightInformation(Date flightDate, String flightNumber) {
        return djpFlightInformationDao.getFlightInformation(flightDate, flightNumber);
    }
}
