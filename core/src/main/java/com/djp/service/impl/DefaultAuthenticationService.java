package com.djp.service.impl;



import java.time.Instant;

import com.djp.core.model.UserModel;
import com.djp.service.AuthenticationService;
import org.apache.http.auth.InvalidCredentialsException;

public class DefaultAuthenticationService implements AuthenticationService {

    public UserModel checkCredentials(String var1, String var2) throws InvalidCredentialsException{
        return null;
    }

    public UserModel login(String var1, String var2) throws InvalidCredentialsException{
        return null;
    }

    public void logout(){
        //
    }
}
   /*
    public static final String LOGIN_ANONYMOUS_ALWAYS_DISABLED = "login.anonymous.always.disabled";
    private transient UserService userService;
    private transient PasswordEncoderService passwordEncoderService;

    public DefaultAuthenticationService() {
    }

    public UserModel login(String login, String password) throws InvalidCredentialsException {
        UserModel user = this.checkCredentials(login, password);
        this.userService.setCurrentUser(user);
        return user;
    }

    public UserModel checkCredentials(String login, String password) throws InvalidCredentialsException {
        UserModel user = null;

        try {
            user = this.userService.getUserForUID(login);
            if (this.checkCustomerAnonymousAndAnonymousLoginDisable(user)) {
                throw this.buildInvalidCredentialsException("Anonymous login is disabled");
            } else if (user.isLoginDisabled() || user.getDeactivationDate() != null && user.getDeactivationDate().toInstant().isBefore(Instant.now())) {
                throw this.buildInvalidCredentialsException();
            } else if (!this.passwordEncoderService.isValid(user, password)) {
                throw this.buildInvalidCredentialsException();
            } else {
                return user;
            }
        } catch (UnknownIdentifierException var4) {
            throw this.buildInvalidCredentialsException();
        } catch (CannotDecodePasswordException var5) {
            throw this.buildInvalidCredentialsException();
        } catch (PasswordEncoderNotFoundException var6) {
            throw this.buildInvalidCredentialsException();
        }
    }

    public void logout() {
        this.getSessionService().closeCurrentSession();
    }

    protected InvalidCredentialsException buildInvalidCredentialsException() {
        return this.buildInvalidCredentialsException("invalid credentials");
    }

    protected InvalidCredentialsException buildInvalidCredentialsException(String message) {
        return new InvalidCredentialsException(message);
    }

    private boolean checkCustomerAnonymousAndAnonymousLoginDisable(UserModel user) {
        return user instanceof CustomerModel && this.userService.isAnonymousUser(user) && Config.getBoolean("login.anonymous.always.disabled", true);
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setPasswordEncoderService(PasswordEncoderService passwordEncoderService) {
        this.passwordEncoderService = passwordEncoderService;
    }
}
*/