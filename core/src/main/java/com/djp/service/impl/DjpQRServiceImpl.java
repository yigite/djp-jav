package com.djp.service.impl;


import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.dao.DjpQRDao;
import com.djp.core.model.*;
import com.djp.facades.impl.DjpQRFacadeImpl;
import com.djp.modelservice.ModelService;
import com.djp.core.enums.QrType;
import com.djp.core.model.DjpContractedTicketsModel;
import com.djp.core.model.UnapprovedPassModel;
import com.djp.response.FioriStatusResponse;
import com.djp.service.DjpQRService;
import com.djp.service.DjpTokenService;

import jakarta.annotation.Resource;
import java.util.*;

import com.djp.service.UserService;
import org.apache.commons.lang.StringUtils;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DjpQRServiceImpl implements DjpQRService {
    private static final Logger LOG = LoggerFactory.getLogger(DjpQRServiceImpl.class);

    @Resource
    private ModelService modelService;

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    private DjpQRDao djpQRDao;

    @Resource
    private UserService userService;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Override
    public QrForFioriModel generateQrForFiori(String useruid, CustomerModel customerModel, String orderCode) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        QrForFioriModel qrForFioriModel = new QrForFioriModel();
        DjpUniqueTokenModel djpUniqueTokenModel = djpTokenService.generateToken(useruid, orderCode, DjpQRFacadeImpl.TOKEN_TYPE_LOGIN, c.getTime());
        qrForFioriModel.setCustomer(customerModel);
        qrForFioriModel.setTokenKey(djpUniqueTokenModel);
        modelService.saveOrUpdate(qrForFioriModel);
        return qrForFioriModel;
    }

    @Override
    public QrForMobileModel validQrCodeForMobile(String tokenKey) {
        return djpQRDao.getQRForMobileByTokenKey(tokenKey);
    }

    @Override
    public QrForVoucherModel validQrCodeForDjp(String tokenKey) {
        return djpQRDao.getQRForDjpByTokenKey(tokenKey);
    }

    @Override
    public QrForFioriModel validQrCodeForFiori(String tokenKey) {
        return djpQRDao.getQRForFioriByTokenKey(tokenKey);
    }

    @Override
    public QrForBankModel validQrCodeForBank(String tokenKey) {
        return djpQRDao.getQRForBankByTokenKey(tokenKey);
    }

    @Override
    public QrForMeetModel getQRForMeetByTokenKey(String tokenKey) {
        return djpQRDao.getQRForMeetByTokenKey(tokenKey);
    }

    @Override
    public QrForCabinetModel getQrForCabinet(String tokenKey){
        return djpQRDao.getQrForCabinet(tokenKey);
    }

    @Override
    public QrForExperienceCenterModel getQrForExperienceCenter(String tokenKey) {
        return djpQRDao.getQRForExperienceCenterByTokenKey(tokenKey);
    }

    @Override
    public QrForCardModel getQRForCardByTokenKey(String tokenKey) {
        return djpQRDao.getQRForCardByTokenKey(tokenKey);
    }

    @Override
    public List<GateNumberModel> getGateNumbers() {
        return djpQRDao.getGates();
    }


    @Override
    public void saveUnapprovedPass(String tokenKey, String whyIsThat, String whereUsed, String whichTable) {
        try {
            UnapprovedPassModel unapprovedPassModel = new UnapprovedPassModel();
            unapprovedPassModel.setTokenKey(tokenKey);
            unapprovedPassModel.setWhichTable(whichTable);
            unapprovedPassModel.setWhyIsThat(whyIsThat);
            unapprovedPassModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));
            unapprovedPassModel.setUsageDate(new Date());
            modelService.saveOrUpdate(unapprovedPassModel);
        } catch (Exception e) {
            LOG.error("has error djpQRServiceImpl::saveUnapprovedPass:" + e);
        }
    }
    @Override
    public DjpContractedTicketsModel getContractedTicketByIDAndContractedService(String uniqueID, String wasWhereUsed) {
        List<DjpContractedTicketsModel> contractedTicketsByID = djpQRDao.getContractedTicketByID(uniqueID);
        DjpContractedTicketsModel djpContractedTicketsModel = null;
        if (contractedTicketsByID.isEmpty()) {
            return djpContractedTicketsModel;
        } else {
            Optional<DjpContractedTicketsModel> optionalForTicket = contractedTicketsByID.stream().filter(djpContractedTicket -> qrTypeEnumHashMap.get(wasWhereUsed).startsWith(djpContractedTicket.getContractedService().getCode())).findFirst();
            if (optionalForTicket.isPresent()) {
                djpContractedTicketsModel = optionalForTicket.get();
            }
            return djpContractedTicketsModel;
        }
    }

    @Override
    public void saveCardsUsedInServices(CardsUsedInServicesModel cardsUsedInServicesModel, String whereUsed, FioriStatusResponse statusResponse, QrForCardModel qrForCard, DjpPassLocationsModel djpPassLocation, String personalId, String personalName) {
        try {
            cardsUsedInServicesModel.setCardId(qrForCard.getCardId());
            cardsUsedInServicesModel.setCompanyId(qrForCard.getCompanyId());
            cardsUsedInServicesModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));
            cardsUsedInServicesModel.setUsageDate(new Date());
            DjpUniqueTokenModel token = qrForCard.getToken();
            if(Objects.isNull(token)){
                token = djpTokenService.generateTokenForDjp(qrForCard.getCardId(), DjpCommerceservicesConstants.QR_FOR_CARD);
                modelService.saveOrUpdate(token);
            }
            cardsUsedInServicesModel.setToken(token);
            if(cardsUsedInServicesModel.getToken() != null){
                EmployeeModel employeeModel = null;
                try {
                    employeeModel = (EmployeeModel) userService.getUserForUID(personalId);
                    cardsUsedInServicesModel.getToken().setPersonalID(StringUtils.isNotBlank(personalId) ? personalId : null);
                    cardsUsedInServicesModel.getToken().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                } catch (Exception e){
                    LOG.error("Cannot find user with uid : " + personalId);
                }
                modelService.saveOrUpdate(cardsUsedInServicesModel.getToken());
            }
            cardsUsedInServicesModel.setWhichLocation(djpPassLocation);
            modelService.saveOrUpdate(cardsUsedInServicesModel);
            qrForCard.setToken(null);
            modelService.saveOrUpdate(qrForCard);
            statusResponse.setMessage("Hizmet Kullandırıldı.");
            statusResponse.setType("S");
        } catch (Exception e) {
            statusResponse.setMessage("Hizmet Kullandırılarken Hata Oluştu.");
            statusResponse.setType("E");
            LOG.error("has error IgaQRServiceImpl::saveCardsUsedInServices:" + e);
        }
    }

    @Override
    public DjpVoucherCodeUsagesModel getCampaignCodeUsageBySapOrderIDAndVoucher(String sapOrderID, String qrKey) {
        return djpQRDao.findCampaignCodeUsageBySapOrderIDAndVoucher(sapOrderID, qrKey);
    }

    @Override
    public List<DjpExternalCompanyUsagesModel> getQrExternalCompanyUsagesList(String qrToken) {
        return djpQRDao.getQrExternalCompanyUsagesList(qrToken);
    }
    @Override
    public QrForExternalModel getQRForExternalByTokenKey(String tokenKey) {
        return djpQRDao.getQRForExternalByTokenKey(tokenKey);

    }

    @Override
    public ContractedCompaniesModel getContractedCompany(String companyId, String contractedService, DjpPassLocationsModel djpPassLocation) {
        try {
            if (StringUtils.isBlank(companyId)) {
                LOG.error("company id is blank for getContractedCompany");
                return null;
            } else {
                companyId = companyId.contains("_") ? companyId.split("_")[0] : companyId;
                QrType contractedServiceEnum = QrType.valueOf(qrTypeEnumHashMap.get(contractedService));
                ContractedCompaniesModel contractedCompany = djpQRDao.getContractedCompany(companyId, contractedServiceEnum, djpPassLocation);// this search for contractedService
                if (Objects.nonNull(contractedCompany)) {
                    return contractedCompany;
                } else {
                    contractedServiceEnum = QrType.valueOf(qrTypeEnumHashMap.get(contractedService.substring(0, 2))); // this search for parent contractedService
                    contractedCompany = djpQRDao.getContractedCompany(companyId, contractedServiceEnum, djpPassLocation);
                    if (Objects.nonNull(contractedCompany)) {
                        return contractedCompany;
                    } else {
                        if (contractedService.length() >= 4) {
                            contractedServiceEnum = QrType.valueOf(qrTypeEnumHashMap.get(contractedService.substring(0, 4))); // this search for parent contractedService
                            contractedCompany = djpQRDao.getContractedCompany(companyId, contractedServiceEnum, djpPassLocation);
                            if (Objects.nonNull(contractedCompany)) {
                                return contractedCompany;
                            } else {
                                contractedCompany = djpQRDao.getContractedCompany(companyId, QrType.ALL, djpPassLocation); // this search for ALL TYPE
                                return contractedCompany;
                            }
                        } else {
                            contractedCompany = djpQRDao.getContractedCompany(companyId, QrType.ALL, djpPassLocation); // this search for ALL TYPE
                            return contractedCompany;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("has error DjpQRServiceImpl::getContractedCompany:" + e);
            return null;
        }
    }

    @Override
    public QrForCardModel getQRForCardByPhoneNumber(String phoneNumber) {
        return  djpQRDao.getQRForCardsByPhoneNumber(phoneNumber);
    }
}
