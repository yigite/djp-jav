package com.djp.service.impl;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.dao.DjpCppDao;
import com.djp.core.model.AddressModel;
import com.djp.core.model.DjpCppOwnerModel;
import com.djp.core.model.DjpCppTxnModel;
import com.djp.data.DjpCppTxnData;
import com.djp.data.DjpCppTxnInvoiceData;
import com.djp.modelservice.ModelService;
import com.djp.service.DjpCppService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.util.Objects;

public class DjpCppServiceImpl implements DjpCppService {
    private static final Logger LOG =  LoggerFactory.getLogger(DjpCppServiceImpl.class);
    @Resource
    private DjpCppDao djpCppDao;

    @Resource
    private ModelService modelService;

    @Override
    public String initPayment(DjpCppTxnModel djpCppTxnModel, DjpCppTxnData djpCppTxnData) {
        setInvoiceData(djpCppTxnModel,djpCppTxnData);
        modelService.saveOrUpdate(djpCppTxnModel);
        logCppData(djpCppTxnModel);
        return djpCppTxnModel.getCppToken();
    }

    @Override
    public DjpCppTxnModel getDjpCppTxnDataByToken(String djpCppToken) {
        return djpCppDao.getDjpCppTxnDataByToken(djpCppToken);
    }

    public void setInvoiceData(DjpCppTxnModel djpCppTxnModel,DjpCppTxnData djpCppTxnData){
        if(Objects.nonNull(djpCppTxnData.getInvoiceAddress()) && DjpCoreConstants.DjpSapReturns.ERP.equals(djpCppTxnData.getLocation())){
            AddressModel addressModel = null;
            DjpCppTxnInvoiceData invoiceAddress = djpCppTxnData.getInvoiceAddress();
            if(StringUtils.isNotBlank(invoiceAddress.getCompanyID())){
                DjpCppOwnerModel djpCppCompanyByCompanyID = djpCppDao.getDjpCppOwnerByID(invoiceAddress.getCompanyID());
                if(Objects.nonNull(djpCppCompanyByCompanyID)){
                    addressModel = generateOrUpdateInvoiceAddressAndSave(djpCppTxnModel, invoiceAddress, djpCppCompanyByCompanyID.getPaymentAddress());
                    djpCppTxnModel.setOwner(djpCppCompanyByCompanyID);
                    addressModel.setOwner(djpCppTxnModel);
                    djpCppCompanyByCompanyID.setPaymentAddress(addressModel);
                    modelService.saveOrUpdate(addressModel);
                    modelService.saveOrUpdate(djpCppTxnModel);
                    modelService.saveOrUpdate(djpCppCompanyByCompanyID);
                }else {
                    djpCppCompanyByCompanyID = createDjpCppOwner(invoiceAddress);
                    addressModel = generateOrUpdateInvoiceAddressAndSave(djpCppTxnModel, invoiceAddress, djpCppCompanyByCompanyID.getPaymentAddress());
                    addressModel.setOwner(djpCppTxnModel);
                    djpCppCompanyByCompanyID.setPaymentAddress(addressModel);
                    djpCppTxnModel.setOwner(djpCppCompanyByCompanyID);
                    modelService.saveOrUpdate(addressModel);
                    modelService.saveOrUpdate(djpCppTxnModel);
                    modelService.saveOrUpdate(djpCppCompanyByCompanyID);
                }
            }
        }
    }
    private AddressModel generateOrUpdateInvoiceAddressAndSave(DjpCppTxnModel igaCppTxnModel, DjpCppTxnInvoiceData invoiceAddressData,AddressModel addressBySapCompany) {
        AddressModel addressByTxn = null;
        if(Objects.isNull(addressBySapCompany)) {
            addressByTxn = new AddressModel();
        }else {
            addressByTxn = modelService.clone(addressBySapCompany);
        }
        addressByTxn.setCompany(invoiceAddressData.getName());
        addressByTxn.setTaxNumber(invoiceAddressData.getIdentityNumber());
        addressByTxn.setTaxOffice(invoiceAddressData.getTaxOffice());
        addressByTxn.setEmail(invoiceAddressData.getEmail());
        addressByTxn.setLine1(invoiceAddressData.getAddress());
        addressByTxn.setPhone1(invoiceAddressData.getPhoneNumber());
        igaCppTxnModel.setPaymentAddress(addressByTxn);
        return addressByTxn;
    }
    private DjpCppOwnerModel createDjpCppOwner(DjpCppTxnInvoiceData invoiceAddressData) {
        DjpCppOwnerModel newOwner = new DjpCppOwnerModel();
        String companyID = invoiceAddressData.getCompanyID();
        newOwner.setId(companyID);
        return newOwner;
    }
    private void logCppData(DjpCppTxnModel djpCppTxnModel){
        LOG.info("Create Info For Cpp Token :: " +
                " CPP TOKEN : "+ djpCppTxnModel.getCppToken() +
                " AMOUNT : "+ djpCppTxnModel.getAmount() +
                " STATUS : GENERATED");
    }
}
