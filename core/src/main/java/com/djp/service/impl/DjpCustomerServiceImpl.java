package com.djp.service.impl;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.dao.CustomerDao;
import com.djp.core.dao.DjpRemainingUsageDao;
import com.djp.core.model.CustomerModel;
import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.modelservice.ModelService;
import com.djp.service.DjpCustomerService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.Objects;
public class DjpCustomerServiceImpl implements DjpCustomerService {

    private static final Logger LOG = LoggerFactory.getLogger(DjpCustomerServiceImpl.class);

    @Resource
    CustomerDao customerDao;

    @Resource
    DjpRemainingUsageDao djpRemainingUsageDao;

    @Resource
    ModelService modelService;

    @Override
    public CustomerModel findCustomerByUid(String uid) {
        return customerDao.findCustomerByUid(uid);
    }

    @Override
    public Boolean checkHaveDjpPass(String uid) {
        try {
            if(!DjpCoreConstants.ANONYMOUS.equals(uid) && StringUtils.isNotBlank(uid)) {
                CustomerModel customerByUid = findCustomerByUid(uid);
                if (Objects.isNull(customerByUid)) {
                    return false;
                } else {
                    List<DjpRemainingUsageModel> djpRemainingUsageForPackageProductByUser = djpRemainingUsageDao.findDjpRemainingUsageForPackageProductByUser(customerByUid);
                    customerByUid.setHaveDjpPass(!djpRemainingUsageForPackageProductByUser.isEmpty());
                    modelService.saveOrUpdate(customerByUid);
                    return !djpRemainingUsageForPackageProductByUser.isEmpty();
                }
            }else {
                return false;
            }
        }catch (Exception e){
            LOG.error("has an error checkHaveDjpPass:"+uid+": ",e);
            return false;
        }
    }
}
