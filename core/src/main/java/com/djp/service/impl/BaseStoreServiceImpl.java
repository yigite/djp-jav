package com.djp.service.impl;


import com.djp.core.dao.BaseStoreDao;
import com.djp.core.model.BaseStoreModel;
import com.djp.service.BaseStoreService;
import com.djp.strategy.BaseStoreSelectorStrategy;
import org.apache.commons.collections.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.Iterator;
import java.util.List;

public class BaseStoreServiceImpl implements BaseStoreService {

    @Resource
    private BaseStoreDao baseStoreDao;

    private List<BaseStoreSelectorStrategy> baseStoreSelectorStrategies;

    public BaseStoreModel getCurrentBaseStore() {
        BaseStoreModel result = null;
        if (!CollectionUtils.isEmpty(this.baseStoreSelectorStrategies)) {
            Iterator var3 = this.baseStoreSelectorStrategies.iterator();

            while(var3.hasNext()) {
                BaseStoreSelectorStrategy strategy = (BaseStoreSelectorStrategy)var3.next();
                result = strategy.getCurrentBaseStore();
                if (result != null) {
                    break;
                }
            }
        }

        return result;
    }
}
