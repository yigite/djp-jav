package com.djp.service;

import com.djp.core.model.CustomerModel;

public interface DjpCustomerService {

    CustomerModel findCustomerByUid(String uid);

    Boolean checkHaveDjpPass(String uid);
}
