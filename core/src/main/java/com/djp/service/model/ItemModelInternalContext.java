package com.djp.service.model;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import java.io.ObjectStreamException;
import java.util.Locale;

public interface ItemModelInternalContext  {
    <T> T getValue(String var1, T var2);

    <T> T setValue(String var1, T var2);

    <T> T getLocalizedValue(String var1, Locale var2);

    <T> T getLocalizedRelationValue(String var1, Locale var2);

    <T> void setLocalizedValue(String var1, Locale var2, T var3);



    Object writeReplace(Object var1) throws ObjectStreamException;


    <T> T getPropertyValue(String var1);

    <T> void setPropertyValue(String var1, T var2);

    boolean isDynamicAttribute(String var1);

    Object loadOriginalValue(String var1);

    Object loadOriginalValue(String var1, Locale var2);
}
