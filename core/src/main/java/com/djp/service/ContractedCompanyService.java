package com.djp.service;

import com.djp.core.model.ContractedCompaniesModel;

public interface ContractedCompanyService {

    ContractedCompaniesModel getContractedCompany(String contractID);
}
