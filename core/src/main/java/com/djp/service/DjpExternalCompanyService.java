package com.djp.service;

import com.djp.core.model.DjpExternalCompanyOrderTableModel;
import com.djp.core.model.DjpExternalCustomerModel;

public interface DjpExternalCompanyService {

    DjpExternalCustomerModel findDjpExternalCustomerByID(String djpExternalCustomerID, String djpExternalCompanyClientID);
    DjpExternalCompanyOrderTableModel findDjpExternalOrderByIdAndCustomer(String orderCode, String customerID);

}
