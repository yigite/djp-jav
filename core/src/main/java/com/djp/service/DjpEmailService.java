package com.djp.service;

public interface DjpEmailService {
    void sendQrToCompanyErrorInfoMail(String uniqueId,String whereUsed,String companyClientID);

}
