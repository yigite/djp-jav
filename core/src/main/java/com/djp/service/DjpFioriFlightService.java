package com.djp.service;

import com.djp.core.model.FlightInformationModel;

import java.util.Date;

public interface DjpFioriFlightService {
    FlightInformationModel getFlightInformation(final Date flightDate, final String flightNumber);
}
