package com.djp.service;

import com.djp.core.model.DjpAccessRightsModel;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.core.model.DjpUniqueTokenModel;

import java.util.Date;
import java.util.List;

public interface    DjpTokenService {

    DjpUniqueTokenModel generateToken(String customerUid, String orderCode, String type, Date expireDate);

    DjpPassLocationsModel getDjpPassLocationByLocationID(String locationID);

    String getAuthToken();

    DjpUniqueTokenModel encodeCustomerFromToken(String token);

    List<String> getMembershipCodes();

    DjpUniqueTokenModel generateTokenForDjp(String refNumber, String type);

    List<DjpAccessRightsModel> getAccessRights(String airport, DjpPassLocationsModel djpPassLocation, Boolean isServiceUsageWithMembership);

    DjpAccessRightsModel getAccessRightsByAirlineCodeAndClassName(String airport,String className,DjpPassLocationsModel igaPassLocation);

}
