package com.djp.service;


import com.djp.core.model.*;
import com.djp.response.FioriStatusResponse;

import java.util.List;

public interface DjpQRService {

    QrForFioriModel generateQrForFiori(String useruid, CustomerModel customerModel, String orderCode);

    QrForMobileModel validQrCodeForMobile(String tokenKey);

    QrForVoucherModel validQrCodeForDjp(String tokenKey);

    QrForFioriModel validQrCodeForFiori(String tokenKey);

    QrForBankModel validQrCodeForBank(String tokenKey);

    QrForMeetModel getQRForMeetByTokenKey(String tokenKey);

    QrForCabinetModel getQrForCabinet(String tokenKey);

    QrForExperienceCenterModel getQrForExperienceCenter(String tokenKey);

    QrForCardModel getQRForCardByTokenKey(String tokenKey);

    List<GateNumberModel> getGateNumbers();

    void saveUnapprovedPass(String tokenKey,String whyIsThat,String whereUsed,String whichTable);

    DjpContractedTicketsModel getContractedTicketByIDAndContractedService(String uniqueID, String wasWhereUsed);

    void saveCardsUsedInServices(CardsUsedInServicesModel cardsUsedInServicesModel, String whereUsed, FioriStatusResponse statusResponse, QrForCardModel qrForCard, DjpPassLocationsModel igaPassLocation, String personalId, String personalName);

    DjpVoucherCodeUsagesModel getCampaignCodeUsageBySapOrderIDAndVoucher(String sapOrderID,String qrKey);

    QrForExternalModel getQRForExternalByTokenKey(String tokenKey);

    ContractedCompaniesModel getContractedCompany(String companyId, String contractedService,DjpPassLocationsModel djpPassLocation);

    List<DjpExternalCompanyUsagesModel> getQrExternalCompanyUsagesList(String tokenKey);

    QrForCardModel getQRForCardByPhoneNumber(String phoneNumber);
}
