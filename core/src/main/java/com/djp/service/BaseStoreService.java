package com.djp.service;


import com.djp.core.model.BaseStoreModel;

public interface BaseStoreService {
    BaseStoreModel getCurrentBaseStore();
}
