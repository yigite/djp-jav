package com.djp.service;


import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.core.model.ItemModel;
import com.djp.core.model.OrderModel;

import java.util.Date;
import java.util.List;

public interface DjpRemainingUsageService {

    OrderModel getOrderByToken(String token);

    List<DjpRemainingUsageModel> getCustomerUsages(String customerUid);

    List<DjpRemainingUsageModel> getCustomerUsages(String customerUid, String productCategory, String productType);

    List<DjpRemainingUsageModel> getExternalCustomerUsages(String externalUid, String productCategory, String productType);

    List<DjpRemainingUsageModel> getDjpRemainingUsageForOrderCode(String orderCode);

    List<DjpRemainingUsageModel> getDailyPackageProducts(String customerUid, String orderCode, String packageProductCode, Date date, Boolean isExternalCustomer);

    String getPackageWithoutParkingByUid(String uid);

    void startDjpUsageSendToErpProcess(String whereUsed, ItemModel qr, ItemModel boarding, String boardingType, String sapOrderID, Double paidPrice);

    void startDjpUsedSendToExternalCompanyProcess(ItemModel qr);

    DjpRemainingUsageModel getDjpRemainingByExternal(String customerId, String orderCode);

}
