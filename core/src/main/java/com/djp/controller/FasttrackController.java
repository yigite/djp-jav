package com.djp.controller;

import com.djp.core.model.*;
import com.djp.data.FioriFlightDetailData;
import com.djp.data.PosReceiptData;
import com.djp.dto.UserWsDto;
import com.djp.facades.*;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;

import com.djp.facades.DjpFioriFlightFacade;
import com.djp.facades.DjpQRFacade;

import com.djp.request.BoardingPassRequest;
import com.djp.request.FioriFlightInfoRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.request.QrPassRequest;
import com.djp.request.UserUpdateRequest;
import com.djp.response.*;
import com.djp.rest.data.BoardingPassFlightDateRequest;
import com.djp.rest.data.BoardingPassFlightDateResponse;
import com.djp.rest.data.ServiceResultWsData;
import com.djp.rest.data.ContractedCompaniesResponse;
import com.djp.service.*;
import com.djp.util.DjpUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.djp.facades.DjpWsFacade;
import com.djp.util.DjpServiceUtil;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/fasttrack")
@Controller
public class FasttrackController {

    private static final Logger LOG = LoggerFactory.getLogger(FasttrackController.class);
    private static final String EXCEPTION_OCCURED="Sistemsel bir hata olustu.";

    @Resource
    private DjpWsFacade djpWsFacade;

    @Resource
    private DjpQRFacade djpQRFacade;

    @Resource
    private DjpFioriFlightFacade djpFioriFlightFacade;

    @Resource
    private DjpManuelRecordService djpManuelRecordService;

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private DjpQRService djpQRService;

    @Resource
    DjpFioriFlightService djpFioriFlightService;
    public UserWsDto boardingPass(UserUpdateRequest userUpdateRequest)throws Exception{

        UserWsDto userWsDto;
        try {
//            return airportWsService.updateCustomerByFioriSystem(userUpdateRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
        userWsDto=new UserWsDto();
        FioriStatusResponse fioriStatusResponse=new FioriStatusResponse();
        fioriStatusResponse.setType("E");
        fioriStatusResponse.setMessage(EXCEPTION_OCCURED);
        userWsDto.setResult(fioriStatusResponse);
        return userWsDto;
    }

    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/boardingpass", method = RequestMethod.POST)
    @ResponseBody
    public BoardingPassResponse boardingPassAction(@RequestBody BoardingPassRequest boardingPassRequest) throws JsonProcessingException, ParseException {
        return djpWsFacade.boardingPassAction(boardingPassRequest);
    }

    @Secured(
            {"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/getUserProductsForFiori", method = RequestMethod.POST)
    @ResponseBody
    public UserWsDto getUserProducts(@RequestBody QrForFioriRequest qrForFioriRequest) throws JsonProcessingException {
        return djpWsFacade.getOrderList(qrForFioriRequest);
    }

    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value = "/getFioriFlightInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FioriFlightDetailResponse getFioriFlightInfo(@RequestBody final FioriFlightInfoRequest fioriFlightInfoRequest){
        FioriStatusResponse status = new FioriStatusResponse();
        FioriFlightDetailResponse response = new FioriFlightDetailResponse();
        FioriFlightDetailData fioriFlightDetailData = djpFioriFlightFacade.getFioriFlightDetail(fioriFlightInfoRequest.getFlightDate(), fioriFlightInfoRequest.getFlightNumber());
        response.setFlightInfo(fioriFlightDetailData);
        response.setServiceResult(status);
        return response;
    }

    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @GetMapping(value = "/getManuelRecords")
    public ResponseEntity<ManuelRecordsResponse> getManuelRecords(@RequestParam String date, @RequestParam String whereUsed)
    {
        ManuelRecordsResponse response = new ManuelRecordsResponse();
        FioriStatusResponse serviceResult = new FioriStatusResponse();
        try {
            response.setManuelRecordsList(djpManuelRecordService.getManuelRecords(date,whereUsed));
            serviceResult.setType("S");
            response.setServiceResult(serviceResult);
            return ResponseEntity.ok(response);
        }catch (Exception ex){
            LOG.error("has an error getManuelRecords:",ex);
            serviceResult.setType("E");
            serviceResult.setMessage("getManuelRecords HATA OLUŞTU!");
            response.setServiceResult(serviceResult);
            return ResponseEntity.of(Optional.of(response));
        }
    }

    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/qrPass", method = RequestMethod.POST)
    @ResponseBody
    public FioriStatusResponse qrPass(@RequestBody QrPassRequest qrPassRequest, final HttpServletRequest request) throws JsonProcessingException, ParseException {
        return djpQRFacade.qrPass(qrPassRequest,request);
    }

    @GetMapping(value = "/gates",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GateNumberModel> getFasttrackGates(){
        return djpQRService.getGateNumbers();
    }


    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value = "/validateTokenForFiori", method = {RequestMethod.POST, RequestMethod.PUT})
    @ResponseBody
    public UserWsDto getUserByToken(@RequestParam final String fields, @RequestBody QrForFioriRequest qrForFioriRequest, final HttpServletRequest request){
        try {
            return djpWsFacade.getUserByToken(fields,qrForFioriRequest,request);
        }catch (Exception ex){
            LOG.error("has an error getUserByToken:",ex);
            return null;
        }
    }
    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value = "/getBoardingFlightInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BoardingPassFlightDateResponse getFioriFlightInfo(@RequestBody final BoardingPassFlightDateRequest boardingPassFlightDateRequest){
        BoardingPassFlightDateResponse boardingPassFlightDateResponse=new BoardingPassFlightDateResponse();
        ServiceResultWsData serviceResultWsData= new ServiceResultWsData();
        try {
            String flightNumber;
            Date flightDate;
            String getFlightDateByFlightNumber;
            String getFlightGateNumber;

            if (Objects.nonNull(boardingPassFlightDateRequest.getPnrCode())){
                flightNumber=boardingPassFlightDateRequest.getPnrCode().substring(36, 43).toUpperCase();
                flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                flightDate= DateUtil.convertJulianDateToDate(boardingPassFlightDateRequest.getPnrCode().substring(44, 47));

                FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDate, flightNumber);
                getFlightGateNumber = flightInformationModel.getGate();
                SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm:ss");
                getFlightDateByFlightNumber = simpleDateFormatDate.format(flightInformationModel.getStoDate()) + " " + simpleDateFormatTime.format(flightInformationModel.getStoTime());

                SimpleDateFormat date
                        = new SimpleDateFormat(
                        "dd/MM/yyyy HH:mm:ss");
                Date flightDateInfo = date.parse(getFlightDateByFlightNumber);
                Date currentDate = new Date();
                long difference_In_Time
                        = flightDateInfo.getTime() - currentDate.getTime();
                boardingPassFlightDateResponse.setBoardingFlightDate(getFlightDateByFlightNumber);
                boardingPassFlightDateResponse.setDiffBetweenToDate(String.valueOf(difference_In_Time));
                boardingPassFlightDateResponse.setGate(getFlightGateNumber != null ? getFlightGateNumber : "");
                serviceResultWsData.setResultType("S");
                serviceResultWsData.setStatusCode(200);
                boardingPassFlightDateResponse.setServiceResult(serviceResultWsData);
                return boardingPassFlightDateResponse;
            }
            else {
                serviceResultWsData.setResultType("E");
                serviceResultWsData.setMessage("Lütfen geçerli bir pnr kodu deneyin");
                boardingPassFlightDateResponse.setServiceResult(serviceResultWsData);
            }

        }catch (Exception e){
            LOG.error("an error occured in getFioriFlightInfo function" );
            serviceResultWsData.setResultType("E");
            serviceResultWsData.setMessage("Error occured");
            serviceResultWsData.setStatusCode(400);
            boardingPassFlightDateResponse.setServiceResult(serviceResultWsData);
            return boardingPassFlightDateResponse;
        }
        return boardingPassFlightDateResponse;
    }
    @Secured(
            {"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/getContractedCompaniesForService/{qrType}", method = RequestMethod.GET)
    @ResponseBody
    public ContractedCompaniesResponse getContractedCompaniesForService(@PathVariable final String qrType)
    {
        ContractedCompaniesResponse responseData = new ContractedCompaniesResponse();
        try {
            responseData.setContractedCompaniesList(djpManuelRecordService.getContractedCompaniesForService(qrType));
            if (Objects.isNull(responseData.getContractedCompaniesList())){
                throw new RuntimeException();
            }
            responseData.setServiceResult(DjpServiceUtil.generateSuccessServiceResult());
            return responseData;
        }catch (Exception e){
            LOG.error("get Contracted Companies For a Service", e);
            responseData.setServiceResult(DjpServiceUtil.generateErrorServiceResult("Error in get Contracted Companies For a Service"));
            return responseData;
        }
    }

    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/getPosReceiptPrice", method = RequestMethod.POST)
    @ResponseBody
    public PosReceiptData getPosReceiptPrice(){
        PosReceiptData posReceiptData = new PosReceiptData();
        try {
            posReceiptData.setReceiptPrice(baseStoreService.getCurrentBaseStore().getDjpReceiptPriceLimit());
            posReceiptData.setStatus(200);
        } catch (Exception e){
            LOG.error("Bir sorun oluştu", e);
            posReceiptData.setReceiptPrice(0D);
            posReceiptData.setStatus(200);
        }
        return posReceiptData;
    }
    @GetMapping("/core")
    public Map<String, String> hello()
    {
        return Collections.singletonMap("text", "Hello Core");
    }

}
