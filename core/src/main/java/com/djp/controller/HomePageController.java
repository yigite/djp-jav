package com.djp.controller;


import com.djp.data.form.LoginForm;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/")
@Scope("session")
public class HomePageController {

    private static final String SPRING_SECURITY_LAST_USERNAME = "SPRING_SECURITY_LAST_USERNAME";

/*


    @GetMapping
    public String getLoginPage(final Model model, final HttpServletRequest request, final HttpSession session)  {
        if (SecurityContextHolder.getContext().getAuthentication() != null && !((String)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).contains("anonymous")){
            return "homePage";
        } else {
            final LoginForm loginForm = new LoginForm();
            if (request.getParameter("loginError")!=null
                    &&Boolean.TRUE.equals(Boolean.valueOf(request.getParameter("loginError")))) {
                final String username = (String) session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
                if (username != null) {
                    session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);
                }

                loginForm.setUsername(username);
            }

            if(request.getParameter("validateMessage") != null){
                model.addAttribute("expiredToken",true);
            }
            if(request.getParameter("isEmptyToken") != null){
                model.addAttribute("isEmptyToken",true);
            }
            model.addAttribute("loginForm", loginForm);
            return "loginpage";
        }
    }

    @GetMapping(value = "successLogin")
    public String successLogin(final HttpSession session, final Model model, final HttpServletRequest request) {
        return "redirect:/";
    }

    @GetMapping(value = "login")
    public String login() {
        return "redirect:/";
    }

    @GetMapping(value = "/errorPage")
    public String errorPage() {
        return "redirect:/";
    }


*/


}
