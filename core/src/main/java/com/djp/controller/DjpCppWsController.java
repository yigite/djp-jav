package com.djp.controller;

import com.djp.facades.DjpCppFacade;
import com.djp.request.DjpCppWsRequest;
import com.djp.response.DjpCppWsResponse;
import com.djp.util.DjpServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;

@RestController
@RequestMapping(value = "/djpstore/igaCppWsController")
public class DjpCppWsController {
    private static final Logger LOG = LoggerFactory.getLogger(DjpCppWsController.class);

    @Resource
    private DjpCppFacade djpCppFacade;

    @Resource
    private MessageSource messageSource;


    @Secured({ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value="/initPayment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DjpCppWsResponse initPayment(@RequestBody DjpCppWsRequest djpCppWsRequest) {
//        validate(djpCppWsRequest, "djpCppWsRequest", djpCppWsRequestValidator);
        DjpCppWsResponse response = new DjpCppWsResponse();
        try {
            return djpCppFacade.initPayment(djpCppWsRequest);
        }catch (Exception ex){
            LOG.error("IgaCppWsController::initPayment has an error:",ex);
            response.setResult(DjpServiceUtil.generateErrorServiceResult(messageSource.getMessage("rest.controller.system.exception",null, LocaleContextHolder.getLocale())));
            return response;
        }

    }
}
