package com.djp.controller;


import com.djp.dto.UserWsDto;
import com.djp.facades.DjpManuelRecordFacade;
import com.djp.request.NewManuelRecordRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.response.FioriStatusResponse;

import com.djp.rest.data.DjpPosLogDataRequest;
import com.djp.rest.data.DjpPosLogDataResponse;
import com.djp.rest.data.ServiceResultWsData;
import com.djp.service.DjpManuelRecordService;
import com.djp.service.DjpPosService;
import com.djp.util.DjpServiceUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;

public class DjpFioriWsController {
    private static final Logger LOG = LoggerFactory.getLogger(DjpFioriWsController.class);

    @Resource
    private DjpManuelRecordFacade djpManuelRecordFacade;

    @Resource
    private DjpManuelRecordService djpManuelRecordService;

    @Resource
    private DjpServiceUtil djpServiceUtil;

    @Resource
    private DjpPosService djpPosService;

    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/checkManuelBoarding", method = RequestMethod.POST)
    @ResponseBody
    public UserWsDto checkManuelBoarding(@RequestBody QrForFioriRequest qrForFioriRequest, final HttpServletRequest request) throws JsonProcessingException {
        LOG.info("checkBoardingPass:" + generateLog(getJsonFromRequest(qrForFioriRequest)));
        UserWsDto response = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();

        try {
            return djpManuelRecordFacade.checkManuelBoarding(qrForFioriRequest);
        } catch (Exception ex) {
            LOG.error("checkBoardingPass has an error :", ex);
            result.setType("E");
            response.setResult(result);
            return response;
        }
    }
    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/newRecord", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ServiceResultWsData saveNewManuelRecord(@RequestBody final NewManuelRecordRequest newManuelRecordRequest)
    {
        try {
            return djpManuelRecordService.saveNewManuelRecord(newManuelRecordRequest);
        }catch (Exception e){
            LOG.error("save New Manuel Record", e);
            return djpServiceUtil.generateErrorServiceResult("Error in save New Manuel Record");
        }
    }
    @Secured(
            {"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/getPosLogData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DjpPosLogDataResponse getPosLogData(@RequestBody DjpPosLogDataRequest dataRequest) throws Exception
    {
        try {
            return djpPosService.getPosLogData(dataRequest);
        }catch (Exception e){
            LOG.error("An unexpected error : ", e);
            return new DjpPosLogDataResponse();
        }
    }
    private String generateLog(String jsonFromRequest) throws JsonProcessingException {
        JSONObject jsonObject = new JSONObject(jsonFromRequest);
        return jsonObject.toString();
    }

    public static String getJsonFromRequest(Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }
}
