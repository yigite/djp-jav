package com.djp.controller;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.data.CustomerData;
import com.djp.dto.UserWsDto;
import com.djp.facades.DjpCustomerFacade;
import com.djp.facades.DjpQRFacade;
import com.djp.response.CheckCustomerOwnedServiceResponse;
import com.djp.strategy.DjpPremiumServiceUsageStrategy;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping(value = "/djpstore/qrController")
public class DjpQrController {
    private static final Logger LOG = LoggerFactory.getLogger(DjpContractedTicketsController.class);

    @Resource
    private DjpQRFacade djpQRFacade;

    @Resource
    private DjpCustomerFacade customerFacade;

    @Resource
    private DjpPremiumServiceUsageStrategy djpPremiumServiceUsageStrategy;

    @RequestMapping(value="/checkCustomerOwnedService", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CheckCustomerOwnedServiceResponse checkCustomerOwnedService(@RequestParam String cardNo, final HttpServletRequest request) {
        CheckCustomerOwnedServiceResponse response=new CheckCustomerOwnedServiceResponse();
        try {
            LOG.info("checkCustomerOwnedService-cardNo:"+cardNo);
            String device = request.getParameter("device");
            CheckCustomerOwnedServiceResponse checkCustomerOwnedServiceResponse = djpQRFacade.checkCustomerOwnedService(cardNo,device);
            boolean isFiori = "F".equals(device);
            if(Boolean.TRUE.equals(isFiori)){
                fillUserWsDto(cardNo, checkCustomerOwnedServiceResponse);
            }
            LOG.info("checkCustomerOwnedServiceResponse:"+getJsonFromRequst(checkCustomerOwnedServiceResponse));
            return checkCustomerOwnedServiceResponse;
        }catch (Exception ex){
            LOG.error("has an error checkCustomerOwnedServiceResponse",ex);
            response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Error);
            response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
            return response;
        }
    }
    private void fillUserWsDto(String cardNo, CheckCustomerOwnedServiceResponse checkCustomerOwnedServiceResponse) {
        CustomerData customerData = customerFacade.getUserForUID(cardNo);
        UserWsDto userWsDto = djpPremiumServiceUsageStrategy.getUserWsDto(customerData);
        if(Objects.nonNull(userWsDto)){
            checkCustomerOwnedServiceResponse.setCustomer(userWsDto);
            if(customerData.getProfilePicture()!=null) {
                userWsDto.setImageUrl(customerData.getProfilePicture().getUrl());
            }
        }
    }

    private String getJsonFromRequst(Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }
}
