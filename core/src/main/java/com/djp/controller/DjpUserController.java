package com.djp.controller;

import com.djp.core.model.EmployeeModel;
import com.djp.core.model.PrincipalGroupModel;
import com.djp.rest.data.ServiceResultWsData;
import com.djp.rest.data.UserLoginRequest;
import com.djp.rest.data.UserLoginResponse;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/djpstore/userController")
public class DjpUserController {
/*
    @Resource
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/loginCheck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserLoginResponse loginCheck(@RequestBody UserLoginRequest userLoginRequest, final HttpServletRequest request)
    {
        UserLoginResponse userLoginResponse = new UserLoginResponse();
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        try{
            EmployeeModel employeeModel = (EmployeeModel) authenticationService.checkCredentials(userLoginRequest.getUserName(), userLoginRequest.getPassword());
            Boolean serviceStatus = Objects.nonNull(employeeModel);
            if(Boolean.TRUE.equals(serviceStatus) && !CollectionUtils.isEmpty(employeeModel.getGroups())){
                serviceResultWsData.setStatusCode(200);
                serviceResultWsData.setMessage("Login Success");
                userLoginResponse.setUserGroups(employeeModel.getGroups().stream().map(PrincipalGroupModel::getUid).collect(Collectors.joining(",")));
            }else{
                serviceResultWsData.setStatusCode(401);
                serviceResultWsData.setMessage("Username - password is wrong or you don't have any authorizations!");
            }
            userLoginResponse.setServiceResult(serviceResultWsData);
            return userLoginResponse;
        } catch (Exception e){
            serviceResultWsData.setStatusCode(401);
            serviceResultWsData.setMessage("Username - password is wrong or you don't have any authorizations!");
            userLoginResponse.setServiceResult(serviceResultWsData);
            //LOG.error("loginCheckExp:",e);
        }
        return userLoginResponse;
    }*/
}
