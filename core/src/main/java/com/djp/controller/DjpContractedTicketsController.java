package com.djp.controller;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.enums.QrType;
import com.djp.core.model.DjpContractedTicketsModel;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.request.QrPassRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.service.DjpEmailService;
import com.djp.service.DjpQRService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.util.*;

public class DjpContractedTicketsController {
    private static final Logger LOG = LoggerFactory.getLogger(DjpContractedTicketsController.class);
    @Resource
    private DjpQRService djpQRService;
    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;
    @Resource
    private ModelService modelService;
    @Resource
    private DjpEmailService igaEmailService;

    @Secured({"ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP"})
    @RequestMapping(value = "/contractedTicketPass", method = RequestMethod.POST)
    @ResponseBody
    public FioriStatusResponse contractedTicketPass(final HttpServletRequest request, @RequestBody QrPassRequest qrPassRequest) throws JsonProcessingException {
        String clientID = request.getRemoteUser();
        LOG.info(clientID+"-qrPassRequest:" + getJsonFromRequst(qrPassRequest));
        FioriStatusResponse status = new FioriStatusResponse();
        Date date = new Date();
        String statusCode = null;
        int passRequestCycleCount = 0;
        try {
            DjpContractedTicketsModel igaContractedTicketsModel = djpQRService.getContractedTicketByIDAndContractedService(qrPassRequest.getUniqueID(),qrPassRequest.getQrType());

            if (Objects.isNull(igaContractedTicketsModel)) {
                status.setType("E");
                status.setMessage("QR IGA PASS Tarafında Bulunmamaktadır!");
                djpQRService.saveUnapprovedPass(qrPassRequest.getUniqueID(), status.getMessage(), qrPassRequest.getQrType(), DjpContractedTicketsModel._TYPECODE);
                return status;
            }
            else {
                boolean usage = Objects.nonNull(igaContractedTicketsModel.getWhereWasTicketUseds()) && igaContractedTicketsModel.getClaimed().equals(igaContractedTicketsModel.getUsed());
                int startDateCompare = DateUtil.dateCompare(igaContractedTicketsModel.getStartDate(), new Date());
                int expireDateCompare = DateUtil.dateCompare(igaContractedTicketsModel.getExpireDate(), new Date());
                if (startDateCompare > 0) {
                    status.setType("E");
                    status.setMessage("Biletin kullanım başlangıç tarihi uygun değil!,Başlangıç Tarihi:"+igaContractedTicketsModel.getStartDate().toString());
                    djpQRService.saveUnapprovedPass(igaContractedTicketsModel.getUniqueID(), status.getMessage(), qrPassRequest.getQrType(), DjpContractedTicketsModel._TYPECODE);
                    return status;
                }
                else if (expireDateCompare < 0) {
                    status.setType("E");
                    status.setMessage("Biletin kullanım tarihi geçmiş!,Bitiş Tarihi:"+igaContractedTicketsModel.getExpireDate().toString());
                    djpQRService.saveUnapprovedPass(igaContractedTicketsModel.getUniqueID(), status.getMessage(), qrPassRequest.getQrType(), DjpContractedTicketsModel._TYPECODE);
                    return status;
                }else if (usage) {
                    status.setType("E");
                    status.setMessage("Biletinizin Kullanım Hakkı Dolmuştur!");
                    djpQRService.saveUnapprovedPass(igaContractedTicketsModel.getUniqueID(), status.getMessage(), qrPassRequest.getQrType(), DjpContractedTicketsModel._TYPECODE);
                    return status;
                }
                else if (!qrTypeEnumHashMap.get(qrPassRequest.getQrType()).startsWith(igaContractedTicketsModel.getContractedService().getCode())) {
                    status.setType("E");
                    status.setMessage("Biletiniz Bu Hizmet İçin Kullanımı Mevcut Değil!");
                    djpQRService.saveUnapprovedPass(igaContractedTicketsModel.getUniqueID(), status.getMessage(), qrPassRequest.getQrType(), DjpContractedTicketsModel._TYPECODE);
                    return status;
                } else {
                    Set<QrType> whereUsed = new HashSet<>();
                    if (!igaContractedTicketsModel.getWhereWasTicketUseds().isEmpty()) {
                        igaContractedTicketsModel.getWhereWasTicketUseds().stream().forEach(qrType -> whereUsed.add(qrType));
                    }
                    whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                    igaContractedTicketsModel.setUsed(igaContractedTicketsModel.getUsed() + 1);
                    igaContractedTicketsModel.setWhereWasTicketUseds(whereUsed);
                    //EmployeeModel employeeModel = null;
                    try {
                        /*employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                        igaContractedTicketsModel.setPersonalID(org.apache.commons.lang.StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                        igaContractedTicketsModel.setPersonalName(employeeModel != null ? employeeModel.getName() : null);*/
                    } catch (Exception e){
                        //LOG.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                    }
                    modelService.saveOrUpdate(igaContractedTicketsModel);
                    do {
                        if(igaContractedTicketsModel.getCompanyClientId().equals(DjpCommerceservicesConstants.ATLAS_GLOBAL_CLIENT_ID)) {
                            passRequestCycleCount = passRequestCycleCount + 1;
                            statusCode = postAtlasGlobalVoucherPassRequest(igaContractedTicketsModel);
                        }
                    }while (passRequestCycleCount != 3 && !"OK".equals(statusCode));

                    if(passRequestCycleCount == 3 && !"OK".equals(statusCode)){
                        igaEmailService.sendQrToCompanyErrorInfoMail(igaContractedTicketsModel.getUniqueID(),igaContractedTicketsModel.getContractedService().getCode(),igaContractedTicketsModel.getCompanyClientId());
                    }
                    status.setType("S");
                    status.setMessage("Bilet Kullandırıldı.");
                    return status;
                }
            }
        } catch (Exception e) {
            LOG.error("contractedTicketPass has error", e);
            status.setType("E");
            status.setMessage("Bileti kullandırırken bir hata oluştu!");
            return status;
        }
    }
    private String postAtlasGlobalVoucherPassRequest(DjpContractedTicketsModel igaContractedTicketsModel) {
        String accessToken = tokenRequestForAtlasGlobal();
        String statusCode = null;
        if(Objects.nonNull(accessToken)) {
            /*AtlasGlobalPassRequest atlasGlobalPassRequest = new AtlasGlobalPassRequest();
            atlasGlobalPassRequest.setUniqueId(igaContractedTicketsModel.getUniqueID());
            atlasGlobalPassRequest.setContractedService(igaContractedTicketsModel.getContractedService().getCode());
            atlasGlobalPassRequest.setUsedDate(DateUtil.dateToString(new Date()));
            try (CloseableHttpClient client = HttpClients.createDefault()) {

                HttpPost httpPost = createPostRequestForAtlasGlobal(atlasGlobalPassRequest, atlasGlobalPassWsUrl, accessToken);
                //LOG.info("passRequestForAtlasGlobal:" + EntityUtils.toString(httpPost.getEntity()));
                CloseableHttpResponse httpResponse = client.execute(httpPost);

                JSONObject myObject = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
                //LOG.info("passResponseForAtlasGlobal:" + myObject.toString());
                statusCode = myObject.getString("statusCode");
                return statusCode;
            } catch (Exception ex) {
                //LOG.error("postAtlasGlobalVoucherPassRequest has error : %s", ex);
                return null;
            }*/
        }
        return null;
    }
    private HttpPost getTokenForAtlasGlobal(Object requestObject, String url) throws JsonProcessingException {
        String jsonEntity = getJsonFromRequst(requestObject);
        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = new StringEntity(jsonEntity, ContentType.APPLICATION_JSON);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type","application/json");
        return httpPost;
    }
    private String getJsonFromRequst(Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }

    private String tokenRequestForAtlasGlobal() {
        String accessToken = null;
        /*AtlasGlobalTokenRequest atlasGlobalTokenRequest = new AtlasGlobalTokenRequest();
        atlasGlobalTokenRequest.setUsername(Config.getString("atlas.global.web.service.username",null));
        atlasGlobalTokenRequest.setPassword(Config.getString("atlas.global.web.service.password",null));
        try (CloseableHttpClient client = HttpClients.createDefault()) {

            HttpPost httpPost = getTokenForAtlasGlobal(atlasGlobalTokenRequest, atlasGlobalTokenWsUrl);
            CloseableHttpResponse httpResponse = client.execute(httpPost);
            JSONObject myObject = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
            accessToken = myObject.getString("accessToken");
            return accessToken;
        } catch (Exception ex) {
            LOG.error("tokenRequestForAtlasGlobal has error : %s", ex);
            return null;
        }*/
        return null;
    }
}
