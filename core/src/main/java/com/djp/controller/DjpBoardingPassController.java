package com.djp.controller;

import com.djp.core.facade.DjpBoardingFacade;
import com.djp.response.DjpBoardingPassesResponse;
import com.djp.response.FioriStatusResponse;
import com.djp.request.UpdateBoardingPassRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.annotation.Resource;
import java.text.ParseException;

@Controller
@RequestMapping(value = "/passWsController")
public class DjpBoardingPassController{

    @Resource
    DjpBoardingFacade djpBoardingFacade;

    private static final Logger LOG = LoggerFactory.getLogger(DjpBoardingPassController.class);

    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value = "/getBoardingPasses", method = RequestMethod.GET)
    @ResponseBody
    public DjpBoardingPassesResponse getBoardingPasses(@RequestParam String date,
                                                       @RequestParam String endDate,
                                                       @RequestParam Boolean isNewSale,
                                                       @RequestParam String whereUsed,
                                                       @RequestParam String locationID) {
        DjpBoardingPassesResponse response = new DjpBoardingPassesResponse();
        FioriStatusResponse serviceResult = new FioriStatusResponse();
        try {
            response.setBoardingpasses(djpBoardingFacade.getBoardingPasses(date,endDate,isNewSale,whereUsed,locationID));
            return response;
        }catch (Exception ex){
            LOG.error("has an error getBoardingPasses:",ex);
            serviceResult.setType("E");
            serviceResult.setMessage("BOARDINGLER SORGULANIRKEN HATA OLUŞTU!");
            response.setServiceResult(serviceResult);
            return response;
        }

    }
    @Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
    @RequestMapping(value = "/updateBoardingPass", method = RequestMethod.POST)
    @ResponseBody
    public FioriStatusResponse updateBoardingPass(@RequestBody UpdateBoardingPassRequest updateBoardingPassRequest) throws ParseException, JsonProcessingException {
        LOG.info("updateBoardingPassRequest:"+generateLog(getJsonFromRequest(updateBoardingPassRequest)));
        FioriStatusResponse response = new FioriStatusResponse();
        try {
            return djpBoardingFacade.updateBoardingPass(updateBoardingPassRequest);
        }catch (Exception ex){
            LOG.error("has an error updateBoardingPass:",ex);
            response.setType("E");
            response.setMessage("MİSAFİR OLUŞTURULURKEN HATA OLUŞTU!");
            return response;
        }
    }
    private String generateLog(String jsonFromRequest) throws JsonProcessingException {
        JSONObject jsonObject = new JSONObject(jsonFromRequest);
        return jsonObject.toString();
    }
    private String getJsonFromRequest(Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }
}
