package com.djp.data;

public class NurusServiceCreateReservationRequestAttendeeData {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequestAttendeeData.Email</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Email;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequestAttendeeData.Name</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Name;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequestAttendeeData.PnrCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String PnrCode;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequestAttendeeData.QRCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String QRCode;

    public NurusServiceCreateReservationRequestAttendeeData()
    {
        // default constructor
    }



    public void setEmail(final String Email)
    {
        this.Email = Email;
    }



    public String getEmail()
    {
        return Email;
    }



    public void setName(final String Name)
    {
        this.Name = Name;
    }



    public String getName()
    {
        return Name;
    }



    public void setPnrCode(final String PnrCode)
    {
        this.PnrCode = PnrCode;
    }



    public String getPnrCode()
    {
        return PnrCode;
    }



    public void setQRCode(final String QRCode)
    {
        this.QRCode = QRCode;
    }



    public String getQRCode()
    {
        return QRCode;
    }


}
