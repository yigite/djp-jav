package com.djp.data;

import java.io.Serializable;

public  class ExperienceCenterGameSaleListItem  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String gameCode;
    private String gameName;
    private Integer gameClaimed;
    private Integer gameUsing;

    public ExperienceCenterGameSaleListItem()
    {
        // default constructor
    }



    public void setGameCode(final String gameCode)
    {
        this.gameCode = gameCode;
    }



    public String getGameCode()
    {
        return gameCode;
    }



    public void setGameName(final String gameName)
    {
        this.gameName = gameName;
    }



    public String getGameName()
    {
        return gameName;
    }



    public void setGameClaimed(final Integer gameClaimed)
    {
        this.gameClaimed = gameClaimed;
    }



    public Integer getGameClaimed()
    {
        return gameClaimed;
    }



    public void setGameUsing(final Integer gameUsing)
    {
        this.gameUsing = gameUsing;
    }



    public Integer getGameUsing()
    {
        return gameUsing;
    }



}
