package com.djp.data;

public class NurusServiceLoginResponseUserData {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.Id</code> property defined at extension <code>djpcommercewebservices</code>. */

    private Integer Id;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.Name</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Name;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.CompanyName</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String CompanyName;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.TokenKey</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String TokenKey;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.CardID</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String CardID;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.Title</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Title;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.Picture</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Picture;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseUserData.Email</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Email;

    public NurusServiceLoginResponseUserData()
    {
        // default constructor
    }



    public void setId(final Integer Id)
    {
        this.Id = Id;
    }



    public Integer getId()
    {
        return Id;
    }



    public void setName(final String Name)
    {
        this.Name = Name;
    }



    public String getName()
    {
        return Name;
    }



    public void setCompanyName(final String CompanyName)
    {
        this.CompanyName = CompanyName;
    }



    public String getCompanyName()
    {
        return CompanyName;
    }



    public void setTokenKey(final String TokenKey)
    {
        this.TokenKey = TokenKey;
    }



    public String getTokenKey()
    {
        return TokenKey;
    }



    public void setCardID(final String CardID)
    {
        this.CardID = CardID;
    }



    public String getCardID()
    {
        return CardID;
    }



    public void setTitle(final String Title)
    {
        this.Title = Title;
    }



    public String getTitle()
    {
        return Title;
    }



    public void setPicture(final String Picture)
    {
        this.Picture = Picture;
    }



    public String getPicture()
    {
        return Picture;
    }



    public void setEmail(final String Email)
    {
        this.Email = Email;
    }



    public String getEmail()
    {
        return Email;
    }



}
