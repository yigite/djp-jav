package com.djp.data;

import java.io.Serializable;

public abstract  class AbstractCustomerFeatureData  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>AbstractCustomerFeatureData.code</code> property defined at extension <code>djpaccount</code>. */

    private String code;

    /** <i>Generated property</i> for <code>AbstractCustomerFeatureData.name</code> property defined at extension <code>djpaccount</code>. */

    private String name;

    /** <i>Generated property</i> for <code>AbstractCustomerFeatureData.other</code> property defined at extension <code>djpaccount</code>. */

    private String other;

    /** <i>Generated property</i> for <code>AbstractCustomerFeatureData.prioritySort</code> property defined at extension <code>djpaccount</code>. */

    private Integer prioritySort;

    public AbstractCustomerFeatureData()
    {
        // default constructor
    }



    public void setCode(final String code)
    {
        this.code = code;
    }



    public String getCode()
    {
        return code;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setOther(final String other)
    {
        this.other = other;
    }



    public String getOther()
    {
        return other;
    }



    public void setPrioritySort(final Integer prioritySort)
    {
        this.prioritySort = prioritySort;
    }



    public Integer getPrioritySort()
    {
        return prioritySort;
    }


    @Override
    public boolean equals(final Object o)
    {

        if (o == null) return false;
        if (o == this) return true;

        try
        {
            final AbstractCustomerFeatureData other = (AbstractCustomerFeatureData) o;
            return new org.apache.commons.lang.builder.EqualsBuilder()
                    .append(getCode(), other.getCode())
                    .isEquals();
        }
        catch (ClassCastException c)
        {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return new org.apache.commons.lang.builder.HashCodeBuilder()
                .append(getCode())
                .toHashCode();
    }

}