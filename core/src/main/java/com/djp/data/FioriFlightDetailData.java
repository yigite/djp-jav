package com.djp.data;

import java.io.Serializable;
import java.util.Date;

public class FioriFlightDetailData implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String toAirport;

    private String fromAirport;

    private Date flightDate;

    public FioriFlightDetailData()
    {
        // default constructor
    }

    public void setToAirport(final String toAirport)
    {
        this.toAirport = toAirport;
    }

    public String getToAirport()
    {
        return toAirport;
    }

    public void setFromAirport(final String fromAirport)
    {
        this.fromAirport = fromAirport;
    }

    public String getFromAirport()
    {
        return fromAirport;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }
}
