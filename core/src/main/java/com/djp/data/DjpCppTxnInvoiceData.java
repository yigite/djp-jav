package com.djp.data;

public class DjpCppTxnInvoiceData {
    private String name;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.identityNumber</code> property defined at extension <code>igacppaddon</code>. */

    private String identityNumber;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.taxOffice</code> property defined at extension <code>igacppaddon</code>. */

    private String taxOffice;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.email</code> property defined at extension <code>igacppaddon</code>. */

    private String email;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.phoneNumber</code> property defined at extension <code>igacppaddon</code>. */

    private String phoneNumber;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.companyID</code> property defined at extension <code>igacppaddon</code>. */

    private String companyID;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.address</code> property defined at extension <code>igacppaddon</code>. */

    private String address;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.nationality</code> property defined at extension <code>igacppaddon</code>. */

    private String nationality;

    /** <i>Generated property</i> for <code>IgaCppTxnInvoiceData.isCorporate</code> property defined at extension <code>igacppaddon</code>. */

    private Boolean isCorporate;

    public DjpCppTxnInvoiceData()
    {
        // default constructor
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setIdentityNumber(final String identityNumber)
    {
        this.identityNumber = identityNumber;
    }



    public String getIdentityNumber()
    {
        return identityNumber;
    }



    public void setTaxOffice(final String taxOffice)
    {
        this.taxOffice = taxOffice;
    }



    public String getTaxOffice()
    {
        return taxOffice;
    }



    public void setEmail(final String email)
    {
        this.email = email;
    }



    public String getEmail()
    {
        return email;
    }



    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }



    public String getPhoneNumber()
    {
        return phoneNumber;
    }



    public void setCompanyID(final String companyID)
    {
        this.companyID = companyID;
    }



    public String getCompanyID()
    {
        return companyID;
    }



    public void setAddress(final String address)
    {
        this.address = address;
    }



    public String getAddress()
    {
        return address;
    }



    public void setNationality(final String nationality)
    {
        this.nationality = nationality;
    }



    public String getNationality()
    {
        return nationality;
    }



    public void setIsCorporate(final Boolean isCorporate)
    {
        this.isCorporate = isCorporate;
    }



    public Boolean getIsCorporate()
    {
        return isCorporate;
    }


}
