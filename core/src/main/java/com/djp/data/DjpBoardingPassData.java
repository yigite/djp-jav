package com.djp.data;

import com.djp.response.QrCodeForExperienceCenterSaleResponse;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DjpBoardingPassData implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String locationID;
    private Date creationTime;
    private String pnrCode;
    private String tokenKey;
    private List<String> whereUsed;
    private String formatCode;
    private String numberOfLegsEncoded;
    private String passengerName;
    private String electronicTicketIndicator;
    private String operatingCarrierPnrCode;
    private String fromCityAirportCode;
    private String toCityAirportCode;
    private String operatingCarrierDesignator;
    private String flightNumber;
    private Date dateOfFlight;
    private String compartmentCode;
    private String seatNumber;
    private String checkInSequenceNumber;
    private String passengerStatus;
    private Boolean newSale;
    private Boolean isOwner;
    private String sapOrderID;
    private Boolean couldNotReadBoardingPass;
    private Integer guestBoardingCount;
    private String personalID;
    private String personalName;
    private String productCode;
    private String contractedCompanyId;
    private String contractedCompanyName;
    private QrCodeForExperienceCenterSaleResponse qrDataInfo;
    private DjpBoardingServiceInfoData contractCompanyInfo;
    private String boardingTypeClassName;

    public DjpBoardingPassData()
    {
        // default constructor
    }



    public void setLocationID(final String locationID)
    {
        this.locationID = locationID;
    }



    public String getLocationID()
    {
        return locationID;
    }



    public void setCreationTime(final Date creationTime)
    {
        this.creationTime = creationTime;
    }



    public Date getCreationTime()
    {
        return creationTime;
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }



    public String getTokenKey()
    {
        return tokenKey;
    }



    public void setWhereUsed(final List<String> whereUsed)
    {
        this.whereUsed = whereUsed;
    }



    public List<String> getWhereUsed()
    {
        return whereUsed;
    }



    public void setFormatCode(final String formatCode)
    {
        this.formatCode = formatCode;
    }



    public String getFormatCode()
    {
        return formatCode;
    }



    public void setNumberOfLegsEncoded(final String numberOfLegsEncoded)
    {
        this.numberOfLegsEncoded = numberOfLegsEncoded;
    }



    public String getNumberOfLegsEncoded()
    {
        return numberOfLegsEncoded;
    }



    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }



    public String getPassengerName()
    {
        return passengerName;
    }



    public void setElectronicTicketIndicator(final String electronicTicketIndicator)
    {
        this.electronicTicketIndicator = electronicTicketIndicator;
    }



    public String getElectronicTicketIndicator()
    {
        return electronicTicketIndicator;
    }



    public void setOperatingCarrierPnrCode(final String operatingCarrierPnrCode)
    {
        this.operatingCarrierPnrCode = operatingCarrierPnrCode;
    }



    public String getOperatingCarrierPnrCode()
    {
        return operatingCarrierPnrCode;
    }



    public void setFromCityAirportCode(final String fromCityAirportCode)
    {
        this.fromCityAirportCode = fromCityAirportCode;
    }



    public String getFromCityAirportCode()
    {
        return fromCityAirportCode;
    }



    public void setToCityAirportCode(final String toCityAirportCode)
    {
        this.toCityAirportCode = toCityAirportCode;
    }



    public String getToCityAirportCode()
    {
        return toCityAirportCode;
    }



    public void setOperatingCarrierDesignator(final String operatingCarrierDesignator)
    {
        this.operatingCarrierDesignator = operatingCarrierDesignator;
    }



    public String getOperatingCarrierDesignator()
    {
        return operatingCarrierDesignator;
    }



    public void setFlightNumber(final String flightNumber)
    {
        this.flightNumber = flightNumber;
    }



    public String getFlightNumber()
    {
        return flightNumber;
    }



    public void setDateOfFlight(final Date dateOfFlight)
    {
        this.dateOfFlight = dateOfFlight;
    }



    public Date getDateOfFlight()
    {
        return dateOfFlight;
    }



    public void setCompartmentCode(final String compartmentCode)
    {
        this.compartmentCode = compartmentCode;
    }



    public String getCompartmentCode()
    {
        return compartmentCode;
    }



    public void setSeatNumber(final String seatNumber)
    {
        this.seatNumber = seatNumber;
    }



    public String getSeatNumber()
    {
        return seatNumber;
    }



    public void setCheckInSequenceNumber(final String checkInSequenceNumber)
    {
        this.checkInSequenceNumber = checkInSequenceNumber;
    }



    public String getCheckInSequenceNumber()
    {
        return checkInSequenceNumber;
    }



    public void setPassengerStatus(final String passengerStatus)
    {
        this.passengerStatus = passengerStatus;
    }



    public String getPassengerStatus()
    {
        return passengerStatus;
    }



    public void setNewSale(final Boolean newSale)
    {
        this.newSale = newSale;
    }



    public Boolean getNewSale()
    {
        return newSale;
    }



    public void setIsOwner(final Boolean isOwner)
    {
        this.isOwner = isOwner;
    }



    public Boolean getIsOwner()
    {
        return isOwner;
    }



    public void setSapOrderID(final String sapOrderID)
    {
        this.sapOrderID = sapOrderID;
    }



    public String getSapOrderID()
    {
        return sapOrderID;
    }



    public void setCouldNotReadBoardingPass(final Boolean couldNotReadBoardingPass)
    {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }



    public Boolean getCouldNotReadBoardingPass()
    {
        return couldNotReadBoardingPass;
    }



    public void setGuestBoardingCount(final Integer guestBoardingCount)
    {
        this.guestBoardingCount = guestBoardingCount;
    }



    public Integer getGuestBoardingCount()
    {
        return guestBoardingCount;
    }



    public void setPersonalID(final String personalID)
    {
        this.personalID = personalID;
    }



    public String getPersonalID()
    {
        return personalID;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }



    public String getProductCode()
    {
        return productCode;
    }



    public void setContractedCompanyId(final String contractedCompanyId)
    {
        this.contractedCompanyId = contractedCompanyId;
    }



    public String getContractedCompanyId()
    {
        return contractedCompanyId;
    }



    public void setContractedCompanyName(final String contractedCompanyName)
    {
        this.contractedCompanyName = contractedCompanyName;
    }



    public String getContractedCompanyName()
    {
        return contractedCompanyName;
    }



    public void setQrDataInfo(final QrCodeForExperienceCenterSaleResponse qrDataInfo)
    {
        this.qrDataInfo = qrDataInfo;
    }



    public QrCodeForExperienceCenterSaleResponse getQrDataInfo()
    {
        return qrDataInfo;
    }



    public void setContractCompanyInfo(final DjpBoardingServiceInfoData contractCompanyInfo)
    {
        this.contractCompanyInfo = contractCompanyInfo;
    }



    public DjpBoardingServiceInfoData getContractCompanyInfo()
    {
        return contractCompanyInfo;
    }



    public void setBoardingTypeClassName(final String boardingTypeClassName)
    {
        this.boardingTypeClassName = boardingTypeClassName;
    }



    public String getBoardingTypeClassName()
    {
        return boardingTypeClassName;
    }

}
