package com.djp.data;

import java.io.Serializable;

public  class LanguageData  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;


    private String isocode;
    private String name;
    private String nativeName;
    private boolean active;
    private boolean required;

    public LanguageData()
    {
        // default constructor
    }



    public void setIsocode(final String isocode)
    {
        this.isocode = isocode;
    }



    public String getIsocode()
    {
        return isocode;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setNativeName(final String nativeName)
    {
        this.nativeName = nativeName;
    }



    public String getNativeName()
    {
        return nativeName;
    }



    public void setActive(final boolean active)
    {
        this.active = active;
    }



    public boolean isActive()
    {
        return active;
    }



    public void setRequired(final boolean required)
    {
        this.required = required;
    }



    public boolean isRequired()
    {
        return required;
    }



}

