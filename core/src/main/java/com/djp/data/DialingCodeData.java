package com.djp.data;

import java.io.Serializable;

public  class DialingCodeData  implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String isoCode;

    private String dialingCode;

    public DialingCodeData()
    {
        // default constructor
    }



    public void setIsoCode(final String isoCode)
    {
        this.isoCode = isoCode;
    }



    public String getIsoCode()
    {
        return isoCode;
    }



    public void setDialingCode(final String dialingCode)
    {
        this.dialingCode = dialingCode;
    }



    public String getDialingCode()
    {
        return dialingCode;
    }



}

