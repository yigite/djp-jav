package com.djp.data;

import java.io.Serializable;

public  class ManuelRecordsListData  implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String pnrCode;

    private String passengerName;

    private String seatNumber;

    private String flightCode;

    private String flightDate;

    private String creationTime;

    private String personalName;

    private String inputMethod;

    private String clientName;

    /** <i>Generated property</i> for <code>ManuelRecordsListData.subMembership</code> property defined at extension <code>igacommercewebservices</code>. */

    private String subMembership;

    /** <i>Generated property</i> for <code>ManuelRecordsListData.programNo</code> property defined at extension <code>igacommercewebservices</code>. */

    private String programNo;

    /** <i>Generated property</i> for <code>ManuelRecordsListData.WhoseGuest</code> property defined at extension <code>igacommercewebservices</code>. */

    private String WhoseGuest;

    /** <i>Generated property</i> for <code>ManuelRecordsListData.note</code> property defined at extension <code>igacommercewebservices</code>. */

    private String note;

    public ManuelRecordsListData()
    {
        // default constructor
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }



    public String getPassengerName()
    {
        return passengerName;
    }



    public void setSeatNumber(final String seatNumber)
    {
        this.seatNumber = seatNumber;
    }



    public String getSeatNumber()
    {
        return seatNumber;
    }



    public void setFlightCode(final String flightCode)
    {
        this.flightCode = flightCode;
    }



    public String getFlightCode()
    {
        return flightCode;
    }



    public void setFlightDate(final String flightDate)
    {
        this.flightDate = flightDate;
    }



    public String getFlightDate()
    {
        return flightDate;
    }



    public void setCreationTime(final String creationTime)
    {
        this.creationTime = creationTime;
    }



    public String getCreationTime()
    {
        return creationTime;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



    public void setInputMethod(final String inputMethod)
    {
        this.inputMethod = inputMethod;
    }



    public String getInputMethod()
    {
        return inputMethod;
    }



    public void setClientName(final String clientName)
    {
        this.clientName = clientName;
    }



    public String getClientName()
    {
        return clientName;
    }



    public void setSubMembership(final String subMembership)
    {
        this.subMembership = subMembership;
    }



    public String getSubMembership()
    {
        return subMembership;
    }



    public void setProgramNo(final String programNo)
    {
        this.programNo = programNo;
    }



    public String getProgramNo()
    {
        return programNo;
    }



    public void setWhoseGuest(final String WhoseGuest)
    {
        this.WhoseGuest = WhoseGuest;
    }



    public String getWhoseGuest()
    {
        return WhoseGuest;
    }



    public void setNote(final String note)
    {
        this.note = note;
    }



    public String getNote()
    {
        return note;
    }


}
