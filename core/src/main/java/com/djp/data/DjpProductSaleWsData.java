package com.djp.data;

import com.djp.dto.DjpDailyKidsUserWsData;
import com.djp.dto.DjpPackageProductSaleWsInfoData;

import java.util.List;

public class DjpProductSaleWsData {
    private String code;


    private String description;


    private String saleUrl;


    private String pictureUrl;


    private String packageName;


    private Integer guestCount;


    private Integer claimed;


    private List plateNumbers;


    private String orderCode;


    private String packageCode;


    private Integer adultGuestCount;


    private Integer childGuestCount;


    private Boolean isAdult;


    private Boolean isGift;


    private Boolean isPackage;


    private Integer quantity;


    private Boolean isOverSixtyFive;


    private Boolean isKiosk;


    private String expireDate;


    private List<DjpPackageProductSaleWsInfoData> premiumPackageRightDetail;


    private DjpDailyKidsUserWsData dailyKidsUser;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSaleUrl() {
        return saleUrl;
    }

    public void setSaleUrl(String saleUrl) {
        this.saleUrl = saleUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(Integer guestCount) {
        this.guestCount = guestCount;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public List getPlateNumbers() {
        return plateNumbers;
    }

    public void setPlateNumbers(List plateNumbers) {
        this.plateNumbers = plateNumbers;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public Integer getAdultGuestCount() {
        return adultGuestCount;
    }

    public void setAdultGuestCount(Integer adultGuestCount) {
        this.adultGuestCount = adultGuestCount;
    }

    public Integer getChildGuestCount() {
        return childGuestCount;
    }

    public void setChildGuestCount(Integer childGuestCount) {
        this.childGuestCount = childGuestCount;
    }

    public Boolean getAdult() {
        return isAdult;
    }

    public void setAdult(Boolean adult) {
        isAdult = adult;
    }

    public Boolean getGift() {
        return isGift;
    }

    public void setGift(Boolean gift) {
        isGift = gift;
    }

    public Boolean getPackage() {
        return isPackage;
    }

    public void setPackage(Boolean aPackage) {
        isPackage = aPackage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getOverSixtyFive() {
        return isOverSixtyFive;
    }

    public void setOverSixtyFive(Boolean overSixtyFive) {
        isOverSixtyFive = overSixtyFive;
    }

    public Boolean getKiosk() {
        return isKiosk;
    }

    public void setKiosk(Boolean kiosk) {
        isKiosk = kiosk;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<DjpPackageProductSaleWsInfoData> getPremiumPackageRightDetail() {
        return premiumPackageRightDetail;
    }

    public void setPremiumPackageRightDetail(List<DjpPackageProductSaleWsInfoData> premiumPackageRightDetail) {
        this.premiumPackageRightDetail = premiumPackageRightDetail;
    }

    public DjpDailyKidsUserWsData getDailyKidsUser() {
        return dailyKidsUser;
    }

    public void setDailyKidsUser(DjpDailyKidsUserWsData dailyKidsUser) {
        this.dailyKidsUser = dailyKidsUser;
    }
}
