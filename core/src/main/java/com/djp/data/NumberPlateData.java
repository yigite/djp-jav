package com.djp.data;

import java.io.Serializable;

public  class NumberPlateData  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NumberPlateData.code</code> property defined at extension <code>djpfacades</code>. */

    private String code;

    /** <i>Generated property</i> for <code>NumberPlateData.pk</code> property defined at extension <code>djpfacades</code>. */

    private String pk;

    public NumberPlateData()
    {
        // default constructor
    }



    public void setCode(final String code)
    {
        this.code = code;
    }



    public String getCode()
    {
        return code;
    }



    public void setPk(final String pk)
    {
        this.pk = pk;
    }



    public String getPk()
    {
        return pk;
    }


    @Override
    public boolean equals(final Object o)
    {

        if (o == null) return false;
        if (o == this) return true;

        try
        {
            final NumberPlateData other = (NumberPlateData) o;
            return new org.apache.commons.lang.builder.EqualsBuilder()
                    .append(getCode(), other.getCode())
                    .isEquals();
        }
        catch (ClassCastException c)
        {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return new org.apache.commons.lang.builder.HashCodeBuilder()
                .append(getCode())
                .toHashCode();
    }

}
