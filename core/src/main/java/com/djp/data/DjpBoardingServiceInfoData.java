package com.djp.data;

import java.io.Serializable;

public class DjpBoardingServiceInfoData implements Serializable {
    private static final long serialVersionUID = 1L;
    private String contractCompanyName;
    private String contractCompanyId;
    private String contractCompanyType;
    private String contractCompanyLengthOfStayHour;
    private String serviceStartDate;
    private String serviceStartTime;
    private String serviceEndDate;
    private String serviceEndTime;
    private String serviceOutDate;
    private String serviceOutTime;
    private Boolean serviceTimeOut;
    private String serviceTimeOutValue;
    private String status;

    public DjpBoardingServiceInfoData()
    {
        // default constructor
    }



    public void setContractCompanyName(final String contractCompanyName)
    {
        this.contractCompanyName = contractCompanyName;
    }



    public String getContractCompanyName()
    {
        return contractCompanyName;
    }



    public void setContractCompanyId(final String contractCompanyId)
    {
        this.contractCompanyId = contractCompanyId;
    }



    public String getContractCompanyId()
    {
        return contractCompanyId;
    }



    public void setContractCompanyType(final String contractCompanyType)
    {
        this.contractCompanyType = contractCompanyType;
    }



    public String getContractCompanyType()
    {
        return contractCompanyType;
    }



    public void setContractCompanyLengthOfStayHour(final String contractCompanyLengthOfStayHour)
    {
        this.contractCompanyLengthOfStayHour = contractCompanyLengthOfStayHour;
    }



    public String getContractCompanyLengthOfStayHour()
    {
        return contractCompanyLengthOfStayHour;
    }



    public void setServiceStartDate(final String serviceStartDate)
    {
        this.serviceStartDate = serviceStartDate;
    }



    public String getServiceStartDate()
    {
        return serviceStartDate;
    }



    public void setServiceStartTime(final String serviceStartTime)
    {
        this.serviceStartTime = serviceStartTime;
    }



    public String getServiceStartTime()
    {
        return serviceStartTime;
    }



    public void setServiceEndDate(final String serviceEndDate)
    {
        this.serviceEndDate = serviceEndDate;
    }



    public String getServiceEndDate()
    {
        return serviceEndDate;
    }



    public void setServiceEndTime(final String serviceEndTime)
    {
        this.serviceEndTime = serviceEndTime;
    }



    public String getServiceEndTime()
    {
        return serviceEndTime;
    }



    public void setServiceOutDate(final String serviceOutDate)
    {
        this.serviceOutDate = serviceOutDate;
    }



    public String getServiceOutDate()
    {
        return serviceOutDate;
    }



    public void setServiceOutTime(final String serviceOutTime)
    {
        this.serviceOutTime = serviceOutTime;
    }



    public String getServiceOutTime()
    {
        return serviceOutTime;
    }



    public void setServiceTimeOut(final Boolean serviceTimeOut)
    {
        this.serviceTimeOut = serviceTimeOut;
    }



    public Boolean getServiceTimeOut()
    {
        return serviceTimeOut;
    }



    public void setServiceTimeOutValue(final String serviceTimeOutValue)
    {
        this.serviceTimeOutValue = serviceTimeOutValue;
    }



    public String getServiceTimeOutValue()
    {
        return serviceTimeOutValue;
    }



    public void setStatus(final String status)
    {
        this.status = status;
    }



    public String getStatus()
    {
        return status;
    }



}
