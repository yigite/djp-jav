package com.djp.data;

public class CustomerFeatureData {

    private String code;
    private String name;
    private String other;
    private Integer prioritySort;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Integer getPrioritySort() {
        return prioritySort;
    }

    public void setPrioritySort(Integer prioritySort) {
        this.prioritySort = prioritySort;
    }
}
