package com.djp.data;

public class MultiUsagesData {
    private static final long serialVersionUID = 1L;

    private String qrType;

    private String quantity;

    private String productCode;


    private Boolean isOverSixtyFive;

    public MultiUsagesData()
    {
        // default constructor
    }



    public void setQrType(final String qrType)
    {
        this.qrType = qrType;
    }



    public String getQrType()
    {
        return qrType;
    }



    public void setQuantity(final String quantity)
    {
        this.quantity = quantity;
    }



    public String getQuantity()
    {
        return quantity;
    }



    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }



    public String getProductCode()
    {
        return productCode;
    }



    public void setIsOverSixtyFive(final Boolean isOverSixtyFive)
    {
        this.isOverSixtyFive = isOverSixtyFive;
    }



    public Boolean getIsOverSixtyFive()
    {
        return isOverSixtyFive;
    }
}
