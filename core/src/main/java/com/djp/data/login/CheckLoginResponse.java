package com.djp.data.login;

import com.djp.rest.data.ServiceResultWsData;

public class CheckLoginResponse {

    private ServiceResultWsData serviceResult;
    private String userGroups;

    public ServiceResultWsData getServiceResult() {
        return serviceResult;
    }

    public void setServiceResult(ServiceResultWsData serviceResult) {
        this.serviceResult = serviceResult;
    }

    public String getUserGroups() {
        return userGroups;
    }

   }
