package com.djp.data;

public class DjpCppInvoiceRequestData {
    private String name;
    
    private String identityNumber;
    
    private String taxOffice;


    private String email;


    private String phoneNumber;


    private String companyID;


    private String address;

    public DjpCppInvoiceRequestData()
    {
        // default constructor
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setIdentityNumber(final String identityNumber)
    {
        this.identityNumber = identityNumber;
    }



    public String getIdentityNumber()
    {
        return identityNumber;
    }



    public void setTaxOffice(final String taxOffice)
    {
        this.taxOffice = taxOffice;
    }



    public String getTaxOffice()
    {
        return taxOffice;
    }



    public void setEmail(final String email)
    {
        this.email = email;
    }



    public String getEmail()
    {
        return email;
    }



    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }



    public String getPhoneNumber()
    {
        return phoneNumber;
    }



    public void setCompanyID(final String companyID)
    {
        this.companyID = companyID;
    }



    public String getCompanyID()
    {
        return companyID;
    }



    public void setAddress(final String address)
    {
        this.address = address;
    }



    public String getAddress()
    {
        return address;
    }

}
