package com.djp.data;

import java.io.Serializable;

public  class CurrencyData  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>CurrencyData.isocode</code> property defined at extension <code>commercefacades</code>. */

    private String isocode;

    /** <i>Generated property</i> for <code>CurrencyData.name</code> property defined at extension <code>commercefacades</code>. */

    private String name;

    /** <i>Generated property</i> for <code>CurrencyData.active</code> property defined at extension <code>commercefacades</code>. */

    private boolean active;

    /** <i>Generated property</i> for <code>CurrencyData.symbol</code> property defined at extension <code>commercefacades</code>. */

    private String symbol;

    public CurrencyData()
    {
        // default constructor
    }



    public void setIsocode(final String isocode)
    {
        this.isocode = isocode;
    }



    public String getIsocode()
    {
        return isocode;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setActive(final boolean active)
    {
        this.active = active;
    }



    public boolean isActive()
    {
        return active;
    }



    public void setSymbol(final String symbol)
    {
        this.symbol = symbol;
    }



    public String getSymbol()
    {
        return symbol;
    }



}
