package com.djp.data;

public class NurusServiceLoginResponseCustomerData {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseCustomerData.Id</code> property defined at extension <code>djpcommercewebservices</code>. */

    private Integer Id;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponseCustomerData.CustomerName</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String CustomerName;

    public NurusServiceLoginResponseCustomerData()
    {
        // default constructor
    }



    public void setId(final Integer Id)
    {
        this.Id = Id;
    }



    public Integer getId()
    {
        return Id;
    }



    public void setCustomerName(final String CustomerName)
    {
        this.CustomerName = CustomerName;
    }



    public String getCustomerName()
    {
        return CustomerName;
    }


}
