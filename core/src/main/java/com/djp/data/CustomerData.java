package com.djp.data;

import java.util.Date;
import java.util.Set;

public  class CustomerData extends PrincipalData
{



    /** <i>Generated property</i> for <code>CustomerData.defaultBillingAddress</code> property defined at extension <code>commercefacades</code>. */

    private AddressData defaultBillingAddress;

    /** <i>Generated property</i> for <code>CustomerData.defaultShippingAddress</code> property defined at extension <code>commercefacades</code>. */

    private AddressData defaultShippingAddress;

    /** <i>Generated property</i> for <code>CustomerData.titleCode</code> property defined at extension <code>commercefacades</code>. */

    private String titleCode;

    /** <i>Generated property</i> for <code>CustomerData.firstName</code> property defined at extension <code>commercefacades</code>. */

    private String firstName;

    /** <i>Generated property</i> for <code>CustomerData.lastName</code> property defined at extension <code>commercefacades</code>. */

    private String lastName;

    /** <i>Generated property</i> for <code>CustomerData.currency</code> property defined at extension <code>commercefacades</code>. */

    private CurrencyData currency;

    /** <i>Generated property</i> for <code>CustomerData.language</code> property defined at extension <code>commercefacades</code>. */

    private LanguageData language;

    /** <i>Generated property</i> for <code>CustomerData.displayUid</code> property defined at extension <code>commercefacades</code>. */

    private String displayUid;

    /** <i>Generated property</i> for <code>CustomerData.customerId</code> property defined at extension <code>commercefacades</code>. */

    private String customerId;

    /** <i>Generated property</i> for <code>CustomerData.deactivationDate</code> property defined at extension <code>commercefacades</code>. */

    private Date deactivationDate;

    /** <i>Generated property</i> for <code>CustomerData.defaultAddress</code> property defined at extension <code>assistedservicefacades</code>. */

    private AddressData defaultAddress;

    /** <i>Generated property</i> for <code>CustomerData.latestCartId</code> property defined at extension <code>assistedservicefacades</code>. */

    private String latestCartId;

    /** <i>Generated property</i> for <code>CustomerData.hasOrder</code> property defined at extension <code>assistedservicefacades</code>. */

    private Boolean hasOrder;

    /** <i>Generated property</i> for <code>CustomerData.profilePicture</code> property defined at extension <code>assistedservicefacades</code>. */

    private ImageData profilePicture;

    /** <i>Generated property</i> for <code>CustomerData.occupation</code> property defined at extension <code>djpaccount</code>. */

    private CustomerOccupationData occupation;

    /** <i>Generated property</i> for <code>CustomerData.education</code> property defined at extension <code>djpaccount</code>. */

    private CustomerEducationData education;

    /** <i>Generated property</i> for <code>CustomerData.birthdayDate</code> property defined at extension <code>djpfacades</code>. */

    private Date birthdayDate;

    /** <i>Generated property</i> for <code>CustomerData.birthdate</code> property defined at extension <code>djpfacades</code>. */

    private String birthdate;

    /** <i>Generated property</i> for <code>CustomerData.identityNumber</code> property defined at extension <code>djpfacades</code>. */

    private String identityNumber;

    /** <i>Generated property</i> for <code>CustomerData.maritalStatus</code> property defined at extension <code>djpfacades</code>. */

    private String maritalStatus;

    /** <i>Generated property</i> for <code>CustomerData.childCount</code> property defined at extension <code>djpfacades</code>. */

    private Integer childCount;

    /** <i>Generated property</i> for <code>CustomerData.nationality</code> property defined at extension <code>djpfacades</code>. */

    private String nationality;

    /** <i>Generated property</i> for <code>CustomerData.packageDesc</code> property defined at extension <code>djpfacades</code>. */

    private String packageDesc;

    /** <i>Generated property</i> for <code>CustomerData.guestofPackage</code> property defined at extension <code>djpfacades</code>. */

    private String guestofPackage;

    /** <i>Generated property</i> for <code>CustomerData.email</code> property defined at extension <code>djpfacades</code>. */

    private String email;

    /** <i>Generated property</i> for <code>CustomerData.gender</code> property defined at extension <code>djpfacades</code>. */

    private String gender;

    /** <i>Generated property</i> for <code>CustomerData.country</code> property defined at extension <code>djpfacades</code>. */

    private String country;

    /** <i>Generated property</i> for <code>CustomerData.numberPlates</code> property defined at extension <code>djpfacades</code>. */

    private Set<NumberPlateData> numberPlates;

    /** <i>Generated property</i> for <code>CustomerData.isPersonnel</code> property defined at extension <code>djpfacades</code>. */

    private Boolean isPersonnel;

    /** <i>Generated property</i> for <code>CustomerData.haveDjpPass</code> property defined at extension <code>djpfacades</code>. */

    private Boolean haveDjpPass;

    /** <i>Generated property</i> for <code>CustomerData.isPartner</code> property defined at extension <code>djpfacades</code>. */

    private Boolean isPartner;

    /** <i>Generated property</i> for <code>CustomerData.partnerType</code> property defined at extension <code>djpfacades</code>. */

    private String partnerType;

    /** <i>Generated property</i> for <code>CustomerData.partnerFirm</code> property defined at extension <code>djpfacades</code>. */

    private String partnerFirm;

    /** <i>Generated property</i> for <code>CustomerData.firstNameChanged</code> property defined at extension <code>djpfacades</code>. */

    private Boolean firstNameChanged;

    /** <i>Generated property</i> for <code>CustomerData.lastNameChanged</code> property defined at extension <code>djpfacades</code>. */

    private Boolean lastNameChanged;

    /** <i>Generated property</i> for <code>CustomerData.title</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String title;

    /** <i>Generated property</i> for <code>CustomerData.phoneNumber</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String phoneNumber;

    /** <i>Generated property</i> for <code>CustomerData.registerToken</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String registerToken;

    public CustomerData()
    {
        // default constructor
    }



    public void setDefaultBillingAddress(final AddressData defaultBillingAddress)
    {
        this.defaultBillingAddress = defaultBillingAddress;
    }



    public AddressData getDefaultBillingAddress()
    {
        return defaultBillingAddress;
    }



    public void setDefaultShippingAddress(final AddressData defaultShippingAddress)
    {
        this.defaultShippingAddress = defaultShippingAddress;
    }



    public AddressData getDefaultShippingAddress()
    {
        return defaultShippingAddress;
    }



    public void setTitleCode(final String titleCode)
    {
        this.titleCode = titleCode;
    }



    public String getTitleCode()
    {
        return titleCode;
    }



    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }



    public String getFirstName()
    {
        return firstName;
    }



    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }



    public String getLastName()
    {
        return lastName;
    }



    public void setCurrency(final CurrencyData currency)
    {
        this.currency = currency;
    }



    public CurrencyData getCurrency()
    {
        return currency;
    }



    public void setLanguage(final LanguageData language)
    {
        this.language = language;
    }



    public LanguageData getLanguage()
    {
        return language;
    }



    public void setDisplayUid(final String displayUid)
    {
        this.displayUid = displayUid;
    }



    public String getDisplayUid()
    {
        return displayUid;
    }



    public void setCustomerId(final String customerId)
    {
        this.customerId = customerId;
    }



    public String getCustomerId()
    {
        return customerId;
    }



    public void setDeactivationDate(final Date deactivationDate)
    {
        this.deactivationDate = deactivationDate;
    }



    public Date getDeactivationDate()
    {
        return deactivationDate;
    }



    public void setDefaultAddress(final AddressData defaultAddress)
    {
        this.defaultAddress = defaultAddress;
    }



    public AddressData getDefaultAddress()
    {
        return defaultAddress;
    }



    public void setLatestCartId(final String latestCartId)
    {
        this.latestCartId = latestCartId;
    }



    public String getLatestCartId()
    {
        return latestCartId;
    }



    public void setHasOrder(final Boolean hasOrder)
    {
        this.hasOrder = hasOrder;
    }



    public Boolean getHasOrder()
    {
        return hasOrder;
    }



    public void setProfilePicture(final ImageData profilePicture)
    {
        this.profilePicture = profilePicture;
    }



    public ImageData getProfilePicture()
    {
        return profilePicture;
    }



    public void setOccupation(final CustomerOccupationData occupation)
    {
        this.occupation = occupation;
    }



    public CustomerOccupationData getOccupation()
    {
        return occupation;
    }



    public void setEducation(final CustomerEducationData education)
    {
        this.education = education;
    }



    public CustomerEducationData getEducation()
    {
        return education;
    }



    public void setBirthdayDate(final Date birthdayDate)
    {
        this.birthdayDate = birthdayDate;
    }



    public Date getBirthdayDate()
    {
        return birthdayDate;
    }



    public void setBirthdate(final String birthdate)
    {
        this.birthdate = birthdate;
    }



    public String getBirthdate()
    {
        return birthdate;
    }



    public void setIdentityNumber(final String identityNumber)
    {
        this.identityNumber = identityNumber;
    }



    public String getIdentityNumber()
    {
        return identityNumber;
    }



    public void setMaritalStatus(final String maritalStatus)
    {
        this.maritalStatus = maritalStatus;
    }



    public String getMaritalStatus()
    {
        return maritalStatus;
    }



    public void setChildCount(final Integer childCount)
    {
        this.childCount = childCount;
    }



    public Integer getChildCount()
    {
        return childCount;
    }



    public void setNationality(final String nationality)
    {
        this.nationality = nationality;
    }



    public String getNationality()
    {
        return nationality;
    }



    public void setPackageDesc(final String packageDesc)
    {
        this.packageDesc = packageDesc;
    }



    public String getPackageDesc()
    {
        return packageDesc;
    }



    public void setGuestofPackage(final String guestofPackage)
    {
        this.guestofPackage = guestofPackage;
    }



    public String getGuestofPackage()
    {
        return guestofPackage;
    }



    public void setEmail(final String email)
    {
        this.email = email;
    }



    public String getEmail()
    {
        return email;
    }



    public void setGender(final String gender)
    {
        this.gender = gender;
    }



    public String getGender()
    {
        return gender;
    }



    public void setCountry(final String country)
    {
        this.country = country;
    }



    public String getCountry()
    {
        return country;
    }



    public void setNumberPlates(final Set<NumberPlateData> numberPlates)
    {
        this.numberPlates = numberPlates;
    }



    public Set<NumberPlateData> getNumberPlates()
    {
        return numberPlates;
    }



    public void setIsPersonnel(final Boolean isPersonnel)
    {
        this.isPersonnel = isPersonnel;
    }



    public Boolean getIsPersonnel()
    {
        return isPersonnel;
    }


    public Boolean getPersonnel() {
        return isPersonnel;
    }

    public void setPersonnel(Boolean personnel) {
        isPersonnel = personnel;
    }

    public Boolean getHaveDjpPass() {
        return haveDjpPass;
    }

    public void setHaveDjpPass(Boolean haveDjpPass) {
        this.haveDjpPass = haveDjpPass;
    }

    public Boolean getPartner() {
        return isPartner;
    }

    public void setPartner(Boolean partner) {
        isPartner = partner;
    }

    public void setIsPartner(final Boolean isPartner)
    {
        this.isPartner = isPartner;
    }



    public Boolean getIsPartner()
    {
        return isPartner;
    }



    public void setPartnerType(final String partnerType)
    {
        this.partnerType = partnerType;
    }



    public String getPartnerType()
    {
        return partnerType;
    }



    public void setPartnerFirm(final String partnerFirm)
    {
        this.partnerFirm = partnerFirm;
    }



    public String getPartnerFirm()
    {
        return partnerFirm;
    }



    public void setFirstNameChanged(final Boolean firstNameChanged)
    {
        this.firstNameChanged = firstNameChanged;
    }



    public Boolean getFirstNameChanged()
    {
        return firstNameChanged;
    }



    public void setLastNameChanged(final Boolean lastNameChanged)
    {
        this.lastNameChanged = lastNameChanged;
    }



    public Boolean getLastNameChanged()
    {
        return lastNameChanged;
    }



    public void setTitle(final String title)
    {
        this.title = title;
    }



    public String getTitle()
    {
        return title;
    }



    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }



    public String getPhoneNumber()
    {
        return phoneNumber;
    }



    public void setRegisterToken(final String registerToken)
    {
        this.registerToken = registerToken;
    }



    public String getRegisterToken()
    {
        return registerToken;
    }



}

