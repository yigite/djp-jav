package com.djp.data;

import java.io.Serializable;

public  class PrincipalData  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>PrincipalData.uid</code> property defined at extension <code>commercefacades</code>. */

    private String uid;

    /** <i>Generated property</i> for <code>PrincipalData.name</code> property defined at extension <code>commercefacades</code>. */

    private String name;

    public PrincipalData()
    {
        // default constructor
    }



    public void setUid(final String uid)
    {
        this.uid = uid;
    }



    public String getUid()
    {
        return uid;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



}
