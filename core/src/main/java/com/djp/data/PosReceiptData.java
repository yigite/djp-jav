package com.djp.data;


import java.io.Serializable;

public  class PosReceiptData  implements Serializable {
    private static final long serialVersionUID = 1L;
    private Double receiptPrice;
    private Integer status;
    public PosReceiptData()
    {
        // default constructor
    }

    public void setReceiptPrice(final Double receiptPrice)
    {
        this.receiptPrice = receiptPrice;
    }
    public Double getReceiptPrice()
    {
        return receiptPrice;
    }
    public void setStatus(final Integer status)
    {
        this.status = status;
    }
    public Integer getStatus()
    {
        return status;
    }

}
