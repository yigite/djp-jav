package com.djp.data;

import java.io.Serializable;

public  class DjpGuestData implements Serializable
{

    private static final long serialVersionUID = 1L;
    private String name;
    private String surname;
    private String passport;
    private String tckn;
    private String phoneNumber;
    private String email;
    private String birthDate;
    private String nationality;
    private String gender;
    private String pnrCode;
    private Boolean kvkkCheck;
    private Boolean wifiCheck;
    private String wifiKey;
    private String date;
    private String flightNumber;
    private String seatCode;
    private String toAirportIATA;
    private String fromAirportIATA;
    private String classCode;
    private Boolean couldNotReadBoardingPass;
    private String passengerType;
    private String qrCode;
    private String userUid;
    private String itemCount;
    private String pageNumber;

    public DjpGuestData()
    {
        // default constructor
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setSurname(final String surname)
    {
        this.surname = surname;
    }



    public String getSurname()
    {
        return surname;
    }



    public void setPassport(final String passport)
    {
        this.passport = passport;
    }



    public String getPassport()
    {
        return passport;
    }



    public void setTckn(final String tckn)
    {
        this.tckn = tckn;
    }



    public String getTckn()
    {
        return tckn;
    }



    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }



    public String getPhoneNumber()
    {
        return phoneNumber;
    }



    public void setEmail(final String email)
    {
        this.email = email;
    }



    public String getEmail()
    {
        return email;
    }



    public void setBirthDate(final String birthDate)
    {
        this.birthDate = birthDate;
    }



    public String getBirthDate()
    {
        return birthDate;
    }



    public void setNationality(final String nationality)
    {
        this.nationality = nationality;
    }



    public String getNationality()
    {
        return nationality;
    }



    public void setGender(final String gender)
    {
        this.gender = gender;
    }



    public String getGender()
    {
        return gender;
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setKvkkCheck(final Boolean kvkkCheck)
    {
        this.kvkkCheck = kvkkCheck;
    }



    public Boolean getKvkkCheck()
    {
        return kvkkCheck;
    }



    public void setWifiCheck(final Boolean wifiCheck)
    {
        this.wifiCheck = wifiCheck;
    }



    public Boolean getWifiCheck()
    {
        return wifiCheck;
    }



    public void setWifiKey(final String wifiKey)
    {
        this.wifiKey = wifiKey;
    }



    public String getWifiKey()
    {
        return wifiKey;
    }



    public void setDate(final String date)
    {
        this.date = date;
    }



    public String getDate()
    {
        return date;
    }



    public void setFlightNumber(final String flightNumber)
    {
        this.flightNumber = flightNumber;
    }



    public String getFlightNumber()
    {
        return flightNumber;
    }



    public void setSeatCode(final String seatCode)
    {
        this.seatCode = seatCode;
    }



    public String getSeatCode()
    {
        return seatCode;
    }



    public void setToAirportIATA(final String toAirportIATA)
    {
        this.toAirportIATA = toAirportIATA;
    }



    public String getToAirportIATA()
    {
        return toAirportIATA;
    }



    public void setFromAirportIATA(final String fromAirportIATA)
    {
        this.fromAirportIATA = fromAirportIATA;
    }



    public String getFromAirportIATA()
    {
        return fromAirportIATA;
    }



    public void setClassCode(final String classCode)
    {
        this.classCode = classCode;
    }



    public String getClassCode()
    {
        return classCode;
    }



    public void setCouldNotReadBoardingPass(final Boolean couldNotReadBoardingPass)
    {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }



    public Boolean getCouldNotReadBoardingPass()
    {
        return couldNotReadBoardingPass;
    }



    public void setPassengerType(final String passengerType)
    {
        this.passengerType = passengerType;
    }



    public String getPassengerType()
    {
        return passengerType;
    }



    public void setQrCode(final String qrCode)
    {
        this.qrCode = qrCode;
    }



    public String getQrCode()
    {
        return qrCode;
    }



    public void setUserUid(final String userUid)
    {
        this.userUid = userUid;
    }



    public String getUserUid()
    {
        return userUid;
    }



    public void setItemCount(final String itemCount)
    {
        this.itemCount = itemCount;
    }



    public String getItemCount()
    {
        return itemCount;
    }



    public void setPageNumber(final String pageNumber)
    {
        this.pageNumber = pageNumber;
    }



    public String getPageNumber()
    {
        return pageNumber;
    }



}

