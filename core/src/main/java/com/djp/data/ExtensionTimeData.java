package com.djp.data;

import java.io.Serializable;

public  class ExtensionTimeData implements Serializable
{


    private static final long serialVersionUID = 1L;


    private String isocode;


    private String name;
    private String startDate;
    private String endDate;
    private String code;

    public ExtensionTimeData()
    {
        // default constructor
    }



    public void setIsocode(final String isocode)
    {
        this.isocode = isocode;
    }



    public String getIsocode()
    {
        return isocode;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
