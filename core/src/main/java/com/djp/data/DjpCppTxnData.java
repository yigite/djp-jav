package com.djp.data;

import java.math.BigDecimal;
import java.util.Date;

public class DjpCppTxnData {
    private String cppToken;

    /** <i>Generated property</i> for <code>DjpCppTxnData.amount</code> property defined at extension <code>djpcppaddon</code>. */

    private BigDecimal amount;

    /** <i>Generated property</i> for <code>DjpCppTxnData.name</code> property defined at extension <code>djpcppaddon</code>. */

    private String name;

    /** <i>Generated property</i> for <code>DjpCppTxnData.surName</code> property defined at extension <code>djpcppaddon</code>. */

    private String surName;

    /** <i>Generated property</i> for <code>DjpCppTxnData.location</code> property defined at extension <code>djpcppaddon</code>. */

    private String location;

    /** <i>Generated property</i> for <code>DjpCppTxnData.youthLoungeCampaignCode</code> property defined at extension <code>djpcppaddon</code>. */

    private String youthLoungeCampaignCode;

    /** <i>Generated property</i> for <code>DjpCppTxnData.youthLoungeEmail</code> property defined at extension <code>djpcppaddon</code>. */

    private String youthLoungeEmail;

    /** <i>Generated property</i> for <code>DjpCppTxnData.language</code> property defined at extension <code>djpcppaddon</code>. */

    private String language;

    /** <i>Generated property</i> for <code>DjpCppTxnData.reservationNumber</code> property defined at extension <code>djpcppaddon</code>. */

    private String reservationNumber;

    /** <i>Generated property</i> for <code>DjpCppTxnData.isPaid</code> property defined at extension <code>djpcppaddon</code>. */

    private Boolean isPaid;

    /** <i>Generated property</i> for <code>DjpCppTxnData.expire</code> property defined at extension <code>djpcppaddon</code>. */

    private Date expire;

    /** <i>Generated property</i> for <code>DjpCppTxnData.invoiceAddress</code> property defined at extension <code>djpcppaddon</code>. */

    private DjpCppTxnInvoiceData invoiceAddress;

    public DjpCppTxnData()
    {
        // default constructor
    }



    public void setCppToken(final String cppToken)
    {
        this.cppToken = cppToken;
    }



    public String getCppToken()
    {
        return cppToken;
    }



    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }



    public BigDecimal getAmount()
    {
        return amount;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setSurName(final String surName)
    {
        this.surName = surName;
    }



    public String getSurName()
    {
        return surName;
    }



    public void setLocation(final String location)
    {
        this.location = location;
    }



    public String getLocation()
    {
        return location;
    }



    public void setYouthLoungeCampaignCode(final String youthLoungeCampaignCode)
    {
        this.youthLoungeCampaignCode = youthLoungeCampaignCode;
    }



    public String getYouthLoungeCampaignCode()
    {
        return youthLoungeCampaignCode;
    }



    public void setYouthLoungeEmail(final String youthLoungeEmail)
    {
        this.youthLoungeEmail = youthLoungeEmail;
    }



    public String getYouthLoungeEmail()
    {
        return youthLoungeEmail;
    }



    public void setLanguage(final String language)
    {
        this.language = language;
    }



    public String getLanguage()
    {
        return language;
    }



    public void setReservationNumber(final String reservationNumber)
    {
        this.reservationNumber = reservationNumber;
    }



    public String getReservationNumber()
    {
        return reservationNumber;
    }



    public void setIsPaid(final Boolean isPaid)
    {
        this.isPaid = isPaid;
    }



    public Boolean getIsPaid()
    {
        return isPaid;
    }



    public void setExpire(final Date expire)
    {
        this.expire = expire;
    }



    public Date getExpire()
    {
        return expire;
    }



    public void setInvoiceAddress(final DjpCppTxnInvoiceData invoiceAddress)
    {
        this.invoiceAddress = invoiceAddress;
    }



    public DjpCppTxnInvoiceData getInvoiceAddress()
    {
        return invoiceAddress;
    }

}
