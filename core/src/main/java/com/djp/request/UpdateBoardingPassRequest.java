package com.djp.request;

public class UpdateBoardingPassRequest extends AccessBoardingPassRequest{


    /** <i>Generated property</i> for <code>UpdateBoardingPassRequest.tokenKey</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String tokenKey;

    public UpdateBoardingPassRequest()
    {
        // default constructor
    }



    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }



    public String getTokenKey()
    {
        return tokenKey;
    }


}
