package com.djp.request;

public class DjpCabinetTabletRequest {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>DjpCabinetTabletRequest.passengerName</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String passengerName;

    /** <i>Generated property</i> for <code>DjpCabinetTabletRequest.cabinetId</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String cabinetId;

    /** <i>Generated property</i> for <code>DjpCabinetTabletRequest.startDate</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String startDate;

    /** <i>Generated property</i> for <code>DjpCabinetTabletRequest.endDate</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String endDate;

    public DjpCabinetTabletRequest()
    {
        // default constructor
    }



    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }



    public String getPassengerName()
    {
        return passengerName;
    }



    public void setCabinetId(final String cabinetId)
    {
        this.cabinetId = cabinetId;
    }



    public String getCabinetId()
    {
        return cabinetId;
    }



    public void setStartDate(final String startDate)
    {
        this.startDate = startDate;
    }



    public String getStartDate()
    {
        return startDate;
    }



    public void setEndDate(final String endDate)
    {
        this.endDate = endDate;
    }



    public String getEndDate()
    {
        return endDate;
    }


}
