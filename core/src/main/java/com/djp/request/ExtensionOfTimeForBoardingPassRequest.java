/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Jan 18, 2024 2:04:01 PM
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.djp.request;

import com.djp.data.ExtensionTimeData;

import java.io.Serializable;
import java.util.List;


public  class ExtensionOfTimeForBoardingPassRequest  implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String pnrCode;

    private String boardingType;

    private List<ExtensionTimeData> extensionTimeList;

    public ExtensionOfTimeForBoardingPassRequest()
    {
        // default constructor
    }


    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setBoardingType(final String boardingType)
    {
        this.boardingType = boardingType;
    }



    public String getBoardingType()
    {
        return boardingType;
    }



    public void setExtensionTimeList(final List<ExtensionTimeData> extensionTimeList)
    {
        this.extensionTimeList = extensionTimeList;
    }



    public List<ExtensionTimeData> getExtensionTimeList()
    {
        return extensionTimeList;
    }



}
