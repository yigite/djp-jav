package com.djp.request;

import com.djp.data.NurusServiceCreateReservationRequestAttendeeData;

import java.util.List;

public class NurusServiceCreateReservationRequest {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.UserToken</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String UserToken;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.RDate</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String RDate;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.BeginTime</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String BeginTime;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.EndTime</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String EndTime;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.RoomID</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String RoomID;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.Subject</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Subject;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.IsCheck_In</code> property defined at extension <code>djpcommercewebservices</code>. */

    private Integer IsCheck_In;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationRequest.AttendeeDetails</code> property defined at extension <code>djpcommercewebservices</code>. */

    private List<NurusServiceCreateReservationRequestAttendeeData> AttendeeDetails;

    public NurusServiceCreateReservationRequest()
    {
        // default constructor
    }



    public void setUserToken(final String UserToken)
    {
        this.UserToken = UserToken;
    }



    public String getUserToken()
    {
        return UserToken;
    }



    public void setRDate(final String RDate)
    {
        this.RDate = RDate;
    }



    public String getRDate()
    {
        return RDate;
    }



    public void setBeginTime(final String BeginTime)
    {
        this.BeginTime = BeginTime;
    }



    public String getBeginTime()
    {
        return BeginTime;
    }



    public void setEndTime(final String EndTime)
    {
        this.EndTime = EndTime;
    }



    public String getEndTime()
    {
        return EndTime;
    }



    public void setRoomID(final String RoomID)
    {
        this.RoomID = RoomID;
    }



    public String getRoomID()
    {
        return RoomID;
    }



    public void setSubject(final String Subject)
    {
        this.Subject = Subject;
    }



    public String getSubject()
    {
        return Subject;
    }



    public void setIsCheck_In(final Integer IsCheck_In)
    {
        this.IsCheck_In = IsCheck_In;
    }



    public Integer getIsCheck_In()
    {
        return IsCheck_In;
    }



    public void setAttendeeDetails(final List<NurusServiceCreateReservationRequestAttendeeData> AttendeeDetails)
    {
        this.AttendeeDetails = AttendeeDetails;
    }



    public List<NurusServiceCreateReservationRequestAttendeeData> getAttendeeDetails()
    {
        return AttendeeDetails;
    }


}
