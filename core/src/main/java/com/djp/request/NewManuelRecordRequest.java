package com.djp.request;

public class NewManuelRecordRequest {
    private String phoneNumber;

    private String name;

    private String inputMethod;

    private String clientName;

    private String subMembership;

    private String program;

    private String whoseGuest;

    private String note;

    private String pnrCode;

    private Boolean isManuelFormOpen;

    private String manuelNameSurname;

    private String manuelFlightDate;

    private String manuelFlightCode;

    private String manuelFlightNo;

    private String manuelSeatNumber;

    private String manuelToAirport;

    private String manuelFromAirport;

    private String manuelClassCode;

    private String whereUsed;

    private String personalName;

    private Boolean couldNotReadManuelBoarding;

    public NewManuelRecordRequest()
    {
        // default constructor
    }



    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }



    public String getPhoneNumber()
    {
        return phoneNumber;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setInputMethod(final String inputMethod)
    {
        this.inputMethod = inputMethod;
    }



    public String getInputMethod()
    {
        return inputMethod;
    }



    public void setClientName(final String clientName)
    {
        this.clientName = clientName;
    }



    public String getClientName()
    {
        return clientName;
    }



    public void setSubMembership(final String subMembership)
    {
        this.subMembership = subMembership;
    }



    public String getSubMembership()
    {
        return subMembership;
    }



    public void setProgram(final String program)
    {
        this.program = program;
    }



    public String getProgram()
    {
        return program;
    }



    public void setWhoseGuest(final String whoseGuest)
    {
        this.whoseGuest = whoseGuest;
    }



    public String getWhoseGuest()
    {
        return whoseGuest;
    }



    public void setNote(final String note)
    {
        this.note = note;
    }



    public String getNote()
    {
        return note;
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setIsManuelFormOpen(final Boolean isManuelFormOpen)
    {
        this.isManuelFormOpen = isManuelFormOpen;
    }



    public Boolean getIsManuelFormOpen()
    {
        return isManuelFormOpen;
    }



    public void setManuelNameSurname(final String manuelNameSurname)
    {
        this.manuelNameSurname = manuelNameSurname;
    }



    public String getManuelNameSurname()
    {
        return manuelNameSurname;
    }



    public void setManuelFlightDate(final String manuelFlightDate)
    {
        this.manuelFlightDate = manuelFlightDate;
    }



    public String getManuelFlightDate()
    {
        return manuelFlightDate;
    }



    public void setManuelFlightCode(final String manuelFlightCode)
    {
        this.manuelFlightCode = manuelFlightCode;
    }



    public String getManuelFlightCode()
    {
        return manuelFlightCode;
    }



    public void setManuelFlightNo(final String manuelFlightNo)
    {
        this.manuelFlightNo = manuelFlightNo;
    }



    public String getManuelFlightNo()
    {
        return manuelFlightNo;
    }



    public void setManuelSeatNumber(final String manuelSeatNumber)
    {
        this.manuelSeatNumber = manuelSeatNumber;
    }



    public String getManuelSeatNumber()
    {
        return manuelSeatNumber;
    }



    public void setManuelToAirport(final String manuelToAirport)
    {
        this.manuelToAirport = manuelToAirport;
    }



    public String getManuelToAirport()
    {
        return manuelToAirport;
    }



    public void setManuelFromAirport(final String manuelFromAirport)
    {
        this.manuelFromAirport = manuelFromAirport;
    }



    public String getManuelFromAirport()
    {
        return manuelFromAirport;
    }



    public void setManuelClassCode(final String manuelClassCode)
    {
        this.manuelClassCode = manuelClassCode;
    }



    public String getManuelClassCode()
    {
        return manuelClassCode;
    }



    public void setWhereUsed(final String whereUsed)
    {
        this.whereUsed = whereUsed;
    }



    public String getWhereUsed()
    {
        return whereUsed;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



    public void setCouldNotReadManuelBoarding(final Boolean couldNotReadManuelBoarding)
    {
        this.couldNotReadManuelBoarding = couldNotReadManuelBoarding;
    }



    public Boolean getCouldNotReadManuelBoarding()
    {
        return couldNotReadManuelBoarding;
    }


}
