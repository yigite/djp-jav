package com.djp.request;

import java.io.Serializable;

public class FioriFlightInfoRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private String flightDate;

    private String flightNumber;

    public FioriFlightInfoRequest() {
        // default constructor
    }

    public void setFlightDate(final String flightDate) {
        this.flightDate = flightDate;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightNumber(final String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }
}
