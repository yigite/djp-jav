package com.djp.request;

import com.djp.data.DjpGuestData;

import java.util.List;

public class BoardingPassRequest {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>BoardingPassRequest.tokenKey</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String tokenKey;

    /** <i>Generated property</i> for <code>BoardingPassRequest.pnrCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String pnrCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.wifiKey</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String wifiKey;

    /** <i>Generated property</i> for <code>BoardingPassRequest.guests</code> property defined at extension <code>djpcommercewebservices</code>. */

    private List<DjpGuestData> guests;

    /** <i>Generated property</i> for <code>BoardingPassRequest.date</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String date;

    /** <i>Generated property</i> for <code>BoardingPassRequest.flightNumber</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String flightNumber;

    /** <i>Generated property</i> for <code>BoardingPassRequest.seatCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String seatCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.toAirportIATA</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String toAirportIATA;

    /** <i>Generated property</i> for <code>BoardingPassRequest.fromAirportIATA</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String fromAirportIATA;

    /** <i>Generated property</i> for <code>BoardingPassRequest.classCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String classCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.couldNotReadBoardingPass</code> property defined at extension <code>igacommercewebservices</code>. */

    private Boolean couldNotReadBoardingPass;

    /** <i>Generated property</i> for <code>BoardingPassRequest.passengerName</code> property defined at extension <code>igacommercewebservices</code>. */

    private String passengerName;

    /** <i>Generated property</i> for <code>BoardingPassRequest.contractID</code> property defined at extension <code>igacommercewebservices</code>. */

    private String contractID;

    /** <i>Generated property</i> for <code>BoardingPassRequest.newSale</code> property defined at extension <code>igacommercewebservices</code>. */

    private Boolean newSale;

    /** <i>Generated property</i> for <code>BoardingPassRequest.orderID</code> property defined at extension <code>igacommercewebservices</code>. */

    private String orderID;

    /** <i>Generated property</i> for <code>BoardingPassRequest.personalID</code> property defined at extension <code>igacommercewebservices</code>. */

    private String personalID;

    /** <i>Generated property</i> for <code>BoardingPassRequest.personalName</code> property defined at extension <code>igacommercewebservices</code>. */

    private String personalName;

    /** <i>Generated property</i> for <code>BoardingPassRequest.userUID</code> property defined at extension <code>igacommercewebservices</code>. */

    private String userUID;

    /** <i>Generated property</i> for <code>BoardingPassRequest.productCode</code> property defined at extension <code>igacommercewebservices</code>. */

    private String productCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.orderCode</code> property defined at extension <code>igacommercewebservices</code>. */

    private String orderCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.productCategory</code> property defined at extension <code>igacommercewebservices</code>. */

    private String productCategory;

    /** <i>Generated property</i> for <code>BoardingPassRequest.whereUsed</code> property defined at extension <code>igacommercewebservices</code>. */

    private String whereUsed;

    /** <i>Generated property</i> for <code>BoardingPassRequest.canUseService</code> property defined at extension <code>igacommercewebservices</code>. */

    private Boolean canUseService;

    /** <i>Generated property</i> for <code>BoardingPassRequest.locationID</code> property defined at extension <code>igacommercewebservices</code>. */

    private String locationID;

    /** <i>Generated property</i> for <code>BoardingPassRequest.passengerType</code> property defined at extension <code>igacommercewebservices</code>. */

    private String passengerType;

    /** <i>Generated property</i> for <code>BoardingPassRequest.adultCount</code> property defined at extension <code>igacommercewebservices</code>. */

    private Integer adultCount;

    /** <i>Generated property</i> for <code>BoardingPassRequest.childCount</code> property defined at extension <code>igacommercewebservices</code>. */

    private Integer childCount;

    /** <i>Generated property</i> for <code>BoardingPassRequest.qrCode</code> property defined at extension <code>igacommercewebservices</code>. */

    private String qrCode;

    /** <i>Generated property</i> for <code>BoardingPassRequest.passNumber</code> property defined at extension <code>igacommercewebservices</code>. */

    private String passNumber;

    /** <i>Generated property</i> for <code>BoardingPassRequest.extensionOfTime</code> property defined at extension <code>igacommercewebservices</code>. */

    private ExtensionOfTimeForBoardingPassRequest extensionOfTime;

    /** <i>Generated property</i> for <code>BoardingPassRequest.campaignNames</code> property defined at extension <code>igacommercewebservices</code>. */

    private List<String> campaignNames;

    public BoardingPassRequest()
    {
        // default constructor
    }



    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }



    public String getTokenKey()
    {
        return tokenKey;
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setWifiKey(final String wifiKey)
    {
        this.wifiKey = wifiKey;
    }



    public String getWifiKey()
    {
        return wifiKey;
    }

    public List<DjpGuestData> getGuests() {
        return guests;
    }

    public void setGuests(List<DjpGuestData> guests) {
        this.guests = guests;
    }

    public void setDate(final String date)
    {
        this.date = date;
    }



    public String getDate()
    {
        return date;
    }



    public void setFlightNumber(final String flightNumber)
    {
        this.flightNumber = flightNumber;
    }



    public String getFlightNumber()
    {
        return flightNumber;
    }



    public void setSeatCode(final String seatCode)
    {
        this.seatCode = seatCode;
    }



    public String getSeatCode()
    {
        return seatCode;
    }



    public void setToAirportIATA(final String toAirportIATA)
    {
        this.toAirportIATA = toAirportIATA;
    }



    public String getToAirportIATA()
    {
        return toAirportIATA;
    }



    public void setFromAirportIATA(final String fromAirportIATA)
    {
        this.fromAirportIATA = fromAirportIATA;
    }



    public String getFromAirportIATA()
    {
        return fromAirportIATA;
    }



    public void setClassCode(final String classCode)
    {
        this.classCode = classCode;
    }



    public String getClassCode()
    {
        return classCode;
    }



    public void setCouldNotReadBoardingPass(final Boolean couldNotReadBoardingPass)
    {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }



    public Boolean getCouldNotReadBoardingPass()
    {
        return couldNotReadBoardingPass;
    }



    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }



    public String getPassengerName()
    {
        return passengerName;
    }



    public void setContractID(final String contractID)
    {
        this.contractID = contractID;
    }



    public String getContractID()
    {
        return contractID;
    }



    public void setNewSale(final Boolean newSale)
    {
        this.newSale = newSale;
    }



    public Boolean getNewSale()
    {
        return newSale;
    }



    public void setOrderID(final String orderID)
    {
        this.orderID = orderID;
    }



    public String getOrderID()
    {
        return orderID;
    }



    public void setPersonalID(final String personalID)
    {
        this.personalID = personalID;
    }



    public String getPersonalID()
    {
        return personalID;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



    public void setUserUID(final String userUID)
    {
        this.userUID = userUID;
    }



    public String getUserUID()
    {
        return userUID;
    }



    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }



    public String getProductCode()
    {
        return productCode;
    }



    public void setOrderCode(final String orderCode)
    {
        this.orderCode = orderCode;
    }



    public String getOrderCode()
    {
        return orderCode;
    }



    public void setProductCategory(final String productCategory)
    {
        this.productCategory = productCategory;
    }



    public String getProductCategory()
    {
        return productCategory;
    }



    public void setWhereUsed(final String whereUsed)
    {
        this.whereUsed = whereUsed;
    }



    public String getWhereUsed()
    {
        return whereUsed;
    }



    public void setCanUseService(final Boolean canUseService)
    {
        this.canUseService = canUseService;
    }



    public Boolean getCanUseService()
    {
        return canUseService;
    }



    public void setLocationID(final String locationID)
    {
        this.locationID = locationID;
    }



    public String getLocationID()
    {
        return locationID;
    }



    public void setPassengerType(final String passengerType)
    {
        this.passengerType = passengerType;
    }



    public String getPassengerType()
    {
        return passengerType;
    }



    public void setAdultCount(final Integer adultCount)
    {
        this.adultCount = adultCount;
    }



    public Integer getAdultCount()
    {
        return adultCount;
    }



    public void setChildCount(final Integer childCount)
    {
        this.childCount = childCount;
    }



    public Integer getChildCount()
    {
        return childCount;
    }



    public void setQrCode(final String qrCode)
    {
        this.qrCode = qrCode;
    }



    public String getQrCode()
    {
        return qrCode;
    }



    public void setPassNumber(final String passNumber)
    {
        this.passNumber = passNumber;
    }



    public String getPassNumber()
    {
        return passNumber;
    }



    public void setExtensionOfTime(final ExtensionOfTimeForBoardingPassRequest extensionOfTime)
    {
        this.extensionOfTime = extensionOfTime;
    }



    public ExtensionOfTimeForBoardingPassRequest getExtensionOfTime()
    {
        return extensionOfTime;
    }



    public void setCampaignNames(final List<String> campaignNames)
    {
        this.campaignNames = campaignNames;
    }



    public List<String> getCampaignNames()
    {
        return campaignNames;
    }



}
