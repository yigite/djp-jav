package com.djp.request;



import com.djp.data.DjpGuestData;

import java.util.List;

public class AccessBoardingPassRequest {

    private static final long serialVersionUID = 1L;

    private String pnrCode;

    private String wifiKey;

    private List<DjpGuestData> guests;

    private String boardingType;

    private String boardingClass;

    private String date;

    private String flightNumber;

    private String seatCode;

    private String toAirportIATA;

    private String fromAirportIATA;

    private String classCode;

    private Boolean couldNotReadBoardingPass;

    private Boolean canUseService;

    private String passengerName;

    private String contractID;

    private Boolean newSale;

    private String orderID;

    private String refNumber;

    private String personalID;

    private String personalName;

    private String locationID;

    private String passengerType;

    public AccessBoardingPassRequest()
    {
        // default constructor
    }



    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }



    public String getPnrCode()
    {
        return pnrCode;
    }



    public void setWifiKey(final String wifiKey)
    {
        this.wifiKey = wifiKey;
    }



    public String getWifiKey()
    {
        return wifiKey;
    }


    public List<DjpGuestData> getGuests() {
        return guests;
    }

    public void setGuests(List<DjpGuestData> guests) {
        this.guests = guests;
    }

    public void setBoardingType(final String boardingType)
    {
        this.boardingType = boardingType;
    }



    public String getBoardingType()
    {
        return boardingType;
    }



    public void setBoardingClass(final String boardingClass)
    {
        this.boardingClass = boardingClass;
    }



    public String getBoardingClass()
    {
        return boardingClass;
    }



    public void setDate(final String date)
    {
        this.date = date;
    }



    public String getDate()
    {
        return date;
    }



    public void setFlightNumber(final String flightNumber)
    {
        this.flightNumber = flightNumber;
    }



    public String getFlightNumber()
    {
        return flightNumber;
    }



    public void setSeatCode(final String seatCode)
    {
        this.seatCode = seatCode;
    }



    public String getSeatCode()
    {
        return seatCode;
    }



    public void setToAirportIATA(final String toAirportIATA)
    {
        this.toAirportIATA = toAirportIATA;
    }



    public String getToAirportIATA()
    {
        return toAirportIATA;
    }



    public void setFromAirportIATA(final String fromAirportIATA)
    {
        this.fromAirportIATA = fromAirportIATA;
    }



    public String getFromAirportIATA()
    {
        return fromAirportIATA;
    }



    public void setClassCode(final String classCode)
    {
        this.classCode = classCode;
    }



    public String getClassCode()
    {
        return classCode;
    }



    public void setCouldNotReadBoardingPass(final Boolean couldNotReadBoardingPass)
    {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }



    public Boolean getCouldNotReadBoardingPass()
    {
        return couldNotReadBoardingPass;
    }



    public void setCanUseService(final Boolean canUseService)
    {
        this.canUseService = canUseService;
    }



    public Boolean getCanUseService()
    {
        return canUseService;
    }



    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }



    public String getPassengerName()
    {
        return passengerName;
    }



    public void setContractID(final String contractID)
    {
        this.contractID = contractID;
    }



    public String getContractID()
    {
        return contractID;
    }



    public void setNewSale(final Boolean newSale)
    {
        this.newSale = newSale;
    }



    public Boolean getNewSale()
    {
        return newSale;
    }



    public void setOrderID(final String orderID)
    {
        this.orderID = orderID;
    }



    public String getOrderID()
    {
        return orderID;
    }



    public void setRefNumber(final String refNumber)
    {
        this.refNumber = refNumber;
    }



    public String getRefNumber()
    {
        return refNumber;
    }



    public void setPersonalID(final String personalID)
    {
        this.personalID = personalID;
    }



    public String getPersonalID()
    {
        return personalID;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



    public void setLocationID(final String locationID)
    {
        this.locationID = locationID;
    }



    public String getLocationID()
    {
        return locationID;
    }



    public void setPassengerType(final String passengerType)
    {
        this.passengerType = passengerType;
    }



    public String getPassengerType()
    {
        return passengerType;
    }
}
