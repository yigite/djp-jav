package com.djp.request;

public class NurusServiceLoginRequest {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceLoginRequest.Email</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Email;

    /** <i>Generated property</i> for <code>NurusServiceLoginRequest.Password</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String Password;

    public NurusServiceLoginRequest()
    {
        // default constructor
    }



    public void setEmail(final String Email)
    {
        this.Email = Email;
    }



    public String getEmail()
    {
        return Email;
    }



    public void setPassword(final String Password)
    {
        this.Password = Password;
    }



    public String getPassword()
    {
        return Password;
    }



}
