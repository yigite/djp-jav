package com.djp.request;

import java.io.Serializable;

public  class QrForFioriRequest  implements Serializable {

    private static final long serialVersionUID = 1L;
    private String locationID;
    private String tokenKey;
    private String uniqueID;
    private String qrType;
    private String productCategory;
    private String pnrCode;
    private String boardingType;
    private String device;
    private String date;
    private String passengerName;
    private String passengerType;
    private String flightNumber;
    private String seatCode;
    private String toAirportIATA;
    private String fromAirportIATA;
    private String classCode;
    private Boolean couldNotReadBoardingPass;
    private String usedDay;
    private String usedHour;
    private String usedMinute;
    private String plateNumber;
    private Boolean newSale;
    private String userUID;
    private Boolean canUseService;
    private Boolean haveDjpPass;
    private Boolean isGuest;
    private String personalID;
    private String personalName;
    private String processType;

    public QrForFioriRequest() {
        // default constructor
    }



    public void setLocationID(final String locationID)
    {
        this.locationID = locationID;
    }

    public String getLocationID()
    {
        return locationID;
    }

    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }

    public String getTokenKey()
    {
        return tokenKey;
    }

    public void setUniqueID(final String uniqueID)
    {
        this.uniqueID = uniqueID;
    }

    public String getUniqueID()
    {
        return uniqueID;
    }

    public void setQrType(final String qrType)
    {
        this.qrType = qrType;
    }

    public String getQrType()
    {
        return qrType;
    }

    public void setProductCategory(final String productCategory)
    {
        this.productCategory = productCategory;
    }

    public String getProductCategory()
    {
        return productCategory;
    }

    public void setPnrCode(final String pnrCode)
    {
        this.pnrCode = pnrCode;
    }

    public String getPnrCode()
    {
        return pnrCode;
    }

    public void setBoardingType(final String boardingType)
    {
        this.boardingType = boardingType;
    }

    public String getBoardingType()
    {
        return boardingType;
    }

    public void setDevice(final String device)
    {
        this.device = device;
    }

    public String getDevice()
    {
        return device;
    }

    public void setDate(final String date)
    {
        this.date = date;
    }

    public String getDate()
    {
        return date;
    }

    public void setPassengerName(final String passengerName)
    {
        this.passengerName = passengerName;
    }

    public String getPassengerName()
    {
        return passengerName;
    }

    public void setPassengerType(final String passengerType)
    {
        this.passengerType = passengerType;
    }

    public String getPassengerType()
    {
        return passengerType;
    }

    public void setFlightNumber(final String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getFlightNumber()
    {
        return flightNumber;
    }

    public void setSeatCode(final String seatCode)
    {
        this.seatCode = seatCode;
    }

    public String getSeatCode()
    {
        return seatCode;
    }

    public void setToAirportIATA(final String toAirportIATA)
    {
        this.toAirportIATA = toAirportIATA;
    }

    public String getToAirportIATA()
    {
        return toAirportIATA;
    }

    public void setFromAirportIATA(final String fromAirportIATA)
    {
        this.fromAirportIATA = fromAirportIATA;
    }

    public String getFromAirportIATA()
    {
        return fromAirportIATA;
    }

    public void setClassCode(final String classCode)
    {
        this.classCode = classCode;
    }

    public String getClassCode()
    {
        return classCode;
    }

    public void setCouldNotReadBoardingPass(final Boolean couldNotReadBoardingPass) {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }

    public Boolean getCouldNotReadBoardingPass()
    {
        return couldNotReadBoardingPass;
    }

    public void setUsedDay(final String usedDay)
    {
        this.usedDay = usedDay;
    }

    public String getUsedDay()
    {
        return usedDay;
    }

    public void setUsedHour(final String usedHour)
    {
        this.usedHour = usedHour;
    }

    public String getUsedHour()
    {
        return usedHour;
    }

    public void setUsedMinute(final String usedMinute)
    {
        this.usedMinute = usedMinute;
    }

    public String getUsedMinute()
    {
        return usedMinute;
    }

    public void setPlateNumber(final String plateNumber)
    {
        this.plateNumber = plateNumber;
    }

    public String getPlateNumber()
    {
        return plateNumber;
    }

    public void setNewSale(final Boolean newSale)
    {
        this.newSale = newSale;
    }

    public Boolean getNewSale()
    {
        return newSale;
    }

    public void setUserUID(final String userUID)
    {
        this.userUID = userUID;
    }

    public String getUserUID()
    {
        return userUID;
    }

    public void setCanUseService(final Boolean canUseService)
    {
        this.canUseService = canUseService;
    }

    public Boolean getCanUseService()
    {
        return canUseService;
    }

    public void setHaveDjpPass(final Boolean haveDjpPass)
    {
        this.haveDjpPass = haveDjpPass;
    }

    public Boolean getHaveDjpPass()
    {
        return haveDjpPass;
    }

    public void setIsGuest(final Boolean isGuest)
    {
        this.isGuest = isGuest;
    }

    public Boolean getIsGuest()
    {
        return isGuest;
    }

    public void setPersonalID(final String personalID)
    {
        this.personalID = personalID;
    }

    public String getPersonalID()
    {
        return personalID;
    }

    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }

    public String getPersonalName()
    {
        return personalName;
    }

    public void setProcessType(final String processType)
    {
        this.processType = processType;
    }

    public String getProcessType()
    {
        return processType;
    }

}
