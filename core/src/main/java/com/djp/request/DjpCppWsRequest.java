package com.djp.request;

import com.djp.data.DjpCppInvoiceRequestData;

import java.math.BigDecimal;

public class DjpCppWsRequest {
    private BigDecimal amount;


    private String name;


    private String surName;


    private String location;


    private String language;


    private String paymentType;


    private String country;


    private String reservationDate;


    private String youthLoungeCampaignCode;


    private String reservationNumber;
    
    private DjpCppInvoiceRequestData invoiceDetail;

    public DjpCppWsRequest()
    {
        // default constructor
    }



    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }



    public BigDecimal getAmount()
    {
        return amount;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setSurName(final String surName)
    {
        this.surName = surName;
    }



    public String getSurName()
    {
        return surName;
    }



    public void setLocation(final String location)
    {
        this.location = location;
    }



    public String getLocation()
    {
        return location;
    }



    public void setLanguage(final String language)
    {
        this.language = language;
    }



    public String getLanguage()
    {
        return language;
    }



    public void setPaymentType(final String paymentType)
    {
        this.paymentType = paymentType;
    }



    public String getPaymentType()
    {
        return paymentType;
    }



    public void setCountry(final String country)
    {
        this.country = country;
    }



    public String getCountry()
    {
        return country;
    }



    public void setReservationDate(final String reservationDate)
    {
        this.reservationDate = reservationDate;
    }



    public String getReservationDate()
    {
        return reservationDate;
    }



    public void setYouthLoungeCampaignCode(final String youthLoungeCampaignCode)
    {
        this.youthLoungeCampaignCode = youthLoungeCampaignCode;
    }



    public String getYouthLoungeCampaignCode()
    {
        return youthLoungeCampaignCode;
    }



    public void setReservationNumber(final String reservationNumber)
    {
        this.reservationNumber = reservationNumber;
    }



    public String getReservationNumber()
    {
        return reservationNumber;
    }



    public void setInvoiceDetail(final DjpCppInvoiceRequestData invoiceDetail)
    {
        this.invoiceDetail = invoiceDetail;
    }



    public DjpCppInvoiceRequestData getInvoiceDetail()
    {
        return invoiceDetail;
    }


}
