package com.djp.request;

import com.djp.data.MultiUsagesData;

import java.util.List;

public class QrPassRequest {

    private static final long serialVersionUID = 1L;

    private String tokenKey;

    private String uniqueID;

    private String qrType;

    private String productCategory;

    private Integer usedDay;

    private Integer usedHour;

    private Integer usedMinute;

    private String plateNumber;

    private Boolean usedVale;

    private String locationID;

    private Integer adultCount;

    private Integer childCount;

    private String orderCode;

    private String entryNumber;

    private String sapOrderID;

    private String paidPrice;

    private Boolean newSaleCompleted;

    private Boolean isOverSixtyFive;

    private List<MultiUsagesData> usages;

    private String personalID;

    private String personalName;

    public QrPassRequest()
    {
        // default constructor
    }



    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }



    public String getTokenKey()
    {
        return tokenKey;
    }



    public void setUniqueID(final String uniqueID)
    {
        this.uniqueID = uniqueID;
    }



    public String getUniqueID()
    {
        return uniqueID;
    }



    public void setQrType(final String qrType)
    {
        this.qrType = qrType;
    }



    public String getQrType()
    {
        return qrType;
    }



    public void setProductCategory(final String productCategory)
    {
        this.productCategory = productCategory;
    }



    public String getProductCategory()
    {
        return productCategory;
    }



    public void setUsedDay(final Integer usedDay)
    {
        this.usedDay = usedDay;
    }



    public Integer getUsedDay()
    {
        return usedDay;
    }



    public void setUsedHour(final Integer usedHour)
    {
        this.usedHour = usedHour;
    }



    public Integer getUsedHour()
    {
        return usedHour;
    }



    public void setUsedMinute(final Integer usedMinute)
    {
        this.usedMinute = usedMinute;
    }



    public Integer getUsedMinute()
    {
        return usedMinute;
    }



    public void setPlateNumber(final String plateNumber)
    {
        this.plateNumber = plateNumber;
    }



    public String getPlateNumber()
    {
        return plateNumber;
    }



    public void setUsedVale(final Boolean usedVale)
    {
        this.usedVale = usedVale;
    }



    public Boolean getUsedVale()
    {
        return usedVale;
    }



    public void setLocationID(final String locationID)
    {
        this.locationID = locationID;
    }



    public String getLocationID()
    {
        return locationID;
    }



    public void setAdultCount(final Integer adultCount)
    {
        this.adultCount = adultCount;
    }



    public Integer getAdultCount()
    {
        return adultCount;
    }



    public void setChildCount(final Integer childCount)
    {
        this.childCount = childCount;
    }



    public Integer getChildCount()
    {
        return childCount;
    }



    public void setOrderCode(final String orderCode)
    {
        this.orderCode = orderCode;
    }



    public String getOrderCode()
    {
        return orderCode;
    }



    public void setEntryNumber(final String entryNumber)
    {
        this.entryNumber = entryNumber;
    }



    public String getEntryNumber()
    {
        return entryNumber;
    }



    public void setSapOrderID(final String sapOrderID)
    {
        this.sapOrderID = sapOrderID;
    }



    public String getSapOrderID()
    {
        return sapOrderID;
    }



    public void setPaidPrice(final String paidPrice)
    {
        this.paidPrice = paidPrice;
    }



    public String getPaidPrice()
    {
        return paidPrice;
    }



    public void setNewSaleCompleted(final Boolean newSaleCompleted)
    {
        this.newSaleCompleted = newSaleCompleted;
    }



    public Boolean getNewSaleCompleted()
    {
        return newSaleCompleted;
    }



    public void setIsOverSixtyFive(final Boolean isOverSixtyFive)
    {
        this.isOverSixtyFive = isOverSixtyFive;
    }



    public Boolean getIsOverSixtyFive()
    {
        return isOverSixtyFive;
    }



    public void setUsages(final List<MultiUsagesData> usages)
    {
        this.usages = usages;
    }



    public List<MultiUsagesData> getUsages()
    {
        return usages;
    }



    public void setPersonalID(final String personalID)
    {
        this.personalID = personalID;
    }



    public String getPersonalID()
    {
        return personalID;
    }



    public void setPersonalName(final String personalName)
    {
        this.personalName = personalName;
    }



    public String getPersonalName()
    {
        return personalName;
    }



}
