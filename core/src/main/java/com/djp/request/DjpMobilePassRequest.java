package com.djp.request;

import com.djp.data.DjpGuestData;

import java.util.List;

public class DjpMobilePassRequest {
    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>DjpMobilePassRequest.tokenKey</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String tokenKey;

    /** <i>Generated property</i> for <code>DjpMobilePassRequest.ownerPnrCode</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String ownerPnrCode;

    /** <i>Generated property</i> for <code>DjpMobilePassRequest.ownerPK</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String ownerPK;

    /** <i>Generated property</i> for <code>DjpMobilePassRequest.guests</code> property defined at extension <code>djpcommercewebservices</code>. */

    private List<DjpGuestData> guests;

    public DjpMobilePassRequest()
    {
        // default constructor
    }



    public void setTokenKey(final String tokenKey)
    {
        this.tokenKey = tokenKey;
    }



    public String getTokenKey()
    {
        return tokenKey;
    }



    public void setOwnerPnrCode(final String ownerPnrCode)
    {
        this.ownerPnrCode = ownerPnrCode;
    }



    public String getOwnerPnrCode()
    {
        return ownerPnrCode;
    }



    public void setOwnerPK(final String ownerPK)
    {
        this.ownerPK = ownerPK;
    }



    public String getOwnerPK()
    {
        return ownerPK;
    }



    public void setGuests(final List<DjpGuestData> guests)
    {
        this.guests = guests;
    }



    public List<DjpGuestData> getGuests()
    {
        return guests;
    }



}
