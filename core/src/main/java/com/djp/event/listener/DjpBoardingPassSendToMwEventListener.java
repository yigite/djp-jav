package com.djp.event.listener;

import com.djp.core.enums.QrType;
import com.djp.core.model.DjpAccessBoardingModel;
import com.djp.core.model.DjpBoardingPassModel;
import com.djp.event.DjpBoardingPassSendToMwEvent;
import com.djp.service.DjpCommerceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;


public class DjpBoardingPassSendToMwEventListener implements ApplicationListener<DjpBoardingPassSendToMwEvent> {
    private static final Logger LOG =  LoggerFactory.getLogger(DjpBoardingPassSendToMwEventListener.class);

    private DjpCommerceService djpCommerceService;
    @Override
    public void onApplicationEvent(DjpBoardingPassSendToMwEvent djpBoardingPassSendToMwEvent) {
        try {
            DjpAccessBoardingModel djpAccessBoardingModel = djpBoardingPassSendToMwEvent.getDjpAccessBoardingModel();
            DjpBoardingPassModel djpBoardingPassModel = djpBoardingPassSendToMwEvent.getDjpBoardingPassModel();
            QrType whereUsed = djpBoardingPassSendToMwEvent.getWhereUsed();

            djpCommerceService.sendMiddleWareBPInfo(djpBoardingPassModel,djpAccessBoardingModel,whereUsed);
        }catch (Exception ex){
            LOG.error("DjpBoardingPassSendToMwEventListener has an error:",ex);
        }
    }

    public DjpCommerceService getDjpCommerceService() {
        return djpCommerceService;
    }

    public void setDjpCommerceService(DjpCommerceService djpCommerceService) {
        this.djpCommerceService = djpCommerceService;
    }

}
