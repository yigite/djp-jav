package com.djp.event;

import com.djp.core.enums.QrType;
import com.djp.core.model.DjpAccessBoardingModel;
import com.djp.core.model.DjpBoardingPassModel;
import org.springframework.context.ApplicationEvent;

import java.io.Serializable;

public class DjpBoardingPassSendToMwEvent extends ApplicationEvent {

    private static final Serializable EMPTY_SOURCE = new Serializable() {
    };
    private DjpBoardingPassModel djpBoardingPassModel;
    private DjpAccessBoardingModel djpAccessBoardingModel;
    private QrType whereUsed;

    public DjpBoardingPassSendToMwEvent() {
        super(EMPTY_SOURCE);
    }

    public DjpBoardingPassModel getDjpBoardingPassModel() {
        return djpBoardingPassModel;
    }

    public void setDjpBoardingPassModel(DjpBoardingPassModel djpBoardingPassModel) {
        this.djpBoardingPassModel = djpBoardingPassModel;
    }

    public DjpAccessBoardingModel getDjpAccessBoardingModel() {
        return djpAccessBoardingModel;
    }

    public void setDjpAccessBoardingModel(DjpAccessBoardingModel djpAccessBoardingModel) {
        this.djpAccessBoardingModel = djpAccessBoardingModel;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }
}
