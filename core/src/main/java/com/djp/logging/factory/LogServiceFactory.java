package com.djp.logging.factory;

public interface LogServiceFactory {

    Object getLoggingService(String logType);
}
