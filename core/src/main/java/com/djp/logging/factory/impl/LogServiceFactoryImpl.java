package com.djp.logging.factory.impl;

import com.djp.logging.factory.LogServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.util.Map;

public class LogServiceFactoryImpl implements LogServiceFactory {

    private static final Logger LOG = LoggerFactory.getLogger(LogServiceFactoryImpl.class);

    @Resource
    private ApplicationContext applicationContext;

    @Override
    public Object getLoggingService(String logType){
        try{
            if(!StringUtils.hasText(logType)){
                return null;
            }
            return null; //applicationContext.getBean(getDynamicLogServiceMap.get(logType));
        }catch (Exception ex){
            LOG.error("getLoggingService", ex);
            return null;
        }
    }

}
