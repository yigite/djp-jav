package com.djp.dto;

public class ContractedPackagedData {

    private String classNames;
    private Integer adultGuestCount;
    private Integer childGuestCount;
    private String contractID;

    private Boolean requiredSameCompanyForGuest;

    public ContractedPackagedData()
    {
        // default constructor
    }



    public Boolean getRequiredSameCompanyForGuest() {
        return requiredSameCompanyForGuest;
    }

    public void setRequiredSameCompanyForGuest(Boolean requiredSameCompanyForGuest) {
        this.requiredSameCompanyForGuest = requiredSameCompanyForGuest;
    }

    public void setClassNames(final String classNames)
    {
        this.classNames = classNames;
    }



    public String getClassNames()
    {
        return classNames;
    }



    public void setAdultGuestCount(final Integer adultGuestCount)
    {
        this.adultGuestCount = adultGuestCount;
    }



    public Integer getAdultGuestCount()
    {
        return adultGuestCount;
    }



    public void setChildGuestCount(final Integer childGuestCount)
    {
        this.childGuestCount = childGuestCount;
    }



    public Integer getChildGuestCount()
    {
        return childGuestCount;
    }



    public void setContractID(final String contractID)
    {
        this.contractID = contractID;
    }



    public String getContractID()
    {
        return contractID;
    }


}
