package com.djp.dto;

public class LanguageWsDTO {

    private String isocode;
    private String name;
    private String nativeName;
    private Boolean active;

    public LanguageWsDTO()
    {
        // default constructor
    }



    public void setIsocode(final String isocode)
    {
        this.isocode = isocode;
    }



    public String getIsocode()
    {
        return isocode;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setNativeName(final String nativeName)
    {
        this.nativeName = nativeName;
    }



    public String getNativeName()
    {
        return nativeName;
    }



    public void setActive(final Boolean active)
    {
        this.active = active;
    }



    public Boolean getActive()
    {
        return active;
    }

}
