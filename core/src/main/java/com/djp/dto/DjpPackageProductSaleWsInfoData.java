package com.djp.dto;

public class DjpPackageProductSaleWsInfoData {

    private String code;
    private String name;
    private Integer claimed;
    private Integer used;
    private Integer remainingRightCount;
    private String orderCode;
    private String packageCode;

    public DjpPackageProductSaleWsInfoData()
    {
        // default constructor
    }



    public void setCode(final String code)
    {
        this.code = code;
    }



    public String getCode()
    {
        return code;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setClaimed(final Integer claimed)
    {
        this.claimed = claimed;
    }



    public Integer getClaimed()
    {
        return claimed;
    }



    public void setUsed(final Integer used)
    {
        this.used = used;
    }



    public Integer getUsed()
    {
        return used;
    }



    public void setRemainingRightCount(final Integer remainingRightCount)
    {
        this.remainingRightCount = remainingRightCount;
    }



    public Integer getRemainingRightCount()
    {
        return remainingRightCount;
    }



    public void setOrderCode(final String orderCode)
    {
        this.orderCode = orderCode;
    }



    public String getOrderCode()
    {
        return orderCode;
    }



    public void setPackageCode(final String packageCode)
    {
        this.packageCode = packageCode;
    }



    public String getPackageCode()
    {
        return packageCode;
    }

}
