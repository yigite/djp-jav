package com.djp.dto;

import java.util.List;

public class DjpProductSaleWsData {

    private String code;
    private String description;
    private String saleUrl;
    private String pictureUrl;
    private String packageName;
    private Integer guestCount;
    private Integer claimed;
    private List plateNumbers;
    private String orderCode;
    private String packageCode;
    private Integer adultGuestCount;
    private Integer childGuestCount;
    private Boolean isAdult;
    private Boolean isGift;
    private Boolean isPackage;
    private Integer quantity;
    private Boolean isOverSixtyFive;
    private Boolean isKiosk;
    private String expireDate;
    private List<DjpPackageProductSaleWsInfoData> premiumPackageRightDetail;
    private DjpDailyKidsUserWsData dailyKidsUser;

    public DjpProductSaleWsData()
    {
        // default constructor
    }



    public void setCode(final String code)
    {
        this.code = code;
    }



    public String getCode()
    {
        return code;
    }



    public void setDescription(final String description)
    {
        this.description = description;
    }



    public String getDescription()
    {
        return description;
    }



    public void setSaleUrl(final String saleUrl)
    {
        this.saleUrl = saleUrl;
    }



    public String getSaleUrl()
    {
        return saleUrl;
    }



    public void setPictureUrl(final String pictureUrl)
    {
        this.pictureUrl = pictureUrl;
    }



    public String getPictureUrl()
    {
        return pictureUrl;
    }



    public void setPackageName(final String packageName)
    {
        this.packageName = packageName;
    }



    public String getPackageName()
    {
        return packageName;
    }



    public void setGuestCount(final Integer guestCount)
    {
        this.guestCount = guestCount;
    }



    public Integer getGuestCount()
    {
        return guestCount;
    }



    public void setClaimed(final Integer claimed)
    {
        this.claimed = claimed;
    }



    public Integer getClaimed()
    {
        return claimed;
    }



    public void setPlateNumbers(final List plateNumbers)
    {
        this.plateNumbers = plateNumbers;
    }



    public List getPlateNumbers()
    {
        return plateNumbers;
    }



    public void setOrderCode(final String orderCode)
    {
        this.orderCode = orderCode;
    }



    public String getOrderCode()
    {
        return orderCode;
    }



    public void setPackageCode(final String packageCode)
    {
        this.packageCode = packageCode;
    }



    public String getPackageCode()
    {
        return packageCode;
    }



    public void setAdultGuestCount(final Integer adultGuestCount)
    {
        this.adultGuestCount = adultGuestCount;
    }



    public Integer getAdultGuestCount()
    {
        return adultGuestCount;
    }



    public void setChildGuestCount(final Integer childGuestCount)
    {
        this.childGuestCount = childGuestCount;
    }



    public Integer getChildGuestCount()
    {
        return childGuestCount;
    }



    public void setIsAdult(final Boolean isAdult)
    {
        this.isAdult = isAdult;
    }



    public Boolean getIsAdult()
    {
        return isAdult;
    }



    public void setIsGift(final Boolean isGift)
    {
        this.isGift = isGift;
    }



    public Boolean getIsGift()
    {
        return isGift;
    }



    public void setIsPackage(final Boolean isPackage)
    {
        this.isPackage = isPackage;
    }



    public Boolean getIsPackage()
    {
        return isPackage;
    }



    public void setQuantity(final Integer quantity)
    {
        this.quantity = quantity;
    }



    public Integer getQuantity()
    {
        return quantity;
    }



    public void setIsOverSixtyFive(final Boolean isOverSixtyFive)
    {
        this.isOverSixtyFive = isOverSixtyFive;
    }



    public Boolean getIsOverSixtyFive()
    {
        return isOverSixtyFive;
    }



    public void setIsKiosk(final Boolean isKiosk)
    {
        this.isKiosk = isKiosk;
    }



    public Boolean getIsKiosk()
    {
        return isKiosk;
    }



    public void setExpireDate(final String expireDate)
    {
        this.expireDate = expireDate;
    }



    public String getExpireDate()
    {
        return expireDate;
    }



    public void setPremiumPackageRightDetail(final List<DjpPackageProductSaleWsInfoData> premiumPackageRightDetail)
    {
        this.premiumPackageRightDetail = premiumPackageRightDetail;
    }



    public List<DjpPackageProductSaleWsInfoData> getPremiumPackageRightDetail()
    {
        return premiumPackageRightDetail;
    }

    public DjpDailyKidsUserWsData getDailyKidsUser() {
        return dailyKidsUser;
    }

    public void setDailyKidsUser(DjpDailyKidsUserWsData dailyKidsUser) {
        this.dailyKidsUser = dailyKidsUser;
    }

}
