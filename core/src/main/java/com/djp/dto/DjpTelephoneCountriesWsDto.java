package com.djp.dto;

public class DjpTelephoneCountriesWsDto {


        private String land1;
        private String landx50;
        private String telefto;

    public DjpTelephoneCountriesWsDto() {
    }

    public DjpTelephoneCountriesWsDto(String land1, String landx50, String telefto) {
            this.land1 = land1;
            this.landx50 = landx50;
            this.telefto = telefto;
        }

        public String getLand1() {
            return land1;
        }

        public void setLand1(String land1) {
            this.land1 = land1;
        }

        public String getLandx50() {
            return landx50;
        }

        public void setLandx50(String landx50) {
            this.landx50 = landx50;
        }

        public String getTelefto() {
            return telefto;
        }

        public void setTelefto(String telefto) {
            this.telefto = telefto;
        }


}
