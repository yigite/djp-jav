package com.djp.dto;

import java.io.Serializable;

public  class  PrincipalWsDTO  implements Serializable {

    private static final long serialVersionUID = 1L;
    private String uid;
    private String name;

    public PrincipalWsDTO() { }

    public void setUid(final String uid) { this.uid = uid; }

    public String getUid() { return uid; }

    public void setName(final String name) { this.name = name; }

    public String getName() { return name; }

}