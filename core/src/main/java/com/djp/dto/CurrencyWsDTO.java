package com.djp.dto;

public class CurrencyWsDTO {

    private String isocode;
    private String name;
    private Boolean active;
    private String symbol;

    public CurrencyWsDTO()
    {
        // default constructor
    }



    public void setIsocode(final String isocode)
    {
        this.isocode = isocode;
    }



    public String getIsocode()
    {
        return isocode;
    }



    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setActive(final Boolean active)
    {
        this.active = active;
    }



    public Boolean getActive()
    {
        return active;
    }



    public void setSymbol(final String symbol)
    {
        this.symbol = symbol;
    }



    public String getSymbol()
    {
        return symbol;
    }

}
