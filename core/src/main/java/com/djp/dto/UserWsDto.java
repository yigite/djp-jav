package com.djp.dto;

import com.djp.response.FioriStatusResponse;

import java.util.Date;
import java.util.List;

public class UserWsDto extends PrincipalWsDTO  {

    private AddressWsDTO defaultAddress;
    private String titleCode;
    private String title;
    private String firstName;
    private String lastName;
    private CurrencyWsDTO currency;
    private LanguageWsDTO language;
    private String displayUid;
    private String customerId;
    private Date deactivationDate;
    private String phone;
    private String passportNumber;
    private String email;
    private String birthdate;
    private String nationality;
    private String gender;
    private Boolean kvkkCheck;
    private Boolean wifiCheck;
    private String packageDesc;
    private String guestofPackage;
    private String messageCode;
    private String orderCode;
    private String imageUrl;
    private List<DjpProductSaleWsData> userProducts;
    private FioriStatusResponse result;
    private String phonePrefix;
    private Boolean isPersonnel;
    private Boolean haveDjpPass;
    private Boolean quickServiceUsage;
    private String name;



    public AddressWsDTO getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(AddressWsDTO defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getTitleCode() {
        return titleCode;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CurrencyWsDTO getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyWsDTO currency) {
        this.currency = currency;
    }

    public LanguageWsDTO getLanguage() {
        return language;
    }

    public void setLanguage(LanguageWsDTO language) {
        this.language = language;
    }

    public String getDisplayUid() {
        return displayUid;
    }

    public void setDisplayUid(String displayUid) {
        this.displayUid = displayUid;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getKvkkCheck() {
        return kvkkCheck;
    }

    public void setKvkkCheck(Boolean kvkkCheck) {
        this.kvkkCheck = kvkkCheck;
    }

    public Boolean getWifiCheck() {
        return wifiCheck;
    }

    public void setWifiCheck(Boolean wifiCheck) {
        this.wifiCheck = wifiCheck;
    }

    public String getPackageDesc() {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc) {
        this.packageDesc = packageDesc;
    }

    public String getGuestofPackage() {
        return guestofPackage;
    }

    public void setGuestofPackage(String guestofPackage) {
        this.guestofPackage = guestofPackage;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<DjpProductSaleWsData> getUserProducts() {
        return userProducts;
    }

    public void setUserProducts(List<DjpProductSaleWsData> userProducts) {
        this.userProducts = userProducts;
    }

    public FioriStatusResponse getResult() {
        return result;
    }

    public void setResult(FioriStatusResponse result) {
        this.result = result;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Boolean getPersonnel() {
        return isPersonnel;
    }

    public void setPersonnel(Boolean personnel) {
        isPersonnel = personnel;
    }

    public Boolean getHaveDjpPass() {
        return haveDjpPass;
    }

    public void setHaveDjpPass(Boolean haveDjpPass) {
        this.haveDjpPass = haveDjpPass;
    }

    public Boolean getQuickServiceUsage() {
        return quickServiceUsage;
    }

    public void setQuickServiceUsage(Boolean quickServiceUsage) {
        this.quickServiceUsage = quickServiceUsage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
