package com.djp.dto;

import java.io.Serializable;
import java.util.Date;

public  class DjpDailyKidsUserWsData  implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String name;

    private String surname;

    private Date birthDate;

    public DjpDailyKidsUserWsData()
    {
        // default constructor
    }

    public void setName(final String name)
    {
        this.name = name;
    }



    public String getName()
    {
        return name;
    }



    public void setSurname(final String surname)
    {
        this.surname = surname;
    }



    public String getSurname()
    {
        return surname;
    }

    public void setBirthDate(final Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

}