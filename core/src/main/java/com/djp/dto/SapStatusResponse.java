package com.djp.dto;

import java.util.List;

public class SapStatusResponse {

    private String type;
    private String message;
    private String customerType;
    private List<ContractedPackagedData> packages;
    private String contractID;
    private Boolean quickServiceUsage;

    public SapStatusResponse()
    {
        // default constructor
    }



    public void setType(final String type)
    {
        this.type = type;
    }



    public String getType()
    {
        return type;
    }



    public void setMessage(final String message)
    {
        this.message = message;
    }



    public String getMessage()
    {
        return message;
    }



    public void setCustomerType(final String customerType)
    {
        this.customerType = customerType;
    }



    public String getCustomerType()
    {
        return customerType;
    }



    public void setPackages(final List<ContractedPackagedData> packages)
    {
        this.packages = packages;
    }



    public List<ContractedPackagedData> getPackages()
    {
        return packages;
    }



    public void setContractID(final String contractID)
    {
        this.contractID = contractID;
    }



    public String getContractID()
    {
        return contractID;
    }



    public void setQuickServiceUsage(final Boolean quickServiceUsage)
    {
        this.quickServiceUsage = quickServiceUsage;
    }



    public Boolean getQuickServiceUsage()
    {
        return quickServiceUsage;
    }

}
