package com.djp.dto;

public class CountryWsDTO {

    private String isocode;
    private String name;

    public CountryWsDTO() {
    }

    public CountryWsDTO(String isocode, String name) {
        this.isocode = isocode;
        this.name = name;
    }

    public String getIsocode() {
        return isocode;
    }

    public void setIsocode(String isocode) {
        this.isocode = isocode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
