package com.djp.modelservice.model;

import com.djp.modelservice.impl.JsonParsable;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.util.CollectionUtils;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Observable;
import java.util.Set;

@MappedSuperclass
public class ItemModel extends Observable implements Cloneable {

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

//    @Transient
//    private ItemModel previousState;

//    @Transient
//    private ItemModel previousState;
//
//    @PostLoad
//    private void setPreviousState() throws CloneNotSupportedException {
//        previousState = this.clone();
//    }
//
//    public ItemModel getPreviousState(){
//        return previousState;
//    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ItemModel clone() throws CloneNotSupportedException
    {
        return (ItemModel)super.clone();
    }
}
