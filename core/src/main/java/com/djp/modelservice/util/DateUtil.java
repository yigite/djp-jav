package com.djp.modelservice.util;


import com.djp.core.enums.PeriodOfValidEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DateUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DateUtil.class);

    @Value("${add.minute.on.expire.date}")
    private static String minuteOnExpireDate;
    private static final String[] dateFormats = {"yyyy-MM-dd","yyyy/MM/dd","dd/MM/yyyy","dd-MM-yyyy"};
    private static final String PLUS_TIMEFORMAT = " HH:mm:ss";


    public static final String formatDate(Date date) {
        if (date == null)
            date = new Date();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);

    }

    public static Date addHourByPropertyMinute(Date date) {
        int addMinuteValue = Integer.parseInt(minuteOnExpireDate);

        if(addMinuteValue > 0){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, addMinuteValue);
            return calendar.getTime();
        }else {
            return date;
        }
    }

    public static Date generateDateByReservationDate(Date currentDate,Date reservationDate) {
        try {
            int addMinuteValue = Integer.parseInt(minuteOnExpireDate);
            if(addMinuteValue > 0){
                int compareValue = differenceHoursBetweenTwoDate(currentDate, reservationDate);
                if(compareValue > 96){
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(currentDate);
                    calendar.add(Calendar.MINUTE, addMinuteValue);
                    return calendar.getTime();
                }else if(compareValue <= 24){
                    return reservationDate;
                }else {
                    int addHourOnCurrentDate = compareValue - 24;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(currentDate);
                    calendar.add(Calendar.HOUR, addHourOnCurrentDate);
                    return calendar.getTime();
                }

            }else {
                return currentDate;
            }
        }catch (Exception ex){
            return currentDate;
        }
    }
    public static final Date convertDate(String strDate) {


        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return (date);
    }

    public static Date createDateWithTime(String dateString){
        if(!StringUtils.hasText(dateString)){
            return null;
        }
        for(String s : dateFormats){
            SimpleDateFormat sdf = new SimpleDateFormat(s + PLUS_TIMEFORMAT);
            try{
                Date d = sdf.parse(dateString);
                return d;
            } catch (Exception e){

            }
        }
        return null;
    }

    public static Date createDateWithoutTime(String dateString){
        if(!StringUtils.hasText(dateString)){
            return null;
        }
        for(String s : dateFormats){
            SimpleDateFormat sdf = new SimpleDateFormat(s);
            try{
                Date d = sdf.parse(dateString);
                return d;
            } catch (Exception e){

            }
        }
        return null;
    }

    public static Date createDateWithTimeNow(String dateString){
        if(!StringUtils.hasText(dateString)){
            return null;
        }
        for(String s : dateFormats){
            SimpleDateFormat sdf = new SimpleDateFormat(s);
            try{
                Date d = sdf.parse(dateString);
                Date now=new Date();
                d.setHours(now.getHours());
                d.setMinutes(now.getMinutes());
                d.setSeconds(now.getSeconds());
                return d;
            } catch (Exception e){

            }
        }
        return null;
    }

    public static String getDateStringWithFormat(Date date){
        if(date==null){
            return "";
        }
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year  = localDate.getYear();
        String monthString = localDate.getMonthValue()<10?"0"+localDate.getMonthValue():localDate.getMonthValue()+"";
        String dayString   = localDate.getDayOfMonth()<10?"0"+localDate.getDayOfMonth():localDate.getDayOfMonth()+"";
        String hourString =date.getHours()<10?"0"+date.getHours():date.getHours()+"";
        String minutesString =date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()+"";
        String secondsString =date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds()+"";
        return dayString+"/"+monthString+"/"+year+" "+hourString+":"+minutesString+":"+secondsString;
    }

    public static String getDateStringForOtopark(Date date){
        if(date==null){
            return "";
        }
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year  = localDate.getYear();
        String monthString = localDate.getMonthValue()<10?"0"+localDate.getMonthValue():localDate.getMonthValue()+"";
        String dayString   = localDate.getDayOfMonth()<10?"0"+localDate.getDayOfMonth():localDate.getDayOfMonth()+"";
        String hourString =date.getHours()<10?"0"+date.getHours():date.getHours()+"";
        String minutesString =date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()+"";
        String secondsString =date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds()+"";
        return monthString+"/"+dayString+"/"+year+" "+hourString+":"+minutesString+":"+secondsString+".000";
    }

    public static String getDateStringWithFormatWithoutTime(Date date){
        if(date==null){
            return "";
        }
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year  = localDate.getYear();
        String monthString = localDate.getMonthValue()<10?"0"+localDate.getMonthValue():localDate.getMonthValue()+"";
        String dayString   = localDate.getDayOfMonth()<10?"0"+localDate.getDayOfMonth():localDate.getDayOfMonth()+"";
        return dayString+"/"+monthString+"/"+year;
    }


    public static String getDateStringForBakingSystems(Date date,int beforeDate){
        if(date==null){
            return "";
        }
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        localDate.plusDays(beforeDate);
        int year  = localDate.getYear();
        String monthString = localDate.getMonthValue()<10?"0"+localDate.getMonthValue():localDate.getMonthValue()+"";
        String dayString   = localDate.getDayOfMonth()<10?"0"+localDate.getDayOfMonth():localDate.getDayOfMonth()+"";

        return year+"-"+monthString+"-"+dayString;
    }

    public static String getDateStringWithTrFormat(Date date){
        if(date==null){
            return "";
        }
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year  = localDate.getYear();
        String monthString = localDate.getMonthValue()<10?"0"+localDate.getMonthValue():localDate.getMonthValue()+"";
        String dayString   = localDate.getDayOfMonth()<10?"0"+localDate.getDayOfMonth():localDate.getDayOfMonth()+"";
        return dayString+"/"+monthString+"/"+year;
    }

    public static Date getDateAddYears(final int years){
        // Get current date
        Date currentDate = new Date();
        // convert date to localdatetime // plus years
        LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusYears(years);
        // convert LocalDateTime to date
        Date currentDatePlus = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        return currentDatePlus;

    }

    public static final String formatToTurkishDate(String date) {

        String returnVal="";
        try{
            String[] dateStr=date.split("-");
            return dateStr[2]+"/"+dateStr[1]+"/"+dateStr[0];
        } catch (Exception e){
            return date;
        }
    }

    public static int dateCompare(Date expireDate,Date currentDate) throws ParseException {
        return expireDate.compareTo(currentDate);
    }



    public static Calendar addOneYearToOrderDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 1);
        return calendar;
    }

    public static Calendar addYearsToDate(Date date, int years) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        return calendar;
    }

    public static Calendar addMonthToOrderDate(Date date, int months) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, months);
        return calendar;
    }

    public static Calendar addDayToDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar;
    }

    public static Calendar subtractDayToDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -day);
        return calendar;
    }


    public static Calendar subOneYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -1);
        return calendar;
    }


    public static Calendar addOneYearToOrderDateAndSubOneDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 1);
        calendar.add(Calendar.HOUR,-24);
        return calendar;
    }

    public static Date sub48HoursCurrentDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR,-48);
        return calendar.getTime();
    }

    public static Date subTenTimeCurrentDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE,-10);
        return calendar.getTime();
    }

    public static Date addMinuteToDate(Date date, Integer minute){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE,minute);
        return calendar.getTime();
    }
    public static Date subOneTimeCurrentDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE,-1);
        return calendar.getTime();
    }

    public static Date convertDateAndAddOneDay(String date) {

        Date convertDate = convertDateForContractedTickets(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(convertDate);
        calendar.add(Calendar.HOUR, 24);
        return calendar.getTime();

    }

    public static Date addOneDay(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, 24);
        return calendar.getTime();

    }
    public static Date addOneHour(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, 1);
        return calendar.getTime();

    }



    public static Date convertDateAndSubOneDay(String date) {

        Date convertDate = convertDateForContractedTickets(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(convertDate);
        calendar.add(Calendar.HOUR, -24);
        return calendar.getTime();

    }

    private static Date convertDateForContractedTickets(String date) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();

        Date expireDateForContractedTickets = null;
        try {
            String timeInExpireDate = time.format(currentDate);
            expireDateForContractedTickets = df.parse(date + " " + timeInExpireDate);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return expireDateForContractedTickets;
    }

    public static Date convertStartDate(String date) {


        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        Date startDate = null;
        try {
            startDate = df.parse(date);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return startDate;
    }

    public static Date convertExpireDateForDjpVoucher(String date) {


        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date expireDateForDjpVoucher = null;
        try {
            String timeInExpireDate = "23:59:59";
            expireDateForDjpVoucher = df.parse(date+" "+timeInExpireDate);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return expireDateForDjpVoucher;
    }

    public static Date convertStartDateForRenewedRemaining(String date) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();

        Date startDateForRenewedRemaining = null;
        try {
            String timeInExpireDate = time.format(currentDate);
            startDateForRenewedRemaining = df.parse(date + " " + timeInExpireDate);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return startDateForRenewedRemaining;
    }

    public static Date convertJodaStringToDate(String date) {


        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        try {
            return  df.parse(date);
        } catch (ParseException e) {
            LOG.error("convertJodaStringToDateExp:",e);
        }

        return null;
    }


    public static Date convertJulianDateToDate(String julianDate) {
        DateFormat fmt1 = new SimpleDateFormat("yyyyDDD");
        DateFormat fmt2 = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String julian = fmt2.format(date) + julianDate;
        try {
            date = fmt1.parse(julian);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return date;
    }

    public static final String formatDateForSap(Date date) {
        if (date == null)
            date = new Date();

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(date);

    }

    public static final String formatTimeForSap(Date date) {
        if (date == null)
            date = new Date();

        SimpleDateFormat df = new SimpleDateFormat("HHmmss");
        return df.format(date);

    }

    public static String toDatePattern(Date date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        if(Objects.nonNull(date)){
            return formatter.format(date);
        }else {
            return "";
        }

    }

    public static Date generateExpireDateForMeetProduct(Date flightDate) {


        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(flightDate);
        calendar.add(Calendar.HOUR,24);

        return calendar.getTime();
    }

    public static int differenceHoursBetweenTwoDate(Date date1,Date date2){
        long diff = date2.getTime() - date1.getTime();

        int diffhours = (int) (diff / (60 * 60 * 1000));
        return diffhours;
    }

    public static int differenceMinutesBetweenTwoDate(Date date1,Date date2){
        long diff = date2.getTime() - date1.getTime();

        int diffhours = (int) (diff / (60 * 1000));
        return diffhours;
    }

    public static Date stringToDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date1 = null;
        try {
            date1 = df.parse(date);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }
        return date1;
    }

    public static Long getSleepTime(Date date) {
        try {
            Date startDateForCurrentDay = stringToDateAndTime(toDatePattern(new Date(),"dd/MM/yyyy"),"dd/MM/yyyy");
            long longValue = Long.parseLong(toDatePattern(date,"SSS"));
            String stringDiff = String.valueOf(startDateForCurrentDay.getTime() - date.getTime());
            long diff = Long.parseLong(stringDiff.substring(stringDiff.length() - 4,stringDiff.length()));
            return customCalculatedSleepTime(longValue, diff);
        } catch (Exception ex) {
            LOG.error("error while getSleepTime ",ex);
            return 1500L;
        }

    }

    private static Long customCalculatedSleepTime(long longValue, long diff) {
        long resultLongValue = 0L;
        if (diff < 3000L && longValue < 250L) {
            return 3000L + (longValue * 2);
        } else {
            if(diff < 3000L && longValue < 500L){
                return 1500L + diff;
            }else if(diff < 3000L && longValue < 750L){
                return 2000L + (diff / 2);
            } else if (diff > 3000L && diff < 6000L) {
                resultLongValue = diff / 2;
                resultLongValue = resultLongValue > 2000L && resultLongValue < 2500L ? resultLongValue + (longValue / 2) : resultLongValue ;
                resultLongValue = resultLongValue > 2000L && resultLongValue < 2500L ? resultLongValue + (diff / 4) : resultLongValue ;
                return addMsValue(longValue,resultLongValue,diff);
            } else {
                resultLongValue = diff / 4;
                resultLongValue = resultLongValue < 2250 && diff > 7500 ? resultLongValue - longValue : resultLongValue;
                return addMsValue(longValue,resultLongValue,diff);
            }
        }
    }

    private static Long addMsValue(long longValue, long result,long diff) {

        if (result < 3000L) {
            if(diff < 7500 && longValue < 500L) {
                if (longValue < 250L) {
                    result = result + (250 + (longValue * 3 / 2));
                } else {
                    result = result + (500 + (longValue / 2));
                }
                result = result > 1250L && result < 2250L ? (result / 2) + longValue : result > 2250L ? (result / 3 * 2) + (longValue / 2) : result;
                result = result > 1700L ? result + longValue : result;
            }else {
                if(result > 2000 && longValue > 600){
                    result = result - longValue;
                }else if((result > 1000 && result < 1500) && longValue > 700){
                    result = result + (longValue * 3/2);
                }
            }
        }
        return result;
    }


    public static String dateToString(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return df.format(date);
    }

    public static String dateToStringWithoutTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(date);
    }

    public static Date convertDateForQrForCard(String date) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date expireDateForQrForCard = null;
        try {
            expireDateForQrForCard = df.parse(date);
        } catch (ParseException pe) {
            LOG.error("error while parsing date " + pe.getMessage(), pe);
        }

        return expireDateForQrForCard;
    }

    public static boolean compareDateWithNowDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 14);
        int compareTo = calendar.getTime().compareTo(new Date());
        if(compareTo < 0){
            return Boolean.TRUE;
        }else {
            return Boolean.FALSE;
        }
    }

    public static Date stringToDateAndTime(String dateAndTime,String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(dateAndTime);
    }

    public static Date generateCalculatedDateForJob(Date dateToBeCalculated,Date startTime){
        if(Objects.nonNull(dateToBeCalculated)){
            return dateToBeCalculated;
        }else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startTime);
            calendar.add(Calendar.HOUR, -24);
            return calendar.getTime();
        }
    }

    public static Date addHoursToDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

    public static Date addMinutesFifteenBufferToDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, 15);
        return calendar.getTime();
    }

    public static String formatDateForBoardingList(Date date){
        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }

    public static String formatTimeForBoardingList(Date date){
        return new SimpleDateFormat("HH:mm").format(date);
    }

    public static String differentFindDateForHour(Date date1, Date date2){
        Instant instant1 = date1.toInstant();
        Instant instant2 = date2.toInstant();
        LocalDateTime dateTime1 = LocalDateTime.ofInstant(instant1, ZoneId.systemDefault());
        LocalDateTime dateTime2 = LocalDateTime.ofInstant(instant2, ZoneId.systemDefault());

        Duration diff = Duration.between(dateTime1,dateTime2);

        long day = diff.toDays();
        long hour = diff.toHours() % 24;
        long minute = (diff.toMinutes() % 60);

        return day + "G " + hour + "S " + minute + "D";
    }

    public static Date parseExtensionDate(String dateStr) {
        try {
            return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse(dateStr);

        } catch (Exception e){
            LOG.error("Error parseExtensionDate : ", e);
        }
        return null;
    }
    public static String daysInfoBetween(Date expireDate) {
        Date today = new Date();
        long daysBetween = (expireDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
        long daysInfoBetween = (daysBetween*100)/365;
        return String.valueOf(daysInfoBetween);
    }
    public static Date addForPeriodValidToDate(Date date, PeriodOfValidEnum periodOfValidEnum, Integer days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if(PeriodOfValidEnum.DAILY.equals(periodOfValidEnum)){
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }else if(PeriodOfValidEnum.WEEKLY.equals(periodOfValidEnum)){
            calendar.add(Calendar.DAY_OF_YEAR, 7);
        }else if(PeriodOfValidEnum.MONTHLY.equals(periodOfValidEnum)){
            calendar.add(Calendar.MONTH, 1);
        }else if(PeriodOfValidEnum.YEARLY.equals(periodOfValidEnum)){
            calendar.add(Calendar.YEAR, 1);
        }else if(PeriodOfValidEnum.NDAYS.equals(periodOfValidEnum)){
            calendar.add(Calendar.DAY_OF_YEAR, Objects.nonNull(days) ? days : 0);
        }
        return calendar.getTime();
    }
}
