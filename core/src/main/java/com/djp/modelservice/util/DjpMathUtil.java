package com.djp.modelservice.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DjpMathUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DjpMathUtil.class);

    public static double round(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
