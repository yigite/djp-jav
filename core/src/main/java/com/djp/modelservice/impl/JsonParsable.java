package com.djp.modelservice.impl;

public interface JsonParsable {
    public String toJsonString();
}
