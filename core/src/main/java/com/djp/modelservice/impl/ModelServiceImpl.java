package com.djp.modelservice.impl;

import com.djp.modelservice.ModelService;

import com.djp.modelservice.util.HibernateUtils;
import org.hibernate.Interceptor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

@Repository
@Transactional
public class ModelServiceImpl implements ModelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelServiceImpl.class);
    private static final String FROM = "FROM ";
    private static final String WHERE_CODE = " where code=:code";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public Boolean saveOrUpdate(Object model) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.saveOrUpdate(model);
            return true;
        } catch (Exception e) {
            LOGGER.error("ModelService saveOrUpdate: ", e);
            return false;
        }
    }

    @Override
    public Boolean refresh(Object model) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.refresh(model);
            return true;
        } catch (Exception e) {
            LOGGER.error("ModelService refresh: ", e);
            return false;
        }
    }

    @Override
    public Boolean delete(Object model) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            LOGGER.error("ModelService delete: ", e);
            return false;
        }
    }

    @Override
    public Boolean merge(Object model) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.merge(model);
            return true;
        } catch (Exception e) {
            LOGGER.error("ModelService merge: ", e);
            return false;
        }
    }

    @Override
    public Boolean persist(Object model) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(model);
            return true;
        } catch (Exception e) {
            LOGGER.error("ModelService merge: ", e);
            return false;
        }
    }

    @Override
    public Object get(Class entityClass, Long id) {
        try {
            Session session = sessionFactory.getCurrentSession();
            return session.get(entityClass, id);
        } catch (Exception e) {
            LOGGER.error("ModelService get: ", e);
            return null;
        }
    }

    @Override
    public Object getByCode(Class entityClass, String code) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(FROM + entityClass.getSimpleName() + WHERE_CODE, entityClass)
                    .setParameter("code", code);
            List objectList = query.getResultList();
            boolean getDjpBoarding = !objectList.isEmpty();
            return getDjpBoarding ? objectList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("ModelService getByCode: ", e);
            return null;
        }
    }

    @Override
    public <T> List<T> getAll(Class<T> entityClass) {
        try {
            Session session = sessionFactory.getCurrentSession();
            String query = FROM + entityClass.getSimpleName();
            return session.createQuery(query, entityClass).getResultList();
        } catch (Exception e) {
            LOGGER.error("ModelService getAll: ", e);
            return Collections.emptyList();
        }
    }

//    this function needs to be checked
    @Override
    public <T> T clone(Object var1, Class<T> var2) {
        if (var1 == null) {
            return null;
        }

        if (!var2.isAssignableFrom(var1.getClass())) {
            throw new IllegalArgumentException("Target class is not compatible with the object's class");
        }

        try {
            Method cloneMethod = var1.getClass().getMethod("clone");
            return var2.cast(cloneMethod.invoke(var1));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new UnsupportedOperationException("Clone method is not accessible or does not exist");
        }
    }

//    this function needs to be checked
    @Override
    public <T> T clone(T var1) {
        if (var1 == null) {
            return null;
        }
        try {
            Method cloneMethod = var1.getClass().getMethod("clone");
            return (T) var1.getClass().cast(cloneMethod.invoke(var1));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new UnsupportedOperationException("Clone method is not accessible or does not exist");
        }
    }

}
