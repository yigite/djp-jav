package com.djp.modelservice;

public enum StatusEnum {

    ACTIVE("ACTIVE", "Aktif", "Active"),
    PASSIVE("PASSIVE","Pasif", "Passive"),
    SAVED("SAVED","Kayıt Edildi", "Saved");

    private final String trDefinition;
    private final String engDefinition;
    private final String code;

    StatusEnum(String code, String trDefinition, String engDefinition) {
        this.trDefinition = trDefinition;
        this.engDefinition = engDefinition;
        this.code = code;
    }

    public String getTrName() {
        return trDefinition;
    }
    public String getEngName() {
        return engDefinition;
    }
    public String getCode(){
        return code;
    }
}
