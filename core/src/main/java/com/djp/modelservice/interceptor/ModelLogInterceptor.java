package com.djp.modelservice.interceptor;





import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import jakarta.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

public class ModelLogInterceptor extends EmptyInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelLogInterceptor.class);
    


    public void onDelete(Object entity,
                         Serializable id,
                         Object[] state,
                         String[] propertyNames,
                         Type[] types) {
   /*     if(!(entity instanceof GeneralLogModel)){
            getLogService().createGeneralLogData(entity, id, state, propertyNames, types, ActionTypeEnum.DELETED);
        }*/
    }

    public boolean onSave(Object entity,
                          Serializable id,
                          Object[] state,
                          String[] propertyNames,
                          Type[] types) {

   /*     if(entity instanceof GeneralLogModel ||
                entity instanceof UserLogModel ||
                entity instanceof DataLogModel ||
                entity instanceof ApplicationLogModel ||
                entity instanceof DocumentLogModel){
            return true;
        }
        getLogService().createGeneralLogData(entity, id, state, propertyNames, types, ActionTypeEnum.CREATED);*/
        return false;
    }

    public boolean onFlushDirty(Object entity,
                                Serializable id,
                                Object[] currentState,
                                Object[] previousState,
                                String[] propertyNames,
                                Type[] types) {

    /*    if(entity instanceof GeneralLogModel){
            return true;
        }
        Method hasChanged = ReflectionUtils.findMethod(entity.getClass(), "hasChanged");
        try {
            if(Boolean.TRUE.equals(hasChanged.invoke(entity) )){
                getLogService().createGeneralLogDataForUpdate(entity, id, currentState, previousState, propertyNames, types, ActionTypeEnum.UPDATED);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }*/
        return true;
    }

  /*  private DataLogServiceImpl getLogService(){
        return (DataLogServiceImpl) logServiceFactory.getLoggingService("DATA");
    }*/

    private boolean checkUpdatedDate(Object entity){
    /*    Method method = ReflectionUtils.findMethod(entity.getClass(), "getUpdatedAt") != null? ReflectionUtils.findMethod(entity.getClass(), "getUpdatedAt"): ReflectionUtils.findMethod(entity.getClass(), "getUpdatedDate");
        if(method != null){
            try {
                Date updatedDate = (Date) method.invoke(entity);
                if(updatedDate != null){
                    Date today = new Date();
                    if(DateUtils.isSameDay(new Date(), updatedDate)) {
                        return true;
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }*/
        return false;
    }
}