package com.djp.modelservice;

import com.djp.core.model.DjpBoardingPassModel;
import com.djp.core.model.DjpMobilePassModel;

import java.util.List;

public interface ModelService {

    Boolean saveOrUpdate(Object model);

    Boolean refresh(Object model);

    Boolean delete(Object model);

    Boolean persist(Object model);

    Object get(Class entityClass, Long id);

    Boolean merge(Object model);

    Object getByCode(Class entityClass, String code);

    <T> List<T> getAll(Class<T> entityClass);

    <T> T clone(Object var1, Class<T> var2);

    <T> T clone(T var1);}
