package com.djp.rest.data;

public class ServiceResultWsData {

    private static final long serialVersionUID = 1L;

    private String message;

    private Integer statusCode;

    private String resultType;

    private String resultValidationMessage;

    private Integer transactionCode;
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getResultValidationMessage() {
        return resultValidationMessage;
    }

    public void setResultValidationMessage(String resultValidationMessage) {
        this.resultValidationMessage = resultValidationMessage;
    }

    public Integer getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(Integer transactionCode) {
        this.transactionCode = transactionCode;
    }
}
