package com.djp.rest.data;

import java.util.List;

public class ContractedCompaniesResponse {

    private List<ContractedCompaniesResponseData> contractedCompaniesList;

    private ServiceResultWsData serviceResult;

    public List<ContractedCompaniesResponseData> getContractedCompaniesList() {
        return contractedCompaniesList;
    }

    public void setContractedCompaniesList(List<ContractedCompaniesResponseData> contractedCompaniesList) {
        this.contractedCompaniesList = contractedCompaniesList;
    }

    public ServiceResultWsData getServiceResult() {
        return serviceResult;
    }

    public void setServiceResult(ServiceResultWsData serviceResult) {
        this.serviceResult = serviceResult;
    }
}
