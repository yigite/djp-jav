package com.djp.rest.data;

public class BoardingPassFlightDateResponse {

    private String boardingFlightDate;

    private String diffBetweenToDate;

    private String gate;

    private ServiceResultWsData serviceResult;

    public String getBoardingFlightDate() {
        return boardingFlightDate;
    }

    public void setBoardingFlightDate(String boardingFlightDate) {
        this.boardingFlightDate = boardingFlightDate;
    }

    public String getDiffBetweenToDate() {
        return diffBetweenToDate;
    }

    public void setDiffBetweenToDate(String diffBetweenToDate) {
        this.diffBetweenToDate = diffBetweenToDate;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public ServiceResultWsData getServiceResult() {
        return serviceResult;
    }

    public void setServiceResult(ServiceResultWsData serviceResult) {
        this.serviceResult = serviceResult;
    }
}
