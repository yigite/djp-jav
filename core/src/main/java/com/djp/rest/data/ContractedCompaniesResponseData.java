package com.djp.rest.data;

import java.util.List;

public class ContractedCompaniesResponseData {

    private String companyName;

    private String companyContractedType;

    private List<String> classNames;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyContractedType() {
        return companyContractedType;
    }

    public void setCompanyContractedType(String companyContractedType) {
        this.companyContractedType = companyContractedType;
    }

    public List<String> getClassNames() {
        return classNames;
    }

    public void setClassNames(List<String> classNames) {
        this.classNames = classNames;
    }
}
