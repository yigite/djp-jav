package com.djp.rest.data;

public class BoardingPassFlightDateRequest {

    private String pnrCode;

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }
}
