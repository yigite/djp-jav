package com.djp.rest.data;

import java.io.Serializable;

public  class UserLoginResponse  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>UserLoginResponse.userGroups</code> property defined at extension <code>igacommercewebservices</code>. */

    private String userGroups;

    /** <i>Generated property</i> for <code>UserLoginResponse.serviceResult</code> property defined at extension <code>igacommercewebservices</code>. */

    private ServiceResultWsData serviceResult;

    public UserLoginResponse()
    {
        // default constructor
    }



    public void setUserGroups(final String userGroups)
    {
        this.userGroups = userGroups;
    }



    public String getUserGroups()
    {
        return userGroups;
    }



    public void setServiceResult(final ServiceResultWsData serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public ServiceResultWsData getServiceResult()
    {
        return serviceResult;
    }



}
