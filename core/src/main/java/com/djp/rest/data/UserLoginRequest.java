package com.djp.rest.data;

/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at Nov 17, 2023 12:21:11 AM
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

import java.io.Serializable;

public  class UserLoginRequest  implements Serializable
{

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>UserLoginRequest.userName</code> property defined at extension <code>igacommercewebservices</code>. */

    private String userName;

    /** <i>Generated property</i> for <code>UserLoginRequest.password</code> property defined at extension <code>igacommercewebservices</code>. */

    private String password;

    public UserLoginRequest()
    {
        // default constructor
    }



    public void setUserName(final String userName)
    {
        this.userName = userName;
    }



    public String getUserName()
    {
        return userName;
    }



    public void setPassword(final String password)
    {
        this.password = password;
    }



    public String getPassword()
    {
        return password;
    }



}
