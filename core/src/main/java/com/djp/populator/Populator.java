package com.djp.populator;

import com.djp.exception.ConversionException;

public interface Populator<S, T>{

    void populate(S source, T target) throws ConversionException;

    T populate(S source) throws ConversionException;
}
