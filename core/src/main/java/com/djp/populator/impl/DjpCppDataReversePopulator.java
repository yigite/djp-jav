package com.djp.populator.impl;

import com.djp.core.model.DjpCppTxnModel;
import com.djp.data.DjpCppTxnData;
import com.djp.exception.ConversionException;
import com.djp.populator.Populator;

import java.util.Objects;

public class DjpCppDataReversePopulator implements Populator<DjpCppTxnData,DjpCppTxnModel> {
    @Override
    public void populate(DjpCppTxnData source, DjpCppTxnModel target) throws ConversionException {
        if(Objects.nonNull(source.getCppToken())){
            target.setCppToken(source.getCppToken());
        }
        if(Objects.nonNull(source.getAmount())){
            target.setAmount(source.getAmount());
        }
        if(Objects.nonNull(source.getName())){
            target.setName(source.getName());
        }
        if(Objects.nonNull(source.getReservationNumber())){
            target.setReservationNumber(source.getReservationNumber());
        }
        if(Objects.nonNull(source.getSurName())){
            target.setSurName(source.getSurName());
        }
        if(Objects.nonNull(source.getLocation())){
            target.setLocation(source.getLocation());
        }
        if(Objects.nonNull(source.getLanguage())){
            target.setLanguage(source.getLanguage());
        }
        if(Objects.nonNull(source.getYouthLoungeCampaignCode())){
            target.setYouthLoungeCampaignCode(source.getYouthLoungeCampaignCode());
        }
        if(Objects.nonNull(source.getYouthLoungeEmail())){
            target.setYouthLoungeEmail(source.getYouthLoungeEmail());
        }
    }

    @Override
    public DjpCppTxnModel populate(DjpCppTxnData source) throws ConversionException {
        DjpCppTxnModel djpCppTxnModel = new DjpCppTxnModel();
        if(Objects.nonNull(source.getCppToken())){
            djpCppTxnModel.setCppToken(source.getCppToken());
        }
        if(Objects.nonNull(source.getAmount())){
            djpCppTxnModel.setAmount(source.getAmount());
        }
        if(Objects.nonNull(source.getName())){
            djpCppTxnModel.setName(source.getName());
        }
        if(Objects.nonNull(source.getReservationNumber())){
            djpCppTxnModel.setReservationNumber(source.getReservationNumber());
        }
        if(Objects.nonNull(source.getSurName())){
            djpCppTxnModel.setSurName(source.getSurName());
        }
        if(Objects.nonNull(source.getLocation())){
            djpCppTxnModel.setLocation(source.getLocation());
        }
        if(Objects.nonNull(source.getLanguage())){
            djpCppTxnModel.setLanguage(source.getLanguage());
        }
        if(Objects.nonNull(source.getYouthLoungeCampaignCode())){
            djpCppTxnModel.setYouthLoungeCampaignCode(source.getYouthLoungeCampaignCode());
        }
        if(Objects.nonNull(source.getYouthLoungeEmail())){
            djpCppTxnModel.setYouthLoungeEmail(source.getYouthLoungeEmail());
        }
        return djpCppTxnModel;
    }
}
