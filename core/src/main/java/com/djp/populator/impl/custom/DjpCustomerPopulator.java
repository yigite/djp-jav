package com.djp.populator.impl.custom;

import com.djp.core.model.*;
import com.djp.data.CustomerData;
import com.djp.exception.ConversionException;
import com.djp.populator.impl.CustomerPopulator;
import com.djp.util.DjpDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class DjpCustomerPopulator extends CustomerPopulator {

    private static final Logger LOG = LoggerFactory.getLogger(DjpCustomerPopulator.class);
    /*@Resource
    private Converter<MediaModel, ImageData> imageConverter;*/

    @Override
    public void populate(CustomerModel source, CustomerData target) {
        super.populate(source,target);
        target.setEmail(source.getContactEmail());
        target.setBirthdate(source.getBirthDate() != null? DjpDateUtil.date2String(source.getBirthDate()): null);
        //target.setNationality(source.getNationality() !=  null ? source.getNationality().getCode() : null);
        target.setPhoneNumber(source.getUid());
        target.setRegisterToken(source.getRegisterToken());
        //target.setGender(source.getGender()!=null ? Localization.getLocalizedString("type.enumType."+source.getGender().getCode().toLowerCase()+".name"): "");
        //target.setProfilePicture(source.getProfilePicture()!=null?imageConverter.convert(source.getProfilePicture()):null);
        target.setCountry(Objects.nonNull(source.getCountry()) ? source.getCountry().getIsoCode() : null);
        //populateNumberPlates(source, target);
        target.setIsPersonnel(source.getIsPersonnel());
        target.setHaveDjpPass(source.getHaveDjpPass());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setChildCount(source.getChildCount());
        //target.setFirstNameChanged(BooleanUtils.isTrue(source.getFirstNameChanged()));
        //target.setLastNameChanged(BooleanUtils.isTrue(source.getLastNameChanged()));
        /*if(Objects.nonNull(source.getMaritalStatus())) {
            target.setMaritalStatus(source.getMaritalStatus().getCode());
        }*/
        target.setIdentityNumber(source.getTckn());

        //populatePartnerDetail(source, target);
    }

    /*private void populatePartnerDetail(CustomerModel source, CustomerData target) {
        if(Boolean.TRUE.equals(source.getIsPartner())){
            target.setIsPartner(source.getIsPartner());
            target.setPartnerFirm(source.getPartnerFirm());
            target.setPartnerType(source.getPartnerType());
        }
    }

    private void populateNumberPlates(CustomerModel source, CustomerData target) {
        if(!CollectionUtils.isEmpty(source.getNumberPlates())) {
            Set<NumberPlateData> numberPlateDataList = new HashSet<>();
            source.getNumberPlates().stream().forEach(numberPlateModel -> {
                        NumberPlateData numberPlateData = new NumberPlateData();
                        numberPlateData.setCode(numberPlateModel.getCode());
                        numberPlateData.setPk(numberPlateModel.getPk().toString());
                        numberPlateDataList.add(numberPlateData);
                    }
            );
            target.setNumberPlates(numberPlateDataList);
        }
    }*/

    protected void setUid(final UserModel source, final CustomerData target)
    {
        target.setUid(source.getUid());
        if (source instanceof CustomerModel)
        {
            final CustomerModel customer = (CustomerModel) source;
            if (customer.getOriginalUid() != null)
            {
                target.setDisplayUid(customer.getOriginalUid());
            }
        }
    }

    @Override
    public CustomerData populate(CustomerModel source) throws ConversionException {
        CustomerData target = new CustomerData();
        this.populate(source, target);
        return target;
    }
}
