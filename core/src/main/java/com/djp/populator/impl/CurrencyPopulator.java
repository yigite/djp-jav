package com.djp.populator.impl;

import com.djp.core.model.CurrencyModel;
import com.djp.data.CurrencyData;
import com.djp.exception.ConversionException;
import com.djp.populator.Populator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class CurrencyPopulator implements Populator<CurrencyModel, CurrencyData> {

    private static final Logger LOG = LoggerFactory.getLogger(CurrencyPopulator.class);

    @Override
    public void populate(CurrencyModel source, CurrencyData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setIsocode(source.getIsocode());
        target.setName(source.getName());
        target.setActive(source.isActive());
        target.setSymbol(source.getSymbol());
    }

    @Override
    public CurrencyData populate(CurrencyModel source) throws ConversionException {
        CurrencyData target = new CurrencyData();
        this.populate(source, target);
        return target;
    }
}
