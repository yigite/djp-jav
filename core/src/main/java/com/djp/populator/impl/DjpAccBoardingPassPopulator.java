package com.djp.populator.impl;

import com.djp.core.enums.QrType;
import com.djp.core.model.BaseStoreModel;
import com.djp.core.model.ContractedCompaniesModel;
import com.djp.core.model.DjpAccessBoardingModel;
import com.djp.data.DjpBoardingPassData;
import com.djp.data.DjpBoardingServiceInfoData;
import com.djp.exception.ConversionException;
import com.djp.modelservice.util.DateUtil;
import com.djp.populator.Populator;
import com.djp.service.BaseStoreService;
import com.djp.service.ContractedCompanyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DjpAccBoardingPassPopulator implements Populator<DjpAccessBoardingModel, DjpBoardingPassData> {

    @Resource
    ContractedCompanyService djpContractedCompanyService;
    @Resource
    BaseStoreService baseStoreService;
    @Override
    public void populate(DjpAccessBoardingModel source, DjpBoardingPassData target) throws ConversionException {
        target.setCreationTime(source.getCreationtime());
        target.setPnrCode(source.getPnrCode());
        target.setFormatCode(source.getFormatCode());
        target.setNumberOfLegsEncoded(source.getNumberOfLegsEncoded());
        target.setPassengerName(source.getPassengerName());
        target.setElectronicTicketIndicator(source.getElectronicTicketIndicator());
        target.setOperatingCarrierPnrCode(source.getOperatingCarrierPnrCode());
        target.setOperatingCarrierDesignator(source.getOperatingCarrierDesignator());
        target.setToCityAirportCode(source.getToCityAirportCode());
        target.setFromCityAirportCode(source.getFromCityAirportCode());
        target.setFlightNumber(source.getFlightNumber());
        target.setDateOfFlight(source.getDateOfFlight());
        target.setCompartmentCode(source.getCompartmentCode());
        target.setSeatNumber(source.getSeatNumber());
        target.setCheckInSequenceNumber(source.getCheckInSequenceNumber());
        target.setPassengerStatus(source.getPassengerStatus());
        target.setNewSale(source.getNewSale());
        target.setIsOwner(!source.getDjpAccBoardingPass().isEmpty());
        target.setBoardingTypeClassName("ABP");
        if (StringUtils.isNotBlank(source.getRefNumber())){
            target.setTokenKey(source.getRefNumber());
        } else if (!CollectionUtils.isEmpty(source.getRefNumberList())){
            target.setTokenKey(String.join(",", source.getRefNumberList()));
        }
        List<String> boardingUsages = new ArrayList<>();
        for (QrType boardingUsage : source.getBoardingUsages()) {
            boardingUsages.add(boardingUsage.getCode());
        }
        target.setWhereUsed(boardingUsages);
        target.setSapOrderID(source.getSapOrderID());
        target.setCouldNotReadBoardingPass(source.getCouldNotReadBoardingPass());
        if (Objects.nonNull(source.getPersonalID())) {
            target.setPersonalID(source.getPersonalID());
        }
        if (Objects.nonNull(source.getPersonalName())) {
            target.setPersonalName(source.getPersonalName());
        }

        if (source.getBoardingUsages().contains(QrType.LOUNGE_DOMESTIC)){
            populateServiceUsingDetail(source,target);
        }
    }

    private void populateServiceUsingDetail(DjpAccessBoardingModel source, DjpBoardingPassData target){
        DjpBoardingServiceInfoData boardingServiceInfo = new DjpBoardingServiceInfoData();
        if (Objects.nonNull(source.getBoardingPassUsingData())){
            if (StringUtils.isNotBlank(source.getContractID())) {
                ContractedCompaniesModel companiesModel = djpContractedCompanyService.getContractedCompany(source.getContractID());
                if (Objects.nonNull(companiesModel)) {
                    if(Objects.nonNull(companiesModel.getContractedType()) && Objects.nonNull(companiesModel.getLengthOfStayHour())){
                        boardingServiceInfo.setContractCompanyId(companiesModel.getCompanyId());
                        boardingServiceInfo.setContractCompanyName(companiesModel.getCompanyName());
                        boardingServiceInfo.setContractCompanyType(Objects.nonNull(companiesModel.getContractedType()) ? companiesModel.getContractedType().getCode() : "");
                        boardingServiceInfo.setContractCompanyLengthOfStayHour(String.valueOf(companiesModel.getLengthOfStayHour()));
                    }
                }
            } else {
                BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                Integer individualStayHour = 0;
                if (Objects.nonNull(baseStoreModel)){
                    individualStayHour = baseStoreModel.getIndividualLengthOfStayHour();
                } else {
                    individualStayHour = 6;
                }
                boardingServiceInfo.setContractCompanyId("");
                boardingServiceInfo.setContractCompanyName("");
                boardingServiceInfo.setContractCompanyType("Individual");
                boardingServiceInfo.setContractCompanyLengthOfStayHour(String.valueOf(individualStayHour));
            }
            boardingServiceInfo.setServiceStartDate(DateUtil.formatDateForBoardingList(source.getBoardingPassUsingData().getScheduledOutStartDate()));
            boardingServiceInfo.setServiceStartTime(DateUtil.formatTimeForBoardingList(source.getBoardingPassUsingData().getScheduledOutStartDate()));
            boardingServiceInfo.setServiceEndDate(DateUtil.formatDateForBoardingList(source.getBoardingPassUsingData().getScheduledOutEndDate()));
            boardingServiceInfo.setServiceEndTime(DateUtil.formatTimeForBoardingList(source.getBoardingPassUsingData().getScheduledOutEndDate()));
            boardingServiceInfo.setStatus(Objects.nonNull(source.getBoardingPassUsingData().getStatus()) ? source.getBoardingPassUsingData().getStatus().getCode() : "");
            Date bNewDate = new Date();
            boardingServiceInfo.setServiceTimeOut((bNewDate.after(source.getBoardingPassUsingData().getScheduledOutEndDate())));
            boardingServiceInfo.setServiceTimeOutValue(DateUtil.differentFindDateForHour(source.getBoardingPassUsingData().getScheduledOutStartDate(), bNewDate));
            boardingServiceInfo.setServiceOutDate((Objects.nonNull(source.getServiceOutDate()) ? DateUtil.formatDateForBoardingList(source.getServiceOutDate()) : ""));
            boardingServiceInfo.setServiceOutTime((Objects.nonNull(source.getServiceOutDate()) ? DateUtil.formatTimeForBoardingList(source.getServiceOutDate()) : ""));
        }

        target.setContractCompanyInfo(boardingServiceInfo);
    }

    @Override
    public DjpBoardingPassData populate(DjpAccessBoardingModel source) throws ConversionException {
        DjpBoardingPassData target = new DjpBoardingPassData();
        this.populate(source, target);
        return target;
    }
}
