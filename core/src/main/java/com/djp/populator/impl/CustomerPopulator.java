package com.djp.populator.impl;

import com.djp.core.model.*;
import com.djp.data.CurrencyData;
import com.djp.data.CustomerData;
import com.djp.data.LanguageData;
import com.djp.populator.Populator;
import org.springframework.core.convert.ConversionException;
import org.springframework.util.Assert;

public class CustomerPopulator implements Populator<CustomerModel,CustomerData> {


    private Populator<CurrencyModel, CurrencyData> currencyPopulator;
    private Populator<LanguageModel, LanguageData> languagePopulator;

    @Override
    public void populate(CustomerModel source, CustomerData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");


        if (source.getSessionCurrency() != null)
        {
            target.setCurrency(currencyPopulator.populate(source.getSessionCurrency()));
        }
        if (source.getSessionLanguage() != null)
        {
            target.setLanguage(languagePopulator.populate(source.getSessionLanguage()));
        }

        final String[] names = source.getName().split(" ");
        if (names != null) {
            target.setFirstName(names[0]);
            target.setLastName(names[1]);
        }

        final TitleModel title = source.getTitle();
        if (title != null)
        {
            target.setTitleCode(title.getCode());
        }

        target.setName(source.getName());
        setUid(source, target);
        target.setCustomerId(source.getCustomerId());
        target.setDeactivationDate(source.getDeactivationDate());
    }

    @Override
    public CustomerData populate(CustomerModel source) throws com.djp.exception.ConversionException {
        CustomerData target = new CustomerData();
        this.populate(source, target);
        return target;
    }


    protected void setUid(final UserModel source, final CustomerData target) {
        target.setUid(source.getUid());
        if (source instanceof CustomerModel) {
            final CustomerModel customer = (CustomerModel) source;
            if (isOriginalUidAvailable(customer)) {
                target.setDisplayUid(customer.getOriginalUid());
            }
        }
    }

    protected boolean isOriginalUidAvailable(final CustomerModel source) {
        return source.getOriginalUid() != null;
    }

    public Populator<CurrencyModel, CurrencyData> getCurrencyPopulator() {
        return currencyPopulator;
    }

    public void setCurrencyPopulator(Populator<CurrencyModel, CurrencyData> currencyPopulator) {
        this.currencyPopulator = currencyPopulator;
    }

    public Populator<LanguageModel, LanguageData> getLanguagePopulator() {
        return languagePopulator;
    }

    public void setLanguagePopulator(Populator<LanguageModel, LanguageData> languagePopulator) {
        this.languagePopulator = languagePopulator;
    }
}

