package com.djp.populator.impl;

import com.djp.core.model.DjpDailyKidsUserModel;
import com.djp.dto.DjpDailyKidsUserWsData;
import com.djp.exception.ConversionException;
import com.djp.populator.Populator;

import java.util.Objects;

public class DjpDailyKidsUserModelPopulator implements Populator<DjpDailyKidsUserModel, DjpDailyKidsUserWsData> {

    @Override
    public void populate(DjpDailyKidsUserModel source, DjpDailyKidsUserWsData target) throws ConversionException {

        if (Objects.nonNull(source.getName())) {
            target.setName(source.getName());
        }
        if (Objects.nonNull(source.getSurname())) {
            target.setSurname(source.getSurname());
        }
        if (Objects.nonNull(source.getBirthDate())) {
            target.setBirthDate(source.getBirthDate());
        }

    }

    @Override
    public DjpDailyKidsUserWsData populate(DjpDailyKidsUserModel source) throws ConversionException {
        DjpDailyKidsUserWsData target = new DjpDailyKidsUserWsData();
        this.populate(source, target);
        return target;
    }
}
