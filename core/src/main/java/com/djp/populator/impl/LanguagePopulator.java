package com.djp.populator.impl;

import com.djp.core.model.LanguageModel;
import com.djp.data.LanguageData;
import com.djp.exception.ConversionException;
import com.djp.populator.Populator;
import org.apache.commons.lang.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Locale;

public class LanguagePopulator implements Populator<LanguageModel, LanguageData> {

    private static final Logger LOG = LoggerFactory.getLogger(LanguagePopulator.class);

    @Override
    public void populate(LanguageModel source, LanguageData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setIsocode(source.getIsocode());
        target.setName(source.getName());
        target.setActive(source.isActive());
        //target.setNativeName(source.getName(getPersistenceContext()));
    }

    @Override
    public LanguageData populate(LanguageModel source) throws ConversionException {
        LanguageData target = new LanguageData();
        this.populate(source, target);
        return target;
    }

    protected Locale toLocale(final LanguageModel source)
    {
        return LocaleUtils.toLocale(source.getIsocode());
    }
}
