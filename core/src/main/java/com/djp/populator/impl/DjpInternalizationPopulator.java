package com.djp.populator.impl;

import com.djp.core.model.DialingCodeModel;
import com.djp.data.DialingCodeData;
import com.djp.populator.Populator;
import org.springframework.core.convert.ConversionException;
import org.springframework.util.Assert;

public class DjpInternalizationPopulator implements Populator<DialingCodeModel, DialingCodeData> {


    @Override
    public void populate(DialingCodeModel source, DialingCodeData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        target.setIsoCode(source.getIsoCode());
        target.setDialingCode(source.getDialingCode());
    }

    @Override
    public DialingCodeData populate(DialingCodeModel source) throws com.djp.exception.ConversionException {
        DialingCodeData target = new DialingCodeData();
        this.populate(source, target);
        return target;
    }
}
