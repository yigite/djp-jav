package com.djp.populator.impl;

import com.djp.core.model.FlightInformationModel;
import com.djp.data.FioriFlightDetailData;
import com.djp.populator.Populator;

import java.util.Objects;

public class DjpFioriFlightPopulator implements Populator<FlightInformationModel, FioriFlightDetailData> {
    @Override
    public void populate(FlightInformationModel source, FioriFlightDetailData target) {
        if (Objects.nonNull(source.getDepartureLocation())) {
            target.setFromAirport(source.getDepartureLocation());
        }
        if (Objects.nonNull(source.getArrivalLocation())) {
            target.setToAirport(source.getArrivalLocation());
        }
    }

    @Override
    public FioriFlightDetailData populate(FlightInformationModel source){
        if(Objects.isNull(source))
        {
            return null;
        }
        FioriFlightDetailData target = new FioriFlightDetailData();
        this.populate(source, target);
        return target;
    }
}
