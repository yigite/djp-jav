package com.djp.populator.impl;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.PremiumPackagePassengerEnum;
import com.djp.core.model.*;
import com.djp.dto.DjpDailyKidsUserWsData;
import com.djp.dto.DjpProductSaleWsData;
import com.djp.exception.ConversionException;
import com.djp.modelservice.util.DateUtil;
import com.djp.populator.Populator;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

public class DjpProductSaleWsDataPopulator implements Populator<DjpRemainingUsageModel, DjpProductSaleWsData> {


    private static final Logger LOG = LoggerFactory.getLogger(DjpProductSaleWsDataPopulator.class);

    @Resource
    private HashMap<String, String> productCodeMapForTestEnvironment;

    @Value("${server.host.type}")
    private String serverHostType;

    @Value("${website.djpstore.https}")
    private String webisteDjpstoreHttps;

    @Value("${website.djpstore.https.forMobileMedia}")
    private String websiteDjpstoreHttpsForMobileMedia;



    @Resource
    private Populator<DjpDailyKidsUserModel, DjpDailyKidsUserWsData> djpDailyKidsUserModelPopulator;



    @Override
    public void populate(DjpRemainingUsageModel source, DjpProductSaleWsData target) throws ConversionException {

        boolean isShouldBeChangedProductCode = Boolean.FALSE;
        isShouldBeChangedProductCode = "TEST".equals(serverHostType) && productCodeMapForTestEnvironment.containsKey(source.getProduct().getCode());
        String productName="";
        if(source.getProduct() instanceof ERPVariantProductModel){
            productName=((ERPVariantProductModel) source.getProduct()).getBaseProduct().getName();
        } else{
            productName=source.getProduct().getName();
        }
        target.setOrderCode(source.getOrderCode());
        target.setSaleUrl(webisteDjpstoreHttps + "/product/p/"+(source.getPackagedProduct()!=null?source.getPackagedProduct().getCode():source.getProduct().getCode()));
        target.setDescription(source.getProduct().getName());
        target.setCode(Boolean.FALSE.equals(isShouldBeChangedProductCode) ? source.getProduct().getCode() : productCodeMapForTestEnvironment.get(source.getProduct().getCode()));
        if (Objects.nonNull(source.getDailyKidsUser())) {
            target.setDailyKidsUser(djpDailyKidsUserModelPopulator.populate(source.getDailyKidsUser()));
        }
        if (Objects.nonNull(source.getPackagedProduct())) {
            if (!CollectionUtils.isEmpty(source.getPackagedProduct().getData_sheet())) {
                target.setPictureUrl(websiteDjpstoreHttpsForMobileMedia + "/" + source.getPackagedProduct().getData_sheet().iterator().next().getDownloadURL().replaceAll("&attachment=true", ""));
            } else {
                target.setPictureUrl("");
            }
        } else {
            target.setClaimed(source.getClaimed());
            if (source.getProduct() instanceof ERPVariantProductModel) {
                ERPVariantProductModel product = (ERPVariantProductModel) source.getProduct();
                if (!CollectionUtils.isEmpty(product.getBaseProduct().getData_sheet())) {
                    target.setPictureUrl(websiteDjpstoreHttpsForMobileMedia + "/" + product.getBaseProduct().getData_sheet().iterator().next().getDownloadURL().replaceAll("&attachment=true", ""));
                } else {
                    target.setPictureUrl("");
                }
            } else {
                ProductModel product = source.getProduct();
                if (!CollectionUtils.isEmpty(product.getData_sheet())) {
                    target.setPictureUrl(websiteDjpstoreHttpsForMobileMedia + "/" + product.getData_sheet().iterator().next().getDownloadURL().replaceAll("&attachment=true", ""));
                } else {
                    target.setPictureUrl("");
                }
            }
        }


        target.setPackageName(source.getPackagedProduct()!=null?source.getPackagedProduct().getName():productName);
        if (Objects.nonNull(source.getPackagedProduct())) {
            Optional<ProductBomComponentModel> optionalRemaining = source.getPackagedProduct().getBomComponentList().stream()
                    .filter(bomComponentModel -> bomComponentModel.getProduct().getCode().equals(source.getProduct().getCode()))
                    .findFirst();
            target.setGuestCount(optionalRemaining.isPresent() ? Objects.nonNull(optionalRemaining.get().getGuestQuantity()) ?  optionalRemaining.get().getGuestQuantity() : 0 : 0);
        } else {
            target.setGuestCount(0);
        }
        if(source.getProduct().getCode().equals(DjpCoreConstants.CAR_PARKING_CODE)) {
            target.setClaimed(source.getClaimed() - source.getUsed());
            CustomerModel user = (CustomerModel) source.getUser();
            if(CollectionUtils.isNotEmpty(user.getNumberPlates())){
                List<NumberPlateModel> numberPlates = new ArrayList<>(user.getNumberPlates());
                List<String> collect = numberPlates.stream().filter(numberPlateModel -> !numberPlateModel.getCode().equals("0")).map(NumberPlateModel::getCode).collect(Collectors.toList());
                target.setPlateNumbers(collect);
            }
        }
        target.setPackageCode(Objects.nonNull(source.getPackagedProduct()) ? source.getPackagedProduct().getCode() : "");
        target.setIsPackage(Objects.nonNull(source.getPackagedProduct()) ? Boolean.TRUE : Boolean.FALSE);
        populateAdultAndChildGuestCount(source,target);
        target.setQuantity(source.getClaimed() - source.getUsed());
        if(Objects.nonNull(source.getExpireDate())) {
            target.setExpireDate(DateUtil.dateToString(source.getExpireDate()));
        }

    }

    private void populateAdultAndChildGuestCount(DjpRemainingUsageModel source, DjpProductSaleWsData target) {
        if (Objects.isNull(source.getPackagedProduct())) {
            ProductModel product = source.getProduct();
            if (product instanceof ERPVariantProductModel) {
                for (ProductFeatureModel productFeature : product.getFeatures()) {
                    try {
                        if (productFeature.getValue() instanceof ClassificationAttributeValueModel) {
                            ClassificationAttributeValueModel c = (ClassificationAttributeValueModel) productFeature.getValue();
                            if (c.getCode().startsWith(DjpCoreConstants.DjpVariantFeature.ZMMDJP2_STARTWITH) && c.getCode().equals(DjpCoreConstants.DjpVariantFeature.ADULT)) {
                                Integer resultClaimed = source.getClaimed() - source.getUsed();
                                resultClaimed = resultClaimed - 1;
                                target.setAdultGuestCount(resultClaimed);
                                target.setIsAdult(Boolean.TRUE);
                            }
                            if (c.getCode().startsWith(DjpCoreConstants.DjpVariantFeature.ZMMDJP2_STARTWITH) && c.getCode().equals(DjpCoreConstants.DjpVariantFeature.CHILD)) {
                                Integer resultClaimed = source.getClaimed() - source.getUsed();
                                resultClaimed = resultClaimed - 1;
                                target.setChildGuestCount(resultClaimed);
                                target.setIsAdult(Boolean.FALSE);
                            }
                        }
                    } catch (Exception e) {
                        LOG.error("populateAdultAndChildGuestCount has error", e);
                    }
                }
            }
        }else {
            PremiumPackagePassengerEnum passengerType = source.getPassengerType();
            if(Objects.nonNull(passengerType)){
                if(passengerType.equals(PremiumPackagePassengerEnum.ADULT)){
                    target.setIsAdult(Boolean.TRUE);
                }else {
                    target.setIsAdult(Boolean.FALSE);
                }
            }
        }
        if(Objects.nonNull(source.getExternalUser())){
            boolean isKiosk = DjpCoreConstants.ExternalCompany.BILESIK_ODEME.equals(source.getExternalUser().getCompany().getCompanyID());
            target.setIsKiosk(isKiosk);
            target.setIsOverSixtyFive(Boolean.TRUE.equals(source.getForOverSixtyFive()));
            target.setIsAdult(Boolean.FALSE.equals(source.getForChild()));
        }

    }


    @Override
    public DjpProductSaleWsData populate(DjpRemainingUsageModel source) throws ConversionException {
        DjpProductSaleWsData target = new DjpProductSaleWsData();
        this.populate(source, target);
        return target;
    }

    public void setDjpDailyKidsUserModelPopulator(DjpDailyKidsUserModelPopulator djpDailyKidsUserModelPopulator) {
        this.djpDailyKidsUserModelPopulator = djpDailyKidsUserModelPopulator;
    }
}
