package com.djp.populator.impl;

import com.djp.core.model.DjpCppTxnModel;
import com.djp.data.DjpCppTxnData;
import com.djp.exception.ConversionException;
import com.djp.populator.Populator;

import java.util.Objects;

public class DjpCppPopulator implements Populator<DjpCppTxnModel, DjpCppTxnData> {
    @Override
    public void populate(DjpCppTxnModel source, DjpCppTxnData target) throws ConversionException {
        if(Objects.nonNull(source.getCppToken())){
            target.setCppToken(source.getCppToken());
        }
        if(Objects.nonNull(source.getAmount())){
            target.setAmount(source.getAmount());
        }
        if(Objects.nonNull(source.getName())){
            target.setName(source.getName());
        }
        if(Objects.nonNull(source.getReservationNumber())){
            target.setReservationNumber(source.getReservationNumber());
        }
        if(Objects.nonNull(source.getSurName())){
            target.setSurName(source.getSurName());
        }
        if(Objects.nonNull(source.getLocation())){
            target.setLocation(source.getLocation());
        }
        if(Objects.nonNull(source.getLanguage())){
            target.setLanguage(source.getLanguage());
        }
        if(Objects.nonNull(source.getYouthLoungeCampaignCode())){
            target.setYouthLoungeCampaignCode(source.getYouthLoungeCampaignCode());
        }
        if(Objects.nonNull(source.getYouthLoungeEmail())){
            target.setYouthLoungeEmail(source.getYouthLoungeEmail());
        }
    }

    @Override
    public DjpCppTxnData populate(DjpCppTxnModel source) throws ConversionException {
        DjpCppTxnData djpCppTxnData = new DjpCppTxnData();
        if(Objects.nonNull(source.getCppToken())){
            djpCppTxnData.setCppToken(source.getCppToken());
        }
        if(Objects.nonNull(source.getAmount())){
            djpCppTxnData.setAmount(source.getAmount());
        }
        if(Objects.nonNull(source.getName())){
            djpCppTxnData.setName(source.getName());
        }
        if(Objects.nonNull(source.getReservationNumber())){
            djpCppTxnData.setReservationNumber(source.getReservationNumber());
        }
        if(Objects.nonNull(source.getSurName())){
            djpCppTxnData.setSurName(source.getSurName());
        }
        if(Objects.nonNull(source.getLocation())){
            djpCppTxnData.setLocation(source.getLocation());
        }
        if(Objects.nonNull(source.getLanguage())){
            djpCppTxnData.setLanguage(source.getLanguage());
        }
        if(Objects.nonNull(source.getYouthLoungeCampaignCode())){
            djpCppTxnData.setYouthLoungeCampaignCode(source.getYouthLoungeCampaignCode());
        }
        if(Objects.nonNull(source.getYouthLoungeEmail())){
            djpCppTxnData.setYouthLoungeEmail(source.getYouthLoungeEmail());
        }
        return djpCppTxnData;
    }
}
