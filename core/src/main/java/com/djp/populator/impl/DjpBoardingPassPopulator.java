package com.djp.populator.impl;

import com.djp.core.dao.DjpTokenDao;
import com.djp.core.enums.QrType;
import com.djp.core.model.BaseStoreModel;
import com.djp.core.model.ContractedCompaniesModel;
import com.djp.core.model.DjpBoardingPassModel;
import com.djp.core.model.QrForExperienceCenterModel;
import com.djp.data.DjpBoardingPassData;
import com.djp.data.DjpBoardingServiceInfoData;
import com.djp.data.ExperienceCenterGameSaleListItem;
import com.djp.exception.ConversionException;
import com.djp.modelservice.util.DateUtil;
import com.djp.populator.Populator;
import com.djp.response.QrCodeForExperienceCenterSaleResponse;
import com.djp.service.BaseStoreService;
import com.djp.service.ContractedCompanyService;
import org.apache.commons.lang.StringUtils;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DjpBoardingPassPopulator implements Populator<DjpBoardingPassModel, DjpBoardingPassData> {
    
    @Resource
    ContractedCompanyService djpContractedCompanyService;

    @Resource
    BaseStoreService baseStoreService;

    @Resource
    DjpTokenDao djpTokenDao;

    @Override
    public void populate(DjpBoardingPassModel source, DjpBoardingPassData target) throws ConversionException {
        target.setCreationTime(source.getCreationtime());
        target.setPnrCode(source.getPnrCode());
        target.setFormatCode(source.getFormatCode());
        target.setNumberOfLegsEncoded(source.getNumberOfLegsEncoded());
        target.setPassengerName(source.getPassengerName());
        target.setElectronicTicketIndicator(source.getElectronicTicketIndicator());
        target.setOperatingCarrierPnrCode(source.getOperatingCarrierPnrCode());
        target.setOperatingCarrierDesignator(source.getOperatingCarrierDesignator());
        target.setToCityAirportCode(source.getToCityAirportCode());
        target.setFromCityAirportCode(source.getFromCityAirportCode());
        target.setFlightNumber(source.getFlightNumber());
        target.setDateOfFlight(source.getDateOfFlight());
        target.setCompartmentCode(source.getCompartmentCode());
        target.setSeatNumber(source.getSeatNumber());
        target.setCheckInSequenceNumber(source.getCheckInSequenceNumber());
        target.setPassengerStatus(source.getPassengerStatus());
        target.setNewSale(source.getNewSale());
        target.setIsOwner(!source.getDjpBoardingPass().isEmpty());
        target.setBoardingTypeClassName("BP");
        if (Objects.nonNull(source.getPersonalID())){
            target.setPersonalID(source.getPersonalID());
        }
        if (Objects.nonNull(source.getPersonalName())){
            target.setPersonalName(source.getPersonalName());
        }
        if(Objects.nonNull(source.getProductCode())){
            target.setProductCode(source.getProductCode());
        }
        if(Objects.nonNull(source.getToken())) {
            target.setTokenKey(source.getToken().getCode());
        }
        target.setSapOrderID(source.getSapOrderID());
        if(Objects.nonNull(source.getBoardingUsage())) {
            List<String> boardingUsages = new ArrayList<>();
            boardingUsages.add(source.getBoardingUsage().getCode());
            target.setWhereUsed(boardingUsages);
        }
        target.setCouldNotReadBoardingPass(source.getCouldNotReadBoardingPass());
        if (!source.getDjpBoardingPass().isEmpty()) {
            List<DjpBoardingPassModel> guestList = source.getDjpBoardingPass().stream().filter(boardingPassModel -> boardingPassModel.getBoardingUsage().equals(source.getBoardingUsage()) && !boardingPassModel.getCancelled()).collect(Collectors.toList());
            Integer guestCount = guestList.size();
            target.setGuestBoardingCount(guestCount);
        }

        if (source.getBoardingUsage().equals(QrType.LOUNGE_DOMESTIC)){
            populateServiceUsingDetail(source,target);
        }
    }

    private void populateServiceUsingDetail(DjpBoardingPassModel source, DjpBoardingPassData target){
        DjpBoardingServiceInfoData boardingServiceInfo = new DjpBoardingServiceInfoData();
        if (Objects.nonNull(source.getBoardingPassUsingData())){
            if (StringUtils.isNotBlank(source.getContractID())) {
                ContractedCompaniesModel companiesModel = djpContractedCompanyService.getContractedCompany(source.getContractID());
                if (Objects.nonNull(companiesModel)) {
                    if(Objects.nonNull(companiesModel.getContractedType()) && Objects.nonNull(companiesModel.getLengthOfStayHour())){
                        boardingServiceInfo.setContractCompanyId(companiesModel.getCompanyId());
                        boardingServiceInfo.setContractCompanyName(companiesModel.getCompanyName());
                        boardingServiceInfo.setContractCompanyType(Objects.nonNull(companiesModel.getContractedType()) ? companiesModel.getContractedType().getCode() : "");
                        boardingServiceInfo.setContractCompanyLengthOfStayHour(String.valueOf(companiesModel.getLengthOfStayHour()));
                    }
                }
            } else {
                BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                Integer individualStayHour = 0;
                if (Objects.nonNull(baseStoreModel)){
                    individualStayHour = baseStoreModel.getIndividualLengthOfStayHour();
                } else {
                    individualStayHour = 6;
                }
                boardingServiceInfo.setContractCompanyId("");
                boardingServiceInfo.setContractCompanyName("");
                boardingServiceInfo.setContractCompanyType("Individual");
                boardingServiceInfo.setContractCompanyLengthOfStayHour(String.valueOf(individualStayHour));
            }
            boardingServiceInfo.setServiceStartDate(DateUtil.formatDateForBoardingList(source.getBoardingPassUsingData().getScheduledOutStartDate()));
            boardingServiceInfo.setServiceStartTime(DateUtil.formatTimeForBoardingList(source.getBoardingPassUsingData().getScheduledOutStartDate()));
            boardingServiceInfo.setServiceEndDate(DateUtil.formatDateForBoardingList(source.getBoardingPassUsingData().getScheduledOutEndDate()));
            boardingServiceInfo.setServiceEndTime(DateUtil.formatTimeForBoardingList(source.getBoardingPassUsingData().getScheduledOutEndDate()));
            boardingServiceInfo.setStatus(Objects.nonNull(source.getBoardingPassUsingData().getStatus()) ? source.getBoardingPassUsingData().getStatus().getCode() : "");
            Date bNewDate = new Date();
            boardingServiceInfo.setServiceTimeOut((bNewDate.after(source.getBoardingPassUsingData().getScheduledOutEndDate())));
            boardingServiceInfo.setServiceTimeOutValue(DateUtil.differentFindDateForHour(source.getBoardingPassUsingData().getScheduledOutStartDate(), bNewDate));
            boardingServiceInfo.setServiceOutDate((Objects.nonNull(source.getServiceOutDate()) ? DateUtil.formatDateForBoardingList(source.getServiceOutDate()) : ""));
            boardingServiceInfo.setServiceOutTime((Objects.nonNull(source.getServiceOutDate()) ? DateUtil.formatTimeForBoardingList(source.getServiceOutDate()) : ""));
        }

        target.setContractCompanyInfo(boardingServiceInfo);
        if (source.getBoardingUsage().equals(QrType.EXPERIENCE_CENTER_INTERNATIONAL) || source.getBoardingUsage().equals(QrType.EXPERIENCE_CENTER)){
            if (Objects.nonNull(source.getToken())){
                QrCodeForExperienceCenterSaleResponse response = new QrCodeForExperienceCenterSaleResponse();
                QrForExperienceCenterModel experienceCenterModel = djpTokenDao.getQRForExperienceCenterByTokenKey(source.getToken().getCode());
                if (Objects.nonNull(experienceCenterModel)){
                    if (Objects.nonNull(experienceCenterModel.getQr())){
                        response.setQrToken(source.getToken().getCode());
                        response.setQrImage(experienceCenterModel.getQr().getDownloadURL());
                    }
                    List<ExperienceCenterGameSaleListItem> itemList = new ArrayList<>();
                    experienceCenterModel.getRemainingUsageList().forEach(djpRemainingUsageModel -> {
                        ExperienceCenterGameSaleListItem item = new ExperienceCenterGameSaleListItem();
                        int claimed = djpRemainingUsageModel.getClaimed() - djpRemainingUsageModel.getUsed();
                        int used = djpRemainingUsageModel.getUsed();
                        item.setGameClaimed(claimed);
                        item.setGameUsing(used);
                        item.setGameCode(djpRemainingUsageModel.getProduct().getCode());
                        item.setGameName(djpRemainingUsageModel.getProduct().getName());
                        itemList.add(item);
                    });
                    response.setGameList(itemList);
                    response.setCreateDate(DateUtil.dateToString(experienceCenterModel.getCreationtime()));
                    response.setExpireDate(DateUtil.dateToString(source.getToken().getExpireDate()));
                    response.setIsActive(source.getToken().getExpireDate().after(new Date()) && experienceCenterModel.getIsPaymentSuccess());
                }
                target.setQrDataInfo(response);
            }

        }
    }

    @Override
    public DjpBoardingPassData populate(DjpBoardingPassModel source) throws ConversionException {
        DjpBoardingPassData target = new DjpBoardingPassData();
        this.populate(source, target);
        return target;
    }
}
