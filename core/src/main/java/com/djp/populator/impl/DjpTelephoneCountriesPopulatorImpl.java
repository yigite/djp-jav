package com.djp.populator.impl;

import com.djp.core.model.DjpTelCountriesModel;
import com.djp.dto.DjpTelephoneCountriesWsDto;
import com.djp.populator.DjpTelephoneCountriesPopulator;



public class DjpTelephoneCountriesPopulatorImpl implements DjpTelephoneCountriesPopulator {


    @Override
    public DjpTelephoneCountriesWsDto modelToDto(DjpTelephoneCountriesWsDto djpTelephoneCountriesWsDto, DjpTelCountriesModel djpTelCountriesModel) {
        djpTelephoneCountriesWsDto.setTelefto(djpTelCountriesModel.getTelefto());
        djpTelephoneCountriesWsDto.setLand1(djpTelCountriesModel.getLand1());
        djpTelephoneCountriesWsDto.setLandx50(djpTelCountriesModel.getLandx50());

        return djpTelephoneCountriesWsDto;
    }
}
