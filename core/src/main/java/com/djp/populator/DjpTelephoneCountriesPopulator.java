package com.djp.populator;

import com.djp.core.model.DjpTelCountriesModel;
import com.djp.dto.DjpTelephoneCountriesWsDto;



public interface DjpTelephoneCountriesPopulator {

    DjpTelephoneCountriesWsDto modelToDto(DjpTelephoneCountriesWsDto djpTelephoneCountriesWsDto, DjpTelCountriesModel djpTelCountriesModel);
}
