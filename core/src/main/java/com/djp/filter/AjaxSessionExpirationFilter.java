package com.djp.filter;


import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;

public class AjaxSessionExpirationFilter implements Filter {

    private int customSessionExpiredErrorCode = 901;

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //no need to do anything
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filerChain) throws IOException, ServletException {
        HttpSession currentSession = ((HttpServletRequest)request).getSession(false);
        if(currentSession == null) {
            String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
            if("XMLHttpRequest".equals(ajaxHeader)) {
                HttpServletResponse resp = (HttpServletResponse) response;
                resp.sendError(this.customSessionExpiredErrorCode);
            } else {
                filerChain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
            }
        } else {
            ((HttpServletRequest) request).getSession().setAttribute("sessionTime", (((RequestWrapper) request).getSession().getMaxInactiveInterval() * 1000) - (System.currentTimeMillis() - ((RequestWrapper) request).getSession().getCreationTime()));
            filerChain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
        }
    }

    @Override
    public void destroy() {
        //no need to do anything
    }
}