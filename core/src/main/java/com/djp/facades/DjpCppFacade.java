package com.djp.facades;

import com.djp.data.DjpCppTxnData;
import com.djp.request.DjpCppWsRequest;
import com.djp.response.DjpCppWsResponse;

public interface DjpCppFacade {

    DjpCppTxnData getDjpCppTxnDataByToken(final String djpCppToken);
    DjpCppWsResponse initPayment(DjpCppWsRequest igaCppWsRequest);
}
