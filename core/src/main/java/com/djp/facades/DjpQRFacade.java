package com.djp.facades;


import com.djp.core.model.DjpPassLocationsModel;
import com.djp.core.model.DjpSingleCampaignCodeInfoModel;
import com.djp.core.model.QrForVoucherModel;
import com.djp.response.CheckCustomerOwnedServiceResponse;
import com.djp.request.QrPassRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.response.QrCodeValidResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Date;

public interface DjpQRFacade {

    FioriStatusResponse qrPass(QrPassRequest qrPassRequest, final HttpServletRequest request) throws JsonProcessingException, ParseException;

    void saveCodeUsage(QrForVoucherModel voucher, QrPassRequest qrPassRequest, Date usageDate, DjpPassLocationsModel igaPassLocation, DjpSingleCampaignCodeInfoModel igaSingleCampaignCodeInfo, double priceConvertedToDouble);

    FioriStatusResponse fillDjpVoucherCodeUsagesAndStatus(QrForVoucherModel voucher, QrPassRequest qrPassRequest, Date usageDate, DjpPassLocationsModel djpPassLocation, DjpSingleCampaignCodeInfoModel djpSingleCampaignCodeInfo);

    void startDjpUsageSendToErpProcess(QrPassRequest qrPassRequest, QrForVoucherModel qrForVoucherModel, boolean isStockedServiceUsaged);

    FioriStatusResponse usePackageWithVoucher(QrForVoucherModel voucher,QrPassRequest passRequest,Date usageDate,DjpPassLocationsModel djpPassLocation) throws ParseException;

    boolean doesTheCompanyHaveUsageRight(QrForVoucherModel qrForVoucherModel);

    QrCodeValidResponse generateResponse(String tokenKey, String qrType, Date currentDate, Boolean newSale, DjpPassLocationsModel djpPassLocation);

    CheckCustomerOwnedServiceResponse checkCustomerOwnedService(String cardNo, String device);

}
