package com.djp.facades.impl;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.facade.DjpBoardingFacade;
import com.djp.core.model.DjpManuelRecordsModel;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.dto.UserWsDto;
import com.djp.facades.DjpManuelRecordFacade;
import com.djp.modelservice.util.DateUtil;
import com.djp.request.QrForFioriRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.service.DjpManuelRecordService;
import com.djp.service.DjpTokenService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class DjpManuelRecordFacadeImpl implements DjpManuelRecordFacade {
    private static final Logger LOG = LoggerFactory.getLogger(DjpManuelRecordFacadeImpl.class);


    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private DjpTokenService djpTokenService;

    @Value("${fiori.host.type}")
    private String fioriHostType;

    @Resource
    private DjpManuelRecordService djpManuelRecordService;

    @Resource
    private DjpBoardingFacade djpBoardingFacade;
    @Override
    public UserWsDto checkManuelBoarding(QrForFioriRequest qrForFioriRequest) {
        FioriStatusResponse response = new FioriStatusResponse();
        UserWsDto userWs = new UserWsDto();
        String pnrCode = null;
        String flightNumber = "";
        Date flightDate = null;

        DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(qrForFioriRequest.getLocationID());
        boolean couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
        try {

            if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode())) {
                pnrCode = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                pnrCode = pnrCode.replaceAll("&", "/");
                qrForFioriRequest.setPnrCode(pnrCode);

                if (StringUtils.isBlank(pnrCode)){
                    flightDate = DateUtil.convertStartDate(Objects.isNull(qrForFioriRequest)  ? "" :  qrForFioriRequest.getDate());
                    flightNumber = Objects.isNull(qrForFioriRequest) ?  ""  : qrForFioriRequest.getFlightNumber().toUpperCase().substring(2, qrForFioriRequest.getFlightNumber().length());
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;

                }else {

                    flightNumber = pnrCode.substring(39, 44);
                    flightNumber = flightNumber.contains(" ") ? flightNumber.replace(" ", "") : flightNumber;
                    flightDate = DateUtil.convertJulianDateToDate(pnrCode.substring(44, 47));
                }
                boolean checkIfStartWithBp = false;

                checkIfStartWithBp = !qrForFioriRequest.getPnrCode().startsWith("M1") && !qrForFioriRequest.getPnrCode().startsWith("M2") && !qrForFioriRequest.getPnrCode().startsWith("M3");
                if (checkIfStartWithBp) {
                    response.setMessage("Boarding Kart M1 / M2 / M3 Karakterleriyle Başlamalıdır!");
                    response.setType("E");
                    userWs.setResult(response);
                    LOG.error("responseForFiori:" + response.getType());
                    return userWs;
                }

                DjpManuelRecordsModel djpManuelRecordsModel = djpManuelRecordService.checkIfUsageByManuelBoarding(qrForFioriRequest,null,null);
                if (Objects.nonNull(qrForFioriRequest.getCanUseService())) {
                    if (!qrForFioriRequest.getCanUseService()) {
                        response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. " + QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())) + " girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile " + QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())) + " hakkından yararlanabilir.");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType());
                        return userWs;
                    }
                }
                if(!Objects.isNull(djpManuelRecordsModel))
                {
                    response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz.");
                    response.setType("E");
                    userWs.setResult(response);
                    LOG.error("responseForFiori:" + response.getType());
                    return userWs;
                }
                if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && qrForFioriRequest.getPnrCode().length() > 50) {

                    checkBoardingFromAndFlightDate(qrForFioriRequest, response, djpPassLocation,flightNumber,flightDate);
                } else {
                    response.setMessage("Hatalı pnr kodu");
                    response.setType("E");
                }
                if((!djpBoardingFacade.isChildBoardingPass(pnrCode,false) && PassengerType.BABY.getCode().equals(qrForFioriRequest.getPassengerType())) ||
                        (djpBoardingFacade.isChildBoardingPass(pnrCode,false) && (!"GENERAL".equals(qrForFioriRequest.getPassengerType()) && !PassengerType.BABY.getCode().equals(qrForFioriRequest.getPassengerType())))){
                    response.setMessage("Bebek boardingi girişmiştir, bebek boardingi sadece Bebek Misafir alanına girilebilir");
                    response.setType("E");
                }
                if ("E".equals(response.getType())) {
                    LOG.error("checkBoardingPassStatus:" + response.getType());
                }else {
                    if(StringUtils.isBlank(response.getType())){
                        response.setType("S");
                        response.setMessage("Boarding Pass Kullanılabilir");
                    }
                }
                userWs.setResult(response);
                return userWs;
            } else if(couldNotReadBoardingPass){
                LOG.info("checkBoardingPass:" + "flightNumber:"+qrForFioriRequest.getFlightNumber()+" classCode:"+qrForFioriRequest.getClassCode()+" for CouldNotReadBoardingPass:"+qrForFioriRequest.getCouldNotReadBoardingPass());
                DjpManuelRecordsModel djpManuelRecordsModel = djpManuelRecordService.checkIfUsageByManuelBoarding(qrForFioriRequest,null,null);
                if(Objects.nonNull(djpManuelRecordsModel)){
                    if(Objects.nonNull(qrForFioriRequest.getCanUseService())) {
                        if (!qrForFioriRequest.getCanUseService()) {
                            response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+ QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" hakkından yararlanabilir.");
                            response.setType("E");
                            userWs.setResult(response);
                            LOG.error("responseForFiori:" + response.getType());
                            return userWs;
                        }
                    }
                    if (StringUtils.isNotBlank(qrForFioriRequest.getFlightNumber())) {
                        if(qrForFioriRequest.getFromAirportIATA().length() != 3){
                            response.setMessage("FromAirport 3 Karakter Olmalı!");
                            response.setType("E");
                            userWs.setResult(response);
                            LOG.error("responseForFiori:" + response.getType());
                            return userWs;
                        }
                        if(qrForFioriRequest.getToAirportIATA().length() != 3){
                            response.setMessage("FromAirport 3 Karakter Olmalı!");
                            response.setType("E");
                            userWs.setResult(response);
                            LOG.error("responseForFiori:" + response.getType());
                            return userWs;
                        }
                        checkBoardingFromAndFlightDate(qrForFioriRequest, response, djpPassLocation,flightNumber,flightDate);
                    } else {
                        response.setMessage("Hatalı pnr kodu");
                        response.setType("E");
                    }

                    if("E".equals(response.getType())){
                        LOG.error("responseForFiori:" + response.getType());
                    }else {
                        if(StringUtils.isBlank(response.getType())){
                            response.setType("S");
                            response.setMessage("Boarding Pass Kullanılabilir");
                        }
                    }

                    userWs.setResult(response);
                    return userWs;
                }
            }
            else {
                response.setType("E");
                response.setMessage("Request içeresinde gerekli alanları doldurunuz!");
                userWs.setResult(response);
                LOG.error("responseForFiori:" + response.getType());
                return userWs;
            }

        }catch (Exception ex){
            LOG.error("DjpBoardingFacedeImpl::checkBoardingPass has an error:",ex);
            return null;
        }

        return userWs;
    }
    private void checkBoardingFromAndFlightDate(QrForFioriRequest qrForFioriRequest, FioriStatusResponse response, DjpPassLocationsModel djpPassLocationsModel, String flightNumber, Date flightDate) throws ParseException {
        int diffBetweenToDate = 0;
        String messageForCheckIfBoardingForLocationIATA = StringUtils.EMPTY;
        if ("PROD".equals(fioriHostType)) {
            diffBetweenToDate = djpManuelRecordService.getDiffBetweenToDate(qrForFioriRequest, null, null,flightNumber,flightDate);
            if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                response.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                response.setType("E");
            } else {
               /* messageForCheckIfBoardingForLocationIATA = igaManuelRecordService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,igaPassLocation);
                if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                    response.setMessage(messageForCheckIfBoardingForLocationIATA);
                    response.setType("E");
                } else if (igaManuelRecordService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,igaPassLocation)) {
                    response.setMessage("Boarding kartın kalkış istasyonu "+igaPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                    response.setType("E");
                }*/
            }
        } else {
            /*messageForCheckIfBoardingForLocationIATA = igaManuelRecordService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,igaPassLocation);
            if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                response.setMessage(messageForCheckIfBoardingForLocationIATA);
                response.setType("E");
            } else if (igaManuelRecordService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,igaPassLocation)) {
                response.setMessage("Boarding kartın kalkış noktası İstanbul değildir, yolcunun Hizmet Kullanım hakkı yoktur.");
                response.setType("E");
            }*/
        }
        /*if (IgaCoreConstants.IgaPassLocations.IGA.equals(igaPassLocation.getLocationID())) {
            try {
                String status = igaManuelRecordService.checkInternationalStatusForBp(qrForFioriRequest, null, null, null, QrTypeEnum.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())));
                if (StringUtils.isNotBlank(status)) {
                    if (IgaCoreConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(status)) {
                        response.setMessage("Yolcuya ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                        response.setType("E");
                    } else if (IgaCoreConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(status)) {
                        response.setMessage("Yolcuya ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                        response.setType("E");
                    }else if(IgaCoreConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_IGA.equals(status)){
                        response.setMessage("Uçuş iGA da planlanmamaktadır.");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                    }
                }
            } catch (Exception ex) {
                LOG.error("checkInternationalStatusForBp has an error:", ex);
                response.setMessage("Uçuş iGA da planlanmamaktadır.");
                response.setType("E");
                LOG.error("responseForFiori:" + response.getType());
            }
        }*/
    }
}
