package com.djp.facades.impl;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.facade.DjpBoardingFacade;
import com.djp.core.model.*;
import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.enums.TourniquetBoardingStatus;
import com.djp.core.enums.UsageMethod;
import com.djp.core.facade.DjpBoardingFacade;
import com.djp.core.model.*;
import com.djp.data.CustomerData;
import com.djp.data.DialingCodeData;
import com.djp.dto.DjpProductSaleWsData;
import com.djp.data.DjpGuestData;
import com.djp.dto.UserWsDto;
import com.djp.facades.*;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.facades.*;
import com.djp.modelservice.ModelService;
import com.djp.request.BoardingPassRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.response.BoardingPassResponse;
import com.djp.response.FioriStatusResponse;
import com.djp.response.QrCodeValidResponse;
import com.djp.service.*;
import com.djp.service.*;
import com.djp.strategy.DjpPremiumServiceUsageStrategy;
import com.djp.util.DjpUtil;
import com.djp.strategy.DjpQrTypeValidateStrategy;
import com.djp.util.DjpUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;
import java.text.ParseException;
import java.util.*;

public class DjpWsFacadeImpl implements DjpWsFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DjpWsFacadeImpl.class);
    private static final String EXCEPTION_OCCURED = "Sistemsel bir hata olustu.";
    private static final String EXCEPTION_TYPE = "E";
    private static final String SUCCESS_MESSAGE = "İşlem Başarılı.";
    private static final String SUCCESS_TYPE = "S";

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    DjpCustomerFacade djpCustomerFacade;

    @Resource
    DjpPremiumServiceUsageStrategy djpPremiumServiceUsageStrategy;

    @Value("${fiori.host.type}")
    private String HOST_TYPE;

    @Resource
    private DjpBoardingFacade djpBoardingFacade;

    @Resource
    private DjpBoardingService djpBoardingService;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private DjpCustomerService djpCustomerService;

    @Resource
    private ModelService modelService;

    @Resource
    private DjpRemainingUsageFacade djpRemainingUsageFacade;

    @Resource
    private UserService userService;

    @Resource
    private DjpQRService djpQRService;

    @Resource
    private DjpQRFacade djpQRFacade;

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private DjpCustomerFacade customerFacade;

    @Resource
    private DjpInternalizationFacade djpInternalizationFacade;

    @Resource
    private ContractedCompanyService contractedCompanyService;

    @Resource
    private MessageSource messageSource;

    @Override
    public UserWsDto getOrderList(QrForFioriRequest qrForFioriRequest) throws JsonProcessingException {
        LOG.info("getOrderList:" + generateLog(getJsonFromRequst(qrForFioriRequest)));
        CustomerData customerData = djpCustomerFacade.getUserForUID(qrForFioriRequest.getUserUID());
        UserWsDto response = new UserWsDto();
        FioriStatusResponse statusResponse = new FioriStatusResponse();
        DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(qrForFioriRequest.getLocationID());
        if (Objects.isNull(customerData)) {
            statusResponse.setType("E");
            statusResponse.setMessage("Müşteri DJP PASS Tarafında Bulunmamaktadır!");
            response.setResult(statusResponse);
            return response;
        } else {
            response = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForFiori(null, null, customerData, qrForFioriRequest, null,djpPassLocation);
            statusResponse = response.getResult();
            if (Objects.isNull(statusResponse.getType())) {
                if (response.getUserProducts().isEmpty()) {
                    statusResponse.setType("E");
                    statusResponse.setMessage("Müşterinin Ürünü Bulunmamaktadır!");
                    return response;
                } else {
                    statusResponse.setType("S");
                    response.setResult(statusResponse);
                    return response;
                }
            }
            return response;
        }
    }

    @Override
    public BoardingPassResponse boardingPassAction(BoardingPassRequest boardingPassRequest) throws JsonProcessingException, ParseException {
        if(Objects.isNull(boardingPassRequest.getFlightNumber()) || Objects.isNull(boardingPassRequest.getUserUID())  || Objects.isNull(boardingPassRequest.getQrCode())  || Objects.isNull(boardingPassRequest.getDate())  || Objects.isNull(boardingPassRequest.getProductCode())  || Objects.isNull(boardingPassRequest.getTokenKey())  || Objects.isNull(boardingPassRequest.getWifiKey()) || Objects.isNull(boardingPassRequest.getSeatCode()))
            LOG.info("boardingPassRequest: The JSON format log couldn't be generated because some fields were null!");
        else
            LOG.info("boardingPassRequest: " + generateLog(getJsonFromRequst(boardingPassRequest)));

        QrForFioriModel qrForFioriModel = null;
        BoardingPassResponse response = new BoardingPassResponse();
        FioriStatusResponse status = new FioriStatusResponse();
        int diffBetweenToDate;
        String messageForCheckIfBoardingForLocationIATA = StringUtils.EMPTY;
        DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(boardingPassRequest.getLocationID());
        boolean newSale = Objects.nonNull(boardingPassRequest.getNewSale()) && boardingPassRequest.getNewSale();
        if (Objects.nonNull(boardingPassRequest.getExtensionOfTime())){
            djpBoardingFacade.extensionOfTimeBoardingPass(boardingPassRequest.getExtensionOfTime(),response);
            return response;
        }
        if(StringUtils.isBlank(boardingPassRequest.getTokenKey()) && !newSale && (StringUtils.isBlank(boardingPassRequest.getUserUID()) && StringUtils.isBlank(boardingPassRequest.getProductCode()) && StringUtils.isBlank(boardingPassRequest.getOrderCode()))){
            status.setMessage("tokenKey is required.");
            status.setType("E");
            response.setServiceResult(status);
            LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
            return response;
        }
        if(StringUtils.isBlank(boardingPassRequest.getPnrCode()) && StringUtils.isBlank(boardingPassRequest.getFlightNumber())){
            status.setMessage("please fill pnrCode or flightNumber.");
            status.setType("E");
            response.setServiceResult(status);
            LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
            return response;
        }
        String pnr = null;
        boolean checkIfStartWithBp =false;
        boolean checkIfBpLength =false;
        if(!boardingPassRequest.getCouldNotReadBoardingPass() && StringUtils.isNotBlank(boardingPassRequest.getPnrCode())) {
            pnr = boardingPassRequest.getPnrCode().toUpperCase();
            checkIfStartWithBp = !pnr.startsWith("M1") && !pnr.startsWith("M2") && !pnr.startsWith("M3");
            if (checkIfStartWithBp) {
                status.setMessage("Boarding Kart M1 / M2 / M3 Karakterleriyle Başlamalıdır!");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
            checkIfBpLength = pnr.length() < 50;
            if(checkIfBpLength){
                status.setMessage("BoardingPass doğru okutulamadı. Lütfen tekrar okutunuz.");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
        }
        djpBoardingService.checkIfUsageByBoardingPass(null, null, boardingPassRequest, null);
        if (Objects.nonNull(boardingPassRequest.getCanUseService())) {
            if (!boardingPassRequest.getCanUseService()) {
                status.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+ QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" hakkından yararlanabilir.");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
        }
        if(!boardingPassRequest.getGuests().isEmpty()){
            for(DjpGuestData djpGuestData:boardingPassRequest.getGuests()){
                if(!djpGuestData.getCouldNotReadBoardingPass() && StringUtils.isNotBlank(djpGuestData.getPnrCode())){
                    pnr = djpGuestData.getPnrCode().toUpperCase();
                    checkIfStartWithBp = !pnr.startsWith("M1") && !pnr.startsWith("M2") && !pnr.startsWith("M3");
                    if (checkIfStartWithBp) {
                        break;
                    }
                    checkIfBpLength = pnr.length() < 50;
                    if(checkIfBpLength){
                        break;
                    }
                }
                djpBoardingService.checkIfUsageByBoardingPass(null, null, boardingPassRequest, djpGuestData);
                if (Objects.nonNull(boardingPassRequest.getCanUseService())) {
                    if (!boardingPassRequest.getCanUseService()) {
                        status.setMessage("Bu misafir yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" hakkından yararlanabilir.");
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    }
                }
            }
            if(checkIfStartWithBp) {
                status.setMessage("Misafir Boarding Kart M1 / M2 / M3 Karakterleriyle Başlamalıdır!");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
            if(checkIfBpLength) {
                status.setMessage("BoardingPass doğru okutulamadı. Lütfen tekrar okutunuz.");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }

        }
        if ("PROD".equals(HOST_TYPE)) {
            diffBetweenToDate = djpBoardingService.getDiffBetweenToDate(null, boardingPassRequest, null, null);
            if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                status.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            } else {
                messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(null, boardingPassRequest, null, null,djpPassLocation);
                if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                    status.setMessage(messageForCheckIfBoardingForLocationIATA);
                    status.setType("E");
                    response.setServiceResult(status);
                    LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                    return response;
                } else if (djpBoardingService.checkIfBoardingFromLocationIATA(null, boardingPassRequest, null, null,djpPassLocation)) {
                    status.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                    status.setType("E");
                    response.setServiceResult(status);
                    LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                    return response;
                }
            }
        } else {
            messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(null, boardingPassRequest, null, null,djpPassLocation);
            if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                status.setMessage(messageForCheckIfBoardingForLocationIATA);
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            } else if (djpBoardingService.checkIfBoardingFromLocationIATA(null, boardingPassRequest, null, null,djpPassLocation)) {
                status.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
        }

        if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
            try {
                String checkStatus = djpBoardingService.checkInternationalStatusForBp(null, boardingPassRequest, null, null, QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed())));
                if (StringUtils.isNotBlank(checkStatus)) {
                    if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(checkStatus)) {
                        status.setMessage("Yolcuya ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                        status.setType("E");
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        response.setServiceResult(status);
                        return response;
                    } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(checkStatus)) {
                        status.setMessage("Yolcuya ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                        status.setType("E");
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        response.setServiceResult(status);
                        return response;
                    }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(checkStatus)){
                        status.setMessage("Uçuş iGA da planlanmamaktadır.");
                        status.setType("E");
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        response.setServiceResult(status);
                        return response;
                    }
                }
            } catch (Exception ex) {
                LOG.error("checkInternationalStatusForBp has an error:", ex);
                status.setMessage("Uçuş iGA da planlanmamaktadır.");
                status.setType("E");
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                response.setServiceResult(status);
                return response;
            }
        }

        boolean couldNotReadBp = Objects.nonNull(boardingPassRequest.getCouldNotReadBoardingPass()) && boardingPassRequest.getCouldNotReadBoardingPass();
        if(couldNotReadBp){
            if(boardingPassRequest.getFromAirportIATA().length() != 3){
                status.setMessage("Owner FromAirportIATA 3 Karakter Olmalı!");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
            if(boardingPassRequest.getToAirportIATA().length() != 3){
                status.setMessage("Owner ToAirportIATA 3 Karakter Olmalı!");
                status.setType("E");
                response.setServiceResult(status);
                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                return response;
            }
        }

        if(!boardingPassRequest.getGuests().isEmpty()){
            for(DjpGuestData djpGuestData:boardingPassRequest.getGuests()){
                boolean couldNotReadBpGuest = Objects.nonNull(djpGuestData.getCouldNotReadBoardingPass()) && djpGuestData.getCouldNotReadBoardingPass();
                if(couldNotReadBpGuest){
                    if(djpGuestData.getFromAirportIATA().length() != 3){
                        status.setMessage("Misafirin FromAirportIATA 3 Karakter Olmalı!");
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    }
                    if(djpGuestData.getToAirportIATA().length() != 3){
                        status.setMessage("Misafirin ToAirportIATA 3 Karakter Olmalı!");
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    }
                }if ("PROD".equals(HOST_TYPE)) {
                    diffBetweenToDate = djpBoardingService.getDiffBetweenToDate(null, null, null, djpGuestData);
                    if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                        status.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                        LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    } else {
                        messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(null,boardingPassRequest,null,djpGuestData,djpPassLocation);
                        if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                            status.setMessage(messageForCheckIfBoardingForLocationIATA);
                            status.setType("E");
                            response.setServiceResult(status);
                            LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                            return response;
                        } else if (djpBoardingService.checkIfBoardingFromLocationIATA(null,boardingPassRequest,null,djpGuestData,djpPassLocation)) {
                            status.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Misafirin hizmet kullanım hakkı yoktur");
                            status.setType("E");
                            response.setServiceResult(status);
                            LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                            return response;
                        }
                    }
                }
                else {
                    messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(null,boardingPassRequest,null,djpGuestData,djpPassLocation);
                    if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                        status.setMessage(messageForCheckIfBoardingForLocationIATA);
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    } else if (djpBoardingService.checkIfBoardingFromLocationIATA(null,boardingPassRequest,null,djpGuestData,djpPassLocation)) {
                        status.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Misafirin hizmet kullanım hakkı yoktur");
                        status.setType("E");
                        response.setServiceResult(status);
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        return response;
                    }
                }
                if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
                    try {
                        String checkStatus = djpBoardingService.checkInternationalStatusForBp(null, null, null, djpGuestData, QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed())));
                        if (StringUtils.isNotBlank(checkStatus)) {
                            if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(checkStatus)) {
                                status.setMessage("Misafire ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                                status.setType("E");
                                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                                response.setServiceResult(status);
                                return response;
                            } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(checkStatus)) {
                                status.setMessage("Misafire ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                                status.setType("E");
                                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                                response.setServiceResult(status);
                                return response;
                            }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(checkStatus)){
                                status.setMessage("Uçuş iGA da planlanmamaktadır.");
                                status.setType("E");
                                LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                                response.setServiceResult(status);
                                return response;
                            }
                        }
                    } catch (Exception ex) {
                        LOG.error("checkInternationalStatusForBp has an error:", ex);
                        status.setMessage("Uçuş iGA da planlanmamaktadır.");
                        status.setType("E");
                        LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
                        response.setServiceResult(status);
                        return response;
                    }
                }
            }
        }
        if (StringUtils.isBlank(boardingPassRequest.getTokenKey()) && StringUtils.isNotBlank(boardingPassRequest.getUserUID()) && StringUtils.isNotBlank(boardingPassRequest.getOrderCode())) {
            CustomerModel customerModel = djpCustomerService.findCustomerByUid(boardingPassRequest.getUserUID());

            if (customerModel != null) {
                try {
                    qrForFioriModel = djpQRService.generateQrForFiori(boardingPassRequest.getUserUID(), customerModel, boardingPassRequest.getOrderCode());
                    if (StringUtils.isBlank(boardingPassRequest.getTokenKey())) {
                        boardingPassRequest.setTokenKey(qrForFioriModel.getTokenKey().getCode());
                        if(qrForFioriModel.getTokenKey() != null){
                            EmployeeModel employeeModel = null;
                            try {
                                employeeModel = (EmployeeModel) userService.getUserForUID(boardingPassRequest.getPersonalID());
                                qrForFioriModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(boardingPassRequest.getPersonalID()) ? boardingPassRequest.getPersonalID() : null);
                                qrForFioriModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                            } catch (Exception e){
                                LOG.error("Cannot find user with uid : " + boardingPassRequest.getPersonalID());
                            }
                            modelService.saveOrUpdate(qrForFioriModel.getTokenKey());
                        }
                        Set<QrType> qrTypeEnumSet = new HashSet<>();
                        qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed())));
                        qrForFioriModel.setWhereIsValidQr(qrTypeEnumSet);
                        modelService.saveOrUpdate(qrForFioriModel);
                    }
                } catch (Exception e) {
                    LOG.error("generateQrForFiori has error:", e);
                    return null;
                }
            } else {
                return null;
            }

        }
        String result = djpBoardingService.saveDjpBoardingPass(boardingPassRequest);
        if(result.equals("200")){
            if (StringUtils.isNotBlank(boardingPassRequest.getTokenKey()) && StringUtils.isNotBlank(boardingPassRequest.getUserUID()) && StringUtils.isNotBlank(boardingPassRequest.getOrderCode())) {
                if(Objects.nonNull(qrForFioriModel) && boardingPassRequest.getTokenKey().equals(qrForFioriModel.getTokenKey().getCode())) {
                    djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status, boardingPassRequest.getUserUID(), boardingPassRequest.getProductCategory(),boardingPassRequest, null, false,null, null);
                    PassengerType passengerTypeEnum = null;
                    Integer passengerCount = 0;
                    if(Objects.nonNull(boardingPassRequest.getAdultCount()) && boardingPassRequest.getAdultCount() > 0){
                        passengerTypeEnum = PassengerType.ADULT;
                        passengerCount = boardingPassRequest.getAdultCount();
                    }else{
                        passengerTypeEnum = PassengerType.CHILD;
                        passengerCount = boardingPassRequest.getChildCount();
                    }
                    qrForFioriModel.setUsageDate(new Date());
                    qrForFioriModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed())));
                    qrForFioriModel.setPassengerType(passengerTypeEnum);
                    qrForFioriModel.setPassengerCount(passengerCount);
                    qrForFioriModel.setWhichLocation(djpPassLocation);
                    DjpUniqueTokenModel tokenKey = qrForFioriModel.getTokenKey();
                    if(tokenKey != null){
                        EmployeeModel employeeModel = null;
                        try {
                            employeeModel = (EmployeeModel) userService.getUserForUID(boardingPassRequest.getPersonalID());
                            tokenKey.setPersonalID(StringUtils.isNotBlank(boardingPassRequest.getPersonalID()) ? boardingPassRequest.getPersonalID() : null);
                            tokenKey.setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                        } catch (Exception e){
                            LOG.error("Cannot find user with uid : " + boardingPassRequest.getPersonalID());
                        }
                    }
                    tokenKey.setOrderCode(boardingPassRequest.getOrderCode());
                    modelService.saveOrUpdate(tokenKey);
                    modelService.saveOrUpdate(qrForFioriModel);
                }
            }
            status.setMessage("BoardingPass Oluşturuldu.");
            status.setType("S");
            response.setServiceResult(status);
            response.setBoardingPassRequest(boardingPassRequest);
        } else if(result.equals("hasSameBp")){
            status.setMessage("Yolcu ile misafirinin boarding kartı aynıdır! Lütfen misafirin kendi boarding kartını sisteme okutun.!");
            status.setType("E");
            response.setServiceResult(status);
        }else if(result.equals("hasSameGuestBp")){
            status.setMessage("Misafirlerin boarding kartları aynıdır! Lütfen her bir misafirin kendi boarding kartını okutun!");
            status.setType("E");
            response.setServiceResult(status);
        }
        else if (result.equals("400")) {
            status.setMessage("ContractID Boş Olmamalı!");
            status.setType("E");
            response.setServiceResult(status);
        }else if(result.equals("hasBp")){
            status.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" hakkından yararlanabilir.");
            status.setType("E");
            response.setServiceResult(status);
        }else if(result.equals("hasGuestBp")){
            status.setMessage("Bu misafir yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(boardingPassRequest.getWhereUsed()))+" hakkından yararlanabilir.");
            status.setType("E");
            response.setServiceResult(status);
        }
        else {
            status.setMessage("BoardingPass Oluşturulurken Hata Oluştu.");
            status.setType("E");
            response.setServiceResult(status);
        }
        if("E".equals(status.getType())) {
            LOG.error("responseForFiori:" + status.getType() + " - " + status.getMessage());
        }
        return response;
    }


    public UserWsDto getUserByToken(
            @RequestParam final String fields,
            @RequestBody QrForFioriRequest qrForFioriRequest, final HttpServletRequest request) throws ParseException, JsonProcessingException {
        String clientId = request.getRemoteUser();
        LOG.info(clientId+"-qrForFioriRequest:"+generateLog(getJsonFromRequst(qrForFioriRequest)));
        Date currentDate = new Date();
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        String pnrCodeForTourniquet = null;
        int diffBetweenToDate = 0;
        DjpPassLocationsModel djpPassLocation = null;
        String messageForCheckIfBoardingForLocationIATA = StringUtils.EMPTY;
        boolean isTourniquet = StringUtils.isNotBlank(qrForFioriRequest.getDevice()) && qrForFioriRequest.getDevice().equals("T");
        if(Boolean.FALSE.equals(isTourniquet)) {
            boolean isCarParking = DjpCommerceservicesConstants.CARPARKING_CODE.equals(qrForFioriRequest.getQrType());
            if(Boolean.FALSE.equals(isCarParking)){
                djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(qrForFioriRequest.getLocationID());
            }else {
                djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(DjpCoreConstants.DjpPassLocations.DJP);
            }
        }else {
            djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(DjpCoreConstants.DjpPassLocations.DJP);
        }

        if (StringUtils.isNotBlank(qrForFioriRequest.getTokenKey())) {
            QrForVoucherModel qrForVoucherModel = djpQRService.validQrCodeForDjp(qrForFioriRequest.getTokenKey());
            if (Objects.nonNull(qrForVoucherModel) && !djpQRFacade.doesTheCompanyHaveUsageRight(qrForVoucherModel)) {
                result.setMessage(messageSource.getMessage("rest.usercontroller.getuserbytoken.usage.right.expired.for.company", new String[] {qrForVoucherModel.getContractId()}, LocaleContextHolder.getLocale()));
                result.setType("E");
                userWs.setResult(result);
                return userWs;
            }
        }

        if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && isTourniquet &&
                (((!qrForFioriRequest.getBoardingType().equals("1000") && !qrForFioriRequest.getBoardingType().equals("1001")) && StringUtils.isBlank(qrForFioriRequest.getProcessType()))
                        || (qrForFioriRequest.getBoardingType().equals("1001") && StringUtils.isBlank(qrForFioriRequest.getProcessType())))) {
            pnrCodeForTourniquet = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
            pnrCodeForTourniquet = pnrCodeForTourniquet.replaceAll("&", "/");
            qrForFioriRequest.setPnrCode(pnrCodeForTourniquet);
            LOG.info("convertRequestForTourniquet:" + generateLog(getJsonFromRequst(qrForFioriRequest)));
            String boardingType = qrForFioriRequest.getBoardingType();
            if (StringUtils.isBlank(boardingType)) {
                result.setMessage("boardingType is required.");
                result.setType("E");
                userWs.setResult(result);
                LOG.error("responseForTourniquet:" + result.getType() + "Message:boardingType is required.");
                return userWs;
            }

            DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), boardingType);
            if (Objects.isNull(djpBoardingPassModel) && (boardingType.equals("1001") || boardingType.equals("1000"))){
                //Check if control for LOUNGE PLUS DOMESTIC
                djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), "1004");
                if (Objects.isNull(djpBoardingPassModel)){
                    //Check if control for LOUNGE PLUS INTERNATIONAL
                    djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), "1005");
                }
            }
            DjpAccessBoardingModel djpAccessBoardingModel = null;
            if (Objects.isNull(djpBoardingPassModel)) {
                DjpMobilePassModel djpMobilePassModel = djpBoardingService.getCouldReadDjpMobilePass(qrForFioriRequest.getPnrCode(), boardingType, qrForFioriRequest.getDevice());
                if (Objects.isNull(djpMobilePassModel)){
                    djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),boardingType);
                    if (Objects.isNull(djpAccessBoardingModel) && (boardingType.equals("1001") || boardingType.equals("1000"))){
                        //Check if control for LOUNGE PLUS DOMESTIC
                        djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),"1004");
                        if (Objects.isNull(djpAccessBoardingModel)){
                            //Check if control for LOUNGE PLUS INTERNATIONAL
                            djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),"1005");
                        }
                    }
                    if (Objects.isNull(djpAccessBoardingModel)) {
                        djpBoardingService.checkIfHaveDjpPassAndGenerateBoardingPass(qrForFioriRequest, userWs,djpPassLocation, UsageMethod.TOURNIQUET);
                        boolean isHaveDjpPass = Objects.nonNull(qrForFioriRequest.getCanUseService()) && qrForFioriRequest.getCanUseService() && Objects.nonNull(qrForFioriRequest.getHaveDjpPass()) && qrForFioriRequest.getHaveDjpPass();
                        if (isHaveDjpPass) {
                            LOG.info("responseForTourniquet:" + userWs.getResult().getType());
                            return userWs;
                        } else {
                            djpPremiumServiceUsageStrategy.beforeSaveQuickBoardingCheckFlightAndAccess(qrForFioriRequest,result,djpPassLocation,Boolean.FALSE);
                            boolean isQuickServiceUsage = "S".equals(result.getType()); //Boolean.TRUE.equals(result.getQuickServiceUsage()) &&
                            if (Boolean.TRUE.equals(isQuickServiceUsage)) {
                                LOG.info("responseForTourniquet:" + result.getType() + " isQuickServiceUsage:"+isQuickServiceUsage);
                                userWs.setResult(result);
                                return userWs;
                            } else if(result.getType().equals("E") && !qrForFioriRequest.getDevice().equals("F")){
                                result.setType("E");
                                result.setMessage("Lütfen desk personeline başvunuruz.");
                                result.setPackages(Collections.emptyList());
                                userWs.setResult(result);
                                return userWs;
                            } else {
                                result.setType("E");
                                result.setMessage("Boarding Pass DJP PASS de Bulunamadı.");
                                userWs.setResult(result);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        }
                    } else {
                        result.setType("S");
                        result.setMessage("DJP Boarding Pass Kullanılabilir.");
                        userWs.setResult(result);
                        LOG.info("responseForTourniquet:" + result.getType());
                        return userWs;
                    }
                } else {
                    result.setType("S");
                    result.setMessage("DJP Boarding Pass Kullanılabilir.");
                    userWs.setResult(result);
                    LOG.info("responseForTourniquet:" + result.getType());
                    return userWs;
                }
            } else {
                result.setType("S");
                result.setMessage("DJP Boarding Pass Kullanılabilir.");
                userWs.setResult(result);
                LOG.info("responseForTourniquet:" + result.getType());
                return userWs;
            }
        } else if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && isTourniquet && qrForFioriRequest.getProcessType().equals("IN") && (qrForFioriRequest.getBoardingType().equals("1000") || qrForFioriRequest.getBoardingType().equals("1001"))) {
            pnrCodeForTourniquet = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
            pnrCodeForTourniquet = pnrCodeForTourniquet.replaceAll("&", "/");
            qrForFioriRequest.setPnrCode(pnrCodeForTourniquet);
            LOG.info("convertRequestForTourniquet:" + generateLog(getJsonFromRequst(qrForFioriRequest)));
            String boardingType = qrForFioriRequest.getBoardingType();
            if (StringUtils.isBlank(boardingType)) {
                result.setMessage("boardingType is required.");
                result.setType("E");
                userWs.setResult(result);
                LOG.error("responseForTourniquet:" + result.getType() + "Message:boardingType is required.");
                return userWs;
            }

            DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), boardingType);
            DjpAccessBoardingModel djpAccessBoardingModel = null;
            if (Objects.isNull(djpBoardingPassModel)) {
                djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),boardingType);
                if (Objects.isNull(djpAccessBoardingModel)) {
                    djpBoardingService.checkIfHaveDjpPassAndGenerateBoardingPass(qrForFioriRequest, userWs,djpPassLocation,UsageMethod.TOURNIQUET);
                    boolean isHaveDjpPass = Objects.nonNull(qrForFioriRequest.getCanUseService()) && qrForFioriRequest.getCanUseService() && Objects.nonNull(qrForFioriRequest.getHaveDjpPass()) && qrForFioriRequest.getHaveDjpPass();
                    if (isHaveDjpPass) {
                        LOG.info("responseForTourniquet:" + userWs.getResult().getType());
                        return userWs;
                    } else {
                        djpPremiumServiceUsageStrategy.beforeSaveQuickBoardingCheckFlightAndAccess(qrForFioriRequest,result,djpPassLocation,Boolean.FALSE);
                        boolean isQuickServiceUsage = "S".equals(result.getType()); //Boolean.TRUE.equals(result.getQuickServiceUsage()) &&
                        if (Boolean.TRUE.equals(isQuickServiceUsage)) {
                            LOG.info("responseForTourniquet:" + result.getType() + " isQuickServiceUsage:"+isQuickServiceUsage);
                            userWs.setResult(result);
                            return userWs;
                        } else if(result.getType().equals("E") && !qrForFioriRequest.getDevice().equals("F")){
                            result.setType("E");
                            result.setMessage("Lütfen desk personeline başvunuruz.");
                            result.setPackages(Collections.emptyList());
                            userWs.setResult(result);
                            return userWs;
                        } else {
                            result.setType("E");
                            result.setMessage("Boarding Pass DJP PASS de Bulunamadı.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    }
                } else {
                    ContractedCompaniesModel contractedCompaniesModel = contractedCompanyService.getContractedCompany(djpAccessBoardingModel.getContractID());
                    if (Objects.nonNull(contractedCompaniesModel)){
                        if (contractedCompaniesModel.getLengthOfStayHour() > 0){
                            Date bNewDate = new Date();
                            if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpAccessBoardingModel.getCreationtime(), contractedCompaniesModel.getLengthOfStayHour())))){
                                if (Objects.isNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                    result.setType("S");
                                    result.setMessage("Giriş yapabilirsiniz.");
                                    userWs.setResult(result);
                                    if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                        djpAccessBoardingModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.IN);
                                    }
                                    djpAccessBoardingModel.setServiceOutDate(null);
                                    modelService.saveOrUpdate(djpAccessBoardingModel);
                                    LOG.info("responseForTourniquet:" + result.getType());
                                    return userWs;
                                } else {
                                    result.setType("RLA");
                                    result.setMessage("Lütfen Desk Personeline Başvurunuz.");
                                    userWs.setResult(result);
                                    return userWs;
                                }
                            } else {
                                result.setType("E");
                                result.setMessage(contractedCompaniesModel.getCompanyName() + " firmasına tanımlanan " + contractedCompaniesModel.getLengthOfStayHour() + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                    djpAccessBoardingModel.getBoardingPassUsingData().setIsTimeOk(true);
                                }
                                userWs.setResult(result);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Anlaşmalı firma ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    } else {
                        BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                        if(Objects.nonNull(baseStoreModel)){
                            Integer lengthOfStay = baseStoreModel.getIndividualLengthOfStayHour();
                            if (lengthOfStay > 0){
                                Date bNewDate = new Date();
                                if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpAccessBoardingModel.getCreationtime(), lengthOfStay)))){
                                    if (Objects.isNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                        result.setType("S");
                                        result.setMessage("Giriş yapabilirsiniz.");
                                        userWs.setResult(result);
                                        if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                            djpAccessBoardingModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.IN);
                                        }
                                        djpAccessBoardingModel.setServiceOutDate(null);
                                        modelService.saveOrUpdate(djpAccessBoardingModel);
                                        LOG.info("responseForTourniquet:" + result.getType());
                                        return userWs;
                                    } else {
                                        result.setType("RLA");
                                        result.setMessage("Lütfen Desk Personeline Başvurunuz.");
                                        userWs.setResult(result);
                                        return userWs;
                                    }
                                } else {
                                    result.setType("E");
                                    result.setMessage(" Münferit kişiye tanımlanan " + lengthOfStay + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                    if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                        djpAccessBoardingModel.getBoardingPassUsingData().setIsTimeOk(true);
                                    }
                                    userWs.setResult(result);
                                    modelService.saveOrUpdate(djpAccessBoardingModel);
                                    LOG.info("responseForTourniquet:" + result.getType());
                                    return userWs;
                                }
                            } else {
                                result.setType("E");
                                result.setMessage("Münferit kayıt ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                                userWs.setResult(result);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Bir Hata Oluştu.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    }
                }
            } else {
                ContractedCompaniesModel contractedCompaniesModel = contractedCompanyService.getContractedCompany(djpBoardingPassModel.getContractID());
                if (Objects.nonNull(contractedCompaniesModel)){
                    if (contractedCompaniesModel.getLengthOfStayHour() > 0){
                        Date bNewDate = new Date();
                        if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpBoardingPassModel.getCreationtime(), contractedCompaniesModel.getLengthOfStayHour())))){
                            if (Objects.isNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                result.setType("S");
                                result.setMessage("Giriş yapabilirsiniz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                    djpBoardingPassModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.IN);
                                }
                                djpBoardingPassModel.setServiceOutDate(null);
                                modelService.saveOrUpdate(djpBoardingPassModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            } else {
                                result.setType("RLA");
                                result.setMessage("Lütfen Desk Personeline Başvurunuz.");
                                userWs.setResult(result);
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage(contractedCompaniesModel.getCompanyName() + " firmasına tanımlanan " + contractedCompaniesModel.getLengthOfStayHour() + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                            userWs.setResult(result);
                            if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                djpBoardingPassModel.getBoardingPassUsingData().setIsTimeOk(true);
                            }
                            modelService.saveOrUpdate(djpBoardingPassModel);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    } else {
                        result.setType("E");
                        result.setMessage("Anlaşmalı firma ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                        userWs.setResult(result);
                        LOG.info("responseForTourniquet:" + result.getType());
                        return userWs;
                    }
                } else {
                    BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                    if(Objects.nonNull(baseStoreModel)){
                        Integer lengthOfStay = baseStoreModel.getIndividualLengthOfStayHour();
                        if (lengthOfStay > 0){
                            Date bNewDate = new Date();
                            if (Objects.isNull(djpBoardingPassModel.getBoardingPassUsingData()) && bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpBoardingPassModel.getCreationtime(), lengthOfStay)))){
                                if (Objects.isNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                    result.setType("S");
                                    result.setMessage("Giriş yapabilirsiniz.");
                                    userWs.setResult(result);
                                    if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                        djpBoardingPassModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.IN);
                                    }
                                    djpBoardingPassModel.setServiceOutDate(null);
                                    modelService.saveOrUpdate(djpBoardingPassModel);
                                    LOG.info("responseForTourniquet:" + result.getType());
                                    return userWs;
                                } else {
                                    result.setType("RLA");
                                    result.setMessage("Lütfen Desk Personeline Başvurunuz.");
                                    userWs.setResult(result);
                                    return userWs;
                                }
                            } else {
                                result.setType("E");
                                result.setMessage(" Münferit kişiye tanımlanan " + lengthOfStay + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                    djpBoardingPassModel.getBoardingPassUsingData().setIsTimeOk(true);
                                }
                                modelService.saveOrUpdate(djpBoardingPassModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Münferit kayıt ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    } else {
                        result.setType("E");
                        result.setMessage("Bir Hata Oluştu.");
                        userWs.setResult(result);
                        LOG.info("responseForTourniquet:" + result.getType());
                        return userWs;
                    }
                }
            }
        }
        else if(StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && isTourniquet && qrForFioriRequest.getProcessType().equals("UNDO")) { //ROLLING DATA
            pnrCodeForTourniquet = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
            pnrCodeForTourniquet = pnrCodeForTourniquet.replaceAll("&", "/");
            qrForFioriRequest.setPnrCode(pnrCodeForTourniquet);
            LOG.info("convertRequestForTourniquet:" + generateLog(getJsonFromRequst(qrForFioriRequest)));
            String boardingType = qrForFioriRequest.getBoardingType();
            if (StringUtils.isBlank(boardingType)) {
                result.setMessage("boardingType is required.");
                result.setType("E");
                userWs.setResult(result);
                LOG.error("responseForTourniquet:" + result.getType() + "Message:boardingType is required.");
                return userWs;
            }

            DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), boardingType);
            DjpAccessBoardingModel djpAccessBoardingModel = null;
            if (Objects.isNull(djpBoardingPassModel)) {
                djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),boardingType);
                if (Objects.isNull(djpAccessBoardingModel)) {
                    result.setType("RE");
                    result.setMessage("Rolling Error");
                    userWs.setResult(result);
                    LOG.info("responseForTourniquet:" + result.getType());
                    return userWs;
                } else {
                    modelService.delete(djpAccessBoardingModel);
                    result.setType("RS");
                    result.setMessage("Rolling Success");
                    userWs.setResult(result);
                    LOG.info("responseForTourniquet:" + result.getType());
                    return userWs;
                }
            } else {
                djpBoardingPassModel.setBoardingPassUsingData(null);
                modelService.saveOrUpdate(djpBoardingPassModel);
                result.setType("RS");
                result.setMessage("Rolling Success");
                userWs.setResult(result);
                LOG.info("responseForTourniquet:" + result.getType());
                return userWs;
            }
        }
        else if(StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && isTourniquet && qrForFioriRequest.getProcessType().equals("OUT")) { //ROLLING DATA
            pnrCodeForTourniquet = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
            pnrCodeForTourniquet = pnrCodeForTourniquet.replaceAll("&", "/");
            qrForFioriRequest.setPnrCode(pnrCodeForTourniquet);
            LOG.info("convertRequestForTourniquet:" + generateLog(getJsonFromRequst(qrForFioriRequest)));
            String boardingType = qrForFioriRequest.getBoardingType();
            if (StringUtils.isBlank(boardingType)) {
                result.setMessage("boardingType is required.");
                result.setType("E");
                userWs.setResult(result);
                LOG.error("responseForTourniquet:" + result.getType() + "Message:boardingType is required.");
                return userWs;
            }

            DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPass(qrForFioriRequest.getPnrCode(), boardingType);
            DjpAccessBoardingModel djpAccessBoardingModel = null;
            if (Objects.isNull(djpBoardingPassModel)) {
                djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoarding(qrForFioriRequest.getPnrCode(),boardingType);
                if (Objects.isNull(djpAccessBoardingModel)) {
                    result.setType("E");
                    result.setMessage("Kayıt bulunamadı.");
                    userWs.setResult(result);
                    LOG.info("responseForTourniquet:" + result.getType());
                    return userWs;
                } else {
                    ContractedCompaniesModel contractedCompaniesModel = contractedCompanyService.getContractedCompany(djpAccessBoardingModel.getContractID());
                    if (Objects.nonNull(contractedCompaniesModel)){
                        if (contractedCompaniesModel.getLengthOfStayHour() > 0){
                            Date bNewDate = new Date();
                            if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpAccessBoardingModel.getBoardingPassUsingData().getScheduledOutStartDate(), contractedCompaniesModel.getLengthOfStayHour())))){
                                result.setType("S");
                                result.setMessage("Çıkış yapabilirsiniz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                    djpAccessBoardingModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.OUT);
                                }
                                djpAccessBoardingModel.setServiceOutDate(new Date());
                                modelService.saveOrUpdate(djpAccessBoardingModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            } else {
                                result.setType("TE");
                                result.setMessage(contractedCompaniesModel.getCompanyName() + " firmasına tanımlanan " + contractedCompaniesModel.getLengthOfStayHour() + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                    djpAccessBoardingModel.getBoardingPassUsingData().setIsTimeOk(true);
                                }
                                djpAccessBoardingModel.setServiceOutDate(new Date());
                                modelService.saveOrUpdate(djpAccessBoardingModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Anlaşmalı firma ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    }else {
                        BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                        if(Objects.nonNull(baseStoreModel)){
                            Integer lengthOfStay = baseStoreModel.getIndividualLengthOfStayHour();
                            if (lengthOfStay > 0){
                                Date bNewDate = new Date();
                                if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpAccessBoardingModel.getBoardingPassUsingData().getScheduledOutStartDate(), lengthOfStay)))){
                                    result.setType("S");
                                    result.setMessage("Çıkış yapabilirsiniz.");
                                    userWs.setResult(result);
                                    if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                        djpAccessBoardingModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.OUT);
                                    }
                                    djpAccessBoardingModel.setServiceOutDate(new Date());
                                    modelService.saveOrUpdate(djpAccessBoardingModel);
                                    LOG.info("responseForTourniquet:" + result.getType());
                                    return userWs;
                                } else {
                                    result.setType("TE");
                                    result.setMessage(" Münferit kişiye tanımlanan " + lengthOfStay + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                    userWs.setResult(result);
                                    if (Objects.nonNull(djpAccessBoardingModel.getBoardingPassUsingData())){
                                        djpAccessBoardingModel.getBoardingPassUsingData().setIsTimeOk(true);
                                    }
                                    djpAccessBoardingModel.setServiceOutDate(new Date());
                                    modelService.saveOrUpdate(djpAccessBoardingModel);
                                    LOG.info("responseForTourniquet:" + result.getType());
                                    return userWs;
                                }
                            } else {
                                result.setType("E");
                                result.setMessage("Münferit kayıt ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                                userWs.setResult(result);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Bir Hata Oluştu.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    }
                }
            } else {
                ContractedCompaniesModel contractedCompaniesModel = contractedCompanyService.getContractedCompany(djpBoardingPassModel.getContractID());
                if (Objects.nonNull(contractedCompaniesModel)){
                    if (contractedCompaniesModel.getLengthOfStayHour() > 0){
                        Date bNewDate = new Date();
                        if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpBoardingPassModel.getBoardingPassUsingData().getScheduledOutStartDate(), contractedCompaniesModel.getLengthOfStayHour())))){
                            result.setType("S");
                            result.setMessage("Çıkış yapabilirsiniz.");
                            userWs.setResult(result);
                            djpBoardingPassModel.setServiceOutDate(new Date());
                            if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                djpBoardingPassModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.OUT);
                            }
                            modelService.saveOrUpdate(djpBoardingPassModel);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        } else {
                            result.setType("E");
                            result.setMessage(contractedCompaniesModel.getCompanyName() + " firmasına tanımlanan " + contractedCompaniesModel.getLengthOfStayHour() + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                            userWs.setResult(result);
                            if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                djpBoardingPassModel.getBoardingPassUsingData().setIsTimeOk(true);
                            }
                            djpBoardingPassModel.setServiceOutDate(new Date());
                            modelService.saveOrUpdate(djpBoardingPassModel);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    } else {
                        result.setType("E");
                        result.setMessage("Anlaşmalı firma ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                        userWs.setResult(result);
                        LOG.info("responseForTourniquet:" + result.getType());
                        return userWs;
                    }
                } else {
                    BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
                    if(Objects.nonNull(baseStoreModel)){
                        Integer lengthOfStay = baseStoreModel.getIndividualLengthOfStayHour();
                        if (lengthOfStay > 0){
                            Date bNewDate = new Date();
                            if (bNewDate.before(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(djpBoardingPassModel.getBoardingPassUsingData().getScheduledOutStartDate(), lengthOfStay)))){
                                result.setType("S");
                                result.setMessage("Çıkış yapabilirsiniz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                    djpBoardingPassModel.getBoardingPassUsingData().setStatus(TourniquetBoardingStatus.OUT);
                                }
                                djpBoardingPassModel.setServiceOutDate(new Date());
                                modelService.saveOrUpdate(djpBoardingPassModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            } else {
                                result.setType("E");
                                result.setMessage(" Münferit kişiye tanımlanan " + lengthOfStay + " saatlik süreyi aştığınız için desk personeline başvurunuz.");
                                userWs.setResult(result);
                                if (Objects.nonNull(djpBoardingPassModel.getBoardingPassUsingData())){
                                    djpBoardingPassModel.getBoardingPassUsingData().setIsTimeOk(true);
                                }
                                djpBoardingPassModel.setServiceOutDate(new Date());
                                modelService.saveOrUpdate(djpBoardingPassModel);
                                LOG.info("responseForTourniquet:" + result.getType());
                                return userWs;
                            }
                        } else {
                            result.setType("E");
                            result.setMessage("Münferit kayıt ile alakalı bir sorun oluştu lütfen desk personeli ile iletişime geçiniz.");
                            userWs.setResult(result);
                            LOG.info("responseForTourniquet:" + result.getType());
                            return userWs;
                        }
                    } else {
                        result.setType("E");
                        result.setMessage("Bir Hata Oluştu.");
                        userWs.setResult(result);
                        LOG.info("responseForTourniquet:" + result.getType());
                        return userWs;
                    }
                }
            }
        } else {
            boolean couldNotReadBoardingPass = Objects.nonNull(qrForFioriRequest.getCouldNotReadBoardingPass()) && qrForFioriRequest.getCouldNotReadBoardingPass();
            if (StringUtils.isNotBlank(qrForFioriRequest.getTokenKey())) {
                QrForCardModel qrForCardModel = djpQRService.getQRForCardByTokenKey(qrForFioriRequest.getTokenKey());
                if (Objects.nonNull(qrForCardModel)) {
                    boolean checkIfValidLocation = qrForCardModel.getValidAtWhichLocation().contains(djpPassLocation);
                    boolean checkIfQrForCardIsCancelled = BooleanUtils.isTrue(qrForCardModel.getCancelled());
                    if(checkIfQrForCardIsCancelled){
                        result.setType("E");
                        result.setMessage("Kartınız Aktif Değil!");
                        userWs.setResult(result);
                        djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForCard");
                        LOG.error("responseForFiori:" + result.getType());
                        return userWs;
                    }
                    if(!checkIfValidLocation){
                        result.setType("E");
                        result.setMessage("Kartınızın Bu Havalimanı İçin Kullanımı Mevcut Değil!");
                        userWs.setResult(result);
                        djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForCard");
                        LOG.error("responseForFiori:" + result.getType());
                        return userWs;
                    }
                    if (Objects.isNull(qrForCardModel.getStartDate()) && Objects.isNull(qrForCardModel.getExpireDate())) {
                        boolean validThisServiceFloor = false;
                        String qrType = qrForFioriRequest.getQrType();
                        if(qrForCardModel.getWhereIsValidQr().contains(QrType.valueOf(DjpCommerceservicesConstants.ALL))){
                            validThisServiceFloor = true;
                        }else {
                            if (qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_TRANSFER_STARTWITH) && !qrForCardModel.getWhereIsValidQr().contains(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE))) {
                                validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType).equals(qrTypeEnum.getCode()));
                            } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_INTERNATIONAL_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_DOMESTIC_STARTWITH)) && !qrForCardModel.getWhereIsValidQr().contains(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.BUGGY_CODE))) {
                                validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                            } else {
                                validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 2))));
                            }
                        }
                        if(!validThisServiceFloor){
                            result.setType("E");
                            result.setMessage("Kartınızın Bu Hizmet İçin Kullanımı Mevcut Değil!");
                            userWs.setResult(result);
                            djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForCard");
                            LOG.error("responseForFiori:" + result.getType());
                            return userWs;
                        }else{
                            if(StringUtils.isNotBlank(qrForCardModel.getPhoneNumber())){
                                CustomerData customerData = customerFacade.getUserForUID(qrForCardModel.getPhoneNumber());
                                String fieldsDto = fields;
                                userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForMobile(fieldsDto, null, customerData, qrForFioriRequest,null,qrForCardModel,null,djpPassLocation);
                                return userWs;
                            }else{
                                List<DjpProductSaleWsData> djpProductSaleWsDataList = new ArrayList<>();
                                DjpProductSaleWsData djpProductSaleWsData = new DjpProductSaleWsData();
                                djpProductSaleWsData.setGuestCount(qrForCardModel.getGuestQuantity());
                                djpProductSaleWsData.setPackageName(qrTypeEnumHashMap.get(qrType));
                                djpProductSaleWsDataList.add(djpProductSaleWsData);
                                userWs.setUserProducts(djpProductSaleWsDataList);
                                result.setType("S");
                                result.setMessage("Kart Kullanılabilir.");
                                userWs.setResult(result);
                                return userWs;

                            }
                        }
                    } else {
                        if (DjpUtil.dateCompare(qrForCardModel.getExpireDate(), currentDate) < 0) {
                            result.setType("E");
                            result.setMessage("DJP PASS Kartınızı Geçerlilik Süresi Dolmuştur");
                            userWs.setResult(result);
                            djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForCard");
                            LOG.error("responseForFiori:" + result.getType());
                            return userWs;
                        } else {
                            boolean validThisServiceFloor = false;
                            String qrType = qrForFioriRequest.getQrType();
                            if(qrForCardModel.getWhereIsValidQr().contains(QrType.valueOf(DjpCommerceservicesConstants.ALL))){
                                validThisServiceFloor = true;
                            }else {
                                if (qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_TRANSFER_STARTWITH) && !qrForCardModel.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                                    validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType).equals(qrTypeEnum.getCode()));
                                } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_INTERNATIONAL_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_DOMESTIC_STARTWITH)) && !qrForCardModel.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.BUGGY_CODE)))) {
                                    validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                                } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_DEPARTURE_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_ARRIVAL_STARTWITH)) && !qrForCardModel.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                                    validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                                } else {
                                    validThisServiceFloor = qrForCardModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 2))));
                                }
                            }
                            if(!validThisServiceFloor){
                                result.setType("E");
                                result.setMessage("Kartınızın Bu Hizmet İçin Kullanımı Mevcut Değil!");
                                userWs.setResult(result);
                                djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForCard");
                                LOG.error("responseForFiori:" + result.getType());
                                return userWs;
                            }else{
                                if(StringUtils.isNotBlank(qrForCardModel.getPhoneNumber())){
                                    CustomerData customerData = customerFacade.getUserForUID(qrForCardModel.getPhoneNumber());
                                    String fieldsDto = fields;
                                    userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForMobile(fieldsDto, null, customerData, qrForFioriRequest,null,qrForCardModel,null,djpPassLocation);
                                    return userWs;
                                }else {
                                    List<DjpProductSaleWsData> djpProductSaleWsDataList = new ArrayList<>();
                                    DjpProductSaleWsData djpProductSaleWsData = new DjpProductSaleWsData();
                                    djpProductSaleWsData.setGuestCount(qrForCardModel.getGuestQuantity());
                                    djpProductSaleWsData.setPackageName(qrTypeEnumHashMap.get(qrType));
                                    djpProductSaleWsDataList.add(djpProductSaleWsData);
                                    userWs.setUserProducts(djpProductSaleWsDataList);
                                    result.setType("S");
                                    result.setMessage("Kart Kullanılabilir.");
                                    userWs.setResult(result);
                                    return userWs;

                                }
                            }

                        }
                    }
                } else {
                    QrForFioriModel qrForFioriModel = djpQRService.validQrCodeForFiori(qrForFioriRequest.getTokenKey());
                    if (Objects.nonNull(qrForFioriModel)) {
                        if (Objects.isNull(qrForFioriModel.getWhereUsed())) {
                            CustomerData customerData = customerFacade.getUserForUID(qrForFioriModel.getTokenKey().getUserUid());
                            String fieldsDto = fields;
                            userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForFiori(fieldsDto, qrForFioriModel.getTokenKey(), customerData, qrForFioriRequest, qrForFioriModel,djpPassLocation);
                            LOG.info("responseForFiori");
                            return userWs;
                        } else {
                            result.setType("E");
                            result.setMessage("Bu QR daha önce kullanılmıştır. Lütfen geçerli bir qr kod giriniz.");
                            userWs.setResult(result);
                            djpQRService.saveUnapprovedPass(qrForFioriModel.getTokenKey().getCode(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForFiori");
                            LOG.error("responseForFiori" + result.getType());
                            return userWs;
                        }

                    } else {
                        QrForMobileModel qrForMobileModel = djpQRService.validQrCodeForMobile(qrForFioriRequest.getTokenKey());
                        if (Objects.nonNull(qrForMobileModel)) {
                            if (Objects.isNull(qrForMobileModel.getWhereUsed())) {
                                CustomerModel customerByUid = djpCustomerService.findCustomerByUid(qrForMobileModel.getTokenKey().getUserUid());
                                CustomerData customerData = customerFacade.getUserForUID(qrForMobileModel.getTokenKey().getUserUid());
                                String fieldsDto = fields;
                                userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForMobile(fieldsDto, qrForMobileModel.getTokenKey(), customerData, qrForFioriRequest, qrForMobileModel,null,null,djpPassLocation);
                                if (Objects.nonNull(customerByUid)) {
                                    userWs.setEmail(customerByUid.getMail());
                                    if (Objects.nonNull(customerByUid.getGender())) {
                                        userWs.setGender(customerByUid.getGender().getCode().equals("KADIN") || customerByUid.getGender().getCode().equals("WOMAN") ? "1" : "2");
                                    }
                                    userWs.setKvkkCheck(Objects.nonNull(customerByUid.getSmsPreference()) && Objects.nonNull(customerByUid.getEmailPreference()) ? customerByUid.getSmsPreference() && customerByUid.getEmailPreference() : false);
                                    if (Objects.nonNull(customerByUid.getCountry())) {
                                        List<DialingCodeData> allDialingCodes = djpInternalizationFacade.getAllDialingCodes();
                                        Optional<DialingCodeData> first = allDialingCodes.stream().filter(dialingCodeData -> dialingCodeData.getIsoCode().equals(customerByUid.getCountry().getIsoCode())).findFirst();
                                        userWs.setPhonePrefix(first.isPresent() ? "+" + first.get().getDialingCode() : null);
                                    }
                                }
                                LOG.info("responseForMobile");
                                return userWs;
                            } else {
                                result.setType("E");
                                result.setMessage("Bu QR daha önce kullanılmıştır. Lütfen geçerli bir qr kod giriniz.");
                                userWs.setResult(result);
                                djpQRService.saveUnapprovedPass(qrForMobileModel.getTokenKey().getCode(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForMobile");
                                LOG.error("responseForFiori:" + result.getType());
                                return userWs;
                            }
                        } else {
                            QrForExternalModel qrForExternalModel = djpQRService.getQRForExternalByTokenKey(qrForFioriRequest.getTokenKey());
                            if(Objects.nonNull(qrForExternalModel)) {
                                ContractedCompaniesModel contractedCompany = djpQRService.getContractedCompany(qrForExternalModel.getCompanyClientId(), qrForFioriRequest.getQrType(), djpPassLocation);
                                if (Objects.isNull(contractedCompany) && Objects.nonNull(djpPassLocation)) {
                                    result.setType("E");
                                    result.setMessage("Qr " + djpPassLocation.getName() + " Havalimanında Geçerli Değildir!");
                                    userWs.setResult(result);
                                    djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                                    return userWs;
                                }else if (DjpUtil.dateCompare(qrForExternalModel.getTokenKey().getExpireDate(),currentDate) < 0 && ("dragonpass_app".equals(qrForExternalModel.getCompanyClientId()) || "highpass_app".equals(qrForExternalModel.getCompanyClientId()))){
                                    result.setType("E");
                                    result.setMessage("QR Geçerlilik Süresi Dolmuştur");
                                    result.setContractID(qrForExternalModel.getContractId());
                                    userWs.setResult(result);
                                    djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(),result.getMessage(),qrForFioriRequest.getQrType(),"QrForExternal");
                                    return userWs;
                                }else {
                                    if(QrType.FAST_TRACK.getCode().equals(qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0,2)))){
                                        List<DjpExternalCompanyUsagesModel> djpExternalCompanyUsagesModelList = djpQRService.getQrExternalCompanyUsagesList(qrForFioriRequest.getTokenKey());
                                        String qrType = qrTypeEnumHashMap.get(qrForFioriRequest.getQrType());
                                        boolean isDoor1Or3Or7 = QrType.FAST_TRACK_DEPARTURE_DOOR1.getCode().equals(qrType) ||
                                                QrType.FAST_TRACK_DEPARTURE_DOOR3.getCode().equals(qrType) ||
                                                QrType.FAST_TRACK_DEPARTURE_DOOR7.getCode().equals(qrType);
                                        if ((Objects.isNull(qrForExternalModel.getWhereUsed()) && isDoor1Or3Or7) || (djpExternalCompanyUsagesModelList.size() == 1 && !isDoor1Or3Or7)
                                        ) {
                                            userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForMobile(null, qrForExternalModel.getTokenKey(), null, qrForFioriRequest, null, null, qrForExternalModel, djpPassLocation);
                                            return userWs;
                                        } else if(Objects.nonNull(qrForExternalModel.getWhereUsed()) && isDoor1Or3Or7)  {
                                            result.setType("E");
                                            result.setMessage("External QR daha önce aynı hizmet noktasından kullanılmıştır. Aynı kapıdan ikinci kez kullanılamaz.");
                                            userWs.setResult(result);
                                            djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                                            LOG.error("responseForFiori:" + result.getType());
                                            return userWs;
                                        } else if(Objects.isNull(qrForExternalModel.getWhereUsed()) && !isDoor1Or3Or7)  {
                                            result.setType("E");
                                            result.setMessage("Bu QR Kodun ilk başta 1, 3 veya 7 numaralı kapılarda kullanılmış olması gerekmektedir. Eğer yolcu bu kapılardan başka bir yere gelmiş ise NonSAP sisteminde önce 1 numaralı kapıdan geçiş yaptırıp sonra kendi bulunduğunuz kapıdan boardingini okutarak geçiş yaptırınız.");
                                            userWs.setResult(result);
                                            djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                                            LOG.error("responseForFiori:" + result.getType());
                                            return userWs;
                                        } else {
                                            result.setType("E");
                                            result.setMessage("External QR daha önce kullanılmıştır. Lütfen geçerli bir qr kod giriniz.");
                                            userWs.setResult(result);
                                            djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                                            LOG.error("responseForFiori:" + result.getType());
                                            return userWs;
                                        }
                                    }else {
                                        if (Objects.isNull(qrForExternalModel.getWhereUsed())) {
                                            userWs = djpPremiumServiceUsageStrategy.fillUserWsDTOByQrForMobile(null, qrForExternalModel.getTokenKey(), null, qrForFioriRequest, null, null, qrForExternalModel, djpPassLocation);
                                            return userWs;
                                        } else {
                                            result.setType("E");
                                            result.setMessage("External QR daha önce kullanılmıştır. Lütfen geçerli bir qr kod giriniz.");
                                            userWs.setResult(result);
                                            djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                                            LOG.error("responseForFiori:" + result.getType());
                                            return userWs;
                                        }
                                    }
                                }

                            } else {
                                QrCodeValidResponse qrCodeValidResponse = djpQRFacade.generateResponse(qrForFioriRequest.getTokenKey(), qrForFioriRequest.getQrType(), currentDate, qrForFioriRequest.getNewSale(),djpPassLocation);
                                if(DjpCoreConstants.TOKEN_TYPE_FOR_SOME_BODY.equals(qrCodeValidResponse.getType())){
                                    return djpPremiumServiceUsageStrategy.fillUserWsDTOForSomeBody(qrForFioriRequest);
                                }else if(DjpCoreConstants.ExternalCompany.QR_TYPE_FOR_DJP_EXTERNAL_CUSTOMER.equals(qrCodeValidResponse.getType())){
                                    return djpPremiumServiceUsageStrategy.fillUserWsDTOForExternalCustomer(qrForFioriRequest);
                                }else if(DjpCoreConstants.TOKEN_TYPE_FOR_SINGLE_CUSTOM_CODE.equals(qrCodeValidResponse.getType())){
                                    return djpPremiumServiceUsageStrategy.fillUserWsDTOForSingleCustomCode(qrForFioriRequest,currentDate);
                                }else {
                                    result.setMessage(qrCodeValidResponse.getMessage());
                                    result.setType(qrCodeValidResponse.getType());
                                    result.setCustomerType(qrCodeValidResponse.getCustomerType());
                                    result.setContractID(qrCodeValidResponse.getContractID());
                                    userWs.setResult(result);
                                    if (result.getType().equals("S")) {
                                        List<DjpProductSaleWsData> djpProductSaleWsDataList = new ArrayList<DjpProductSaleWsData>();
                                        if (Objects.nonNull(qrCodeValidResponse.getPackageData())) {
                                            djpProductSaleWsDataList.add(qrCodeValidResponse.getPackageData());
                                        } else {
                                            DjpProductSaleWsData djpProductSaleWsData = new DjpProductSaleWsData();
                                            String qrType = qrTypeEnumHashMap.get(qrForFioriRequest.getQrType());
                                            String packageName = "";
                                            QrForVoucherModel voucherModel =  djpQRService.validQrCodeForDjp(qrForFioriRequest.getTokenKey());
                                            if (Objects.nonNull(voucherModel)){
                                                if (!CollectionUtils.isEmpty(voucherModel.getRemainingForPackage())){
                                                    if (Objects.nonNull(voucherModel.getRemainingForPackage().iterator().next())){
                                                        packageName = voucherModel.getRemainingForPackage().iterator().next().getPackagedProduct().getName();
                                                    } else {
                                                        String[] split = qrType.split("_");
                                                        packageName = qrType.startsWith(QrType.BAVUL_KAPLAMA.getCode()) ? qrType : qrType.startsWith(QrType.FAST_TRACK.getCode()) ? split[0] + " " + split[1] : split[0];
                                                    }
                                                }else {
                                                    String[] split = qrType.split("_");
                                                    packageName = qrType.startsWith(QrType.BAVUL_KAPLAMA.getCode()) ? qrType : qrType.startsWith(QrType.FAST_TRACK.getCode()) ? split[0] + " " + split[1] : split[0];
                                                }
                                            }else {
                                                String[] split = qrType.split("_");
                                                packageName = qrType.startsWith(QrType.BAVUL_KAPLAMA.getCode()) ? qrType : qrType.startsWith(QrType.FAST_TRACK.getCode()) ? split[0] + " " + split[1] : split[0];
                                            }
                                            djpProductSaleWsData.setPackageName(packageName);
                                            djpProductSaleWsData.setGuestCount(0);
                                            djpProductSaleWsDataList.add(djpProductSaleWsData);

                                        }
                                        userWs.setUserProducts(djpProductSaleWsDataList);
                                    }
                                    return userWs;

                                }
                            }
                        }
                    }
                }
            } else if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode())) {
                pnrCodeForTourniquet = qrForFioriRequest.getPnrCode().toUpperCase().replaceAll("\\.", "/");
                pnrCodeForTourniquet = pnrCodeForTourniquet.replaceAll("&", "/");
                qrForFioriRequest.setPnrCode(pnrCodeForTourniquet);
                FioriStatusResponse response = new FioriStatusResponse();
                List<DjpAccessRightsModel> accessRight = new ArrayList<DjpAccessRightsModel>();
                List<String> membershipCodes = djpTokenService.getMembershipCodes();
                boolean checkIfStartWithBp = false;
                boolean isServiceUsageWithMembership = false;
                if(membershipCodes.isEmpty()){
                    isServiceUsageWithMembership = false;
                }else {
                    String airlineCode = qrForFioriRequest.getPnrCode().substring(0,2).toUpperCase();
                    isServiceUsageWithMembership = membershipCodes.contains(airlineCode);
                }
                if(!isServiceUsageWithMembership) {
                    checkIfStartWithBp = !qrForFioriRequest.getPnrCode().startsWith("M1") && !qrForFioriRequest.getPnrCode().startsWith("M2") && !qrForFioriRequest.getPnrCode().startsWith("M3");
                    if(checkIfStartWithBp){
                        response.setMessage("Boarding Kart M1 / M2 / M3 Karakterleriyle Başlamalıdır!");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        return userWs;
                    }

                    djpBoardingService.checkIfUsageByBoardingPass(qrForFioriRequest, null, null, null);
                    if (Objects.nonNull(qrForFioriRequest.getCanUseService())) {
                        if (!qrForFioriRequest.getCanUseService()) {
                            response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" hakkından yararlanabilir.");
                            response.setType("E");
                            userWs.setResult(response);
                            LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                            return userWs;
                        }
                    }
                    djpBoardingService.checkIfHaveDjpPassAndGenerateBoardingPass(qrForFioriRequest,userWs,djpPassLocation,UsageMethod.DESK);
                    boolean isHaveDjpPass = Objects.nonNull(qrForFioriRequest.getCanUseService()) && qrForFioriRequest.getCanUseService() && Objects.nonNull(qrForFioriRequest.getHaveDjpPass()) && qrForFioriRequest.getHaveDjpPass();
                    if(isHaveDjpPass){
                        return userWs;
                    }
                    boolean cantUseService = Objects.nonNull(qrForFioriRequest.getCanUseService()) && !qrForFioriRequest.getCanUseService();
                    if(cantUseService){
                        response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" hakkından yararlanabilir.");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        return userWs;
                    }
                }
                if (!isServiceUsageWithMembership && StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && qrForFioriRequest.getPnrCode().length() > 50) {

                    if((!djpBoardingFacade.isChildBoardingPass(qrForFioriRequest.getPnrCode(),false) && PassengerType.BABY.getCode().equals(qrForFioriRequest.getPassengerType())) ||
                            (djpBoardingFacade.isChildBoardingPass(qrForFioriRequest.getPnrCode(),false) && !"GENERAL".equals(qrForFioriRequest.getPassengerType()))){
                        response.setMessage("Bebek boardingi girilmiştir, yetişkin boardingini aldığınızdan emin olunuz");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        userWs.setResult(response);
                        return userWs;
                    }

                    if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
                        try {
                            String status = djpBoardingService.checkInternationalStatusForBp(qrForFioriRequest, null, null, null, QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())));
                            if (StringUtils.isNotBlank(status)) {
                                if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(status)) {
                                    response.setMessage("Yolcuya ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(status)) {
                                    response.setMessage("Yolcuya ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(status)){
                                    response.setMessage("Uçuş iGA da planlanmamaktadır.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                }
                            }
                        } catch (Exception ex) {
                            LOG.error("checkInternationalStatusForBp has an error:", ex);
                            response.setMessage("Uçuş iGA da planlanmamaktadır.");
                            response.setType("E");
                            LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                            userWs.setResult(response);
                            LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                            return userWs;
                        }
                    }

                    if ("PROD".equals(HOST_TYPE)) {
                        diffBetweenToDate = djpBoardingService.getDiffBetweenToDate(qrForFioriRequest, null, null, null);
                        if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                            response.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                            LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                            response.setType("E");
                        } else {
                            messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                            if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                                response.setMessage(messageForCheckIfBoardingForLocationIATA);
                                response.setType("E");
                            } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                                response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                                response.setType("E");
                            } else {
                                accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getPnrCode().substring(36, 39),djpPassLocation,isServiceUsageWithMembership);
                                djpPremiumServiceUsageStrategy.fillResponseByAccessRight(response, accessRight, qrForFioriRequest, false, UsageMethod.DESK);
                            }
                        }
                    }
                    else {
                        messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                        if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                            response.setMessage(messageForCheckIfBoardingForLocationIATA);
                            response.setType("E");
                        } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                            response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                            response.setType("E");
                        } else {
                            accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getPnrCode().substring(36, 39),djpPassLocation,isServiceUsageWithMembership);
                            djpPremiumServiceUsageStrategy.fillResponseByAccessRight(response, accessRight, qrForFioriRequest, false, UsageMethod.DESK);
                        }
                    }


                } else if (qrForFioriRequest.getPnrCode().length() < 50 || (qrForFioriRequest.getPnrCode().startsWith("MA") && qrForFioriRequest.getPnrCode().length() >= 30 )) {
                    accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getPnrCode().substring(0, 2).toUpperCase(),djpPassLocation,isServiceUsageWithMembership);
                    djpPremiumServiceUsageStrategy.fillResponseByAccessRight(response, accessRight, qrForFioriRequest, false, UsageMethod.DESK);
                } else {
                    response.setMessage("Hatalı pnr kodu");
                    response.setType("E");
                    LOG.info("validateaccessboardingpass code is not valid 202");
                }
                if("E".equals(response.getType())){
                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                }
                userWs.setResult(response);
                return userWs;
            } else if(couldNotReadBoardingPass){
                LOG.info("validateaccessboardingpass:" + "flightNumber:"+qrForFioriRequest.getFlightNumber()+" classCode:"+qrForFioriRequest.getClassCode()+" for CouldNotReadBoardingPass:"+qrForFioriRequest.getCouldNotReadBoardingPass());
                FioriStatusResponse response = new FioriStatusResponse();
                List<DjpAccessRightsModel> accessRight = new ArrayList<DjpAccessRightsModel>();

                djpBoardingService.checkIfUsageByBoardingPass(qrForFioriRequest,null,null,null);
                if(Objects.nonNull(qrForFioriRequest.getCanUseService())) {
                    if (!qrForFioriRequest.getCanUseService()) {
                        response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" hakkından yararlanabilir.");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        return userWs;
                    }
                }
                djpBoardingService.checkIfHaveDjpPassAndGenerateBoardingPass(qrForFioriRequest,userWs,djpPassLocation,UsageMethod.DESK);
                boolean isHaveDjpPass = Objects.nonNull(qrForFioriRequest.getCanUseService()) && qrForFioriRequest.getCanUseService() && Objects.nonNull(qrForFioriRequest.getHaveDjpPass()) && qrForFioriRequest.getHaveDjpPass();
                if(isHaveDjpPass){
                    return userWs;
                }
                boolean cantUseService = Objects.nonNull(qrForFioriRequest.getCanUseService()) && !qrForFioriRequest.getCanUseService();
                if(cantUseService){
                    response.setMessage("Bu yolcunun boarding kartı yeni bir işlem kaydında kullanılamaz. "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" girişi için yolcunun kaydı daha önce alınmıştır ve ilgili kaydı ile "+QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))+" hakkından yararlanabilir.");
                    response.setType("E");
                    userWs.setResult(response);
                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                    return userWs;
                }
                if (StringUtils.isNotBlank(qrForFioriRequest.getFlightNumber())) {
                    if(qrForFioriRequest.getFromAirportIATA().length() != 3){
                        response.setMessage("FromAirport 3 Karakter Olmalı!");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        return userWs;
                    }
                    if(qrForFioriRequest.getToAirportIATA().length() != 3){
                        response.setMessage("FromAirport 3 Karakter Olmalı!");
                        response.setType("E");
                        userWs.setResult(response);
                        LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                        return userWs;
                    }

                    if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
                        try {
                            String status = djpBoardingService.checkInternationalStatusForBp(qrForFioriRequest, null, null, null, QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())));
                            if (StringUtils.isNotBlank(status)) {
                                if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(status)) {
                                    response.setMessage("Yolcuya ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(status)) {
                                    response.setMessage("Yolcuya ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(status)){
                                    response.setMessage("Uçuş iGA da planlanmamaktadır.");
                                    response.setType("E");
                                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                                    userWs.setResult(response);
                                    return userWs;
                                }
                            }
                        } catch (Exception ex) {
                            LOG.error("checkInternationalStatusForBp has an error:", ex);
                            response.setMessage("Uçuş iGA da planlanmamaktadır.");
                            response.setType("E");
                            LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                            userWs.setResult(response);
                            return userWs;
                        }
                    }

                    if ("PROD".equals(HOST_TYPE)) {
                        diffBetweenToDate = djpBoardingService.getDiffBetweenToDate(qrForFioriRequest, null, null, null);
                        if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                            response.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                            LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                            response.setType("E");
                        } else {
                            messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                            if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                                response.setMessage(messageForCheckIfBoardingForLocationIATA);
                                response.setType("E");
                            } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                                response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                                response.setType("E");
                            } else {
                                accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2),djpPassLocation,Boolean.FALSE);
                                djpPremiumServiceUsageStrategy.fillResponseByAccessRight(response, accessRight, qrForFioriRequest, true,UsageMethod.DESK);
                            }
                        }
                    } else {
                        messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                        if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                            response.setMessage(messageForCheckIfBoardingForLocationIATA);
                            response.setType("E");
                        } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                            response.setMessage("Boarding kartın kalkış noktası İstanbul değildir, yolcunun Hizmet Kullanım hakkı yoktur.");
                            response.setType("E");
                        } else {
                            accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getFlightNumber().toUpperCase().substring(0, 2),djpPassLocation,Boolean.FALSE);
                            djpPremiumServiceUsageStrategy.fillResponseByAccessRight(response, accessRight, qrForFioriRequest, true, UsageMethod.DESK);
                        }
                    }

                } else {
                    response.setMessage("Hatalı pnr kodu");
                    response.setType("E");
                    LOG.info("validateaccessboardingpass code is not valid 202");
                }

                if("E".equals(response.getType())){
                    LOG.error("responseForFiori:" + response.getType()+" - "+response.getMessage());
                }

                userWs.setResult(response);
                return userWs;
            }
            else {
                result.setType("E");
                result.setMessage("Request içeresinde gerekli alanları doldurunuz!");
                userWs.setResult(result);
                LOG.error("responseForFiori:" + result.getType());
                return userWs;
            }
        }
    }
    private String getJsonFromRequst (Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }

    private String generateLog (String jsonFromRequest) {
        JSONObject jsonObject = new JSONObject(jsonFromRequest);
        return jsonObject.toString();
    }
}
