package com.djp.facades.impl;

import com.djp.core.model.DjpTelCountriesModel;
import com.djp.dto.DjpTelephoneCountriesWsDto;
import com.djp.facades.DjpTelephoneCountriesFacade;
import com.djp.populator.DjpTelephoneCountriesPopulator;
import com.djp.service.DjpTelephoneCountriesService;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class DjpTelephoneCountriesFacadeImpl implements DjpTelephoneCountriesFacade {

    private DjpTelephoneCountriesService telephoneCountriesService;

    private DjpTelephoneCountriesPopulator djpTelephoneCountriesPopulator;

    public DjpTelephoneCountriesFacadeImpl(DjpTelephoneCountriesService telephoneCountriesService, DjpTelephoneCountriesPopulator djpTelephoneCountriesPopulator) {
        this.telephoneCountriesService = telephoneCountriesService;
        this.djpTelephoneCountriesPopulator = djpTelephoneCountriesPopulator;
    }

    @Override
    public List<DjpTelephoneCountriesWsDto> findTelephoneCountries() {
        List<DjpTelCountriesModel> djpTelCountriesModels=getTelephoneCountriesService().findCountriesTelephone();
        List<DjpTelephoneCountriesWsDto> djpTelCountriesList= new ArrayList<>();
        if(!CollectionUtils.isEmpty(djpTelCountriesModels)){
            djpTelCountriesModels.stream().forEach(djpTelCountriesModel ->djpTelCountriesList.add(getDjpTelephoneCountriesPopulator().modelToDto(new DjpTelephoneCountriesWsDto(),djpTelCountriesModel )));
        }
        return djpTelCountriesList;
    }

    public DjpTelephoneCountriesPopulator getDjpTelephoneCountriesPopulator() {
        return djpTelephoneCountriesPopulator;
    }

    public void setDjpTelephoneCountriesPopulator(DjpTelephoneCountriesPopulator djpTelephoneCountriesPopulator) {
        this.djpTelephoneCountriesPopulator = djpTelephoneCountriesPopulator;
    }

    public DjpTelephoneCountriesService getTelephoneCountriesService() {
        return telephoneCountriesService;
    }

    public void setTelephoneCountriesService(DjpTelephoneCountriesService telephoneCountriesService) {
        this.telephoneCountriesService = telephoneCountriesService;
    }
}
