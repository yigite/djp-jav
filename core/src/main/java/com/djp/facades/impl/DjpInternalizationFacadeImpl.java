package com.djp.facades.impl;

import com.djp.core.dao.DjpInternalizationDao;
import com.djp.facades.DjpInternalizationFacade;
import com.djp.core.model.DialingCodeModel;
import com.djp.data.DialingCodeData;
import com.djp.populator.Populator;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class DjpInternalizationFacadeImpl implements DjpInternalizationFacade {

    @Resource
    DjpInternalizationDao djpInternalizationDao;

    @Resource
    Populator<DialingCodeModel,DialingCodeData> djpInternalizationPopulator;

    @Override
    public List<DialingCodeData> getAllDialingCodes() {
        List<DialingCodeModel> dialingCodeModelList = djpInternalizationDao.getDialogCodes();
        List<DialingCodeData> dialingCodeDataList = new ArrayList<>();
        dialingCodeModelList.stream().forEach(dialingCodeModel->{
            DialingCodeData dialingCodeData = djpInternalizationPopulator.populate(dialingCodeModel);
            dialingCodeDataList.add(dialingCodeData);
        });

        return dialingCodeDataList;
    }
}
