package com.djp.facades.impl;

import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.model.DjpCppTxnModel;
import com.djp.core.model.enumeration.CppTokenStatus;
import com.djp.data.DjpCppInvoiceRequestData;
import com.djp.data.DjpCppTxnData;
import com.djp.data.DjpCppTxnInvoiceData;
import com.djp.facades.DjpCppFacade;
import com.djp.modelservice.util.DateUtil;
import com.djp.populator.impl.DjpCppDataReversePopulator;
import com.djp.populator.impl.DjpCppPopulator;
import com.djp.request.DjpCppWsRequest;
import com.djp.response.DjpCppWsResponse;
import com.djp.service.DjpCppService;
import com.djp.util.DjpServiceUtil;
import com.djp.util.DjpUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import jakarta.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class DjpCppFacadeImpl implements DjpCppFacade {
    private static final Logger LOG = LoggerFactory.getLogger(DjpCppFacadeImpl.class);

    @Resource
    private MessageSource messageSource;

    @Resource
    private DjpCppPopulator djpCppDataPopulator;

    @Resource
    private DjpCppDataReversePopulator djpCppDataReversePopulator;

    @Resource
    private DjpCppService djpCppService;

    @Resource
    private HashMap<String, String> djpCppLinkSpecialTypeMap;

    @Override
    public DjpCppTxnData getDjpCppTxnDataByToken(final String djpCppToken) {
        DjpCppTxnModel djpCppTxnModel = djpCppService.getDjpCppTxnDataByToken(djpCppToken);
        if (djpCppTxnModel == null) {
            return new DjpCppTxnData();
        }
        return djpCppDataPopulator.populate(djpCppTxnModel);
    }
    @Override
    public DjpCppWsResponse initPayment(DjpCppWsRequest djpCppWsRequest) {
        Date currentDate = new Date();
        Date convertedReservationDate = null;
        DjpCppWsResponse response = new DjpCppWsResponse();
        try {
            if (StringUtils.isNotBlank(djpCppWsRequest.getReservationDate())) {
                convertedReservationDate = DateUtil.stringToDateAndTime(djpCppWsRequest.getReservationDate(), "yyyyMMdd HHmmss");
                if (DjpUtil.dateCompare(convertedReservationDate, currentDate) < 0) {
                    LOG.error("invalid reservationDate:" + djpCppWsRequest.getReservationDate());
                    response.setResult(DjpServiceUtil.generateErrorServiceResult(messageSource.getMessage("rest.cpp.invalid.reservation.date.exception",null, LocaleContextHolder.getLocale())));
                    return response;
                }
            }
            String djpCppToken = UUID.randomUUID().toString();

            DjpCppTxnData cppData = this.getDjpCppTxnDataByToken(djpCppToken);
            if (StringUtils.isNotEmpty(cppData.getCppToken())) {
                djpCppToken = UUID.randomUUID().toString();
            }

            DjpCppTxnData djpCppTxnData = new DjpCppTxnData();
            djpCppTxnData.setCppToken(djpCppToken);
            djpCppTxnData.setName(djpCppWsRequest.getName());
            djpCppTxnData.setSurName(djpCppWsRequest.getSurName());
            djpCppTxnData.setAmount(djpCppWsRequest.getAmount());
            djpCppTxnData.setLocation(djpCppWsRequest.getLocation());
            djpCppTxnData.setLanguage(djpCppWsRequest.getLanguage());
            djpCppTxnData.setReservationNumber(djpCppWsRequest.getReservationNumber());
            djpCppTxnData.setInvoiceAddress(getInvoiceData(djpCppWsRequest.getInvoiceDetail(), djpCppWsRequest.getLocation()));
            djpCppTxnData.setYouthLoungeCampaignCode(djpCppWsRequest.getYouthLoungeCampaignCode());
            /*djpCppTxnData.setCountry(StringUtils.isNotBlank(djpCppWsRequest.getCountry()) ? djpCppWsRequest.getCountry().toUpperCase() : "");*/
            if(djpCppWsRequest.getInvoiceDetail()!=null) {
                djpCppTxnData.setYouthLoungeEmail(djpCppWsRequest.getInvoiceDetail().getEmail());
            }

            DjpCppTxnModel djpCppTxnModel = djpCppDataReversePopulator.populate(djpCppTxnData);
            djpCppTxnModel.setTokenStatus(CppTokenStatus.GENERATED);
            generateExpireDate(currentDate, djpCppTxnModel, djpCppWsRequest.getLocation(), convertedReservationDate);
            response.setCppToken(djpCppService.initPayment(djpCppTxnModel, djpCppTxnData));
            return response;
        } catch (Exception ex) {
            LOG.error("DjpCppFacade::initPayment has an error:", ex);
            response.setResult(DjpServiceUtil.generateErrorServiceResult(messageSource.getMessage("rest.controller.system.exception",null,LocaleContextHolder.getLocale())));
            return response;
        }
    }
    private void generateExpireDate(Date currentDate, DjpCppTxnModel igaCppTxnModel, String location, Date reservationDate) {
        Date expireDate = DateUtil.addHourByPropertyMinute(currentDate);
        if (DjpCoreConstants.DjpSapReturns.ERP.equals(location)) {
            if (Objects.nonNull(reservationDate)) {
                igaCppTxnModel.setExpireDate(DateUtil.generateDateByReservationDate(currentDate, reservationDate));
            } else {
                igaCppTxnModel.setExpireDate(expireDate);
            }
        } else {
            boolean isSpecialLinkType = org.springframework.util.StringUtils.hasText(djpCppLinkSpecialTypeMap.get(location));
            if (Boolean.TRUE.equals(isSpecialLinkType)) {
                igaCppTxnModel.setExpireDate(DateUtil.addOneDay(currentDate));
            } else {
                igaCppTxnModel.setExpireDate(expireDate);
            }
        }
        igaCppTxnModel.setReservationDate(reservationDate);
    }
    private DjpCppTxnInvoiceData getInvoiceData(DjpCppInvoiceRequestData invoiceData, String location){
        if(Objects.nonNull(invoiceData) && DjpCoreConstants.DjpSapReturns.ERP.equals(location)){
            DjpCppTxnInvoiceData data = new DjpCppTxnInvoiceData();
            data.setCompanyID(invoiceData.getCompanyID());
            data.setAddress(invoiceData.getAddress());
            data.setEmail(invoiceData.getEmail());
            data.setName(invoiceData.getName());
            data.setPhoneNumber(invoiceData.getPhoneNumber());
            data.setIdentityNumber(invoiceData.getIdentityNumber());
            data.setTaxOffice(invoiceData.getTaxOffice());
            return data;
        }
        return null;
    }
}
