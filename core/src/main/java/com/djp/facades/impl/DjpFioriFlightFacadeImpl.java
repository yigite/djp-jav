package com.djp.facades.impl;

import com.djp.core.model.FlightInformationModel;
import com.djp.data.FioriFlightDetailData;
import com.djp.facades.DjpFioriFlightFacade;
import com.djp.populator.Populator;
import com.djp.service.DjpFioriFlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DjpFioriFlightFacadeImpl implements DjpFioriFlightFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpFioriFlightFacadeImpl.class);

    @Resource
    DjpFioriFlightService djpFioriFlightService;

    @Resource
    Populator<FlightInformationModel,FioriFlightDetailData> djpFioriFlightPopulator;

    @Override
    public FioriFlightDetailData getFioriFlightDetail(String flightDate, String flightNumber) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date flightDateConvert = simpleDateFormat.parse(flightDate);
            FlightInformationModel flightInformationModel = djpFioriFlightService.getFlightInformation(flightDateConvert, flightNumber);
            return djpFioriFlightPopulator.populate(flightInformationModel);
        } catch (ParseException e) {
            LOGGER.error("DjpFioriFlightFacadeImpl : getFioriFlightDetail has error", e);
            return null;
        }
    }

    public void setDjpFioriFlightPopulator(Populator<FlightInformationModel, FioriFlightDetailData> djpFioriFlightPopulator) {
        this.djpFioriFlightPopulator = djpFioriFlightPopulator;
    }
}
