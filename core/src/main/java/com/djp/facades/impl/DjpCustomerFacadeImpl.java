package com.djp.facades.impl;

import com.djp.core.model.CustomerModel;
import com.djp.data.CustomerData;
import com.djp.facades.DjpCustomerFacade;
import com.djp.populator.impl.custom.DjpCustomerPopulator;
import com.djp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;

public class DjpCustomerFacadeImpl implements DjpCustomerFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DjpCustomerFacadeImpl.class);

    @Resource
    private DjpCustomerPopulator djpCustomerPopulator;

    @Resource
    private UserService userService;

    @Override
    public CustomerData getUserForUID(final String userId)
    {
        try {
            if(StringUtils.hasText(userId)){
                return djpCustomerPopulator.populate((CustomerModel) userService.getUserForUID(userId));
            }
        }catch (Exception ex){
            LOG.error("getUserForUIDExp: " + ex);
        }
        return null;
    }
}
