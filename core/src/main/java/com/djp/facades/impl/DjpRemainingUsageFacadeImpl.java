package com.djp.facades.impl;


import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.PassengerType;
import com.djp.core.enums.PremiumPackagePassengerEnum;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.data.MultiUsagesData;
import com.djp.dto.DjpProductSaleWsData;
import com.djp.facades.DjpRemainingUsageFacade;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.request.BoardingPassRequest;
import com.djp.request.QrPassRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.service.DjpCustomerService;
import com.djp.service.DjpRemainingUsageService;
import com.djp.service.DjpTokenService;

import com.djp.modelservice.ModelService;
import com.djp.populator.Populator;
import com.djp.service.DjpRemainingUsageService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

public class DjpRemainingUsageFacadeImpl implements DjpRemainingUsageFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpRemainingUsageFacadeImpl.class);
    @Resource
    private DjpRemainingUsageService djpRemainingUsageService;

    @Value("${djpcarparingusage.defaultday.DailyDomestic}")
    private String DailyDomestic;

    @Value("${server.host.type}")
    private String HOST_TYPE;

    @Resource
    private HashMap<String, String> qrTypeForRemainingHashMap;

    @Resource
    private ModelService modelService;

    @Resource
    private DjpCustomerService djpCustomerService;

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private HashMap<String, String> productCodeReverseMapForTestEnvironment;

    @Resource
    private HashMap<String, String> qrTypeForPackagedProductRemainingHashMap;

    @Resource
    private HashMap<String, String> domesticPackageEnumHashMap;

    @Resource
    private Populator<DjpRemainingUsageModel, DjpProductSaleWsData> djpProductSaleWsDataPopulator;

    @Override
    public List<DjpProductSaleWsData> getUserProducts(String uid, String productCategory, Object o, QrForFioriModel qrForFioriModel, Object o1, Object o2, Object o3, String qrType) {
        return null;
    }

    @Override
    public void addUsedInDjpRemainingUsage(FioriStatusResponse statusResponse, String customerUid, String productCategory, BoardingPassRequest boardingPassRequest, QrPassRequest qrPassRequest, boolean usageWithDjpPassCard, QrForExternalModel qrForExternalModel, String boardingTypeCode) {
        try {
            DjpUniqueTokenModel djpUniqueTokenModel;
            List<DjpRemainingUsageModel> customerUsages = null;
            Boolean isExternalCustomer = false;
            OrderModel forSomeBodyOrderByToken = null;
            CustomerModel customerForSomeBody = null;
            if (DjpCoreConstants.TOKEN_TYPE_FOR_SOME_BODY.equals(customerUid)) {
                forSomeBodyOrderByToken = djpRemainingUsageService.getOrderByToken(qrPassRequest.getTokenKey());
                String phoneNumber = forSomeBodyOrderByToken.getDjpOtherCustomer().getPhone();
                phoneNumber = phoneNumber.contains(" ") ? phoneNumber.replace(" ", "") : phoneNumber;
                customerForSomeBody = djpCustomerService.findCustomerByUid(phoneNumber);
                String uid = "";
                if (Objects.isNull(customerForSomeBody)) {
                    uid = forSomeBodyOrderByToken.getUser().getUid();
                } else {
                    uid = customerForSomeBody.getUid();
                }
                customerUsages = djpRemainingUsageService.getCustomerUsages(uid, productCategory, null);
                OrderModel finalOrderByToken = forSomeBodyOrderByToken;
                customerUsages = customerUsages
                        .stream()
                        .filter(djpRemainingUsageModel -> finalOrderByToken.getActivationOrUsageCode().equals(djpRemainingUsageModel.getActivationOrUsageCode()))
                        .filter(djpRemainingUsageModel -> Objects.isNull(djpRemainingUsageModel.getPackagedProduct()))
                        .collect(Collectors.toList());
            } else {
                customerUsages = djpRemainingUsageService.getCustomerUsages(customerUid, productCategory, null);
                if(customerUsages.isEmpty()){
                    customerUsages = djpRemainingUsageService.getExternalCustomerUsages(customerUid,productCategory,null);
                    isExternalCustomer = Boolean.TRUE;
                }
            }

            if (!customerUsages.isEmpty()) {

                if(Objects.nonNull(boardingPassRequest) && StringUtils.isNotEmpty(boardingPassRequest.getOrderCode())) {
                    customerUsages = customerUsages.stream().filter(djpRemainingUsageModel -> djpRemainingUsageModel.getOrderCode().equals(boardingPassRequest.getOrderCode())).collect(Collectors.toList());
                }

                String productCode = Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getProductCode() : "";
                if(Objects.nonNull(qrPassRequest)) {
                    djpUniqueTokenModel = djpTokenService.encodeCustomerFromToken(qrPassRequest.getTokenKey());
                    if(Objects.nonNull(djpUniqueTokenModel)){
                        if (StringUtils.isNotBlank(qrPassRequest.getOrderCode())){
                            djpUniqueTokenModel.setOrderCode(qrPassRequest.getOrderCode());
                        }
                        if (StringUtils.isNotBlank(qrPassRequest.getPersonalID())){
                            djpUniqueTokenModel.setPersonalID(qrPassRequest.getPersonalID());
                        }

                        modelService.saveOrUpdate(djpUniqueTokenModel);
                    }
                } else {
                    djpUniqueTokenModel = null;
                }
                if (StringUtils.isNotBlank(productCode)) {
                    Integer adultCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getAdultCount() :Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getAdultCount() : 0;
                    Integer childCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getChildCount() : Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getChildCount() : 0;
                    boolean isPassAdult = adultCount != 0;
                    boolean isPassChild = childCount != 0;
                    String orderCode = boardingPassRequest.getOrderCode();
                    for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                        if (productCode.equals(djpRemainingUsageModel.getProduct().getCode()) && orderCode.equals(djpRemainingUsageModel.getOrderCode())) {
                            addDailyDomesticConditions(djpRemainingUsageService.getDjpRemainingUsageForOrderCode(djpRemainingUsageModel.getOrderCode()),djpRemainingUsageModel);//firstCheckDailyForUsedCondition
                            if(isPassAdult){
                                djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + adultCount);
                            }else if(isPassChild){
                                djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + childCount);
                            }
                            modelService.saveOrUpdate(djpRemainingUsageModel);
                            if(Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())){
                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                modelService.saveOrUpdate(djpUniqueTokenModel);
                            }
                            break;
                        }
                    }
                } else {
                    boolean hasPackagedProductOnCustomer = customerUsages.stream().anyMatch(djpRemainingUsageModel -> djpRemainingUsageModel.getPackagedProduct() != null);
                    if (!hasPackagedProductOnCustomer) {
                        if (Objects.nonNull(boardingTypeCode)){
                            boolean hasServiceProductOnCustomer = customerUsages.stream().anyMatch(djpRemainingUsageModel -> djpRemainingUsageModel.getProduct() != null && qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()).equals(boardingTypeCode)) ;
                            if (hasServiceProductOnCustomer) {
                                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                    ProductModel product = djpRemainingUsageModel.getProduct();
                                    String usedValue = qrTypeForRemainingHashMap.get(product.getCode());
                                    if (boardingTypeCode.equals(usedValue)) {
                                        djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                        modelService.saveOrUpdate(djpRemainingUsageModel);
                                        if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                            djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                            modelService.saveOrUpdate(djpUniqueTokenModel);
                                        }
                                        break;
                                    }
                                }
                            } else {
                                statusResponse.setType("E");
                                statusResponse.setMessage("Müşterinin Ürünü ya da Kullanım Hakkı Bulunmamaktadır.");
                            }
                        } else {
                            String orderCode = StringUtils.isNotBlank(qrPassRequest.getOrderCode()) ? qrPassRequest.getOrderCode().contains(" ") ? qrPassRequest.getOrderCode().replace(" ", "") : qrPassRequest.getOrderCode() : StringUtils.EMPTY;
                            String whereUsed = Objects.nonNull(qrPassRequest) ? qrPassRequest.getQrType() : Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getWhereUsed() : "";
                            Integer adultCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getAdultCount() :Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getAdultCount() : 0;
                            Integer childCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getChildCount() : Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getChildCount() : 0;
                            boolean isPassAdult = adultCount != 0;
                            boolean isPassChild = childCount != 0;
                            String feature = "";

                            if (isPassAdult) {
                                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                    ProductModel product = djpRemainingUsageModel.getProduct();
                                    if (product instanceof ERPVariantProductModel) {
                                        Optional<ProductFeatureModel> availableFeatureOptional = product.getFeatures().stream().filter(productFeatureModel -> productFeatureModel.getQualifier().contains(DjpCoreConstants.DjpVariantFeature.AVAILABLE_QUALIFER_CODE)).findFirst();
                                        for (ProductFeatureModel productFeature : product.getFeatures()) {
                                            try {
                                                if (productFeature.getValue() instanceof ClassificationAttributeValueModel) {
                                                    ClassificationAttributeValueModel c = (ClassificationAttributeValueModel) productFeature.getValue();
                                                    if (c.getCode().startsWith(DjpCoreConstants.DjpVariantFeature.ZMMDJP2_STARTWITH) && c.getCode().equals(DjpCoreConstants.DjpVariantFeature.ADULT)) {
                                                        if(availableFeatureOptional.isPresent()){
                                                            ProductFeatureModel availableFeature = availableFeatureOptional.get();
                                                            ClassificationAttributeValueModel featureValue = (ClassificationAttributeValueModel) availableFeature.getValue();
                                                            feature = ((ERPVariantProductModel) djpRemainingUsageModel.getProduct()).getBaseProduct().getCode() + featureValue.getCode();
                                                            feature = qrTypeForRemainingHashMap.get(feature);
                                                        }
                                                        boolean checkOrder = StringUtils.isNotBlank(orderCode) ? djpRemainingUsageModel.getOrderCode().equals(orderCode): Boolean.TRUE;
                                                        if(whereUsed.startsWith(feature) && Boolean.TRUE.equals(checkOrder)) {
                                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + adultCount);
                                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            } catch (Exception e) {
                                                LOGGER.error("addRemainingAdult has error", e);
                                            }
                                        }
                                    }else {
                                        if(djpRemainingUsageModel.getOrderCode().equals(djpUniqueTokenModel.getOrderCode())) {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + adultCount);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            break;
                                        }
                                    }
                                }
                            }
                            if (isPassChild) {
                                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                    ProductModel product = djpRemainingUsageModel.getProduct();
                                    if (product instanceof ERPVariantProductModel) {
                                        Optional<ProductFeatureModel> availableFeatureOptional = product.getFeatures().stream().filter(productFeatureModel -> productFeatureModel.getQualifier().contains(DjpCoreConstants.DjpVariantFeature.AVAILABLE_QUALIFER_CODE)).findFirst();
                                        for (ProductFeatureModel productFeature : product.getFeatures()) {
                                            try {
                                                if (productFeature.getValue() instanceof ClassificationAttributeValueModel) {
                                                    ClassificationAttributeValueModel c = (ClassificationAttributeValueModel) productFeature.getValue();
                                                    if (c.getCode().startsWith(DjpCoreConstants.DjpVariantFeature.ZMMDJP2_STARTWITH) && c.getCode().equals(DjpCoreConstants.DjpVariantFeature.CHILD)) {
                                                        if(availableFeatureOptional.isPresent()){
                                                            ProductFeatureModel availableFeature = availableFeatureOptional.get();
                                                            ClassificationAttributeValueModel featureValue = (ClassificationAttributeValueModel) availableFeature.getValue();
                                                            feature = ((ERPVariantProductModel) djpRemainingUsageModel.getProduct()).getBaseProduct().getCode() + featureValue.getCode();
                                                            feature = qrTypeForRemainingHashMap.get(feature);
                                                        }
                                                        boolean checkOrder = StringUtils.isNotBlank(orderCode) ? djpRemainingUsageModel.getOrderCode().equals(orderCode): Boolean.TRUE;
                                                        if(whereUsed.startsWith(feature) && Boolean.TRUE.equals(checkOrder)) {
                                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + childCount);
                                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            } catch (Exception e) {
                                                LOGGER.error("addRemainingAdult has error", e);
                                            }
                                        }
                                    }else {
                                        if(djpRemainingUsageModel.getOrderCode().equals(djpUniqueTokenModel.getOrderCode())) {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + childCount);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            break;
                                        }
                                    }
                                }
                            }else{
                                if(CollectionUtils.isEmpty(Objects.nonNull(qrPassRequest) ? qrPassRequest.getUsages() : null) && StringUtils.isNotBlank(Objects.nonNull(qrPassRequest) ? qrPassRequest.getQrType() :null)) {
                                    for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                        ProductModel product = djpRemainingUsageModel.getProduct();
                                        String usedValue = qrTypeForRemainingHashMap.get(product.getCode());
                                        if (whereUsed.equals(usedValue) && CollectionUtils.isEmpty(product.getFeatures())) {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                            }
                                            break;
                                        }
                                    }
                                }else {
                                    if(CollectionUtils.isNotEmpty(Objects.nonNull(qrPassRequest) ? qrPassRequest.getUsages() : null)){
                                        for (MultiUsagesData usage : qrPassRequest.getUsages()) {
                                            for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                                Boolean isOverSixtyFive = Boolean.TRUE.equals(djpRemainingUsageModel.getForOverSixtyFive());
                                                if(StringUtils.isNotBlank(usage.getProductCode())){
                                                    boolean isShouldBeChangedProductCode = "TEST".equals(HOST_TYPE) &&  productCodeReverseMapForTestEnvironment.containsKey(usage.getProductCode());
                                                    String usageProductCode = Boolean.FALSE.equals(isShouldBeChangedProductCode) ? usage.getProductCode() : productCodeReverseMapForTestEnvironment.get(usage.getProductCode());
                                                    ProductModel product = djpRemainingUsageModel.getProduct();
                                                    if (usageProductCode.equals(product.getCode())
                                                            && djpRemainingUsageModel.getOrderCode().equals(djpUniqueTokenModel.getOrderCode())
                                                            && isOverSixtyFive.equals(djpRemainingUsageModel.getForOverSixtyFive())) {
                                                        djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + Integer.valueOf(usage.getQuantity()));
                                                        modelService.saveOrUpdate(djpRemainingUsageModel);
                                                    }
                                                }else {
                                                    ProductModel product = djpRemainingUsageModel.getProduct();
                                                    String usedValue = qrTypeForRemainingHashMap.get(product.getCode());
                                                    if (usage.getQrType().equals(usedValue)
                                                            && CollectionUtils.isEmpty(product.getFeatures())
                                                            && djpRemainingUsageModel.getOrderCode().equals(djpUniqueTokenModel.getOrderCode())
                                                            && isOverSixtyFive.equals(djpRemainingUsageModel.getForOverSixtyFive())) {
                                                        djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + Integer.valueOf(usage.getQuantity()));
                                                        modelService.saveOrUpdate(djpRemainingUsageModel);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } else {
                        if (Objects.nonNull(boardingTypeCode)) {
                            boolean hasServicePackagedProductOnCustomer = customerUsages.stream().anyMatch(djpRemainingUsageModel -> djpRemainingUsageModel.getPackagedProduct() != null && qrTypeForPackagedProductRemainingHashMap.get(boardingTypeCode).equals(djpRemainingUsageModel.getProduct().getCode())) ;
                            boolean hasServiceProductOnCustomer = customerUsages.stream().anyMatch(djpRemainingUsageModel -> djpRemainingUsageModel.getProduct() != null && qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()).equals(boardingTypeCode)) ;
                            if (hasServicePackagedProductOnCustomer) {
                                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                    if (Objects.nonNull(djpRemainingUsageModel.getPackagedProduct())){
                                        ProductModel product = djpRemainingUsageModel.getProduct();
                                        String usedValue = qrTypeForPackagedProductRemainingHashMap.get(boardingTypeCode);
                                        if (product.getCode().equals(usedValue)) {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }else if (hasServiceProductOnCustomer) {
                                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                    if (Objects.isNull(djpRemainingUsageModel.getPackagedProduct())){
                                        ProductModel product = djpRemainingUsageModel.getProduct();
                                        String usedValue = qrTypeForRemainingHashMap.get(product.getCode());
                                        if (boardingTypeCode.equals(usedValue)) {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                            }
                                            break;
                                        }
                                    }
                                }
                            } else {
                                statusResponse.setType("E");
                                statusResponse.setMessage("Müşterinin Ürünü ya da Kullanım Hakkı Bulunmamaktadır.");
                            }
                        } else {
                            Integer adultCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getAdultCount() : Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getAdultCount() : null;
                            Integer childCount = Objects.nonNull(qrPassRequest) ? qrPassRequest.getChildCount() : Objects.nonNull(boardingPassRequest) ? boardingPassRequest.getChildCount() : null;
                            PremiumPackagePassengerEnum passengerEnum = null;
                            if(Objects.nonNull(adultCount) && Objects.nonNull(childCount)){
                                if(adultCount > 0){
                                    passengerEnum = PremiumPackagePassengerEnum.ADULT;
                                }else {
                                    passengerEnum = PremiumPackagePassengerEnum.CHILD;
                                }
                            }
                            List<DjpRemainingUsageModel> dailyPassPackageRemainings = new ArrayList<>();
                            if(CollectionUtils.isNotEmpty(customerUsages) && Objects.nonNull(djpUniqueTokenModel)) {
                                customerUsages = customerUsages.stream().filter(djpRemainingUsageModel -> djpRemainingUsageModel.getOrderCode().equals(djpUniqueTokenModel.getOrderCode())).collect(Collectors.toList());
                            }
                            boolean haveDailyPass = false;
                            boolean fastTrack = false;
                            if (customerUsages.stream().anyMatch(djpRemainingUsageModel -> Objects.nonNull(djpRemainingUsageModel.getPackagedProduct()))) {
                                haveDailyPass = customerUsages.stream().map(DjpRemainingUsageModel::getPackagedProduct).map(ProductModel::getSupercategories).anyMatch(categoryModels -> categoryModels.stream().anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCoreConstants.PREMIUM_DAILY_PACKAGE_CATEGORY)));
                                fastTrack = customerUsages.stream().map(DjpRemainingUsageModel::getPackagedProduct).map(ProductModel::getSupercategories).anyMatch(categoryModels -> categoryModels.stream().anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCoreConstants.FAST_TRACK_CATEGORY)));
                            }
                            for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                                if (djpRemainingUsageModel.getProduct().getCode().equals(DjpCoreConstants.CAR_PARKING_CODE)) {
                                    boolean isDailyDomestic=false;
                                    if(djpRemainingUsageModel.getPackagedProduct()!=null
                                            && !CollectionUtils.isEmpty(djpRemainingUsageModel.getPackagedProduct().getSupercategories())) {
                                        isDailyDomestic = djpRemainingUsageModel.getPackagedProduct().getSupercategories().stream()
                                                .filter(categoryModel -> categoryModel instanceof CategoryModel)
                                                .anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCommerceservicesConstants.PREMIUM_DAILY_DOMESTIC_CATEGORY));

                                    }
                                    generateUsedCarParking(statusResponse, qrPassRequest.getUsedDay(), qrPassRequest.getUsedHour(), qrPassRequest.getUsedMinute(), djpRemainingUsageModel,usageWithDjpPassCard);

                                    if(Objects.nonNull(djpRemainingUsageModel.getPeriodOfValid())){
                                        if(Boolean.TRUE.equals(isExternalCustomer)){
                                            dailyPassPackageRemainings = djpRemainingUsageService.getDailyPackageProducts(djpRemainingUsageModel.getExternalUser().getCustomerID(), djpRemainingUsageModel.getOrderCode(), djpRemainingUsageModel.getPackagedProduct().getCode(), djpRemainingUsageModel.getExpireDate(),isExternalCustomer);
                                        }else {
                                            dailyPassPackageRemainings = djpRemainingUsageService.getDailyPackageProducts(djpRemainingUsageModel.getUser().getUid(), djpRemainingUsageModel.getOrderCode(), djpRemainingUsageModel.getPackagedProduct().getCode(), djpRemainingUsageModel.getExpireDate(),isExternalCustomer);
                                        }
                                    }
                                    if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                        djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                        modelService.saveOrUpdate(djpUniqueTokenModel);
                                    }
                                    break;
                                } else {
                                    if (haveDailyPass) {
                                        if (Objects.nonNull(djpRemainingUsageModel.getPassengerType()) && Objects.nonNull(passengerEnum)) {
                                            if (djpRemainingUsageModel.getPassengerType().equals(passengerEnum)) {
                                                djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                                modelService.saveOrUpdate(djpRemainingUsageModel);
                                                if (Objects.nonNull(djpRemainingUsageModel.getPeriodOfValid())) {
                                                    if(Boolean.TRUE.equals(isExternalCustomer)){
                                                        dailyPassPackageRemainings = djpRemainingUsageService.getDailyPackageProducts(djpRemainingUsageModel.getExternalUser().getCustomerID(), djpRemainingUsageModel.getOrderCode(), djpRemainingUsageModel.getPackagedProduct().getCode(), djpRemainingUsageModel.getExpireDate(),isExternalCustomer);
                                                    }else {
                                                        dailyPassPackageRemainings = djpRemainingUsageService.getDailyPackageProducts(djpRemainingUsageModel.getUser().getUid(), djpRemainingUsageModel.getOrderCode(), djpRemainingUsageModel.getPackagedProduct().getCode(), djpRemainingUsageModel.getExpireDate(),isExternalCustomer);
                                                    }

                                                }
                                                if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                    djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                    modelService.saveOrUpdate(djpUniqueTokenModel);
                                                }
                                                break;
                                            }
                                        }else {
                                            djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                            if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                                djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                                modelService.saveOrUpdate(djpUniqueTokenModel);
                                            }
                                            break;
                                        }
                                    } else {
                                        djpRemainingUsageModel.setUsed(djpRemainingUsageModel.getUsed() + 1);
                                        modelService.saveOrUpdate(djpRemainingUsageModel);
                                        if (Objects.nonNull(djpUniqueTokenModel) && StringUtils.isEmpty(djpUniqueTokenModel.getOrderCode())) {
                                            djpUniqueTokenModel.setOrderCode(djpRemainingUsageModel.getOrderCode());
                                            modelService.saveOrUpdate(djpUniqueTokenModel);
                                        }
                                        break;
                                    }
                                }
                            }
                            if(!dailyPassPackageRemainings.isEmpty()){

                                Date date = new Date();
                                for (DjpRemainingUsageModel dailyPassPackageRemaining : dailyPassPackageRemainings) {
                                    if(Objects.nonNull(dailyPassPackageRemaining.getPeriodOfValid())) {
                                        Date expireDate;
                                        boolean isCarParking = dailyPassPackageRemaining.getProduct().getCode().equals(DjpCoreConstants.CAR_PARKING_CODE);
                                        if(isCarParking && !Boolean.TRUE.equals(dailyPassPackageRemaining.getCarparkingUsed())){
                                            expireDate = DateUtil.addForPeriodValidToDate(date, dailyPassPackageRemaining.getPeriodOfValid(),Integer.parseInt(DailyDomestic));
                                            dailyPassPackageRemaining.setExpireDate(expireDate);
                                        } else{
                                            expireDate = DateUtil.addForPeriodValidToDate(date, dailyPassPackageRemaining.getPeriodOfValid(),dailyPassPackageRemaining.getnDaysPeriodDayCount());
                                            dailyPassPackageRemaining.setExpireDate(expireDate);
                                        }
                                        if(Objects.nonNull(dailyPassPackageRemaining.getPeriodOfValid())) {
                                            dailyPassPackageRemaining.setExpireDate(expireDate);
                                            dailyPassPackageRemaining.setPeriodOfValid(null);
                                        }
                                        modelService.saveOrUpdate(dailyPassPackageRemaining);
                                    }
                                }
                            }
                        }
                    }
                }


                if (DjpCoreConstants.TOKEN_TYPE_FOR_SOME_BODY.equals(customerUid)) {
                    DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(qrPassRequest.getLocationID());
                    PassengerType passengerTypeEnum = null;
                    Integer passengerCount = 0;
                    if(Objects.nonNull(qrPassRequest.getAdultCount()) && qrPassRequest.getAdultCount() > 0){
                        passengerTypeEnum = PassengerType.ADULT;
                        passengerCount = qrPassRequest.getAdultCount();
                        generateForSomeBodyUsageQr(qrPassRequest, djpUniqueTokenModel, forSomeBodyOrderByToken, customerForSomeBody, djpPassLocation, passengerTypeEnum, passengerCount);
                    }
                    if(Objects.nonNull(qrPassRequest.getChildCount()) && qrPassRequest.getChildCount() > 0){
                        passengerTypeEnum = PassengerType.CHILD;
                        passengerCount = qrPassRequest.getChildCount();
                        generateForSomeBodyUsageQr(qrPassRequest, djpUniqueTokenModel, forSomeBodyOrderByToken, customerForSomeBody, djpPassLocation, passengerTypeEnum, passengerCount);
                    }

                }

            } else {
                djpUniqueTokenModel = null;
                if(!usageWithDjpPassCard) {
                    statusResponse.setType("E");
                    statusResponse.setMessage("Müşterinin Ürünü ya da Kullanım Hakkı Bulunmamaktadır.");
                }
            }
        } catch (Exception e) {
            LOGGER.error("addUsedInDjpRemainingUsage has error:", e);
        }
    }

    @Override
    public List<DjpProductSaleWsData> getUserProducts(String customerUid, String productCategory, String productType, QrForFioriModel qrForFioriModel, QrForMobileModel qrForMobileModel, QrForExternalModel qrForExternalModel, QrForVoucherModel qrForVoucherModel, String whereUsed) {
        Set<QrType> qrTypeEnumSet = new HashSet<>();
        List<DjpRemainingUsageModel> customerUsages = new ArrayList<>();
        List<DjpRemainingUsageModel> customerUsagesForUsedArea = new ArrayList<>();
        List<DjpProductSaleWsData> userProducts=new ArrayList<>();
        if(StringUtils.isNotBlank(customerUid)){
            customerUsages = djpRemainingUsageService.getCustomerUsages(customerUid, productCategory, productType);
        }else {
            if(Objects.nonNull(qrForExternalModel)){
                customerUsages = djpRemainingUsageService.getExternalCustomerUsages(qrForExternalModel.getTokenKey().getUserUid(), productCategory, productType);
            }else {
                if(Objects.nonNull(qrForVoucherModel)){
                    customerUsages = djpRemainingUsageService.getExternalCustomerUsages(qrForVoucherModel.getTokenKey().getUserUid(), productCategory, productType);
                }
            }
        }

        if(!customerUsages.isEmpty()) {
            customerUsages.stream()
                    .forEach(djpRemainingUsageModel -> {
                        if (Objects.nonNull(djpRemainingUsageModel.getPackagedProduct())) {
                            if(checkForDailyDomesticMatch(whereUsed,djpRemainingUsageModel)) {
                                qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()))));
                                customerUsagesForUsedArea.add(djpRemainingUsageModel);
                            }
                        } else {
                            if(djpRemainingUsageModel.getProduct() instanceof ERPVariantProductModel) {
                                if(CollectionUtils.isEmpty(djpRemainingUsageModel.getProduct().getFeatures())){
                                    customerUsagesForUsedArea.add(djpRemainingUsageModel);
                                    qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()))));
                                }else {
                                    for (ProductFeatureModel productFeatureModel : djpRemainingUsageModel.getProduct().getFeatures()) {
                                        try {
                                            if (productFeatureModel.getValue() instanceof ClassificationAttributeValueModel) {
                                                ClassificationAttributeValueModel c = (ClassificationAttributeValueModel) productFeatureModel.getValue();
                                                if (productFeatureModel.getQualifier().contains("zmmdjp_001") || productFeatureModel.getQualifier().contains("zmmdjp_003")) {
                                                    String feature = ((ERPVariantProductModel) djpRemainingUsageModel.getProduct()).getBaseProduct().getCode() + c.getCode();
                                                    String enumValueForfeature = qrTypeForRemainingHashMap.get(feature);
                                                    if(StringUtils.isNotBlank(whereUsed)){
                                                        if (StringUtils.isNotBlank(enumValueForfeature)){
                                                            if(whereUsed.startsWith(enumValueForfeature)){
                                                                qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(enumValueForfeature)));
                                                                if(!customerUsagesForUsedArea.contains(djpRemainingUsageModel)){
                                                                    customerUsagesForUsedArea.add(djpRemainingUsageModel);
                                                                }
                                                            }
                                                        }
                                                    }else {
                                                        qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(enumValueForfeature)));
                                                        customerUsagesForUsedArea.add(djpRemainingUsageModel);
                                                    }
                                                }
                                            }
                                        } catch (Exception e) {
                                            LOGGER.error("error", e);
                                        }
                                    }
                                }
                            }else {

                                if (Objects.nonNull(qrForVoucherModel) && Boolean.TRUE.equals(qrForVoucherModel.getVoucherForDjpExternalCustomer())) {
                                    if(djpRemainingUsageModel.getOrderCode().equals(qrForVoucherModel.getTokenKey().getOrderCode())) {
                                        customerUsagesForUsedArea.add(djpRemainingUsageModel);
                                        qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()))));
                                    }
                                } else {
                                    customerUsagesForUsedArea.add(djpRemainingUsageModel);
                                    qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(qrTypeForRemainingHashMap.get(djpRemainingUsageModel.getProduct().getCode()))));
                                }

                            }
                        }
                    });


            customerUsages = customerUsagesForUsedArea;

            if(!CollectionUtils.isEmpty(customerUsages)) {
                if (Objects.nonNull(qrForFioriModel)) {
                    qrForFioriModel.setWhereIsValidQr(qrTypeEnumSet);
                    modelService.saveOrUpdate(qrForFioriModel);
                }
                if (Objects.nonNull(qrForMobileModel)) {
                    qrForMobileModel.setWhereIsValidQr(qrTypeEnumSet);
                    modelService.saveOrUpdate(qrForMobileModel);
                }
                if (Objects.nonNull(qrForExternalModel)) {
                    qrForExternalModel.setWhereIsValidQr(qrTypeEnumSet);
                    modelService.saveOrUpdate(qrForExternalModel);
                }
                if (Objects.nonNull(qrForVoucherModel)) {
                    qrForVoucherModel.setWhereIsValidQr(qrTypeEnumSet);
                    modelService.saveOrUpdate(qrForVoucherModel);
                    if (Boolean.TRUE.equals(qrForVoucherModel.getVoucherForDjpExternalCustomer())) {
                        customerUsages = customerUsages
                                .stream()
                                .filter(remainingUsageModel -> remainingUsageModel.getOrderCode().equals(qrForVoucherModel.getTokenKey().getOrderCode()))
                                .collect(Collectors.toList());
                    }
                }

                for (DjpRemainingUsageModel djpRemainingUsageModel : customerUsages) {
                    DjpProductSaleWsData djpProductSaleWsData = djpProductSaleWsDataPopulator.populate(djpRemainingUsageModel);
                    userProducts.add(djpProductSaleWsData);
                }
            }

        }


        if(!CollectionUtils.isEmpty(userProducts)){
            userProducts.sort(Comparator.comparing(DjpProductSaleWsData::getPackageName));
        }
        return userProducts;
    }

    @Override
    public void startDjpUsageSendToErpProcess(String whereUsed, ItemModel qr, ItemModel boarding, String boardingType, String sapOrderID, Double paidPrice) {
        djpRemainingUsageService.startDjpUsageSendToErpProcess(whereUsed,qr,boarding,boardingType,sapOrderID,paidPrice);
    }

    @Override
    public void startDjpUsedSendToExternalCompanyProcess(ItemModel qr) {
        djpRemainingUsageService.startDjpUsedSendToExternalCompanyProcess(qr);
    }

    @Override
    public List<DjpProductSaleWsData> getForSomeBodyProducts(String customerUid, String productCategory, QrForVoucherModel qrForVoucherModel, String whereUsed) {
        List<DjpRemainingUsageModel> customerUsagesForUsedArea = new ArrayList<>();
        Set<QrType> qrTypeEnumSet = new HashSet<>();
        List<DjpRemainingUsageModel> customerUsages = djpRemainingUsageService.getCustomerUsages(customerUid, productCategory, null);
        if(Objects.nonNull(qrForVoucherModel)){
            customerUsages = customerUsages
                    .stream()
                    .filter(igaRemainingUsageModel -> Objects.nonNull(igaRemainingUsageModel.getActivationOrUsageCode()))
                    .filter(igaRemainingUsageModel -> Objects.isNull(igaRemainingUsageModel.getPackagedProduct()))
                    .filter(igaRemainingUsageModel -> qrForVoucherModel.getTokenKey().getCode().equals(igaRemainingUsageModel.getActivationOrUsageCode().getCode()))
                    .collect(Collectors.toList());
        }
        if(!customerUsages.isEmpty() && Objects.nonNull(qrForVoucherModel)) {
            customerUsages.stream()
                    .forEach(igaRemainingUsageModel -> {
                        if (Objects.nonNull(igaRemainingUsageModel.getPackagedProduct())) {
                            qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(qrTypeForRemainingHashMap.get(igaRemainingUsageModel.getProduct().getCode()))));
                        } else {
                            for (ProductFeatureModel productFeatureModel : igaRemainingUsageModel.getProduct().getFeatures()) {
                                try {
                                    if (productFeatureModel.getValue() instanceof ClassificationAttributeValueModel) {
                                        ClassificationAttributeValueModel c = (ClassificationAttributeValueModel) productFeatureModel.getValue();
                                        if (productFeatureModel.getQualifier().contains("zmmiga_001")) {
                                            String feature = ((ERPVariantProductModel) igaRemainingUsageModel.getProduct()).getBaseProduct().getCode() + c.getCode();
                                            String enumValueForfeature = qrTypeForRemainingHashMap.get(feature);
                                            if(StringUtils.isNotBlank(whereUsed)){
                                                if(whereUsed.startsWith(enumValueForfeature)){
                                                    qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(enumValueForfeature)));
                                                    customerUsagesForUsedArea.add(igaRemainingUsageModel);
                                                }
                                            }else {
                                                qrTypeEnumSet.add(QrType.valueOf(qrTypeEnumHashMap.get(enumValueForfeature)));
                                                customerUsagesForUsedArea.add(igaRemainingUsageModel);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    LOGGER.error("error", e);
                                }
                            }
                        }
                    });
            customerUsages = customerUsagesForUsedArea;
            if(Objects.nonNull(qrForVoucherModel)){
                if(qrForVoucherModel.getWhereIsValidQr().isEmpty()){
                    qrForVoucherModel.setWhereIsValidQr(qrTypeEnumSet);
                }else {
                    Set<QrType> whereIsValidQr = new HashSet<>();
                    whereIsValidQr.addAll(qrForVoucherModel.getWhereIsValidQr());
                    whereIsValidQr.addAll(qrTypeEnumSet);
                    qrForVoucherModel.setWhereIsValidQr(whereIsValidQr);
                }
                modelService.saveOrUpdate(qrForVoucherModel);
            }

        }
        List<DjpProductSaleWsData> userProducts = fillForSomeBodyProduct(customerUsages);
        return userProducts;
    }

    private List<DjpProductSaleWsData> fillForSomeBodyProduct(List<DjpRemainingUsageModel> customerUsages) {
        List<DjpProductSaleWsData> userProducts = new ArrayList<>();
        for (DjpRemainingUsageModel djpRemainingUsage : customerUsages){
            DjpProductSaleWsData djpProductSaleWsData = djpProductSaleWsDataPopulator.populate(djpRemainingUsage);
            userProducts.add(djpProductSaleWsData);
        }
        if(!userProducts.isEmpty()) {
            DjpProductSaleWsData productSaleWsData = new DjpProductSaleWsData();
            Integer adultGuestCount = null;
            Integer childGuestCount = null;
            productSaleWsData.setOrderCode(userProducts.get(0).getOrderCode());
            productSaleWsData.setSaleUrl(userProducts.get(0).getSaleUrl());
            productSaleWsData.setDescription(userProducts.get(0).getDescription());
            productSaleWsData.setCode(userProducts.get(0).getCode());
            productSaleWsData.setPictureUrl(userProducts.get(0).getPictureUrl());
            productSaleWsData.setPackageName(userProducts.get(0).getPackageName());
            productSaleWsData.setPlateNumbers(userProducts.get(0).getPlateNumbers());
            productSaleWsData.setPackageCode(userProducts.get(0).getPackageCode());
            for (DjpProductSaleWsData userProduct : userProducts) {
                if (Objects.nonNull(userProduct.getAdultGuestCount())) {
                    adultGuestCount = userProduct.getAdultGuestCount() + 1;
                }
                if (Objects.nonNull(userProduct.getChildGuestCount())) {
                    childGuestCount = userProduct.getChildGuestCount() + 1;
                }
            }
            if (Objects.nonNull(adultGuestCount)) {
                if(adultGuestCount == 0){
                    productSaleWsData.setIsAdult(Boolean.FALSE);
                }else {
                    productSaleWsData.setIsAdult(Boolean.TRUE);
                }
            } else {
                productSaleWsData.setIsAdult(Boolean.FALSE);
            }

            if(Boolean.TRUE.equals(productSaleWsData.getIsAdult())){

                if(Objects.nonNull(adultGuestCount)){
                    adultGuestCount = adultGuestCount - 1;
                }
                productSaleWsData.setAdultGuestCount(adultGuestCount);
                productSaleWsData.setChildGuestCount(childGuestCount);
            }else {
                if(Objects.nonNull(childGuestCount)){
                    childGuestCount = childGuestCount - 1;
                }
                productSaleWsData.setAdultGuestCount(adultGuestCount);
                productSaleWsData.setChildGuestCount(childGuestCount);
            }
            productSaleWsData.setIsGift(Boolean.TRUE);
            userProducts.clear();
            userProducts.add(productSaleWsData);
        }
        return userProducts;

    }

    private boolean checkForDailyDomesticMatch(String usageArea, DjpRemainingUsageModel djpRemainingUsageModel) {
        boolean isDailyDomestic=false;
        if(djpRemainingUsageModel.getPackagedProduct()!=null
                && !CollectionUtils.isEmpty(djpRemainingUsageModel.getPackagedProduct().getSupercategories())) {
            isDailyDomestic = djpRemainingUsageModel.getPackagedProduct().getSupercategories().stream()
                    .filter(categoryModel -> categoryModel instanceof CategoryModel)
                    .anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCommerceservicesConstants.PREMIUM_DAILY_DOMESTIC_CATEGORY));

        }
        if(!isDailyDomestic){
            return true;
        }

        return domesticPackageEnumHashMap.containsKey(qrTypeEnumHashMap.get(usageArea));
    }

    private void generateUsedCarParking(FioriStatusResponse statusResponse, Integer usedDay, Integer usedHour, Integer usedMinute, DjpRemainingUsageModel djpRemainingUsageModel,boolean usageWithDjpPassCard) {
        if(Objects.nonNull(usedDay) && Objects.nonNull(usedHour) && Objects.nonNull(usedMinute)) {
            Integer resultForUsedCarParking = djpRemainingUsageModel.getUsed();
            Integer temporaryUsedDay = usedDay;
            Integer temporaryUsedHour = usedHour;
            if (!usedMinute.equals(0)) {
                temporaryUsedHour = temporaryUsedHour + 1;
            }
            if (!temporaryUsedHour.equals(0)) {
                temporaryUsedDay = temporaryUsedDay + 1;
            }
            resultForUsedCarParking = resultForUsedCarParking + temporaryUsedDay;
            if(!usageWithDjpPassCard) {
                Integer currentClaimedOnRemaining = djpRemainingUsageModel.getClaimed() - djpRemainingUsageModel.getUsed();
                if (temporaryUsedDay > currentClaimedOnRemaining) {
                    statusResponse.setType("E");
                    statusResponse.setMessage("Müşterinin '" + currentClaimedOnRemaining + "' gün " + djpRemainingUsageModel.getProduct().getName() + " hakkı bulunmaktadır.'" + currentClaimedOnRemaining + "' gün veya daha az kullanım yaptırabilirsiniz.");
                }
            }
            boolean hasErrorOnResponse = Objects.nonNull(statusResponse.getType()) && statusResponse.getType().equals("E");
            if (!hasErrorOnResponse) {
                djpRemainingUsageModel.setUsed(resultForUsedCarParking);
                modelService.saveOrUpdate(djpRemainingUsageModel);
            }
        }
    }
    private void generateForSomeBodyUsageQr(QrPassRequest qrPassRequest, DjpUniqueTokenModel djpUniqueTokenModel, OrderModel forSomeBodyOrderByToken, CustomerModel customerForSomeBody, DjpPassLocationsModel djpPassLocation, PassengerType passengerType, Integer passengerCount) {
        QrForFioriModel someBodyUsaqeQr = new QrForFioriModel();
        someBodyUsaqeQr.setCustomer(Objects.nonNull(customerForSomeBody) ? customerForSomeBody : (CustomerModel) forSomeBodyOrderByToken.getUser());
        someBodyUsaqeQr.setTokenKey(djpUniqueTokenModel);
        Set<QrType> qrTypeEnumSet = new HashSet<>();
        QrType whereUsed = QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType()));
        qrTypeEnumSet.add(whereUsed);
        someBodyUsaqeQr.setWhereIsValidQr(qrTypeEnumSet);
        someBodyUsaqeQr.setUsageDate(new Date());
        someBodyUsaqeQr.setWhereUsed(whereUsed);
        someBodyUsaqeQr.setPassengerType(passengerType);
        someBodyUsaqeQr.setPassengerCount(passengerCount);
        someBodyUsaqeQr.setWhichLocation(djpPassLocation);
        someBodyUsaqeQr.setForSomeBodyElseUsage(Boolean.TRUE);
        modelService.saveOrUpdate(someBodyUsaqeQr);
    }

    private void addDailyDomesticConditions(List<DjpRemainingUsageModel> customerUsages, DjpRemainingUsageModel djpRemainingUsageModel) {
        if(djpRemainingUsageModel.getPackagedProduct()!=null
                && djpRemainingUsageModel.getOrderCode()!=null
                && !CollectionUtils.isEmpty(djpRemainingUsageModel.getPackagedProduct().getSupercategories())) {

            boolean isDailyDomestic = djpRemainingUsageModel.getPackagedProduct().getSupercategories().stream()
                    .filter(categoryModel -> categoryModel instanceof CategoryModel)
                    .anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCommerceservicesConstants.PREMIUM_DAILY_DOMESTIC_CATEGORY));
            if(isDailyDomestic){
                doAddDailyDomesticConditions(customerUsages);
            }
        }
    }
    private void doAddDailyDomesticConditions(List<DjpRemainingUsageModel> dailyPackages) {
        Date date = new Date();
        boolean isUsedOfAny=dailyPackages.stream().anyMatch(djpRemainingUsageModel -> djpRemainingUsageModel.getUsed()>0);
        if(!isUsedOfAny) {
            for (DjpRemainingUsageModel djpRemainingUsageModel : dailyPackages) {
                if (Objects.nonNull(djpRemainingUsageModel.getPeriodOfValid())) {
                    Date expireDate;
                    boolean isCarParking = djpRemainingUsageModel.getProduct().getCode().equals(DjpCoreConstants.CAR_PARKING_CODE);
                    if (isCarParking && !Boolean.TRUE.equals(djpRemainingUsageModel.getCarparkingUsed())) {
                        expireDate = DateUtil.addForPeriodValidToDate(date, djpRemainingUsageModel.getPeriodOfValid(), Integer.parseInt(DailyDomestic));
                        djpRemainingUsageModel.setExpireDate(expireDate);
                    } else {
                        expireDate = DateUtil.addForPeriodValidToDate(date, djpRemainingUsageModel.getPeriodOfValid(), djpRemainingUsageModel.getnDaysPeriodDayCount());
                        djpRemainingUsageModel.setExpireDate(expireDate);
                    }

                    djpRemainingUsageModel.setPeriodOfValid(null);

                    modelService.saveOrUpdate(djpRemainingUsageModel);
                }
            }
        }
    }
}
