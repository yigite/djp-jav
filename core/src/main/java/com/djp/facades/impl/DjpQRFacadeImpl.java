package com.djp.facades.impl;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.data.DjpProductSaleWsData;
import com.djp.core.enums.PremiumPackagePassengerEnum;
import com.djp.data.MultiUsagesData;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.core.model.QrForVoucherModel;
import com.djp.facades.DjpQRFacade;
import com.djp.facades.DjpRemainingUsageFacade;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.request.QrPassRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.service.*;
import com.djp.strategy.DjpPremiumServiceUsageStrategy;
import com.djp.strategy.DjpQrTypeValidateStrategy;
import com.djp.util.DjpUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;
import com.djp.populator.Populator;
import com.djp.response.CheckCustomerOwnedServiceResponse;
import com.djp.response.QrCodeValidResponse;
import com.djp.service.DjpQRService;
import com.djp.service.DjpRemainingUsageService;
import com.djp.util.DjpUtil;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.stream.Collectors;

public class DjpQRFacadeImpl implements DjpQRFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpQRFacadeImpl.class);
    public static final String TOKEN_TYPE_LOGIN = "QR_LOGIN";
    private static final String EXCEPTION_OCCURED="Sistemsel bir hata olustu.";
    private static final String EXTERNAL_NUEVO = "nuevo";

    @Resource
    private MessageSource messageSource;

    @Resource
    private DjpQRService djpQRService;

    @Resource
    private DjpRemainingUsageService djpRemainingUsageService;

    @Resource
    private Populator<DjpRemainingUsageModel, DjpProductSaleWsData> djpProductSaleWsDataPopulator;

    @Resource
    private ModelService modelService;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private DjpBoardingService djpBoardingService;

    @Resource
    private DjpRemainingUsageFacade djpRemainingUsageFacade;

    @Resource
    private HashMap<String, DjpQrTypeValidateStrategy> djpQrTypeValidateStrategyHashMap;

    @Resource
    private DjpUtils djpUtils;

    @Resource
    private DjpQRFacade djpQRFacade;

    @Resource
    private DjpPremiumServiceUsageStrategy djpPremiumServiceUsageStrategy;

    @Resource
    private DjpExternalCompanyService djpExternalCompanyService;

    @Resource
    private List<String> stockedServiceUsagePoint;

    @Value("${fiori.host.type}")
    private String HOST_TYPE;

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    private DjpCustomerService djpCustomerService;

    @Resource
    private UserService userService;

    @Override
    public FioriStatusResponse qrPass(QrPassRequest qrPassRequest, final HttpServletRequest request) throws JsonProcessingException, ParseException {
        String clientId = request.getRemoteUser();
        LOGGER.info(clientId+"-qrPassRequest:"+generateLog(getJsonFromRequst(qrPassRequest)));
        FioriStatusResponse status = new FioriStatusResponse();
        Date date = new Date();
        DjpPassLocationsModel djpPassLocation =null;
        boolean isCarParking = DjpCommerceservicesConstants.CARPARKING_CODE.equals(qrPassRequest.getQrType());
        if (Boolean.FALSE.equals(isCarParking)) {
            djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(qrPassRequest.getLocationID());
        } else {
            djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(DjpCoreConstants.DjpPassLocations.DJP);
        }
        if(Objects.isNull(djpPassLocation)){
            status.setType("E");
            status.setMessage("HAVALIMANININ DJP PASS SERVISLERINE KULLANIMI YOKTUR");
            return status;
        }

        QrForCardModel qrForCardModel = djpQRService.getQRForCardByTokenKey(qrPassRequest.getTokenKey());
        QrForBankModel qrForBankModel = null;
        QrForVoucherModel qrForVoucherModel = null;
        QrForFioriModel qrForFioriModel = null;
        QrForMobileModel qrForMobileModel = null;
        QrForMeetModel qrForMeetModel = null;
        QrForExperienceCenterModel qrForExperienceCenterModel = null;
        if (Objects.nonNull(qrForCardModel)) {
            if (Objects.nonNull(qrForCardModel.getExpireDate()) && djpUtils.dateCompare(qrForCardModel.getExpireDate(), date) < 0) {
                status.setType("E");
                status.setMessage("Kartınızı Geçerlilik Süresi Dolmuştur");
                return status;
            } else {

                String validateResult = djpQrTypeValidateStrategyHashMap.get("QrForCardModel").validateWhereUsedQr(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())),qrPassRequest.getTokenKey());
                if (!"OK".equals(validateResult)){
                    status.setType("E");
                    status.setMessage(validateResult);
                    djpQRService.saveUnapprovedPass(qrForCardModel.getCardKey(),validateResult,qrTypeEnumHashMap.get(qrPassRequest.getQrType()),"QrForCardModel");
                    return status;
                }
                CardsUsedInServicesModel cardsUsedInServicesModel = new CardsUsedInServicesModel();
                djpQRService.saveCardsUsedInServices(cardsUsedInServicesModel,qrPassRequest.getQrType(),status,qrForCardModel,djpPassLocation,qrPassRequest.getPersonalID(),qrPassRequest.getPersonalName());
                if (status.getType().equals("S")) {
                    if(Objects.isNull(qrForCardModel.getStartDate()) && Objects.isNull(qrForCardModel.getExpireDate())) {
                        qrForCardModel.setStartDate(date);
                        Calendar expire = DateUtil.addOneYearToOrderDateAndSubOneDay(date);
                        qrForCardModel.setExpireDate(expire.getTime());
                        modelService.saveOrUpdate(qrForCardModel);
                    }
                    if(Objects.nonNull(cardsUsedInServicesModel.getWhereUsed()) && cardsUsedInServicesModel.getWhereUsed().equals(QrType.CAR_PARKING)){
                        String carParkingUsedInfo = qrPassRequest.getUsedDay()+" day/"+qrPassRequest.getUsedHour()+" hour/"+qrPassRequest.getUsedMinute()+" minute";
                        cardsUsedInServicesModel.setCarParkingUsedInfo(carParkingUsedInfo);
                        cardsUsedInServicesModel.setUsedVale(qrPassRequest.getUsedVale());
                        cardsUsedInServicesModel.setPlateNumber(qrPassRequest.getPlateNumber());
                        if(cardsUsedInServicesModel.getToken() != null){
                            EmployeeModel employeeModel = null;
                            try {
                                employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                cardsUsedInServicesModel.getToken().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                cardsUsedInServicesModel.getToken().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                            } catch (Exception e){
                                LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                            }
                            modelService.saveOrUpdate(cardsUsedInServicesModel.getToken());
                        }
                        modelService.saveOrUpdate(cardsUsedInServicesModel);
                    }
                    if(StringUtils.isNotBlank(qrForCardModel.getPhoneNumber())){
                        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,qrForCardModel.getPhoneNumber(),qrPassRequest.getProductCategory(),null,qrPassRequest,true,null, null);
                    }
                }
                return status;
            }
        }else {
            qrForBankModel = djpQRService.validQrCodeForBank(qrPassRequest.getTokenKey());
            if (Objects.nonNull(qrForBankModel)) {
                if (Objects.nonNull(qrForBankModel.getWhereUsed())) {
                    status.setMessage("Qr'ın Kullanım Hakkı Kalmamıştır.");
                    status.setType("E");
                    return status;
                }
                qrForBankModel.setUsageDate(date);
                qrForBankModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                qrForBankModel.setWhichLocation(djpPassLocation);
                if(qrForBankModel.getTokenKey() != null){
                    EmployeeModel employeeModel = null;
                    try {
                        employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                        qrForBankModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                        qrForBankModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                    } catch (Exception e){
                        LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                    }
                    modelService.saveOrUpdate(qrForBankModel.getTokenKey());
                }
                modelService.saveOrUpdate(qrForBankModel);

                // send qr to sap
                DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getDjpBoardingPassByToken(qrForBankModel.getTokenKey().getCode());
                if (Objects.nonNull(djpBoardingPassModel)) {
                    if (StringUtils.isBlank(djpBoardingPassModel.getContractID())) {
                        djpBoardingPassModel.setContractID(qrForBankModel.getContractId());
                        modelService.saveOrUpdate(djpBoardingPassModel);
                    }
                }
                // call company send process start
                djpRemainingUsageFacade.startDjpUsedSendToExternalCompanyProcess(qrForBankModel);

                // call erp send process start
                djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrPassRequest.getQrType(), qrForBankModel, djpBoardingPassModel, null,null,null);

                status.setMessage("Hizmet Kullandırıldı.");
                status.setType("S");
                return status;
            } else {
                qrForVoucherModel = djpQRService.validQrCodeForDjp(qrPassRequest.getTokenKey());
                if (Objects.nonNull(qrForVoucherModel)) {
                    if (Boolean.TRUE.equals(qrForVoucherModel.getVoucherForRemainingUsage())) {
                        return djpPremiumServiceUsageStrategy.passForRemainingUsageWithVoucher(qrPassRequest, status, date, djpPassLocation, qrForVoucherModel);
                    } else if (Boolean.TRUE.equals(qrForVoucherModel.getVoucherForDjpExternalCustomer())) {
                        return djpPremiumServiceUsageStrategy.passForExternalCustomerWithVoucher(qrPassRequest, status, date, djpPassLocation, qrForVoucherModel);
                    }else if (Boolean.TRUE.equals(qrForVoucherModel.getIsCustomSingleCampaignCode())) {
                        return djpPremiumServiceUsageStrategy.passForSingleCampaignCodeVoucher(qrPassRequest, status, date, djpPassLocation, qrForVoucherModel);
                    } else {
                        if (CollectionUtils.isNotEmpty(qrForVoucherModel.getRemainingForPackage())) {
                            return djpQRFacade.usePackageWithVoucher(qrForVoucherModel,qrPassRequest,date,djpPassLocation);
                        } else {
                            boolean isStockedServiceUsaged = StringUtils.isNotBlank(qrPassRequest.getQrType()) && stockedServiceUsagePoint.contains(qrPassRequest.getQrType());
                            if (qrForVoucherModel.getClaimed().equals(qrForVoucherModel.getUsed())) {
                                status.setMessage("Voucher'ın Kullanım Hakkı Kalmamıştır.");
                                status.setType("E");
                                return status;
                            }
                            // check order and paid price , before the set djp campaign code usages
                            if(Boolean.TRUE.equals(qrForVoucherModel.getPromotionVoucher())){
                                status = djpQRFacade.fillDjpVoucherCodeUsagesAndStatus(qrForVoucherModel,qrPassRequest,date,djpPassLocation,null);
                                if("E".equals(status.getType())){
                                    return status;
                                }
                            }else {
                                djpQRFacade.saveCodeUsage(qrForVoucherModel,qrPassRequest,date,djpPassLocation,null,0D);
                            }
                            Set<QrType> whereUsed = new HashSet<>();
                            if (!qrForVoucherModel.getWhereUsed().isEmpty()) {
                                whereUsed.addAll(qrForVoucherModel.getWhereUsed());
                            }
                            qrForVoucherModel.setUsageDate(date);
                            whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                            qrForVoucherModel.setWhereUsed(whereUsed);
                            qrForVoucherModel.setUsed(qrForVoucherModel.getUsed() + 1);
                            qrForVoucherModel.setWhichLocation(djpPassLocation);
                            qrForVoucherModel.setSapOrderID(StringUtils.isNotBlank(qrPassRequest.getSapOrderID()) ? qrPassRequest.getSapOrderID() : null);
                            qrForVoucherModel.setEntryNumberOnSapOrder(StringUtils.isNotBlank(qrPassRequest.getEntryNumber()) ? qrPassRequest.getEntryNumber() : null);
                            qrForVoucherModel.setUsageForOverSixtyFiveOld(Boolean.TRUE.equals(qrPassRequest.getIsOverSixtyFive()) ? Boolean.TRUE : Boolean.FALSE);
                            if(StringUtils.isNotBlank(qrPassRequest.getOrderCode())){
                                DjpUniqueTokenModel tokenKey = qrForVoucherModel.getTokenKey();
                                tokenKey.setOrderCode(qrPassRequest.getOrderCode());
                                modelService.saveOrUpdate(tokenKey);
                            }
                            if(qrForVoucherModel.getTokenKey() != null){
                                EmployeeModel employeeModel = null;
                                try {
                                    employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                    qrForVoucherModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                    qrForVoucherModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                                } catch (Exception e){
                                    LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                                }
                                modelService.saveOrUpdate(qrForVoucherModel.getTokenKey());
                            }
                            modelService.saveOrUpdate(qrForVoucherModel);

                            djpQRFacade.startDjpUsageSendToErpProcess(qrPassRequest,qrForVoucherModel,isStockedServiceUsaged);

                            status.setMessage("Hizmet Kullandırıldı.");
                            status.setType("S");
                            return status;
                        }
                    }
                } else {
                    qrForFioriModel = djpQRService.validQrCodeForFiori(qrPassRequest.getTokenKey());
                    if (Objects.nonNull(qrForFioriModel)) {

                        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,qrForFioriModel.getTokenKey().getUserUid(), qrPassRequest.getProductCategory(),null,qrPassRequest,false,null, null);
                        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
                        if (!hasErrorOnResponse) {
                            PassengerType passengerTypeEnum = null;
                            Integer passengerCount = 0;
                            if(Objects.nonNull(qrPassRequest.getAdultCount()) && qrPassRequest.getAdultCount() > 0){
                                passengerTypeEnum = PassengerType.ADULT;
                                passengerCount = qrPassRequest.getAdultCount();
                            }else{
                                passengerTypeEnum = PassengerType.CHILD;
                                passengerCount = qrPassRequest.getChildCount();
                            }
                            qrForFioriModel.setUsageDate(date);
                            qrForFioriModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                            qrForFioriModel.setPlateNumber(qrPassRequest.getPlateNumber());
                            if(Objects.nonNull(qrForFioriModel.getWhereUsed()) && qrForFioriModel.getWhereUsed().equals(QrType.CAR_PARKING)){
                                String carParkingUsedInfo = qrPassRequest.getUsedDay()+" day/"+qrPassRequest.getUsedHour()+" hour/"+qrPassRequest.getUsedMinute()+" minute";
                                qrForFioriModel.setCarParkingUsedInfo(carParkingUsedInfo);
                                qrForFioriModel.setUsedVale(qrPassRequest.getUsedVale());
                            }
                            qrForFioriModel.setPassengerType(passengerTypeEnum);
                            qrForFioriModel.setPassengerCount(passengerCount);
                            qrForFioriModel.setWhichLocation(djpPassLocation);
                            if(qrForFioriModel.getTokenKey() != null){
                                EmployeeModel employeeModel = null;
                                try {
                                    employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                    qrForFioriModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                    qrForFioriModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                                } catch (Exception e){
                                    LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                                }
                                modelService.saveOrUpdate(qrForFioriModel.getTokenKey());
                            }
                            modelService.saveOrUpdate(qrForFioriModel);
                            status.setMessage("Hizmet Kullandırıldı.");
                            status.setType("S");
                        }
                        return status;
                    } else {
                        qrForMobileModel = djpQRService.validQrCodeForMobile(qrPassRequest.getTokenKey());
                        if (Objects.nonNull(qrForMobileModel)) {
                            PassengerType passengerTypeEnum = null;
                            Integer passengerCount = 0;
                            if(Objects.nonNull(qrPassRequest.getAdultCount()) && qrPassRequest.getAdultCount() > 0){
                                passengerTypeEnum = PassengerType.ADULT;
                                passengerCount = qrPassRequest.getAdultCount();
                            }else {
                                if (Objects.nonNull(qrPassRequest.getChildCount()) && qrPassRequest.getChildCount() > 0) {
                                    passengerTypeEnum = PassengerType.CHILD;
                                    passengerCount = qrPassRequest.getChildCount();
                                }
                            }
                            CustomerModel customerByUid = djpCustomerService.findCustomerByUid(qrForMobileModel.getTokenKey().getUserUid());
                            if(Objects.isNull(customerByUid)){
                                status.setMessage("QR'a ait kullanıcı DJP PASS de bulunamadı. tokenKey:" + qrPassRequest.getTokenKey());
                                status.setType("E");
                                return status;
                            }
                            if(Objects.nonNull(qrForMobileModel.getWhereUsed())){
                                status.setMessage("Qr Daha Önce Kullanılmış!");
                                status.setType("E");
                                return status;
                            }

                            djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,qrForMobileModel.getTokenKey().getUserUid(), qrPassRequest.getProductCategory(),null,qrPassRequest,false,null, null);
                            boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
                            if (!hasErrorOnResponse) {
                                qrForMobileModel.setUsageDate(date);
                                qrForMobileModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                                qrForMobileModel.setPlateNumber(qrPassRequest.getPlateNumber());
                                if(Objects.nonNull(qrForMobileModel.getWhereUsed()) && qrForMobileModel.getWhereUsed().equals(QrType.CAR_PARKING)){
                                    String carParkingUsedInfo = qrPassRequest.getUsedDay()+" day/"+qrPassRequest.getUsedHour()+" hour/"+qrPassRequest.getUsedMinute()+" minute";
                                    qrForMobileModel.setCarParkingUsedInfo(carParkingUsedInfo);
                                    qrForMobileModel.setUsedVale(qrPassRequest.getUsedVale());
                                }
                                qrForMobileModel.setPassengerType(passengerTypeEnum);
                                qrForMobileModel.setPassengerCount(passengerCount);
                                qrForMobileModel.setWhichLocation(djpPassLocation);
                                if(qrForMobileModel.getTokenKey() != null){
                                    EmployeeModel employeeModel = null;
                                    try {
                                        employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                        qrForMobileModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                        qrForMobileModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                                    } catch (Exception e){
                                        LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                                    }
                                    modelService.saveOrUpdate(qrForMobileModel.getTokenKey());
                                }
                                modelService.saveOrUpdate(qrForMobileModel);
                                status.setMessage("Hizmet Kullandırıldı.");
                                status.setType("S");
                            }
                            return status;
                        } else {
                            QrForExternalModel qrForExternalModel = djpQRService.getQRForExternalByTokenKey(qrPassRequest.getTokenKey());
                            if (Objects.nonNull(qrForExternalModel)) {
                                DjpExternalCustomerModel customerByUid = djpExternalCompanyService.findDjpExternalCustomerByID(qrForExternalModel.getTokenKey().getUserUid(),qrForExternalModel.getCompanyClientId());
                                if(Objects.isNull(customerByUid)){
                                    status.setMessage("QR'a ait kullanıcı DJP PASS de bulunamadı. tokenKey:" + qrPassRequest.getTokenKey());
                                    status.setType("E");
                                    return status;
                                }
                                if(Objects.nonNull(qrForExternalModel.getWhereUsed()) && !QrType.FAST_TRACK.getCode().equals(qrTypeEnumHashMap.get(qrPassRequest.getQrType().substring(0,2)))){
                                    status.setMessage("Qr Daha Önce Kullanılmış!");
                                    status.setType("E");
                                    return status;
                                }
                                if(dateValidationForNuevo(qrForExternalModel, qrPassRequest.getQrType())){
                                    status.setMessage("Qr Kullanım Tarihlerini Kontrol Ediniz! Başlangıç Tarihi: " + DateUtil.dateToString(qrForExternalModel.getStartDate())
                                            + " Bitiş Tarihi: " + DateUtil.dateToString(qrForExternalModel.getEndDate()));
                                    status.setType("E");
                                    return status;
                                }

                                djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,qrForExternalModel.getTokenKey().getUserUid(), qrPassRequest.getProductCategory(),null,qrPassRequest,false,qrForExternalModel, null);
                                boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
                                if (!hasErrorOnResponse) {
                                    qrForExternalModel.setUsageDate(date);
                                    qrForExternalModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                                    qrForExternalModel.setAdultCount(qrPassRequest.getAdultCount());
                                    qrForExternalModel.setChildCount(qrPassRequest.getChildCount());
                                    if(qrForExternalModel.getTokenKey() != null){
                                        EmployeeModel employeeModel = null;
                                        try {
                                            employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                            qrForExternalModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                            qrForExternalModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                                        } catch (Exception e){
                                            LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                                        }
                                        modelService.saveOrUpdate(qrForExternalModel.getTokenKey());
                                    }
                                    modelService.saveOrUpdate(qrForExternalModel);
                                    if(QrType.FAST_TRACK.getCode().equals(qrTypeEnumHashMap.get(qrPassRequest.getQrType().substring(0,2)))){
                                        List<DjpExternalCompanyUsagesModel> djpExternalCompanyUsagesModelList = djpQRService.getQrExternalCompanyUsagesList(qrPassRequest.getTokenKey());
                                        DjpExternalCompanyUsagesModel djpExternalCompanyUsagesModel = new DjpExternalCompanyUsagesModel();
                                        DjpUniqueTokenModel djpUniqueTokenModel = qrForExternalModel.getTokenKey();
                                        if (djpExternalCompanyUsagesModelList.isEmpty()){
                                            Calendar cal = Calendar.getInstance();
                                            cal.setTime(date);
                                            cal.add(Calendar.DATE, 1);
                                            djpUniqueTokenModel.setExpireDate(cal.getTime());
                                            DjpRemainingUsageModel usageModel = djpRemainingUsageService.getDjpRemainingByExternal(djpUniqueTokenModel.getUserUid(), djpUniqueTokenModel.getOrderCode());
                                            if (Objects.nonNull(usageModel)){
                                                if (usageModel.getUsed() == 1){
                                                    usageModel.setUsed(0);
                                                }
                                                modelService.saveOrUpdate(usageModel);
                                            }
                                            djpExternalCompanyUsagesModel.setBilledQuantity("1");
                                        } else if(djpExternalCompanyUsagesModelList.size() == 1){
                                            DjpExternalCompanyUsagesModel oldDjpExternalCompanyUsagesModel = djpExternalCompanyUsagesModelList.get(0);
                                            oldDjpExternalCompanyUsagesModel.setBilledQuantity("0.5");
                                            modelService.saveOrUpdate(oldDjpExternalCompanyUsagesModel);
                                            djpExternalCompanyUsagesModel.setBilledQuantity("0.5");
                                        }
                                        djpExternalCompanyUsagesModel.setTokenKey(qrForExternalModel.getTokenKey());
                                        djpExternalCompanyUsagesModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                                        modelService.saveOrUpdate(djpExternalCompanyUsagesModel);
                                        modelService.saveOrUpdate(djpUniqueTokenModel);
                                    }
                                    status.setMessage("Hizmet Kullandırıldı.");
                                    status.setType("S");
                                    // call company send start
                                    djpRemainingUsageFacade.startDjpUsedSendToExternalCompanyProcess(qrForExternalModel);

                                    DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getDjpBoardingPassByToken(qrForExternalModel.getTokenKey().getCode());
                                    if (Objects.nonNull(djpBoardingPassModel)) {
                                        if (StringUtils.isBlank(djpBoardingPassModel.getContractID())) {
                                            ContractedCompaniesModel contractedCompany = djpQRService.getContractedCompany(qrForExternalModel.getCompanyClientId(), qrPassRequest.getQrType(),djpPassLocation);
                                            qrForExternalModel.setContractId(contractedCompany.getContractId());
                                            modelService.saveOrUpdate(qrForExternalModel);
                                            djpBoardingPassModel.setContractID(qrForExternalModel.getContractId());
                                            modelService.saveOrUpdate(djpBoardingPassModel);
                                        }
                                    }else {
                                        if (StringUtils.isBlank(qrForExternalModel.getContractId())) {
                                            ContractedCompaniesModel contractedCompany = djpQRService.getContractedCompany(qrForExternalModel.getCompanyClientId(), qrPassRequest.getQrType(),djpPassLocation);
                                            qrForExternalModel.setContractId(contractedCompany.getContractId());
                                            modelService.saveOrUpdate(qrForExternalModel);
                                        }
                                    }
                                    djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrPassRequest.getQrType(), qrForExternalModel, djpBoardingPassModel, null,null,null);
                                }
                                return status;
                            } else {
                                qrForMeetModel = djpQRService.getQRForMeetByTokenKey(qrPassRequest.getTokenKey());
                                if(Objects.nonNull(qrForMeetModel)){
                                    if(Objects.isNull(qrForMeetModel.getWhereUsed()) && Objects.isNull(qrForMeetModel.getUsageDate())) {
                                        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
                                        if (!hasErrorOnResponse) {
                                            qrForMeetModel.setUsageDate(date);
                                            qrForMeetModel.setWhereUsed(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                                            if(qrForMeetModel.getTokenKey() != null){
                                                EmployeeModel employeeModel = null;
                                                try {
                                                    employeeModel = (EmployeeModel) userService.getUserForUID(qrPassRequest.getPersonalID());
                                                    qrForMeetModel.getTokenKey().setPersonalID(StringUtils.isNotBlank(qrPassRequest.getPersonalID()) ? qrPassRequest.getPersonalID() : null);
                                                    qrForMeetModel.getTokenKey().setPersonalName(employeeModel != null ? employeeModel.getName() : null);
                                                } catch (Exception e){
                                                    LOGGER.error("Cannot find user with uid : " + qrPassRequest.getPersonalID());
                                                }
                                                modelService.saveOrUpdate(qrForMeetModel.getTokenKey());
                                            }
                                            modelService.saveOrUpdate(qrForMeetModel);
                                            status.setMessage("Hizmet Kullandırıldı.");
                                            status.setType("S");
                                        }
                                        return status;
                                    }
                                    else {
                                        status.setMessage("Qr Daha Önce Kullanılmış!");
                                        status.setType("E");
                                        return status;
                                    }
                                }
                                else {
                                    qrForExperienceCenterModel = djpQRService.getQrForExperienceCenter(qrPassRequest.getTokenKey());
                                    if(Objects.nonNull(qrForExperienceCenterModel)){
                                        if (qrForExperienceCenterModel.getTokenKey().getExpireDate().after(new Date())){
                                            if (qrForExperienceCenterModel.getIsPaymentSuccess()){
                                                QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType()));
                                                List<DjpRemainingUsageModel> usageList = qrForExperienceCenterModel.getRemainingUsageList();
                                                for (DjpRemainingUsageModel djpRemainingUsageModel : usageList) {
                                                    if (Objects.nonNull(djpRemainingUsageModel)) {
                                                        if (Objects.nonNull(djpRemainingUsageModel.getProduct())) {
                                                            if (Objects.nonNull(djpRemainingUsageModel.getProduct().getQrValidType())) {
                                                                String enumCode = djpRemainingUsageModel.getProduct().getQrValidType().getEnumCode();
                                                                String enumName = djpRemainingUsageModel.getProduct().getQrValidType().getEnumName();
                                                                if (StringUtils.isNotBlank(enumCode) && StringUtils.isNotBlank(enumName)) {
                                                                    if (enumCode.equals(qrPassRequest.getQrType())) {
                                                                        if (djpRemainingUsageModel.getUsed() < djpRemainingUsageModel.getClaimed()) {
                                                                            int used = djpRemainingUsageModel.getUsed();
                                                                            List<QrType> modifableList = CollectionUtils.isEmpty(qrForExperienceCenterModel.getUsedHistory()) ? new ArrayList<>() : new ArrayList<>(qrForExperienceCenterModel.getUsedHistory());
                                                                            modifableList.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                                                                            djpRemainingUsageModel.setUsed(used + 1);
                                                                            qrForExperienceCenterModel.setUsedHistory(modifableList);
                                                                            modelService.saveOrUpdate(djpRemainingUsageModel);
                                                                            modelService.saveOrUpdate(qrForExperienceCenterModel);
                                                                            status.setMessage("Geçiş yapabilir.");
                                                                            status.setType("S");
                                                                            return status;
                                                                        } else {
                                                                            status.setMessage("Kullanım hakkı kalmamıştır.");
                                                                            status.setType("E");
                                                                            return status;
                                                                        }
                                                                    }
                                                                } else {
                                                                    status.setMessage("Bir sorun oluştu lütfen yönetici ile iletişime geçiniz.");
                                                                    status.setType("E");
                                                                    return status;
                                                                }
                                                            } else {
                                                                status.setMessage("Bir sorun oluştu lütfen yönetici ile iletişime geçiniz.");
                                                                status.setType("E");
                                                                return status;
                                                            }
                                                        } else {
                                                            status.setMessage("Bir sorun oluştu lütfen yönetici ile iletişime geçiniz.");
                                                            status.setType("E");
                                                            return status;
                                                        }
                                                    } else {
                                                        status.setMessage("Bir sorun oluştu lütfen yönetici ile iletişime geçiniz.");
                                                        status.setType("E");
                                                        return status;
                                                    }
                                                }
                                            } else {
                                                status.setMessage("Lütfen ödemenizi gerçekleştirdikten sonra tekrar deneyiniz.");
                                                status.setType("E");
                                                return status;
                                            }
                                        } else {
                                            status.setMessage("Qr'ın kullanım süresi geçmiştir. Lütfen deske başvurunuz.");
                                            status.setType("E");
                                            return status;
                                        }
                                        return status;
                                    }
                                    else {
                                        status.setMessage("QR , DJP PASS de bulunamadı.! tokenKey:" + qrPassRequest.getTokenKey());
                                        status.setType("E");
                                        return status;
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    @Override
    public void saveCodeUsage(QrForVoucherModel voucher, QrPassRequest qrPassRequest, Date usageDate, DjpPassLocationsModel djpPassLocation, DjpSingleCampaignCodeInfoModel djpSingleCampaignCodeInfo, double priceConvertedToDouble) {
        Date sub48HoursCurrentDate = DateUtil.sub48HoursCurrentDate(usageDate);
        String sapOrderID = qrPassRequest.getSapOrderID();
        if(CollectionUtils.isNotEmpty(qrPassRequest.getUsages())){
            List<String> usageList = new ArrayList<>();
            for (MultiUsagesData usage : qrPassRequest.getUsages()) {
                if(!usageList.contains(usage.getQrType())) {
                    fillAndSaveUsage(voucher, sapOrderID, usage.getQrType(), usageDate, djpPassLocation, djpSingleCampaignCodeInfo, priceConvertedToDouble, sub48HoursCurrentDate);
                    usageList.add(usage.getQrType());
                }
            }
        }else if(StringUtils.isNotBlank(qrPassRequest.getQrType())) {
            fillAndSaveUsage(voucher, sapOrderID,qrPassRequest.getQrType(), usageDate, djpPassLocation, djpSingleCampaignCodeInfo, priceConvertedToDouble, sub48HoursCurrentDate);
        }
    }

    @Override
    public FioriStatusResponse fillDjpVoucherCodeUsagesAndStatus(QrForVoucherModel voucher, QrPassRequest qrPassRequest, Date usageDate, DjpPassLocationsModel djpPassLocation, DjpSingleCampaignCodeInfoModel djpSingleCampaignCodeInfo) {
        FioriStatusResponse statusResponse = new FioriStatusResponse();

        boolean validOrder = Boolean.TRUE;

        try {

            String sapOrderID = qrPassRequest.getSapOrderID();

            // check sap order id and qr on igapass
            DjpVoucherCodeUsagesModel campaignCodeUsageBySapOrderIDAndVoucher = djpQRService.getCampaignCodeUsageBySapOrderIDAndVoucher(sapOrderID, voucher.getTokenKey().getCode());
            if(Objects.nonNull(campaignCodeUsageBySapOrderIDAndVoucher)){
                statusResponse.setType("E");
                statusResponse.setMessage("Girilen kod: "+voucher.getTokenKey().getCode()+" , "+DateUtil.dateToString(campaignCodeUsageBySapOrderIDAndVoucher.getUsageDate())+ " Tarihinde "+campaignCodeUsageBySapOrderIDAndVoucher.getSapOrderID()+" ID li sipariş ile kullanılmıştır. Aynı sipariş ile tekrar kullanılamaz!");
                return statusResponse;
            }

            String paidPrice = qrPassRequest.getPaidPrice();
            double priceConvertedToDouble = Double.parseDouble(paidPrice);
            priceConvertedToDouble = priceConvertedToDouble / 100;
            BigDecimal priceConvertedToBigDecimal = BigDecimal.valueOf(priceConvertedToDouble).setScale(2, RoundingMode.HALF_UP);

            // check sap order id and paid price on erp, set validOrder
            // TODO: need to be solved
//            validOrder = djpSapFactory.getIgaSap().checkIfValidOrderAndPaidPrice(voucher.getTokenKey().getCode(),sapOrderID,priceConvertedToBigDecimal);

            if (Boolean.FALSE.equals(validOrder)) {
                statusResponse.setType("E");
                statusResponse.setMessage("Order ID : "+sapOrderID+" Odeme Tutarı : "+priceConvertedToBigDecimal.toString()+" Verilerine ait Sipariş Bilgileri SAP ile uyuşmamaktadır !");
                return statusResponse;
            }

            // fill voucher code usage detail


            saveCodeUsage(voucher, qrPassRequest, usageDate, djpPassLocation, djpSingleCampaignCodeInfo, priceConvertedToDouble);

            return statusResponse;
        } catch (Exception ex) {
            LOGGER.error("fillDjpVoucherCodeUsagesAndStatus has an error:",ex);
            statusResponse.setType("E");
            statusResponse.setMessage("Sipariş bilgileri kontrol edilirken hata oluştu");
            return statusResponse;
        }
    }

    @Override
    public void startDjpUsageSendToErpProcess(QrPassRequest qrPassRequest, QrForVoucherModel qrForVoucherModel, boolean isStockedServiceUsaged) {
        Double paidPrice = StringUtils.isNotBlank(qrPassRequest.getPaidPrice()) ? Double.valueOf(qrPassRequest.getPaidPrice()) : null;
        if (Boolean.FALSE.equals(qrForVoucherModel.getVoucherForRemainingUsage())) {
            DjpBoardingPassModel djpBoardingPassModel = null;
            if(Boolean.TRUE.equals(qrForVoucherModel.getIsCustomSingleCampaignCode())){
                djpBoardingPassModel = djpBoardingService.getDjpBoardingPassByTokenAndSapOrderID(qrForVoucherModel.getTokenKey().getCode(),qrPassRequest.getSapOrderID());
            }else {
                djpBoardingPassModel = djpBoardingService.getDjpBoardingPassByToken(qrForVoucherModel.getTokenKey().getCode());
            }

            if (Objects.nonNull(djpBoardingPassModel) && StringUtils.isBlank(djpBoardingPassModel.getContractID())) {
                djpBoardingPassModel.setContractID(qrForVoucherModel.getContractId());
                modelService.saveOrUpdate(djpBoardingPassModel);
            }
            if(StringUtils.isNotBlank(qrForVoucherModel.getSapOrderID()) && StringUtils.isNotBlank(qrForVoucherModel.getEntryNumberOnSapOrder())){
                if(Boolean.TRUE.equals(qrPassRequest.getNewSaleCompleted())){
                    djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrPassRequest.getQrType(), qrForVoucherModel, djpBoardingPassModel, null,qrPassRequest.getSapOrderID(),paidPrice);
                }
            }else {
                if(!Boolean.TRUE.equals(isStockedServiceUsaged))
                    djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrPassRequest.getQrType(), qrForVoucherModel, djpBoardingPassModel, null,qrPassRequest.getSapOrderID(),paidPrice);
            }

        }
    }

    @Override
    public FioriStatusResponse usePackageWithVoucher(QrForVoucherModel voucher, QrPassRequest passRequest, Date usageDate, DjpPassLocationsModel djpPassLocation) throws ParseException {
        FioriStatusResponse status = new FioriStatusResponse();
        boolean hasNotClaimed = Boolean.FALSE;
        String whereUsed = passRequest.getQrType();
        hasNotClaimed = checkIfClaimedForPackageVoucher(whereUsed,voucher,hasNotClaimed);
        if (Boolean.TRUE.equals(hasNotClaimed)) {
            status.setMessage("Voucher'ın Kullanım Hakkı Kalmamıştır.");
            status.setType("E");
            return status;
        }else {
            Optional<DjpVoucherRemainingModel> remainingOptional = voucher.getRemainingForPackage().stream().filter(igaVoucherRemainingModel -> igaVoucherRemainingModel.getWhereIsValid().getCode().equals(qrTypeEnumHashMap.get(whereUsed.substring(0, 2)))
                    || igaVoucherRemainingModel.getWhereIsValid().getCode().equals(qrTypeEnumHashMap.get(whereUsed))).findFirst();
            if(remainingOptional.isPresent()){
                Integer adultCount = Objects.nonNull(passRequest) ? passRequest.getAdultCount() : null;
                Integer childCount = Objects.nonNull(passRequest) ? passRequest.getChildCount() : null;
                PremiumPackagePassengerEnum passengerEnum = null;
                if(adultCount > 0 && remainingOptional.get().getPassengerType().equals(PremiumPackagePassengerEnum.ADULT)){
                    passengerEnum = PremiumPackagePassengerEnum.ADULT;
                } else {
                    passengerEnum = PremiumPackagePassengerEnum.CHILD;
                }
                DjpVoucherRemainingModel remainingForVoucher = remainingOptional.get();
                boolean haveDailyPass = remainingForVoucher.getPackagedProduct().getSupercategories().stream().anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCoreConstants.PREMIUM_DAILY_PACKAGE_CATEGORY));
                if (haveDailyPass) {
                    if (Objects.nonNull(remainingForVoucher.getPassengerType()) && Objects.nonNull(passengerEnum)) {
                        if (remainingForVoucher.getPassengerType().equals(passengerEnum)) {
                            remainingForVoucher.setUsed(remainingForVoucher.getUsed() + 1);
                            modelService.saveOrUpdate(remainingForVoucher);
                            if (Objects.nonNull(remainingForVoucher.getPeriodOfValid())) {
                                Date newExpireDate = usageDate;
                                for (DjpVoucherRemainingModel dailyPassPackageRemaining : voucher.getRemainingForPackage()) {
                                    if(Objects.nonNull(dailyPassPackageRemaining.getPeriodOfValid())) {
                                        Date expireDate = DateUtil.addForPeriodValidToDate(usageDate, dailyPassPackageRemaining.getPeriodOfValid(),dailyPassPackageRemaining.getnDaysPeriodDayCount());
                                        dailyPassPackageRemaining.setExpireDate(expireDate);
                                        dailyPassPackageRemaining.setPeriodOfValid(null);
                                        modelService.saveOrUpdate(dailyPassPackageRemaining);
                                        int expireDateCompare = DateUtil.dateCompare(newExpireDate, expireDate);
                                        if(expireDateCompare < 0){
                                            newExpireDate = expireDate;
                                        }
                                    }
                                }
                                voucher.setExpireDate(newExpireDate);
                            }
                        }
                    }
                }else {
                    remainingForVoucher.setUsed(remainingForVoucher.getUsed() + 1);
                    modelService.saveOrUpdate(remainingForVoucher);
                }
                Set<QrType> whereUseds= new HashSet<>();
                if (!voucher.getWhereUsed().isEmpty()) {
                    whereUseds.addAll(voucher.getWhereUsed());
                }
                voucher.setUsageDate(usageDate);
                whereUseds.add(QrType.valueOf(qrTypeEnumHashMap.get(whereUsed)));
                voucher.setWhereUsed(whereUseds);
                voucher.setUsed(voucher.getUsed() + 1);
                voucher.setWhichLocation(djpPassLocation);
                modelService.saveOrUpdate(voucher);
                saveCodeUsage(voucher,passRequest,usageDate,djpPassLocation,null,0D);
                this.startDjpUsageSendToErpProcess(passRequest,voucher,Boolean.FALSE);
            }
            status.setMessage("Hizmet Kullandırıldı.");
            status.setType("S");
            return status;
        }
    }
    private boolean checkIfClaimedForPackageVoucher(String qrType, QrForVoucherModel voucher, boolean hasNotClaimed) {
        Optional<DjpVoucherRemainingModel> remainingOptional = voucher.getRemainingForPackage().stream().filter(igaVoucherRemainingModel -> igaVoucherRemainingModel.getWhereIsValid().getCode().equals(qrTypeEnumHashMap.get(qrType.substring(0, 2))) ||
                igaVoucherRemainingModel.getWhereIsValid().getCode().equals(qrTypeEnumHashMap.get(qrType))).findFirst();
        if(remainingOptional.isPresent()){
            DjpVoucherRemainingModel igaVoucherRemainingModel = remainingOptional.get();
            hasNotClaimed = igaVoucherRemainingModel.getClaimed().equals(igaVoucherRemainingModel.getUsed());
        }
        return hasNotClaimed;
    }
    private void fillAndSaveUsage(QrForVoucherModel voucher, String sapOrderID,String whereUsed, Date usageDate, DjpPassLocationsModel djpPassLocation, DjpSingleCampaignCodeInfoModel djpSingleCampaignCodeInfo, double priceConvertedToDouble, Date sub48HoursCurrentDate) {
        DjpVoucherCodeUsagesModel codeUsage = new DjpVoucherCodeUsagesModel();
        codeUsage.setSapOrderID(sapOrderID);
        codeUsage.setUsageDate(usageDate);

        String usageAreaCode = qrTypeEnumHashMap.get(whereUsed);
        QrType qrTypeEnum = QrType.valueOf(usageAreaCode);
        codeUsage.setWhereUsed(qrTypeEnum);

        codeUsage.setPaidPrice(priceConvertedToDouble);
        codeUsage.setVoucher(voucher);
        codeUsage.setContractID(Objects.nonNull(djpSingleCampaignCodeInfo) ? djpSingleCampaignCodeInfo.getContractID() : voucher.getContractId());
        codeUsage.setWhichLocation(djpPassLocation);
        String qrKey = voucher.getTokenKey().getCode();
        if (Boolean.TRUE.equals(voucher.getPromotionVoucher())) {
            List<DjpBoardingPassModel> getDjpBoardingPassesByTokenAndSapOrderID = djpBoardingService.getDjpBoardingPassesByTokenAndSapOrderID(qrKey, sapOrderID);
            if (CollectionUtils.isNotEmpty(getDjpBoardingPassesByTokenAndSapOrderID)) {
                codeUsage.setBoardingpasses(getDjpBoardingPassesByTokenAndSapOrderID);
            }
        } else {
            List<DjpBoardingPassModel> getDjpBoardingPassByTokenAndUsage = djpBoardingService.getDjpBoardingPassByTokenAndUsage(qrKey, qrTypeEnum, sub48HoursCurrentDate);

            if (CollectionUtils.isNotEmpty(getDjpBoardingPassByTokenAndUsage)) {
                Collection<DjpVoucherCodeUsagesModel> usageDetails = voucher.getUsageDetails();
                if (CollectionUtils.isNotEmpty(usageDetails)) {
                    List<DjpBoardingPassModel> boardingPassesWithCurrentUsage = new ArrayList<>();
                    boardingPassesWithCurrentUsage.addAll(getDjpBoardingPassByTokenAndUsage);
                    for (DjpBoardingPassModel djpBoardingPassModel : getDjpBoardingPassByTokenAndUsage) {
                        for (DjpVoucherCodeUsagesModel usageDetail : usageDetails) {
                            Collection<DjpBoardingPassModel> boardingpasses = usageDetail.getBoardingpasses();
                            if (CollectionUtils.isNotEmpty(boardingpasses)) {
                                if (boardingpasses.contains(djpBoardingPassModel) && usageDetail.getWhereUsed().equals(qrTypeEnum)) {
                                    boardingPassesWithCurrentUsage.remove(djpBoardingPassModel);
                                }
                            }
                        }
                    }
                    if(CollectionUtils.isNotEmpty(boardingPassesWithCurrentUsage)) {
                        codeUsage.setBoardingpasses(boardingPassesWithCurrentUsage);
                    }
                } else {
                    codeUsage.setBoardingpasses(getDjpBoardingPassByTokenAndUsage);
                }
            }
        }

        modelService.saveOrUpdate(codeUsage);
    }
    private boolean dateValidationForNuevo(QrForExternalModel qrForExternalModel, String qrType){
        if(isNuevoAndFastTrack(qrForExternalModel, qrType)){
            Date currentDate = new Date();
            if(qrForExternalModel.getStartDate() != null && qrForExternalModel.getEndDate() != null){
                return qrForExternalModel.getStartDate().after(currentDate) || qrForExternalModel.getEndDate().before(currentDate);
            }
        }
        return false;
    }
    private boolean isNuevoAndFastTrack(QrForExternalModel qrForExternalModel, String qrType){
        return EXTERNAL_NUEVO.equals(StringUtils.isNotEmpty(qrForExternalModel.getCompanyClientId())? qrForExternalModel.getCompanyClientId().split("_")[0]: Strings.EMPTY)
                && QrType.FAST_TRACK.getCode().equals(qrTypeEnumHashMap.get(qrType.substring(0,2)))
                && qrForExternalModel.getStartDate() != null && qrForExternalModel.getEndDate() != null;
    }
    private String getJsonFromRequst(Object dto) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dto);
    }

    private String generateLog(String jsonFromRequest) throws JsonProcessingException {
        JSONObject jsonObject = new JSONObject(jsonFromRequest);
        return jsonObject.toString();
    }

    @Override
    public boolean doesTheCompanyHaveUsageRight(QrForVoucherModel qrForVoucherModel) {
        return false;
    }

    @Override
    public QrCodeValidResponse generateResponse(String tokenKey, String qrType, Date currentDate, Boolean newSale, DjpPassLocationsModel djpPassLocation) {
        return null;
    }

    @Override
    public CheckCustomerOwnedServiceResponse checkCustomerOwnedService(String cardNo, String device) {
        CheckCustomerOwnedServiceResponse response = new CheckCustomerOwnedServiceResponse();
        if(StringUtils.isBlank(cardNo)){
            response.setMessage(messageSource.getMessage("djp.check.custermer.owned.service.card.no.mandatory",null, LocaleContextHolder.getLocale()));
            response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
            return response;
        }else {
            if(cardNo.startsWith("+")){
                return getCheckCustomerOwnedByUid(cardNo,device, response);
            }else {
                QrForCardModel qrForCardModel = djpQRService.getQRForCardByTokenKey(cardNo);
                if(Objects.nonNull(qrForCardModel)){
                    boolean hasLoungeServiceCurrentDjpPassCard = qrForCardModel.getWhereIsValidQr().contains(QrType.ALL) || qrForCardModel.getWhereIsValidQr().contains(QrType.LOUNGE);
                    if (hasLoungeServiceCurrentDjpPassCard) {
                        response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.DJP_PASS_CARD);
                        response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                        return response;
                    } else {
                        response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                        response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                        return response;
                    }
                }else {
                    QrForMobileModel qrForMobile = djpQRService.validQrCodeForMobile(cardNo);
                    if(Objects.nonNull(qrForMobile)){
                        String phoneNumber = qrForMobile.getTokenKey().getUserUid();
                        if(StringUtils.isNotBlank(phoneNumber)){
                            return getCheckCustomerOwnedByUid(phoneNumber,device,response);
                        }else {
                            response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                            response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                            return response;
                        }
                    }else {
                        response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                        response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                        return response;
                    }

                }

            }

        }
    }
    private CheckCustomerOwnedServiceResponse getCheckCustomerOwnedByUid(String cardNo, String device, CheckCustomerOwnedServiceResponse response) {
        QrForCardModel qrForCardModel = djpQRService.getQRForCardByPhoneNumber(cardNo);
        boolean isFiori = "F".equals(device);
        if(Objects.nonNull(qrForCardModel) && !isFiori){
            boolean hasLoungeServiceCurrentDjpPassCard = qrForCardModel.getWhereIsValidQr().contains(QrType.ALL) || qrForCardModel.getWhereIsValidQr().contains(QrType.LOUNGE);
            if (hasLoungeServiceCurrentDjpPassCard) {
                response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.DJP_PASS_CARD);
                response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                return response;
            } else {
                response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                return response;
            }
        }else {
            List<DjpRemainingUsageModel> customerUsages = djpRemainingUsageService.getCustomerUsages(cardNo, null, DjpCoreConstants.PackageOrProduct.PACKAGE);
            if (customerUsages.isEmpty()) {
                response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                return response;
            } else {
                boolean isDailyPackage = customerUsages
                        .stream()
                        .filter(djpRemainingUsageModel -> Objects.nonNull(djpRemainingUsageModel.getPackagedProduct()))
                        .map(DjpRemainingUsageModel::getPackagedProduct)
                        .map(ProductModel::getSupercategories)
                        .anyMatch(categoryModel -> categoryModel.stream().anyMatch(category -> category.getCode().equals(DjpCoreConstants.PREMIUM_DAILY_PACKAGE_CATEGORY)));
                if(isDailyPackage){
                    boolean isItUsed = false;
                    List<DjpRemainingUsageModel> isItUsedDailyUsage = customerUsages
                            .stream()
                            .filter(djpRemainingUsageModel -> Objects.isNull(djpRemainingUsageModel.getPeriodOfValid()))
                            .filter(djpRemainingUsageModel -> djpRemainingUsageModel.getPackagedProduct().getSupercategories().stream().anyMatch(categoryModel -> categoryModel.getCode().equals(DjpCoreConstants.PREMIUM_DAILY_PACKAGE_CATEGORY)))
                            .collect(Collectors.toList());
                    isItUsed = !isItUsedDailyUsage.isEmpty();
                    if(Boolean.FALSE.equals(isItUsed)){
                        response.setCustomerOwnedService(DjpCommerceservicesConstants.checkCustomerOwnedService.NO_MATCH);
                        response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
                        return response;
                    }else {
                        Optional<DjpRemainingUsageModel> adultOptional = isItUsedDailyUsage
                                .stream()
                                .filter(djpRemainingUsageModel -> Objects.nonNull(djpRemainingUsageModel.getPassengerType()))
                                .filter(djpRemainingUsageModel -> PassengerType.ADULT.getCode().equals(djpRemainingUsageModel.getPassengerType().getCode()))
                                .findFirst();
                        if(adultOptional.isPresent()){
                            isItUsedDailyUsage.clear();
                            isItUsedDailyUsage.add(adultOptional.get());
                        }
                        return generateResponseForOwnedService(response, isItUsedDailyUsage);
                    }
                }else {
                    return generateResponseForOwnedService(response, customerUsages);
                }

            }
        }
    }
    private CheckCustomerOwnedServiceResponse generateResponseForOwnedService(CheckCustomerOwnedServiceResponse response, List<DjpRemainingUsageModel> customerUsages) {
        List<DjpProductSaleWsData> userProducts = new ArrayList<>();
        customerUsages.forEach(customerUsage -> userProducts.add(djpProductSaleWsDataPopulator.populate(customerUsage)));
        if (!CollectionUtils.isEmpty(userProducts)) {
            Collections.sort(userProducts, (o2, o1) -> o1.getPackageCode().compareTo(o2.getPackageCode()));
        }
        response.setCustomerOwnedService(DjpUtil.clearTurkishCharacters(userProducts.get(0).getPackageName()).toUpperCase());
        response.setMessage(DjpCommerceservicesConstants.checkCustomerOwnedService.Success);
        return response;
    }
}
