package com.djp.facades;

import com.djp.dto.DjpTelephoneCountriesWsDto;

import java.util.List;

public interface DjpTelephoneCountriesFacade {

    List<DjpTelephoneCountriesWsDto> findTelephoneCountries();
}
