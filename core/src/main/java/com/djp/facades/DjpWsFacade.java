package com.djp.facades;

import com.djp.dto.UserWsDto;
import com.djp.request.BoardingPassRequest;
import com.djp.request.QrForFioriRequest;
import com.djp.response.BoardingPassResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.servlet.http.HttpServletRequest;
import java.text.ParseException;

import java.text.ParseException;

public interface DjpWsFacade {
    UserWsDto getOrderList(QrForFioriRequest qrForFioriRequest) throws JsonProcessingException;

    BoardingPassResponse boardingPassAction(BoardingPassRequest boardingPassRequest) throws JsonProcessingException, ParseException;

     UserWsDto getUserByToken(final String fields, QrForFioriRequest qrForFioriRequest, final HttpServletRequest request) throws ParseException, JsonProcessingException;
}

