package com.djp.facades;

import com.djp.core.model.*;
import com.djp.dto.DjpProductSaleWsData;
import com.djp.request.BoardingPassRequest;
import com.djp.request.QrPassRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.dto.DjpProductSaleWsData;

import java.util.List;

public interface DjpRemainingUsageFacade {

    List<DjpProductSaleWsData> getUserProducts(String uid, String productCategory, Object o, QrForFioriModel qrForFioriModel, Object o1, Object o2, Object o3, String qrType);

    void addUsedInDjpRemainingUsage(FioriStatusResponse statusResponse, String customerUid, String productCategory, BoardingPassRequest boardingPassRequest, QrPassRequest qrPassRequest, boolean usageWithDjpPassCard, QrForExternalModel qrForExternalModel, String boardingTypeCode);

    List<DjpProductSaleWsData> getUserProducts(String customerUid, String productCategory, String productType, QrForFioriModel qrForFioriModel, QrForMobileModel qrForMobileModel, QrForExternalModel qrForExternalModel, QrForVoucherModel qrForVoucherModel, String whereUsed);

    void startDjpUsageSendToErpProcess(String whereUsed,ItemModel qr,ItemModel boarding,String boardingType,String sapOrderID,Double paidPrice);

    void startDjpUsedSendToExternalCompanyProcess(ItemModel qr);

    List<DjpProductSaleWsData> getForSomeBodyProducts(String customerUid, String productCategory, QrForVoucherModel qrForVoucherModel,String whereUsed);
}
