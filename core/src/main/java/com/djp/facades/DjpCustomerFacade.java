package com.djp.facades;

import com.djp.data.CustomerData;

public interface DjpCustomerFacade {
    CustomerData getUserForUID(String userId);
}