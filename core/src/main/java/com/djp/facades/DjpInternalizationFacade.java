package com.djp.facades;

import com.djp.data.DialingCodeData;

import java.util.List;

public interface DjpInternalizationFacade {
    List<DialingCodeData> getAllDialingCodes();
}
