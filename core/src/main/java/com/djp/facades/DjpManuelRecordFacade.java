package com.djp.facades;

import com.djp.dto.UserWsDto;
import com.djp.request.QrForFioriRequest;

public interface DjpManuelRecordFacade {
    UserWsDto checkManuelBoarding(QrForFioriRequest qrForFioriRequest);
}
