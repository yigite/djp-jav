package com.djp.facades;

import com.djp.data.FioriFlightDetailData;

public interface DjpFioriFlightFacade {
    FioriFlightDetailData getFioriFlightDetail(String flightDate, final String flightNumber);
}
