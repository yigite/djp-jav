package com.djp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DjpDateUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DjpDateUtil.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    public static String date2String(Date date){
        return DATE_FORMAT.format(date);
    }
}
