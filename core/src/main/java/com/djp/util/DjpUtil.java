package com.djp.util;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DjpUtil {
    private static Locale localeForTurkish = Locale.forLanguageTag("tr_TR");
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", localeForTurkish);

    public static String toDatePattern(Date date, String pattern) {
        if (StringUtils.isEmpty(pattern))
            return simpleDateFormat.format(date);
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    public static Date stringToDate(String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

    public static Date stringToDateAndTime(String dateAndTime,String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(dateAndTime);
    }

    public static String subOneYearDateAndToDatePattern(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(calendar.getTime());
    }


    public static String daysBetween(Date expireDate) {
        Date today = new Date();
        long daysBetween = (expireDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
        return String.valueOf(daysBetween);
    }

    public static int dateCompare(Date expireDate,Date currentDate) throws ParseException {
        return expireDate.compareTo(currentDate);
    }

    public static String clearTurkishCharacters(String str) {
        String ret = str;
        char[] turkishChars = new char[]{0x131, 0x130, 0xFC, 0xDC, 0xF6, 0xD6, 0x15F, 0x15E, 0xE7, 0xC7, 0x11F, 0x11E};
        char[] englishChars = new char[]{'i', 'I', 'u', 'U', 'o', 'O', 's', 'S', 'c', 'C', 'g', 'G'};
        for (int i = 0; i < turkishChars.length; i++) {
            ret = ret.replaceAll(new String(new char[]{turkishChars[i]}), new String(new char[]{englishChars[i]}));
        }
        return ret;
    }
}
