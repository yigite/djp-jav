package com.djp.util;

import com.djp.rest.data.ServiceResultWsData;
import org.springframework.http.HttpStatus;

public class DjpServiceUtil {

    private DjpServiceUtil(){

    }

    public static ServiceResultWsData generateServiceResult(String message, String resultType, String validationMessage, int statusCode, int transactionCode){
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(message);
        serviceResultWsData.setResultType(resultType);
        serviceResultWsData.setResultValidationMessage(validationMessage);
        serviceResultWsData.setStatusCode(statusCode);
        serviceResultWsData.setTransactionCode(transactionCode);
        return serviceResultWsData;
    }

    public static ServiceResultWsData generateSuccessServiceResult(){

        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(HttpStatus.OK.toString());
        serviceResultWsData.setResultType("OK");
        serviceResultWsData.setResultValidationMessage("");
        serviceResultWsData.setStatusCode(HttpStatus.OK.value());
        serviceResultWsData.setTransactionCode(HttpStatus.OK.value());
        return serviceResultWsData;
    }

    public static ServiceResultWsData generateErrorServiceResult(String errorMessage){
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(errorMessage);
        serviceResultWsData.setResultType("");
        serviceResultWsData.setResultValidationMessage("");
        serviceResultWsData.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        serviceResultWsData.setTransactionCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return serviceResultWsData;
    }

    public static ServiceResultWsData generateErrorServiceResult(String errorMessage,String status){
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(errorMessage);
        serviceResultWsData.setResultType("");
        serviceResultWsData.setResultValidationMessage("");
        serviceResultWsData.setStatusCode(HttpStatus.valueOf(status).value());
        serviceResultWsData.setTransactionCode(HttpStatus.valueOf(status).value());
        return serviceResultWsData;
    }

    public static ServiceResultWsData generateErrorServiceResultWithCustomStatusCode(String errorMessage,String status){
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(errorMessage);
        serviceResultWsData.setResultType("");
        serviceResultWsData.setResultValidationMessage("");
        serviceResultWsData.setStatusCode(Integer.valueOf(status));
        serviceResultWsData.setTransactionCode(Integer.valueOf(status));
        return serviceResultWsData;
    }

    public static ServiceResultWsData generateErrorServiceResultForMobile(String errorMessage){
        ServiceResultWsData serviceResultWsData = new ServiceResultWsData();
        serviceResultWsData.setMessage(errorMessage);
        serviceResultWsData.setResultType("");
        serviceResultWsData.setResultValidationMessage("");
        serviceResultWsData.setStatusCode(HttpStatus.GONE.value());
        serviceResultWsData.setTransactionCode(HttpStatus.GONE.value());
        return serviceResultWsData;
    }
}
