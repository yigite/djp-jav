package com.djp.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.Resource;

public class ProjectUtil {
    @Resource(name = "airportSapServicesAccessObjectMapper")
    public ObjectMapper airportSapServicesAccessObjectMapper;

    public String convertToJsonString(Object objects) {
        try {
            return airportSapServicesAccessObjectMapper.writeValueAsString(objects);
        } catch (Exception e) {
            //LOGGER.error("JSON CONVERT ERROR :", e);
            return "";
        }
    }

}
