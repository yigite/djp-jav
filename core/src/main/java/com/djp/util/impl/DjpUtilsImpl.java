package com.djp.util.impl;

import com.djp.service.DjpRemainingUsageService;
import com.djp.util.DjpUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.util.Date;

public class DjpUtilsImpl implements DjpUtils {

    @Resource
    DjpRemainingUsageService djpRemainingUsageService;
    @Resource
    MessageSource messageSource;
    @Override
    public String generateErrorResultMessageForCarParking(String uid) {
        String packageWithoutParkingByUid = djpRemainingUsageService.getPackageWithoutParkingByUid(uid);
        if(StringUtils.isNotBlank(packageWithoutParkingByUid)){
            return messageSource.getMessage("has.not.remaining.custom.error.text", new Object[]{ packageWithoutParkingByUid }, LocaleContextHolder.getLocale());
        }else {
            return messageSource.getMessage("has.not.remaining.default.error.text",null, LocaleContextHolder.getLocale());
        }

    }

    @Override
    public int dateCompare(Date expireDate, Date currentDate) {
        return expireDate.compareTo(currentDate);
    }
}
