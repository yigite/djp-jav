package com.djp.util;

import java.text.ParseException;
import java.util.Date;

public interface DjpUtils {
    String generateErrorResultMessageForCarParking(String uid);

    int dateCompare(Date expireDate, Date currentDate);
}
