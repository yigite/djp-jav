package com.djp.strategy.impl;

import com.djp.core.enums.QrType;
import com.djp.core.model.QrForCardModel;
import com.djp.service.DjpQRService;
import com.djp.strategy.DjpQrTypeValidateStrategy;

import jakarta.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class DjpQrTypeValidateQrForCardStrategy implements DjpQrTypeValidateStrategy {

    @Resource
    private DjpQRService igaQRService;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Override
    public String validateWhereUsedQr(QrType qrTypeEnum, final String qrUniqueId) {

        QrForCardModel qrForCardModel = igaQRService.getQRForCardByTokenKey(qrUniqueId);
        String hashMapKey = qrTypeEnumHashMap.entrySet()
                .stream()
                .filter(entry -> qrTypeEnum.getCode().equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList()).get(0);

        QrType type = QrType.valueOf(qrTypeEnumHashMap.get(hashMapKey.substring(0,2)));

        if (Objects.isNull(qrForCardModel)) {
            return "Kart bulunamadı. Kart key : " + qrUniqueId;
        }

        if (qrForCardModel.getWhereIsValidQr().contains(QrType.ALL)) {
            return "OK";
        }
        if (Objects.nonNull(type)){
            if (qrForCardModel.getWhereIsValidQr().contains(type)){
                return "OK";
            }
        }
        if (qrForCardModel.getWhereIsValidQr().contains(qrTypeEnum)) {
            return "OK";
        } else {
            return "Kartınızın Bu Hizmet İçin Kullanımı Mevcut Değil!";
        }

    }
}
