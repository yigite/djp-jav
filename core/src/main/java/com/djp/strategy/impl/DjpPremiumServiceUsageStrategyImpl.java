package com.djp.strategy.impl;

import com.djp.core.constant.DjpCommerceservicesConstants;
import com.djp.core.constant.DjpCoreConstants;
import com.djp.core.enums.QrType;
import com.djp.core.enums.UsageMethod;
import com.djp.core.model.*;
import com.djp.dto.AddressWsDTO;
import com.djp.dto.ContractedPackagedData;
import com.djp.dto.DjpProductSaleWsData;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.data.MultiUsagesData;
import com.djp.facades.DjpInternalizationFacade;
import com.djp.data.CustomerData;
import com.djp.data.DialingCodeData;
import com.djp.dto.UserWsDto;
import com.djp.facades.DjpQRFacade;
import com.djp.modelservice.ModelService;
import com.djp.request.QrPassRequest;
import com.djp.service.DjpExternalCompanyService;
import com.djp.modelservice.util.DateUtil;
import com.djp.service.*;
import com.djp.util.DjpUtils;
import com.djp.request.QrForFioriRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.facades.DjpRemainingUsageFacade;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import com.djp.strategy.DjpPremiumServiceUsageStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.util.*;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicBoolean;

public class DjpPremiumServiceUsageStrategyImpl implements  DjpPremiumServiceUsageStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DjpPremiumServiceUsageStrategyImpl.class);
    private static final String WOMAN_TR = "KADIN";
    private static final String WOMAN_EN = "WOMAN";

    @Resource
    DjpCustomerService djpCustomerService;

    @Resource
    DjpQRService djpQRService;

    @Resource
    DjpRemainingUsageFacade djpRemainingUsageFacade;

    @Resource
    private DjpUtils djpUtils;

    @Resource
    DjpInternalizationFacade djpInternalizationFacade;

    @Resource
    private HashMap<String, String> qrTypeEnumHashMap;

    @Resource
    private ModelService modelService;

    @Resource
    private DjpQRFacade djpQRFacade;

    @Value("${show.dynamic.has.not.remaining.message}")
    private String showDynamicHasNotRemainingMessage;

    @Resource
    private DjpBoardingService djpBoardingService;

    @Resource
    private DjpTokenService djpTokenService;

    @Value("${fiori.host.type}")
    private String HOST_TYPE;

    @Resource
    private DjpRemainingUsageService djpRemainingUsageService;

    @Resource
    private DjpExternalCompanyService djpExternalCompanyService;

    private final static String EMIRATES_AIRLINE_CODE = "EK";

    @Resource
    private List<String> stockedServiceUsagePoint;

    @Override
    public UserWsDto fillUserWsDTOByQrForFiori(String fieldsDto, DjpUniqueTokenModel token, CustomerData customerData, QrForFioriRequest qrForFioriRequest, QrForFioriModel qrForFioriModel, DjpPassLocationsModel djpPassLocationsModel){
        FioriStatusResponse result = new FioriStatusResponse();

        UserWsDto userWs = new UserWsDto();
        userWs.setEmail(customerData.getEmail());
        if(customerData.getProfilePicture()!=null) {
            userWs.setImageUrl(customerData.getProfilePicture().getUrl());
        }
        CustomerModel customerByUid = djpCustomerService.findCustomerByUid(customerData.getUid());
        if(Objects.nonNull(customerByUid)){
            setOneMoreAreaOnUserDto(customerByUid,userWs);
        }
        Boolean haveDjpPass = djpCustomerService.checkHaveDjpPass(customerData.getUid());
        if(Boolean.FALSE.equals(haveDjpPass) && !djpPassLocationsModel.getLocationID().equals(DjpCommerceservicesConstants.DjpPassLocations.DJP)){
            result.setType("E");
            result.setMessage("Müşteri DJP Üyelik Paketi Sahibi Olmadığı için sadece DJP servislerden faydalanabilir.");
            if(Objects.nonNull(qrForFioriModel)) {
                djpQRService.saveUnapprovedPass(qrForFioriModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForFiori");
            }
        }
        if(StringUtils.isNotBlank(qrForFioriRequest.getProductCategory())) {
            userWs.setUserProducts(djpRemainingUsageFacade.getUserProducts(customerData.getUid(), qrForFioriRequest.getProductCategory(), null, qrForFioriModel, null,null,null,qrForFioriRequest.getQrType()));
        }
        if(Objects.nonNull(token))
            userWs.setOrderCode(token.getOrderCode());


        if(userWs.getUserProducts().isEmpty()){
            result.setType("E");
            boolean isCarParking = DjpCommerceservicesConstants.CARPARKING_CODE.equals(qrForFioriRequest.getQrType());
            String isShowDynamicMessage = showDynamicHasNotRemainingMessage;
            if(Boolean.TRUE.equals(isCarParking) && Boolean.TRUE.equals(DjpCoreConstants.ExternalCompany.ENABLED.equals(isShowDynamicMessage))){
                result.setMessage(djpUtils.generateErrorResultMessageForCarParking(customerData.getUid()));
            }else {
                result.setMessage("Kullanıcıya Ait Ürün Kullanım Hakkı Yoktur!");
            }

            if(Objects.nonNull(qrForFioriModel)) {
                djpQRService.saveUnapprovedPass(qrForFioriModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForFiori");
            }
        }else {
            if(Objects.nonNull(qrForFioriModel)){
                if(!"E".equals(result.getType())) {
                    if (!qrForFioriModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2)).startsWith(qrTypeEnum.getCode()) || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2))))) {
                        result.setType("E");
                        result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                        djpQRService.saveUnapprovedPass(qrForFioriModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForFiori");
                    } else {
                        result.setType("S");
                        result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
                    }
                }
            }
        }


        userWs.setResult(result);
        return userWs;
    }

    @Override
    public void beforeSaveQuickBoardingCheckFlightAndAccess(QrForFioriRequest qrForFioriRequest, FioriStatusResponse response, DjpPassLocationsModel djpPassLocation,boolean isServiceUsageWithMembership) {
        int diffBetweenToDate = 0;
        List<DjpAccessRightsModel> accessRight = new ArrayList<DjpAccessRightsModel>();
        if (DjpCoreConstants.DjpPassLocations.DJP.equals(djpPassLocation.getLocationID())) {
            try {
                String status = djpBoardingService.checkInternationalStatusForBp(qrForFioriRequest, null, null, null, QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType())));
                if (StringUtils.isNotBlank(status)) {
                    if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_DOMESTIC.equals(status)) {
                        response.setMessage("Yolcuya ait boarding kartın uçuşu dış hat uçuşudur. İç hatlarda kullanım hakkı yoktur.");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                    } else if (DjpCommerceservicesConstants.StatusForFlightHours.NOT_USED_FOR_INTERNATIONAL.equals(status)) {
                        response.setMessage("Yolcuya ait boarding kartın uçuşu iç hat uçuşudur. Dış hatlarda kullanım hakkı yoktur.");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());
                    }else if(DjpCommerceservicesConstants.StatusForFlightHours.NOT_AVAILABLE_FLIGHT_IN_DJP.equals(status)){
                        response.setMessage("Uçuş iGA da planlanmamaktadır.");
                        response.setType("E");
                        LOG.error("responseForFiori:" + response.getType());

                    }
                }
            } catch (Exception ex) {
                LOG.error("checkInternationalStatusForBp has an error:", ex);
                response.setMessage("Uçuş iGA da planlanmamaktadır.");
                response.setType("E");
                LOG.error("responseForFiori:" + response.getType());
            }
        }

        if(!"E".equals(response.getType())){
            String messageForCheckIfBoardingForLocationIATA = StringUtils.EMPTY;
            if ("PROD".equals(HOST_TYPE)) {
                try {
                    diffBetweenToDate = djpBoardingService.getDiffBetweenToDate(qrForFioriRequest, null, null, null);
                    if (diffBetweenToDate > 48 || diffBetweenToDate < -48) {
                        response.setMessage("Boarding kart güncel bir boarding kart değildir, yolcunun hizmet kullanım hakkı yoktur.");
                        LOG.info("diffBetweenToDate:" + diffBetweenToDate);
                        response.setType("E");
                    } else {
                        messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                        if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                            response.setMessage(messageForCheckIfBoardingForLocationIATA);
                            response.setType("E");
                        } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                            response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                            response.setType("E");
                        } else {
                            accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getPnrCode().substring(36, 39),djpPassLocation,isServiceUsageWithMembership);
                            fillResponseByAccessRight(response, accessRight, qrForFioriRequest, false,UsageMethod.TOURNIQUET);
                        }
                    }
                }catch (Exception ex){
                    response.setMessage("Uçuş kontrol edilirken hata oluştu");
                    LOG.error("has an error diffBetweenToDate:",ex);
                    response.setType("E");
                }

            }
            else {
                messageForCheckIfBoardingForLocationIATA = djpBoardingService.checkIfBoardingForLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation);
                if (StringUtils.isNotBlank(messageForCheckIfBoardingForLocationIATA)) {
                    response.setMessage(messageForCheckIfBoardingForLocationIATA);
                    response.setType("E");
                } else if (djpBoardingService.checkIfBoardingFromLocationIATA(qrForFioriRequest, null, null, null,djpPassLocation)) {
                    response.setMessage("Boarding kartın kalkış istasyonu "+djpPassLocation.getName()+" değildir. Yolcunun hizmet kullanım hakkı yoktur");
                    response.setType("E");
                } else {
                    accessRight = djpTokenService.getAccessRights(qrForFioriRequest.getPnrCode().substring(36, 39),djpPassLocation,isServiceUsageWithMembership);
                    fillResponseByAccessRight(response, accessRight, qrForFioriRequest, false,UsageMethod.TOURNIQUET);
                }
            }
        }
    }

    @Override
    public UserWsDto fillUserWsDTOByQrForMobile(String fieldsDto, DjpUniqueTokenModel token, CustomerData customerData, QrForFioriRequest qrForFioriRequest,QrForMobileModel qrForMobileModel,QrForCardModel qrForCardModel,QrForExternalModel qrForExternalModel,DjpPassLocationsModel djpPassLocation) {
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        if (Objects.nonNull(customerData)) {
//            TODO: this data mapper needs some explain as hybris provide data mapper to populate some attributes
//            userWs = dataMapper.map(customerData, UserWsDto.class, fieldsDto);
            userWs.setEmail(customerData.getEmail());

            if (customerData.getProfilePicture() != null) {
                userWs.setImageUrl(customerData.getProfilePicture().getUrl());
            }
        }

        if(StringUtils.isNotBlank(qrForFioriRequest.getProductCategory())) {
            if (Objects.nonNull(customerData)) {
                userWs.setUserProducts(djpRemainingUsageFacade.getUserProducts(customerData.getUid(),qrForFioriRequest.getProductCategory(),null,null,qrForMobileModel,null,null,qrForFioriRequest.getQrType()));
            } else {
                userWs.setUserProducts(djpRemainingUsageFacade.getUserProducts("", qrForFioriRequest.getProductCategory(), null, null, null, qrForExternalModel,null,qrForFioriRequest.getQrType()));
            }
        }

        //----check token productCategory and use for-----//
        if(Objects.nonNull(token) && token.getOrderCode().contains("FAST_TRACK") && StringUtils.isNotBlank(qrForFioriRequest.getProductCategory())
                && !(qrForFioriRequest.getProductCategory().equalsIgnoreCase("010501"))){
            if(CollectionUtils.isNotEmpty(userWs.getUserProducts())){
                userWs.setUserProducts(new ArrayList<>());
            }
            result.setType("E");
            result.setMessage("FAST TRACK hizmetine ait QR ile "+ qrTypeEnumHashMap.get(qrForFioriRequest.getQrType()) +" işlemi yapılamaz.");
            if(Objects.nonNull(qrForMobileModel)) {
                djpQRService.saveUnapprovedPass(qrForMobileModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForMobile");
            }
            userWs.setResult(result);
            return userWs;
        }
        //-----------------end----------------------------//

        if(Objects.nonNull(token))
            userWs.setOrderCode(token.getOrderCode());
        if(Objects.nonNull(qrForMobileModel)) {
            Boolean haveDjpPass = djpCustomerService.checkHaveDjpPass(customerData.getUid());
            if (Boolean.FALSE.equals(haveDjpPass) && !djpPassLocation.getLocationID().equals(DjpCommerceservicesConstants.DjpPassLocations.DJP)) {
                result.setType("E");
                result.setMessage("Müşteri iGA PASS Üyelik Paketi Sahibi Olmadığı için sadece iGA Havalimanında servislerden faydalanabilir.");
                djpQRService.saveUnapprovedPass(qrForMobileModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForFiori");
            }
        }
        if(userWs.getUserProducts().isEmpty()){
            result.setType("E");

            if (Objects.nonNull(customerData)) {
                boolean isCarParking = DjpCommerceservicesConstants.CARPARKING_CODE.equals(qrForFioriRequest.getQrType());
                String isShowDynamicMessage = showDynamicHasNotRemainingMessage;
                if(Boolean.TRUE.equals(isCarParking) && Boolean.TRUE.equals(DjpCoreConstants.ExternalCompany.ENABLED.equals(isShowDynamicMessage))){
                    result.setMessage(djpUtils.generateErrorResultMessageForCarParking(customerData.getUid()));
                } else {
                    result.setMessage("Kullanıcıya Ait Ürün Kullanım Hakkı Yoktur!");
                }
            }else {
                result.setMessage("Kullanıcıya Ait Ürün Kullanım Hakkı Yoktur!");
            }
            if(Objects.nonNull(qrForMobileModel)) {
                djpQRService.saveUnapprovedPass(qrForMobileModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForMobile");
            }
            if(Objects.nonNull(qrForCardModel)) {
                djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForCard");
            }
            if(Objects.nonNull(qrForExternalModel)) {
                djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType()," QrForExternal");
            }
        }
        if(Objects.nonNull(qrForMobileModel)){
            if(!"E".equals(result.getType())) {
                if (!qrForMobileModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2)).startsWith(qrTypeEnum.getCode()) || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2))))) {
                    result.setType("E");
                    result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                    djpQRService.saveUnapprovedPass(qrForMobileModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForMobile");
                } else {
                    result.setType("S");
                    result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
                }
            }
        }

        if (Objects.nonNull(qrForCardModel)) {
            if (!userWs.getUserProducts().isEmpty()) {
                result.setType("S");
                result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
            } else {
                result.setType("E");
                result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                djpQRService.saveUnapprovedPass(qrForCardModel.getCardId(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForCard");
            }
        }

        if(Objects.nonNull(qrForExternalModel)){
            if (userWs.getUserProducts().isEmpty()) {
                result.setType("E");
                result.setMessage("Kullanıcıya Ait Ürün Kullanım Hakkı Yoktur!");
            }else {
                if (!qrForExternalModel.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2)).startsWith(qrTypeEnum.getCode()) || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getQrType().substring(0, 2))))) {
                    result.setType("E");
                    result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                    djpQRService.saveUnapprovedPass(qrForExternalModel.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
                } else {
                    result.setType("S");
                    result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
                }
            }
        }

        userWs.setResult(result);
        return userWs;
    }

    @Override
    public UserWsDto fillUserWsDTOForSomeBody(QrForFioriRequest qrForFioriRequest) {
        QrForVoucherModel voucherForSomeBody = null;
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        OrderModel orderByToken = djpRemainingUsageService.getOrderByToken(qrForFioriRequest.getTokenKey());

        String phoneNumber = orderByToken.getDjpOtherCustomer().getPhone();
        phoneNumber = phoneNumber.contains(" ") ? phoneNumber.replace(" ", "") : phoneNumber;
        CustomerModel customerForSomeBody = djpCustomerService.findCustomerByUid(phoneNumber);
        fillOtherCustomerInfo(orderByToken.getDjpOtherCustomer(), userWs,customerForSomeBody);
        String uid = "";
        if(StringUtils.isNotBlank(qrForFioriRequest.getProductCategory())) {
            if(Objects.isNull(customerForSomeBody)){
                uid = orderByToken.getUser().getUid();
            }else {
                uid = customerForSomeBody.getUid();
            }
            voucherForSomeBody = djpQRService.validQrCodeForDjp(qrForFioriRequest.getTokenKey());
            userWs.setUserProducts(djpRemainingUsageFacade.getForSomeBodyProducts(uid, qrForFioriRequest.getProductCategory(), voucherForSomeBody,qrForFioriRequest.getQrType()));
        }
        if(userWs.getUserProducts().isEmpty()){
            result.setType("E");
            result.setMessage(voucherForSomeBody.getTokenKey().getCode() +" ilgili kullanım kodunun '"+qrTypeEnumHashMap.get(qrForFioriRequest.getQrType())+"' Hizmet Kullanım Hakkı Yoktur!");
            if(Objects.nonNull(voucherForSomeBody)) {
                djpQRService.saveUnapprovedPass(voucherForSomeBody.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForMobile");
            }
        }else {
            String qrType = qrForFioriRequest.getQrType();
            boolean validThisServiceFloor = false;
            if (voucherForSomeBody.getWhereIsValidQr().contains(QrType.valueOf(DjpCommerceservicesConstants.ALL))) {
                validThisServiceFloor = true;
            } else {
                if (qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_TRANSFER_STARTWITH) && !voucherForSomeBody.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                    validThisServiceFloor = voucherForSomeBody.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType).equals(qrTypeEnum.getCode()) || qrTypeEnum.equals(QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_C1) || qrTypeEnum.equals(QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_E1));
                } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_INTERNATIONAL_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_DOMESTIC_STARTWITH)) && !voucherForSomeBody.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.BUGGY_CODE)))) {
                    validThisServiceFloor = voucherForSomeBody.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_DEPARTURE_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_ARRIVAL_STARTWITH)) && !voucherForSomeBody.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                    validThisServiceFloor = voucherForSomeBody.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                } else {
                    validThisServiceFloor = voucherForSomeBody.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 2)).startsWith(qrTypeEnum.getCode())
                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 2))));
                }
            }
            if (!validThisServiceFloor) {
                result.setType("E");
                result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                djpQRService.saveUnapprovedPass(voucherForSomeBody.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForMobile");
            } else {
                result.setType("S");
                result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
            }
        }
        userWs.setResult(result);
        return userWs;
    }

    private void fillOtherCustomerInfo(DjpOtherCustomerModel djpOtherCustomer, UserWsDto userWsDto, CustomerModel customerForSomeBody) {
        userWsDto.setFirstName(djpOtherCustomer.getFirstName());
        userWsDto.setLastName(djpOtherCustomer.getLastName());
        userWsDto.setName(djpOtherCustomer.getFirstName()+" "+djpOtherCustomer.getLastName());
        userWsDto.setPhone(djpOtherCustomer.getPhone());
        userWsDto.setEmail(djpOtherCustomer.getEmail());
        if (Objects.nonNull(customerForSomeBody)) {
            if (Objects.nonNull(customerForSomeBody.getGender())) {
                userWsDto.setGender(customerForSomeBody.getGender().getCode().equals("KADIN") || customerForSomeBody.getGender().getCode().equals("WOMAN") ? "1" : "2");
            }
            userWsDto.setKvkkCheck(Objects.nonNull(customerForSomeBody.getSmsPreference()) && Objects.nonNull(customerForSomeBody.getEmailPreference()) ? customerForSomeBody.getSmsPreference() && customerForSomeBody.getEmailPreference() : false);
            if (Objects.nonNull(customerForSomeBody.getCountry())) {
                List<DialingCodeData> allDialingCodes = djpInternalizationFacade.getAllDialingCodes();
                Optional<DialingCodeData> first = allDialingCodes.stream().filter(dialingCodeData -> dialingCodeData.getIsoCode().equals(customerForSomeBody.getCountry().getIsoCode())).findFirst();
                userWsDto.setPhonePrefix(first.isPresent() ? "+" + first.get().getDialingCode() : null);
            }
        }

    }

    @Override
    public UserWsDto fillUserWsDTOForExternalCustomer(QrForFioriRequest qrForFioriRequest) {
        QrForVoucherModel voucherForExternalCustomer = null;
        DjpUniqueTokenModel token = null;
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        if(StringUtils.isNotBlank(qrForFioriRequest.getProductCategory())) {
            voucherForExternalCustomer = djpQRService.validQrCodeForDjp(qrForFioriRequest.getTokenKey());
            token = voucherForExternalCustomer.getTokenKey();
            fillExternalCustomerByVoucher(token, userWs);
            userWs.setUserProducts(djpRemainingUsageFacade.getUserProducts("", qrForFioriRequest.getProductCategory(), null, null, null, null,voucherForExternalCustomer,qrForFioriRequest.getQrType()));
        }
        if(Objects.nonNull(token))
            userWs.setOrderCode(token.getOrderCode());
        if (userWs.getUserProducts().isEmpty()) {
            result.setType("E");
            result.setMessage("Kullanıcıya Ait Ürün Kullanım Hakkı Yoktur!");
            if (Objects.nonNull(voucherForExternalCustomer)) {
                djpQRService.saveUnapprovedPass(voucherForExternalCustomer.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForVoucher");
            }
        } else if (Objects.nonNull(voucherForExternalCustomer)) {
            String qrType = qrForFioriRequest.getQrType();
            boolean validThisServiceFloor = false;
            if (voucherForExternalCustomer.getWhereIsValidQr().contains(QrType.valueOf(DjpCommerceservicesConstants.ALL))) {
                validThisServiceFloor = true;
            } else {
                if(DjpCommerceservicesConstants.BAGAJ_SARMA_CATEGORY.equals(qrForFioriRequest.getProductCategory())){
                    validThisServiceFloor = true;
                }else {
                    if (qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_TRANSFER_STARTWITH) && !voucherForExternalCustomer.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType).equals(qrTypeEnum.getCode()) || qrTypeEnum.equals(QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_C1) || qrTypeEnum.equals(QrType.FAST_TRACK_ARRIVAL_INTERNATIONAL_E1));
                    } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_INTERNATIONAL_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BUGGY_DOMESTIC_STARTWITH)) && !voucherForExternalCustomer.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.BUGGY_CODE)))) {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));

                    } else if ((qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_DEPARTURE_STARTWITH) || qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.FAST_TRACK_ARRIVAL_STARTWITH)) && !voucherForExternalCustomer.getWhereIsValidQr().contains(QrType.valueOf(qrTypeEnumHashMap.get(DjpCommerceservicesConstants.FAST_TRACK_CODE)))) {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                    } else if (qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.BAVUL_KAPLAMA_STARTWITH)) {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 4)).startsWith(qrTypeEnum.getCode())
                                || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 4))));
                    } else if(qrTypeEnumHashMap.get(qrType).contains(DjpCommerceservicesConstants.EMANET_DOLABI_STARTWITH)) {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType).startsWith(qrTypeEnum.getCode()));
                    } else {
                        validThisServiceFloor = voucherForExternalCustomer.getWhereIsValidQr().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrType.substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrType.substring(0, 2))));
                    }
                }
            }
            if (!validThisServiceFloor) {
                result.setType("E");
                result.setMessage("Kullanıcı Bu Hizmeti Kullanamaz!");
                djpQRService.saveUnapprovedPass(voucherForExternalCustomer.getTokenKey().getCode(), result.getMessage(), qrForFioriRequest.getQrType(), "QrForExternal");
            } else {
                result.setType("S");
                result.setMessage("Kullanıcı Hizmeti Kullanabilir.");
            }
        }


        userWs.setResult(result);
        return userWs;
    }

    private void fillExternalCustomerByVoucher(DjpUniqueTokenModel token, UserWsDto userWs) {
        DjpExternalCompanyOrderTableModel djpExternalOrderByIDandCustomer = djpExternalCompanyService.findDjpExternalOrderByIdAndCustomer(token.getOrderCode(), token.getUserUid());
        userWs.setUid(token.getUserUid());
        if(Objects.nonNull(djpExternalOrderByIDandCustomer)){
            DjpExternalCustomerModel customer = djpExternalOrderByIDandCustomer.getCustomer();
            if(StringUtils.isNotBlank(customer.getName()) && StringUtils.isNotBlank(customer.getLastName())){
                userWs.setFirstName(customer.getName());
                userWs.setLastName(customer.getLastName());
                userWs.setName(customer.getName()+" "+customer.getLastName());
            }
            if(StringUtils.isNotBlank(customer.getPhoneNumber())){
                userWs.setPhone(customer.getPhoneNumber());
            }
            if(StringUtils.isNotBlank(customer.getEmail())){
                userWs.setEmail(customer.getEmail());
            }
        }

    }

    @Override
    public UserWsDto fillUserWsDTOForSingleCustomCode(QrForFioriRequest qrForFioriRequest, Date date) {
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        try {
            QrForVoucherModel voucherModel = djpQRService.validQrCodeForDjp(qrForFioriRequest.getTokenKey());
            boolean isNewSale = Boolean.TRUE.equals(qrForFioriRequest.getNewSale());
            return generateResponseForPackageVoucher(qrForFioriRequest.getQrType(),voucherModel,date,isNewSale);
        }catch (Exception ex){
            LOG.error("fillUserWsDTOForSingleCustomCode has an error:",ex);
            result.setType("E");
            result.setMessage("Sistemsel Bir Hata Oluştu!");
            userWs.setResult(result);
            return userWs;
        }
    }

    private UserWsDto generateResponseForPackageVoucher(String qrType, QrForVoucherModel voucher, Date date, boolean isNewSale) throws ParseException {
        UserWsDto userWs = new UserWsDto();
        FioriStatusResponse result = new FioriStatusResponse();
        boolean hasNotClaimed =false;
        boolean isValidStartDate = true;
        boolean isValidExpireDate = true;
        DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid = getSingleCampaignInfoAndCheckValid(qrType, voucher);
        hasNotClaimed = checkIfClaimedForSingleCampaignVoucher(singleCampaignInfoAndCheckValid, hasNotClaimed);
        isValidStartDate = checkIfSingleCampaignVoucherStartDate(singleCampaignInfoAndCheckValid,date);
        isValidExpireDate = checkIfSingleCampaignVoucherExpireDate(singleCampaignInfoAndCheckValid,date);
        if (Objects.isNull(singleCampaignInfoAndCheckValid)) {
            result.setType("E");
            result.setMessage("Lütfen geçerli bir qr kod giriniz.");
            result.setContractID(voucher.getContractId());
            userWs.setResult(result);
            djpQRService.saveUnapprovedPass(voucher.getTokenKey().getCode(), result.getMessage(), qrType, "QrForVoucher");
            return userWs;
        }else if (Boolean.TRUE.equals(hasNotClaimed)) {
            result.setType("E");
            result.setMessage("Voucher'ın Kullanım Hakkı Kalmamıştır. Lütfen geçerli bir qr kod giriniz.");
            result.setContractID(voucher.getContractId());
            userWs.setResult(result);
            djpQRService.saveUnapprovedPass(voucher.getTokenKey().getCode(), result.getMessage(), qrType, "QrForVoucher");
            return userWs;
        } else if (Boolean.FALSE.equals(isValidStartDate)) {
            result.setType("E");
            result.setMessage("Voucher'ın kullanım başlangıç tarihi uygun değil!,Başlangıç Tarihi:" + singleCampaignInfoAndCheckValid.getStartDate().toString());
            result.setContractID(voucher.getContractId());
            djpQRService.saveUnapprovedPass(voucher.getTokenKey().getCode(), result.getMessage(), qrType, "QrForVoucher");
            userWs.setResult(result);
            return userWs;
        } else if (Boolean.FALSE.equals(isValidExpireDate)) {
            result.setType("E");
            result.setMessage("Voucher'ın kullanım tarihi geçmiş!,Bitiş Tarihi:" + singleCampaignInfoAndCheckValid.getExpireDate().toString());
            result.setContractID(voucher.getContractId());
            djpQRService.saveUnapprovedPass(voucher.getTokenKey().getCode(), result.getMessage(), qrType, "QrForVoucher");
            userWs.setResult(result);
            return userWs;
        } else {
            if (Boolean.TRUE.equals(voucher.getPromotionVoucher())) {
                if (Boolean.TRUE.equals(isNewSale)) {
                    result.setMessage("Voucher'ınız Yeni Satışta İndirim İçin Kullanılabilir.");
                    result.setContractID(singleCampaignInfoAndCheckValid.getContractID());
                    result.setType("S");
                    userWs.setUserProducts(getSaleWsDataList(singleCampaignInfoAndCheckValid));
                    userWs.setResult(result);
                    return userWs;
                } else {
                    result.setMessage("Voucher'ınız Yeni Satışta İndirim İçin Kullandırınız.");
                    result.setContractID(singleCampaignInfoAndCheckValid.getContractID());
                    result.setType("E");
                    userWs.setResult(result);
                    return userWs;
                }
            }
            result.setType("S");
            result.setMessage("Voucher Kullanıma Uygundur!");
            result.setContractID(singleCampaignInfoAndCheckValid.getContractID());
            userWs.setUserProducts(getSaleWsDataList(singleCampaignInfoAndCheckValid));
            userWs.setResult(result);
            return userWs;
        }

    }

    private List<DjpProductSaleWsData> getSaleWsDataList(DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid) {
        List<DjpProductSaleWsData> djpProductSaleWsDataList = new ArrayList<DjpProductSaleWsData>();
        DjpProductSaleWsData djpProductSaleWsData = new DjpProductSaleWsData();

        String code = singleCampaignInfoAndCheckValid.getWhereIsValid().getCode();
        String packageName = code.replace("_"," ");
        djpProductSaleWsData.setPackageName(DjpCoreConstants.WebServiceParameters.DJP+" "+packageName);
        djpProductSaleWsData.setGuestCount(0);
        djpProductSaleWsDataList.add(djpProductSaleWsData);
        return djpProductSaleWsDataList;
    }

    private boolean checkIfClaimedForSingleCampaignVoucher(DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid, boolean hasNotClaimed) {
        if(Objects.nonNull(singleCampaignInfoAndCheckValid)) {
            hasNotClaimed = singleCampaignInfoAndCheckValid.getClaimed().equals(singleCampaignInfoAndCheckValid.getUsed());
        }
        return hasNotClaimed;
    }

    private boolean checkIfSingleCampaignVoucherStartDate(DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid, Date date) throws ParseException {
        if(Objects.nonNull(singleCampaignInfoAndCheckValid)) {
            int startDateCompare = DateUtil.dateCompare(singleCampaignInfoAndCheckValid.getStartDate(), date);
            boolean isValid = startDateCompare > 0;
            return !isValid;
        }else {
            return Boolean.FALSE;
        }
    }

    private boolean checkIfSingleCampaignVoucherExpireDate(DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid, Date date) throws ParseException {
        if(Objects.nonNull(singleCampaignInfoAndCheckValid)) {
            int expireDateCompare = DateUtil.dateCompare(singleCampaignInfoAndCheckValid.getExpireDate(), date);
            boolean isValid = expireDateCompare < 0;
            return !isValid;
        }else {
            return Boolean.FALSE;
        }
    }

    private DjpSingleCampaignCodeInfoModel getSingleCampaignInfoAndCheckValid(String qrType, QrForVoucherModel voucher) {
        DjpSingleCampaignCodeInfoModel campaignCodeInfo = null;
        Collection<DjpSingleCampaignCodeInfoModel> campaignDetails = voucher.getCampaignDetails();
        for (DjpSingleCampaignCodeInfoModel campaignDetail : campaignDetails) {
            boolean isValid = qrTypeEnumHashMap.get(qrType).startsWith(campaignDetail.getWhereIsValid().getCode());
            if(Boolean.TRUE.equals(isValid)){
                campaignCodeInfo = campaignDetail;
                break;
            }
        }
        return campaignCodeInfo;

    }

    @Override
    public void fillResponseByAccessRight(FioriStatusResponse response, List<DjpAccessRightsModel> accessRightList, QrForFioriRequest qrForFioriRequest, boolean couldNotReadBoardingPass, UsageMethod usageMethod) {
        if (accessRightList.isEmpty()) {
            LOG.info("validateaccessboardingpass access right null 202");
            response.setMessage("Belirtilen Havayolu ve Sınıf için IGA PREMIUM Anlaşması Bulunamadı");
            response.setType("E");
        }
        boolean newSale = Objects.nonNull(qrForFioriRequest.getNewSale()) && qrForFioriRequest.getNewSale();
        if(StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && qrForFioriRequest.getPnrCode().length() < 50){
            List<ContractedPackagedData> contractedPackagedDatas = new ArrayList<ContractedPackagedData>();
            for (DjpAccessRightsModel accessRight : accessRightList) {
                if ("1".equals(accessRight.getAccessRight())
                        && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                    LOG.info("validateboardingpass access right 1 200");
                    successResponseForAccessRight(response, contractedPackagedDatas, accessRight,qrForFioriRequest.getBoardingType(),qrForFioriRequest,usageMethod);
                    response.setCustomerType(qrForFioriRequest.getPnrCode().substring(0,2).toUpperCase()+"/" + accessRight.getAirlineName());
                    break;
                }else {
                    LOG.info("validateaccessboardingpass has not access right 201 ");
                    response.setType("E");
                    response.setMessage("Müşteri " + accessRight.getAirlineName() + " yolcusudur. Geçiş izni yoktur");
                    response.setCustomerType(qrForFioriRequest.getPnrCode().startsWith("*PP") || qrForFioriRequest.getPnrCode().startsWith("*pp") ? qrForFioriRequest.getPnrCode().substring(1, 3).toUpperCase() : qrForFioriRequest.getPnrCode().substring(0, 2).toUpperCase() + "/" + accessRight.getAirlineName());
                }
            }

        }
        else if(newSale){
            List<ContractedPackagedData> contractedPackagedDatas = new ArrayList<ContractedPackagedData>();
            for (DjpAccessRightsModel accessRight : accessRightList) {
                if (Objects.nonNull(accessRight.getWhichRange()) && Objects.nonNull(accessRight.getCharactersToSearch())) {
                    String[] split = accessRight.getWhichRange().split(",");
                    String charactersToSearch = accessRight.getCharactersToSearch();
                    boolean checkCharacter = false;
                    if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode())) {
                        if (qrForFioriRequest.getPnrCode().length() >= Integer.valueOf(split[1])) {
                            if (qrForFioriRequest.getPnrCode().substring(Integer.valueOf(split[0]), Integer.valueOf(split[1])).equals(charactersToSearch)) {
                                checkCharacter = true;
                            }
                        }
                    }
                    if ("0".equals(accessRight.getAccessRight()) && checkCharacter && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                        LOG.info("validateaccessboardingpass access right");
                        successResponseForAccessRight(response, contractedPackagedDatas, accessRight,qrForFioriRequest.getBoardingType(),qrForFioriRequest,usageMethod);
                        break;
                    }
                    else {
                        LOG.info("validateaccessboardingpass has not access right 201 ");
                        response.setType("E");
                        response.setMessage("Müşteri " + accessRight.getAirlineName() + " & " + accessRight.getClassName() + " yolcusudur. Geçiş izni yoktur");
                        response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
                    }
                }
                else {
                    LOG.info("validateaccessboardingpass has not access right 201 ");
                    response.setType("E");
                    response.setMessage("Müşteri " + accessRight.getAirlineName() + " & " + accessRight.getClassName() + " yolcusudur. Geçiş izni yoktur");
                    response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
                }
            }
        } else {
            boolean hasCheck = accessRightList.stream().anyMatch(djpAccessRightsModel -> Objects.isNull(djpAccessRightsModel.getWhichRange()) && Objects.isNull(djpAccessRightsModel.getCharactersToSearch()) && Objects.isNull(djpAccessRightsModel.getWhichBeforeLastCharacterPlace()));
            boolean isQuickServiceUsage = accessRightList.
                    stream()
                    .anyMatch(djpAccessRightsModel -> (Objects.nonNull(djpAccessRightsModel.getWhichRange()) || Objects.nonNull(djpAccessRightsModel.getWhichBeforeLastCharacterPlace()))
                            && Objects.nonNull(djpAccessRightsModel.getCharactersToSearch())
                            && Boolean.TRUE.equals(djpAccessRightsModel.getQuickServiceUsage()));
            String businessCharacter = StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) ? qrForFioriRequest.getPnrCode().substring(47, 48) : qrForFioriRequest.getClassCode().toUpperCase();
            boolean isBusinessCustomer = accessRightList.stream().filter(djpAccessRightsModel -> djpAccessRightsModel.getClassName().equals(DjpCommerceservicesConstants.BUSINESS_BOARDING_CLASS_CODE)).anyMatch(djpAccessRightsModel -> djpAccessRightsModel.getClassCode().equals(businessCharacter));
            List<ContractedPackagedData> contractedPackagedDatas = new ArrayList<ContractedPackagedData>();
            if (hasCheck && !isQuickServiceUsage) {
                AtomicBoolean isThereContract = new AtomicBoolean(false);
                accessRightList.stream()
                        .forEach(djpAccessRightsModel -> {
                            if ((Objects.isNull(djpAccessRightsModel.getWhichRange()) && Objects.isNull(djpAccessRightsModel.getCharactersToSearch()))) {
                                DjpPassLocationsModel djpPassLocation = null;
                                Optional<DjpPassLocationsModel> locations = djpAccessRightsModel.getValidAtWhichLocation().stream().filter(djpPassLocationModel -> djpPassLocationModel.getLocationID().equals(qrForFioriRequest.getLocationID())).findFirst();
                                if(locations.isPresent()){
                                    djpPassLocation = locations.get();
                                }
                                ContractedCompaniesModel contractedCompaniesModel = djpQRService.getContractedCompany(djpAccessRightsModel.getUid(),qrForFioriRequest.getBoardingType(),djpPassLocation);
                                ContractedPackagedData contractedPackagedData = new ContractedPackagedData();
                                contractedPackagedData.setClassNames(djpAccessRightsModel.getClassName());
                                contractedPackagedData.setAdultGuestCount(djpAccessRightsModel.getAdultGuestCount());
                                contractedPackagedData.setChildGuestCount(djpAccessRightsModel.getChildGuestCount());
                                String contractID = Objects.nonNull(contractedCompaniesModel) ? contractedCompaniesModel.getContractId() : null;
                                contractedPackagedData.setContractID(contractID);
                                if (djpAccessRightsModel.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                                    String operatingCarrierDesignator = qrForFioriRequest.getPnrCode().substring(36, 39);
                                    operatingCarrierDesignator = operatingCarrierDesignator.contains(" ") ? operatingCarrierDesignator.replace(" ", "") : operatingCarrierDesignator;
                                    if (operatingCarrierDesignator.equals("SQ")) {
                                        String singaporeClassCode = qrForFioriRequest.getPnrCode().substring(47, 48);
                                        if (singaporeClassCode.equals(djpAccessRightsModel.getClassCode().substring(0, 1)) && singaporeClassCode.equals("J")) {
                                            String isSingaporeMembership = qrForFioriRequest.getPnrCode().substring(131, 132);
                                            if (isSingaporeMembership.equals(djpAccessRightsModel.getClassCode().substring(1, 2)) && isSingaporeMembership.equals("Y")) {
                                                String singaporeMembershipCode;
                                                try {
                                                    singaporeMembershipCode = qrForFioriRequest.getPnrCode().substring(153, 154);
                                                } catch (Exception e) {
                                                    singaporeMembershipCode = "0";
                                                }
                                                if (singaporeMembershipCode.equals(djpAccessRightsModel.getClassCode().substring(2, 3)) || djpAccessRightsModel.getClassCode().substring(2, 3).equals("0")){
                                                    contractedPackagedDatas.add(contractedPackagedData);
                                                }
                                            } else if (isSingaporeMembership.equals(djpAccessRightsModel.getClassCode().substring(1, 2)) && isSingaporeMembership.equals("N")) {
                                                contractedPackagedDatas.add(contractedPackagedData);
                                            }
                                        } else {
                                            if (!singaporeClassCode.equals("J") && !djpAccessRightsModel.getClassCode().substring(0, 1).equals("J")) {
                                                String isSingaporeMembership = qrForFioriRequest.getPnrCode().substring(131, 132);
                                                if (isSingaporeMembership.equals("Y")) {
                                                    String singaporeMembershipCode;
                                                    try {
                                                        singaporeMembershipCode = qrForFioriRequest.getPnrCode().substring(153, 154);
                                                    } catch (Exception e) {
                                                        singaporeMembershipCode = "0";
                                                    }
                                                    if (singaporeMembershipCode.equals(djpAccessRightsModel.getClassCode().substring(2, 3))){
                                                        contractedPackagedDatas.add(contractedPackagedData);
                                                    }
                                                }
                                            }
                                        }
                                    } else if (!contractedPackagedData.getClassNames().equals(DjpCommerceservicesConstants.BUSINESS_BOARDING_CLASS_CODE)) {
                                        if (contractedPackagedDatas.isEmpty()) {
                                            contractedPackagedDatas.add(contractedPackagedData);
                                        } else {
                                            if (!contractedPackagedDatas.stream().anyMatch(packaged -> packaged.getClassNames().equals(contractedPackagedData.getClassNames()))) {
                                                contractedPackagedDatas.add(contractedPackagedData);
                                            }
                                        }
                                    } else {
                                        if (contractedPackagedDatas.isEmpty() && isBusinessCustomer) {
                                            contractedPackagedDatas.add(contractedPackagedData);
                                        } else {
                                            if (!contractedPackagedDatas.stream().anyMatch(packaged -> packaged.getClassNames().equals(contractedPackagedData.getClassNames())) && isBusinessCustomer) {
                                                contractedPackagedDatas.add(contractedPackagedData);
                                            }
                                        }
                                    }
                                }
                                if (!qrForFioriRequest.getDevice().equals("F")){
                                    if (djpAccessRightsModel.getUid().equals(contractedCompaniesModel.getCompanyName() + "_" + businessCharacter) && (djpAccessRightsModel.getBoardingTypes().contains(QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0,2)))) || djpAccessRightsModel.getBoardingTypes().contains(QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))))){
                                        djpBoardingService.saveAccessBoardingPassForQuickServiceUsage(qrForFioriRequest,contractedCompaniesModel.getContractId(),djpAccessRightsModel.getClassName(),djpPassLocation,usageMethod);
                                        isThereContract.set(true);
                                    }
                                }
                            }
                        });

                if (contractedPackagedDatas.isEmpty()) {
                    LOG.info("validateaccessboardingpass has not access right 201 ");
                    response.setType("E");
                    response.setMessage(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()) + " Hizmeti için geçiş izni yoktur");
                    response.setCustomerType("HM/");
                } else {
                    response.setType("S");
                    response.setMessage("Müşteri bu yolculardan birisi ise geçiş iznine sahiptir.");
                    response.setCustomerType("HM/");
                    response.setPackages(contractedPackagedDatas);
                }
                if (!qrForFioriRequest.getDevice().equals("F")){
                    if (!isThereContract.get()){
                        response.setType("E");
                        response.setMessage("Anlaşmalı havayoulu için geçiş izni bulunamadı. Lütfen desk personeline başvurunuz.");
                        response.setPackages(Collections.emptyList());
                    }
                }
            } else {
                AtomicBoolean isThereContracts = new AtomicBoolean(false);
                accessRightList.forEach(x -> {
                    DjpPassLocationsModel djpPassLocation = null;
                    Optional<DjpPassLocationsModel> locations = x.getValidAtWhichLocation().stream().filter(djpPassLocationModel -> djpPassLocationModel.getLocationID().equals(qrForFioriRequest.getLocationID())).findFirst();
                    if(locations.isPresent()){
                        djpPassLocation = locations.get();
                    }
                    ContractedCompaniesModel companiesModel = djpQRService.getContractedCompany(x.getUid(), qrForFioriRequest.getBoardingType(), djpPassLocation);
                    if (Objects.nonNull(companiesModel)){
                        if (x.getUid().equals(companiesModel.getCompanyName() + "_" + businessCharacter)){
                            isThereContracts.set(true);
                        }
                    } else {
                        isThereContracts.set(false);
                    }

                });

                if (!isThereContracts.get() && !qrForFioriRequest.getDevice().equals("F")){
                    response.setType("E");
                    response.setMessage("Lütfen Desk Personeline Başvurunuz.");
                } else {
                    for (DjpAccessRightsModel accessRight : accessRightList) {
                        String classCodeOnPnrCode = StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) ? qrForFioriRequest.getPnrCode().substring(47, 48) : qrForFioriRequest.getClassCode().toUpperCase();
                        if(isQuickServiceUsage){
                            Optional<DjpAccessRightsModel> quickServiceOptional = accessRightList.stream().filter(djpAccessRightsModel -> djpAccessRightsModel.getQuickServiceUsage()).findFirst();
                            if(quickServiceOptional.isPresent()){
                                accessRight = quickServiceOptional.get();

                                if ((Objects.nonNull(accessRight.getWhichRange()) || Objects.nonNull(accessRight.getWhichBeforeLastCharacterPlace())) && Objects.nonNull(accessRight.getCharactersToSearch())) {
                                    String charactersToSearch = accessRight.getCharactersToSearch();
                                    boolean checkCharacter = Boolean.FALSE;
                                    if(Objects.nonNull(accessRight.getWhichRange())) {
                                        String[] split = accessRight.getWhichRange().split(",");
                                        if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode())) {
                                            if (qrForFioriRequest.getPnrCode().length() >= Integer.valueOf(split[1])) {
                                                if (qrForFioriRequest.getPnrCode().substring(Integer.valueOf(split[0]), Integer.valueOf(split[1])).equals(charactersToSearch)) {
                                                    checkCharacter = Boolean.TRUE;
                                                }
                                            }
                                        }
                                    }
                                    if(Objects.nonNull(accessRight.getWhichBeforeLastCharacterPlace())){
                                        int whichBeforeLastCharacterPlace = accessRight.getWhichBeforeLastCharacterPlace();
                                        String pnrCode = qrForFioriRequest.getPnrCode();
                                        if(pnrCode.charAt(pnrCode.length() - 1) == ' ') {
                                            do {
                                                pnrCode = pnrCode.substring(0, pnrCode.length() - 1);
                                            } while (pnrCode.charAt(pnrCode.length() - 1) == ' ');
                                        }
                                        if (pnrCode.substring(pnrCode.length() - whichBeforeLastCharacterPlace, pnrCode.length() - whichBeforeLastCharacterPlace + charactersToSearch.length()).equals(charactersToSearch)) {
                                            checkCharacter = Boolean.TRUE;
                                        }
                                    }
                                    if ("1".equals(accessRight.getAccessRight()) && checkCharacter && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                                        LOG.info("validateaccessboardingpass access right");
                                        successResponseForAccessRight(response, contractedPackagedDatas, accessRight, qrForFioriRequest.getBoardingType(), qrForFioriRequest,usageMethod);
                                        break;
                                    } else {
                                        LOG.info("validateaccessboardingpass has not access right 201 ");
                                        response.setType("E");
                                        response.setMessage("Yolcu " + accessRight.getAirlineName() + " yolcusudur. ve anlaşma kapsamında '" + qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()) + "' geçiş hakkı yoktur.");
                                        response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());

                                    }
                                }
                            }else {
                                LOG.info("validateaccessboardingpass has not access right 201 ");
                                response.setType("E");
                                response.setMessage("Müşteri " + accessRight.getAirlineName() + " & " + accessRight.getClassName() + " yolcusudur. Geçiş izni yoktur");
                                response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
                                break;
                            }
                        }
                        else{
                            if (Objects.nonNull(accessRight.getWhichRange()) && Objects.nonNull(accessRight.getCharactersToSearch())) {
                                String[] split = accessRight.getWhichRange().split(",");
                                String charactersToSearch = accessRight.getCharactersToSearch();
                                String lastThirdCharacterInBp = StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && qrForFioriRequest.getPnrCode().trim().length() > 2
                                        ? qrForFioriRequest.getPnrCode().trim().substring(qrForFioriRequest.getPnrCode().trim().length()-3,qrForFioriRequest.getPnrCode().trim().length()-2)
                                        :"";
                                boolean checkCharacter = false;
                                if (StringUtils.isNotBlank(qrForFioriRequest.getPnrCode())) {
                                    if (qrForFioriRequest.getPnrCode().length() >= Integer.valueOf(split[1])) {
                                        if (qrForFioriRequest.getPnrCode().substring(Integer.valueOf(split[0]), Integer.valueOf(split[1])).equals(charactersToSearch)) {
                                            checkCharacter = true;
                                        }
                                    }
                                }
                                if (StringUtils.isNotBlank(lastThirdCharacterInBp) && charactersToSearch.equals(lastThirdCharacterInBp)  && accessRight.getAirlineCode().equals(EMIRATES_AIRLINE_CODE)){
                                    checkCharacter = true;
                                }
                                if ("1".equals(accessRight.getAccessRight()) && checkCharacter && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                                    LOG.info("validateaccessboardingpass access right");
                                    successResponseForAccessRight(response, contractedPackagedDatas, accessRight, qrForFioriRequest.getBoardingType(), qrForFioriRequest,usageMethod);
                                    if (!accessRight.getAirlineCode().equals(EMIRATES_AIRLINE_CODE)){
                                        break;
                                    }
                                } else {
                                    if (classCodeOnPnrCode.equals(accessRight.getClassCode()) && "1".equals(accessRight.getAccessRight()) && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                            || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                                        LOG.info("validateboardingpass access right 1 200");
                                        successResponseForAccessRight(response, contractedPackagedDatas, accessRight, qrForFioriRequest.getBoardingType(), qrForFioriRequest,usageMethod);
                                        if (!accessRight.getAirlineCode().equals(EMIRATES_AIRLINE_CODE)){
                                            break;
                                        }
                                    } else {
                                        LOG.info("validateaccessboardingpass has not access right 201 ");
                                        response.setType("E");
                                        response.setMessage("Müşteri " + accessRight.getAirlineName() + " & " + accessRight.getClassName() + " yolcusudur. Geçiş izni yoktur");
                                        response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
                                    }
                                }
                            } else if (Objects.isNull(accessRight.getWhichRange()) && Objects.isNull(accessRight.getCharactersToSearch())) {
                                if (classCodeOnPnrCode.equals(accessRight.getClassCode())
                                        && "1".equals(accessRight.getAccessRight())
                                        && accessRight.getBoardingTypes().stream().anyMatch(qrTypeEnum -> qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2)).startsWith(qrTypeEnum.getCode())
                                        || qrTypeEnum.getCode().startsWith(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0, 2))))) {
                                    LOG.info("validateboardingpass access right 1 200");
                                    successResponseForAccessRight(response, contractedPackagedDatas, accessRight, qrForFioriRequest.getBoardingType(), qrForFioriRequest,usageMethod);
                                    break;
                                }
                            }
                        }
                    }
                }


                if (response.getPackages()!= null && EMIRATES_AIRLINE_CODE.equals(accessRightList.get(0).getAirlineCode()) && response.getPackages().size()==1 ){
                    response.setPackages(new ArrayList<>());
                    response.setType("E");
                    response.setMessage("Yolcu " + accessRightList.get(0).getAirlineName() + "  yolcusudur. Ancak girilen boardingin anlaşma kapsamında geçiş izni yoktur.");
                    response.setCustomerType("HM/" + accessRightList.get(0).getAirlineName());
                } else if(response.getPackages()!=null && !response.getPackages().isEmpty()){
                    response.setType("S");
                    response.setMessage("Müşteri " + accessRightList.get(0).getAirlineName() + " yolcusudur. Geçiş iznine sahiptir.");
                    response.setCustomerType("HM/" + accessRightList.get(0).getAirlineName());
                }
            }
        }
        if ("E".equals(response.getType()) && !newSale) {
            if (couldNotReadBoardingPass) {
                String pnrCode = qrForFioriRequest.getDate()
                        +"/"+qrForFioriRequest.getFlightNumber().toUpperCase()
                        +"/"+qrForFioriRequest.getSeatCode()
                        +"/"+qrForFioriRequest.getFromAirportIATA()
                        +"/"+qrForFioriRequest.getToAirportIATA()
                        +"/"+qrForFioriRequest.getFromAirportIATA()
                        +"/"+qrForFioriRequest.getClassCode().toUpperCase();
                djpQRService.saveUnapprovedPass(pnrCode, response.getMessage(), qrForFioriRequest.getBoardingType(), "DjpAccessBoarding");
            } else {
                djpQRService.saveUnapprovedPass(qrForFioriRequest.getPnrCode(), response.getMessage(), qrForFioriRequest.getBoardingType(), "DjpAccessBoarding");
            }
        }
    }

    @Override
    public UserWsDto getUserWsDto(CustomerData customerData) {
        UserWsDto userWs = new UserWsDto();
        if(Objects.nonNull(customerData)) {
            userWs.setTitleCode(customerData.getTitleCode());
            userWs.setTitle(customerData.getTitle());
            userWs.setFirstName(customerData.getFirstName());
            userWs.setLastName(customerData.getLastName());
            userWs.setDisplayUid(customerData.getDisplayUid());
            userWs.setCustomerId(customerData.getCustomerId());
            userWs.setDeactivationDate(customerData.getDeactivationDate());
            userWs.setEmail(customerData.getEmail());
            userWs.setBirthdate(customerData.getBirthdate());
            userWs.setNationality(customerData.getNationality());
            userWs.setGender(customerData.getGender());
            userWs.setPackageDesc(customerData.getPackageDesc());
            userWs.setGuestofPackage(customerData.getGuestofPackage());
            return userWs;
        }
        return null;
    }

    private void successResponseForAccessRight(FioriStatusResponse response, List<ContractedPackagedData> contractedPackagedDatas, DjpAccessRightsModel accessRight, String boardingType, QrForFioriRequest qrForFioriRequest, UsageMethod usageMethod) {
        DjpPassLocationsModel djpPassLocation = null;
        Optional<DjpPassLocationsModel> locations = accessRight.getValidAtWhichLocation().stream().filter(djpPassLocationModel -> djpPassLocationModel.getLocationID().equals(qrForFioriRequest.getLocationID())).findFirst();
        String businessCharacter = StringUtils.isNotBlank(qrForFioriRequest.getPnrCode()) && qrForFioriRequest.getPnrCode().length() > 50  ? qrForFioriRequest.getPnrCode().substring(47, 48) : qrForFioriRequest.getClassCode().toUpperCase();
        boolean isBusinessCustomer=accessRight.getClassName().equals(DjpCommerceservicesConstants.BUSINESS_BOARDING_CLASS_CODE) && accessRight
                .getClassCode().equals(businessCharacter);
        if(locations.isPresent()){
            djpPassLocation = locations.get();
        }
        ContractedCompaniesModel contractedCompaniesModel = djpQRService.getContractedCompany(accessRight.getUid(),boardingType,djpPassLocation);
        if(accessRight.getQuickServiceUsage() && (!accessRight.getAirlineCode().equals(EMIRATES_AIRLINE_CODE) || (accessRight.getAirlineCode().equals(EMIRATES_AIRLINE_CODE) && (boardingType.equals(200001) || boardingType.equals(200002) || boardingType.equals(200003)))) ){

            response.setQuickServiceUsage(accessRight.getQuickServiceUsage());
            response.setType("S");
            response.setMessage("Yolcu " + accessRight.getAirlineName() + " yolcusudur. ve anlaşma kapsamında '"+qrTypeEnumHashMap.get(boardingType)+"' geçiş hakkı vardır.");
            response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
            ContractedPackagedData contractedPackagedData = new ContractedPackagedData();
            contractedPackagedData.setClassNames(accessRight.getClassName());
            contractedPackagedData.setContractID(Objects.nonNull(contractedCompaniesModel) ? contractedCompaniesModel.getContractId() : null);
            contractedPackagedDatas.add(contractedPackagedData);
            response.setPackages(contractedPackagedDatas);

            djpBoardingService.saveAccessBoardingPassForQuickServiceUsage(qrForFioriRequest,contractedCompaniesModel.getContractId(),accessRight.getClassName(),djpPassLocation,usageMethod);
        }else {
            response.setType("S");
            response.setMessage("Müşteri " + accessRight.getAirlineName() + " & " + accessRight.getClassName() + " yolcusudur. Geçiş iznine sahiptir.");
            response.setCustomerType("HM/" + accessRight.getAirlineName() + " & " + accessRight.getClassName());
            ContractedPackagedData contractedPackagedData = new ContractedPackagedData();
            contractedPackagedData.setClassNames(accessRight.getClassName());
            contractedPackagedData.setAdultGuestCount(accessRight.getAdultGuestCount());
            contractedPackagedData.setChildGuestCount(accessRight.getChildGuestCount());
            contractedPackagedData.setContractID(Objects.nonNull(contractedCompaniesModel) ? contractedCompaniesModel.getContractId() : null);
            contractedPackagedData.setRequiredSameCompanyForGuest(accessRight.getRequiredSameCompanyForGuest());
                /*contractedPackagedDatas.add(contractedPackagedData);
                response.setPackages(contractedPackagedDatas);*/
            if (!contractedPackagedData.getClassNames().equals(DjpCommerceservicesConstants.BUSINESS_BOARDING_CLASS_CODE)) {
                if (contractedPackagedDatas.isEmpty()) {
                    contractedPackagedDatas.add(contractedPackagedData);
                    response.setPackages(contractedPackagedDatas);
                } else {
                    if (!contractedPackagedDatas.stream().anyMatch(packaged -> packaged.getClassNames().equals(contractedPackagedData.getClassNames()))) {
                        contractedPackagedDatas.add(contractedPackagedData);
                        response.setPackages(contractedPackagedDatas);
                    }
                }
            } else {
                if (contractedPackagedDatas.isEmpty() && isBusinessCustomer) {
                    contractedPackagedDatas.add(contractedPackagedData);
                    response.setPackages(contractedPackagedDatas);
                } else {
                    if (!contractedPackagedDatas.stream().anyMatch(packaged -> packaged.getClassNames().equals(contractedPackagedData.getClassNames())) && isBusinessCustomer) {
                        contractedPackagedDatas.add(contractedPackagedData);
                        response.setPackages(contractedPackagedDatas);
                    }
                }
            }
            if (!qrForFioriRequest.getDevice().equals("F")){
                if (accessRight.getUid().equals(contractedCompaniesModel.getCompanyName() + "_" + businessCharacter) && (accessRight.getBoardingTypes().contains(QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType().substring(0,2)))) || accessRight.getBoardingTypes().contains(QrType.valueOf(qrTypeEnumHashMap.get(qrForFioriRequest.getBoardingType()))))){
                    djpBoardingService.saveAccessBoardingPassForQuickServiceUsage(qrForFioriRequest,contractedCompaniesModel.getContractId(),accessRight.getClassName(),djpPassLocation,usageMethod);
                }
            }
        }

    }

    @Override
    public FioriStatusResponse passForRemainingUsageWithVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel) {
        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status, DjpCoreConstants.TOKEN_TYPE_FOR_SOME_BODY, qrPassRequest.getProductCategory(), null, qrPassRequest, false, null, null);
        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
        if (!hasErrorOnResponse) {
            Set<QrType> whereUsed = new HashSet<>();
            if (!qrForVoucherModel.getWhereUsed().isEmpty()) {
                whereUsed.addAll(qrForVoucherModel.getWhereUsed());
            }
            qrForVoucherModel.setUsageDate(date);
            whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
            qrForVoucherModel.setWhereUsed(whereUsed);
            qrForVoucherModel.setWhichLocation(djpPassLocation);
            modelService.saveOrUpdate(qrForVoucherModel);
            status.setMessage("Hizmet Kullandırıldı.");
            status.setType("S");
            djpQRFacade.saveCodeUsage(qrForVoucherModel,qrPassRequest,date,djpPassLocation,null,0D);
        }
        return status;
    }

    @Override
    public FioriStatusResponse passForExternalCustomerWithVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel) {
        DjpExternalCustomerModel customerByUid = djpExternalCompanyService.findDjpExternalCustomerByID(qrForVoucherModel.getTokenKey().getUserUid(),qrForVoucherModel.getCompanyId());
        if(Objects.isNull(customerByUid)){
            status.setMessage("QR'a ait kullanıcı DJP PASS de bulunamadı. tokenKey:" + qrPassRequest.getTokenKey());
            status.setType("E");
            return status;
        }

        djpRemainingUsageFacade.addUsedInDjpRemainingUsage(status,qrForVoucherModel.getTokenKey().getUserUid(), qrPassRequest.getProductCategory(),null,qrPassRequest,false,null, null);
        boolean hasErrorOnResponse = Objects.nonNull(status.getType()) && status.getType().equals("E");
        if (!hasErrorOnResponse) {
            Set<QrType> whereUsed = new HashSet<>();
            if (!qrForVoucherModel.getWhereUsed().isEmpty()) {
                whereUsed.addAll(qrForVoucherModel.getWhereUsed());
            }
            qrForVoucherModel.setUsageDate(date);

            if(CollectionUtils.isNotEmpty(qrPassRequest.getUsages())){
                Integer used = qrForVoucherModel.getUsed();
                for (MultiUsagesData usage : qrPassRequest.getUsages()) {
                    whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(usage.getQrType())));
                    used = used + Integer.valueOf(usage.getQuantity());
                }
                qrForVoucherModel.setWhereUsed(whereUsed);
                qrForVoucherModel.setUsed(used);
            }else if(StringUtils.isNotBlank(qrPassRequest.getQrType())) {
                whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
                qrForVoucherModel.setWhereUsed(whereUsed);
                qrForVoucherModel.setUsed(qrForVoucherModel.getUsed() + 1);
            }

            qrForVoucherModel.setWhichLocation(djpPassLocation);
            qrForVoucherModel.setUsageForOverSixtyFiveOld(Boolean.TRUE.equals(qrPassRequest.getIsOverSixtyFive()) ? Boolean.TRUE : Boolean.FALSE);
            modelService.saveOrUpdate(qrForVoucherModel);

            if(CollectionUtils.isNotEmpty(qrPassRequest.getUsages())){
                for (MultiUsagesData usage : qrPassRequest.getUsages()) {
                    if(stockedServiceUsagePoint.contains(usage.getQrType())){
                        djpRemainingUsageFacade.startDjpUsageSendToErpProcess(qrPassRequest.getQrType(), qrForVoucherModel, null, null,null,null);
                        break;
                    }
                }
            }

            djpQRFacade.saveCodeUsage(qrForVoucherModel,qrPassRequest,date,djpPassLocation,null,0D);
            status.setMessage("Hizmet Kullandırıldı.");
            status.setType("S");
        }
        return status;
    }

    @Override
    public FioriStatusResponse passForSingleCampaignCodeVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel) {
        boolean isStockedServiceUsaged = StringUtils.isNotBlank(qrPassRequest.getQrType()) && stockedServiceUsagePoint.contains(qrPassRequest.getQrType());
        String qrType = qrPassRequest.getQrType();
        DjpSingleCampaignCodeInfoModel singleCampaignInfoAndCheckValid = getSingleCampaignInfoAndCheckValid(qrType, qrForVoucherModel);
        if (singleCampaignInfoAndCheckValid.getClaimed().equals(singleCampaignInfoAndCheckValid.getUsed())) {
            status.setMessage("Voucher'ın Kullanım Hakkı Kalmamıştır.");
            status.setType("E");
            return status;
        }

        // check order and paid price , before the set djp campaign code usages
        if(Boolean.TRUE.equals(qrForVoucherModel.getPromotionVoucher())){
            status = djpQRFacade.fillDjpVoucherCodeUsagesAndStatus(qrForVoucherModel,qrPassRequest,date,djpPassLocation,singleCampaignInfoAndCheckValid);
            if("E".equals(status.getType())){
                return status;
            }
        }
        Set<QrType> whereUsed = new HashSet<>();
        if (!qrForVoucherModel.getWhereUsed().isEmpty()) {
            whereUsed.addAll(qrForVoucherModel.getWhereUsed());
        }

        qrForVoucherModel.setUsageDate(date);
        whereUsed.add(QrType.valueOf(qrTypeEnumHashMap.get(qrPassRequest.getQrType())));
        qrForVoucherModel.setWhereUsed(whereUsed);
        qrForVoucherModel.setUsed(qrForVoucherModel.getUsed() + 1);
        qrForVoucherModel.setWhichLocation(djpPassLocation);
        singleCampaignInfoAndCheckValid.setUsed(singleCampaignInfoAndCheckValid.getUsed() + 1);
        modelService.saveOrUpdate(singleCampaignInfoAndCheckValid);
        modelService.saveOrUpdate(qrForVoucherModel);

        djpQRFacade.startDjpUsageSendToErpProcess(qrPassRequest,qrForVoucherModel,isStockedServiceUsaged);

        status.setMessage("Hizmet Kullandırıldı.");
        status.setType("S");
        return status;
    }

    private void setOneMoreAreaOnUserDto(CustomerModel customerModel, UserWsDto userWs) {
        userWs.setEmail(customerModel.getMail());
        if (Objects.nonNull(customerModel.getGender())) {
            userWs.setGender(customerModel.getGender().getCode().equals(WOMAN_TR) || customerModel.getGender().getCode().equals(WOMAN_EN) ? "1" : "2");
        }
        userWs.setKvkkCheck(Objects.nonNull(customerModel.getSmsPreference()) && Objects.nonNull(customerModel.getEmailPreference()) ?
                customerModel.getSmsPreference() && customerModel.getEmailPreference() : false);
        if (Objects.nonNull(customerModel.getCountry())) {
            List<DialingCodeData> allDialingCodes = djpInternalizationFacade.getAllDialingCodes();
            Optional<DialingCodeData> first = allDialingCodes.stream().filter(dialingCodeData -> dialingCodeData.getIsoCode().equals(customerModel.getCountry().getIsoCode())).findFirst();
            userWs.setPhonePrefix(first.isPresent() ? "+" + first.get().getDialingCode() : null);
        }
    }

}
