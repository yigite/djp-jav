package com.djp.strategy.impl;

import com.djp.core.model.BaseStoreModel;
import com.djp.strategy.BaseStoreSelectorStrategy;

public class DefaultBaseStoreSelectorStrategy implements BaseStoreSelectorStrategy {
    @Override
    public BaseStoreModel getCurrentBaseStore() {
        return null;
    }
}
