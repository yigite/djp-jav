package com.djp.strategy;

import com.djp.core.model.BaseStoreModel;

public interface BaseStoreSelectorStrategy {
    BaseStoreModel getCurrentBaseStore();
}
