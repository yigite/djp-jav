package com.djp.strategy;

import com.djp.core.enums.UsageMethod;
import com.djp.core.model.*;
import com.djp.core.model.QrForVoucherModel;
import com.djp.data.CustomerData;
import com.djp.dto.UserWsDto;
import com.djp.request.QrForFioriRequest;
import com.djp.response.FioriStatusResponse;
import com.djp.request.QrPassRequest;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public interface DjpPremiumServiceUsageStrategy {
    UserWsDto fillUserWsDTOByQrForFiori(String fieldsDto, DjpUniqueTokenModel token, CustomerData customerData,
                                        QrForFioriRequest qrForFioriRequest, QrForFioriModel qrForFioriModel, DjpPassLocationsModel djpPassLocationsModel);
    FioriStatusResponse passForRemainingUsageWithVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel);

    FioriStatusResponse passForExternalCustomerWithVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel);

    FioriStatusResponse passForSingleCampaignCodeVoucher(QrPassRequest qrPassRequest, FioriStatusResponse status, Date date, DjpPassLocationsModel djpPassLocation, QrForVoucherModel qrForVoucherModel);

    void beforeSaveQuickBoardingCheckFlightAndAccess(QrForFioriRequest qrForFioriRequest, FioriStatusResponse response, DjpPassLocationsModel djpPassLocation,boolean isServiceUsageWithMembership);

    UserWsDto fillUserWsDTOByQrForMobile(String fieldsDto, DjpUniqueTokenModel token, CustomerData customerData, QrForFioriRequest qrForFioriRequest,QrForMobileModel qrForMobileModel,QrForCardModel qrForCardModel,QrForExternalModel qrForExternalModel,DjpPassLocationsModel djpPassLocation);

    UserWsDto fillUserWsDTOForSomeBody(QrForFioriRequest qrForFioriRequest);

    UserWsDto fillUserWsDTOForExternalCustomer(QrForFioriRequest qrForFioriRequest);

    UserWsDto fillUserWsDTOForSingleCustomCode(QrForFioriRequest qrForFioriRequest, Date currentDate);

    void fillResponseByAccessRight(FioriStatusResponse response, List<DjpAccessRightsModel> accessRight, QrForFioriRequest qrForFioriRequest, boolean b, UsageMethod usageMethod);

    UserWsDto getUserWsDto(CustomerData customerData);
}
