package com.djp.strategy;

import com.djp.core.enums.QrType;

public interface DjpQrTypeValidateStrategy {
    String validateWhereUsedQr(QrType qrTypeEnum, final String qrUniqueId);
}
