package com.djp.response;

import com.djp.data.ManuelRecordsListData;
import com.djp.response.FioriStatusResponse;

import java.io.Serializable;
import java.util.List;

public  class ManuelRecordsResponse  implements Serializable
{
    private static final long serialVersionUID = 1L;

    private List<ManuelRecordsListData> ManuelRecordsList;

    private FioriStatusResponse serviceResult;

    public ManuelRecordsResponse()
    {
        // default constructor
    }

    public void setManuelRecordsList(final List<ManuelRecordsListData> ManuelRecordsList)
    {
        this.ManuelRecordsList = ManuelRecordsList;
    }



    public List<ManuelRecordsListData> getManuelRecordsList()
    {
        return ManuelRecordsList;
    }



    public void setServiceResult(final FioriStatusResponse serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public FioriStatusResponse getServiceResult()
    {
        return serviceResult;
    }



}