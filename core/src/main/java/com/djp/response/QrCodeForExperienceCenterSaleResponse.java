package com.djp.response;

import com.djp.data.ExperienceCenterGameSaleListItem;

import java.io.Serializable;
import java.util.List;

public class QrCodeForExperienceCenterSaleResponse implements Serializable {


    private static final long serialVersionUID = 1L;
    private List<ExperienceCenterGameSaleListItem> gameList;
    private String qrImage;
    private String qrToken;
    private String createDate;
    private String expireDate;
    private Boolean isActive;

    public QrCodeForExperienceCenterSaleResponse()
    {
        // default constructor
    }

    public void setGameList(final List<ExperienceCenterGameSaleListItem> gameList)
    {
        this.gameList = gameList;
    }



    public List<ExperienceCenterGameSaleListItem> getGameList()
    {
        return gameList;
    }



    public void setQrImage(final String qrImage)
    {
        this.qrImage = qrImage;
    }



    public String getQrImage()
    {
        return qrImage;
    }



    public void setQrToken(final String qrToken)
    {
        this.qrToken = qrToken;
    }



    public String getQrToken()
    {
        return qrToken;
    }



    public void setCreateDate(final String createDate)
    {
        this.createDate = createDate;
    }



    public String getCreateDate()
    {
        return createDate;
    }



    public void setExpireDate(final String expireDate)
    {
        this.expireDate = expireDate;
    }

    public String getExpireDate()
    {
        return expireDate;
    }

    public void setIsActive(final Boolean isActive)
    {
        this.isActive = isActive;
    }
    public Boolean getIsActive()
    {
        return isActive;
    }

}
