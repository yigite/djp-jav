package com.djp.response;

import com.djp.dto.ContractedPackagedData;

import java.io.Serializable;
import java.util.List;

public  class FioriStatusResponse  implements Serializable {


    private static final long serialVersionUID = 1L;
    private String type;
    private String message;
    private String customerType;
    private List<ContractedPackagedData> packages;
    private String contractID;
    private Boolean quickServiceUsage;

    public FioriStatusResponse()
    {
        // default constructor
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public List<ContractedPackagedData> getPackages() {
        return packages;
    }

    public void setPackages(List<ContractedPackagedData> packages) {
        this.packages = packages;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public Boolean getQuickServiceUsage() {
        return quickServiceUsage;
    }

    public void setQuickServiceUsage(Boolean quickServiceUsage) {
        this.quickServiceUsage = quickServiceUsage;
    }
}

