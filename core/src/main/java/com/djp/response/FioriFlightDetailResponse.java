package com.djp.response;

import com.djp.data.FioriFlightDetailData;

import java.io.Serializable;

public class FioriFlightDetailResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private FioriStatusResponse serviceResult;

    private FioriFlightDetailData flightInfo;

    public FioriFlightDetailResponse()
    {
        // default constructor
    }



    public void setServiceResult(final FioriStatusResponse serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public FioriStatusResponse getServiceResult()
    {
        return serviceResult;
    }



    public void setFlightInfo(final FioriFlightDetailData flightInfo)
    {
        this.flightInfo = flightInfo;
    }



    public FioriFlightDetailData getFlightInfo()
    {
        return flightInfo;
    }
}
