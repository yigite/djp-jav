package com.djp.response;

import com.djp.request.BoardingPassRequest;

import java.io.Serializable;

public class BoardingPassResponse implements Serializable {

    private static final long serialVersionUID = 1L;


    private BoardingPassRequest boardingPassRequest;


    private FioriStatusResponse serviceResult;

    public BoardingPassResponse()
    {
        // default constructor
    }



    public void setBoardingPassRequest(final BoardingPassRequest boardingPassRequest)
    {
        this.boardingPassRequest = boardingPassRequest;
    }



    public BoardingPassRequest getBoardingPassRequest()
    {
        return boardingPassRequest;
    }



    public void setServiceResult(final FioriStatusResponse serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public FioriStatusResponse getServiceResult()
    {
        return serviceResult;
    }

}
