package com.djp.response;

import com.djp.data.NurusServiceLoginResponseCustomerData;
import com.djp.data.NurusServiceLoginResponseUserData;

import java.util.List;

public class NurusServiceLoginResponse {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponse.Response</code> property defined at extension <code>djpcommercewebservices</code>. */

    private Boolean Response;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponse.ResponseDescription</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String ResponseDescription;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponse.User</code> property defined at extension <code>djpcommercewebservices</code>. */

    private List<NurusServiceLoginResponseUserData> User;

    /** <i>Generated property</i> for <code>NurusServiceLoginResponse.Customer</code> property defined at extension <code>djpcommercewebservices</code>. */

    private List<NurusServiceLoginResponseCustomerData> Customer;

    public NurusServiceLoginResponse()
    {
        // default constructor
    }



    public void setResponse(final Boolean Response)
    {
        this.Response = Response;
    }



    public Boolean getResponse()
    {
        return Response;
    }



    public void setResponseDescription(final String ResponseDescription)
    {
        this.ResponseDescription = ResponseDescription;
    }



    public String getResponseDescription()
    {
        return ResponseDescription;
    }



    public void setUser(final List<NurusServiceLoginResponseUserData> User)
    {
        this.User = User;
    }



    public List<NurusServiceLoginResponseUserData> getUser()
    {
        return User;
    }



    public void setCustomer(final List<NurusServiceLoginResponseCustomerData> Customer)
    {
        this.Customer = Customer;
    }



    public List<NurusServiceLoginResponseCustomerData> getCustomer()
    {
        return Customer;
    }



}
