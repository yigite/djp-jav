package com.djp.response;

import com.djp.dto.UserWsDto;

public class CheckCustomerOwnedServiceResponse {

    private String customerOwnedService;
    private UserWsDto customer;
    private String message;
    public CheckCustomerOwnedServiceResponse()
    {
        // default constructor
    }
    public void setCustomerOwnedService(final String customerOwnedService)
    {
        this.customerOwnedService = customerOwnedService;
    }
    public String getCustomerOwnedService()
    {
        return customerOwnedService;
    }

    public void setCustomer(final UserWsDto customer)
    {
        this.customer = customer;
    }

    public UserWsDto getCustomer()
    {
        return customer;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }



}
