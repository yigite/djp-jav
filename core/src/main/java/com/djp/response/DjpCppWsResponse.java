package com.djp.response;

import com.djp.rest.data.ServiceResultWsData;

public class DjpCppWsResponse {

    private String cppToken;


    private ServiceResultWsData result;

    public DjpCppWsResponse()
    {
        // default constructor
    }



    public void setCppToken(final String cppToken)
    {
        this.cppToken = cppToken;
    }



    public String getCppToken()
    {
        return cppToken;
    }



    public void setResult(final ServiceResultWsData result)
    {
        this.result = result;
    }



    public ServiceResultWsData getResult()
    {
        return result;
    }


}
