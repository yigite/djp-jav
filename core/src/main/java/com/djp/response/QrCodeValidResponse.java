package com.djp.response;

import com.djp.dto.DjpProductSaleWsData;

import java.io.Serializable;

public  class QrCodeValidResponse  implements Serializable{

    private static final long serialVersionUID = 1L;
    private String type;
    private String message;
    private String customerType;
    private String contractID;
    private Boolean promotionVoucher;

    private DjpProductSaleWsData packageData;

    public QrCodeValidResponse()
    {
        // default constructor
    }



    public void setType(final String type)
    {
        this.type = type;
    }



    public String getType()
    {
        return type;
    }



    public void setMessage(final String message)
    {
        this.message = message;
    }



    public String getMessage()
    {
        return message;
    }



    public void setCustomerType(final String customerType)
    {
        this.customerType = customerType;
    }



    public String getCustomerType()
    {
        return customerType;
    }



    public void setContractID(final String contractID)
    {
        this.contractID = contractID;
    }



    public String getContractID()
    {
        return contractID;
    }



    public void setPromotionVoucher(final Boolean promotionVoucher)
    {
        this.promotionVoucher = promotionVoucher;
    }



    public Boolean getPromotionVoucher()
    {
        return promotionVoucher;
    }



    public void setPackageData(final DjpProductSaleWsData packageData)
    {
        this.packageData = packageData;
    }



    public DjpProductSaleWsData getPackageData()
    {
        return packageData;
    }

}