package com.djp.response;

import com.djp.request.AccessBoardingPassRequest;

import java.io.Serializable;

public class AccessBoardingPassResponse implements Serializable {

    private static final long serialVersionUID = 1L;


    private AccessBoardingPassRequest boardingPassRequest;


    private FioriStatusResponse serviceResult;

    public AccessBoardingPassResponse()
    {
        // default constructor
    }



    public void setBoardingPassRequest(final AccessBoardingPassRequest boardingPassRequest)
    {
        this.boardingPassRequest = boardingPassRequest;
    }



    public AccessBoardingPassRequest getBoardingPassRequest()
    {
        return boardingPassRequest;
    }



    public void setServiceResult(final FioriStatusResponse serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public FioriStatusResponse getServiceResult()
    {
        return serviceResult;
    }


}
