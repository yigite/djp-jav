package com.djp.response;

import com.djp.data.DjpBoardingPassData;

import java.io.Serializable;
import java.util.List;

public class DjpBoardingPassesResponse implements Serializable
{
    private static final long serialVersionUID = 1L;
    private List<DjpBoardingPassData> boardingpasses;
    private FioriStatusResponse serviceResult;

    public DjpBoardingPassesResponse()
    {
        // default constructor
    }



    public void setBoardingpasses(final List<DjpBoardingPassData> boardingpasses)
    {
        this.boardingpasses = boardingpasses;
    }



    public List<DjpBoardingPassData> getBoardingpasses()
    {
        return boardingpasses;
    }



    public void setServiceResult(final FioriStatusResponse serviceResult)
    {
        this.serviceResult = serviceResult;
    }



    public FioriStatusResponse getServiceResult()
    {
        return serviceResult;
    }



}
