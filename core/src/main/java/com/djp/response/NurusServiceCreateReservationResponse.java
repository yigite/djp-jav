package com.djp.response;

public class NurusServiceCreateReservationResponse {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationResponse.Response</code> property defined at extension <code>djpcommercewebservices</code>. */

    private Boolean Response;

    /** <i>Generated property</i> for <code>NurusServiceCreateReservationResponse.ResponseDescription</code> property defined at extension <code>djpcommercewebservices</code>. */

    private String ResponseDescription;

    public NurusServiceCreateReservationResponse()
    {
        // default constructor
    }



    public void setResponse(final Boolean Response)
    {
        this.Response = Response;
    }



    public Boolean getResponse()
    {
        return Response;
    }



    public void setResponseDescription(final String ResponseDescription)
    {
        this.ResponseDescription = ResponseDescription;
    }



    public String getResponseDescription()
    {
        return ResponseDescription;
    }



}
