package com.djp.response;


import com.djp.user.CurrentUser;

public class UserDetailResponse {

    private CurrentUser userDetail;

    public CurrentUser getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(CurrentUser userDetail) {
        this.userDetail = userDetail;
    }
}
