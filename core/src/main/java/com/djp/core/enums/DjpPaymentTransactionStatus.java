package com.djp.core.enums;

public enum DjpPaymentTransactionStatus {

    APPROVED,
    UNAPPROVED,
    CANCELLED_BY_ADMIN,
    CANCELLED_BY_PAYER,
    REVIEW_NEEDED,
    UNKNOWN;
}
