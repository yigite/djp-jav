package com.djp.core.enums;

public enum ProductInfoStatus {
    NONE,
    SUCCESS,
    INFO,
    WARNING,
    ERROR;
}
