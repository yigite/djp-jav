package com.djp.core.enums;

public enum PassengerType {
    ADULT("ZMMDJP_002_01","ADULT"),
    CHILD("ZMMDJP_002_02","CHILD"),
    BABY("ZMMDJP_002_03","BABY");

    private final String code;
    private final String name;

    PassengerType(String code,String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

}
