package com.djp.core.enums;

public enum ExportStatus {

    NOTEXPORTED,
    EXPORTED;
}
