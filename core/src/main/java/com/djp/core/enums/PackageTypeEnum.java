package com.djp.core.enums;

public enum PackageTypeEnum {
    ANNUAL,
    DAILY,
    SERVICE;
}
