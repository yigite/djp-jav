package com.djp.core.enums;

public enum OrderStatus {
    CREATED,
    ON_VALIDATION,
    COMPLETED,
    CANCELLED;
}
