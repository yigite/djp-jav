package com.djp.core.enums;

public enum SAPOrderStatus {

    NOT_SENT_TO_ERP,
    SENT_TO_ERP,
    CONFIRMED_FROM_ERP,
    CANCELLED_FROM_ERP;
}
