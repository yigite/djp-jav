package com.djp.core.enums;

public enum TourniquetBoardingStatus {
    USE("ExternalCompany"),
    COMPLETE("Bank"),
    IN("Bank"),
    OUT("Airline");

    private final String code;

    TourniquetBoardingStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
