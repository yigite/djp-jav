package com.djp.core.enums;

public enum ConsignmentEntryStatus {
    WAITING,
    PICKPACK,
    READY,
    SHIPPED,
    CANCELLED;
}
