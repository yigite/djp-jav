package com.djp.core.enums;

public enum RetentionState {
    PROCESSED("PROCESSED");

    private final String code;

    RetentionState(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
