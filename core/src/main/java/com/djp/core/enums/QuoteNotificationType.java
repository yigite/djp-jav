package com.djp.core.enums;

public enum QuoteNotificationType {
    EXPIRING_SOON,
    EXPIRED;
}
