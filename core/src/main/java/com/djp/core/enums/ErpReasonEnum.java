package com.djp.core.enums;

public enum ErpReasonEnum {

    APPROVE,
    CANCEL;
}
