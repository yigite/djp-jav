package com.djp.core.enums;

public enum GenderEnum {

    KADIN("KADIN"),
    ERKEK("ERKEK"),
    BELIRTMEKISTEMIYORUM("BELIRTMEKISTEMIYORUM");

    private final String code;

    GenderEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
