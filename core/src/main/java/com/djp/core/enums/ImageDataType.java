package com.djp.core.enums;

public enum ImageDataType {
    PRIMARY("PRIMARY"),
    GALLERY("GALLERY"),
    UNAPPROVED("UNAPPROVED");

    private final String code;

    ImageDataType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
