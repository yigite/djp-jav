package com.djp.core.enums;

public enum PriceRowChannel implements DjpEnumValue{
    DESKTOP("DESKTOP"),
    MOBILE("MOBILE");

    PriceRowChannel(String code) {
        this.code = code;
    }

    private static final String type = "PriceRowChannel";
    private String code;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getCode() {
        return code;
    }
}
