package com.djp.core.enums;

public enum StockLevelUpdateType {
    warehouse,
    customer_reserve,
    customer_release;
}
