package com.djp.core.enums;

public enum QuoteState {
    CREATED,
    DRAFT,
    SUBMITTED,
    OFFER,
    ORDERED,
    CANCELLED,
    EXPIRED;
}
