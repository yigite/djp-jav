package com.djp.core.enums;

public enum UsageMethod {
    DESK("DESK"),
    TOURNIQUET("TOURNIQUET");

    private final String code;

    UsageMethod(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
