package com.djp.core.enums;

public enum BarcodeType implements DjpEnumValue {
    QR("QR"),
    DATAMATRIX("DATAMATRIX");

    BarcodeType(String code) {
        this.code = code;
    }

    private static final String type = "BarcodeType";
    private String code;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getCode() {
        return code;
    }
}
