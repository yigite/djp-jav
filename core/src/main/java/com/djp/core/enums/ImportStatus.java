package com.djp.core.enums;

public enum ImportStatus {
    PROCESSING, COMPLETED;
}
