package com.djp.core.enums;

public enum MaritalStatusEnum {

    EVLI("EVLI"),
    BEKAR("BEKAR"),
    BELIRTMEK_ISTEMIYORUM("BELIRTMEK_ISTEMIYORUM");

    private final String code;

    MaritalStatusEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
