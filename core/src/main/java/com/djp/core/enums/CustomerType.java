package com.djp.core.enums;

public enum  CustomerType {

    GUEST("GUEST"),
    REGISTERED("REGISTERED");

    private final String code;

    CustomerType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
