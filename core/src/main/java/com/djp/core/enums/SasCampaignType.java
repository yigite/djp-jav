package com.djp.core.enums;

public enum SasCampaignType {
    DogumGunu("DogumGunu"),
    Hosgeldin("Hosgeldin");

    private final String code;

    SasCampaignType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
