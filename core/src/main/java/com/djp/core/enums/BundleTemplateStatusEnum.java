package com.djp.core.enums;

public enum BundleTemplateStatusEnum {

    check,
    unapproved,
    approved,
    archived;
}
