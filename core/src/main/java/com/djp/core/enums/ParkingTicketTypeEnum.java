package com.djp.core.enums;

public enum ParkingTicketTypeEnum {
    SHORTTERM,
    SUBSCRIBER,
    TICKETSALE,
    TECHSUPP;
}
