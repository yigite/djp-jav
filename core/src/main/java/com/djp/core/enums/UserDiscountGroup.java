package com.djp.core.enums;

public enum UserDiscountGroup {
    SPOT_ALICI("1","Toptan Alıcı"),
    TOPTAN_ALICI("2", "Spot Alıcı");

    private final String code;
    private final String name;

    UserDiscountGroup(String code, String name) {
        this.code = code;
        this.name= name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
