package com.djp.core.enums;

public enum PremiumPackagePassengerEnum {

    ADULT("ADULT"),
    CHILD("CHILD");

    private final String code;

    PremiumPackagePassengerEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
