package com.djp.core.enums;

public enum InStockStatus {
    forceInStock,
    forceOutOfStock,
    notSpecified;
}
