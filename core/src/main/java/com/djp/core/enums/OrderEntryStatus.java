package com.djp.core.enums;

public enum OrderEntryStatus {
    LIVING,
    DEAD;
}
