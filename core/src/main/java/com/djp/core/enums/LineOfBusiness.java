package com.djp.core.enums;

public enum LineOfBusiness implements DjpEnumValue{
    TRADE("trade"),
    BANK("bank"),
    INDUSTRY("industry"),
    BUILDING("building"),
    GOVERNMENT("government"),
    SERVICE("service");

    LineOfBusiness(String code) {
        this.code = code;
    }

    private static final String type = "LineOfBusiness";
    private String code;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getCode() {
        return code;
    }
}
