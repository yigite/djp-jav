package com.djp.core.enums;

public enum ProcessState {
    CREATED,
    RUNNING,
    WAITING,
    SUCCEEDED,
    FAILED,
    ERROR,
    SAPBACKENDERROR;
}
