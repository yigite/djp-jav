package com.djp.core.enums;

public enum PaymentStatus {

    NOTPAID,
    PARTPAID,
    PAID;
}
