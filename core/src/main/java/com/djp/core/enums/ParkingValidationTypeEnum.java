package com.djp.core.enums;

public enum ParkingValidationTypeEnum {
    TICKET,
    PLACE,
    MOBILE;
}
