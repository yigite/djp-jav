package com.djp.core.enums;

import java.io.Serializable;

public interface DjpEnumValue extends Serializable {
    String getType();

    String getCode();
}
