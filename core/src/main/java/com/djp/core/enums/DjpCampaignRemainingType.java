package com.djp.core.enums;

public enum DjpCampaignRemainingType {
    HOSGELDIN, BIRTHDATE;
}
