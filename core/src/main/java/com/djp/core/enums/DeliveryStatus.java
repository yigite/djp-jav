package com.djp.core.enums;

public enum DeliveryStatus {
    NOTSHIPPED,
    PARTSHIPPED,
    SHIPPED;
}
