package com.djp.core.enums;

public enum UserTaxGroup {
    CA("ca-taxes","CA Taxes"),
    DE("de-taxes","DE Taxes"),
    JP("jp-taxes","JP Taxes"),
    UK("uk-taxes","UK Taxes"),
    US("us-taxes","US Taxes"),
    TR("tr-taxes","TR Taxes");

    private final String code;
    private final String name;

    UserTaxGroup(String code,String name) {
        this.code = code;
        this.name= name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
