package com.djp.core.enums;

public enum ParkingVehicleClassTypeEnum {
    CLASS0,
    CLASS1,
    CLASS2,
    CLASS3;
}
