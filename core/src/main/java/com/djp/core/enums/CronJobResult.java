package com.djp.core.enums;

public enum CronJobResult {
    SUCCESS,
    FAILURE,
    ERROR,
    UNKNOWN;
}
