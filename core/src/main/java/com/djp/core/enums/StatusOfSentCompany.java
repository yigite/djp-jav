package com.djp.core.enums;

public enum StatusOfSentCompany {
    WILL_NOT_SENT_TO_COMPANY("WILL_NOT_SENT_TO_COMPANY"),
    SHOULD_BE_SENT_TO_COMPANY("SHOULD_BE_SENT_TO_COMPANY"),
    SENT_TO_COMPANY("SENT_TO_COMPANY");

    private final String code;

    StatusOfSentCompany(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
