package com.djp.core.enums;

public enum BooleanOperator {
    AND, OR;
}
