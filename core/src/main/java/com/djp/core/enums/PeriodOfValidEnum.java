package com.djp.core.enums;

public enum PeriodOfValidEnum {
    DAILY("DAILY"),
    WEEKLY("WEEKLY"),
    MONTHLY("MONTHLY"),
    YEARLY("YEARLY"),
    NDAYS("NDAYS");

    private final String code;

    PeriodOfValidEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
