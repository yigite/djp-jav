package com.djp.core.enums;

public enum JobLogLevel {
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    FATAL,
    UNKNOWN;
}
