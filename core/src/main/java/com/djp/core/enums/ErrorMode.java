package com.djp.core.enums;

public enum ErrorMode {
    FAIL,
    PAUSE,
    IGNORE;
}
