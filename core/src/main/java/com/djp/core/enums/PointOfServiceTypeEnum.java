package com.djp.core.enums;

public enum PointOfServiceTypeEnum {
    STORE,
    WAREHOUSE,
    POS;
}
