package com.djp.core.enums;

public enum ProductReferenceTypeEnum {
    ACCESSORIES,
    BASE_PRODUCT,
    CONSISTS_OF,
    DIFF_ORDERUNIT,
    FOLLOWUP,
    MANDATORY,
    SIMILAR,
    SELECT,
    SPAREPART,
    OTHERS,
    UPSELLING,
    CROSSELLING;
}
