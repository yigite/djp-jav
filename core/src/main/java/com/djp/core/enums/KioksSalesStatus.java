package com.djp.core.enums;

public enum KioksSalesStatus {
    WAITING_FOR_PAYMENT("WAITING_FOR_PAYMENT"),
    WAITING_FOR_APPROVE("WAITING_FOR_APPROVE"),
    MANAGER_APPROVED("MANAGER_APPROVED"),
    QR_CANCELED("QR_CANCELED"),
    PAYMENT_COMPLETE("PAYMENT_COMPLETE"),
    PAYMENT_NOT_RECEIVED("PAYMENT_NOT_RECEIVED"),
    STATUS_UNKNOWN("STATUS_UNKNOWN");

    private final String code;

    KioksSalesStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}