package com.djp.core.enums;

public enum ContractedType {
    ExternalCompany("ExternalCompany"),
    Bank("Bank"),
    Airline("Airline");

    private final String code;

    ContractedType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
