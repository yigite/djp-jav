package com.djp.core.enums;

public enum DjpPaymentTransactionStatusDetails {
    THREE_D_SECURE_AUTHENTICATION_REQUIRED,
    SUCCESFULL,
    UNKNOWN_CODE;
}
