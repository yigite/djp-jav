package com.djp.core.enums;

public enum RemainingType {
    GIFT("GIFT");

    private final String code;

    RemainingType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
