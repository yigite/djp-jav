package com.djp.core.enums;

public enum CronJobStatus {
    RUNNINGRESTART,
    RUNNING,
    PAUSED,
    FINISHED,
    ABORTED,
    UNKNOWN;
}
