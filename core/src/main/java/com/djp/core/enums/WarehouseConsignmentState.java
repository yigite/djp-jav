package com.djp.core.enums;

public enum WarehouseConsignmentState {
    COMPLETE,
    CANCEL,
    PARTIAL;
}
