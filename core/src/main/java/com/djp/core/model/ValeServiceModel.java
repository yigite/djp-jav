package com.djp.core.model;

import com.djp.core.enums.HizmetDurumEnum;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ValeService")
public class ValeServiceModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn
    private PlateMovementWsModel plateMovementWs;

    private BigDecimal additionalPrice;
    private String date;
    private String time;
    private String startTime;
    private String endTime;
    private String description;
    private Integer oid;
    private BigDecimal discount;
    private BigDecimal price;
    private BigDecimal totalPrice;
    private BigDecimal taxValueInTotalPrice;
    private HizmetDurumEnum serviceStatus;
    private boolean paid;

    @OneToOne
    private TaxModel tax;

    @OneToOne
    private ValeSubServiceModel valeSubService;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PlateMovementWsModel getPlateMovementWs() {
        return plateMovementWs;
    }

    public void setPlateMovementWs(PlateMovementWsModel plateMovementWs) {
        this.plateMovementWs = plateMovementWs;
    }

    public BigDecimal getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(BigDecimal additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTaxValueInTotalPrice() {
        return taxValueInTotalPrice;
    }

    public void setTaxValueInTotalPrice(BigDecimal taxValueInTotalPrice) {
        this.taxValueInTotalPrice = taxValueInTotalPrice;
    }

    public HizmetDurumEnum getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(HizmetDurumEnum serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public TaxModel getTax() {
        return tax;
    }

    public void setTax(TaxModel tax) {
        this.tax = tax;
    }

    public ValeSubServiceModel getValeSubService() {
        return valeSubService;
    }

    public void setValeSubService(ValeSubServiceModel valeSubService) {
        this.valeSubService = valeSubService;
    }
}
