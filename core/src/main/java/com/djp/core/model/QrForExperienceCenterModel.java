package com.djp.core.model;


import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "QrForExperienceCenter")
public class QrForExperienceCenterModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nameSurname;
    private String pnrCode;
    private Boolean isPaymentSuccess;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    private MediaModel qr;

    private QrType whereIsValidQr;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForExperienceCenterModel_QrType", joinColumns = @JoinColumn(name = "QrForExperienceCenterModel_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private List<QrType> usedHistory;

    private DjpPassLocationsModel whichLocation;

    @OneToMany
    private List<DjpRemainingUsageModel> remainingUsageList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public Boolean getIsPaymentSuccess() {
        return isPaymentSuccess;
    }

    public void setIsPaymentSuccess(Boolean paymentSuccess) {
        isPaymentSuccess = paymentSuccess;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public QrType getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(QrType whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public List<QrType> getUsedHistory() {
        return usedHistory;
    }

    public void setUsedHistory(List<QrType> usedHistory) {
        this.usedHistory = usedHistory;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public List<DjpRemainingUsageModel> getRemainingUsageList() {
        return remainingUsageList;
    }

    public void setRemainingUsageList(List<DjpRemainingUsageModel> remainingUsageList) {
        this.remainingUsageList = remainingUsageList;
    }
}
