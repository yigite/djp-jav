package com.djp.core.model;

import com.djp.core.enums.ProductReferenceTypeEnum;
import jakarta.persistence.*;

@Entity
@Table(name="ProductReference")
public class ProductReferenceModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String qualifier;
    private String description;
    private int quantity;
    private boolean preselected;
    private boolean active;
    private ProductReferenceTypeEnum referenceType;

    @ManyToOne
    @JoinColumn
    private ProductModel source;

    @OneToOne
    private ProductModel target;

	@OneToOne
	private MediaModel icon;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQualifier() {
		return qualifier;
	}

	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isPreselected() {
		return preselected;
	}

	public void setPreselected(boolean preselected) {
		this.preselected = preselected;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ProductReferenceTypeEnum getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ProductReferenceTypeEnum referenceType) {
		this.referenceType = referenceType;
	}

	public ProductModel getSource() {
		return source;
	}

	public void setSource(ProductModel source) {
		this.source = source;
	}

	public ProductModel getTarget() {
		return target;
	}

	public void setTarget(ProductModel target) {
		this.target = target;
	}

	public MediaModel getIcon() {
		return icon;
	}

	public void setIcon(MediaModel icon) {
		this.icon = icon;
	}
}
