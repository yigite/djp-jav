package com.djp.core.model;

import com.djp.core.enums.ErrorMode;
import com.djp.core.enums.JobLogLevel;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Jobs")
@Inheritance(strategy = InheritanceType.JOINED)
public class JobModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    private String code;

    private String alternativeDataSourceID;
    private String emailAddress;
    private String nodeGroup;
    private int nodeID;
    private int priority;
    private boolean active;
    private boolean changeRecordingEnabled;
    private boolean logToDatabase;
    private boolean logToFile;
    private boolean removeOnExit;
    private boolean requestAbort;
    private boolean requestAbortStep;
    private boolean retry;
    private boolean sendEmail;
    private boolean singleExecutable;
    private ErrorMode errorMode;
    private JobLogLevel logLevelDatabase;
    private JobLogLevel logLevelFile;

    @OneToOne
    private CurrencyModel sessionCurrency;
    @OneToOne
    private LanguageModel sessionLanguage;
    @OneToOne
    private UserModel sessionUser;

    /**
     * TODO: Mail entegrasyonundan sonra ui yapısına göre kurulabilir.
     * private RendererTemplateModel emailNotificationTemplate;
     */

    @OneToMany(mappedBy = "job", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CronJobModel> cronJobs;

    @OneToMany(mappedBy = "job", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<TriggerModel> triggers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAlternativeDataSourceID() {
        return alternativeDataSourceID;
    }

    public void setAlternativeDataSourceID(String alternativeDataSourceID) {
        this.alternativeDataSourceID = alternativeDataSourceID;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getNodeGroup() {
        return nodeGroup;
    }

    public void setNodeGroup(String nodeGroup) {
        this.nodeGroup = nodeGroup;
    }

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isChangeRecordingEnabled() {
        return changeRecordingEnabled;
    }

    public void setChangeRecordingEnabled(boolean changeRecordingEnabled) {
        this.changeRecordingEnabled = changeRecordingEnabled;
    }

    public boolean isLogToDatabase() {
        return logToDatabase;
    }

    public void setLogToDatabase(boolean logToDatabase) {
        this.logToDatabase = logToDatabase;
    }

    public boolean isLogToFile() {
        return logToFile;
    }

    public void setLogToFile(boolean logToFile) {
        this.logToFile = logToFile;
    }

    public boolean isRemoveOnExit() {
        return removeOnExit;
    }

    public void setRemoveOnExit(boolean removeOnExit) {
        this.removeOnExit = removeOnExit;
    }

    public boolean isRequestAbort() {
        return requestAbort;
    }

    public void setRequestAbort(boolean requestAbort) {
        this.requestAbort = requestAbort;
    }

    public boolean isRequestAbortStep() {
        return requestAbortStep;
    }

    public void setRequestAbortStep(boolean requestAbortStep) {
        this.requestAbortStep = requestAbortStep;
    }

    public boolean isRetry() {
        return retry;
    }

    public void setRetry(boolean retry) {
        this.retry = retry;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public boolean isSingleExecutable() {
        return singleExecutable;
    }

    public void setSingleExecutable(boolean singleExecutable) {
        this.singleExecutable = singleExecutable;
    }

    public ErrorMode getErrorMode() {
        return errorMode;
    }

    public void setErrorMode(ErrorMode errorMode) {
        this.errorMode = errorMode;
    }

    public JobLogLevel getLogLevelDatabase() {
        return logLevelDatabase;
    }

    public void setLogLevelDatabase(JobLogLevel logLevelDatabase) {
        this.logLevelDatabase = logLevelDatabase;
    }

    public JobLogLevel getLogLevelFile() {
        return logLevelFile;
    }

    public void setLogLevelFile(JobLogLevel logLevelFile) {
        this.logLevelFile = logLevelFile;
    }

    public CurrencyModel getSessionCurrency() {
        return sessionCurrency;
    }

    public void setSessionCurrency(CurrencyModel sessionCurrency) {
        this.sessionCurrency = sessionCurrency;
    }

    public LanguageModel getSessionLanguage() {
        return sessionLanguage;
    }

    public void setSessionLanguage(LanguageModel sessionLanguage) {
        this.sessionLanguage = sessionLanguage;
    }

    public UserModel getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(UserModel sessionUser) {
        this.sessionUser = sessionUser;
    }

    public Set<CronJobModel> getCronJobs() {
        return cronJobs;
    }

    public void setCronJobs(Set<CronJobModel> cronJobs) {
        this.cronJobs = cronJobs;
    }

    public Set<TriggerModel> getTriggers() {
        return triggers;
    }

    public void setTriggers(Set<TriggerModel> triggers) {
        this.triggers = triggers;
    }
}
