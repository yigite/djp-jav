package com.djp.core.model;


import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "DjpUser")
@Inheritance(strategy = InheritanceType.JOINED)
public class UserModel extends PrincipalModel{
    private String password;
    @OneToOne
    private LanguageModel sessionLanguage;
    @OneToOne
    private CurrencyModel sessionCurrency;
    private String loginDisabled;
    private Date lastLogin;
    private String contactInfos;
    private String phoneNumbers;
    private String customerType;
    private String tckn;
    private String vkn;
    /*
    private String europe1FactoryUDG;
    private String europe1FactoryUPG;
    private String europe1FactoryUTG;
    private String europe1Discounts;
    private String ownEurope1Discounts;
    private String tokens;
    private String passwordAnswer;
    private String passwordQuestion;*/

    @OneToOne(orphanRemoval = true)
    private AddressModel defaultPaymentAddress;

    @OneToOne(orphanRemoval = true)
    private AddressModel defaultShipmentAddress;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<CartModel> carts;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<OrderModel> orders;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<AddressModel> addresses;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<PaymentInfoModel> paymentInfos;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AbstractOrderModel> abstractOrderModels;

    @OneToMany(mappedBy = "assignee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<QuoteModel> assignedQuotes;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LanguageModel getSessionLanguage() {
        return sessionLanguage;
    }

    public void setSessionLanguage(LanguageModel sessionLanguage) {
        this.sessionLanguage = sessionLanguage;
    }

    public CurrencyModel getSessionCurrency() {
        return sessionCurrency;
    }

    public void setSessionCurrency(CurrencyModel sessionCurrency) {
        this.sessionCurrency = sessionCurrency;
    }

    public String getLoginDisabled() {
        return loginDisabled;
    }

    public void setLoginDisabled(String loginDisabled) {
        this.loginDisabled = loginDisabled;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getContactInfos() {
        return contactInfos;
    }

    public void setContactInfos(String contactInfos) {
        this.contactInfos = contactInfos;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getVkn() {
        return vkn;
    }

    public void setVkn(String vkn) {
        this.vkn = vkn;
    }

    public AddressModel getDefaultPaymentAddress() {
        return defaultPaymentAddress;
    }

    public void setDefaultPaymentAddress(AddressModel defaultPaymentAddress) {
        this.defaultPaymentAddress = defaultPaymentAddress;
    }

    public AddressModel getDefaultShipmentAddress() {
        return defaultShipmentAddress;
    }

    public void setDefaultShipmentAddress(AddressModel defaultShipmentAddress) {
        this.defaultShipmentAddress = defaultShipmentAddress;
    }

    public Set<CartModel> getCarts() {
        return carts;
    }

    public void setCarts(Set<CartModel> carts) {
        this.carts = carts;
    }

    public Set<OrderModel> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderModel> orders) {
        this.orders = orders;
    }

    public Set<AddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressModel> addresses) {
        this.addresses = addresses;
    }

    public Set<PaymentInfoModel> getPaymentInfos() {
        return paymentInfos;
    }

    public void setPaymentInfos(Set<PaymentInfoModel> paymentInfos) {
        this.paymentInfos = paymentInfos;
    }

    public Set<AbstractOrderModel> getAbstractOrderModels() {
        return abstractOrderModels;
    }

    public void setAbstractOrderModels(Set<AbstractOrderModel> abstractOrderModels) {
        this.abstractOrderModels = abstractOrderModels;
    }

    public Set<QuoteModel> getAssignedQuotes() {
        return assignedQuotes;
    }

    public void setAssignedQuotes(Set<QuoteModel> assignedQuotes) {
        this.assignedQuotes = assignedQuotes;
    }
}