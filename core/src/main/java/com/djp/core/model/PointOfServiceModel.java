
package com.djp.core.model;


import com.djp.core.enums.PointOfServiceTypeEnum;
import com.djp.modelservice.model.ItemModel;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "PointOfService")
public class PointOfServiceModel extends ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String description;
    private PointOfServiceTypeEnum type;
    private String displayName;
    private Date geocodeTimestamp;
    private double latitude;
    private double longitude;
    private double nearbyStoreRadius;
    private String storeContent;

    @OneToOne
    private AddressModel address;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="PoS2WarehouseRel",
            joinColumns={@JoinColumn(name="pointsOfService")},
            inverseJoinColumns={@JoinColumn(name="warehouses")})
    private Set<WarehouseModel> warehouses;

    @OneToMany(mappedBy = "store", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<StoreEmployeeGroupModel> storeEmployeeGroups;

    @ManyToOne
    @JoinColumn
    private BaseStoreModel baseStore;

    @ManyToMany(mappedBy = "pointOfServices", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<StoreLocatorFeatureModel> features;

    @OneToOne
    private MediaModel mapIcon;

    @OneToOne
    private MediaContainerModel storeImage;

    @OneToOne
    private OpeningScheduleModel openingSchedule;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PointOfServiceTypeEnum getType() {
        return type;
    }

    public void setType(PointOfServiceTypeEnum type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Date getGeocodeTimestamp() {
        return geocodeTimestamp;
    }

    public void setGeocodeTimestamp(Date geocodeTimestamp) {
        this.geocodeTimestamp = geocodeTimestamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getNearbyStoreRadius() {
        return nearbyStoreRadius;
    }

    public void setNearbyStoreRadius(double nearbyStoreRadius) {
        this.nearbyStoreRadius = nearbyStoreRadius;
    }

    public String getStoreContent() {
        return storeContent;
    }

    public void setStoreContent(String storeContent) {
        this.storeContent = storeContent;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public Set<WarehouseModel> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Set<WarehouseModel> warehouses) {
        this.warehouses = warehouses;
    }

    public Set<StoreEmployeeGroupModel> getStoreEmployeeGroups() {
        return storeEmployeeGroups;
    }

    public void setStoreEmployeeGroups(Set<StoreEmployeeGroupModel> storeEmployeeGroups) {
        this.storeEmployeeGroups = storeEmployeeGroups;
    }

    public BaseStoreModel getBaseStore() {
        return baseStore;
    }

    public void setBaseStore(BaseStoreModel baseStore) {
        this.baseStore = baseStore;
    }

    public Set<StoreLocatorFeatureModel> getFeatures() {
        return features;
    }

    public void setFeatures(Set<StoreLocatorFeatureModel> features) {
        this.features = features;
    }

    public MediaModel getMapIcon() {
        return mapIcon;
    }

    public void setMapIcon(MediaModel mapIcon) {
        this.mapIcon = mapIcon;
    }

    public MediaContainerModel getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(MediaContainerModel storeImage) {
        this.storeImage = storeImage;
    }

    public OpeningScheduleModel getOpeningSchedule() {
        return openingSchedule;
    }

    public void setOpeningSchedule(OpeningScheduleModel openingSchedule) {
        this.openingSchedule = openingSchedule;
    }
}
