package com.djp.core.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "FlightInformation")
public class FlightInformationModel extends ItemModel{

    @Id
    private Long id;
    private Date flightDate;
    private String flightNumber;
    private String departureLocation;
    private String arrivalLocation;
    private String gate;
    private String stoDate;
    private String stoTime;

    public String getStoDate() {
        return stoDate;
    }
    private String internationalStatus;

    public void setStoDate(String stoDate) {
        this.stoDate = stoDate;
    }

    public String getStoTime() {
        return stoTime;
    }

    public void setStoTime(String stoTime) {
        this.stoTime = stoTime;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }
    private String operationalStatus;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(String departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public String getInternationalStatus() {
        return internationalStatus;
    }

    public void setInternationalStatus(String internationalStatus) {
        this.internationalStatus = internationalStatus;
    }

    public String getOperationalStatus() {
        return operationalStatus;
    }

    public void setOperationalStatus(String operationalStatus) {
        this.operationalStatus = operationalStatus;
    }
}
