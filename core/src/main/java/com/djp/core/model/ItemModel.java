package com.djp.core.model;
/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Nov 17, 2023 12:21:06 AM                    ---
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 * Copyright (c) 2023 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.*;
import java.util.Date;

@MappedSuperclass
public class ItemModel {

    private String itemtype;

    private String airportId;
    public static final String OWNER = "owner";

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationtime;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedtime;

    public String getAirportId() {
        return airportId;
    }
    /** <i>Generated constant</i> - Attribute key of <code>Item.pk</code> attribute defined at extension <code>core</code>. */
    public static final String PK = "pk";

    public void setAirportId(String airportId) {
        this.airportId = airportId;
    }

    public String getItemtype() {
        return itemtype;
    }

    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }
    /** <i>Generated constant</i> - Attribute key of <code>Item.sealed</code> attribute defined at extension <code>core</code>. */
    public static final String SEALED = "sealed";

    /** <i>Generated constant</i> - Attribute key of <code>Item.comments</code> attribute defined at extension <code>comments</code>. */
    public static final String COMMENTS = "comments";
    public void setOwner(final ItemModel value)
    {
        //
    }
    public Date getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(Date creationtime) {
        this.creationtime = creationtime;
    }

    public Date getModifiedtime() {
        return modifiedtime;
    }

    public void setModifiedtime(Date modifiedtime) {
        this.modifiedtime = modifiedtime;
    }
}
