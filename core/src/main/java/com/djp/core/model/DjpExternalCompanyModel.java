package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpExternalCompany")
public class DjpExternalCompanyModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String companyID;
    private String companyName;
    private Boolean requiredOrderPayDetail = false;
    private Boolean requiredDeviceDetail = false;
    private Boolean canMakeRefund = false;
    private Boolean canMakeRefundOnlyWithSms = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getRequiredOrderPayDetail() {
        return requiredOrderPayDetail;
    }

    public void setRequiredOrderPayDetail(Boolean requiredOrderPayDetail) {
        this.requiredOrderPayDetail = requiredOrderPayDetail;
    }

    public Boolean getRequiredDeviceDetail() {
        return requiredDeviceDetail;
    }

    public void setRequiredDeviceDetail(Boolean requiredDeviceDetail) {
        this.requiredDeviceDetail = requiredDeviceDetail;
    }

    public Boolean getCanMakeRefund() {
        return canMakeRefund;
    }

    public void setCanMakeRefund(Boolean canMakeRefund) {
        this.canMakeRefund = canMakeRefund;
    }

    public Boolean getCanMakeRefundOnlyWithSms() {
        return canMakeRefundOnlyWithSms;
    }

    public void setCanMakeRefundOnlyWithSms(Boolean canMakeRefundOnlyWithSms) {
        this.canMakeRefundOnlyWithSms = canMakeRefundOnlyWithSms;
    }
}