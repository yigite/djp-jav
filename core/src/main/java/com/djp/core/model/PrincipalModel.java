package com.djp.core.model;


import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Principal")
@Inheritance(strategy = InheritanceType.JOINED)
public class PrincipalModel extends ItemModel{

    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long uid;

    private String description;
    private String name;
    private String displayName;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="PrincipalGroupRelation",
            joinColumns={@JoinColumn(name="members")},
            inverseJoinColumns={@JoinColumn(name="groups")})
    private Set<PrincipalGroupModel> groups;

    @ManyToMany(mappedBy = "allowedPrincipals", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CategoryModel> accessibleCategories;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name = "Pcp2WrtbleCVRel",
            joinColumns = {@JoinColumn(name = "writePrincipal")},
            inverseJoinColumns = {@JoinColumn(name = "writableCatalogVersion")})
    private Set<CatalogVersionModel> writableCatalogVersions;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name = "Pcpl2RdbleCVRel",
            joinColumns = {@JoinColumn(name = "readPrincipal")},
            inverseJoinColumns = {@JoinColumn(name = "readableCatalogVersion")})
    private Set<CatalogVersionModel> readableCatalogVersions;

    /*
    private String allGroups;
    private String profilePicture;*/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUid() {
        return String.valueOf(uid);
    }

    public void setUid(String uid) {
        this.uid = Long.parseLong(uid);
    }

    public Set<PrincipalGroupModel> getGroups() {
        return groups;
    }

    public void setGroups(Set<PrincipalGroupModel> groups) {
        this.groups = groups;
    }

    public Set<CategoryModel> getAccessibleCategories() {
        return accessibleCategories;
    }

    public void setAccessibleCategories(Set<CategoryModel> accessibleCategories) {
        this.accessibleCategories = accessibleCategories;
    }

    public Set<CatalogVersionModel> getWritableCatalogVersions() {
        return writableCatalogVersions;
    }

    public void setWritableCatalogVersions(Set<CatalogVersionModel> writableCatalogVersions) {
        this.writableCatalogVersions = writableCatalogVersions;
    }

    public Set<CatalogVersionModel> getReadableCatalogVersions() {
        return readableCatalogVersions;
    }

    public void setReadableCatalogVersions(Set<CatalogVersionModel> readableCatalogVersions) {
        this.readableCatalogVersions = readableCatalogVersions;
    }
}
