package com.djp.core.model;

import com.djp.core.enums.ErpReasonEnum;
import com.djp.core.enums.ErpStatusEnum;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "PaymentTransaction")
public class PaymentTransactionModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private String requestId;
    private String requestToken;
    private String paymentProvider;
    private String provDate;
    private String reasonCode;
    private String receiptNum;
    private String terminalNo;
    private String transId;
    private String userDescription;
    private String userip;
    private String versionID;
    private String zNum;
    private String paymentType;
    private String acquirerId;
    private String additionalField;
    private String personalOrCorporate;
    private Integer additionalInstallmentAmount;
    private Integer installmentAmount;
    private String authCode;
    private String bankName;
    private String bankResponse;
    private String binCode;
    private String cardHolderName;
    private String cardName;
    private String cardNumberMasked;
    private String cityName;
    private String cppToken;
    private String dealerCode;
    private String dealerUnitName;
    private String district;
    private String firmNote;
    private String maturity;
    private String erpErrorMessage;
    private ErpReasonEnum erpReason;
    private ErpStatusEnum erpStatus;
    private BigDecimal interest;
    private BigDecimal plannedAmount;

    @OneToOne
    private PlateMovementWsModel parkingValeProcess;

    @OneToOne
    private ParkingWProcessModel parkingWProcess;

    @OneToOne
    private PaymentInfoModel info;

    @OneToOne
    private CurrencyModel currency;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    @OneToMany(mappedBy = "paymentTransaction", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PaymentTransactionEntryModel> entries;

    @OneToOne
    private CustomerModel payer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(String paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public String getProvDate() {
        return provDate;
    }

    public void setProvDate(String provDate) {
        this.provDate = provDate;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getUserip() {
        return userip;
    }

    public void setUserip(String userip) {
        this.userip = userip;
    }

    public String getVersionID() {
        return versionID;
    }

    public void setVersionID(String versionID) {
        this.versionID = versionID;
    }

    public String getzNum() {
        return zNum;
    }

    public void setzNum(String zNum) {
        this.zNum = zNum;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAcquirerId() {
        return acquirerId;
    }

    public void setAcquirerId(String acquirerId) {
        this.acquirerId = acquirerId;
    }

    public String getAdditionalField() {
        return additionalField;
    }

    public void setAdditionalField(String additionalField) {
        this.additionalField = additionalField;
    }

    public String getPersonalOrCorporate() {
        return personalOrCorporate;
    }

    public void setPersonalOrCorporate(String personalOrCorporate) {
        this.personalOrCorporate = personalOrCorporate;
    }

    public Integer getAdditionalInstallmentAmount() {
        return additionalInstallmentAmount;
    }

    public void setAdditionalInstallmentAmount(Integer additionalInstallmentAmount) {
        this.additionalInstallmentAmount = additionalInstallmentAmount;
    }

    public Integer getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(Integer installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankResponse() {
        return bankResponse;
    }

    public void setBankResponse(String bankResponse) {
        this.bankResponse = bankResponse;
    }

    public String getBinCode() {
        return binCode;
    }

    public void setBinCode(String binCode) {
        this.binCode = binCode;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNumberMasked() {
        return cardNumberMasked;
    }

    public void setCardNumberMasked(String cardNumberMasked) {
        this.cardNumberMasked = cardNumberMasked;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCppToken() {
        return cppToken;
    }

    public void setCppToken(String cppToken) {
        this.cppToken = cppToken;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerUnitName() {
        return dealerUnitName;
    }

    public void setDealerUnitName(String dealerUnitName) {
        this.dealerUnitName = dealerUnitName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getFirmNote() {
        return firmNote;
    }

    public void setFirmNote(String firmNote) {
        this.firmNote = firmNote;
    }

    public String getMaturity() {
        return maturity;
    }

    public void setMaturity(String maturity) {
        this.maturity = maturity;
    }

    public String getErpErrorMessage() {
        return erpErrorMessage;
    }

    public void setErpErrorMessage(String erpErrorMessage) {
        this.erpErrorMessage = erpErrorMessage;
    }

    public ErpReasonEnum getErpReason() {
        return erpReason;
    }

    public void setErpReason(ErpReasonEnum erpReason) {
        this.erpReason = erpReason;
    }

    public ErpStatusEnum getErpStatus() {
        return erpStatus;
    }

    public void setErpStatus(ErpStatusEnum erpStatus) {
        this.erpStatus = erpStatus;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getPlannedAmount() {
        return plannedAmount;
    }

    public void setPlannedAmount(BigDecimal plannedAmount) {
        this.plannedAmount = plannedAmount;
    }

    public PlateMovementWsModel getParkingValeProcess() {
        return parkingValeProcess;
    }

    public void setParkingValeProcess(PlateMovementWsModel parkingValeProcess) {
        this.parkingValeProcess = parkingValeProcess;
    }

    public ParkingWProcessModel getParkingWProcess() {
        return parkingWProcess;
    }

    public void setParkingWProcess(ParkingWProcessModel parkingWProcess) {
        this.parkingWProcess = parkingWProcess;
    }

    public PaymentInfoModel getInfo() {
        return info;
    }

    public void setInfo(PaymentInfoModel info) {
        this.info = info;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public Set<PaymentTransactionEntryModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<PaymentTransactionEntryModel> entries) {
        this.entries = entries;
    }

    public CustomerModel getPayer() {
        return payer;
    }

    public void setPayer(CustomerModel payer) {
        this.payer = payer;
    }
}
