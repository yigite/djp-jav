package com.djp.core.model;

import com.djp.core.enums.GroupType;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "EntryGroup")
public class EntryGroupModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Integer groupNumber;

    private Integer priority;

    private String label;

    private GroupType groupType;

    @OneToMany(mappedBy = "parent" ,fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<EntryGroupModel> children;

    @ManyToOne
    @JoinColumn
    private EntryGroupModel parent;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    private String externalReferenceId;

    private Boolean erroneous;

    public EntryGroupModel()
    {
        // default constructor
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public void setGroupNumber(final Integer groupNumber)
    {
        this.groupNumber = groupNumber;
    }



    public Integer getGroupNumber()
    {
        return groupNumber;
    }



    public void setPriority(final Integer priority)
    {
        this.priority = priority;
    }



    public Integer getPriority()
    {
        return priority;
    }



    public void setLabel(final String label)
    {
        this.label = label;
    }



    public String getLabel()
    {
        return label;
    }



    public void setGroupType(final GroupType groupType)
    {
        this.groupType = groupType;
    }



    public GroupType getGroupType()
    {
        return groupType;
    }


    public Set<EntryGroupModel> getChildren() {
        return children;
    }

    public void setChildren(Set<EntryGroupModel> children) {
        this.children = children;
    }

    public EntryGroupModel getParent() {
        return parent;
    }

    public void setParent(EntryGroupModel parent) {
        this.parent = parent;
    }

    public void setExternalReferenceId(final String externalReferenceId)
    {
        this.externalReferenceId = externalReferenceId;
    }



    public String getExternalReferenceId()
    {
        return externalReferenceId;
    }



    public void setErroneous(final Boolean erroneous)
    {
        this.erroneous = erroneous;
    }



    public Boolean getErroneous()
    {
        return erroneous;
    }
}
