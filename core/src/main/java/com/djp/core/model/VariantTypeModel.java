package com.djp.core.model;

import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;

import java.util.Set;

@Entity
@Table(name="VariantType")
@PrimaryKeyJoinColumn(name = "id")
public class VariantTypeModel extends ComposedTypeModel{

    public VariantTypeModel()
    {
        super();
    }

    @Override
    public void setSuperType(final ComposedTypeModel value)
    {
        super.setSuperType(value);
    }
}
