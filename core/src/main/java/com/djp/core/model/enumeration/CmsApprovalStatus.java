package com.djp.core.model.enumeration;

public enum CmsApprovalStatus {

    CHECKED("CHECKED"),
    APPROVED("APPROVED"),
    UNAPPROVED("UNAPPROVED");

    private final String code;

    CmsApprovalStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
