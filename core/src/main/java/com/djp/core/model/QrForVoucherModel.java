package com.djp.core.model;

import com.djp.core.enums.KioksSalesStatus;
import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "QrForVoucher")
public class QrForVoucherModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date startDate;
    private Date expireDate;
    private Date usageDate;

    private KioksSalesStatus kioksSalesStatus;

    private String voucherContentKey;
    private String companyId;
    private String contractId;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForVoucher_QrType_whereIsValidQr", joinColumns = @JoinColumn(name = "QrForVoucher_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereIsValidQr;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForVoucher_QrType_whereUsed", joinColumns = @JoinColumn(name = "QrForVoucher_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereUsed;

    private Integer claimed;
    private Integer used;
    private Boolean promotionVoucher;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private Boolean voucherForRemainingUsage;
    private Boolean usedForCampaign;
    private Boolean voucherForDjpExternalCustomer;

    @OneToOne
    private MediaModel qr;
    private String totalPrice;
    private String currencyIsoCode;

    @OneToMany
    private Set<DjpVoucherRemainingModel> remainingForPackage;

    private String sapOrderID;
    private String entryNumberOnSapOrder;
    private Boolean usageForOverSixtyFiveOld;
    private Boolean isSuccesfullySentToSap;
    private boolean isCustomSingleCampaignCode;

    @OneToMany(mappedBy = "voucher")
    private Set<DjpVoucherCodeUsagesModel> usageDetails;

    @OneToMany(mappedBy = "voucher")
    private Set<DjpSingleCampaignCodeInfoModel> campaignDetails;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public KioksSalesStatus getKioksSalesStatus() {
        return kioksSalesStatus;
    }

    public void setKioksSalesStatus(KioksSalesStatus kioksSalesStatus) {
        this.kioksSalesStatus = kioksSalesStatus;
    }

    public String getVoucherContentKey() {
        return voucherContentKey;
    }

    public void setVoucherContentKey(String voucherContentKey) {
        this.voucherContentKey = voucherContentKey;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public Set<QrType> getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(Set<QrType> whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public Set<QrType> getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(Set<QrType> whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Boolean getPromotionVoucher() {
        return promotionVoucher;
    }

    public void setPromotionVoucher(Boolean promotionVoucher) {
        this.promotionVoucher = promotionVoucher;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public Boolean getVoucherForRemainingUsage() {
        return voucherForRemainingUsage;
    }

    public void setVoucherForRemainingUsage(Boolean voucherForRemainingUsage) {
        this.voucherForRemainingUsage = voucherForRemainingUsage;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public Boolean getVoucherForDjpExternalCustomer() {
        return voucherForDjpExternalCustomer;
    }

    public void setVoucherForDjpExternalCustomer(Boolean voucherForDjpExternalCustomer) {
        this.voucherForDjpExternalCustomer = voucherForDjpExternalCustomer;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public Set<DjpVoucherRemainingModel> getRemainingForPackage() {
        return remainingForPackage;
    }

    public void setRemainingForPackage(Set<DjpVoucherRemainingModel> remainingForPackage) {
        this.remainingForPackage = remainingForPackage;
    }

    public String getSapOrderID() {
        return sapOrderID;
    }

    public void setSapOrderID(String sapOrderID) {
        this.sapOrderID = sapOrderID;
    }

    public String getEntryNumberOnSapOrder() {
        return entryNumberOnSapOrder;
    }

    public void setEntryNumberOnSapOrder(String entryNumberOnSapOrder) {
        this.entryNumberOnSapOrder = entryNumberOnSapOrder;
    }

    public Boolean getUsageForOverSixtyFiveOld() {
        return usageForOverSixtyFiveOld;
    }

    public void setUsageForOverSixtyFiveOld(Boolean usageForOverSixtyFiveOld) {
        this.usageForOverSixtyFiveOld = usageForOverSixtyFiveOld;
    }

    public Boolean getSuccesfullySentToSap() {
        return isSuccesfullySentToSap;
    }

    public void setSuccesfullySentToSap(Boolean succesfullySentToSap) {
        isSuccesfullySentToSap = succesfullySentToSap;
    }

    public Boolean getIsCustomSingleCampaignCode() {
        return isCustomSingleCampaignCode;
    }

    public void setIsCustomSingleCampaignCode(Boolean customSingleCampaignCode) {
        isCustomSingleCampaignCode = customSingleCampaignCode;
    }

    public Set<DjpVoucherCodeUsagesModel> getUsageDetails() {
        return usageDetails;
    }

    public void setUsageDetails(Set<DjpVoucherCodeUsagesModel> usageDetails) {
        this.usageDetails = usageDetails;
    }

    public Set<DjpSingleCampaignCodeInfoModel> getCampaignDetails() {
        return campaignDetails;
    }

    public void setCampaignDetails(Set<DjpSingleCampaignCodeInfoModel> campaignDetails) {
        this.campaignDetails = campaignDetails;
    }
}
