package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "SyncItemJob")
@PrimaryKeyJoinColumn(name = "id")
public class SyncItemJobModel extends JobModel{

	@AttributeOverride(name = "code", column = @Column(unique = true, nullable = true))
	private String code;

	@ManyToOne
	@JoinColumn
	private CatalogVersionModel sourceVersion;

	@ManyToOne
	@JoinColumn
	private CatalogVersionModel targetVersion;

    public SyncItemJobModel() {
        super();
    }

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public CatalogVersionModel getSourceVersion() {
		return sourceVersion;
	}

	public void setSourceVersion(CatalogVersionModel sourceVersion) {
		this.sourceVersion = sourceVersion;
	}

	public CatalogVersionModel getTargetVersion() {
		return targetVersion;
	}

	public void setTargetVersion(CatalogVersionModel targetVersion) {
		this.targetVersion = targetVersion;
	}
}
