package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name="AbstractRestriction")
@Inheritance(strategy = InheritanceType.JOINED)
public class AbstractRestrictionModel extends CMSItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String type;
    private String description;

    public AbstractRestrictionModel() {
        super();
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
