package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "ClassificationAttributeValue")
public class ClassificationAttributeValueModel extends ItemModel{

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private long id;

   private String code;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
