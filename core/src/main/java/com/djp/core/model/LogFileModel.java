package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "LogFiles")
@PrimaryKeyJoinColumn(name = "id")
public class LogFileModel extends MediaModel {

    @ManyToOne
    @JoinColumn
    private CronJobModel cronJob;

    public LogFileModel() {
        super();
    }

    public CronJobModel getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJobModel cronJob) {
        this.cronJob = cronJob;
    }
}
