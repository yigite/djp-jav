package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "ParkingCalculation")
public class ParkingCalculationModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String crc;
    private String detailsText;
    private String durationsText;
    private String errorText;
    private String lastExitTime;

    @OneToOne
    private ParkingWTransactionModel transaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

    public String getDetailsText() {
        return detailsText;
    }

    public void setDetailsText(String detailsText) {
        this.detailsText = detailsText;
    }

    public String getDurationsText() {
        return durationsText;
    }

    public void setDurationsText(String durationsText) {
        this.durationsText = durationsText;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getLastExitTime() {
        return lastExitTime;
    }

    public void setLastExitTime(String lastExitTime) {
        this.lastExitTime = lastExitTime;
    }

    public ParkingWTransactionModel getTransaction() {
        return transaction;
    }

    public void setTransaction(ParkingWTransactionModel transaction) {
        this.transaction = transaction;
    }
}
