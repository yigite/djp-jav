package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "OpeningDay")
public class OpeningDayModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date openingTime;

    private Date closingTime;

    @ManyToOne
    @JoinColumn
    private OpeningScheduleModel openingSchedule;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(Date openingTime) {
        this.openingTime = openingTime;
    }

    public Date getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Date closingTime) {
        this.closingTime = closingTime;
    }

    public OpeningScheduleModel getOpeningSchedule() {
        return openingSchedule;
    }

    public void setOpeningSchedule(OpeningScheduleModel openingSchedule) {
        this.openingSchedule = openingSchedule;
    }
}
