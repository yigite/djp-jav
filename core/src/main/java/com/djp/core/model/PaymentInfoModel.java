package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PaymentInfo")
public class PaymentInfoModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //private ItemModel original;
    private String authCode;
    private String terminalId;
    private String merchantId;
    private String acquiredid;
    private boolean duplicate;
    private boolean saved;
    private String cardNumberMasked;

    @OneToOne(orphanRemoval = true)
    private AddressModel billingAddress;

    @OneToMany(mappedBy = "paymentInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartToOrderCronJobModel> cartToOrderCronJob;

    @ManyToOne
    @JoinColumn(name = "user")
    private UserModel user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getAcquiredid() {
        return acquiredid;
    }

    public void setAcquiredid(String acquiredid) {
        this.acquiredid = acquiredid;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getCardNumberMasked() {
        return cardNumberMasked;
    }

    public void setCardNumberMasked(String cardNumberMasked) {
        this.cardNumberMasked = cardNumberMasked;
    }

    public AddressModel getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AddressModel billingAddress) {
        this.billingAddress = billingAddress;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
