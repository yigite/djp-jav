package com.djp.core.model;

import com.djp.core.enums.InStockStatus;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "StockLevel")
public class StockLevelModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int available;
    private InStockStatus inStockStatus;
    private int maxPreOrder;
    private int maxStockLevelHistoryCount;
    private Date nextDeliveryTime;
    private Date releaseDate;
    private int overSelling;
    private int preOrder;
    private int reserved;
    private boolean treatNegativeAsZero;
    private String productCode;

    @OneToMany
    private Set<StockLevelHistoryEntryModel> stockLevelHistoryEntries;


    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="StockLevProductRel",
            joinColumns={@JoinColumn(name="product")},
            inverseJoinColumns={@JoinColumn(name="stockLevel")})
    private Set<ProductModel> products;

    @ManyToOne
    @JoinColumn
    private WarehouseModel warehouse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public InStockStatus getInStockStatus() {
        return inStockStatus;
    }

    public void setInStockStatus(InStockStatus inStockStatus) {
        this.inStockStatus = inStockStatus;
    }

    public int getMaxPreOrder() {
        return maxPreOrder;
    }

    public void setMaxPreOrder(int maxPreOrder) {
        this.maxPreOrder = maxPreOrder;
    }

    public int getMaxStockLevelHistoryCount() {
        return maxStockLevelHistoryCount;
    }

    public void setMaxStockLevelHistoryCount(int maxStockLevelHistoryCount) {
        this.maxStockLevelHistoryCount = maxStockLevelHistoryCount;
    }

    public Date getNextDeliveryTime() {
        return nextDeliveryTime;
    }

    public void setNextDeliveryTime(Date nextDeliveryTime) {
        this.nextDeliveryTime = nextDeliveryTime;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getOverSelling() {
        return overSelling;
    }

    public void setOverSelling(int overSelling) {
        this.overSelling = overSelling;
    }

    public int getPreOrder() {
        return preOrder;
    }

    public void setPreOrder(int preOrder) {
        this.preOrder = preOrder;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public boolean isTreatNegativeAsZero() {
        return treatNegativeAsZero;
    }

    public void setTreatNegativeAsZero(boolean treatNegativeAsZero) {
        this.treatNegativeAsZero = treatNegativeAsZero;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Set<StockLevelHistoryEntryModel> getStockLevelHistoryEntries() {
        return stockLevelHistoryEntries;
    }

    public void setStockLevelHistoryEntries(Set<StockLevelHistoryEntryModel> stockLevelHistoryEntries) {
        this.stockLevelHistoryEntries = stockLevelHistoryEntries;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public WarehouseModel getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseModel warehouse) {
        this.warehouse = warehouse;
    }
}
