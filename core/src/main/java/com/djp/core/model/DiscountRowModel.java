package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name="DiscountRow")
@PrimaryKeyJoinColumn(name = "id")
public class DiscountRowModel extends AbstractDiscountRowModel{

    private boolean asTargetPrice;
    private String sapConditionId;

    @ManyToOne
    @JoinColumn
    private ProductModel product;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    public DiscountRowModel() {
        super();
    }

    public boolean isAsTargetPrice() {
        return asTargetPrice;
    }

    public void setAsTargetPrice(boolean asTargetPrice) {
        this.asTargetPrice = asTargetPrice;
    }

    public String getSapConditionId() {
        return sapConditionId;
    }

    public void setSapConditionId(String sapConditionId) {
        this.sapConditionId = sapConditionId;
    }

    @Override
    public ProductModel getProduct() {
        return product;
    }

    @Override
    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }
}
