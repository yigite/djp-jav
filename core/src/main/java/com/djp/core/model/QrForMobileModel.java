package com.djp.core.model;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "QrForMobile")
public class QrForMobileModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    private MediaModel qr;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForMobile_QrType", joinColumns = @JoinColumn(name = "QrForMobile_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereIsValidQr;

    private QrType whereUsed;
    private Date usageDate;
    private String plateNumber;
    private String carParkingUsedInfo;
    private Boolean usedVale;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private Boolean usedForCampaign;
    private PassengerType passengerType;
    private Integer passengerCount;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public Set<QrType> getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(Set<QrType> whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCarParkingUsedInfo() {
        return carParkingUsedInfo;
    }

    public void setCarParkingUsedInfo(String carParkingUsedInfo) {
        this.carParkingUsedInfo = carParkingUsedInfo;
    }

    public Boolean getUsedVale() {
        return usedVale;
    }

    public void setUsedVale(Boolean usedVale) {
        this.usedVale = usedVale;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }
}
