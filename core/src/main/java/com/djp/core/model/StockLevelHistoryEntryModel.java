package com.djp.core.model;

import com.djp.core.enums.StockLevelUpdateType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "StockLevelHistoryEntry")
public class StockLevelHistoryEntryModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int actual;
    private int reserved;
    private String comment;
    private Date updateDate;
    private StockLevelUpdateType updateType;
    private String stockLevelPOS;

    @ManyToOne
    @JoinColumn
    private StockLevelModel stockLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getActual() {
        return actual;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public StockLevelUpdateType getUpdateType() {
        return updateType;
    }

    public void setUpdateType(StockLevelUpdateType updateType) {
        this.updateType = updateType;
    }

    public String getStockLevelPOS() {
        return stockLevelPOS;
    }

    public void setStockLevelPOS(String stockLevelPOS) {
        this.stockLevelPOS = stockLevelPOS;
    }

    public StockLevelModel getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(StockLevelModel stockLevel) {
        this.stockLevel = stockLevel;
    }
}
