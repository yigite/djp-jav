package com.djp.core.model;

import com.djp.core.enums.*;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "AbstractOrder")
@Inheritance(strategy = InheritanceType.JOINED)
public class AbstractOrderModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private Date date;
    private Date expirationTime;
    private Date flightDateTime;
    private Date flightDate;
    private Date customerArrivalDate;
    private Date renewedUsageStartDate;
    private boolean calculated;
    private boolean net;
    private boolean discountsIncludeDeliveryCost;
    private boolean discountsIncludePaymentCost;
    private boolean isSuccesfullySentToGTM;
    private boolean orderForRenewedUsages;
    private boolean forSomeBodyElse;
    private boolean isMainNonSapPacketOrder;
    private boolean isRezervationOrder;
    private boolean isStoppage;
    private boolean dontSendWelcomeMessage;
    private String personalOrCorporate;
    private String nationalOrForeigner;
    private double deliveryCost;
    private double paymentCost;
    private double totalPrice;
    private double totalDiscounts;
    private double totalTax;
    private double subtotal;
    private int adultGuestCount;
    private int childGuestCount;
    private int babyGuestCount;
    private int luggageCount;
    private int priority;
    private String description;
    private String name;
    private String globalDiscountValuesInternal;
    private String totalTaxValuesInternal;
    private String statusInfo;
    private String quoteDiscountValuesInternal;
    private String ymktTrackingId;
    private String tcknNumber;
    private String passportNumber;
    private String taxNumber;
    private String taxOffice;
    private String serviceType;
    private String domesticOrExternalLine;
    private String servicePoint;
    private String flightNumber;
    private String destination;
    private String assignedAssistantUid;
    private String note;
    private String customerArrivalTime;
    private String salesDocument;
    private String slipNo;
    private String bankCodeForSlipNo;
    private String terminalNoOfPOS;
    private String havaleEftNo;
    private String asmCurrency;
    private String asmDate;
    private String corporateName;
    private String corporateDepartment;
    private String corporateDescriptionType;
    private String corporateFirm;
    private String corporateDescription;
    private String bankOrderID;
    private String payInfoByBank;
    private String cppToken;

    /**
     * The guid for the anonymous cart used to lookup stored carts.
     * The order guid is used as a non-authenticated deep link to the order history page.
     */
    private String guid;

    @OneToOne
    private DjpOtherCustomerModel djpOtherCustomer;

    @OneToOne
    private CurrencyModel currency;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ConsignmentModel> consignments;

    @ManyToMany
    @JoinTable(name="CartGuestCustomerRelation",
            joinColumns={@JoinColumn(name="guestCustomer")},
            inverseJoinColumns={@JoinColumn(name="cart")})
    private Set<DjpOtherCustomerModel> guestCustomers;

    @OneToOne
    private AddressModel deliveryAddress;

    @OneToOne
    private AddressModel paymentAddress;

    @OneToOne
    private DeliveryModeModel deliveryMode;

    @OneToOne
    private DeliveryModeModel previousDeliveryMode;

    private DeliveryStatus deliveryStatus;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<DiscountValueModel> globalDiscountValues;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<TaxValueModel> totalTaxValues;

    @OneToOne(cascade = CascadeType.ALL)
    private PaymentInfoModel paymentInfo;

    @OneToOne
    private PaymentModeModel paymentMode;

    private PaymentStatus paymentStatus;
    private OrderStatus status;
    private ExportStatus exportStatus;

    @ManyToOne
    @JoinColumn(name = "djpUser")
    private UserModel user;

    @OneToMany(mappedBy = "order", fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<EntryGroupModel> entryGroups;

    @OneToMany(mappedBy = "order", fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AbstractOrderEntryModel> entries;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="OrderDiscountRelation",
            joinColumns={@JoinColumn(name="order")},
            inverseJoinColumns={@JoinColumn(name="discount")})
    private Set<DiscountModel> discounts;

    private UserDiscountGroup Europe1PriceFactory_UDG;
    private UserPriceGroup Europe1PriceFactory_UPG;
    private UserTaxGroup Europe1PriceFactory_UTG;

    @ElementCollection
    @CollectionTable(name="AbstractOrderAppliedVoucherCodes", joinColumns=@JoinColumn(name="orderCode"))
    @Column(name="appliedVoucherCode")
    private Set<String> appliedVoucherCodes;

    @ElementCollection
    @CollectionTable(name="BillingResponseLog", joinColumns=@JoinColumn(name="abstractOrder"))
    @Column(name="billingResponseLog")
    private Set<String> billingResponseLog;

    /**
     * TODO: Promotion yapısı kurulduktan sonra eklenebilir
     * Set<PromotionResultModel> allPromotionResults;
     * Set<PromotionOrderRestrictionModel> promotionOrderRestrictions;
     * Set<String> appliedCouponCodes;
     */

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PaymentTransactionModel> paymentTransactions;

    @OneToOne
    private BaseSiteModel site;

    @OneToOne
    private BaseStoreModel store;

    @OneToMany(mappedBy = "parentOrder", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AbstractOrderEntryModel> subOrderList;

    @OneToOne(mappedBy = "order", cascade = CascadeType.ALL)
    private DjpFlightInformationModel djpFlightInformation;

    @OneToOne(cascade = CascadeType.ALL)
    private DjpPaidPriceModel paidPrice;

    @OneToOne(cascade = CascadeType.ALL)
    private DjpInfoDeskLocationModel djpInfoDeskLocation;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="djpMeetGreetInfoigaMeetGreetInfo")
    private DjpMeetGreetInfoModel djpMeetGreetInfoigaMeetGreetInfo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="deviceDetail")
    private DjpOrderDeviceDetailModel deviceDetail;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="activationOrUsageCode")
    private DjpUniqueTokenModel activationOrUsageCode;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Ord2PlateRel",
            joinColumns={@JoinColumn(name="abstractOrder")},
            inverseJoinColumns={@JoinColumn(name="numberPlate")})
    private Set<NumberPlateModel> numberPlates;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public boolean isCalculated() {
        return calculated;
    }

    public void setCalculated(boolean calculated) {
        this.calculated = calculated;
    }

    public boolean isNet() {
        return net;
    }

    public void setNet(boolean net) {
        this.net = net;
    }

    public boolean isDiscountsIncludeDeliveryCost() {
        return discountsIncludeDeliveryCost;
    }

    public void setDiscountsIncludeDeliveryCost(boolean discountsIncludeDeliveryCost) {
        this.discountsIncludeDeliveryCost = discountsIncludeDeliveryCost;
    }

    public boolean isDiscountsIncludePaymentCost() {
        return discountsIncludePaymentCost;
    }

    public void setDiscountsIncludePaymentCost(boolean discountsIncludePaymentCost) {
        this.discountsIncludePaymentCost = discountsIncludePaymentCost;
    }

    public double getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(double deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public double getPaymentCost() {
        return paymentCost;
    }

    public void setPaymentCost(double paymentCost) {
        this.paymentCost = paymentCost;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalDiscounts() {
        return totalDiscounts;
    }

    public void setTotalDiscounts(double totalDiscounts) {
        this.totalDiscounts = totalDiscounts;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGlobalDiscountValuesInternal() {
        return globalDiscountValuesInternal;
    }

    public void setGlobalDiscountValuesInternal(String globalDiscountValuesInternal) {
        this.globalDiscountValuesInternal = globalDiscountValuesInternal;
    }

    public String getTotalTaxValuesInternal() {
        return totalTaxValuesInternal;
    }

    public void setTotalTaxValuesInternal(String totalTaxValuesInternal) {
        this.totalTaxValuesInternal = totalTaxValuesInternal;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public Set<ConsignmentModel> getConsignments() {
        return consignments;
    }

    public void setConsignments(Set<ConsignmentModel> consignments) {
        this.consignments = consignments;
    }

    public Set<DjpOtherCustomerModel> getGuestCustomers() {
        return guestCustomers;
    }

    public void setGuestCustomers(Set<DjpOtherCustomerModel> guestCustomers) {
        this.guestCustomers = guestCustomers;
    }

    public AddressModel getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressModel deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AddressModel getPaymentAddress() {
        return paymentAddress;
    }

    public void setPaymentAddress(AddressModel paymentAddress) {
        this.paymentAddress = paymentAddress;
    }

    public DeliveryModeModel getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(DeliveryModeModel deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public DeliveryModeModel getPreviousDeliveryMode() {
        return previousDeliveryMode;
    }

    public void setPreviousDeliveryMode(DeliveryModeModel previousDeliveryMode) {
        this.previousDeliveryMode = previousDeliveryMode;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Set<DiscountValueModel> getGlobalDiscountValues() {
        return globalDiscountValues;
    }

    public void setGlobalDiscountValues(Set<DiscountValueModel> globalDiscountValues) {
        this.globalDiscountValues = globalDiscountValues;
    }

    public Set<TaxValueModel> getTotalTaxValues() {
        return totalTaxValues;
    }

    public void setTotalTaxValues(Set<TaxValueModel> totalTaxValues) {
        this.totalTaxValues = totalTaxValues;
    }

    public PaymentInfoModel getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoModel paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public PaymentModeModel getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentModeModel paymentMode) {
        this.paymentMode = paymentMode;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public Set<EntryGroupModel> getEntryGroups() {
        return entryGroups;
    }

    public void setEntryGroups(Set<EntryGroupModel> entryGroups) {
        this.entryGroups = entryGroups;
    }

    public Set<AbstractOrderEntryModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<AbstractOrderEntryModel> entries) {
        this.entries = entries;
    }

    public Set<DiscountModel> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Set<DiscountModel> discounts) {
        this.discounts = discounts;
    }

    public UserDiscountGroup getEurope1PriceFactory_UDG() {
        return Europe1PriceFactory_UDG;
    }

    public void setEurope1PriceFactory_UDG(UserDiscountGroup europe1PriceFactory_UDG) {
        Europe1PriceFactory_UDG = europe1PriceFactory_UDG;
    }

    public UserPriceGroup getEurope1PriceFactory_UPG() {
        return Europe1PriceFactory_UPG;
    }

    public void setEurope1PriceFactory_UPG(UserPriceGroup europe1PriceFactory_UPG) {
        Europe1PriceFactory_UPG = europe1PriceFactory_UPG;
    }

    public UserTaxGroup getEurope1PriceFactory_UTG() {
        return Europe1PriceFactory_UTG;
    }

    public void setEurope1PriceFactory_UTG(UserTaxGroup europe1PriceFactory_UTG) {
        Europe1PriceFactory_UTG = europe1PriceFactory_UTG;
    }

    public Set<String> getAppliedVoucherCodes() {
        return appliedVoucherCodes;
    }

    public void setAppliedVoucherCodes(Set<String> appliedVoucherCodes) {
        this.appliedVoucherCodes = appliedVoucherCodes;
    }

    public Set<PaymentTransactionModel> getPaymentTransactions() {
        return paymentTransactions;
    }

    public void setPaymentTransactions(Set<PaymentTransactionModel> paymentTransactions) {
        this.paymentTransactions = paymentTransactions;
    }


    public Date getFlightDateTime() {
        return flightDateTime;
    }

    public void setFlightDateTime(Date flightDateTime) {
        this.flightDateTime = flightDateTime;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public Date getCustomerArrivalDate() {
        return customerArrivalDate;
    }

    public void setCustomerArrivalDate(Date customerArrivalDate) {
        this.customerArrivalDate = customerArrivalDate;
    }

    public Date getRenewedUsageStartDate() {
        return renewedUsageStartDate;
    }

    public void setRenewedUsageStartDate(Date renewedUsageStartDate) {
        this.renewedUsageStartDate = renewedUsageStartDate;
    }

    public boolean isSuccesfullySentToGTM() {
        return isSuccesfullySentToGTM;
    }

    public void setSuccesfullySentToGTM(boolean succesfullySentToGTM) {
        isSuccesfullySentToGTM = succesfullySentToGTM;
    }

    public boolean isOrderForRenewedUsages() {
        return orderForRenewedUsages;
    }

    public void setOrderForRenewedUsages(boolean orderForRenewedUsages) {
        this.orderForRenewedUsages = orderForRenewedUsages;
    }

    public boolean isForSomeBodyElse() {
        return forSomeBodyElse;
    }

    public void setForSomeBodyElse(boolean forSomeBodyElse) {
        this.forSomeBodyElse = forSomeBodyElse;
    }

    public boolean isMainNonSapPacketOrder() {
        return isMainNonSapPacketOrder;
    }

    public void setMainNonSapPacketOrder(boolean mainNonSapPacketOrder) {
        isMainNonSapPacketOrder = mainNonSapPacketOrder;
    }

    public boolean isRezervationOrder() {
        return isRezervationOrder;
    }

    public void setRezervationOrder(boolean rezervationOrder) {
        isRezervationOrder = rezervationOrder;
    }

    public boolean isStoppage() {
        return isStoppage;
    }

    public void setStoppage(boolean stoppage) {
        isStoppage = stoppage;
    }

    public boolean isDontSendWelcomeMessage() {
        return dontSendWelcomeMessage;
    }

    public void setDontSendWelcomeMessage(boolean dontSendWelcomeMessage) {
        this.dontSendWelcomeMessage = dontSendWelcomeMessage;
    }

    public String getPersonalOrCorporate() {
        return personalOrCorporate;
    }

    public void setPersonalOrCorporate(String personalOrCorporate) {
        this.personalOrCorporate = personalOrCorporate;
    }

    public String getNationalOrForeigner() {
        return nationalOrForeigner;
    }

    public void setNationalOrForeigner(String nationalOrForeigner) {
        this.nationalOrForeigner = nationalOrForeigner;
    }

    public int getAdultGuestCount() {
        return adultGuestCount;
    }

    public void setAdultGuestCount(int adultGuestCount) {
        this.adultGuestCount = adultGuestCount;
    }

    public int getChildGuestCount() {
        return childGuestCount;
    }

    public void setChildGuestCount(int childGuestCount) {
        this.childGuestCount = childGuestCount;
    }

    public int getBabyGuestCount() {
        return babyGuestCount;
    }

    public void setBabyGuestCount(int babyGuestCount) {
        this.babyGuestCount = babyGuestCount;
    }

    public int getLuggageCount() {
        return luggageCount;
    }

    public void setLuggageCount(int luggageCount) {
        this.luggageCount = luggageCount;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getQuoteDiscountValuesInternal() {
        return quoteDiscountValuesInternal;
    }

    public void setQuoteDiscountValuesInternal(String quoteDiscountValuesInternal) {
        this.quoteDiscountValuesInternal = quoteDiscountValuesInternal;
    }

    public String getYmktTrackingId() {
        return ymktTrackingId;
    }

    public void setYmktTrackingId(String ymktTrackingId) {
        this.ymktTrackingId = ymktTrackingId;
    }

    public String getTcknNumber() {
        return tcknNumber;
    }

    public void setTcknNumber(String tcknNumber) {
        this.tcknNumber = tcknNumber;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getDomesticOrExternalLine() {
        return domesticOrExternalLine;
    }

    public void setDomesticOrExternalLine(String domesticOrExternalLine) {
        this.domesticOrExternalLine = domesticOrExternalLine;
    }

    public String getServicePoint() {
        return servicePoint;
    }

    public void setServicePoint(String servicePoint) {
        this.servicePoint = servicePoint;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAssignedAssistantUid() {
        return assignedAssistantUid;
    }

    public void setAssignedAssistantUid(String assignedAssistantUid) {
        this.assignedAssistantUid = assignedAssistantUid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCustomerArrivalTime() {
        return customerArrivalTime;
    }

    public void setCustomerArrivalTime(String customerArrivalTime) {
        this.customerArrivalTime = customerArrivalTime;
    }

    public String getSalesDocument() {
        return salesDocument;
    }

    public void setSalesDocument(String salesDocument) {
        this.salesDocument = salesDocument;
    }

    public String getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String slipNo) {
        this.slipNo = slipNo;
    }

    public String getBankCodeForSlipNo() {
        return bankCodeForSlipNo;
    }

    public void setBankCodeForSlipNo(String bankCodeForSlipNo) {
        this.bankCodeForSlipNo = bankCodeForSlipNo;
    }

    public String getTerminalNoOfPOS() {
        return terminalNoOfPOS;
    }

    public void setTerminalNoOfPOS(String terminalNoOfPOS) {
        this.terminalNoOfPOS = terminalNoOfPOS;
    }

    public String getHavaleEftNo() {
        return havaleEftNo;
    }

    public void setHavaleEftNo(String havaleEftNo) {
        this.havaleEftNo = havaleEftNo;
    }

    public String getAsmCurrency() {
        return asmCurrency;
    }

    public void setAsmCurrency(String asmCurrency) {
        this.asmCurrency = asmCurrency;
    }

    public String getAsmDate() {
        return asmDate;
    }

    public void setAsmDate(String asmDate) {
        this.asmDate = asmDate;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCorporateDepartment() {
        return corporateDepartment;
    }

    public void setCorporateDepartment(String corporateDepartment) {
        this.corporateDepartment = corporateDepartment;
    }

    public String getCorporateDescriptionType() {
        return corporateDescriptionType;
    }

    public void setCorporateDescriptionType(String corporateDescriptionType) {
        this.corporateDescriptionType = corporateDescriptionType;
    }

    public String getCorporateFirm() {
        return corporateFirm;
    }

    public void setCorporateFirm(String corporateFirm) {
        this.corporateFirm = corporateFirm;
    }

    public String getCorporateDescription() {
        return corporateDescription;
    }

    public void setCorporateDescription(String corporateDescription) {
        this.corporateDescription = corporateDescription;
    }

    public String getBankOrderID() {
        return bankOrderID;
    }

    public void setBankOrderID(String bankOrderID) {
        this.bankOrderID = bankOrderID;
    }

    public String getPayInfoByBank() {
        return payInfoByBank;
    }

    public void setPayInfoByBank(String payInfoByBank) {
        this.payInfoByBank = payInfoByBank;
    }

    public String getCppToken() {
        return cppToken;
    }

    public void setCppToken(String cppToken) {
        this.cppToken = cppToken;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public DjpOtherCustomerModel getDjpOtherCustomer() {
        return djpOtherCustomer;
    }

    public void setDjpOtherCustomer(DjpOtherCustomerModel djpOtherCustomer) {
        this.djpOtherCustomer = djpOtherCustomer;
    }

    public Set<String> getBillingResponseLog() {
        return billingResponseLog;
    }

    public void setBillingResponseLog(Set<String> billingResponseLog) {
        this.billingResponseLog = billingResponseLog;
    }

    public BaseSiteModel getSite() {
        return site;
    }

    public void setSite(BaseSiteModel site) {
        this.site = site;
    }

    public BaseStoreModel getStore() {
        return store;
    }

    public void setStore(BaseStoreModel store) {
        this.store = store;
    }

    public Set<AbstractOrderEntryModel> getSubOrderList() {
        return subOrderList;
    }

    public void setSubOrderList(Set<AbstractOrderEntryModel> subOrderList) {
        this.subOrderList = subOrderList;
    }

    public DjpFlightInformationModel getDjpFlightInformation() {
        return djpFlightInformation;
    }

    public void setDjpFlightInformation(DjpFlightInformationModel djpFlightInformation) {
        this.djpFlightInformation = djpFlightInformation;
    }

    public DjpPaidPriceModel getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(DjpPaidPriceModel paidPrice) {
        this.paidPrice = paidPrice;
    }

    public DjpInfoDeskLocationModel getDjpInfoDeskLocation() {
        return djpInfoDeskLocation;
    }

    public void setDjpInfoDeskLocation(DjpInfoDeskLocationModel djpInfoDeskLocation) {
        this.djpInfoDeskLocation = djpInfoDeskLocation;
    }

    public DjpMeetGreetInfoModel getDjpMeetGreetInfoigaMeetGreetInfo() {
        return djpMeetGreetInfoigaMeetGreetInfo;
    }

    public void setDjpMeetGreetInfoigaMeetGreetInfo(DjpMeetGreetInfoModel djpMeetGreetInfoigaMeetGreetInfo) {
        this.djpMeetGreetInfoigaMeetGreetInfo = djpMeetGreetInfoigaMeetGreetInfo;
    }

    public DjpOrderDeviceDetailModel getDeviceDetail() {
        return deviceDetail;
    }

    public void setDeviceDetail(DjpOrderDeviceDetailModel deviceDetail) {
        this.deviceDetail = deviceDetail;
    }

    public DjpUniqueTokenModel getActivationOrUsageCode() {
        return activationOrUsageCode;
    }

    public void setActivationOrUsageCode(DjpUniqueTokenModel activationOrUsageCode) {
        this.activationOrUsageCode = activationOrUsageCode;
    }

    public Set<NumberPlateModel> getNumberPlates() {
        return numberPlates;
    }

    public void setNumberPlates(Set<NumberPlateModel> numberPlates) {
        this.numberPlates = numberPlates;
    }
}
