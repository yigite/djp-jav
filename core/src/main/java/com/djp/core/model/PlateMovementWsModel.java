package com.djp.core.model;

import com.djp.core.enums.HizmetDurumEnum;
import com.djp.core.enums.OdemeDurumEnum;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "PlateMovementWs")
public class PlateMovementWsModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private String customerBarcode;
    private Integer parking;
    private Integer contractNumber;
    private Integer oid;
    private Integer djpPassDays;
    private String enterance;
    private String date;
    private String contractSerial;
    private BigDecimal priceWithDjppass;
    private BigDecimal priceWithoutDjppass;
    private boolean payWithDjpPass;
    private String salesDocument;
    private HizmetDurumEnum serviceStatus;
    private OdemeDurumEnum paymentStatus;

    @OneToOne
    private ValePlateModel valePlate;

    @ManyToOne
    @JoinColumn
    private ValePersonModel valePerson;

    @OneToOne
    private SubscriptionPackageModel subscriptionPackage;

    @ManyToOne
    @JoinColumn
    private CustomerModel user;

    @OneToMany(mappedBy = "plateMovementWs")
    private Set<ValeServiceModel> valeServices;

    //İş akışı öğrenildikten sonra onetomany ise yapılabilir
    @OneToOne
    private DjpRemainingUsageModel djpRemainingUsage;

    @ElementCollection
    @CollectionTable(name="BillingResponseLog", joinColumns=@JoinColumn(name="plateMovementWs_id"))
    @Column(name="billingResponseLog")
    private Set<String> billingResponseLog;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCustomerBarcode() {
        return customerBarcode;
    }

    public void setCustomerBarcode(String customerBarcode) {
        this.customerBarcode = customerBarcode;
    }

    public Integer getParking() {
        return parking;
    }

    public void setParking(Integer parking) {
        this.parking = parking;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Integer getDjpPassDays() {
        return djpPassDays;
    }

    public void setDjpPassDays(Integer djpPassDays) {
        this.djpPassDays = djpPassDays;
    }

    public String getEnterance() {
        return enterance;
    }

    public void setEnterance(String enterance) {
        this.enterance = enterance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContractSerial() {
        return contractSerial;
    }

    public void setContractSerial(String contractSerial) {
        this.contractSerial = contractSerial;
    }

    public BigDecimal getPriceWithDjppass() {
        return priceWithDjppass;
    }

    public void setPriceWithDjppass(BigDecimal priceWithDjppass) {
        this.priceWithDjppass = priceWithDjppass;
    }

    public BigDecimal getPriceWithoutDjppass() {
        return priceWithoutDjppass;
    }

    public void setPriceWithoutDjppass(BigDecimal priceWithoutDjppass) {
        this.priceWithoutDjppass = priceWithoutDjppass;
    }

    public boolean isPayWithDjpPass() {
        return payWithDjpPass;
    }

    public void setPayWithDjpPass(boolean payWithDjpPass) {
        this.payWithDjpPass = payWithDjpPass;
    }

    public String getSalesDocument() {
        return salesDocument;
    }

    public void setSalesDocument(String salesDocument) {
        this.salesDocument = salesDocument;
    }

    public ValePlateModel getValePlate() {
        return valePlate;
    }

    public void setValePlate(ValePlateModel valePlate) {
        this.valePlate = valePlate;
    }

    public HizmetDurumEnum getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(HizmetDurumEnum serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public OdemeDurumEnum getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(OdemeDurumEnum paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public ValePersonModel getValePerson() {
        return valePerson;
    }

    public void setValePerson(ValePersonModel valePerson) {
        this.valePerson = valePerson;
    }

    public SubscriptionPackageModel getSubscriptionPackage() {
        return subscriptionPackage;
    }

    public void setSubscriptionPackage(SubscriptionPackageModel subscriptionPackage) {
        this.subscriptionPackage = subscriptionPackage;
    }

    public CustomerModel getUser() {
        return user;
    }

    public void setUser(CustomerModel user) {
        this.user = user;
    }

    public Set<ValeServiceModel> getValeServices() {
        return valeServices;
    }

    public void setValeServices(Set<ValeServiceModel> valeServices) {
        this.valeServices = valeServices;
    }

    public DjpRemainingUsageModel getDjpRemainingUsage() {
        return djpRemainingUsage;
    }

    public void setDjpRemainingUsage(DjpRemainingUsageModel djpRemainingUsage) {
        this.djpRemainingUsage = djpRemainingUsage;
    }

    public Set<String> getBillingResponseLog() {
        return billingResponseLog;
    }

    public void setBillingResponseLog(Set<String> billingResponseLog) {
        this.billingResponseLog = billingResponseLog;
    }
}
