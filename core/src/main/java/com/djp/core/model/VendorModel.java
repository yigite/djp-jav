package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Vendor")
public class VendorModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique=true)
    private String code;
    private String name;

    @OneToMany(mappedBy = "vendor", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<WarehouseModel> warehouses;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="ProductVendorRelation",
            joinColumns = {@JoinColumn(name="vendor")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> products;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<WarehouseModel> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Set<WarehouseModel> warehouses) {
        this.warehouses = warehouses;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }
}
