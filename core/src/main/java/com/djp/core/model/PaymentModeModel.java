package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PaymentMode")
public class PaymentModeModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String code;

    private boolean active;
    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name="PaymentMode2DeliveryMode",
            joinColumns={@JoinColumn(name="paymentMode")},
            inverseJoinColumns={@JoinColumn(name="deliveryMode")})
    private Set<DeliveryModeModel> supportedDeliveryModes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DeliveryModeModel> getSupportedDeliveryModes() {
        return supportedDeliveryModes;
    }

    public void setSupportedDeliveryModes(Set<DeliveryModeModel> supportedDeliveryModes) {
        this.supportedDeliveryModes = supportedDeliveryModes;
    }
}
