package com.djp.core.model;

import jakarta.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Warehouse")
public class WarehouseModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String code;
    private String name;
    private boolean isDefault;
    private String vendorPOS;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private VendorModel vendor;

    @OneToMany(mappedBy = "warehouse")
    private Set<ConsignmentModel> consignments;

    @OneToMany(mappedBy = "warehouse", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<StockLevelModel> stockLevels;

    @ManyToMany(mappedBy = "warehouses", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<PointOfServiceModel> pointsOfService;

    @ManyToMany(mappedBy = "warehouses", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<BaseStoreModel> baseStores;

    @OneToMany(mappedBy = "sapPlant", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<ProductModel> product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getVendorPOS() {
        return vendorPOS;
    }

    public void setVendorPOS(String vendorPOS) {
        this.vendorPOS = vendorPOS;
    }

    public VendorModel getVendor() {
        return vendor;
    }

    public void setVendor(VendorModel vendor) {
        this.vendor = vendor;
    }

    public Set<ConsignmentModel> getConsignments() {
        return consignments;
    }

    public void setConsignments(Set<ConsignmentModel> consignments) {
        this.consignments = consignments;
    }

    public Set<StockLevelModel> getStockLevels() {
        return stockLevels;
    }

    public void setStockLevels(Set<StockLevelModel> stockLevels) {
        this.stockLevels = stockLevels;
    }

    public Set<PointOfServiceModel> getPointsOfService() {
        return pointsOfService;
    }

    public void setPointsOfService(Set<PointOfServiceModel> pointsOfService) {
        this.pointsOfService = pointsOfService;
    }

    public Set<BaseStoreModel> getBaseStores() {
        return baseStores;
    }

    public void setBaseStores(Set<BaseStoreModel> baseStores) {
        this.baseStores = baseStores;
    }

    public Set<ProductModel> getProduct() {
        return product;
    }

    public void setProduct(Set<ProductModel> product) {
        this.product = product;
    }
}
