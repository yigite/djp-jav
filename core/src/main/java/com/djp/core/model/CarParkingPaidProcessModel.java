package com.djp.core.model;


import jakarta.persistence.*;

@Entity
@Table(name = "CarParkingPaidProcess")
@PrimaryKeyJoinColumn(name = "id")
public class CarParkingPaidProcessModel extends BusinessProcessModel{

    @ManyToOne
    @JoinColumn
    private ParkingWProcessModel parkingWProcess;

    public CarParkingPaidProcessModel() {
        super();
    }

    public ParkingWProcessModel getParkingWProcess() {
        return parkingWProcess;
    }

    public void setParkingWProcess(ParkingWProcessModel parkingWProcess) {
        this.parkingWProcess = parkingWProcess;
    }
}
