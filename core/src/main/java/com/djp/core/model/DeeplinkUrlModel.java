package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="DeeplinkUrl")
public class DeeplinkUrlModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private String name;
    private String baseUrl;

    @OneToMany(mappedBy = "deeplinkUrl", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<BarcodeMediaModel> barcodeMedias;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Set<BarcodeMediaModel> getBarcodeMedias() {
        return barcodeMedias;
    }

    public void setBarcodeMedias(Set<BarcodeMediaModel> barcodeMedias) {
        this.barcodeMedias = barcodeMedias;
    }
}
