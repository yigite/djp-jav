package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DeliveryMode")
public class DeliveryModeModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String code;

    private boolean active;
    private String name;
    private String description;
    private String supportedPaymentModesInternal;

    @ManyToMany(mappedBy = "supportedDeliveryModes", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<PaymentModeModel> supportedPaymentModes;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="BaseStore2DeliveryModeRel",
            joinColumns={@JoinColumn(name="deliveryMode")},
            inverseJoinColumns={@JoinColumn(name="baseStore")})
    private Set<BaseStoreModel> stores;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupportedPaymentModesInternal() {
        return supportedPaymentModesInternal;
    }

    public void setSupportedPaymentModesInternal(String supportedPaymentModesInternal) {
        this.supportedPaymentModesInternal = supportedPaymentModesInternal;
    }

    public Set<PaymentModeModel> getSupportedPaymentModes() {
        return supportedPaymentModes;
    }

    public void setSupportedPaymentModes(Set<PaymentModeModel> supportedPaymentModes) {
        this.supportedPaymentModes = supportedPaymentModes;
    }

    public Set<BaseStoreModel> getStores() {
        return stores;
    }

    public void setStores(Set<BaseStoreModel> stores) {
        this.stores = stores;
    }
}
