package com.djp.core.model;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "ParkingWTransaction")
public class ParkingWTransactionModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String currency;
    private String date;
    private boolean isOverdue;
    private BigDecimal valueWithoutDiscount;
    private BigDecimal value;

    @OneToMany(mappedBy = "parkingWTransaction", fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ParkingWValidationModel> parkingWValidations;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isOverdue() {
        return isOverdue;
    }

    public void setOverdue(boolean overdue) {
        isOverdue = overdue;
    }

    public BigDecimal getValueWithoutDiscount() {
        return valueWithoutDiscount;
    }

    public void setValueWithoutDiscount(BigDecimal valueWithoutDiscount) {
        this.valueWithoutDiscount = valueWithoutDiscount;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Set<ParkingWValidationModel> getParkingWValidations() {
        return parkingWValidations;
    }

    public void setParkingWValidations(Set<ParkingWValidationModel> parkingWValidations) {
        this.parkingWValidations = parkingWValidations;
    }
}
