package com.djp.core.model;

import com.djp.core.enums.*;
import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "AbstractOrderEntry")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractOrderEntryModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Double basePrice;
    private boolean calculated;
    private String discountValuesInternal;
    private String taxValuesInternal;
    private int entryNumber;
    private Integer quantity;
    private Double totalPrice;
    private String info;
    private boolean giveAway;
    private boolean rejected;
    private Date namedDeliveryDate;
    private String externalConfiguration;
    private FeatureEnum featureTypeForZmmiga4;
    private String specialNote;
    private OrderEntryStatus quantityStatus;
    private Date arrivalFlightDate;
    private String arrivalFlightCode;
    private Date departureFlightDate;
    private String departureFlightCode;
    private Date customerArrivalDate;
    private String serviceTypeCode;
    private Long passengerQty;
    private Integer bundleNo;

    @ElementCollection
    @CollectionTable(name="EntryGroupNumbersList", joinColumns=@JoinColumn(name="abstractOrderEntry"))
    @Column(name="entryGroupNumber")
    private Set<Integer> entryGroupNumbers;

    @ManyToMany(mappedBy = "entries", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<DjpAdditionalServiceEntryModel> additionalService;

    @OneToMany
    @JoinColumn
    private Set<DiscountValueModel> discountValues;

    @OneToMany
    @JoinColumn
    private Set<TaxValueModel> taxValues;

    private ProductPriceGroup Europe1PriceFactory_PPG;
    private ProductTaxGroup Europe1PriceFactory_PTG;
    private ProductDiscountGroup Europe1PriceFactory_PDG;

    @OneToOne
    private ProductModel product;

    @OneToOne
    private UnitModel unit;

   @OneToMany(mappedBy = "orderEntry", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
   private List<AbstractOrderEntryProductInfoModel> productInfos;

    @OneToMany(mappedBy = "orderEntry", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ConsignmentEntryModel> consignmentEntries;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    @OneToOne
    private VendorModel chosenVendor;

    @OneToOne
    private AddressModel deliveryAddress;

    @OneToOne
    private DeliveryModeModel deliveryMode;

    @OneToOne
    private PointOfServiceModel deliveryPointOfService;

    @OneToMany(mappedBy = "orderEntry", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<SAPPricingConditionModel> sapPricingConditions;

    @OneToOne
    private BundleTemplateModel bundleTemplate;

    @OneToOne
    private DjpMeetingPassengerInfoModel djpMeetingPassengerInfoModel;

    @OneToOne
    private DjpSleepodPassengerInfoModel djpSleepodPassengerInfo;

    @ManyToMany(mappedBy = "entries", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<DjpOtherCustomerModel> guestCustomers;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel parentOrder;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public boolean isCalculated() {
        return calculated;
    }

    public void setCalculated(boolean calculated) {
        this.calculated = calculated;
    }

    public String getDiscountValuesInternal() {
        return discountValuesInternal;
    }

    public void setDiscountValuesInternal(String discountValuesInternal) {
        this.discountValuesInternal = discountValuesInternal;
    }

    public String getTaxValuesInternal() {
        return taxValuesInternal;
    }

    public void setTaxValuesInternal(String taxValuesInternal) {
        this.taxValuesInternal = taxValuesInternal;
    }

    public int getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(int entryNumber) {
        this.entryNumber = entryNumber;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isGiveAway() {
        return giveAway;
    }

    public void setGiveAway(boolean giveAway) {
        this.giveAway = giveAway;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

    public Date getNamedDeliveryDate() {
        return namedDeliveryDate;
    }

    public void setNamedDeliveryDate(Date namedDeliveryDate) {
        this.namedDeliveryDate = namedDeliveryDate;
    }

    public String getExternalConfiguration() {
        return externalConfiguration;
    }

    public void setExternalConfiguration(String externalConfiguration) {
        this.externalConfiguration = externalConfiguration;
    }

    public FeatureEnum getFeatureTypeForZmmiga4() {
        return featureTypeForZmmiga4;
    }

    public void setFeatureTypeForZmmiga4(FeatureEnum featureTypeForZmmiga4) {
        this.featureTypeForZmmiga4 = featureTypeForZmmiga4;
    }

    public String getSpecialNote() {
        return specialNote;
    }

    public void setSpecialNote(String specialNote) {
        this.specialNote = specialNote;
    }

    public OrderEntryStatus getQuantityStatus() {
        return quantityStatus;
    }

    public void setQuantityStatus(OrderEntryStatus quantityStatus) {
        this.quantityStatus = quantityStatus;
    }

    public Date getArrivalFlightDate() {
        return arrivalFlightDate;
    }

    public void setArrivalFlightDate(Date arrivalFlightDate) {
        this.arrivalFlightDate = arrivalFlightDate;
    }

    public String getArrivalFlightCode() {
        return arrivalFlightCode;
    }

    public void setArrivalFlightCode(String arrivalFlightCode) {
        this.arrivalFlightCode = arrivalFlightCode;
    }

    public Date getDepartureFlightDate() {
        return departureFlightDate;
    }

    public void setDepartureFlightDate(Date departureFlightDate) {
        this.departureFlightDate = departureFlightDate;
    }

    public String getDepartureFlightCode() {
        return departureFlightCode;
    }

    public void setDepartureFlightCode(String departureFlightCode) {
        this.departureFlightCode = departureFlightCode;
    }

    public Date getCustomerArrivalDate() {
        return customerArrivalDate;
    }

    public void setCustomerArrivalDate(Date customerArrivalDate) {
        this.customerArrivalDate = customerArrivalDate;
    }

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }

    public Long getPassengerQty() {
        return passengerQty;
    }

    public void setPassengerQty(Long passengerQty) {
        this.passengerQty = passengerQty;
    }

    public Integer getBundleNo() {
        return bundleNo;
    }

    public void setBundleNo(Integer bundleNo) {
        this.bundleNo = bundleNo;
    }

    public Set<Integer> getEntryGroupNumbers() {
        return entryGroupNumbers;
    }

    public void setEntryGroupNumbers(Set<Integer> entryGroupNumbers) {
        this.entryGroupNumbers = entryGroupNumbers;
    }

    public Set<DjpAdditionalServiceEntryModel> getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(Set<DjpAdditionalServiceEntryModel> additionalService) {
        this.additionalService = additionalService;
    }

    public Set<DiscountValueModel> getDiscountValues() {
        return discountValues;
    }

    public void setDiscountValues(Set<DiscountValueModel> discountValues) {
        this.discountValues = discountValues;
    }

    public Set<TaxValueModel> getTaxValues() {
        return taxValues;
    }

    public void setTaxValues(Set<TaxValueModel> taxValues) {
        this.taxValues = taxValues;
    }

    public ProductPriceGroup getEurope1PriceFactory_PPG() {
        return Europe1PriceFactory_PPG;
    }

    public void setEurope1PriceFactory_PPG(ProductPriceGroup europe1PriceFactory_PPG) {
        Europe1PriceFactory_PPG = europe1PriceFactory_PPG;
    }

    public ProductTaxGroup getEurope1PriceFactory_PTG() {
        return Europe1PriceFactory_PTG;
    }

    public void setEurope1PriceFactory_PTG(ProductTaxGroup europe1PriceFactory_PTG) {
        Europe1PriceFactory_PTG = europe1PriceFactory_PTG;
    }

    public ProductDiscountGroup getEurope1PriceFactory_PDG() {
        return Europe1PriceFactory_PDG;
    }

    public void setEurope1PriceFactory_PDG(ProductDiscountGroup europe1PriceFactory_PDG) {
        Europe1PriceFactory_PDG = europe1PriceFactory_PDG;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public UnitModel getUnit() {
        return unit;
    }

    public void setUnit(UnitModel unit) {
        this.unit = unit;
    }

    public List<AbstractOrderEntryProductInfoModel> getProductInfos() {
        return productInfos;
    }

    public void setProductInfos(List<AbstractOrderEntryProductInfoModel> productInfos) {
        this.productInfos = productInfos;
    }

    public Set<ConsignmentEntryModel> getConsignmentEntries() {
        return consignmentEntries;
    }

    public void setConsignmentEntries(Set<ConsignmentEntryModel> consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public VendorModel getChosenVendor() {
        return chosenVendor;
    }

    public void setChosenVendor(VendorModel chosenVendor) {
        this.chosenVendor = chosenVendor;
    }

    public AddressModel getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressModel deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public DeliveryModeModel getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(DeliveryModeModel deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public PointOfServiceModel getDeliveryPointOfService() {
        return deliveryPointOfService;
    }

    public void setDeliveryPointOfService(PointOfServiceModel deliveryPointOfService) {
        this.deliveryPointOfService = deliveryPointOfService;
    }

    public Set<SAPPricingConditionModel> getSapPricingConditions() {
        return sapPricingConditions;
    }

    public void setSapPricingConditions(Set<SAPPricingConditionModel> sapPricingConditions) {
        this.sapPricingConditions = sapPricingConditions;
    }

    public BundleTemplateModel getBundleTemplate() {
        return bundleTemplate;
    }

    public void setBundleTemplate(BundleTemplateModel bundleTemplate) {
        this.bundleTemplate = bundleTemplate;
    }

    public DjpMeetingPassengerInfoModel getDjpMeetingPassengerInfoModel() {
        return djpMeetingPassengerInfoModel;
    }

    public void setDjpMeetingPassengerInfoModel(DjpMeetingPassengerInfoModel djpMeetingPassengerInfoModel) {
        this.djpMeetingPassengerInfoModel = djpMeetingPassengerInfoModel;
    }

    public DjpSleepodPassengerInfoModel getDjpSleepodPassengerInfo() {
        return djpSleepodPassengerInfo;
    }

    public void setDjpSleepodPassengerInfo(DjpSleepodPassengerInfoModel djpSleepodPassengerInfo) {
        this.djpSleepodPassengerInfo = djpSleepodPassengerInfo;
    }

    public Set<DjpOtherCustomerModel> getGuestCustomers() {
        return guestCustomers;
    }

    public void setGuestCustomers(Set<DjpOtherCustomerModel> guestCustomers) {
        this.guestCustomers = guestCustomers;
    }

    public AbstractOrderModel getParentOrder() {
        return parentOrder;
    }

    public void setParentOrder(AbstractOrderModel parentOrder) {
        this.parentOrder = parentOrder;
    }
}
