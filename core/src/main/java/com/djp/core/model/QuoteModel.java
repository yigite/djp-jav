package com.djp.core.model;

import com.djp.core.enums.QuoteNotificationType;
import com.djp.core.enums.QuoteState;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Quote", indexes = @Index(columnList = "quoteCode, version", name = "version_idx"))
@PrimaryKeyJoinColumn(name = "id")
public class QuoteModel extends AbstractOrderModel{

    private int version;
    private double previousEstimatedTotal;
    private QuoteState state;

    @ManyToOne
    @JoinColumn(name = "parent")
    private AbstractOrderModel parent;

    @Column
    private String quoteCode;

    @ManyToOne
    @JoinColumn
    private UserModel assignee;

    @OneToOne
    @JoinColumn
    private CartModel cartReference;

    @ElementCollection(targetClass = QuoteNotificationType.class)
    @CollectionTable(name = "GeneratedNotificationsForQuote", joinColumns = @JoinColumn(name = "quote"))
    @Enumerated(EnumType.STRING)
    @Column(name = "notification")
    private Set<QuoteNotificationType> generatedNotifications;

    public QuoteModel()
    {
        super();
    }

    public int getVersion() {
        return version;
    }

    public AbstractOrderModel getParent() {
        return parent;
    }

    public void setParent(AbstractOrderModel parent) {
        this.parent = parent;
    }

    public String getCode() {
        return super.getCode();
    }

    public String getQuoteCode() {
        return this.quoteCode;
    }

    public void setCode(String code) {
        super.setCode(code);
        this.quoteCode = code;
    }

    public void setQuoteCode(String code) {
        super.setCode(code);
        this.quoteCode = code;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public double getPreviousEstimatedTotal() {
        return previousEstimatedTotal;
    }

    public void setPreviousEstimatedTotal(double previousEstimatedTotal) {
        this.previousEstimatedTotal = previousEstimatedTotal;
    }

    public QuoteState getState() {
        return state;
    }

    public void setState(QuoteState state) {
        this.state = state;
    }

    public UserModel getAssignee() {
        return assignee;
    }

    public void setAssignee(UserModel assignee) {
        this.assignee = assignee;
    }

    public CartModel getCartReference() {
        return cartReference;
    }

    public void setCartReference(CartModel cartReference) {
        this.cartReference = cartReference;
    }

    public Set<QuoteNotificationType> getGeneratedNotifications() {
        return generatedNotifications;
    }

    public void setGeneratedNotifications(Set<QuoteNotificationType> generatedNotifications) {
        this.generatedNotifications = generatedNotifications;
    }
}
