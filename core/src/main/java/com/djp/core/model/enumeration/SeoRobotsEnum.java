package com.djp.core.model.enumeration;

public enum SeoRobotsEnum {


    INDEX_FOLLOW("ACTIVE"),
    OWINDEX_NOFOLLOW("OWINDEX_NOFOLLOW"),
    NOINDEX_FOLLOW("NOINDEX_FOLLOW"),
    NOINDEX_NOFOLLOW("NOINDEX_NOFOLLOW");


    private final String code;

    SeoRobotsEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
