package com.djp.core.model;

import com.djp.core.enums.BundleTemplateStatusEnum;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "BundleTemplateStatus")
public class BundleTemplateStatusModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    private BundleTemplateStatusEnum status;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE})
    private Set<BundleTemplateModel> bundleTemplates;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public BundleTemplateStatusEnum getStatus() {
        return status;
    }

    public void setStatus(BundleTemplateStatusEnum status) {
        this.status = status;
    }

    public Set<BundleTemplateModel> getBundleTemplates() {
        return bundleTemplates;
    }

    public void setBundleTemplates(Set<BundleTemplateModel> bundleTemplates) {
        this.bundleTemplates = bundleTemplates;
    }
}
