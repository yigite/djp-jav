package com.djp.core.model;


import com.djp.core.enums.LineOfBusiness;
import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "Company")
@PrimaryKeyJoinColumn(name = "uid")
public class CompanyModel extends UserGroupModel{

    @Column(unique = true)
    private String id;
    private String dunsID;
    private String ilnID;
    private String buyerSpecificID;
    private String supplierSpecificID;
    private String vatID;
    private boolean buyer;
    private boolean supplier;
    private boolean manufacturer;
    private boolean carrier;

    private LineOfBusiness lineOfBusiness;

    @OneToMany(mappedBy = "supplier", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CatalogModel> providedCatalogs;

    @OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CatalogModel> purchasedCatalogs;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<AddressModel> addresses;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<MediaModel> medias;

    @OneToOne(cascade = CascadeType.ALL)
    private AddressModel shippingAddress;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<AddressModel> shippingAddresses;

    @OneToOne(cascade = CascadeType.ALL)
    private AddressModel unloadingAddress;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<AddressModel> unloadingAddresses;

    @OneToOne(cascade = CascadeType.ALL)
    private AddressModel billingAddress;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<AddressModel> billingAddresses;

    @OneToOne(cascade = CascadeType.ALL)
    private AddressModel contactAddress;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<AddressModel> contactAddresses;

    @OneToOne
    private UserModel contact;

    @OneToOne
    private CountryModel country;

    @OneToOne
    private CompanyModel responsibleCompany;

    public CompanyModel() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDunsID() {
        return dunsID;
    }

    public void setDunsID(String dunsID) {
        this.dunsID = dunsID;
    }

    public String getIlnID() {
        return ilnID;
    }

    public void setIlnID(String ilnID) {
        this.ilnID = ilnID;
    }

    public String getBuyerSpecificID() {
        return buyerSpecificID;
    }

    public void setBuyerSpecificID(String buyerSpecificID) {
        this.buyerSpecificID = buyerSpecificID;
    }

    public String getSupplierSpecificID() {
        return supplierSpecificID;
    }

    public void setSupplierSpecificID(String supplierSpecificID) {
        this.supplierSpecificID = supplierSpecificID;
    }

    public String getVatID() {
        return vatID;
    }

    public void setVatID(String vatID) {
        this.vatID = vatID;
    }

    public boolean isBuyer() {
        return buyer;
    }

    public void setBuyer(boolean buyer) {
        this.buyer = buyer;
    }

    public boolean isSupplier() {
        return supplier;
    }

    public void setSupplier(boolean supplier) {
        this.supplier = supplier;
    }

    public boolean isManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(boolean manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isCarrier() {
        return carrier;
    }

    public void setCarrier(boolean carrier) {
        this.carrier = carrier;
    }

    public LineOfBusiness getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(LineOfBusiness lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public Set<CatalogModel> getProvidedCatalogs() {
        return providedCatalogs;
    }

    public void setProvidedCatalogs(Set<CatalogModel> providedCatalogs) {
        this.providedCatalogs = providedCatalogs;
    }

    public Set<CatalogModel> getPurchasedCatalogs() {
        return purchasedCatalogs;
    }

    public void setPurchasedCatalogs(Set<CatalogModel> purchasedCatalogs) {
        this.purchasedCatalogs = purchasedCatalogs;
    }

    public Set<AddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressModel> addresses) {
        this.addresses = addresses;
    }

    public Set<MediaModel> getMedias() {
        return medias;
    }

    public void setMedias(Set<MediaModel> medias) {
        this.medias = medias;
    }

    public AddressModel getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AddressModel shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Set<AddressModel> getShippingAddresses() {
        return shippingAddresses;
    }

    public void setShippingAddresses(Set<AddressModel> shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
    }

    public AddressModel getUnloadingAddress() {
        return unloadingAddress;
    }

    public void setUnloadingAddress(AddressModel unloadingAddress) {
        this.unloadingAddress = unloadingAddress;
    }

    public Set<AddressModel> getUnloadingAddresses() {
        return unloadingAddresses;
    }

    public void setUnloadingAddresses(Set<AddressModel> unloadingAddresses) {
        this.unloadingAddresses = unloadingAddresses;
    }

    public AddressModel getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AddressModel billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Set<AddressModel> getBillingAddresses() {
        return billingAddresses;
    }

    public void setBillingAddresses(Set<AddressModel> billingAddresses) {
        this.billingAddresses = billingAddresses;
    }

    public AddressModel getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(AddressModel contactAddress) {
        this.contactAddress = contactAddress;
    }

    public Set<AddressModel> getContactAddresses() {
        return contactAddresses;
    }

    public void setContactAddresses(Set<AddressModel> contactAddresses) {
        this.contactAddresses = contactAddresses;
    }

    public UserModel getContact() {
        return contact;
    }

    public void setContact(UserModel contact) {
        this.contact = contact;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }

    public CompanyModel getResponsibleCompany() {
        return responsibleCompany;
    }

    public void setResponsibleCompany(CompanyModel responsibleCompany) {
        this.responsibleCompany = responsibleCompany;
    }
}
