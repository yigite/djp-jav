package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpUsageSendToExternalCompanyProcess")
public class DjpUsageSendToExternalCompanyProcessModel extends BusinessProcessModel{

    @Transient
    private ItemModel qr;

    public ItemModel getQr() {
        return qr;
    }

    public void setQr(ItemModel qr) {
        this.qr = qr;
    }
}
