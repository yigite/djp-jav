package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "BatchJob")
@PrimaryKeyJoinColumn(name = "id")
public class BatchJobModel extends JobModel {

    @OneToMany(mappedBy = "batchJob", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<StepModel> steps;

    public BatchJobModel() {
        super();
    }

    public Set<StepModel> getSteps() {
        return steps;
    }

    public void setSteps(Set<StepModel> steps) {
        this.steps = steps;
    }
}
