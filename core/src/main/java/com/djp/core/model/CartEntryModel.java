package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Cart")
@PrimaryKeyJoinColumn(name = "id")
public class CartEntryModel extends AbstractOrderEntryModel {

    @ManyToOne
    @JoinColumn
    private CartModel lastModifiedMasterCart;

    public CartEntryModel() {
        super();
    }

    @Override
    public void setOrder(AbstractOrderModel order) {
        if( order == null || order instanceof CartModel)
        {
            super.setOrder(order);
        }
        else
        {
            throw new IllegalArgumentException("Given value is not instance of com.djp.core.model.CartModel");
        }
    }

    @Override
    public CartModel getOrder()
    {
        return (CartModel) super.getOrder();
    }

    public CartModel getLastModifiedMasterCart() {
        return lastModifiedMasterCart;
    }

    public void setLastModifiedMasterCart(CartModel lastModifiedMasterCart) {
        this.lastModifiedMasterCart = lastModifiedMasterCart;
    }
}
