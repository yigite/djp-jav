package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Agreement")
public class AgreementModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @Column(unique = true)
    private String id;

    private Date startdate;
    private Date enddate;

    @ManyToOne
    @JoinColumn
    private CatalogModel catalog;

    @OneToOne
    private CompanyModel buyer;

    @OneToOne
    private CompanyModel supplier;

    @OneToOne
    private CurrencyModel currency;

    @OneToOne
    private UserModel buyerContact;

    @OneToOne
    private UserModel supplierContact;

    @ManyToOne
    @JoinColumn
    private CatalogVersionModel catalogVersion;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public CatalogModel getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogModel catalog) {
        this.catalog = catalog;
    }

    public CompanyModel getBuyer() {
        return buyer;
    }

    public void setBuyer(CompanyModel buyer) {
        this.buyer = buyer;
    }

    public CompanyModel getSupplier() {
        return supplier;
    }

    public void setSupplier(CompanyModel supplier) {
        this.supplier = supplier;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public UserModel getBuyerContact() {
        return buyerContact;
    }

    public void setBuyerContact(UserModel buyerContact) {
        this.buyerContact = buyerContact;
    }

    public UserModel getSupplierContact() {
        return supplierContact;
    }

    public void setSupplierContact(UserModel supplierContact) {
        this.supplierContact = supplierContact;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }
}
