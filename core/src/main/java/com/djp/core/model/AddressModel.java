package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Address")
public class AddressModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String passportNumber;
    private String taxNumber;
    private String taxOffice;
    private String tckn;

    private String company;

    private String phone1;

    private String line1;

    private String email;
    private String addressTitle;
    private Boolean isPersonal;
    private Boolean isForeigner;
    @OneToOne
    private NationalityModel nationality;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "user2LanguageRel",
            joinColumns = {@JoinColumn(name = "address")},
            inverseJoinColumns = {@JoinColumn(name = "user")})
    private Set<BaseStoreModel> baseStoreSet;

    @ManyToOne
    private UserModel user;

    @OneToOne(mappedBy = "billingAddress")
    private PaymentInfoModel paymentInfo;

    @OneToMany(mappedBy = "deliveryAddress", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartToOrderCronJobModel> deliveryAddresss2CartToOrderCronJob;

    @OneToMany(mappedBy = "paymentAddress", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartToOrderCronJobModel> paymentAddresss2CartToOrderCronJob;

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public NationalityModel getNationality() {
        return nationality;
    }

    public void setNationality(NationalityModel nationality) {
        this.nationality = nationality;
    }

    public Boolean getPersonal() {
        return isPersonal;
    }

    public void setPersonal(Boolean personal) {
        isPersonal = personal;
    }

    public Boolean getForeigner() {
        return isForeigner;
    }

    public void setForeigner(Boolean foreigner) {
        isForeigner = foreigner;
    }

//    public Set<BaseStoreModel> getBaseStoreSet() {
//        return baseStoreSet;
//    }
//
//    public void setBaseStoreSet(Set<BaseStoreModel> baseStoreSet) {
//        this.baseStoreSet = baseStoreSet;
//    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PaymentInfoModel getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoModel paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Set<BaseStoreModel> getBaseStoreSet() {
        return baseStoreSet;
    }

    public void setBaseStoreSet(Set<BaseStoreModel> baseStoreSet) {
        this.baseStoreSet = baseStoreSet;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<CartToOrderCronJobModel> getDeliveryAddresss2CartToOrderCronJob() {
        return deliveryAddresss2CartToOrderCronJob;
    }

    public void setDeliveryAddresss2CartToOrderCronJob(Set<CartToOrderCronJobModel> deliveryAddresss2CartToOrderCronJob) {
        this.deliveryAddresss2CartToOrderCronJob = deliveryAddresss2CartToOrderCronJob;
    }
}
