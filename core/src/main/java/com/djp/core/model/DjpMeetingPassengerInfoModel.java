package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpMeetingPassengerInfo")
public class DjpMeetingPassengerInfoModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String contactToBeMet;
    private String greetingsName;
    private String greetingsPhone;
    private boolean isMyGreets;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContactToBeMet() {
        return contactToBeMet;
    }

    public void setContactToBeMet(String contactToBeMet) {
        this.contactToBeMet = contactToBeMet;
    }

    public String getGreetingsName() {
        return greetingsName;
    }

    public void setGreetingsName(String greetingsName) {
        this.greetingsName = greetingsName;
    }

    public String getGreetingsPhone() {
        return greetingsPhone;
    }

    public void setGreetingsPhone(String greetingsPhone) {
        this.greetingsPhone = greetingsPhone;
    }

    public boolean isMyGreets() {
        return isMyGreets;
    }

    public void setMyGreets(boolean myGreets) {
        isMyGreets = myGreets;
    }
}
