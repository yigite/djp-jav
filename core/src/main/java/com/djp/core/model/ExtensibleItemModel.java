
package com.djp.core.model;

import java.util.Collections;
import java.util.Map;

import com.djp.modelservice.model.ItemModel;
import org.apache.commons.collections.map.CaseInsensitiveMap;

public abstract class ExtensibleItemModel extends ItemModel {
    private Map jaloOnlyPropertyValues;

    public ExtensibleItemModel() {
    }


    private Map getJaloOnlyPropertyMap(boolean createNew) {
        if (this.jaloOnlyPropertyValues == null && createNew) {
            this.jaloOnlyPropertyValues = new CaseInsensitiveMap();
        }

        return this.jaloOnlyPropertyValues != null ? this.jaloOnlyPropertyValues : Collections.EMPTY_MAP;
    }

}
