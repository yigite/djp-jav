package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "QrForCard")
public class QrForCardModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cardKey;
    private String cardId;


    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForCardModel_QrType_whereIsValidQr", joinColumns = @JoinColumn(name = "QrForCardModel_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereIsValidQr;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForCardModel_QrType_whereUsed", joinColumns = @JoinColumn(name = "QrForCardModel_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereUsed;

    private Date startDate;
    private Date expireDate;
    private String companyId;
    private Boolean usedForCampaign;
    private String phoneNumber;
    private Integer guestQuantity;

    @OneToMany
    private Set<DjpPassLocationsModel> validAtWhichLocation;

    @OneToOne
    private DjpUniqueTokenModel token;
    private Boolean isCancelled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCardKey() {
        return cardKey;
    }

    public void setCardKey(String cardKey) {
        this.cardKey = cardKey;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Set<QrType> getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(Set<QrType> whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public Set<QrType> getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(Set<QrType> whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getGuestQuantity() {
        return guestQuantity;
    }

    public void setGuestQuantity(Integer guestQuantity) {
        this.guestQuantity = guestQuantity;
    }

    public Set<DjpPassLocationsModel> getValidAtWhichLocation() {
        return validAtWhichLocation;
    }

    public void setValidAtWhichLocation(Set<DjpPassLocationsModel> validAtWhichLocation) {
        this.validAtWhichLocation = validAtWhichLocation;
    }

    public DjpUniqueTokenModel getToken() {
        return token;
    }

    public void setToken(DjpUniqueTokenModel token) {
        this.token = token;
    }

    public Boolean getCancelled() {
        return isCancelled;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }
}
