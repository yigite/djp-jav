package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OpeningSchedule")
public class OpeningScheduleModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private String name;

    @OneToMany(mappedBy = "openingSchedule", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OpeningDayModel> openingDays;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<OpeningDayModel> getOpeningDays() {
        return openingDays;
    }

    public void setOpeningDays(Set<OpeningDayModel> openingDays) {
        this.openingDays = openingDays;
    }
}
