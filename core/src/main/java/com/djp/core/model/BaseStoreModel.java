package com.djp.core.model;

import com.djp.core.model.enumeration.PickupInStoreMode;
import com.djp.core.enums.UserTaxGroup;
import com.djp.modelservice.model.ItemModel;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "BaseStore")
public class BaseStoreModel extends ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String Uid;

    private String name;

    @OneToOne
    private DistanceUnitModel storeLocatorDistanceUnit;

    private UserTaxGroup taxGroup;

    @OneToOne(fetch = FetchType.EAGER)
    private LanguageModel defaultLanguage;

    @OneToOne(fetch = FetchType.EAGER)
    private CurrencyModel defaultCurrency;

    @OneToMany(mappedBy = "baseStore")
    private Set<PointOfServiceModel> defaultDeliveryOrigin;

    @OneToOne
    private SolrFacetSearchConfigModel solrFacetSearchConfigurationModel;

    private PickupInStoreMode pickupInStoreMode;

    @OneToMany
    private Set<CountryModel> billingCountries;

    private Double maxRadiusForPoSSearch;
    private Double djpReceiptPriceLimit;
    private Integer individualLengthOfStayHour;
    private Boolean isWelcomeToDjpMessageSendOpen;
    private String welcomeToDjpText;
    private Boolean captchaCheckEnabled;
    private Boolean walletEnabled;
    private Boolean net;
    private String submitOrderProcessCode;
    private String createReturnProcessCode;
    private Boolean externalTaxEnabled;
    private Boolean customerAllowedToIgnoreSuggestions;
    private String paymentProvider;
    private Boolean expressCheckoutEnabled;
    private Boolean taxEstimationEnabled;
    private String checkoutFlowGroup;


    @ManyToMany(mappedBy = "stores", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<DeliveryModeModel> deliveryModes;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="BaseStore2WarehouseRel",
            joinColumns={@JoinColumn(name="baseStores")},
            inverseJoinColumns={@JoinColumn(name="warehouses")})
    private Set<WarehouseModel> warehouses;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn(insertable = false, updatable = false)
    private Set<PointOfServiceModel> pointsOfService;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="Catalogs4BaseStores",
            joinColumns={@JoinColumn(name="baseStore")},
            inverseJoinColumns={@JoinColumn(name="catalog")})
    private Set<CatalogModel> catalogs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserTaxGroup getTaxGroup() {
        return taxGroup;
    }

    public void setTaxGroup(UserTaxGroup taxGroup) {
        this.taxGroup = taxGroup;
    }

    public LanguageModel getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageModel defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public CurrencyModel getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyModel defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Set<PointOfServiceModel> getDefaultDeliveryOrigin() {
        return defaultDeliveryOrigin;
    }

    public void setDefaultDeliveryOrigin(Set<PointOfServiceModel> defaultDeliveryOrigin) {
        this.defaultDeliveryOrigin = defaultDeliveryOrigin;
    }

    public SolrFacetSearchConfigModel getSolrFacetSearchConfigurationModel() {
        return solrFacetSearchConfigurationModel;
    }

    public void setSolrFacetSearchConfigurationModel(SolrFacetSearchConfigModel solrFacetSearchConfigurationModel) {
        this.solrFacetSearchConfigurationModel = solrFacetSearchConfigurationModel;
    }

    public void setStoreLocatorDistanceUnit(DistanceUnitModel storeLocatorDistanceUnit) {
        this.storeLocatorDistanceUnit = storeLocatorDistanceUnit;
    }

    public PickupInStoreMode getPickupInStoreMode() {
        return pickupInStoreMode;
    }

    public void setPickupInStoreMode(PickupInStoreMode pickupInStoreMode) {
        this.pickupInStoreMode = pickupInStoreMode;
    }

    public Double getMaxRadiusForPoSSearch() {
        return maxRadiusForPoSSearch;
    }

    public void setMaxRadiusForPoSSearch(Double maxRadiusForPoSSearch) {
        this.maxRadiusForPoSSearch = maxRadiusForPoSSearch;
    }

    public Double getDjpReceiptPriceLimit() {
        return djpReceiptPriceLimit;
    }

    public void setDjpReceiptPriceLimit(Double djpReceiptPriceLimit) {
        this.djpReceiptPriceLimit = djpReceiptPriceLimit;
    }

    public Integer getIndividualLengthOfStayHour() {
        return individualLengthOfStayHour;
    }

    public void setIndividualLengthOfStayHour(Integer individualLengthOfStayHour) {
        this.individualLengthOfStayHour = individualLengthOfStayHour;
    }

    public Boolean getWelcomeToDjpMessageSendOpen() {
        return isWelcomeToDjpMessageSendOpen;
    }

    public void setWelcomeToDjpMessageSendOpen(Boolean welcomeToDjpMessageSendOpen) {
        isWelcomeToDjpMessageSendOpen = welcomeToDjpMessageSendOpen;
    }

    public String getWelcomeToDjpText() {
        return welcomeToDjpText;
    }

    public void setWelcomeToDjpText(String welcomeToDjpText) {
        this.welcomeToDjpText = welcomeToDjpText;
    }

    public Boolean getCaptchaCheckEnabled() {
        return captchaCheckEnabled;
    }

    public void setCaptchaCheckEnabled(Boolean captchaCheckEnabled) {
        this.captchaCheckEnabled = captchaCheckEnabled;
    }

    public Boolean getWalletEnabled() {
        return walletEnabled;
    }

    public void setWalletEnabled(Boolean walletEnabled) {
        this.walletEnabled = walletEnabled;
    }

    public Boolean getNet() {
        return net;
    }

    public void setNet(Boolean net) {
        this.net = net;
    }

    public String getSubmitOrderProcessCode() {
        return submitOrderProcessCode;
    }

    public void setSubmitOrderProcessCode(String submitOrderProcessCode) {
        this.submitOrderProcessCode = submitOrderProcessCode;
    }

    public String getCreateReturnProcessCode() {
        return createReturnProcessCode;
    }

    public void setCreateReturnProcessCode(String createReturnProcessCode) {
        this.createReturnProcessCode = createReturnProcessCode;
    }

    public Boolean getExternalTaxEnabled() {
        return externalTaxEnabled;
    }

    public void setExternalTaxEnabled(Boolean externalTaxEnabled) {
        this.externalTaxEnabled = externalTaxEnabled;
    }

    public Boolean getCustomerAllowedToIgnoreSuggestions() {
        return customerAllowedToIgnoreSuggestions;
    }

    public void setCustomerAllowedToIgnoreSuggestions(Boolean customerAllowedToIgnoreSuggestions) {
        this.customerAllowedToIgnoreSuggestions = customerAllowedToIgnoreSuggestions;
    }

    public String getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(String paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public Boolean getExpressCheckoutEnabled() {
        return expressCheckoutEnabled;
    }

    public void setExpressCheckoutEnabled(Boolean expressCheckoutEnabled) {
        this.expressCheckoutEnabled = expressCheckoutEnabled;
    }

    public Boolean getTaxEstimationEnabled() {
        return taxEstimationEnabled;
    }

    public void setTaxEstimationEnabled(Boolean taxEstimationEnabled) {
        this.taxEstimationEnabled = taxEstimationEnabled;
    }

    public String getCheckoutFlowGroup() {
        return checkoutFlowGroup;
    }

    public void setCheckoutFlowGroup(String checkoutFlowGroup) {
        this.checkoutFlowGroup = checkoutFlowGroup;
    }

    public DistanceUnitModel getStoreLocatorDistanceUnit() {
        return storeLocatorDistanceUnit;
    }

    public Set<CountryModel> getBillingCountries() {
        return billingCountries;
    }

    public void setBillingCountries(Set<CountryModel> billingCountries) {
        this.billingCountries = billingCountries;
    }

    public Set<DeliveryModeModel> getDeliveryModes() {
        return deliveryModes;
    }

    public void setDeliveryModes(Set<DeliveryModeModel> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    public Set<WarehouseModel> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Set<WarehouseModel> warehouses) {
        this.warehouses = warehouses;
    }

    public Set<PointOfServiceModel> getPointsOfService() {
        return pointsOfService;
    }

    public void setPointsOfService(Set<PointOfServiceModel> pointsOfService) {
        this.pointsOfService = pointsOfService;
    }

    public Set<CatalogModel> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(Set<CatalogModel> catalogs) {
        this.catalogs = catalogs;
    }
}
