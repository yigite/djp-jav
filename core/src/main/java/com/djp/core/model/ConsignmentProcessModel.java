package com.djp.core.model;

import com.djp.core.enums.WarehouseConsignmentState;

import jakarta.persistence.*;

@Entity
@Table(name = "ConsignmentProcess")
@PrimaryKeyJoinColumn(name = "id")
public class ConsignmentProcessModel extends BusinessProcessModel {

    private boolean done;
    private boolean waitingForConsignment;
    private WarehouseConsignmentState warehouseConsignmentState;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private ConsignmentModel consignment;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private OrderProcessModel parentProcess;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isWaitingForConsignment() {
        return waitingForConsignment;
    }

    public void setWaitingForConsignment(boolean waitingForConsignment) {
        this.waitingForConsignment = waitingForConsignment;
    }

    public WarehouseConsignmentState getWarehouseConsignmentState() {
        return warehouseConsignmentState;
    }

    public void setWarehouseConsignmentState(WarehouseConsignmentState warehouseConsignmentState) {
        this.warehouseConsignmentState = warehouseConsignmentState;
    }

    public ConsignmentModel getConsignment() {
        return consignment;
    }

    public void setConsignment(ConsignmentModel consignment) {
        this.consignment = consignment;
    }

    public OrderProcessModel getParentProcess() {
        return parentProcess;
    }

    public void setParentProcess(OrderProcessModel parentProcess) {
        this.parentProcess = parentProcess;
    }
}
