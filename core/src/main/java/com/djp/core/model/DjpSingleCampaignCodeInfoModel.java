package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DjpSingleCampaignCodeInfo")
public class DjpSingleCampaignCodeInfoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private QrType whereIsValid;
    private String contractID;
    private Date startDate;
    private Date expireDate;
    private Integer claimed;
    private Integer used;

    @ManyToOne
    private QrForVoucherModel voucher;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QrType getWhereIsValid() {
        return whereIsValid;
    }

    public void setWhereIsValid(QrType whereIsValid) {
        this.whereIsValid = whereIsValid;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public QrForVoucherModel getVoucher() {
        return voucher;
    }

    public void setVoucher(QrForVoucherModel voucher) {
        this.voucher = voucher;
    }
}
