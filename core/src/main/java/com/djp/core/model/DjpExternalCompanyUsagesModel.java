package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;

@Entity
@Table(name="DjpExternalCompanyUsages")
public class DjpExternalCompanyUsagesModel  extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private QrType whereUsed;
    @OneToOne
    private DjpUniqueTokenModel tokenKey;
    private String billedQuantity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getBilledQuantity() {
        return billedQuantity;
    }

    public void setBilledQuantity(String billedQuantity) {
        this.billedQuantity = billedQuantity;
    }
}
