package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PointOfService")
public class MediaContainerModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String qualifier;

    private String name;

    @OneToMany(mappedBy = "container", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<MediaModel> medias;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MediaModel> getMedias() {
        return medias;
    }

    public void setMedias(Set<MediaModel> medias) {
        this.medias = medias;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }
}
