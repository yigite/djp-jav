package com.djp.core.model;

import com.djp.core.enums.DiscountChannel;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Discount")
public class DiscountModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private String name;
    private String discountString;

    private boolean absolute;
    private boolean global;
    private Integer priority;
    private Double value;

    @OneToOne
    private CurrencyModel currency;

    private DiscountChannel salesChannel;

    @ManyToMany(mappedBy = "discounts", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AbstractOrderModel> orders;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscountString() {
        return discountString;
    }

    public void setDiscountString(String discountString) {
        this.discountString = discountString;
    }

    public boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public DiscountChannel getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(DiscountChannel salesChannel) {
        this.salesChannel = salesChannel;
    }

    public Set<AbstractOrderModel> getOrders() {
        return orders;
    }

    public void setOrders(Set<AbstractOrderModel> orders) {
        this.orders = orders;
    }
}
