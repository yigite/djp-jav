package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="CMSCategoryRestriction")
@PrimaryKeyJoinColumn(name="id")
public class CMSCategoryRestrictionModel extends AbstractRestrictionModel{

    public CMSCategoryRestrictionModel()
    {
        super();
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Categories4Restriction",
            joinColumns = {@JoinColumn(name="restriction")},
            inverseJoinColumns = {@JoinColumn(name="category")})
    private Set<CategoryModel> categories;

    public Set<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryModel> categories) {
        this.categories = categories;
    }
}
