package com.djp.core.model;

import com.djp.modelservice.model.ItemModel;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpTelCountries")
public class DjpTelCountriesModel extends ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String land1;

    private String landx50;

    private String telefto;

    public String getLand1() {
        return land1;
    }

    public void setLand1(String land1) {
        this.land1 = land1;
    }

    public String getLandx50() {
        return landx50;
    }

    public void setLandx50(String landx50) {
        this.landx50 = landx50;
    }

    public String getTelefto() {
        return telefto;
    }

    public void setTelefto(String telefto) {
        this.telefto = telefto;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
