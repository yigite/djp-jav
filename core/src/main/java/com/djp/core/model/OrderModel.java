package com.djp.core.model;

import org.springframework.core.annotation.Order;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Order")
@PrimaryKeyJoinColumn(name = "id")
public class OrderModel extends AbstractOrderModel{

    @ManyToOne
    @JoinColumn(name = "user")
    private UserModel user;

    private String originalVersion;
    private String fraudulent;
    private String potentiallyFraudulent;
    private String statusDisplay;
    private String fraudReports;
    private String historyEntries;
    private String versionID;
    private String orderTemplateCronJob;
    private String orderScheduleCronJob;
    private String modificationRecords;
    private String returnRequests;
    private String salesApplication;

    @OneToOne
    private LanguageModel language;

    private String placedBy;
    private String quoteReference;
    private String giftInfoProcess;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OrderProcessModel> orderProcess;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<SAPOrderModel> sapOrders;

    public OrderModel() {
        super();
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getOriginalVersion() {
        return originalVersion;
    }

    public void setOriginalVersion(String originalVersion) {
        this.originalVersion = originalVersion;
    }

    public String getFraudulent() {
        return fraudulent;
    }

    public void setFraudulent(String fraudulent) {
        this.fraudulent = fraudulent;
    }

    public String getPotentiallyFraudulent() {
        return potentiallyFraudulent;
    }

    public void setPotentiallyFraudulent(String potentiallyFraudulent) {
        this.potentiallyFraudulent = potentiallyFraudulent;
    }

    public String getStatusDisplay() {
        return statusDisplay;
    }

    public void setStatusDisplay(String statusDisplay) {
        this.statusDisplay = statusDisplay;
    }

    public String getFraudReports() {
        return fraudReports;
    }

    public void setFraudReports(String fraudReports) {
        this.fraudReports = fraudReports;
    }

    public String getHistoryEntries() {
        return historyEntries;
    }

    public void setHistoryEntries(String historyEntries) {
        this.historyEntries = historyEntries;
    }

    public String getVersionID() {
        return versionID;
    }

    public void setVersionID(String versionID) {
        this.versionID = versionID;
    }

    public String getOrderTemplateCronJob() {
        return orderTemplateCronJob;
    }

    public void setOrderTemplateCronJob(String orderTemplateCronJob) {
        this.orderTemplateCronJob = orderTemplateCronJob;
    }

    public String getOrderScheduleCronJob() {
        return orderScheduleCronJob;
    }

    public void setOrderScheduleCronJob(String orderScheduleCronJob) {
        this.orderScheduleCronJob = orderScheduleCronJob;
    }

    public String getModificationRecords() {
        return modificationRecords;
    }

    public void setModificationRecords(String modificationRecords) {
        this.modificationRecords = modificationRecords;
    }

    public String getReturnRequests() {
        return returnRequests;
    }

    public void setReturnRequests(String returnRequests) {
        this.returnRequests = returnRequests;
    }

    public String getSalesApplication() {
        return salesApplication;
    }

    public void setSalesApplication(String salesApplication) {
        this.salesApplication = salesApplication;
    }

    public LanguageModel getLanguage() {
        return language;
    }

    public void setLanguage(LanguageModel language) {
        this.language = language;
    }

    public String getPlacedBy() {
        return placedBy;
    }

    public void setPlacedBy(String placedBy) {
        this.placedBy = placedBy;
    }

    public String getQuoteReference() {
        return quoteReference;
    }

    public void setQuoteReference(String quoteReference) {
        this.quoteReference = quoteReference;
    }

    public String getGiftInfoProcess() {
        return giftInfoProcess;
    }

    public void setGiftInfoProcess(String giftInfoProcess) {
        this.giftInfoProcess = giftInfoProcess;
    }

    public Set<OrderProcessModel> getOrderProcess() {
        return orderProcess;
    }

    public void setOrderProcess(Set<OrderProcessModel> orderProcess) {
        this.orderProcess = orderProcess;
    }

    public Set<SAPOrderModel> getSapOrders() {
        return sapOrders;
    }

    public void setSapOrders(Set<SAPOrderModel> sapOrders) {
        this.sapOrders = sapOrders;
    }

}
