package com.djp.core.model;

import com.djp.core.enums.PeriodOfValidEnum;
import com.djp.core.enums.PremiumPackagePassengerEnum;
import com.djp.core.enums.RemainingType;
import com.djp.core.enums.SasCampaignType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DjpRemainingUsage")
public class DjpRemainingUsageModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Integer claimed;
    private Integer used;
    private Date expireDate;
    private Date startDate;
    private String orderCode;
    private Date arrivalFlightDate;
    private String arrivalFlightCode;
    private Date departureFlightDate;
    private String departureFlightCode;
    private Date customerArrivalDate;
    private PremiumPackagePassengerEnum passengerType;
    private PeriodOfValidEnum periodOfValid;
    private Integer nDaysPeriodDayCount;
    private Boolean forChild;
    private Boolean forOverSixtyFive;
    private Boolean carparkingUsed;
    private Boolean isCancelled;
    private RemainingType remainingType;
    private SasCampaignType sasCampaignType;
    private String sasCampaignName;
//    private DjpDailyKidsUserModel dailyKidsUser;
//    private DjpMeetGreetInfoModel djpMeetGreetInfo;
//    private DjpAdditionalServicesRemainingModel djpAdditionalServicesRemaining;

    @OneToOne
    private DjpDailyKidsUserModel dailyKidsUser;

    @OneToOne
    private DjpExternalCustomerModel externalUser;

    @OneToOne
    private UserModel user;

    @OneToOne
    private ProductModel packagedProduct;

    @OneToOne
    private ProductModel product;

    @OneToOne
    private DjpUniqueTokenModel activationOrUsageCode;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public ProductModel getPackagedProduct() {
        return packagedProduct;
    }

    public void setPackagedProduct(ProductModel packagedProduct) {
        this.packagedProduct = packagedProduct;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Date getArrivalFlightDate() {
        return arrivalFlightDate;
    }

    public void setArrivalFlightDate(Date arrivalFlightDate) {
        this.arrivalFlightDate = arrivalFlightDate;
    }

    public String getArrivalFlightCode() {
        return arrivalFlightCode;
    }

    public void setArrivalFlightCode(String arrivalFlightCode) {
        this.arrivalFlightCode = arrivalFlightCode;
    }

    public Date getDepartureFlightDate() {
        return departureFlightDate;
    }

    public void setDepartureFlightDate(Date departureFlightDate) {
        this.departureFlightDate = departureFlightDate;
    }

    public String getDepartureFlightCode() {
        return departureFlightCode;
    }

    public void setDepartureFlightCode(String departureFlightCode) {
        this.departureFlightCode = departureFlightCode;
    }

    public Date getCustomerArrivalDate() {
        return customerArrivalDate;
    }

    public void setCustomerArrivalDate(Date customerArrivalDate) {
        this.customerArrivalDate = customerArrivalDate;
    }

    public DjpUniqueTokenModel getActivationOrUsageCode() {
        return activationOrUsageCode;
    }

    public void setActivationOrUsageCode(DjpUniqueTokenModel activationOrUsageCode) {
        this.activationOrUsageCode = activationOrUsageCode;
    }

    public PremiumPackagePassengerEnum getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PremiumPackagePassengerEnum passengerType) {
        this.passengerType = passengerType;
    }

    public PeriodOfValidEnum getPeriodOfValid() {
        return periodOfValid;
    }

    public void setPeriodOfValid(PeriodOfValidEnum periodOfValid) {
        this.periodOfValid = periodOfValid;
    }

    public Integer getnDaysPeriodDayCount() {
        return nDaysPeriodDayCount;
    }

    public void setnDaysPeriodDayCount(Integer nDaysPeriodDayCount) {
        this.nDaysPeriodDayCount = nDaysPeriodDayCount;
    }

    public Boolean getForChild() {
        return forChild;
    }

    public void setForChild(Boolean forChild) {
        this.forChild = forChild;
    }

    public Boolean getForOverSixtyFive() {
        return forOverSixtyFive;
    }

    public void setForOverSixtyFive(Boolean forOverSixtyFive) {
        this.forOverSixtyFive = forOverSixtyFive;
    }

    public Boolean getCarparkingUsed() {
        return carparkingUsed;
    }

    public void setCarparkingUsed(Boolean carparkingUsed) {
        this.carparkingUsed = carparkingUsed;
    }

    public Boolean getCancelled() {
        return isCancelled;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public RemainingType getRemainingType() {
        return remainingType;
    }

    public void setRemainingType(RemainingType remainingType) {
        this.remainingType = remainingType;
    }

    public SasCampaignType getSasCampaignType() {
        return sasCampaignType;
    }

    public void setSasCampaignType(SasCampaignType sasCampaignType) {
        this.sasCampaignType = sasCampaignType;
    }

    public String getSasCampaignName() {
        return sasCampaignName;
    }

    public void setSasCampaignName(String sasCampaignName) {
        this.sasCampaignName = sasCampaignName;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public DjpDailyKidsUserModel getDailyKidsUser() {
        return dailyKidsUser;
    }

    public void setDailyKidsUser(DjpDailyKidsUserModel dailyKidsUser) {
        this.dailyKidsUser = dailyKidsUser;
    }

    public DjpExternalCustomerModel getExternalUser() {
        return externalUser;
    }

    public void setExternalUser(DjpExternalCustomerModel externalUser) {
        this.externalUser = externalUser;
    }
}
