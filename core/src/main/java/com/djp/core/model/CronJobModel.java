package com.djp.core.model;

import com.djp.core.enums.*;

import jakarta.persistence.*;
import jakarta.persistence.criteria.Predicate;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "CronJobs")
@Inheritance(strategy = InheritanceType.JOINED)
public class CronJobModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private boolean active;
    private boolean retry;
    private boolean logToFile;
    private boolean singleExecutable;
    private boolean sendEmail;
    private boolean changeRecordingEnabled;
    private boolean requestAbort;
    private boolean requestAbortStep;
    private boolean removeOnExit;
    private int nodeID;
    private int runningOnClusterNode;
    private int priority;
    private int logsDaysOld;
    private int logsCount;
    private int filesDaysOld;
    private int filesCount;
    private int queryCount;
    private Date startTime;
    private Date endTime;
    private String nodeGroup;
    private String emailAddress;
    private String logText;
    private String timeTable;
    private String alternativeDataSourceID;
    private CronJobResult result;
    private CronJobStatus status;
    private BooleanOperator logsOperator;
    private BooleanOperator filesOperator;

    private ErrorMode errorMode;
    private JobLogLevel logLevelDatabase;
    private JobLogLevel logLevelFile;

    @ManyToOne
    @JoinColumn
    private JobModel job;

    @OneToOne
    private StepModel currentStep;

    @OneToOne
    private UserModel sessionUser;

    @OneToOne
    private LanguageModel sessionLanguage;

    @OneToOne
    private CurrencyModel sessionCurrency;

    @OneToOne
    private CronJobHistoryModel activeCronJobHistory;

    @OneToMany(mappedBy = "cronJob", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CronJobHistoryModel> cronJobHistoryEntries;

    @OneToMany(mappedBy = "cronJob", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<TriggerModel> triggers;

    @ManyToMany(mappedBy = "processedCronJobs", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<StepModel> processedSteps;

    @ManyToMany(mappedBy = "pendingCronJobs", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<StepModel> pendingSteps;

    @OneToMany(mappedBy = "cronJob", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<LogFileModel> logFiles;

    @OneToMany(mappedBy = "cronJob", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<JobLogModel> logs;

    /**
     * TODO: Mail entegrasyonundan sonra ui yapısına göre kurulabilir.
     * private RendererTemplateModel emailNotificationTemplate;
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isRetry() {
        return retry;
    }

    public void setRetry(boolean retry) {
        this.retry = retry;
    }

    public boolean isLogToFile() {
        return logToFile;
    }

    public void setLogToFile(boolean logToFile) {
        this.logToFile = logToFile;
    }

    public boolean isSingleExecutable() {
        return singleExecutable;
    }

    public void setSingleExecutable(boolean singleExecutable) {
        this.singleExecutable = singleExecutable;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public boolean isChangeRecordingEnabled() {
        return changeRecordingEnabled;
    }

    public void setChangeRecordingEnabled(boolean changeRecordingEnabled) {
        this.changeRecordingEnabled = changeRecordingEnabled;
    }

    public boolean isRequestAbort() {
        return requestAbort;
    }

    public void setRequestAbort(boolean requestAbort) {
        this.requestAbort = requestAbort;
    }

    public boolean isRequestAbortStep() {
        return requestAbortStep;
    }

    public void setRequestAbortStep(boolean requestAbortStep) {
        this.requestAbortStep = requestAbortStep;
    }

    public boolean isRemoveOnExit() {
        return removeOnExit;
    }

    public void setRemoveOnExit(boolean removeOnExit) {
        this.removeOnExit = removeOnExit;
    }

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public int getRunningOnClusterNode() {
        return runningOnClusterNode;
    }

    public void setRunningOnClusterNode(int runningOnClusterNode) {
        this.runningOnClusterNode = runningOnClusterNode;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getLogsDaysOld() {
        return logsDaysOld;
    }

    public void setLogsDaysOld(int logsDaysOld) {
        this.logsDaysOld = logsDaysOld;
    }

    public int getLogsCount() {
        return logsCount;
    }

    public void setLogsCount(int logsCount) {
        this.logsCount = logsCount;
    }

    public int getFilesDaysOld() {
        return filesDaysOld;
    }

    public void setFilesDaysOld(int filesDaysOld) {
        this.filesDaysOld = filesDaysOld;
    }

    public int getFilesCount() {
        return filesCount;
    }

    public void setFilesCount(int filesCount) {
        this.filesCount = filesCount;
    }

    public int getQueryCount() {
        return queryCount;
    }

    public void setQueryCount(int queryCount) {
        this.queryCount = queryCount;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getNodeGroup() {
        return nodeGroup;
    }

    public void setNodeGroup(String nodeGroup) {
        this.nodeGroup = nodeGroup;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    public String getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(String timeTable) {
        this.timeTable = timeTable;
    }

    public String getAlternativeDataSourceID() {
        return alternativeDataSourceID;
    }

    public void setAlternativeDataSourceID(String alternativeDataSourceID) {
        this.alternativeDataSourceID = alternativeDataSourceID;
    }

    public CronJobResult getResult() {
        return result;
    }

    public void setResult(CronJobResult result) {
        this.result = result;
    }

    public CronJobStatus getStatus() {
        return status;
    }

    public void setStatus(CronJobStatus status) {
        this.status = status;
    }

    public BooleanOperator getLogsOperator() {
        return logsOperator;
    }

    public void setLogsOperator(BooleanOperator logsOperator) {
        this.logsOperator = logsOperator;
    }

    public BooleanOperator getFilesOperator() {
        return filesOperator;
    }

    public void setFilesOperator(BooleanOperator filesOperator) {
        this.filesOperator = filesOperator;
    }

    public ErrorMode getErrorMode() {
        return errorMode;
    }

    public void setErrorMode(ErrorMode errorMode) {
        this.errorMode = errorMode;
    }

    public JobLogLevel getLogLevelDatabase() {
        return logLevelDatabase;
    }

    public void setLogLevelDatabase(JobLogLevel logLevelDatabase) {
        this.logLevelDatabase = logLevelDatabase;
    }

    public JobLogLevel getLogLevelFile() {
        return logLevelFile;
    }

    public void setLogLevelFile(JobLogLevel logLevelFile) {
        this.logLevelFile = logLevelFile;
    }

    public JobModel getJob() {
        return job;
    }

    public void setJob(JobModel job) {
        this.job = job;
    }

    public StepModel getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(StepModel currentStep) {
        this.currentStep = currentStep;
    }

    public UserModel getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(UserModel sessionUser) {
        this.sessionUser = sessionUser;
    }

    public LanguageModel getSessionLanguage() {
        return sessionLanguage;
    }

    public void setSessionLanguage(LanguageModel sessionLanguage) {
        this.sessionLanguage = sessionLanguage;
    }

    public CurrencyModel getSessionCurrency() {
        return sessionCurrency;
    }

    public void setSessionCurrency(CurrencyModel sessionCurrency) {
        this.sessionCurrency = sessionCurrency;
    }

    public CronJobHistoryModel getActiveCronJobHistory() {
        return activeCronJobHistory;
    }

    public void setActiveCronJobHistory(CronJobHistoryModel activeCronJobHistory) {
        this.activeCronJobHistory = activeCronJobHistory;
    }

    public Set<CronJobHistoryModel> getCronJobHistoryEntries() {
        return cronJobHistoryEntries;
    }

    public void setCronJobHistoryEntries(Set<CronJobHistoryModel> cronJobHistoryEntries) {
        this.cronJobHistoryEntries = cronJobHistoryEntries;
    }

    public Set<TriggerModel> getTriggers() {
        return triggers;
    }

    public void setTriggers(Set<TriggerModel> triggers) {
        this.triggers = triggers;
    }

    public Set<StepModel> getProcessedSteps() {
        return processedSteps;
    }

    public void setProcessedSteps(Set<StepModel> processedSteps) {
        this.processedSteps = processedSteps;
    }

    public Set<StepModel> getPendingSteps() {
        return pendingSteps;
    }

    public void setPendingSteps(Set<StepModel> pendingSteps) {
        this.pendingSteps = pendingSteps;
    }

    public Set<LogFileModel> getLogFiles() {
        return logFiles;
    }

    public void setLogFiles(Set<LogFileModel> logFiles) {
        this.logFiles = logFiles;
    }

    public Set<JobLogModel> getLogs() {
        return logs;
    }

    public void setLogs(Set<JobLogModel> logs) {
        this.logs = logs;
    }
}
