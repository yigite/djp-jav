package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.OneToOne;

public class DjpUsageSendToErpProcessModel extends BusinessProcessModel{

    private QrType whereUsed;

    @OneToOne
    private ItemModel qr;

    @OneToOne
    private ItemModel boarding;

    private String boardingType;
    private String sapOrderID;
    private Double paidPrice;

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public ItemModel getQr() {
        return qr;
    }

    public void setQr(ItemModel qr) {
        this.qr = qr;
    }

    public ItemModel getBoarding() {
        return boarding;
    }

    public void setBoarding(ItemModel boarding) {
        this.boarding = boarding;
    }

    public String getBoardingType() {
        return boardingType;
    }

    public void setBoardingType(String boardingType) {
        this.boardingType = boardingType;
    }

    public String getSapOrderID() {
        return sapOrderID;
    }

    public void setSapOrderID(String sapOrderID) {
        this.sapOrderID = sapOrderID;
    }

    public Double getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(Double paidPrice) {
        this.paidPrice = paidPrice;
    }
}
