package com.djp.core.model;

import jakarta.persistence.*;


@Entity
@Table(name = "AbstractMedia")
public abstract class AbstractMediaModel extends GenericItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String mime;

    private Long size;

    private Long dataPK;

    private String location;

    private String locationHash;

    private String realFileName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getDataPK() {
        return dataPK;
    }

    public void setDataPK(Long dataPK) {
        this.dataPK = dataPK;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationHash() {
        return locationHash;
    }

    public void setLocationHash(String locationHash) {
        this.locationHash = locationHash;
    }

    public String getRealFileName() {
        return realFileName;
    }

    public void setRealFileName(String realFileName) {
        this.realFileName = realFileName;
    }
}
