package com.djp.core.model;

import jakarta.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "NumberPlate")
public class NumberPlateModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;

    @ManyToOne
    @JoinColumn
    private CustomerModel customer;

    @ManyToMany(mappedBy = "numberPlates", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AbstractOrderModel> abstractOrder;

    //customerPOS

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public Set<AbstractOrderModel> getAbstractOrder() {
        return abstractOrder;
    }

    public void setAbstractOrder(Set<AbstractOrderModel> abstractOrder) {
        this.abstractOrder = abstractOrder;
    }
}
