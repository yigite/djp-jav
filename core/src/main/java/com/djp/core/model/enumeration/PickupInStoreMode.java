package com.djp.core.model.enumeration;

public enum PickupInStoreMode {
    DISABLED("DISABLED"),
    BUY_AND_COLLECT("BUY_AND_COLLECT"),
    RESERVE_AND_COLLECT("RESERVE_AND_COLLECT");

    private final String code;

    PickupInStoreMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
