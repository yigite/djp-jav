package com.djp.core.model;

import com.djp.core.enums.ErrorMode;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Steps")
public class StepModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private int sequenceNumber;
    private boolean synchronous;

    private ErrorMode errorMode;

    @ManyToOne
    @JoinColumn
    private BatchJobModel batchJob;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="CronJobProcessedStepsRelation",
            joinColumns={@JoinColumn(name="processedStep")},
            inverseJoinColumns={@JoinColumn(name="processedCronJob")})
    private Set<CronJobModel> processedCronJobs;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="CronJobPendingStepsRelation",
            joinColumns={@JoinColumn(name="pendingStep")},
            inverseJoinColumns={@JoinColumn(name="pendingCronJob")})
    private Set<CronJobModel> pendingCronJobs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public boolean isSynchronous() {
        return synchronous;
    }

    public void setSynchronous(boolean synchronous) {
        this.synchronous = synchronous;
    }

    public ErrorMode getErrorMode() {
        return errorMode;
    }

    public void setErrorMode(ErrorMode errorMode) {
        this.errorMode = errorMode;
    }

    public BatchJobModel getBatchJob() {
        return batchJob;
    }

    public void setBatchJob(BatchJobModel batchJob) {
        this.batchJob = batchJob;
    }

    public Set<CronJobModel> getProcessedCronJobs() {
        return processedCronJobs;
    }

    public void setProcessedCronJobs(Set<CronJobModel> processedCronJobs) {
        this.processedCronJobs = processedCronJobs;
    }

    public Set<CronJobModel> getPendingCronJobs() {
        return pendingCronJobs;
    }

    public void setPendingCronJobs(Set<CronJobModel> pendingCronJobs) {
        this.pendingCronJobs = pendingCronJobs;
    }
}
