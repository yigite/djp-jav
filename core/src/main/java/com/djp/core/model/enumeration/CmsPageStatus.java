package com.djp.core.model.enumeration;

public enum CmsPageStatus {

    ACTIVE("ACTIVE"),
    DELETED("DELETED");


    private final String code;

    CmsPageStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
