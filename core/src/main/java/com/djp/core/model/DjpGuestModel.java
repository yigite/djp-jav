package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DjpGuest")
public class DjpGuestModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String surname;
    private String passport;
    private String tckn;
    private String phoneNumber;
    private String email;
    private String birthDate;
    private String nationality;
    private String gender;
    private String pnrCode;
    private Boolean kvkkCheck;
    private Boolean wifiCheck;
    private String phonePrefix;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "DjpBP2G",
            joinColumns = {@JoinColumn(name = "guests")},
            inverseJoinColumns = {@JoinColumn(name = "boardingPasses")})
    private Set<DjpBoardingPassModel> boardingPasses;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "DjpAccBP2G",
            joinColumns = {@JoinColumn(name = "guests")},
            inverseJoinColumns = {@JoinColumn(name = "accboardingPasses")})
    private Set<DjpAccessBoardingModel> accboardingPasses;

    @ManyToOne
    private DjpRemainingUsageModel remaining;

    @ManyToOne
    private DjpExternalOrderEntriesModel externalEntries;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public Boolean getKvkkCheck() {
        return kvkkCheck;
    }

    public void setKvkkCheck(Boolean kvkkCheck) {
        this.kvkkCheck = kvkkCheck;
    }

    public Boolean getWifiCheck() {
        return wifiCheck;
    }

    public void setWifiCheck(Boolean wifiCheck) {
        this.wifiCheck = wifiCheck;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Set<DjpBoardingPassModel> getBoardingPasses() {
        return boardingPasses;
    }

    public void setBoardingPasses(Set<DjpBoardingPassModel> boardingPasses) {
        this.boardingPasses = boardingPasses;
    }

    public Set<DjpAccessBoardingModel> getAccboardingPasses() {
        return accboardingPasses;
    }

    public void setAccboardingPasses(Set<DjpAccessBoardingModel> accboardingPasses) {
        this.accboardingPasses = accboardingPasses;
    }

    public DjpRemainingUsageModel getRemaining() {
        return remaining;
    }

    public void setRemaining(DjpRemainingUsageModel remaining) {
        this.remaining = remaining;
    }

    public DjpExternalOrderEntriesModel getExternalEntries() {
        return externalEntries;
    }

    public void setExternalEntries(DjpExternalOrderEntriesModel externalEntries) {
        this.externalEntries = externalEntries;
    }
}
