package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "SAPPricingCondition")
public class SAPPricingConditionModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String order;
    private String conditionType;
    private String stepNumber;
    private String conditionCounter;
    private String currencyKey;
    private String conditionPricingUnit;
    private String conditionUnit;
    private String conditionCalculationType;
    private String conditionRate;
    private String conditionValue;

    @ManyToOne
    @JoinColumn
    private AbstractOrderEntryModel orderEntry;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getConditionType() {
        return conditionType;
    }

    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(String stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getConditionCounter() {
        return conditionCounter;
    }

    public void setConditionCounter(String conditionCounter) {
        this.conditionCounter = conditionCounter;
    }

    public String getCurrencyKey() {
        return currencyKey;
    }

    public void setCurrencyKey(String currencyKey) {
        this.currencyKey = currencyKey;
    }

    public String getConditionPricingUnit() {
        return conditionPricingUnit;
    }

    public void setConditionPricingUnit(String conditionPricingUnit) {
        this.conditionPricingUnit = conditionPricingUnit;
    }

    public String getConditionUnit() {
        return conditionUnit;
    }

    public void setConditionUnit(String conditionUnit) {
        this.conditionUnit = conditionUnit;
    }

    public String getConditionCalculationType() {
        return conditionCalculationType;
    }

    public void setConditionCalculationType(String conditionCalculationType) {
        this.conditionCalculationType = conditionCalculationType;
    }

    public String getConditionRate() {
        return conditionRate;
    }

    public void setConditionRate(String conditionRate) {
        this.conditionRate = conditionRate;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public AbstractOrderEntryModel getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(AbstractOrderEntryModel orderEntry) {
        this.orderEntry = orderEntry;
    }
}
