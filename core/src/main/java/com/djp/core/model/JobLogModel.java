package com.djp.core.model;

import com.djp.core.enums.JobLogLevel;

import jakarta.persistence.*;

@Entity
@Table(name = "JobLogs")
public class JobLogModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private JobLogLevel level;
    private String message;
    private String shortMessage;

    @OneToOne
    private StepModel step;

    @ManyToOne
    @JoinColumn
    private CronJobModel cronJob;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public JobLogLevel getLevel() {
        return level;
    }

    public void setLevel(JobLogLevel level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public StepModel getStep() {
        return step;
    }

    public void setStep(StepModel step) {
        this.step = step;
    }

    public CronJobModel getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJobModel cronJob) {
        this.cronJob = cronJob;
    }
}
