package com.djp.core.model;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.enums.UsageMethod;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "DjpAccessBoarding")
public class DjpAccessBoardingModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String pnrCode;
    private String formatCode;
    private String numberOfLegsEncoded;
    private String passengerName;
    private String electronicTicketIndicator;
    private String operatingCarrierPnrCode;
    private String fromCityAirportCode;
    private String toCityAirportCode;
    private String operatingCarrierDesignator;
    private String flightNumber;
    private Date dateOfFlight;
    private String compartmentCode;
    private String seatNumber;
    private String checkInSequenceNumber;
    private String passengerStatus;
    private String boardingUsageType;

    private boolean isBoardingValid;
    private boolean rightOfPassage;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "DjpAccessBoarding_QrType_boardingUsages", joinColumns = @JoinColumn(name = "DjpAccessBoarding_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> boardingUsages;

    private String boardingClass;
    private String wifiPassword;
    private Boolean couldNotReadBoardingPass;
    private String contractID;
    private Boolean newSale;
    private String sapOrderID;
    private String refNumber;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "DjpAccessBoarding_RefNumberList", joinColumns = @JoinColumn(name = "DjpAccessBoarding_id"))
    @Column(name = "refNumber")
    private List<String> refNumberList;

    private String personalID;
    private String personalName;
    private Boolean isCancelled;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    @OneToOne
    private DjpMiddleWareBoardingPassStatusModel middleWareDetail;

    private UsageMethod usageMethod;
    private Double quantityToBeInvoiced;

    private PassengerType passengerType;
    private String cancellationUserID;
    private String cancellationReason;

    @OneToOne
    private DjpBoardingPassUsingInfoModel boardingPassUsingData;
    private Date serviceOutDate;

    @ManyToMany(mappedBy = "accboardingPasses")
    private Set<DjpGuestModel> guests;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "AccBrd2BrdRel",
            joinColumns = {@JoinColumn(name = "djpAccBoardingPass")},
            inverseJoinColumns = {@JoinColumn(name = "superAccBoarding")})
    private List<DjpAccessBoardingModel> superAccBoarding ;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "AccBoard2DjpBoardRelation",
            joinColumns = {@JoinColumn(name = "djpAccBoardingPass")},
            inverseJoinColumns = {@JoinColumn(name = "2djpAccBoardingPass")})
    private List<DjpAccessBoardingModel> djpAccBoardingPass ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public String getFormatCode() {
        return formatCode;
    }

    public void setFormatCode(String formatCode) {
        this.formatCode = formatCode;
    }

    public String getNumberOfLegsEncoded() {
        return numberOfLegsEncoded;
    }

    public void setNumberOfLegsEncoded(String numberOfLegsEncoded) {
        this.numberOfLegsEncoded = numberOfLegsEncoded;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getElectronicTicketIndicator() {
        return electronicTicketIndicator;
    }

    public void setElectronicTicketIndicator(String electronicTicketIndicator) {
        this.electronicTicketIndicator = electronicTicketIndicator;
    }

    public String getOperatingCarrierPnrCode() {
        return operatingCarrierPnrCode;
    }

    public void setOperatingCarrierPnrCode(String operatingCarrierPnrCode) {
        this.operatingCarrierPnrCode = operatingCarrierPnrCode;
    }

    public String getFromCityAirportCode() {
        return fromCityAirportCode;
    }

    public void setFromCityAirportCode(String fromCityAirportCode) {
        this.fromCityAirportCode = fromCityAirportCode;
    }

    public String getToCityAirportCode() {
        return toCityAirportCode;
    }

    public void setToCityAirportCode(String toCityAirportCode) {
        this.toCityAirportCode = toCityAirportCode;
    }

    public String getOperatingCarrierDesignator() {
        return operatingCarrierDesignator;
    }

    public void setOperatingCarrierDesignator(String operatingCarrierDesignator) {
        this.operatingCarrierDesignator = operatingCarrierDesignator;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDateOfFlight() {
        return dateOfFlight;
    }

    public void setDateOfFlight(Date dateOfFlight) {
        this.dateOfFlight = dateOfFlight;
    }

    public String getCompartmentCode() {
        return compartmentCode;
    }

    public void setCompartmentCode(String compartmentCode) {
        this.compartmentCode = compartmentCode;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getCheckInSequenceNumber() {
        return checkInSequenceNumber;
    }

    public void setCheckInSequenceNumber(String checkInSequenceNumber) {
        this.checkInSequenceNumber = checkInSequenceNumber;
    }

    public String getPassengerStatus() {
        return passengerStatus;
    }

    public void setPassengerStatus(String passengerStatus) {
        this.passengerStatus = passengerStatus;
    }

    public String getBoardingUsageType() {
        return boardingUsageType;
    }

    public void setBoardingUsageType(String boardingUsageType) {
        this.boardingUsageType = boardingUsageType;
    }

    public boolean isBoardingValid() {
        return isBoardingValid;
    }

    public void setBoardingValid(boolean boardingValid) {
        isBoardingValid = boardingValid;
    }

    public boolean isRightOfPassage() {
        return rightOfPassage;
    }

    public void setRightOfPassage(boolean rightOfPassage) {
        this.rightOfPassage = rightOfPassage;
    }

    public Set<QrType> getBoardingUsages() {
        return boardingUsages;
    }

    public void setBoardingUsages(Set<QrType> boardingUsages) {
        this.boardingUsages = boardingUsages;
    }

    public String getBoardingClass() {
        return boardingClass;
    }

    public void setBoardingClass(String boardingClass) {
        this.boardingClass = boardingClass;
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public Boolean getCouldNotReadBoardingPass() {
        return couldNotReadBoardingPass;
    }

    public void setCouldNotReadBoardingPass(Boolean couldNotReadBoardingPass) {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public Boolean getNewSale() {
        return newSale;
    }

    public void setNewSale(Boolean newSale) {
        this.newSale = newSale;
    }

    public String getSapOrderID() {
        return sapOrderID;
    }

    public void setSapOrderID(String sapOrderID) {
        this.sapOrderID = sapOrderID;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public List<String> getRefNumberList() {
        return refNumberList;
    }

    public void setRefNumberList(List<String> refNumberList) {
        this.refNumberList = refNumberList;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public Boolean getCancelled() {
        return isCancelled;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public DjpMiddleWareBoardingPassStatusModel getMiddleWareDetail() {
        return middleWareDetail;
    }

    public void setMiddleWareDetail(DjpMiddleWareBoardingPassStatusModel middleWareDetail) {
        this.middleWareDetail = middleWareDetail;
    }

    public UsageMethod getUsageMethod() {
        return usageMethod;
    }

    public void setUsageMethod(UsageMethod usageMethod) {
        this.usageMethod = usageMethod;
    }

    public Double getQuantityToBeInvoiced() {
        return quantityToBeInvoiced;
    }

    public void setQuantityToBeInvoiced(Double quantityToBeInvoiced) {
        this.quantityToBeInvoiced = quantityToBeInvoiced;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public String getCancellationUserID() {
        return cancellationUserID;
    }

    public void setCancellationUserID(String cancellationUserID) {
        this.cancellationUserID = cancellationUserID;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public DjpBoardingPassUsingInfoModel getBoardingPassUsingData() {
        return boardingPassUsingData;
    }

    public void setBoardingPassUsingData(DjpBoardingPassUsingInfoModel boardingPassUsingData) {
        this.boardingPassUsingData = boardingPassUsingData;
    }

    public Date getServiceOutDate() {
        return serviceOutDate;
    }

    public void setServiceOutDate(Date serviceOutDate) {
        this.serviceOutDate = serviceOutDate;
    }

    public Set<DjpGuestModel> getGuests() {
        return guests;
    }

    public void setGuests(Set<DjpGuestModel> guests) {
        this.guests = guests;
    }

    public List<DjpAccessBoardingModel> getSuperAccBoarding() {
        return superAccBoarding;
    }

    public void setSuperAccBoarding(List<DjpAccessBoardingModel> superAccBoarding) {
        this.superAccBoarding = superAccBoarding;
    }

    public List<DjpAccessBoardingModel> getDjpAccBoardingPass() {
        return djpAccBoardingPass;
    }

    public void setDjpAccBoardingPass(List<DjpAccessBoardingModel> djpAccBoardingPass) {
        this.djpAccBoardingPass = djpAccBoardingPass;
    }
}
