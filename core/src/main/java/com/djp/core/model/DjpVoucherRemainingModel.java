package com.djp.core.model;

import com.djp.core.enums.PeriodOfValidEnum;
import com.djp.core.enums.PremiumPackagePassengerEnum;
import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DjpVoucherRemaining")
public class DjpVoucherRemainingModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private QrType whereIsValid;
    private Integer claimed;
    private Integer used;
    private Integer guestCount;
    private Date expireDate;
    private Date startDate;

    @OneToOne
    private ProductModel packagedProduct;

    private PremiumPackagePassengerEnum passengerType;
    private PeriodOfValidEnum periodOfValid;
    private Integer nDaysPeriodDayCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QrType getWhereIsValid() {
        return whereIsValid;
    }

    public void setWhereIsValid(QrType whereIsValid) {
        this.whereIsValid = whereIsValid;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Integer getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(Integer guestCount) {
        this.guestCount = guestCount;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public ProductModel getPackagedProduct() {
        return packagedProduct;
    }

    public void setPackagedProduct(ProductModel packagedProduct) {
        this.packagedProduct = packagedProduct;
    }

    public PremiumPackagePassengerEnum getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PremiumPackagePassengerEnum passengerType) {
        this.passengerType = passengerType;
    }

    public PeriodOfValidEnum getPeriodOfValid() {
        return periodOfValid;
    }

    public void setPeriodOfValid(PeriodOfValidEnum periodOfValid) {
        this.periodOfValid = periodOfValid;
    }

    public Integer getnDaysPeriodDayCount() {
        return nDaysPeriodDayCount;
    }

    public void setnDaysPeriodDayCount(Integer nDaysPeriodDayCount) {
        this.nDaysPeriodDayCount = nDaysPeriodDayCount;
    }
}

