package com.djp.core.model.enumeration;

public enum ArticleApprovalStatus {
    Check("Check"),
    Approved("Approved"),
    Unapproved("Unapproved");

    private String code;

    ArticleApprovalStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
