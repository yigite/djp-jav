package com.djp.core.model;

import com.djp.core.enums.DjpEnumValue;
import com.djp.core.enums.PriceRowChannel;
import com.djp.core.enums.ProductPriceGroup;
import jakarta.persistence.*;

@Entity
@Table(name="PriceRow")
@PrimaryKeyJoinColumn(name = "id")
public class PriceRowModel extends PDTRowModel{

    private String sapConditionId;
    private PriceRowChannel channel;
    private int matchValue;
    private int unitFactor;
    private boolean giveAwayPrice;
    private boolean net;
    private long minqtd;
    private long sequenceId;
    private double price;

    @OneToOne
    private CurrencyModel currency;

    @OneToOne
    private UnitModel unit;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    @ManyToOne
    @JoinColumn
    private ProductModel product;

    public PriceRowModel() {super();}

    public String getSapConditionId() {
        return sapConditionId;
    }

    public void setSapConditionId(String sapConditionId) {
        this.sapConditionId = sapConditionId;
    }

    public PriceRowChannel getChannel() {
        return channel;
    }

    public void setChannel(PriceRowChannel channel) {
        this.channel = channel;
    }

    public int getMatchValue() {
        return matchValue;
    }

    public void setMatchValue(int matchValue) {
        this.matchValue = matchValue;
    }

    public int getUnitFactor() {
        return unitFactor;
    }

    public void setUnitFactor(int unitFactor) {
        this.unitFactor = unitFactor;
    }

    public boolean isGiveAwayPrice() {
        return giveAwayPrice;
    }

    public void setGiveAwayPrice(boolean giveAwayPrice) {
        this.giveAwayPrice = giveAwayPrice;
    }

    public boolean isNet() {
        return net;
    }

    public void setNet(boolean net) {
        this.net = net;
    }

    public long getMinqtd() {
        return minqtd;
    }

    public void setMinqtd(long minqtd) {
        this.minqtd = minqtd;
    }

    public long getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(long sequenceId) {
        this.sequenceId = sequenceId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public UnitModel getUnit() {
        return unit;
    }

    public void setUnit(UnitModel unit) {
        this.unit = unit;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    @Override
    public ProductModel getProduct() {
        return product;
    }

    @Override
    public void setProduct(ProductModel product) {
        this.product = product;
    }

    @Override
    public void setPg(final DjpEnumValue value)
    {
        if( value == null || value instanceof ProductPriceGroup)
        {
            super.setPg(value);
        }
        else
        {
            throw new IllegalArgumentException("Given value is not instance of ProductPriceGroup");
        }
    }
}
