package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PrincipalGroup")
@Inheritance(strategy = InheritanceType.JOINED)
public class PrincipalGroupModel extends PrincipalModel
{

    private String locName;
    private String maxBruteForceLoginAttempts;

    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<PrincipalModel> members;

    public PrincipalGroupModel() {
        super();
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getMaxBruteForceLoginAttempts() {
        return maxBruteForceLoginAttempts;
    }

    public void setMaxBruteForceLoginAttempts(String maxBruteForceLoginAttempts) {
        this.maxBruteForceLoginAttempts = maxBruteForceLoginAttempts;
    }

    public Set<PrincipalModel> getMembers() {
        return members;
    }

    public void setMembers(Set<PrincipalModel> members) {
        this.members = members;
    }
}
