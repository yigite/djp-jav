package com.djp.core.model;

import com.djp.core.enums.BarcodeType;
import jakarta.persistence.*;

@Entity
@Table(name="PriceRow")
@PrimaryKeyJoinColumn(name = "id")
public class BarcodeMediaModel extends MediaModel{

    private String barcodeText;
    private BarcodeType barcodeType;

    @ManyToOne
    @JoinColumn
    private DeeplinkUrlModel deeplinkUrl;

    public BarcodeMediaModel() {
        super();
    }

    public String getBarcodeText() {
        return barcodeText;
    }

    public void setBarcodeText(String barcodeText) {
        this.barcodeText = barcodeText;
    }

    public BarcodeType getBarcodeType() {
        return barcodeType;
    }

    public void setBarcodeType(BarcodeType barcodeType) {
        this.barcodeType = barcodeType;
    }

    public DeeplinkUrlModel getDeeplinkUrl() {
        return deeplinkUrl;
    }

    public void setDeeplinkUrl(DeeplinkUrlModel deeplinkUrl) {
        this.deeplinkUrl = deeplinkUrl;
    }
}
