package com.djp.core.model;

import com.djp.core.model.enumeration.CppTokenStatus;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "DjpCppTxn")
public class DjpCppTxnModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cppToken;

    private BigDecimal amount;

    private String name;

    private String reservationNumber;

    private String surName;

    private String location;

    private CppTokenStatus tokenStatus;

    private String sapErrorMessage;

    private String language;

    private String youthLoungeCampaignCode;

    private String youthLoungeEmail;

    private String country;

    @OneToOne
    private AbstractOrderModel order;

    private BigDecimal parity;

    private BigDecimal amountToPay;

    private Date expireDate;

    private Date reservationDate;

    @OneToOne
    private AddressModel paymentAddress;

    private Boolean hasShipCrew;
    private Boolean hasBusinessCornerDevice;
    private Boolean hasBusinessCornerExtraDevice;
    private Boolean hasBusinessCornerPrinter;

    @OneToOne
    private CurrencyModel currency;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCppToken() {
        return cppToken;
    }

    public void setCppToken(String cppToken) {
        this.cppToken = cppToken;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public CppTokenStatus getTokenStatus() {
        return tokenStatus;
    }

    public void setTokenStatus(CppTokenStatus tokenStatus) {
        this.tokenStatus = tokenStatus;
    }

    public String getSapErrorMessage() {
        return sapErrorMessage;
    }

    public void setSapErrorMessage(String sapErrorMessage) {
        this.sapErrorMessage = sapErrorMessage;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getYouthLoungeCampaignCode() {
        return youthLoungeCampaignCode;
    }

    public void setYouthLoungeCampaignCode(String youthLoungeCampaignCode) {
        this.youthLoungeCampaignCode = youthLoungeCampaignCode;
    }

    public String getYouthLoungeEmail() {
        return youthLoungeEmail;
    }

    public void setYouthLoungeEmail(String youthLoungeEmail) {
        this.youthLoungeEmail = youthLoungeEmail;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public BigDecimal getParity() {
        return parity;
    }

    public void setParity(BigDecimal parity) {
        this.parity = parity;
    }

    public BigDecimal getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(BigDecimal amountToPay) {
        this.amountToPay = amountToPay;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public AddressModel getPaymentAddress() {
        return paymentAddress;
    }

    public void setPaymentAddress(AddressModel paymentAddress) {
        this.paymentAddress = paymentAddress;
    }

    public Boolean getHasShipCrew() {
        return hasShipCrew;
    }

    public void setHasShipCrew(Boolean hasShipCrew) {
        this.hasShipCrew = hasShipCrew;
    }

    public Boolean getHasBusinessCornerDevice() {
        return hasBusinessCornerDevice;
    }

    public void setHasBusinessCornerDevice(Boolean hasBusinessCornerDevice) {
        this.hasBusinessCornerDevice = hasBusinessCornerDevice;
    }

    public Boolean getHasBusinessCornerExtraDevice() {
        return hasBusinessCornerExtraDevice;
    }

    public void setHasBusinessCornerExtraDevice(Boolean hasBusinessCornerExtraDevice) {
        this.hasBusinessCornerExtraDevice = hasBusinessCornerExtraDevice;
    }

    public Boolean getHasBusinessCornerPrinter() {
        return hasBusinessCornerPrinter;
    }

    public void setHasBusinessCornerPrinter(Boolean hasBusinessCornerPrinter) {
        this.hasBusinessCornerPrinter = hasBusinessCornerPrinter;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }
}
