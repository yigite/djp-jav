package com.djp.core.model;

import com.djp.core.enums.ConsignmentStatus;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "Consignment")
public class ConsignmentModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String code;

    @OneToOne
    private AddressModel shippingAddress;

    @OneToOne
    private DeliveryModeModel deliveryMode;
    private Date namedDeliveryDate;
    private Date shippingDate;
    private String trackingID;
    private String carrier;
    private ConsignmentStatus status;

    @OneToMany(mappedBy = "consignment", cascade = {CascadeType.ALL})
    private Set<ConsignmentEntryModel> consignmentEntries;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    @OneToMany(mappedBy = "consignment", cascade = {CascadeType.ALL})
    private Set<ConsignmentProcessModel> consignmentProcesses;

    @OneToOne
    private PointOfServiceModel deliveryPointOfService;

    @ManyToOne
    @JoinColumn
    private SAPOrderModel sapOrder;

    @OneToOne
    private CarrierModel carrierDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private WarehouseModel warehouse;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AddressModel getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AddressModel shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public DeliveryModeModel getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(DeliveryModeModel deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Date getNamedDeliveryDate() {
        return namedDeliveryDate;
    }

    public void setNamedDeliveryDate(Date namedDeliveryDate) {
        this.namedDeliveryDate = namedDeliveryDate;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getTrackingID() {
        return trackingID;
    }

    public void setTrackingID(String trackingID) {
        this.trackingID = trackingID;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public ConsignmentStatus getStatus() {
        return status;
    }

    public void setStatus(ConsignmentStatus status) {
        this.status = status;
    }

    public Set<ConsignmentEntryModel> getConsignmentEntries() {
        return consignmentEntries;
    }

    public void setConsignmentEntries(Set<ConsignmentEntryModel> consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public Set<ConsignmentProcessModel> getConsignmentProcesses() {
        return consignmentProcesses;
    }

    public void setConsignmentProcesses(Set<ConsignmentProcessModel> consignmentProcesses) {
        this.consignmentProcesses = consignmentProcesses;
    }

    public PointOfServiceModel getDeliveryPointOfService() {
        return deliveryPointOfService;
    }

    public void setDeliveryPointOfService(PointOfServiceModel deliveryPointOfService) {
        this.deliveryPointOfService = deliveryPointOfService;
    }

    public SAPOrderModel getSapOrder() {
        return sapOrder;
    }

    public void setSapOrder(SAPOrderModel sapOrder) {
        this.sapOrder = sapOrder;
    }


    public CarrierModel getCarrierDetails() {
        return carrierDetails;
    }

    public void setCarrierDetails(CarrierModel carrierDetails) {
        this.carrierDetails = carrierDetails;
    }

    public WarehouseModel getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseModel warehouse) {
        this.warehouse = warehouse;
    }


}
