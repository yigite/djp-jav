package com.djp.core.model;


import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Country")
public class CountryModel extends C2LItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<RegionModel> regions;

    @ManyToMany( fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(
            name = "Country2RegionRel",
            joinColumns = {@JoinColumn(name = "country")},
            inverseJoinColumns = {@JoinColumn(name = "region")})
    private Set<RegionModel> zones;

    //private Set<BaseStoreModel> baseStores;
    //private Set<BaseStoreModel> billingBaseStores;

    private String sapCode;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Set<RegionModel> getRegions() {
        return regions;
    }

    public void setRegions(Set<RegionModel> regions) {
        this.regions = regions;
    }

    public Set<RegionModel> getZones() {
        return zones;
    }

    public void setZones(Set<RegionModel> zones) {
        this.zones = zones;
    }

    public String getSapCode() {
        return sapCode;
    }

    public void setSapCode(String sapCode) {
        this.sapCode = sapCode;
    }
}
