package com.djp.core.model;

import com.djp.core.enums.DjpPaymentTransactionStatus;
import com.djp.core.enums.DjpPaymentTransactionStatusDetails;
import com.djp.core.enums.PaymentTransactionType;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "PaymentTransactionEntry")
public class PaymentTransactionEntryModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private BigDecimal amount;

    private String channel;
    private String requestId;
    private String requestToken;
    private String retRefNum;
    private String versionID;
    private String subscriptionID;
    private String transactionStatus;
    private String transactionStatusDetails;
    private Date time;
    private DjpPaymentTransactionStatus txnStatus;
    private DjpPaymentTransactionStatusDetails txnStxnStatusDetailstatus;
    private PaymentTransactionType type;

    @OneToOne
    private CurrencyModel currency;

    @ManyToOne
    @JoinColumn
    private PaymentTransactionModel paymentTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getRetRefNum() {
        return retRefNum;
    }

    public void setRetRefNum(String retRefNum) {
        this.retRefNum = retRefNum;
    }

    public String getVersionID() {
        return versionID;
    }

    public void setVersionID(String versionID) {
        this.versionID = versionID;
    }

    public String getSubscriptionID() {
        return subscriptionID;
    }

    public void setSubscriptionID(String subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatusDetails() {
        return transactionStatusDetails;
    }

    public void setTransactionStatusDetails(String transactionStatusDetails) {
        this.transactionStatusDetails = transactionStatusDetails;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public DjpPaymentTransactionStatus getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(DjpPaymentTransactionStatus txnStatus) {
        this.txnStatus = txnStatus;
    }

    public DjpPaymentTransactionStatusDetails getTxnStxnStatusDetailstatus() {
        return txnStxnStatusDetailstatus;
    }

    public void setTxnStxnStatusDetailstatus(DjpPaymentTransactionStatusDetails txnStxnStatusDetailstatus) {
        this.txnStxnStatusDetailstatus = txnStxnStatusDetailstatus;
    }

    public PaymentTransactionType getType() {
        return type;
    }

    public void setType(PaymentTransactionType type) {
        this.type = type;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public PaymentTransactionModel getPaymentTransaction() {
        return paymentTransaction;
    }

    public void setPaymentTransaction(PaymentTransactionModel paymentTransaction) {
        this.paymentTransaction = paymentTransaction;
    }
}
