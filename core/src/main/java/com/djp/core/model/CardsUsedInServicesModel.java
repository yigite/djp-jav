package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CardsUsedInServices")
public class CardsUsedInServicesModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cardId;
    private String companyId;

    private QrType whereUsed;
    private Date usageDate;
    private String plateNumber;
    private String carParkingUsedInfo;
    private Boolean usedVale;

    @OneToOne
    private DjpUniqueTokenModel token;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private Integer usedValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCarParkingUsedInfo() {
        return carParkingUsedInfo;
    }

    public void setCarParkingUsedInfo(String carParkingUsedInfo) {
        this.carParkingUsedInfo = carParkingUsedInfo;
    }

    public Boolean getUsedVale() {
        return usedVale;
    }

    public void setUsedVale(Boolean usedVale) {
        this.usedVale = usedVale;
    }

    public DjpUniqueTokenModel getToken() {
        return token;
    }

    public void setToken(DjpUniqueTokenModel token) {
        this.token = token;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public Integer getUsedValue() {
        return usedValue;
    }

    public void setUsedValue(Integer usedValue) {
        this.usedValue = usedValue;
    }
}
