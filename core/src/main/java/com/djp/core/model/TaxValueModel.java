package com.djp.core.model;

import com.djp.modelservice.util.DjpMathUtil;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name = "TaxValue")
public class TaxValueModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private final String code;

    private double value;
    private double appliedValue;
    private boolean absolute;
    private String isoCode;

    @Transient
    private static final String TV_HEADER = "<TV<";
    @Transient
    private static final String TV_FOOTER = ">VT>";
    @Transient
    private static final String INTERNAL_DELIMITER = "#";
    @Transient
    private static final String DELIMITER = "|";
    @Transient
    private static final String EMPTY = "[]";

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }

    public String getCode() {
        return this.code;
    }

    public String getCurrencyIsoCode() {
        return this.isoCode;
    }

    public double getValue() {
        return this.value;
    }

    public double getAppliedValue() {
        return this.appliedValue;
    }

    public boolean isAbsolute() {
        return this.absolute;
    }

    public TaxValueModel(String code, double value, boolean absolute, String currencyIsoCode) {
        this(code, value, absolute, 0.0D, currencyIsoCode);
    }

    public TaxValueModel(String code, double value, boolean absolute, double appliedValue, String currencyIsoCode) {
        if (code == null) {
            throw new IllegalArgumentException("tax value code may not be null");
        } else {
            this.code = code;
            this.value = value;
            this.absolute = absolute;
            this.appliedValue = appliedValue;
            this.isoCode = currencyIsoCode;
        }
    }

    public Object clone() {
        return new TaxValueModel(this.getCode(), this.getValue(), this.isAbsolute(), this.getAppliedValue(), this.getCurrencyIsoCode());
    }

    public TaxValueModel apply(double quantity, double price, int digits, boolean priceIsNet, String currencyIsoCode) {
        if (!this.isAbsolute() || currencyIsoCode == this.getCurrencyIsoCode() || currencyIsoCode != null && currencyIsoCode.equals(this.getCurrencyIsoCode())) {
            //return new TaxValueModel(this.getCode(), this.getValue(), this.isAbsolute(), CoreAlgorithms.round(this.isAbsolute() ? this.getValue() * quantity : (priceIsNet ? price * this.getValue() / 100.0D : price * this.getValue() / (this.getValue() + 100.0D)), digits > 0 ? digits : 0), currencyIsoCode);
            return new TaxValueModel(this.getCode(), this.getValue(), this.isAbsolute(), DjpMathUtil.round(this.isAbsolute() ? this.getValue() * quantity : (priceIsNet ? price * this.getValue() / 100.0D : price * this.getValue() / (this.getValue() + 100.0D)), digits > 0 ? digits : 0), currencyIsoCode);
        } else {
            throw new IllegalArgumentException("cannot apply price " + price + " with currency " + currencyIsoCode + " to absolute tax value with currency " + this.getCurrencyIsoCode() + " currencies must be equal");
        }
    }

    public static Collection apply(double quantity, double price, int digits, Collection taxValues, boolean priceIsNet, String currencyIso) {
        if (taxValues != null && !taxValues.isEmpty()) {
            Collection ret = new ArrayList(taxValues.size());
            Iterator it = taxValues.iterator();

            while(it.hasNext()) {
                ret.add(((TaxValueModel)it.next()).apply(quantity, price, digits, priceIsNet, currencyIso));
            }

            return ret;
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public static double sumRelativeTaxValueModels(Collection taxValues) {
        if (taxValues != null && !taxValues.isEmpty()) {
            double sum = 0.0D;
            Iterator iter = taxValues.iterator();

            while(iter.hasNext()) {
                TaxValueModel taxValue = (TaxValueModel)iter.next();
                if (!taxValue.isAbsolute()) {
                    sum += taxValue.getValue();
                }
            }

            return sum;
        } else {
            return 0.0D;
        }
    }

    public static double sumAbsoluteTaxValueModels(Collection taxValues) {
        if (taxValues != null && !taxValues.isEmpty()) {
            double sum = 0.0D;
            Iterator iter = taxValues.iterator();

            while(iter.hasNext()) {
                TaxValueModel taxValue = (TaxValueModel)iter.next();
                if (taxValue.isAbsolute()) {
                    sum += taxValue.getValue();
                }
            }

            return sum;
        } else {
            return 0.0D;
        }
    }

    public static double sumAppliedTaxValueModels(Collection taxValues) {
        if (taxValues != null && !taxValues.isEmpty()) {
            double sum = 0.0D;

            TaxValueModel taxValue;
            for(Iterator iter = taxValues.iterator(); iter.hasNext(); sum += taxValue.getAppliedValue()) {
                taxValue = (TaxValueModel)iter.next();
            }

            return sum;
        } else {
            return 0.0D;
        }
    }

    public String toString() {
        return "<TV<" + this.getCode() + "#" + this.getValue() + "#" + this.isAbsolute() + "#" + this.getAppliedValue() + "#" + (this.getCurrencyIsoCode() != null ? this.getCurrencyIsoCode() : "NULL") + ">VT>";
    }

    public static String toString(Collection taxValueCollection) {
        if (taxValueCollection == null) {
            return null;
        } else if (taxValueCollection.isEmpty()) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Iterator it = taxValueCollection.iterator();

            while(it.hasNext()) {
                stringBuilder.append(((TaxValueModel)it.next()).toString());
                if (it.hasNext()) {
                    stringBuilder.append("|");
                }
            }

            stringBuilder.append("]");
            return stringBuilder.toString();
        }
    }

    public static Collection parseTaxValueModelCollection(String str) throws IllegalArgumentException {
        if (str == null) {
            return null;
        } else if (str.equals("[]")) {
            return new LinkedList();
        } else {
            Collection ret = new LinkedList();
            StringTokenizer st = new StringTokenizer(str.substring(1, str.length() - 1), "|");

            while(st.hasMoreTokens()) {
                ret.add(parseTaxValueModel(st.nextToken()));
            }

            return ret;
        }
    }

    public static TaxValueModel parseTaxValueModel(String str) throws IllegalArgumentException {
        try {
            int start = str.indexOf("<TV<");
            if (start < 0) {
                throw new IllegalArgumentException("could not find <TV< in tax value string '" + str + "'");
            } else {
                int end = str.indexOf(">VT>", start);
                if (start < 0) {
                    throw new IllegalArgumentException("could not find >VT> in tax value string '" + str + "'");
                } else {
                    int pos = 0;
                    String code = null;
                    double value = 0.0D;
                    boolean absolute = false;
                    double appliedValue = 0.0D;
                    String iso = null;

                    for(StringTokenizer st = new StringTokenizer(str.substring(start + "<TV<".length(), end), "#"); st.hasMoreTokens(); ++pos) {
                        String token = st.nextToken();
                        switch(pos) {
                            case 0:
                                code = token;
                                break;
                            case 1:
                                value = Double.parseDouble(token);
                                break;
                            case 2:
                                absolute = "true".equalsIgnoreCase(token);
                                break;
                            case 3:
                                appliedValue = Double.parseDouble(token);
                                break;
                            case 4:
                                iso = token;
                                break;
                            default:
                                throw new IllegalArgumentException("illgeal tax value string '" + str + "' (pos=" + pos + ",moreTokens=" + st.hasMoreTokens() + ",nextToke='" + token + "')");
                        }
                    }

                    if (pos < 3) {
                        throw new IllegalArgumentException("illgeal tax value string '" + str + "' (pos=" + pos + ")");
                    } else {
                        return new TaxValueModel(code, value, absolute, appliedValue, "NULL".equals(iso) ? null : iso);
                    }
                }
            }
        } catch (Exception var13) {
            throw new IllegalArgumentException("error parsing tax value string '" + str + "' : " + var13);
        }
    }

    public int hashCode() {
        return this.isAbsolute() ? this.getCode().hashCode() ^ (this.isAbsolute() ? 1 : 0) ^ (int)this.getValue() ^ (this.getCurrencyIsoCode() == null ? 0 : this.getCurrencyIsoCode().hashCode()) : this.getCode().hashCode() ^ (this.isAbsolute() ? 1 : 0) ^ (int)this.getValue();
    }

    public boolean equals(Object object) {
        String iso = this.getCurrencyIsoCode();
        return object instanceof TaxValueModel && this.getCode().equals(((TaxValueModel)object).getCode()) && this.absolute == ((TaxValueModel)object).isAbsolute() && (!this.absolute || iso == null && ((TaxValueModel)object).getCurrencyIsoCode() == null || iso.equals(((TaxValueModel)object).getCurrencyIsoCode())) && this.getValue() == ((TaxValueModel)object).getValue() && this.getAppliedValue() == ((TaxValueModel)object).getAppliedValue();
    }

    public TaxValueModel unapply() {
        return new TaxValueModel(this.getCode(), this.getValue(), this.isAbsolute(), this.getCurrencyIsoCode());
    }
}
