package com.djp.core.model;

import com.djp.core.enums.QuoteNotificationType;

import jakarta.persistence.*;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "Trigger")
public class TriggerModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean active;
    private boolean relative;
    private int day;
    private int hour;
    private int minute;
    private int month;
    private int second;
    private int weekInterval;
    private int year;
    private int maxAcceptableDelay;
    private Date activationTime;
    private String timeTable;
    private String cronExpression;

    @ElementCollection(targetClass = DayOfWeek.class)
    @CollectionTable(name = "Trigger2DaysOfWeek", joinColumns = @JoinColumn(name = "trigger"))
    @Enumerated(EnumType.STRING)
    @Column(name = "day")
    private Set<DayOfWeek> daysOfWeek;

    /**
     * Instead of de.hybris.platform.util.StandardDateRange, we can use start and end dates
     */
    private Date start;
    private Date end;

    @ManyToOne
    @JoinColumn
    private JobModel job;

    @ManyToOne
    @JoinColumn
    private CronJobModel cronJob;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isRelative() {
        return relative;
    }

    public void setRelative(boolean relative) {
        this.relative = relative;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getWeekInterval() {
        return weekInterval;
    }

    public void setWeekInterval(int weekInterval) {
        this.weekInterval = weekInterval;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMaxAcceptableDelay() {
        return maxAcceptableDelay;
    }

    public void setMaxAcceptableDelay(int maxAcceptableDelay) {
        this.maxAcceptableDelay = maxAcceptableDelay;
    }

    public Date getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public String getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(String timeTable) {
        this.timeTable = timeTable;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Set<DayOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Set<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public JobModel getJob() {
        return job;
    }

    public void setJob(JobModel job) {
        this.job = job;
    }

    public CronJobModel getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJobModel cronJob) {
        this.cronJob = cronJob;
    }
}
