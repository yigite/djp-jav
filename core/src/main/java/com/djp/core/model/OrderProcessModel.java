package com.djp.core.model;

import com.djp.core.enums.PackageTypeEnum;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OrderProcess")
@PrimaryKeyJoinColumn(name = "id")
public class OrderProcessModel extends BusinessProcessModel{

    @OneToMany(mappedBy = "parentProcess", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ConsignmentProcessModel> consignmentProcesses;

    @ManyToOne
    @JoinColumn
    private OrderModel order;

    private Integer sendOrderRetryCount;

    private PackageTypeEnum packageType;

    public Set<ConsignmentProcessModel> getConsignmentProcesses() {
        return consignmentProcesses;
    }

    public void setConsignmentProcesses(Set<ConsignmentProcessModel> consignmentProcesses) {
        this.consignmentProcesses = consignmentProcesses;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public Integer getSendOrderRetryCount() {
        return sendOrderRetryCount;
    }

    public void setSendOrderRetryCount(Integer sendOrderRetryCount) {
        this.sendOrderRetryCount = sendOrderRetryCount;
    }

    public PackageTypeEnum getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageTypeEnum packageType) {
        this.packageType = packageType;
    }
}
