package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "ValeSubService")
public class ValeSubServiceModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private boolean isParkingService;
    private boolean isValeService;
    private boolean isSubscription;
    private Integer oid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isParkingService() {
        return isParkingService;
    }

    public void setParkingService(boolean parkingService) {
        isParkingService = parkingService;
    }

    public boolean isValeService() {
        return isValeService;
    }

    public void setValeService(boolean valeService) {
        isValeService = valeService;
    }

    public boolean isSubscription() {
        return isSubscription;
    }

    public void setSubscription(boolean subscription) {
        isSubscription = subscription;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }
}
