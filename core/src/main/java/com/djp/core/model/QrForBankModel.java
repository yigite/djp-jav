package com.djp.core.model;

import com.djp.core.enums.QrType;
import com.djp.core.enums.StatusOfSentCompany;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "QrForBank")
public class QrForBankModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String refNumber;

    @OneToOne
    private MediaModel qr;
    private String banksClientId;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;
    private QrType qrType;
    private QrType whereUsed;
    private Date usageDate;
    private Boolean usedForCampaign;
    private String contractId;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private StatusOfSentCompany statusOfSentToCompany;
    private Boolean isSuccesfullySentToSap;
    private Double quantityToBeInvoiced;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public String getBanksClientId() {
        return banksClientId;
    }

    public void setBanksClientId(String banksClientId) {
        this.banksClientId = banksClientId;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public QrType getQrType() {
        return qrType;
    }

    public void setQrType(QrType qrType) {
        this.qrType = qrType;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public StatusOfSentCompany getStatusOfSentToCompany() {
        return statusOfSentToCompany;
    }

    public void setStatusOfSentToCompany(StatusOfSentCompany statusOfSentToCompany) {
        this.statusOfSentToCompany = statusOfSentToCompany;
    }

    public Boolean getSuccesfullySentToSap() {
        return isSuccesfullySentToSap;
    }

    public void setSuccesfullySentToSap(Boolean succesfullySentToSap) {
        isSuccesfullySentToSap = succesfullySentToSap;
    }

    public Double getQuantityToBeInvoiced() {
        return quantityToBeInvoiced;
    }

    public void setQuantityToBeInvoiced(Double quantityToBeInvoiced) {
        this.quantityToBeInvoiced = quantityToBeInvoiced;
    }
}
