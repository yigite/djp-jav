package com.djp.core.model;

import jakarta.persistence.*;
import com.djp.core.enums.ParkingProcessEnum;
import com.djp.core.enums.ParkingTicketTypeEnum;
import com.djp.core.enums.ParkingVehicleClassTypeEnum;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ParkingWProcess")
public class ParkingWProcessModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private String plate;
    private String processId;
    private String rawsData;
    private String salesDocument;
    private String ticketCode;
    private String ticketNo;
    private ParkingTicketTypeEnum ticketType;
    private ParkingProcessEnum process;
    private ParkingVehicleClassTypeEnum vehicleClass;

    @ElementCollection
    @CollectionTable(name="BillingResponseLog", joinColumns=@JoinColumn(name="parkingWProcess_id"))
    @Column(name="billingResponseLog")
    private Set<String> billingResponseLog;

    @OneToOne
    private ParkingCalculationModel calculation;

    @OneToMany(mappedBy = "parkingWProcess", fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CarParkingPaidProcessModel> carParkingProcesses;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getRawsData() {
        return rawsData;
    }

    public void setRawsData(String rawsData) {
        this.rawsData = rawsData;
    }

    public String getSalesDocument() {
        return salesDocument;
    }

    public void setSalesDocument(String salesDocument) {
        this.salesDocument = salesDocument;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public ParkingTicketTypeEnum getTicketType() {
        return ticketType;
    }

    public void setTicketType(ParkingTicketTypeEnum ticketType) {
        this.ticketType = ticketType;
    }

    public ParkingProcessEnum getProcess() {
        return process;
    }

    public void setProcess(ParkingProcessEnum process) {
        this.process = process;
    }

    public ParkingVehicleClassTypeEnum getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(ParkingVehicleClassTypeEnum vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public Set<String> getBillingResponseLog() {
        return billingResponseLog;
    }

    public void setBillingResponseLog(Set<String> billingResponseLog) {
        this.billingResponseLog = billingResponseLog;
    }

    public ParkingCalculationModel getCalculation() {
        return calculation;
    }

    public void setCalculation(ParkingCalculationModel calculation) {
        this.calculation = calculation;
    }

    public Set<CarParkingPaidProcessModel> getCarParkingProcesses() {
        return carParkingProcesses;
    }

    public void setCarParkingProcesses(Set<CarParkingPaidProcessModel> carParkingProcesses) {
        this.carParkingProcesses = carParkingProcesses;
    }
}
