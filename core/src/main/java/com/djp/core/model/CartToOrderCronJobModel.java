package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "CartToOrderCronJob")
@PrimaryKeyJoinColumn(name = "id")
public class CartToOrderCronJobModel extends CronJobModel {

    @ManyToOne
    @JoinColumn
    private CartModel cart;

    @ManyToOne
    @JoinColumn
    private AddressModel deliveryAddress;

    @ManyToOne
    @JoinColumn
    private AddressModel paymentAddress;

    @ManyToOne
    @JoinColumn
    private PaymentInfoModel paymentInfo;

    public CartToOrderCronJobModel() {
        super();
    }

    public CartModel getCart() {
        return cart;
    }

    public void setCart(CartModel cart) {
        this.cart = cart;
    }

    public AddressModel getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressModel deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AddressModel getPaymentAddress() {
        return paymentAddress;
    }

    public void setPaymentAddress(AddressModel paymentAddress) {
        this.paymentAddress = paymentAddress;
    }

    public PaymentInfoModel getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoModel paymentInfo) {
        this.paymentInfo = paymentInfo;
    }
}
