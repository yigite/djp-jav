package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Region")
public class RegionModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String isocodeShort;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private CountryModel country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsocodeShort() {
        return isocodeShort;
    }

    public void setIsocodeShort(String isocodeShort) {
        this.isocodeShort = isocodeShort;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }
}
