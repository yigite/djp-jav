package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpFlightInformation")
public class DjpFlightInformationModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String flightNature;
    private String flightNumber;
    private String stoDate;
    private String stoTime;
    private String airlineIcao;
    private String airlineName;
    private String internationalStatus;
    private String destinationIata;
    private String destinationName;
    private String destinationCountry;
    private String originIata;
    private String originName;
    private String originCity;
    private String operationalStatus;
    private String terminal;
    private String baggageclaim;
    private String runway;
    private String lounge;
    private String gate;
    private String counter;
    private String chute;
    private String bay;

    @OneToOne
    @JoinColumn
    private AbstractOrderModel order;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFlightNature() {
        return flightNature;
    }

    public void setFlightNature(String flightNature) {
        this.flightNature = flightNature;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getStoDate() {
        return stoDate;
    }

    public void setStoDate(String stoDate) {
        this.stoDate = stoDate;
    }

    public String getStoTime() {
        return stoTime;
    }

    public void setStoTime(String stoTime) {
        this.stoTime = stoTime;
    }

    public String getAirlineIcao() {
        return airlineIcao;
    }

    public void setAirlineIcao(String airlineIcao) {
        this.airlineIcao = airlineIcao;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getInternationalStatus() {
        return internationalStatus;
    }

    public void setInternationalStatus(String internationalStatus) {
        this.internationalStatus = internationalStatus;
    }

    public String getDestinationIata() {
        return destinationIata;
    }

    public void setDestinationIata(String destinationIata) {
        this.destinationIata = destinationIata;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getOriginIata() {
        return originIata;
    }

    public void setOriginIata(String originIata) {
        this.originIata = originIata;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getOperationalStatus() {
        return operationalStatus;
    }

    public void setOperationalStatus(String operationalStatus) {
        this.operationalStatus = operationalStatus;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getBaggageclaim() {
        return baggageclaim;
    }

    public void setBaggageclaim(String baggageclaim) {
        this.baggageclaim = baggageclaim;
    }

    public String getRunway() {
        return runway;
    }

    public void setRunway(String runway) {
        this.runway = runway;
    }

    public String getLounge() {
        return lounge;
    }

    public void setLounge(String lounge) {
        this.lounge = lounge;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getChute() {
        return chute;
    }

    public void setChute(String chute) {
        this.chute = chute;
    }

    public String getBay() {
        return bay;
    }

    public void setBay(String bay) {
        this.bay = bay;
    }

    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }
}
