package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "CMSRelation")
public class CMSRelationModel extends GenericItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String uid;

    @OneToOne
    private CatalogVersionModel catalogVersion;
}
