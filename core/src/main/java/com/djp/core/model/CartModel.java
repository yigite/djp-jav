package com.djp.core.model;

import com.djp.core.enums.ImportStatus;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "Cart")
@PrimaryKeyJoinColumn(name = "id")
public class CartModel extends AbstractOrderModel{

    private String sessionId;
    private String paymentTransactionId;
    private String paymentProvider;
    private Date lockUntilTime;
    private Date saveTime;
    private ImportStatus importStatus;

    @ManyToOne
    @JoinColumn(name = "user")
    private UserModel user;

    @OneToOne
    private UserModel savedBy;

    @OneToOne
    private CartModel tempCart;

    @OneToOne
    @JoinColumn
    private QuoteModel quoteReference;

    @OneToMany(mappedBy = "lastModifiedMasterCart", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartEntryModel> lastModifiedEntries;

    @OneToMany(mappedBy = "cart", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartToOrderCronJobModel> cartToOrderCronJob;

    public CartModel() {
        super();
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public void setPaymentTransactionId(String paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public String getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(String paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public Date getLockUntilTime() {
        return lockUntilTime;
    }

    public void setLockUntilTime(Date lockUntilTime) {
        this.lockUntilTime = lockUntilTime;
    }

    public Date getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(Date saveTime) {
        this.saveTime = saveTime;
    }

    public ImportStatus getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(ImportStatus importStatus) {
        this.importStatus = importStatus;
    }

    @Override
    public UserModel getUser() {
        return user;
    }

    @Override
    public void setUser(UserModel user) {
        this.user = user;
    }

    public UserModel getSavedBy() {
        return savedBy;
    }

    public void setSavedBy(UserModel savedBy) {
        this.savedBy = savedBy;
    }

    public CartModel getTempCart() {
        return tempCart;
    }

    public void setTempCart(CartModel tempCart) {
        this.tempCart = tempCart;
    }

    public QuoteModel getQuoteReference() {
        return quoteReference;
    }

    public void setQuoteReference(QuoteModel quoteReference) {
        this.quoteReference = quoteReference;
    }

    public Set<CartEntryModel> getLastModifiedEntries() {
        return lastModifiedEntries;
    }

    public void setLastModifiedEntries(Set<CartEntryModel> lastModifiedEntries) {
        this.lastModifiedEntries = lastModifiedEntries;
    }

    public Set<CartToOrderCronJobModel> getCartToOrderCronJob() {
        return cartToOrderCronJob;
    }

    public void setCartToOrderCronJob(Set<CartToOrderCronJobModel> cartToOrderCronJob) {
        this.cartToOrderCronJob = cartToOrderCronJob;
    }
}
