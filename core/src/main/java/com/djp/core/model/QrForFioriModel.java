package com.djp.core.model;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "QrForFiori")
public class QrForFioriModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date usageDate;

    private String plateNumber;

    private String carParkingUsedInfo;

    private Boolean usedVale;

    private PassengerType PassengerType;

    private Integer passengerCount;

    private Boolean forSomeBodyElseUsage;

    private Boolean usedForCampaign;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    //Many to one olabilir, akışa bağlı değişebilir
    private CustomerModel customer;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForFiori_QrType", joinColumns = @JoinColumn(name = "QrForFiori_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereIsValidQr;

    private QrType whereUsed;

    @OneToOne
    private DjpPassLocationsModel whichLocation;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public Set<QrType> getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(Set<QrType> whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCarParkingUsedInfo() {
        return carParkingUsedInfo;
    }

    public void setCarParkingUsedInfo(String carParkingUsedInfo) {
        this.carParkingUsedInfo = carParkingUsedInfo;
    }

    public Boolean getUsedVale() {
        return usedVale;
    }

    public void setUsedVale(Boolean usedVale) {
        this.usedVale = usedVale;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public com.djp.core.enums.PassengerType getPassengerType() {
        return PassengerType;
    }

    public void setPassengerType(com.djp.core.enums.PassengerType passengerType) {
        PassengerType = passengerType;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }

    public Boolean getForSomeBodyElseUsage() {
        return forSomeBodyElseUsage;
    }

    public void setForSomeBodyElseUsage(Boolean forSomeBodyElseUsage) {
        this.forSomeBodyElseUsage = forSomeBodyElseUsage;
    }
}
