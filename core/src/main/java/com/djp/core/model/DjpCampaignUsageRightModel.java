package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="DjpCampaignUsageRight")
public class DjpCampaignUsageRightModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;
    private int usageCount;

    @OneToOne
    @Column(unique = true)
    private DjpCampaignModel campaign;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="DjpCmpurPrRel",
            joinColumns = {@JoinColumn(name="superCategory")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> products;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public DjpCampaignModel getCampaign() {
        return campaign;
    }

    public void setCampaign(DjpCampaignModel campaign) {
        this.campaign = campaign;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }
}
