package com.djp.core.model;

import com.djp.core.enums.ContractedType;
import com.djp.core.enums.QrType;

import jakarta.persistence.*;

@Entity
@Table(name = "ContractedCompanies")
public class ContractedCompaniesModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String companyId;

    private String companyName;

    private String contractId;

    private QrType contractedService;

    @OneToOne
    private DjpPassLocationsModel validLocation;

    private ContractedType contractedType;

    private Integer lengthOfStayHour;

    private Integer maxUsageCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public QrType getContractedService() {
        return contractedService;
    }

    public void setContractedService(QrType contractedService) {
        this.contractedService = contractedService;
    }

    public DjpPassLocationsModel getValidLocation() {
        return validLocation;
    }

    public void setValidLocation(DjpPassLocationsModel validLocation) {
        this.validLocation = validLocation;
    }

    public ContractedType getContractedType() {
        return contractedType;
    }

    public void setContractedType(ContractedType contractedType) {
        this.contractedType = contractedType;
    }

    public Integer getLengthOfStayHour() {
        return lengthOfStayHour;
    }

    public void setLengthOfStayHour(Integer lengthOfStayHour) {
        this.lengthOfStayHour = lengthOfStayHour;
    }

    public Integer getMaxUsageCount() {
        return maxUsageCount;
    }

    public void setMaxUsageCount(Integer maxUsageCount) {
        this.maxUsageCount = maxUsageCount;
    }
}
