package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Medias")
@Inheritance(strategy = InheritanceType.JOINED)
public class MediaModel extends AbstractMediaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;

    private String internalURL;

    private String URL;

    private String URL2;

    private String downloadURL;

    private String description;

    private String altText;

    private Boolean removable;

//    private MediaFormat mediaFormat;
//    private MediaFormat folder;

    private String subFolderPath;

    @Transient
    private Set<MediaModel> foreignDataOwners;

    @Transient
    private Set<PrincipalModel> permittedPrincipals;

    @Transient
    private Set<PrincipalModel> deniedPrincipals;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CMSSite")
    private CMSSiteModel cmsSiteModel;

    @ManyToOne
    @JoinColumn
    private MediaContainerModel container;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInternalURL() {
        return internalURL;
    }

    public void setInternalURL(String internalURL) {
        this.internalURL = internalURL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getURL2() {
        return URL2;
    }

    public void setURL2(String URL2) {
        this.URL2 = URL2;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public Boolean getRemovable() {
        return removable;
    }

    public void setRemovable(Boolean removable) {
        this.removable = removable;
    }

    public String getSubFolderPath() {
        return subFolderPath;
    }

    public void setSubFolderPath(String subFolderPath) {
        this.subFolderPath = subFolderPath;
    }

    public Set<MediaModel> getForeignDataOwners() {
        return foreignDataOwners;
    }

    public void setForeignDataOwners(Set<MediaModel> foreignDataOwners) {
        this.foreignDataOwners = foreignDataOwners;
    }

    public Set<PrincipalModel> getPermittedPrincipals() {
        return permittedPrincipals;
    }

    public void setPermittedPrincipals(Set<PrincipalModel> permittedPrincipals) {
        this.permittedPrincipals = permittedPrincipals;
    }

    public Set<PrincipalModel> getDeniedPrincipals() {
        return deniedPrincipals;
    }

    public void setDeniedPrincipals(Set<PrincipalModel> deniedPrincipals) {
        this.deniedPrincipals = deniedPrincipals;
    }

    public MediaContainerModel getContainer() {
        return container;
    }

    public void setContainer(MediaContainerModel container) {
        this.container = container;
    }
}
