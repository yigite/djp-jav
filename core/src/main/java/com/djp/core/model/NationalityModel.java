package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Nationality")
public class NationalityModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public String code;

    private String nationalityName;

    @OneToMany(mappedBy = "nationality", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CustomerModel> customers;

    @OneToMany(mappedBy = "nationality", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<DjpUniqueTokenModel> djpUniqueTokens;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public Set<CustomerModel> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<CustomerModel> customers) {
        this.customers = customers;
    }

    public Set<DjpUniqueTokenModel> getDjpUniqueTokens() {
        return djpUniqueTokens;
    }

    public void setDjpUniqueTokens(Set<DjpUniqueTokenModel> djpUniqueTokens) {
        this.djpUniqueTokens = djpUniqueTokens;
    }
}
