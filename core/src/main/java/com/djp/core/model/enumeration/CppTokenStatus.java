package com.djp.core.model.enumeration;

public enum CppTokenStatus {
    GENERATED("AGENERATED"),
    SENT_TO_BANK("SENT_TO_BANK"),
    BANK_SUCCESS("BANK_SUCCESS"),
    BANK_FACTIVE("BANK_FACTIVE");

    private final String code;

    CppTokenStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
