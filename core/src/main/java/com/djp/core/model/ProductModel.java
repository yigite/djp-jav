package com.djp.core.model;

import com.djp.core.enums.ProductDiscountGroup;
import com.djp.core.enums.ProductPriceGroup;
import com.djp.core.enums.ProductTaxGroup;
import com.djp.core.model.enumeration.ArticleApprovalStatus;
import com.djp.core.model.enumeration.ArticleStatus;
import com.djp.core.model.enumeration.IDType;
import org.springframework.core.annotation.Order;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "Product", uniqueConstraints = @UniqueConstraint(columnNames = {"code", "catalog", "catalogVersion"}))
public class ProductModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String code;
    private String name;
    private String periodOfValidDescription;
    private String description;
    private String summary;
    private Date onlineDate;
    private Date offlineDate;
    private Date sapBlockedDate;
    private String ean;
    private String sapEAN;
    private String supplierAlternativeAID;
    private String manufacturerAID;
    private String manufacturerName;
    private String manufacturerTypeDescription;
    private String erpGroupBuyer;
    private String erpGroupSupplier;
    private String sapProductID;
    private Double deliveryTime;
    private Double sapBaseUnitConversion;
    private String remarks;
    private String segment;
    private String seoDescription;
    private String seoTitle;
    private String seoH1Tag;
    private String mobileDescription;
    private Double numberContentUnits;
    private Integer minOrderQuantity;
    private Integer priorityForSolrSort;
    private Integer maxOrderQuantity;
    private Integer orderQuantityInterval;
    private Double priceQuantity;
    private Integer startLineNumber;
    private Integer endLineNumber;
    private Integer xmlcontent;
    private boolean photoRequired;
    private boolean convertEuroPriceForTRY;
    private boolean specialPriceFromSap;
    private boolean isPacket;
    private boolean sapBlocked;
    private boolean plateRequired;
    private boolean sapConfigurable;
    private boolean soldIndividually;
    private boolean forWeb;
    private boolean isTherePriceUpdated;
    private Long sequenceId;
    private BigDecimal originalProductPrice;
    private ProductPriceGroup Europe1PriceFactory_PPG;
    private ProductTaxGroup Europe1PriceFactory_PTG;
    private ProductDiscountGroup Europe1PriceFactory_PDG;

    @ManyToOne
    @JoinColumn
    private UnitModel unit;

    @ManyToOne
    @JoinColumn
    private WarehouseModel sapPlant;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(
            name = "Product2OrderRel",
            joinColumns = {@JoinColumn(name = "product")},
            inverseJoinColumns = {@JoinColumn(name = "order")}
    )
    private Set<OrderModel> order;

    @OneToOne
    private MediaModel thumbnail;
    @OneToOne
    private MediaModel picture;

    @OneToOne
    @JoinColumn(name = "catalog")
    private CatalogModel catalog;

    @OneToOne
    @JoinColumn(name = "catalogVersion")
    private CatalogVersionModel catalogVersion;

    @ManyToMany(mappedBy = "products", cascade = {CascadeType.ALL})
    private Set<StockLevelModel> stockLevels;

    @OneToMany(mappedBy = "product")
    private List<ProductBomComponentModel> bomComponentList;

    @ElementCollection
    @CollectionTable(name = "product_buyer_ids_mapping_table", joinColumns = @JoinColumn(name = "product_id"))
    @MapKeyColumn(name = "id_type")
    @Column(name = "value")
    private Map<IDType, String> buyerIDS;

    @ElementCollection
    @CollectionTable(name = "product_special_treatment_classes_mapping_table", joinColumns = @JoinColumn(name = "product_id"))
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    private Map<String, String> specialTreatmentClasses;

    @ElementCollection
    @CollectionTable(name = "product_articleStatus_mapping_table", joinColumns = @JoinColumn(name = "product_id"))
    @MapKeyColumn(name = "article_status")
    @Column(name = "value")
    private Map<ArticleStatus, String> articleStatus;
    private ArticleApprovalStatus approvalStatus;
    @OneToOne
    private UnitModel contentUnit;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> normal;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> thumbnails;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> detail;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> logo;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> data_sheet;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> others;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProductFeatureModel> untypedFeatures;

    private String classificationIndexString;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<ClassificationClassModel> classificationClasses;

    @OneToOne
    private VariantTypeModel variantType;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProductFeatureModel> features;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<VariantProductModel> variants;

    @OneToMany(mappedBy = "source", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Set<ProductReferenceModel> productReferences;

    @OneToOne
    private VariantProductModel baseProduct;

    @ManyToOne
    @JoinColumn
    private CampaignSegmentInformationModel campaignSegmentInformation;

    @ManyToMany
    @JoinTable(name = "Prod2BundleTemplRel",
            joinColumns = {@JoinColumn(name = "product")},
            inverseJoinColumns = {@JoinColumn(name = "bundleTemplate")})
    private Set<BundleTemplateModel> bundleTemplates;

    @ManyToMany(mappedBy = "conditionalProducts", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AbstractBundleRuleModel> conditionalBundleRules;

    @ManyToMany(mappedBy = "targetProducts", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AbstractBundleRuleModel> targetBundleRules;

    @OneToOne
    private ProductEnumTypeModel qrValidType;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<KeywordModel> keywords;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CategoryModel> supercategories;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CMSProductRestrictionModel> restrictions;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<DjpCampaignUsageRightModel> djpCampaignUsageRights;

    @ManyToMany(mappedBy = "productList", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CampaignNotificationModel> campaign;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<DiscountRowModel> ownEurope1Discounts;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PriceRowModel> ownEurope1Prices;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<TaxRowModel> ownEurope1Taxes;

    @OneToMany
    @JoinColumn
    private Set<BarcodeMediaModel> barcodes;

    @ManyToOne
    @JoinColumn
    private ProductOrderLimitModel productOrderLimit;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<VendorModel> vendors;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE})
    private Set<DeliveryModeModel> deliveryModes;

    @OneToMany
    @JoinColumn
    private Set<MediaContainerModel> galleryImages;

    @OneToMany
    @JoinColumn
    private Set<ProductModel> additionalServices;

    @OneToMany
    @JoinColumn
    private Set<DjpExternalCompanyModel> canSellExternalCompanies;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriodOfValidDescription() {
        return periodOfValidDescription;
    }

    public void setPeriodOfValidDescription(String periodOfValidDescription) {
        this.periodOfValidDescription = periodOfValidDescription;
    }

    public String getMobileDescription() {
        return mobileDescription;
    }

    public void setMobileDescription(String mobileDescription) {
        this.mobileDescription = mobileDescription;
    }

    public Double getNumberContentUnits() {
        return numberContentUnits;
    }

    public void setNumberContentUnits(Double numberContentUnits) {
        this.numberContentUnits = numberContentUnits;
    }

    public Integer getMinOrderQuantity() {
        return minOrderQuantity;
    }

    public void setMinOrderQuantity(Integer minOrderQuantity) {
        this.minOrderQuantity = minOrderQuantity;
    }

    public Integer getPriorityForSolrSort() {
        return priorityForSolrSort;
    }

    public void setPriorityForSolrSort(Integer priorityForSolrSort) {
        this.priorityForSolrSort = priorityForSolrSort;
    }

    public Integer getMaxOrderQuantity() {
        return maxOrderQuantity;
    }

    public void setMaxOrderQuantity(Integer maxOrderQuantity) {
        this.maxOrderQuantity = maxOrderQuantity;
    }

    public Integer getOrderQuantityInterval() {
        return orderQuantityInterval;
    }

    public void setOrderQuantityInterval(Integer orderQuantityInterval) {
        this.orderQuantityInterval = orderQuantityInterval;
    }

    public Double getPriceQuantity() {
        return priceQuantity;
    }

    public void setPriceQuantity(Double priceQuantity) {
        this.priceQuantity = priceQuantity;
    }

    public Integer getStartLineNumber() {
        return startLineNumber;
    }

    public void setStartLineNumber(Integer startLineNumber) {
        this.startLineNumber = startLineNumber;
    }

    public Integer getEndLineNumber() {
        return endLineNumber;
    }

    public void setEndLineNumber(Integer endLineNumber) {
        this.endLineNumber = endLineNumber;
    }

    public Integer getXmlcontent() {
        return xmlcontent;
    }

    public void setXmlcontent(Integer xmlcontent) {
        this.xmlcontent = xmlcontent;
    }

    public boolean isPhotoRequired() {
        return photoRequired;
    }

    public void setPhotoRequired(boolean photoRequired) {
        this.photoRequired = photoRequired;
    }

    public boolean isConvertEuroPriceForTRY() {
        return convertEuroPriceForTRY;
    }

    public void setConvertEuroPriceForTRY(boolean convertEuroPriceForTRY) {
        this.convertEuroPriceForTRY = convertEuroPriceForTRY;
    }

    public boolean isSpecialPriceFromSap() {
        return specialPriceFromSap;
    }

    public void setSpecialPriceFromSap(boolean specialPriceFromSap) {
        this.specialPriceFromSap = specialPriceFromSap;
    }

    public boolean isPacket() {
        return isPacket;
    }

    public void setPacket(boolean packet) {
        isPacket = packet;
    }

    public boolean isSapBlocked() {
        return sapBlocked;
    }

    public void setSapBlocked(boolean sapBlocked) {
        this.sapBlocked = sapBlocked;
    }

    public boolean isPlateRequired() {
        return plateRequired;
    }

    public void setPlateRequired(boolean plateRequired) {
        this.plateRequired = plateRequired;
    }

    public boolean isSapConfigurable() {
        return sapConfigurable;
    }

    public void setSapConfigurable(boolean sapConfigurable) {
        this.sapConfigurable = sapConfigurable;
    }

    public boolean isSoldIndividually() {
        return soldIndividually;
    }

    public void setSoldIndividually(boolean soldIndividually) {
        this.soldIndividually = soldIndividually;
    }

    public boolean isForWeb() {
        return forWeb;
    }

    public void setForWeb(boolean forWeb) {
        this.forWeb = forWeb;
    }

    public boolean isTherePriceUpdated() {
        return isTherePriceUpdated;
    }

    public void setTherePriceUpdated(boolean therePriceUpdated) {
        isTherePriceUpdated = therePriceUpdated;
    }

    public Long getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Long sequenceId) {
        this.sequenceId = sequenceId;
    }

    public BigDecimal getOriginalProductPrice() {
        return originalProductPrice;
    }

    public void setOriginalProductPrice(BigDecimal originalProductPrice) {
        this.originalProductPrice = originalProductPrice;
    }

    public ProductPriceGroup getEurope1PriceFactory_PPG() {
        return Europe1PriceFactory_PPG;
    }

    public void setEurope1PriceFactory_PPG(ProductPriceGroup europe1PriceFactory_PPG) {
        Europe1PriceFactory_PPG = europe1PriceFactory_PPG;
    }

    public ProductTaxGroup getEurope1PriceFactory_PTG() {
        return Europe1PriceFactory_PTG;
    }

    public void setEurope1PriceFactory_PTG(ProductTaxGroup europe1PriceFactory_PTG) {
        Europe1PriceFactory_PTG = europe1PriceFactory_PTG;
    }

    public ProductDiscountGroup getEurope1PriceFactory_PDG() {
        return Europe1PriceFactory_PDG;
    }

    public void setEurope1PriceFactory_PDG(ProductDiscountGroup europe1PriceFactory_PDG) {
        Europe1PriceFactory_PDG = europe1PriceFactory_PDG;
    }

    public UnitModel getUnit() {
        return unit;
    }

    public void setUnit(UnitModel unit) {
        this.unit = unit;
    }

    public WarehouseModel getSapPlant() {
        return sapPlant;
    }

    public void setSapPlant(WarehouseModel sapPlant) {
        this.sapPlant = sapPlant;
    }

    public Set<OrderModel> getOrder() {
        return order;
    }

    public void setOrder(Set<OrderModel> order) {
        this.order = order;
    }

    public MediaModel getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(MediaModel thumbnail) {
        this.thumbnail = thumbnail;
    }

    public MediaModel getPicture() {
        return picture;
    }

    public void setPicture(MediaModel picture) {
        this.picture = picture;
    }

    public CatalogModel getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogModel catalog) {
        this.catalog = catalog;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public Set<StockLevelModel> getStockLevels() {
        return stockLevels;
    }

    public void setStockLevels(Set<StockLevelModel> stockLevels) {
        this.stockLevels = stockLevels;
    }

    public List<ProductBomComponentModel> getBomComponentList() {
        return bomComponentList;
    }

    public void setBomComponentList(List<ProductBomComponentModel> bomComponentList) {
        this.bomComponentList = bomComponentList;
    }

    public Map<IDType, String> getBuyerIDS() {
        return buyerIDS;
    }

    public void setBuyerIDS(Map<IDType, String> buyerIDS) {
        this.buyerIDS = buyerIDS;
    }

    public Map<String, String> getSpecialTreatmentClasses() {
        return specialTreatmentClasses;
    }

    public void setSpecialTreatmentClasses(Map<String, String> specialTreatmentClasses) {
        this.specialTreatmentClasses = specialTreatmentClasses;
    }

    public Map<ArticleStatus, String> getArticleStatus() {
        return articleStatus;
    }

    public void setArticleStatus(Map<ArticleStatus, String> articleStatus) {
        this.articleStatus = articleStatus;
    }

    public ArticleApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ArticleApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public UnitModel getContentUnit() {
        return contentUnit;
    }

    public void setContentUnit(UnitModel contentUnit) {
        this.contentUnit = contentUnit;
    }

    public Set<MediaModel> getNormal() {
        return normal;
    }

    public void setNormal(Set<MediaModel> normal) {
        this.normal = normal;
    }

    public Set<MediaModel> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Set<MediaModel> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public Set<MediaModel> getDetail() {
        return detail;
    }

    public void setDetail(Set<MediaModel> detail) {
        this.detail = detail;
    }

    public Set<MediaModel> getLogo() {
        return logo;
    }

    public void setLogo(Set<MediaModel> logo) {
        this.logo = logo;
    }

    public Set<MediaModel> getData_sheet() {
        return data_sheet;
    }

    public void setData_sheet(Set<MediaModel> data_sheet) {
        this.data_sheet = data_sheet;
    }

    public Set<MediaModel> getOthers() {
        return others;
    }

    public void setOthers(Set<MediaModel> others) {
        this.others = others;
    }

    public List<ProductFeatureModel> getUntypedFeatures() {
        return untypedFeatures;
    }

    public void setUntypedFeatures(List<ProductFeatureModel> untypedFeatures) {
        this.untypedFeatures = untypedFeatures;
    }

    public String getClassificationIndexString() {
        return classificationIndexString;
    }

    public void setClassificationIndexString(String classificationIndexString) {
        this.classificationIndexString = classificationIndexString;
    }

    public Set<ClassificationClassModel> getClassificationClasses() {
        return classificationClasses;
    }

    public void setClassificationClasses(Set<ClassificationClassModel> classificationClasses) {
        this.classificationClasses = classificationClasses;
    }

    public VariantTypeModel getVariantType() {
        return variantType;
    }

    public void setVariantType(VariantTypeModel variantType) {
        this.variantType = variantType;
    }

    public List<ProductFeatureModel> getFeatures() {
        return features;
    }

    public void setFeatures(List<ProductFeatureModel> features) {
        this.features = features;
    }

    public Set<VariantProductModel> getVariants() {
        return variants;
    }

    public void setVariants(Set<VariantProductModel> variants) {
        this.variants = variants;
    }

    public Set<ProductReferenceModel> getProductReferences() {
        return productReferences;
    }

    public void setProductReferences(Set<ProductReferenceModel> productReferences) {
        this.productReferences = productReferences;
    }

    public VariantProductModel getBaseProduct() {
        return baseProduct;
    }

    public void setBaseProduct(VariantProductModel baseProduct) {
        this.baseProduct = baseProduct;
    }

    public CampaignSegmentInformationModel getCampaignSegmentInformation() {
        return campaignSegmentInformation;
    }

    public void setCampaignSegmentInformation(CampaignSegmentInformationModel campaignSegmentInformation) {
        this.campaignSegmentInformation = campaignSegmentInformation;
    }

    public Set<BundleTemplateModel> getBundleTemplates() {
        return bundleTemplates;
    }

    public void setBundleTemplates(Set<BundleTemplateModel> bundleTemplates) {
        this.bundleTemplates = bundleTemplates;
    }

    public Set<AbstractBundleRuleModel> getConditionalBundleRules() {
        return conditionalBundleRules;
    }

    public void setConditionalBundleRules(Set<AbstractBundleRuleModel> conditionalBundleRules) {
        this.conditionalBundleRules = conditionalBundleRules;
    }

    public Set<AbstractBundleRuleModel> getTargetBundleRules() {
        return targetBundleRules;
    }

    public void setTargetBundleRules(Set<AbstractBundleRuleModel> targetBundleRules) {
        this.targetBundleRules = targetBundleRules;
    }

    public ProductEnumTypeModel getQrValidType() {
        return qrValidType;
    }

    public void setQrValidType(ProductEnumTypeModel qrValidType) {
        this.qrValidType = qrValidType;
    }

    public Set<KeywordModel> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<KeywordModel> keywords) {
        this.keywords = keywords;
    }

    public Set<CategoryModel> getSupercategories() {
        return supercategories;
    }

    public void setSupercategories(Set<CategoryModel> supercategories) {
        this.supercategories = supercategories;
    }

    public Set<CMSProductRestrictionModel> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(Set<CMSProductRestrictionModel> restrictions) {
        this.restrictions = restrictions;
    }

    public Set<DjpCampaignUsageRightModel> getDjpCampaignUsageRights() {
        return djpCampaignUsageRights;
    }

    public void setDjpCampaignUsageRights(Set<DjpCampaignUsageRightModel> djpCampaignUsageRights) {
        this.djpCampaignUsageRights = djpCampaignUsageRights;
    }

    public Set<CampaignNotificationModel> getCampaign() {
        return campaign;
    }

    public void setCampaign(Set<CampaignNotificationModel> campaign) {
        this.campaign = campaign;
    }

    public Set<DiscountRowModel> getOwnEurope1Discounts() {
        return ownEurope1Discounts;
    }

    public void setOwnEurope1Discounts(Set<DiscountRowModel> ownEurope1Discounts) {
        this.ownEurope1Discounts = ownEurope1Discounts;
    }

    public Set<PriceRowModel> getOwnEurope1Prices() {
        return ownEurope1Prices;
    }

    public void setOwnEurope1Prices(Set<PriceRowModel> ownEurope1Prices) {
        this.ownEurope1Prices = ownEurope1Prices;
    }

    public Set<TaxRowModel> getOwnEurope1Taxes() {
        return ownEurope1Taxes;
    }

    public void setOwnEurope1Taxes(Set<TaxRowModel> ownEurope1Taxes) {
        this.ownEurope1Taxes = ownEurope1Taxes;
    }

    public Set<BarcodeMediaModel> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(Set<BarcodeMediaModel> barcodes) {
        this.barcodes = barcodes;
    }

    public ProductOrderLimitModel getProductOrderLimit() {
        return productOrderLimit;
    }

    public void setProductOrderLimit(ProductOrderLimitModel productOrderLimit) {
        this.productOrderLimit = productOrderLimit;
    }

    public Set<VendorModel> getVendors() {
        return vendors;
    }

    public void setVendors(Set<VendorModel> vendors) {
        this.vendors = vendors;
    }

    public Set<DeliveryModeModel> getDeliveryModes() {
        return deliveryModes;
    }

    public void setDeliveryModes(Set<DeliveryModeModel> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    public Set<MediaContainerModel> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(Set<MediaContainerModel> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public Set<ProductModel> getAdditionalServices() {
        return additionalServices;
    }

    public void setAdditionalServices(Set<ProductModel> additionalServices) {
        this.additionalServices = additionalServices;
    }

    public Set<DjpExternalCompanyModel> getCanSellExternalCompanies() {
        return canSellExternalCompanies;
    }

    public void setCanSellExternalCompanies(Set<DjpExternalCompanyModel> canSellExternalCompanies) {
        this.canSellExternalCompanies = canSellExternalCompanies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(Date onlineDate) {
        this.onlineDate = onlineDate;
    }

    public Date getOfflineDate() {
        return offlineDate;
    }

    public void setOfflineDate(Date offlineDate) {
        this.offlineDate = offlineDate;
    }

    public Date getSapBlockedDate() {
        return sapBlockedDate;
    }

    public void setSapBlockedDate(Date sapBlockedDate) {
        this.sapBlockedDate = sapBlockedDate;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getSapEAN() {
        return sapEAN;
    }

    public void setSapEAN(String sapEAN) {
        this.sapEAN = sapEAN;
    }

    public String getSupplierAlternativeAID() {
        return supplierAlternativeAID;
    }

    public void setSupplierAlternativeAID(String supplierAlternativeAID) {
        this.supplierAlternativeAID = supplierAlternativeAID;
    }

    public String getManufacturerAID() {
        return manufacturerAID;
    }

    public void setManufacturerAID(String manufacturerAID) {
        this.manufacturerAID = manufacturerAID;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getManufacturerTypeDescription() {
        return manufacturerTypeDescription;
    }

    public void setManufacturerTypeDescription(String manufacturerTypeDescription) {
        this.manufacturerTypeDescription = manufacturerTypeDescription;
    }

    public String getErpGroupBuyer() {
        return erpGroupBuyer;
    }

    public void setErpGroupBuyer(String erpGroupBuyer) {
        this.erpGroupBuyer = erpGroupBuyer;
    }

    public String getErpGroupSupplier() {
        return erpGroupSupplier;
    }

    public void setErpGroupSupplier(String erpGroupSupplier) {
        this.erpGroupSupplier = erpGroupSupplier;
    }

    public String getSapProductID() {
        return sapProductID;
    }

    public void setSapProductID(String sapProductID) {
        this.sapProductID = sapProductID;
    }

    public Double getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Double deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Double getSapBaseUnitConversion() {
        return sapBaseUnitConversion;
    }

    public void setSapBaseUnitConversion(Double sapBaseUnitConversion) {
        this.sapBaseUnitConversion = sapBaseUnitConversion;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoH1Tag() {
        return seoH1Tag;
    }

    public void setSeoH1Tag(String seoH1Tag) {
        this.seoH1Tag = seoH1Tag;
    }
}
