package com.djp.core.model;

import com.djp.core.enums.QrType;
import com.djp.core.enums.StatusOfSentCompany;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "DjpVoucherCodeUsages")
public class DjpVoucherCodeUsagesModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private QrType whereUsed;

    private String sapOrderID;

    private Date usageDate;

    private Double paidPrice;

    private String contractID;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private StatusOfSentCompany statusOfSentToCompany;

    private Boolean isSuccesfullySentToSap;

    private Double quantityToBeInvoiced;

    @ManyToOne
    private QrForVoucherModel voucher;

    @OneToMany
    private List<DjpBoardingPassModel> boardingpasses;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public String getSapOrderID() {
        return sapOrderID;
    }

    public void setSapOrderID(String sapOrderID) {
        this.sapOrderID = sapOrderID;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public Double getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(Double paidPrice) {
        this.paidPrice = paidPrice;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public StatusOfSentCompany getStatusOfSentToCompany() {
        return statusOfSentToCompany;
    }

    public void setStatusOfSentToCompany(StatusOfSentCompany statusOfSentToCompany) {
        this.statusOfSentToCompany = statusOfSentToCompany;
    }

    public Boolean getSuccesfullySentToSap() {
        return isSuccesfullySentToSap;
    }

    public void setSuccesfullySentToSap(Boolean succesfullySentToSap) {
        isSuccesfullySentToSap = succesfullySentToSap;
    }

    public Double getQuantityToBeInvoiced() {
        return quantityToBeInvoiced;
    }

    public void setQuantityToBeInvoiced(Double quantityToBeInvoiced) {
        this.quantityToBeInvoiced = quantityToBeInvoiced;
    }

    public QrForVoucherModel getVoucher() {
        return voucher;
    }

    public void setVoucher(QrForVoucherModel voucher) {
        this.voucher = voucher;
    }

    public List<DjpBoardingPassModel> getBoardingpasses() {
        return boardingpasses;
    }

    public void setBoardingpasses(List<DjpBoardingPassModel> boardingpasses) {
        this.boardingpasses = boardingpasses;
    }
}
