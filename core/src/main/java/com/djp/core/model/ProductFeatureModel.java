package com.djp.core.model;

import org.hibernate.annotations.Type;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ProductFeature")
public class ProductFeatureModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String qualifier;

//    private ClassAttributeAssignment classificationAttributeAssignment;
    @OneToOne
    private LanguageModel language;
    private Integer valuePosition;
    private Integer featurePosition;
    private Integer valueType;
    private String stringValue;
    private Boolean booleanValue;
    private BigDecimal numberValue;

    private String rawValue;

    @Transient
    private Object value;

//    private ClassificationAttributeUnit unit;

    private String valueDetails;
    private String description;

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public LanguageModel getLanguage() {
        return language;
    }

    public void setLanguage(LanguageModel language) {
        this.language = language;
    }

    public Integer getValuePosition() {
        return valuePosition;
    }

    public void setValuePosition(Integer valuePosition) {
        this.valuePosition = valuePosition;
    }

    public Integer getFeaturePosition() {
        return featurePosition;
    }

    public void setFeaturePosition(Integer featurePosition) {
        this.featurePosition = featurePosition;
    }

    public Integer getValueType() {
        return valueType;
    }

    public void setValueType(Integer valueType) {
        this.valueType = valueType;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public BigDecimal getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(BigDecimal numberValue) {
        this.numberValue = numberValue;
    }

    public Object getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getValueDetails() {
        return valueDetails;
    }

    public void setValueDetails(String valueDetails) {
        this.valueDetails = valueDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
