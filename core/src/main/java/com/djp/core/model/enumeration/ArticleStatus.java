package com.djp.core.model.enumeration;

public enum ArticleStatus {
    BARGAIN("BARGAIN"),
    NEW_ARTICLE("NEW_ARTICLE"),
    OLD_ARTICLE("OLD_ARTICLE"),
    NEW("NEW"),
    USED("USED"),
    REFURBISHED("REFURBISHED"),
    CORE_ARTICLE("CORE_ARTICLE"),
    OTHERS("OTHERS");
    private String Code;

    ArticleStatus(String code) {
        Code = code;
    }

    public String getCode() {
        return Code;
    }
}
