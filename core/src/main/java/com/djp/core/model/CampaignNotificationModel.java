package com.djp.core.model;

import com.djp.core.enums.CustomerClassEnum;
import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "CampaignNotification")
public class CampaignNotificationModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private String integrationId;
    private String title;
    private String speechTitle;
    private String description;
    private String brandIntegrationId;
    private boolean createQrCode;
    private boolean shouldSpeechBubble;
    private CustomerClassEnum customerClass;

    @OneToOne
    private MediaModel media;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="CampNot2Prod",
            joinColumns = {@JoinColumn(name="campaign")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> productList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntegrationId() {
        return integrationId;
    }

    public void setIntegrationId(String integrationId) {
        this.integrationId = integrationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpeechTitle() {
        return speechTitle;
    }

    public void setSpeechTitle(String speechTitle) {
        this.speechTitle = speechTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrandIntegrationId() {
        return brandIntegrationId;
    }

    public void setBrandIntegrationId(String brandIntegrationId) {
        this.brandIntegrationId = brandIntegrationId;
    }

    public boolean isCreateQrCode() {
        return createQrCode;
    }

    public void setCreateQrCode(boolean createQrCode) {
        this.createQrCode = createQrCode;
    }

    public boolean isShouldSpeechBubble() {
        return shouldSpeechBubble;
    }

    public void setShouldSpeechBubble(boolean shouldSpeechBubble) {
        this.shouldSpeechBubble = shouldSpeechBubble;
    }

    public CustomerClassEnum getCustomerClass() {
        return customerClass;
    }

    public void setCustomerClass(CustomerClassEnum customerClass) {
        this.customerClass = customerClass;
    }

    public MediaModel getMedia() {
        return media;
    }

    public void setMedia(MediaModel media) {
        this.media = media;
    }

    public Set<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(Set<ProductModel> productList) {
        this.productList = productList;
    }
}
