package com.djp.core.model;

import jakarta.persistence.*;


@Entity
@Table(name="ComposedType")
@Inheritance(strategy = InheritanceType.JOINED)
public class ComposedTypeModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private String name;
    private String description;

    @ManyToOne
    @JoinColumn
    private ComposedTypeModel superType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ComposedTypeModel getSuperType() {
        return superType;
    }

    public void setSuperType(ComposedTypeModel superType) {
        this.superType = superType;
    }
}
