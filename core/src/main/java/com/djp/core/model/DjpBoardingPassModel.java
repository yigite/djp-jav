package com.djp.core.model;

import com.djp.core.enums.PassengerType;
import com.djp.core.enums.QrType;
import com.djp.core.enums.UsageMethod;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "DjpBoardingPass")
public class DjpBoardingPassModel extends ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String userUid;

    private String passportNumber ;

    @Column(length = 1024)
    private String pnrCode ;

    private Date actionDate ;

    private String attendantUid ;

    private String gateway ;

    @OneToOne
    private DjpUniqueTokenModel token ;

    private String wifiPassword ;

    private String formatCode ;

    private String numberOfLegsEncoded ;

    private String passengerName ;

    private String electronicTicketIndicator ;

    private String operatingCarrierPnrCode ;

    private String fromCityAirportCode ;

    private String toCityAirportCode ;

    private String operatingCarrierDesignator ;

    private String flightNumber ;

    private Date dateOfFlight ;

    private String compartmentCode ;

    private String seatNumber ;

    private String checkInSequenceNumber ;

    private String passengerStatus ;

    private Boolean couldNotReadBoardingPass ;

    private String contractID ;

    private Boolean newSale ;

    private String sapOrderID ;

    private String personalID ;

    private String personalName ;

    private Boolean isCancelled ;

    @OneToOne
    private DjpPassLocationsModel whichLocation ;

    private QrType boardingUsage ;

    private PassengerType passengerType ;

    @OneToOne
    private DjpMiddleWareBoardingPassStatusModel middleWareDetail ;

    private UsageMethod usageMethod ;

    private String productCode ;

    private String cancellationUserID ;

    private String cancellationReason ;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "DjpBoardingPass_campaignName", joinColumns = @JoinColumn(name = "DjpBoardingPass_id"))
    @Column(name = "campaignName")
    private List<String> campaignName ;

    @OneToOne
    private DjpBoardingPassUsingInfoModel boardingPassUsingData ;

    private Date serviceOutDate ;

    @ManyToMany(mappedBy = "boardingPasses")
    private List<DjpGuestModel> guests;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "Board2BoardRelation",
            joinColumns = {@JoinColumn(name = "djpBoardingPass")},
            inverseJoinColumns = {@JoinColumn(name = "superBoarding")})
    private List<DjpBoardingPassModel> superBoarding ;

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "Board2DjpBoardRelation",
            joinColumns = {@JoinColumn(name = "djpBoardingPass")},
            inverseJoinColumns = {@JoinColumn(name = "2djpBoardingPass")})
    private List<DjpBoardingPassModel> djpBoardingPass ;
    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(
            name = "Usages2BoardingRel",
            joinColumns = {@JoinColumn(name = "djpBoardingPass")},
            inverseJoinColumns = {@JoinColumn(name = "voucherUsages")})
    private List<DjpVoucherCodeUsagesModel> voucherUsages ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getAttendantUid() {
        return attendantUid;
    }

    public void setAttendantUid(String attendantUid) {
        this.attendantUid = attendantUid;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public DjpUniqueTokenModel getToken() {
        return token;
    }

    public void setToken(DjpUniqueTokenModel token) {
        this.token = token;
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public String getFormatCode() {
        return formatCode;
    }

    public void setFormatCode(String formatCode) {
        this.formatCode = formatCode;
    }

    public String getNumberOfLegsEncoded() {
        return numberOfLegsEncoded;
    }

    public void setNumberOfLegsEncoded(String numberOfLegsEncoded) {
        this.numberOfLegsEncoded = numberOfLegsEncoded;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getElectronicTicketIndicator() {
        return electronicTicketIndicator;
    }

    public void setElectronicTicketIndicator(String electronicTicketIndicator) {
        this.electronicTicketIndicator = electronicTicketIndicator;
    }

    public String getOperatingCarrierPnrCode() {
        return operatingCarrierPnrCode;
    }

    public void setOperatingCarrierPnrCode(String operatingCarrierPnrCode) {
        this.operatingCarrierPnrCode = operatingCarrierPnrCode;
    }

    public String getFromCityAirportCode() {
        return fromCityAirportCode;
    }

    public void setFromCityAirportCode(String fromCityAirportCode) {
        this.fromCityAirportCode = fromCityAirportCode;
    }

    public String getToCityAirportCode() {
        return toCityAirportCode;
    }

    public void setToCityAirportCode(String toCityAirportCode) {
        this.toCityAirportCode = toCityAirportCode;
    }

    public String getOperatingCarrierDesignator() {
        return operatingCarrierDesignator;
    }

    public void setOperatingCarrierDesignator(String operatingCarrierDesignator) {
        this.operatingCarrierDesignator = operatingCarrierDesignator;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDateOfFlight() {
        return dateOfFlight;
    }

    public void setDateOfFlight(Date dateOfFlight) {
        this.dateOfFlight = dateOfFlight;
    }

    public String getCompartmentCode() {
        return compartmentCode;
    }

    public void setCompartmentCode(String compartmentCode) {
        this.compartmentCode = compartmentCode;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getCheckInSequenceNumber() {
        return checkInSequenceNumber;
    }

    public void setCheckInSequenceNumber(String checkInSequenceNumber) {
        this.checkInSequenceNumber = checkInSequenceNumber;
    }

    public String getPassengerStatus() {
        return passengerStatus;
    }

    public void setPassengerStatus(String passengerStatus) {
        this.passengerStatus = passengerStatus;
    }

    public Boolean getCouldNotReadBoardingPass() {
        return couldNotReadBoardingPass;
    }

    public void setCouldNotReadBoardingPass(Boolean couldNotReadBoardingPass) {
        this.couldNotReadBoardingPass = couldNotReadBoardingPass;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public Boolean getNewSale() {
        return newSale;
    }

    public void setNewSale(Boolean newSale) {
        this.newSale = newSale;
    }

    public String getSapOrderID() {
        return sapOrderID;
    }

    public void setSapOrderID(String sapOrderID) {
        this.sapOrderID = sapOrderID;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public Boolean getCancelled() {
        return isCancelled;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public QrType getBoardingUsage() {
        return boardingUsage;
    }

    public void setBoardingUsage(QrType boardingUsage) {
        this.boardingUsage = boardingUsage;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public DjpMiddleWareBoardingPassStatusModel getMiddleWareDetail() {
        return middleWareDetail;
    }

    public void setMiddleWareDetail(DjpMiddleWareBoardingPassStatusModel middleWareDetail) {
        this.middleWareDetail = middleWareDetail;
    }

    public UsageMethod getUsageMethod() {
        return usageMethod;
    }

    public void setUsageMethod(UsageMethod usageMethod) {
        this.usageMethod = usageMethod;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCancellationUserID() {
        return cancellationUserID;
    }

    public void setCancellationUserID(String cancellationUserID) {
        this.cancellationUserID = cancellationUserID;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public List<String> getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(List<String> campaignName) {
        this.campaignName = campaignName;
    }

    public DjpBoardingPassUsingInfoModel getBoardingPassUsingData() {
        return boardingPassUsingData;
    }

    public void setBoardingPassUsingData(DjpBoardingPassUsingInfoModel boardingPassUsingData) {
        this.boardingPassUsingData = boardingPassUsingData;
    }

    public Date getServiceOutDate() {
        return serviceOutDate;
    }

    public void setServiceOutDate(Date serviceOutDate) {
        this.serviceOutDate = serviceOutDate;
    }

    public List<DjpGuestModel> getGuests() {
        return guests;
    }

    public void setGuests(List<DjpGuestModel> guests) {
        this.guests = guests;
    }

    public List<DjpBoardingPassModel> getSuperBoarding() {
        return superBoarding;
    }

    public void setSuperBoarding(List<DjpBoardingPassModel> superBoarding) {
        this.superBoarding = superBoarding;
    }

    public List<DjpVoucherCodeUsagesModel> getVoucherUsages() {
        return voucherUsages;
    }

    public void setVoucherUsages(List<DjpVoucherCodeUsagesModel> voucherUsages) {
        this.voucherUsages = voucherUsages;
    }

    public List<DjpBoardingPassModel> getDjpBoardingPass() {
        return djpBoardingPass;
    }

    public void setDjpBoardingPass(List<DjpBoardingPassModel> djpBoardingPass) {
        this.djpBoardingPass = djpBoardingPass;
    }
}
