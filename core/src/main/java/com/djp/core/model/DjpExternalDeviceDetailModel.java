package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpExternalDeviceDetail")
public class DjpExternalDeviceDetailModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;
    private String locationDetail;
    private String salesPartnerNumber;

    @OneToOne
    private DjpExternalCompanyModel company;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocationDetail() {
        return locationDetail;
    }

    public void setLocationDetail(String locationDetail) {
        this.locationDetail = locationDetail;
    }

    public String getSalesPartnerNumber() {
        return salesPartnerNumber;
    }

    public void setSalesPartnerNumber(String salesPartnerNumber) {
        this.salesPartnerNumber = salesPartnerNumber;
    }

    public DjpExternalCompanyModel getCompany() {
        return company;
    }

    public void setCompany(DjpExternalCompanyModel company) {
        this.company = company;
    }
}
