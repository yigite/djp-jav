package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="CMSCatalogRestriction")
@PrimaryKeyJoinColumn(name="id")
public class CMSCatalogRestrictionModel extends AbstractRestrictionModel{

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="Catalogs4Restriction",
            joinColumns={@JoinColumn(name="restriction")},
            inverseJoinColumns={@JoinColumn(name="catalog")})
    private Set<CatalogModel> catalogs;

    public CMSCatalogRestrictionModel() {
        super();
    }

    public Set<CatalogModel> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(Set<CatalogModel> catalogs) {
        this.catalogs = catalogs;
    }
}
