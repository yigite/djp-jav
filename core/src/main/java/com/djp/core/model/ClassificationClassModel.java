package com.djp.core.model;

import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;

@Entity
@Table(name="ClassificationClass")
@PrimaryKeyJoinColumn(name = "id")
public class ClassificationClassModel extends CategoryModel{

    public ClassificationClassModel() {
        super();
    }
}
