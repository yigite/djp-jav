package com.djp.core.model;

import com.djp.core.enums.ParkingProcessEnum;
import com.djp.core.enums.ParkingValidationTypeEnum;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ParkingWValidation")
public class ParkingWValidationModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String validatorId;
    private BigDecimal value;

    private ParkingProcessEnum validationIdent;
    private ParkingValidationTypeEnum validationType;

    @ManyToOne
    @JoinColumn
    private ParkingWTransactionModel parkingWTransaction;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidatorId() {
        return validatorId;
    }

    public void setValidatorId(String validatorId) {
        this.validatorId = validatorId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public ParkingProcessEnum getValidationIdent() {
        return validationIdent;
    }

    public void setValidationIdent(ParkingProcessEnum validationIdent) {
        this.validationIdent = validationIdent;
    }

    public ParkingValidationTypeEnum getValidationType() {
        return validationType;
    }

    public void setValidationType(ParkingValidationTypeEnum validationType) {
        this.validationType = validationType;
    }

    public ParkingWTransactionModel getParkingWTransaction() {
        return parkingWTransaction;
    }

    public void setParkingWTransaction(ParkingWTransactionModel parkingWTransaction) {
        this.parkingWTransaction = parkingWTransaction;
    }
}
