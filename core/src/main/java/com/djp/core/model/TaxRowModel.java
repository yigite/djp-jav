package com.djp.core.model;

import com.djp.core.enums.DjpEnumValue;
import com.djp.core.enums.ProductTaxGroup;
import jakarta.persistence.*;

@Entity
@Table(name="TaxRow")
@PrimaryKeyJoinColumn(name = "id")
public class TaxRowModel extends PDTRowModel{

    private boolean absolute;
    private double value;

    @OneToOne
    private TaxModel tax;
    @ManyToOne
    @JoinColumn
    private ProductModel product;

    @OneToOne
    private CurrencyModel currency;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    public TaxRowModel() {
        super();
    }

    @Override
    public void setPg(final DjpEnumValue value)
    {
        if( value == null || value instanceof ProductTaxGroup)
        {
            super.setPg(value);
        }
        else
        {
            throw new IllegalArgumentException("Given value is not instance of ProductTaxGroup");
        }
    }

    public boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public TaxModel getTax() {
        return tax;
    }

    public void setTax(TaxModel tax) {
        this.tax = tax;
    }

    @Override
    public ProductModel getProduct() {
        return product;
    }

    @Override
    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }
}
