package com.djp.core.model;

import com.djp.core.enums.PeriodOfValidEnum;
import com.djp.core.enums.PremiumPackagePassengerEnum;

import jakarta.persistence.*;

@Table
@Entity(name = "ProductBomComponent")
public class ProductBomComponentModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;

    @ManyToOne
    private ProductModel product;
    private Integer quantitiy;
    private Integer guestQuantity;
    @OneToOne
    private MediaModel icon;
    private String description;
    private String detailDescription;
    private Integer priorityForSort;
    private Integer nDaysPeriodDayCount;
    private PremiumPackagePassengerEnum passengerType;
    private PeriodOfValidEnum periodOfValid;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getQuantitiy() {
        return quantitiy;
    }

    public void setQuantitiy(Integer quantitiy) {
        this.quantitiy = quantitiy;
    }

    public Integer getGuestQuantity() {
        return guestQuantity;
    }

    public void setGuestQuantity(Integer guestQuantity) {
        this.guestQuantity = guestQuantity;
    }

    public MediaModel getIcon() {
        return icon;
    }

    public void setIcon(MediaModel icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public Integer getPriorityForSort() {
        return priorityForSort;
    }

    public void setPriorityForSort(Integer priorityForSort) {
        this.priorityForSort = priorityForSort;
    }

    public Integer getnDaysPeriodDayCount() {
        return nDaysPeriodDayCount;
    }

    public void setnDaysPeriodDayCount(Integer nDaysPeriodDayCount) {
        this.nDaysPeriodDayCount = nDaysPeriodDayCount;
    }

    public PremiumPackagePassengerEnum getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PremiumPackagePassengerEnum passengerType) {
        this.passengerType = passengerType;
    }

    public PeriodOfValidEnum getPeriodOfValid() {
        return periodOfValid;
    }

    public void setPeriodOfValid(PeriodOfValidEnum periodOfValid) {
        this.periodOfValid = periodOfValid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }
}
