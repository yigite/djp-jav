package com.djp.core.model;

import com.djp.core.enums.BundleRuleTypeEnum;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name="AbstractBundleRule", indexes = {
        @Index(columnList = "catalogVersion", name = "versionIdx"),
        @Index(columnList = "id, catalogVersion", name = "idVersionIdx")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class AbstractBundleRuleModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @Column(unique = true)
    private String id;

    private String name;

    private BundleRuleTypeEnum ruleType;

    @OneToOne
    @JoinColumn(name = "catalogVersion")
    private CatalogVersionModel catalogVersion;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name = "BundleRule2CondProdRel",
            joinColumns = {@JoinColumn(name = "conditionalBundleRule")},
            inverseJoinColumns = {@JoinColumn(name = "conditionalProduct")})
    private Set<ProductModel> conditionalProducts;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name = "BundleRule2TargProdRel",
            joinColumns = {@JoinColumn(name = "targetBundleRule")},
            inverseJoinColumns = {@JoinColumn(name = "targetProduct")})
    private Set<ProductModel> targetProducts;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BundleRuleTypeEnum getRuleType() {
        return ruleType;
    }

    public void setRuleType(BundleRuleTypeEnum ruleType) {
        this.ruleType = ruleType;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public Set<ProductModel> getConditionalProducts() {
        return conditionalProducts;
    }

    public void setConditionalProducts(Set<ProductModel> conditionalProducts) {
        this.conditionalProducts = conditionalProducts;
    }

    public Set<ProductModel> getTargetProducts() {
        return targetProducts;
    }

    public void setTargetProducts(Set<ProductModel> targetProducts) {
        this.targetProducts = targetProducts;
    }
}
