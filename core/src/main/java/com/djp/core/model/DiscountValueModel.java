package com.djp.core.model;

import com.djp.modelservice.util.DjpMathUtil;
import org.apache.commons.lang.StringUtils;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name = "DiscountValue")
public class DiscountValueModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private final String code;

    private double value;
    private double appliedValue;
    private boolean absolute;
    private String isoCode;
    private boolean asTargetPrice;

    @ManyToOne
    @JoinColumn
    private AbstractOrderModel order;

    @Transient
    private static final String DV_HEADER = "<DV<";
    @Transient
    private static final String DV_FOOTER = ">VD>";
    @Transient
    private static final String INTERNAL_DELIMITER = "#";
    @Transient
    private static final String DELIMITER = "|";
    @Transient
    private static final String EMPTY = "[]";

    public static DiscountValueModel createRelative(String code, Double doubleValue) {
        return new DiscountValueModel(code, doubleValue, false, (String)null);
    }

    public static DiscountValueModel createAbsolute(String code, Double doubleValue, String currencyIsoCode) {
        return new DiscountValueModel(code, doubleValue, true, currencyIsoCode);
    }

    public static DiscountValueModel createTargetPrice(String code, Double doubleValue, String currencyIsoCode) {
        return new DiscountValueModel(code, doubleValue, currencyIsoCode, true);
    }

    public DiscountValueModel apply(double quantity, double price, int digits, String currencyIsoCode) {
        if (this.isAbsolute() && currencyIsoCode != this.getCurrencyIsoCode() && (currencyIsoCode == null || !currencyIsoCode.equals(this.getCurrencyIsoCode()))) {
            throw new IllegalArgumentException("cannot apply price " + price + " with currency " + currencyIsoCode + " to absolute discount with currency " + this.getCurrencyIsoCode());
        } else if (this.isAbsolute()) {
            return this.isAsTargetPrice() ? this.createTargetPriceAppliedValue(quantity, price, digits, currencyIsoCode) : this.createAbsoluteAppliedValue(quantity, digits, currencyIsoCode);
        } else {
            return this.createRelativeAppliedValue(price, digits, currencyIsoCode);
        }
    }

    protected DiscountValueModel createRelativeAppliedValue(double price, int digits, String currencyIsoCode) {
        //Hybris CoreAlgoritms Origin
        //return new DiscountValueModel(this.getCode(), this.getValue(), false, CoreAlgorithms.round(price * this.getValue() / 100.0D, digits > 0 ? digits : 0), currencyIsoCode);
        return new DiscountValueModel(this.getCode(), this.getValue(), false, DjpMathUtil.round(price * this.getValue() / 100.0D, digits > 0 ? digits : 0), currencyIsoCode);
    }

    protected DiscountValueModel createAbsoluteAppliedValue(double quantity, int digits, String currencyIsoCode) {
        //return new DiscountValue(this.getCode(), this.getValue(), true, CoreAlgorithms.round(this.getValue() * quantity, digits > 0 ? digits : 0), currencyIsoCode);
        return new DiscountValueModel(this.getCode(), this.getValue(), true, DjpMathUtil.round(this.getValue() * quantity, digits > 0 ? digits : 0), currencyIsoCode);
    }

    protected DiscountValueModel createTargetPriceAppliedValue(double quantity, double totalPriceWithoutDiscounts, int digits, String currencyIsoCode) {
        double targetPricePerPiece = this.getValue();
        double totalTargetPrice = targetPricePerPiece * quantity;
        double differenceToTargetPrice = totalPriceWithoutDiscounts - totalTargetPrice;
        //return new DiscountValue(this.getCode(), this.getValue(), true, CoreAlgorithms.round(differenceToTargetPrice, digits > 0 ? digits : 0), currencyIsoCode, true);
        return new DiscountValueModel(this.getCode(), this.getValue(), true, DjpMathUtil.round(differenceToTargetPrice, digits > 0 ? digits : 0), currencyIsoCode, true);
    }

    public static List apply(double quantity, double startPrice, int digits, List values, String currencyIsoCode) {
        List ret = new ArrayList(values.size());
        double tmp = startPrice;
        Iterator it = values.iterator();

        while(it.hasNext()) {
            DiscountValueModel discountValue = ((DiscountValueModel)it.next()).apply(quantity, tmp, digits, currencyIsoCode);
            tmp -= discountValue.getAppliedValue();
            ret.add(discountValue);
        }

        return ret;
    }

    public static double sumAppliedValues(Collection values) {
        if (values != null && !values.isEmpty()) {
            double sum = 0.0D;

            for(Iterator it = values.iterator(); it.hasNext(); sum += ((DiscountValueModel)it.next()).getAppliedValue()) {
            }

            return sum;
        } else {
            return 0.0D;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public String getCurrencyIsoCode() {
        return this.isoCode;
    }

    public double getValue() {
        return this.value;
    }

    public boolean isAsTargetPrice() {
        return this.asTargetPrice;
    }

    public double getAppliedValue() {
        return this.appliedValue;
    }

    public boolean isAbsolute() {
        return this.absolute;
    }

    public DiscountValueModel(String code, double value, boolean absolute, String currencyIsoCode) {
        this(code, value, absolute, 0.0D, currencyIsoCode);
    }

    public DiscountValueModel(String code, double value, String currencyIsoCode, boolean asTargetPrice) {
        this(code, value, true, 0.0D, currencyIsoCode, asTargetPrice);
    }

    public DiscountValueModel(String code, double value, boolean absolute, double appliedValue, String isoCode) {
        this(code, value, absolute, appliedValue, isoCode, false);
    }

    public DiscountValueModel(String code, double value, boolean absolute, double appliedValue, String isoCode, boolean asTargetPrice) {
        if (code == null) {
            throw new IllegalArgumentException("discount value code may not be null");
        } else if (asTargetPrice && StringUtils.isBlank(isoCode)) {
            throw new IllegalArgumentException("discount value cannot be target price without currency iso code");
        } else {
            this.code = code;
            this.value = value;
            this.absolute = absolute;
            this.appliedValue = appliedValue;
            this.isoCode = isoCode;
            this.asTargetPrice = asTargetPrice;
        }
    }

    public Object clone() {
        return new DiscountValueModel(this.getCode(), this.getValue(), this.isAbsolute(), this.getAppliedValue(), this.getCurrencyIsoCode());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("<DV<");
        sb.append(this.getCode());
        sb.append("#").append(this.getValue());
        sb.append("#").append(this.isAbsolute());
        sb.append("#").append(this.getAppliedValue());
        sb.append("#").append(this.getCurrencyIsoCode() != null ? this.getCurrencyIsoCode() : "NULL");
        sb.append("#").append(this.isAsTargetPrice());
        sb.append(">VD>");
        return sb.toString();
    }

    public static String toString(Collection discountValueCollection) {
        if (discountValueCollection == null) {
            return null;
        } else if (discountValueCollection.isEmpty()) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Iterator it = discountValueCollection.iterator();

            while(it.hasNext()) {
                stringBuilder.append(((DiscountValueModel)it.next()).toString());
                if (it.hasNext()) {
                    stringBuilder.append("|");
                }
            }

            stringBuilder.append("]");
            return stringBuilder.toString();
        }
    }

    public static Collection parseDiscountValueModelCollection(String str) throws IllegalArgumentException {
        if (str == null) {
            return null;
        } else if (str.equals("[]")) {
            return new LinkedList();
        } else {
            Collection ret = new LinkedList();
            StringTokenizer st = new StringTokenizer(str.substring(1, str.length() - 1), "|");

            while(st.hasMoreTokens()) {
                ret.add(parseDiscountValueModel(st.nextToken()));
            }

            return ret;
        }
    }

    public static DiscountValueModel parseDiscountValueModel(String str) throws IllegalArgumentException {
        try {
            int start = str.indexOf("<DV<");
            if (start < 0) {
                throw new IllegalArgumentException("could not find <DV< in discount value string '" + str + "'");
            } else {
                int end = str.indexOf(">VD>", start);
                if (start < 0) {
                    throw new IllegalArgumentException("could not find >VD> in discount value string '" + str + "'");
                } else {
                    int pos = 0;
                    String code = null;
                    double value = 0.0D;
                    boolean absolute = false;
                    boolean asTargetPrice = false;
                    double appliedValue = 0.0D;
                    String iso = null;
                    String substr = str.substring(start + "<DV<".length(), end);
                    if (substr.startsWith("#")) {
                        pos = 1;
                        code = "";
                    }

                    for(StringTokenizer st = new StringTokenizer(substr, "#"); st.hasMoreTokens(); ++pos) {
                        String token = st.nextToken();
                        switch(pos) {
                            case 0:
                                code = token;
                                break;
                            case 1:
                                value = Double.parseDouble(token);
                                break;
                            case 2:
                                absolute = Boolean.parseBoolean(token);
                                break;
                            case 3:
                                appliedValue = Double.parseDouble(token);
                                break;
                            case 4:
                                iso = token;
                                break;
                            case 5:
                                asTargetPrice = Boolean.parseBoolean(token);
                                break;
                            default:
                                throw new IllegalArgumentException("illegal discount value string '" + str + "' (pos=" + pos + ", moreTokens=" + st.hasMoreTokens() + ", nextToken='" + token + "')");
                        }
                    }

                    if (pos < 3) {
                        throw new IllegalArgumentException("illegal discount value string '" + str + "' (pos=" + pos + ")");
                    } else {
                        return new DiscountValueModel(code, value, absolute, appliedValue, "NULL".equals(iso) ? null : iso, asTargetPrice);
                    }
                }
            }
        } catch (Exception var15) {
            throw new IllegalArgumentException("error parsing discount value string '" + str + "' : " + var15);
        }
    }

    public int hashCode() {
        return this.isAbsolute() ? this.getCode().hashCode() ^ (this.isAbsolute() ? 1 : 0) ^ (int)this.getValue() ^ (this.getCurrencyIsoCode() == null ? 0 : this.getCurrencyIsoCode().hashCode()) : this.getCode().hashCode() ^ (this.isAbsolute() ? 1 : 0) ^ (int)this.getValue();
    }

    public boolean equals(Object object) {
        String iso = this.getCurrencyIsoCode();
        return object instanceof DiscountValueModel && this.getCode().equals(((DiscountValueModel)object).getCode()) && this.absolute == ((DiscountValueModel)object).isAbsolute() && (!this.absolute || iso == null && ((DiscountValueModel)object).getCurrencyIsoCode() == null || iso.equals(((DiscountValueModel)object).getCurrencyIsoCode())) && this.value == ((DiscountValueModel)object).getValue() && this.appliedValue == ((DiscountValueModel)object).getAppliedValue();
    }

    public boolean equalsIgnoreAppliedValue(DiscountValueModel discountValue) {
        String iso = this.getCurrencyIsoCode();
        return this.getCode().equals(discountValue.getCode()) && this.absolute == discountValue.isAbsolute() && (!this.absolute || iso == null && discountValue.getCurrencyIsoCode() == null || iso.equals(discountValue.getCurrencyIsoCode())) && this.value == discountValue.getValue();
    }

    public String getIsoCode() {
        return isoCode;
    }


    public AbstractOrderModel getOrder() {
        return order;
    }

    public void setOrder(AbstractOrderModel order) {
        this.order = order;
    }
}
