package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "DjpSleepodPassengerInfo")
public class DjpSleepodPassengerInfoModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date dateForRent;
    private int hourForRent;
    private int rentedCabinsCount;
    private int sleepKitCount;

    @ElementCollection
    @CollectionTable(name="RentedCabins", joinColumns=@JoinColumn(name="djpSleepodPassengerInfo"))
    @Column(name="rentedCabin")
    private Set<String> rentedCabins;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateForRent() {
        return dateForRent;
    }

    public void setDateForRent(Date dateForRent) {
        this.dateForRent = dateForRent;
    }

    public int getHourForRent() {
        return hourForRent;
    }

    public void setHourForRent(int hourForRent) {
        this.hourForRent = hourForRent;
    }

    public int getRentedCabinsCount() {
        return rentedCabinsCount;
    }

    public void setRentedCabinsCount(int rentedCabinsCount) {
        this.rentedCabinsCount = rentedCabinsCount;
    }

    public int getSleepKitCount() {
        return sleepKitCount;
    }

    public void setSleepKitCount(int sleepKitCount) {
        this.sleepKitCount = sleepKitCount;
    }

    public Set<String> getRentedCabins() {
        return rentedCabins;
    }

    public void setRentedCabins(Set<String> rentedCabins) {
        this.rentedCabins = rentedCabins;
    }
}
