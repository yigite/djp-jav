package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="Category")
@Inheritance(strategy = InheritanceType.JOINED)
public class CategoryModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;
    private String name;
    private String description;
    private String seoTitle;
    private String seoDescription;
    private int order;

    @OneToOne
    private CatalogModel catalog;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    @OneToOne
    private MediaModel thumbnail;

    @OneToOne
    private MediaModel picture;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> normal;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> medias;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> thumbnails;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> detail;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> logo;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> dataSheet;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<MediaModel> others;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Cat2ProdRel",
            joinColumns = {@JoinColumn(name="superCategory")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> products;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Cat2CatRel",
            joinColumns = {@JoinColumn(name="superCategory")},
            inverseJoinColumns = {@JoinColumn(name="category")})
    private Set<CategoryModel> categories;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Cat2CatRel",
            joinColumns = {@JoinColumn(name="category")},
            inverseJoinColumns = {@JoinColumn(name="superCategory")})
    private Set<CategoryModel> supercategories;

    @ManyToMany(mappedBy = "categories", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CMSCategoryRestrictionModel> restrictions;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="Cat2AllSubCatCollection",
            joinColumns = {@JoinColumn(name="p_category")},
            inverseJoinColumns = {@JoinColumn(name="sub_category")})
    private Set<CategoryModel> allSubcategories;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="Cat2AllSuperCatCollection",
            joinColumns = {@JoinColumn(name="p_category")},
            inverseJoinColumns = {@JoinColumn(name="super_category")})
    private Set<CategoryModel> allSupercategories;

    @ManyToMany(mappedBy = "categories", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<KeywordModel> keywords;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="Cat2PrincRel",
            joinColumns = {@JoinColumn(name="category")},
            inverseJoinColumns = {@JoinColumn(name="principal")})
    private Set<PrincipalModel> allowedPrincipals;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public CatalogModel getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogModel catalog) {
        this.catalog = catalog;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public MediaModel getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(MediaModel thumbnail) {
        this.thumbnail = thumbnail;
    }

    public MediaModel getPicture() {
        return picture;
    }

    public void setPicture(MediaModel picture) {
        this.picture = picture;
    }

    public Set<MediaModel> getNormal() {
        return normal;
    }

    public void setNormal(Set<MediaModel> normal) {
        this.normal = normal;
    }

    public Set<MediaModel> getMedias() {
        return medias;
    }

    public void setMedias(Set<MediaModel> medias) {
        this.medias = medias;
    }

    public Set<MediaModel> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Set<MediaModel> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public Set<MediaModel> getDetail() {
        return detail;
    }

    public void setDetail(Set<MediaModel> detail) {
        this.detail = detail;
    }

    public Set<MediaModel> getLogo() {
        return logo;
    }

    public void setLogo(Set<MediaModel> logo) {
        this.logo = logo;
    }

    public Set<MediaModel> getDataSheet() {
        return dataSheet;
    }

    public void setDataSheet(Set<MediaModel> dataSheet) {
        this.dataSheet = dataSheet;
    }

    public Set<MediaModel> getOthers() {
        return others;
    }

    public void setOthers(Set<MediaModel> others) {
        this.others = others;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public Set<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryModel> categories) {
        this.categories = categories;
    }

    public Set<CategoryModel> getSupercategories() {
        return supercategories;
    }

    public void setSupercategories(Set<CategoryModel> supercategories) {
        this.supercategories = supercategories;
    }

    public Set<CMSCategoryRestrictionModel> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(Set<CMSCategoryRestrictionModel> restrictions) {
        this.restrictions = restrictions;
    }

    public Set<CategoryModel> getAllSubcategories() {
        return allSubcategories;
    }

    public void setAllSubcategories(Set<CategoryModel> allSubcategories) {
        this.allSubcategories = allSubcategories;
    }

    public Set<CategoryModel> getAllSupercategories() {
        return allSupercategories;
    }

    public void setAllSupercategories(Set<CategoryModel> allSupercategories) {
        this.allSupercategories = allSupercategories;
    }

    public Set<KeywordModel> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<KeywordModel> keywords) {
        this.keywords = keywords;
    }

    public Set<PrincipalModel> getAllowedPrincipals() {
        return allowedPrincipals;
    }

    public void setAllowedPrincipals(Set<PrincipalModel> allowedPrincipals) {
        this.allowedPrincipals = allowedPrincipals;
    }
}
