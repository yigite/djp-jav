package com.djp.core.model;

import com.djp.core.enums.CustomerType;
import com.djp.core.enums.GenderEnum;
import com.djp.core.enums.MaritalStatusEnum;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Customer")
@PrimaryKeyJoinColumn(name = "uid")
public class CustomerModel extends UserModel {
    @Column(unique = true)
    private String customerId;

    private String originalUid;
    private String firstName;
    private String lastName;
    private String contactEmail;
    private CustomerType type;
    private Date birthDate;
    private String mail;
    private GenderEnum gender;
    @OneToOne
    private CountryModel country;
    private String registerToken;
    private String notificationId;
    private String passportNumber;
    private String smsOtp;
    private String partnerType;
    private String partnerFirm;
    private Integer childCount;
    private Boolean isMobile;
    private Boolean callPreference;
    private Boolean lastResetPwdDate;
    private Boolean isPersonnel;
    private Boolean whatsappPreference;
    private Boolean haveDjpPass;
    private Boolean isPartner;
    private Boolean isMarketingUpdate;
    private Boolean firstNameChanged;
    private Boolean lastNameChanged;
    private Boolean emailPreference;
    private Boolean smsPreference;
    private Date lockUntilTime;
    private Date DeactivationDate;
    private MaritalStatusEnum maritalStatus;

    @OneToOne(orphanRemoval = true)
    private DjpCustomerPreferenceChangeDetailModel smsPreferenceChangedDetail;

    @OneToOne(orphanRemoval = true)
    private DjpCustomerPreferenceChangeDetailModel mailPreferenceChangedDetail;

    @OneToOne(orphanRemoval = true)
    private DjpCustomerPreferenceChangeDetailModel callPreferenceChangedDetail;

    @OneToOne(orphanRemoval = true)
    private DjpCustomerPreferenceChangeDetailModel whatsappPreferenceChangedDetail;

    @OneToOne(orphanRemoval = true)
    private PaymentInfoModel defaultPaymentInfo;

    @OneToOne(orphanRemoval = true)
    private TitleModel title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private NationalityModel nationality;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<NumberPlateModel> numberPlates;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<PlateMovementWsModel> plateMovementWsModels;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public PaymentInfoModel getDefaultPaymentInfo() {
        return defaultPaymentInfo;
    }

    public void setDefaultPaymentInfo(PaymentInfoModel defaultPaymentInfo) {
        this.defaultPaymentInfo = defaultPaymentInfo;
    }

    public Boolean getPersonnel() {
        return isPersonnel;
    }

    public void setPersonnel(Boolean personnel) {
        isPersonnel = personnel;
    }

    public TitleModel getTitle() {
        return title;
    }

    public void setTitle(TitleModel title) {
        this.title = title;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getOriginalUid() {
        return originalUid;
    }

    public void setOriginalUid(String originalUid) {
        this.originalUid = originalUid;
    }

    public CustomerType getType() {
        return type;
    }

    public void setType(CustomerType type) {
        this.type = type;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }

    public String getRegisterToken() {
        return registerToken;
    }

    public void setRegisterToken(String registerToken) {
        this.registerToken = registerToken;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public Boolean getMobile() {
        return isMobile;
    }

    public void setMobile(Boolean mobile) {
        isMobile = mobile;
    }

    public Boolean getCallPreference() {
        return callPreference;
    }

    public void setCallPreference(Boolean callPreference) {
        this.callPreference = callPreference;
    }

    public Boolean getLastResetPwdDate() {
        return lastResetPwdDate;
    }

    public void setLastResetPwdDate(Boolean lastResetPwdDate) {
        this.lastResetPwdDate = lastResetPwdDate;
    }

    public Boolean getIsPersonnel() {
        return isPersonnel;
    }

    public void setIsPersonnel(Boolean personnel) {
        isPersonnel = personnel;
    }

    public Boolean getWhatsappPreference() {
        return whatsappPreference;
    }

    public void setWhatsappPreference(Boolean whatsappPreference) {
        this.whatsappPreference = whatsappPreference;
    }

    public Boolean getHaveDjpPass() {
        return haveDjpPass;
    }

    public void setHaveDjpPass(Boolean haveDjpPass) {
        this.haveDjpPass = haveDjpPass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public MaritalStatusEnum getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatusEnum maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public DjpCustomerPreferenceChangeDetailModel getSmsPreferenceChangedDetail() {
        return smsPreferenceChangedDetail;
    }

    public void setSmsPreferenceChangedDetail(DjpCustomerPreferenceChangeDetailModel smsPreferenceChangedDetail) {
        this.smsPreferenceChangedDetail = smsPreferenceChangedDetail;
    }

    public DjpCustomerPreferenceChangeDetailModel getMailPreferenceChangedDetail() {
        return mailPreferenceChangedDetail;
    }

    public void setMailPreferenceChangedDetail(DjpCustomerPreferenceChangeDetailModel mailPreferenceChangedDetail) {
        this.mailPreferenceChangedDetail = mailPreferenceChangedDetail;
    }

    public DjpCustomerPreferenceChangeDetailModel getCallPreferenceChangedDetail() {
        return callPreferenceChangedDetail;
    }

    public void setCallPreferenceChangedDetail(DjpCustomerPreferenceChangeDetailModel callPreferenceChangedDetail) {
        this.callPreferenceChangedDetail = callPreferenceChangedDetail;
    }

    public DjpCustomerPreferenceChangeDetailModel getWhatsappPreferenceChangedDetail() {
        return whatsappPreferenceChangedDetail;
    }

    public void setWhatsappPreferenceChangedDetail(DjpCustomerPreferenceChangeDetailModel whatsappPreferenceChangedDetail) {
        this.whatsappPreferenceChangedDetail = whatsappPreferenceChangedDetail;
    }

    public String getSmsOtp() {
        return smsOtp;
    }

    public void setSmsOtp(String smsOtp) {
        this.smsOtp = smsOtp;
    }

    public Boolean getPartner() {
        return isPartner;
    }

    public void setPartner(Boolean partner) {
        isPartner = partner;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getPartnerFirm() {
        return partnerFirm;
    }

    public void setPartnerFirm(String partnerFirm) {
        this.partnerFirm = partnerFirm;
    }

    public Boolean getMarketingUpdate() {
        return isMarketingUpdate;
    }

    public void setMarketingUpdate(Boolean marketingUpdate) {
        isMarketingUpdate = marketingUpdate;
    }

    public Date getLockUntilTime() {
        return lockUntilTime;
    }

    public void setLockUntilTime(Date lockUntilTime) {
        this.lockUntilTime = lockUntilTime;
    }

    public Boolean getFirstNameChanged() {
        return firstNameChanged;
    }

    public void setFirstNameChanged(Boolean firstNameChanged) {
        this.firstNameChanged = firstNameChanged;
    }

    public Boolean getLastNameChanged() {
        return lastNameChanged;
    }

    public void setLastNameChanged(Boolean lastNameChanged) {
        this.lastNameChanged = lastNameChanged;
    }

    public Boolean getEmailPreference() {
        return emailPreference;
    }

    public void setEmailPreference(Boolean emailPreference) {
        this.emailPreference = emailPreference;
    }

    public Boolean getSmsPreference() {
        return smsPreference;
    }

    public void setSmsPreference(Boolean smsPreference) {
        this.smsPreference = smsPreference;
    }

    public Date getDeactivationDate() {
        return DeactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        DeactivationDate = deactivationDate;
    }


    public NationalityModel getNationality() {
        return nationality;
    }

    public void setNationality(NationalityModel nationality) {
        this.nationality = nationality;
    }

    public Set<NumberPlateModel> getNumberPlates() {
        return numberPlates;
    }

    public void setNumberPlates(Set<NumberPlateModel> numberPlates) {
        this.numberPlates = numberPlates;
    }

    public Set<PlateMovementWsModel> getPlateMovementWsModels() {
        return plateMovementWsModels;
    }

    public void setPlateMovementWsModels(Set<PlateMovementWsModel> plateMovementWsModels) {
        this.plateMovementWsModels = plateMovementWsModels;
    }
}
