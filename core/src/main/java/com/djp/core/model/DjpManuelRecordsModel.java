package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpManuelRecordsModel")
public class DjpManuelRecordsModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String clientName;
    private String inputMethod;
    private Boolean isManuelPnr;
    private String name;
    private Boolean newSale;
    private String note;
    private String personalName;
    private String phoneNumber;
    private String pnrCode;
    private String programNo;
    private String subMembership;
    private QrType whereUsed;
    private String whoseGuest;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getInputMethod() {
        return inputMethod;
    }

    public void setInputMethod(String inputMethod) {
        this.inputMethod = inputMethod;
    }

    public Boolean getManuelPnr() {
        return isManuelPnr;
    }

    public void setManuelPnr(Boolean manuelPnr) {
        isManuelPnr = manuelPnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getNewSale() {
        return newSale;
    }

    public void setNewSale(Boolean newSale) {
        this.newSale = newSale;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public String getProgramNo() {
        return programNo;
    }

    public void setProgramNo(String programNo) {
        this.programNo = programNo;
    }

    public String getSubMembership() {
        return subMembership;
    }

    public void setSubMembership(String subMembership) {
        this.subMembership = subMembership;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public String getWhoseGuest() {
        return whoseGuest;
    }

    public void setWhoseGuest(String whoseGuest) {
        this.whoseGuest = whoseGuest;
    }
}
