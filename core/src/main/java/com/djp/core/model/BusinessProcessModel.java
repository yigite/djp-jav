package com.djp.core.model;

import com.djp.core.enums.ProcessState;

import jakarta.persistence.*;

@Entity
@Table(name = "BusinessProcess")
@Inheritance(strategy = InheritanceType.JOINED)
public class BusinessProcessModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String code;

    private String processDefinitionName;
    private String processDefinitionVersion;
    private ProcessState state;
    private ProcessState processState;
    private String endMessage;

    @OneToOne
    private UserModel user;

    /*private Set<ProcessTaskModel> currentTasks;
    private String contextParameters;
    private String taskLogs;
    private List<String> emails;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public String getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(String processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public ProcessState getState() {
        return state;
    }

    public void setState(ProcessState state) {
        this.state = state;
    }

    public ProcessState getProcessState() {
        return processState;
    }

    public void setProcessState(ProcessState processState) {
        this.processState = processState;
    }

    public String getEndMessage() {
        return endMessage;
    }

    public void setEndMessage(String endMessage) {
        this.endMessage = endMessage;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
