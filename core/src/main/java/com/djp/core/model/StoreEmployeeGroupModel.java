package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "StoreEmployeeGroup")
@PrimaryKeyJoinColumn(name = "uid")
public class StoreEmployeeGroupModel extends UserGroupModel {

    @ManyToOne
    @JoinColumn
    private PointOfServiceModel store;

    public StoreEmployeeGroupModel() {
        super();
    }

    public PointOfServiceModel getStore() {
        return store;
    }

    public void setStore(PointOfServiceModel store) {
        this.store = store;
    }
}
