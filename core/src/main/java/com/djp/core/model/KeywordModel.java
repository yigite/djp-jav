package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="Keyword")
public class KeywordModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Prod2KeywordRel",
            joinColumns = {@JoinColumn(name="keyword")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> products;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Cat2KeywordRel",
            joinColumns = {@JoinColumn(name="keyword")},
            inverseJoinColumns = {@JoinColumn(name="category")})
    private Set<CategoryModel> categories;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public Set<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryModel> categories) {
        this.categories = categories;
    }
}
