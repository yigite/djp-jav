package com.djp.core.model;

import com.djp.core.enums.UserDiscountGroup;
import com.djp.core.enums.UserPriceGroup;
import com.djp.core.enums.UserTaxGroup;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "UserGroup")
@Inheritance(strategy = InheritanceType.JOINED)
public class UserGroupModel extends PrincipalGroupModel{

    @ManyToOne
    @JoinColumn
    private CampaignSegmentInformationModel campaignSegmentInformation;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="UserGroupWritableLangRel",
            joinColumns={@JoinColumn(name="campaign")},
            inverseJoinColumns={@JoinColumn(name="userGroup")})
    private Set<LanguageModel> writeableLanguages;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="UserGroupReadableLangRel",
            joinColumns={@JoinColumn(name="campaign")},
            inverseJoinColumns={@JoinColumn(name="userGroup")})
    private Set<LanguageModel> readableLanguages;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="DjpCampaign2UserGroupRel",
            joinColumns={@JoinColumn(name="campaign")},
            inverseJoinColumns={@JoinColumn(name="userGroup")})
    private Set<DjpCampaignModel> campaigns;

    private UserDiscountGroup userDiscountGroup;
    private UserPriceGroup userPriceGroup;
    private UserTaxGroup userTaxGroup;

    public UserGroupModel() {
        super();
    }

    public CampaignSegmentInformationModel getCampaignSegmentInformation() {
        return campaignSegmentInformation;
    }

    public void setCampaignSegmentInformation(CampaignSegmentInformationModel campaignSegmentInformation) {
        this.campaignSegmentInformation = campaignSegmentInformation;
    }

    public Set<LanguageModel> getWriteableLanguages() {
        return writeableLanguages;
    }

    public void setWriteableLanguages(Set<LanguageModel> writeableLanguages) {
        this.writeableLanguages = writeableLanguages;
    }

    public Set<LanguageModel> getReadableLanguages() {
        return readableLanguages;
    }

    public void setReadableLanguages(Set<LanguageModel> readableLanguages) {
        this.readableLanguages = readableLanguages;
    }

    public Set<DjpCampaignModel> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(Set<DjpCampaignModel> campaigns) {
        this.campaigns = campaigns;
    }

    public UserDiscountGroup getUserDiscountGroup() {
        return userDiscountGroup;
    }

    public void setUserDiscountGroup(UserDiscountGroup userDiscountGroup) {
        this.userDiscountGroup = userDiscountGroup;
    }

    public UserPriceGroup getUserPriceGroup() {
        return userPriceGroup;
    }

    public void setUserPriceGroup(UserPriceGroup userPriceGroup) {
        this.userPriceGroup = userPriceGroup;
    }

    public UserTaxGroup getUserTaxGroup() {
        return userTaxGroup;
    }

    public void setUserTaxGroup(UserTaxGroup userTaxGroup) {
        this.userTaxGroup = userTaxGroup;
    }
}
