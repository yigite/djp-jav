package com.djp.core.model;

import com.djp.core.enums.QrType;
import com.djp.core.enums.StatusOfSentCompany;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "QrForExternal")
public class QrForExternalModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    private MediaModel qr;
    private String companyClientId;


    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "QrForExternal_QrType_whereIsValidQr", joinColumns = @JoinColumn(name = "QrForExternal_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> whereIsValidQr;

    private Boolean usedForCampaign;

    private QrType whereUsed;

    private Date usageDate;
    private String plateNumber;
    private String carParkingUsedInfo;
    private Boolean usedVale;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    private String contractId;
    private Integer adultCount;
    private Integer childCount;

    private StatusOfSentCompany statusOfSentToCompany;
    private Boolean isSuccesfullySentToSap;

    private Date startDate;
    private Date endDate;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public String getCompanyClientId() {
        return companyClientId;
    }

    public void setCompanyClientId(String companyClientId) {
        this.companyClientId = companyClientId;
    }

    public Set<QrType> getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(Set<QrType> whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCarParkingUsedInfo() {
        return carParkingUsedInfo;
    }

    public void setCarParkingUsedInfo(String carParkingUsedInfo) {
        this.carParkingUsedInfo = carParkingUsedInfo;
    }

    public Boolean getUsedVale() {
        return usedVale;
    }

    public void setUsedVale(Boolean usedVale) {
        this.usedVale = usedVale;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Integer getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(Integer adultCount) {
        this.adultCount = adultCount;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public StatusOfSentCompany getStatusOfSentToCompany() {
        return statusOfSentToCompany;
    }

    public void setStatusOfSentToCompany(StatusOfSentCompany statusOfSentToCompany) {
        this.statusOfSentToCompany = statusOfSentToCompany;
    }

    public Boolean getSuccesfullySentToSap() {
        return isSuccesfullySentToSap;
    }

    public void setSuccesfullySentToSap(Boolean succesfullySentToSap) {
        isSuccesfullySentToSap = succesfullySentToSap;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}