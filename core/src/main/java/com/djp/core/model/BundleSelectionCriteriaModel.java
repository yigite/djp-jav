package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name="BundleSelectionCriteria", indexes = {
        @Index(columnList = "catalogVersion", name = "versionIdx"),
        @Index(columnList = "id, catalogVersion", name = "idVersionIdx")
})
public class BundleSelectionCriteriaModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @Column(unique = true)
    private String id;

    private boolean starter;

    @OneToOne
    @JoinColumn(name = "catalogVersion")
    private CatalogVersionModel catalogVersion;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isStarter() {
        return starter;
    }

    public void setStarter(boolean starter) {
        this.starter = starter;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }
}
