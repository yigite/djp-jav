package com.djp.core.model;

import com.djp.core.enums.GenderEnum;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "DjpOtherCustomer")
public class DjpOtherCustomerModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private GenderEnum gender;

    @OneToOne
    private NationalityModel nationality;

    @ManyToMany(mappedBy = "guestCustomers", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AbstractOrderModel> carts;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="OrderEntry2GuestRel",
            joinColumns = {@JoinColumn(name="guestCustomer")},
            inverseJoinColumns = {@JoinColumn(name = "entry")})
    private Set<AbstractOrderEntryModel> entries;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public NationalityModel getNationality() {
        return nationality;
    }

    public void setNationality(NationalityModel nationality) {
        this.nationality = nationality;
    }

    public Set<AbstractOrderModel> getCarts() {
        return carts;
    }

    public void setCarts(Set<AbstractOrderModel> carts) {
        this.carts = carts;
    }

    public Set<AbstractOrderEntryModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<AbstractOrderEntryModel> entries) {
        this.entries = entries;
    }
}
