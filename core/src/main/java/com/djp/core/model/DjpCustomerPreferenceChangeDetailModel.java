package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpCusPrefChangeDetail")
public class DjpCustomerPreferenceChangeDetailModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private UserModel customer;

    @OneToOne
    private UserModel changedBy;

    private Boolean whichValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserModel getCustomer() {
        return customer;
    }

    public void setCustomer(UserModel customer) {
        this.customer = customer;
    }

    public UserModel getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(UserModel changedBy) {
        this.changedBy = changedBy;
    }

    public Boolean getWhichValue() {
        return whichValue;
    }

    public void setWhichValue(Boolean whichValue) {
        this.whichValue = whichValue;
    }
}
