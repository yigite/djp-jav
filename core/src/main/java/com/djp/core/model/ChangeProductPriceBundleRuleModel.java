package com.djp.core.model;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="ChangeProductPriceBundleRule")
@PrimaryKeyJoinColumn(name = "_id")
public class ChangeProductPriceBundleRuleModel extends AbstractBundleRuleModel {

    private BigDecimal price;

    @OneToOne
    private CurrencyModel currency;

    @ManyToOne
    @JoinColumn
    private BundleTemplateModel bundleTemplate;

    public ChangeProductPriceBundleRuleModel() {
        super();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public BundleTemplateModel getBundleTemplate() {
        return bundleTemplate;
    }

    public void setBundleTemplate(BundleTemplateModel bundleTemplate) {
        this.bundleTemplate = bundleTemplate;
    }
}
