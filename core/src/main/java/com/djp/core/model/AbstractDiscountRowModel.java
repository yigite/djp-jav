package com.djp.core.model;

import com.djp.core.enums.DjpEnumValue;
import com.djp.core.enums.ProductDiscountGroup;
import jakarta.persistence.*;

@Entity
@Table(name="AbstractDiscountRow")
@Inheritance(strategy = InheritanceType.JOINED)
public class AbstractDiscountRowModel extends PDTRowModel{

    private String discountString;
    private boolean absolute;
    private double value;

    @OneToOne
    private DiscountModel discount;
    @OneToOne
    private CurrencyModel currency;

    public AbstractDiscountRowModel() {
        super();
    }

    @Override
    public void setPg(final DjpEnumValue value)
    {
        if( value == null || value instanceof ProductDiscountGroup)
        {
            super.setPg(value);
        }
        else
        {
            throw new IllegalArgumentException("Given value is not instance of ProductDiscountGroup");
        }
    }

    public String getDiscountString() {
        return discountString;
    }

    public void setDiscountString(String discountString) {
        this.discountString = discountString;
    }

    public boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public DiscountModel getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountModel discount) {
        this.discount = discount;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }
}
