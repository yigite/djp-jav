package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Catalog", indexes = @Index(columnList = "id", name = "idIdx"))
public class CatalogModel extends ItemModel {

    //  needs to be checked because in hybris the id is string
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @Column(unique = true)
    private String id;

    private String name;
    private String version;
    private String mimeRootDirectory;
    private String generatorInfo;
    private boolean defaultCatalog;
    private boolean inclFreight;
    private boolean inclPacking;
    private boolean inclAssurance;
    private boolean inclDuty;
    private Date generationDate;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "Catalog_urlPatterns", joinColumns = @JoinColumn(name = "Catalog_id"))
    @Column(name = "urlPattern")
    private Set<String> urlPatterns;

    @ManyToOne
    @Column
    private CompanyModel supplier;

    @ManyToOne
    @Column
    private CompanyModel buyer;

    @ManyToMany(mappedBy = "catalogs", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<BaseStoreModel> baseStores;

    @ManyToMany(mappedBy = "catalogs", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CMSCatalogRestrictionModel> restrictions;

    @OneToOne
    private CatalogVersionModel activeCatalogVersion;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<CategoryModel> rootCategories;

    @OneToOne
    private CurrencyModel defaultCurrency;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<LanguageModel> languages;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<CountryModel> territories;

    @OneToMany(mappedBy = "catalog", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AgreementModel> agreements;

    @OneToMany(mappedBy = "catalog", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CatalogVersionModel> catalogVersions;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMimeRootDirectory() {
        return mimeRootDirectory;
    }

    public void setMimeRootDirectory(String mimeRootDirectory) {
        this.mimeRootDirectory = mimeRootDirectory;
    }

    public String getGeneratorInfo() {
        return generatorInfo;
    }

    public void setGeneratorInfo(String generatorInfo) {
        this.generatorInfo = generatorInfo;
    }

    public boolean isDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(boolean defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public boolean isInclFreight() {
        return inclFreight;
    }

    public void setInclFreight(boolean inclFreight) {
        this.inclFreight = inclFreight;
    }

    public boolean isInclPacking() {
        return inclPacking;
    }

    public void setInclPacking(boolean inclPacking) {
        this.inclPacking = inclPacking;
    }

    public boolean isInclAssurance() {
        return inclAssurance;
    }

    public void setInclAssurance(boolean inclAssurance) {
        this.inclAssurance = inclAssurance;
    }

    public boolean isInclDuty() {
        return inclDuty;
    }

    public void setInclDuty(boolean inclDuty) {
        this.inclDuty = inclDuty;
    }

    public Date getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(Date generationDate) {
        this.generationDate = generationDate;
    }

    public Set<String> getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(Set<String> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public CompanyModel getSupplier() {
        return supplier;
    }

    public void setSupplier(CompanyModel supplier) {
        this.supplier = supplier;
    }

    public CompanyModel getBuyer() {
        return buyer;
    }

    public void setBuyer(CompanyModel buyer) {
        this.buyer = buyer;
    }

    public Set<BaseStoreModel> getBaseStores() {
        return baseStores;
    }

    public void setBaseStores(Set<BaseStoreModel> baseStores) {
        this.baseStores = baseStores;
    }

    public Set<CMSCatalogRestrictionModel> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(Set<CMSCatalogRestrictionModel> restrictions) {
        this.restrictions = restrictions;
    }

    public CatalogVersionModel getActiveCatalogVersion() {
        return activeCatalogVersion;
    }

    public void setActiveCatalogVersion(CatalogVersionModel activeCatalogVersion) {
        this.activeCatalogVersion = activeCatalogVersion;
    }

    public Set<CategoryModel> getRootCategories() {
        return rootCategories;
    }

    public void setRootCategories(Set<CategoryModel> rootCategories) {
        this.rootCategories = rootCategories;
    }

    public CurrencyModel getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyModel defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Set<LanguageModel> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<LanguageModel> languages) {
        this.languages = languages;
    }

    public Set<CountryModel> getTerritories() {
        return territories;
    }

    public void setTerritories(Set<CountryModel> territories) {
        this.territories = territories;
    }

    public Set<AgreementModel> getAgreements() {
        return agreements;
    }

    public void setAgreements(Set<AgreementModel> agreements) {
        this.agreements = agreements;
    }

    public Set<CatalogVersionModel> getCatalogVersions() {
        return catalogVersions;
    }

    public void setCatalogVersions(Set<CatalogVersionModel> catalogVersions) {
        this.catalogVersions = catalogVersions;
    }
}
