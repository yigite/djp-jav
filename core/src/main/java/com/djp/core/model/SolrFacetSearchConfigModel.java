
package com.djp.core.model;


import jakarta.persistence.*;
import javax.print.attribute.standard.Media;

@Entity
@Table(name = "SolrFacetSearchConfig")
public class SolrFacetSearchConfigModel extends GenericItemModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;
    private Media document;
    private Boolean enabledLanguageFallbackMechanism;
    private String indexNamePrefix;
//    private SolrKeywordRedirectLangMapping languageKeywordRedirectMapping;
//    private SolrStopWordLangMapping languageStopWordMapping;
//    private SolrSynonymLangMapping languageSynonymMapping;
//    private StringCollection listeners;
    private String name;
//    private SolrIndexConfig solrIndexConfig;
//    private SolrSearchConfig solrSearchConfig;
//    private SolrServerConfig solrServerConfig;


    public SolrFacetSearchConfigModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Media getDocument() {
        return document;
    }

    public void setDocument(Media document) {
        this.document = document;
    }

    public Boolean getEnabledLanguageFallbackMechanism() {
        return enabledLanguageFallbackMechanism;
    }

    public void setEnabledLanguageFallbackMechanism(Boolean enabledLanguageFallbackMechanism) {
        this.enabledLanguageFallbackMechanism = enabledLanguageFallbackMechanism;
    }

    public String getIndexNamePrefix() {
        return indexNamePrefix;
    }

    public void setIndexNamePrefix(String indexNamePrefix) {
        this.indexNamePrefix = indexNamePrefix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
