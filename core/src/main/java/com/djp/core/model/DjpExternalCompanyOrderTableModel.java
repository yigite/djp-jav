package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "DjpExternalCompanyOrderTable")
public class DjpExternalCompanyOrderTableModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;
    private String generateExternalCode;
    private String salesDocument;

    @OneToOne
    private DjpExternalCustomerModel customer;

    @OneToOne
    private DjpExternalCompanyModel company;

    private Double totalPrice;

    @OneToOne
    private CurrencyModel currency;

    @OneToOne
    private AddressModel invoiceAddress;

    private Boolean toBePricedByContract;
    private Boolean invoiceRequired;

    @OneToOne
    private DjpExternalDeviceDetailModel deviceDetail;

    @OneToOne
    private QrForVoucherModel qr;

    private String personalDetail;
    private String deviceID;
    private Date sapOrderDate;

    @OneToMany
    private Set<DjpExternalOrderEntriesModel> entries;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGenerateExternalCode() {
        return generateExternalCode;
    }

    public void setGenerateExternalCode(String generateExternalCode) {
        this.generateExternalCode = generateExternalCode;
    }

    public String getSalesDocument() {
        return salesDocument;
    }

    public void setSalesDocument(String salesDocument) {
        this.salesDocument = salesDocument;
    }

    public DjpExternalCustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(DjpExternalCustomerModel customer) {
        this.customer = customer;
    }

    public DjpExternalCompanyModel getCompany() {
        return company;
    }

    public void setCompany(DjpExternalCompanyModel company) {
        this.company = company;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public AddressModel getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(AddressModel invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public Boolean getToBePricedByContract() {
        return toBePricedByContract;
    }

    public void setToBePricedByContract(Boolean toBePricedByContract) {
        this.toBePricedByContract = toBePricedByContract;
    }

    public Boolean getInvoiceRequired() {
        return invoiceRequired;
    }

    public void setInvoiceRequired(Boolean invoiceRequired) {
        this.invoiceRequired = invoiceRequired;
    }

    public DjpExternalDeviceDetailModel getDeviceDetail() {
        return deviceDetail;
    }

    public void setDeviceDetail(DjpExternalDeviceDetailModel deviceDetail) {
        this.deviceDetail = deviceDetail;
    }

    public QrForVoucherModel getQr() {
        return qr;
    }

    public void setQr(QrForVoucherModel qr) {
        this.qr = qr;
    }

    public String getPersonalDetail() {
        return personalDetail;
    }

    public void setPersonalDetail(String personalDetail) {
        this.personalDetail = personalDetail;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Date getSapOrderDate() {
        return sapOrderDate;
    }

    public void setSapOrderDate(Date sapOrderDate) {
        this.sapOrderDate = sapOrderDate;
    }

    public Set<DjpExternalOrderEntriesModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<DjpExternalOrderEntriesModel> entries) {
        this.entries = entries;
    }
}