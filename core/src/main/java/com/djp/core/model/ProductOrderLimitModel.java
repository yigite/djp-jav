package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="ProductOrderLimit")
public class ProductOrderLimitModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;
    private int intervalMaxOrdersNumber;
    private int intervalValue;
    private int maxNumberPerOrder;

    @OneToMany(mappedBy = "productOrderLimit", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<ProductModel> products;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getIntervalMaxOrdersNumber() {
        return intervalMaxOrdersNumber;
    }

    public void setIntervalMaxOrdersNumber(int intervalMaxOrdersNumber) {
        this.intervalMaxOrdersNumber = intervalMaxOrdersNumber;
    }

    public int getIntervalValue() {
        return intervalValue;
    }

    public void setIntervalValue(int intervalValue) {
        this.intervalValue = intervalValue;
    }

    public int getMaxNumberPerOrder() {
        return maxNumberPerOrder;
    }

    public void setMaxNumberPerOrder(int maxNumberPerOrder) {
        this.maxNumberPerOrder = maxNumberPerOrder;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }
}
