package com.djp.core.model;


import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "CMSSite")
public class CMSSiteModel extends BaseSiteModel {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "CMSSite_urlPatterns", joinColumns = @JoinColumn(name = "CMSSite_id"))
    @Column(name = "urlPattern")
    private Set<String> urlPatterns;

    private Boolean active;

    private Date activeFrom;

    private Date activeUntil;

    @OneToOne
    private CatalogModel defaultCatalog;

    private String redirectURL;

    private String previewURL;

    private String startPageLabel;

    private boolean openPreviewInNewTab;

    @OneToOne
    private CategoryModel defaultPreviewCategory;

    @OneToOne
    private ProductModel defaultPreviewProduct;

    @OneToOne
    private CatalogModel defaultPreviewCatalog;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "CMSSite_urlEncodingAttributes", joinColumns = @JoinColumn(name = "CMSSite_id"))
    @Column(name = "urlEncodingAttribute")
    private Set<String> urlEncodingAttributes;

    @OneToMany(mappedBy = "cmsSiteModel")
    private Set<MediaModel> siteMaps;

    @OneToOne
    private SiteMapConfigModel siteMapConfig;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<String> getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(Set<String> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveUntil() {
        return activeUntil;
    }

    public void setActiveUntil(Date activeUntil) {
        this.activeUntil = activeUntil;
    }

    public CatalogModel getDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(CatalogModel defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getPreviewURL() {
        return previewURL;
    }

    public void setPreviewURL(String previewURL) {
        this.previewURL = previewURL;
    }

    public String getStartPageLabel() {
        return startPageLabel;
    }

    public void setStartPageLabel(String startPageLabel) {
        this.startPageLabel = startPageLabel;
    }

    public boolean isOpenPreviewInNewTab() {
        return openPreviewInNewTab;
    }

    public void setOpenPreviewInNewTab(boolean openPreviewInNewTab) {
        this.openPreviewInNewTab = openPreviewInNewTab;
    }

    public CategoryModel getDefaultPreviewCategory() {
        return defaultPreviewCategory;
    }

    public void setDefaultPreviewCategory(CategoryModel defaultPreviewCategory) {
        this.defaultPreviewCategory = defaultPreviewCategory;
    }

    public ProductModel getDefaultPreviewProduct() {
        return defaultPreviewProduct;
    }

    public void setDefaultPreviewProduct(ProductModel defaultPreviewProduct) {
        this.defaultPreviewProduct = defaultPreviewProduct;
    }

    public CatalogModel getDefaultPreviewCatalog() {
        return defaultPreviewCatalog;
    }

    public void setDefaultPreviewCatalog(CatalogModel defaultPreviewCatalog) {
        this.defaultPreviewCatalog = defaultPreviewCatalog;
    }

    public Set<String> getUrlEncodingAttributes() {
        return urlEncodingAttributes;
    }

    public void setUrlEncodingAttributes(Set<String> urlEncodingAttributes) {
        this.urlEncodingAttributes = urlEncodingAttributes;
    }

    public Set<MediaModel> getSiteMaps() {
        return siteMaps;
    }

    public void setSiteMaps(Set<MediaModel> siteMaps) {
        this.siteMaps = siteMaps;
    }

    public SiteMapConfigModel getSiteMapConfig() {
        return siteMapConfig;
    }

    public void setSiteMapConfig(SiteMapConfigModel siteMapConfig) {
        this.siteMapConfig = siteMapConfig;
    }
}
