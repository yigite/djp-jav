package com.djp.core.model;

import com.djp.core.enums.ConfiguratorType;
import com.djp.core.enums.ProductInfoStatus;

import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "AbstractOrderEntryProductInfo")
public class AbstractOrderEntryProductInfoModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn
    private AbstractOrderEntryModel orderEntry;

    private ConfiguratorType configuratorType;
    private ProductInfoStatus productInfoStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AbstractOrderEntryModel getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(AbstractOrderEntryModel orderEntry) {
        this.orderEntry = orderEntry;
    }

    public ConfiguratorType getConfiguratorType() {
        return configuratorType;
    }

    public void setConfiguratorType(ConfiguratorType configuratorType) {
        this.configuratorType = configuratorType;
    }

    public ProductInfoStatus getProductInfoStatus() {
        return productInfoStatus;
    }

    public void setProductInfoStatus(ProductInfoStatus productInfoStatus) {
        this.productInfoStatus = productInfoStatus;
    }
}
