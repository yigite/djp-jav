package com.djp.core.model;

import com.djp.core.enums.DiscountChannel;
import com.djp.core.enums.DjpEnumValue;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name="PDTRow")
@Inheritance(strategy = InheritanceType.JOINED)
public class PDTRowModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String productId;
    private long productMatchQualifier;
    private long userMatchQualifier;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;

    @ManyToOne
    @JoinColumn
    private ProductModel product;

    @OneToOne
    private UserModel user;

    private DiscountChannel salesChannel;

    private DjpEnumValue pg;
    private DjpEnumValue ug;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getProductMatchQualifier() {
        return productMatchQualifier;
    }

    public void setProductMatchQualifier(long productMatchQualifier) {
        this.productMatchQualifier = productMatchQualifier;
    }

    public long getUserMatchQualifier() {
        return userMatchQualifier;
    }

    public void setUserMatchQualifier(long userMatchQualifier) {
        this.userMatchQualifier = userMatchQualifier;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public DiscountChannel getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(DiscountChannel salesChannel) {
        this.salesChannel = salesChannel;
    }

    public DjpEnumValue getPg() {
        return pg;
    }

    public void setPg(DjpEnumValue pg) {
        this.pg = pg;
    }

    public DjpEnumValue getUg() {
        return ug;
    }

    public void setUg(DjpEnumValue ug) {
        this.ug = ug;
    }
}
