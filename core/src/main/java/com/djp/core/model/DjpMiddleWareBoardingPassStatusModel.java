package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpMiddleWareBoardingPassStatus")
public class DjpMiddleWareBoardingPassStatusModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Integer statusForMiddleWare;
    private String statusFromMiddleWare;
    private QrType whereUsed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getStatusForMiddleWare() {
        return statusForMiddleWare;
    }

    public void setStatusForMiddleWare(Integer statusForMiddleWare) {
        this.statusForMiddleWare = statusForMiddleWare;
    }

    public String getStatusFromMiddleWare() {
        return statusFromMiddleWare;
    }

    public void setStatusFromMiddleWare(String statusFromMiddleWare) {
        this.statusFromMiddleWare = statusFromMiddleWare;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }
}
