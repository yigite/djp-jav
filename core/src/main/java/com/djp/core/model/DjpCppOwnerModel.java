package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name = "DjpCppOwner")
public class DjpCppOwnerModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    private String id;

    @OneToOne
    private AddressModel paymentAddress;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AddressModel getPaymentAddress() {
        return paymentAddress;
    }

    public void setPaymentAddress(AddressModel paymentAddress) {
        this.paymentAddress = paymentAddress;
    }
}
