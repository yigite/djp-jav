package com.djp.core.model;

import org.apache.commons.collections.map.CaseInsensitiveMap;

import java.util.Collections;
import java.util.Map;

public abstract class LocalizableItemModel extends ExtensibleItemModel{
    public static final String LANGUAGE_FALLBACK_ENABLED = "enable.language.fallback";
    private transient Map localizedPropertyValues;


    public LocalizableItemModel() {
    }


    private Map getBaseMap(boolean createNew) {
        if (this.localizedPropertyValues == null && createNew) {
            this.localizedPropertyValues = new CaseInsensitiveMap();
        }

        return this.localizedPropertyValues != null ? this.localizedPropertyValues : Collections.EMPTY_MAP;
    }

    private Map getLocalizedJaloOnlyPropertyMap(String name, boolean createNew) {
        Map map = (Map)this.getBaseMap(createNew).get(name);
        if (map == null && createNew) {
            this.getBaseMap(createNew).put(name, map = new CaseInsensitiveMap());
        }

        return (Map)(map != null ? map : Collections.EMPTY_MAP);
    }
}
