package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "DjpExternalOrderEntries")
public class DjpExternalOrderEntriesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private ProductModel entry;

    private Integer entryQuantity;
    private Integer entryNumber;
    private Double entryPrice;
    private Double basePrice;

    @OneToOne
    private CurrencyModel currency;

    private Date arrivalFlightDate;
    private String arrivalFlightCode;
    private Date departureFlightDate;
    private String departureFlightCode;
    private Date customerArrivalDate;

    @OneToOne
    private DjpMeetGreetInfoModel djpMeetGreetInfoModel;

    private Boolean forChild;
    private Boolean forOverSixtyFiveYearsOld;
    private Date birthDate;

    @ManyToOne
    private DjpExternalRefundRemainingModel refund;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "guest")
    private Set<DjpGuestModel> guests;

    @ManyToOne
    private DjpExternalCompanyOrderTableModel order;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProductModel getEntry() {
        return entry;
    }

    public void setEntry(ProductModel entry) {
        this.entry = entry;
    }

    public Integer getEntryQuantity() {
        return entryQuantity;
    }

    public void setEntryQuantity(Integer entryQuantity) {
        this.entryQuantity = entryQuantity;
    }

    public Integer getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(Integer entryNumber) {
        this.entryNumber = entryNumber;
    }

    public Double getEntryPrice() {
        return entryPrice;
    }

    public void setEntryPrice(Double entryPrice) {
        this.entryPrice = entryPrice;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public Date getArrivalFlightDate() {
        return arrivalFlightDate;
    }

    public void setArrivalFlightDate(Date arrivalFlightDate) {
        this.arrivalFlightDate = arrivalFlightDate;
    }

    public String getArrivalFlightCode() {
        return arrivalFlightCode;
    }

    public void setArrivalFlightCode(String arrivalFlightCode) {
        this.arrivalFlightCode = arrivalFlightCode;
    }

    public Date getDepartureFlightDate() {
        return departureFlightDate;
    }

    public void setDepartureFlightDate(Date departureFlightDate) {
        this.departureFlightDate = departureFlightDate;
    }

    public String getDepartureFlightCode() {
        return departureFlightCode;
    }

    public void setDepartureFlightCode(String departureFlightCode) {
        this.departureFlightCode = departureFlightCode;
    }

    public Date getCustomerArrivalDate() {
        return customerArrivalDate;
    }

    public void setCustomerArrivalDate(Date customerArrivalDate) {
        this.customerArrivalDate = customerArrivalDate;
    }

    public DjpMeetGreetInfoModel getDjpMeetGreetInfoModel() {
        return djpMeetGreetInfoModel;
    }

    public void setDjpMeetGreetInfoModel(DjpMeetGreetInfoModel djpMeetGreetInfoModel) {
        this.djpMeetGreetInfoModel = djpMeetGreetInfoModel;
    }

    public Boolean getForChild() {
        return forChild;
    }

    public void setForChild(Boolean forChild) {
        this.forChild = forChild;
    }

    public Boolean getForOverSixtyFiveYearsOld() {
        return forOverSixtyFiveYearsOld;
    }

    public void setForOverSixtyFiveYearsOld(Boolean forOverSixtyFiveYearsOld) {
        this.forOverSixtyFiveYearsOld = forOverSixtyFiveYearsOld;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public DjpExternalRefundRemainingModel getRefund() {
        return refund;
    }

    public void setRefund(DjpExternalRefundRemainingModel refund) {
        this.refund = refund;
    }

    public Set<DjpGuestModel> getGuests() {
        return guests;
    }

    public void setGuests(Set<DjpGuestModel> guests) {
        this.guests = guests;
    }

    public DjpExternalCompanyOrderTableModel getOrder() {
        return order;
    }

    public void setOrder(DjpExternalCompanyOrderTableModel order) {
        this.order = order;
    }
}
