package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "StoreLocatorFeature")
public class StoreLocatorFeatureModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private String name;

    @OneToOne
    private MediaModel icon;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="StoreLocation2Locator",
            joinColumns = {@JoinColumn(name="feature")},
            inverseJoinColumns = {@JoinColumn(name="pointOfService")})
    private Set<PointOfServiceModel> pointOfServices;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaModel getIcon() {
        return icon;
    }

    public void setIcon(MediaModel icon) {
        this.icon = icon;
    }

    public Set<PointOfServiceModel> getPointOfServices() {
        return pointOfServices;
    }

    public void setPointOfServices(Set<PointOfServiceModel> pointOfServices) {
        this.pointOfServices = pointOfServices;
    }
}
