package com.djp.core.model;

import com.djp.core.enums.TourniquetBoardingStatus;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DjpBoardingPassUsingInfo")
public class DjpBoardingPassUsingInfoModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Boolean companyOrIndividual;

    @OneToOne
    private ContractedCompaniesModel company;

    private Date scheduledOutStartDate;

    private Date scheduledOutEndDate;

    private boolean isTimeOk;

    private TourniquetBoardingStatus status;

    private Integer indexUsingData;

    private Integer stayHourLimit;

    @OneToOne
    private DjpBoardingPassModel boardingPass;

    @OneToOne
    private DjpAccessBoardingModel accBoardingPass;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getCompanyOrIndividual() {
        return companyOrIndividual;
    }

    public void setCompanyOrIndividual(Boolean companyOrIndividual) {
        this.companyOrIndividual = companyOrIndividual;
    }

    public ContractedCompaniesModel getCompany() {
        return company;
    }

    public void setCompany(ContractedCompaniesModel company) {
        this.company = company;
    }

    public Date getScheduledOutStartDate() {
        return scheduledOutStartDate;
    }

    public void setScheduledOutStartDate(Date scheduledOutStartDate) {
        this.scheduledOutStartDate = scheduledOutStartDate;
    }

    public Date getScheduledOutEndDate() {
        return scheduledOutEndDate;
    }

    public void setScheduledOutEndDate(Date scheduledOutEndDate) {
        this.scheduledOutEndDate = scheduledOutEndDate;
    }

    public Boolean getIsTimeOk() {
        return isTimeOk;
    }

    public void setIsTimeOk(Boolean timeOk) {
        isTimeOk = timeOk;
    }

    public TourniquetBoardingStatus getStatus() {
        return status;
    }

    public void setStatus(TourniquetBoardingStatus status) {
        this.status = status;
    }

    public Integer getIndexUsingData() {
        return indexUsingData;
    }

    public void setIndexUsingData(Integer indexUsingData) {
        this.indexUsingData = indexUsingData;
    }

    public Integer getStayHourLimit() {
        return stayHourLimit;
    }

    public void setStayHourLimit(Integer stayHourLimit) {
        this.stayHourLimit = stayHourLimit;
    }

    public DjpBoardingPassModel getBoardingPass() {
        return boardingPass;
    }

    public void setBoardingPass(DjpBoardingPassModel boardingPass) {
        this.boardingPass = boardingPass;
    }

    public DjpAccessBoardingModel getAccBoardingPass() {
        return accBoardingPass;
    }

    public void setAccBoardingPass(DjpAccessBoardingModel accBoardingPass) {
        this.accBoardingPass = accBoardingPass;
    }
}
