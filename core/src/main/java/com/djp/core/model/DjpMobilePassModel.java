package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name="DjpMobilePass")
public class DjpMobilePassModel extends DjpBoardingPassModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
}
