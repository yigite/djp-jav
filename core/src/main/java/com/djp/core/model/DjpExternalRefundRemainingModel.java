package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DjpExternalRefundRemaining")
public class DjpExternalRefundRemainingModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private DjpExternalCustomerModel externalUser;

    @OneToOne
    private DjpExternalCompanyModel company;

    @OneToOne
    private DjpExternalCompanyOrderTableModel order;

    private Double totalPrice;

    @OneToOne
    private CurrencyModel currency;

    private String salesDocument;
    private String generateExternalCode;
    private String personalDetail;

    @OneToMany(mappedBy = "refund")
    private Set<DjpExternalOrderEntriesModel> entries;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DjpExternalCustomerModel getExternalUser() {
        return externalUser;
    }

    public void setExternalUser(DjpExternalCustomerModel externalUser) {
        this.externalUser = externalUser;
    }

    public DjpExternalCompanyModel getCompany() {
        return company;
    }

    public void setCompany(DjpExternalCompanyModel company) {
        this.company = company;
    }

    public DjpExternalCompanyOrderTableModel getOrder() {
        return order;
    }

    public void setOrder(DjpExternalCompanyOrderTableModel order) {
        this.order = order;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public String getSalesDocument() {
        return salesDocument;
    }

    public void setSalesDocument(String salesDocument) {
        this.salesDocument = salesDocument;
    }

    public String getGenerateExternalCode() {
        return generateExternalCode;
    }

    public void setGenerateExternalCode(String generateExternalCode) {
        this.generateExternalCode = generateExternalCode;
    }

    public String getPersonalDetail() {
        return personalDetail;
    }

    public void setPersonalDetail(String personalDetail) {
        this.personalDetail = personalDetail;
    }

    public Set<DjpExternalOrderEntriesModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<DjpExternalOrderEntriesModel> entries) {
        this.entries = entries;
    }
}
