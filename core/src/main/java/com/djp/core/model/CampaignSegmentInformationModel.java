package com.djp.core.model;

import com.djp.core.enums.UserGroupCampaignTypeEnum;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "CampaignSegmentInformation")
public class CampaignSegmentInformationModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int count;
    private boolean isGeneral;
    private UserGroupCampaignTypeEnum campaignTypeEnum;

    @OneToMany(mappedBy = "campaignSegmentInformation", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<ProductModel> products;

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isGeneral() {
        return isGeneral;
    }

    public void setGeneral(boolean general) {
        isGeneral = general;
    }

    public UserGroupCampaignTypeEnum getCampaignTypeEnum() {
        return campaignTypeEnum;
    }

    public void setCampaignTypeEnum(UserGroupCampaignTypeEnum campaignTypeEnum) {
        this.campaignTypeEnum = campaignTypeEnum;
    }
}
