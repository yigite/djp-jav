package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "CatalogVersion")
public class CatalogVersionModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String version;
    private String mimeRootDirectory;
    private String generatorInfo;
    private String categorySystemID;
    private String categorySystemName;
    private String categorySystemDescription;
    private String mnemonic;
    private boolean active;
    private boolean inclFreight;
    private boolean inclPacking;
    private boolean inclAssurance;
    private boolean inclDuty;
    private int previousUpdateVersion;
    private Date generationDate;

    @OneToMany(mappedBy = "catalogVersion", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<AgreementModel> agreements;

    @ManyToOne
    @JoinColumn
    private CatalogModel catalog;

    @ManyToMany(mappedBy = "writableCatalogVersions", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<PrincipalModel> writePrincipals;

    @ManyToMany(mappedBy = "readableCatalogVersions", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<PrincipalModel> readPrincipals;

    @OneToOne
    private CurrencyModel defaultCurrency;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<LanguageModel> languages;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<CountryModel> territories;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<CategoryModel> rootCategories;

    @OneToMany(mappedBy = "sourceVersion", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<SyncItemJobModel> synchronizations;

    @OneToMany(mappedBy = "targetVersion", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn
    private Set<SyncItemJobModel> incomingSynchronizations;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="CxPersProcToCatVer",
            joinColumns={@JoinColumn(name="catalogVersion")},
            inverseJoinColumns={@JoinColumn(name="cxPersonalizationProcess")})
    private Set<CxPersonalizationProcessModel> cxPersonalizationProcesses;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMimeRootDirectory() {
        return mimeRootDirectory;
    }

    public void setMimeRootDirectory(String mimeRootDirectory) {
        this.mimeRootDirectory = mimeRootDirectory;
    }

    public String getGeneratorInfo() {
        return generatorInfo;
    }

    public void setGeneratorInfo(String generatorInfo) {
        this.generatorInfo = generatorInfo;
    }

    public String getCategorySystemID() {
        return categorySystemID;
    }

    public void setCategorySystemID(String categorySystemID) {
        this.categorySystemID = categorySystemID;
    }

    public String getCategorySystemName() {
        return categorySystemName;
    }

    public void setCategorySystemName(String categorySystemName) {
        this.categorySystemName = categorySystemName;
    }

    public String getCategorySystemDescription() {
        return categorySystemDescription;
    }

    public void setCategorySystemDescription(String categorySystemDescription) {
        this.categorySystemDescription = categorySystemDescription;
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isInclFreight() {
        return inclFreight;
    }

    public void setInclFreight(boolean inclFreight) {
        this.inclFreight = inclFreight;
    }

    public boolean isInclPacking() {
        return inclPacking;
    }

    public void setInclPacking(boolean inclPacking) {
        this.inclPacking = inclPacking;
    }

    public boolean isInclAssurance() {
        return inclAssurance;
    }

    public void setInclAssurance(boolean inclAssurance) {
        this.inclAssurance = inclAssurance;
    }

    public boolean isInclDuty() {
        return inclDuty;
    }

    public void setInclDuty(boolean inclDuty) {
        this.inclDuty = inclDuty;
    }

    public int getPreviousUpdateVersion() {
        return previousUpdateVersion;
    }

    public void setPreviousUpdateVersion(int previousUpdateVersion) {
        this.previousUpdateVersion = previousUpdateVersion;
    }

    public Date getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(Date generationDate) {
        this.generationDate = generationDate;
    }

    public Set<AgreementModel> getAgreements() {
        return agreements;
    }

    public void setAgreements(Set<AgreementModel> agreements) {
        this.agreements = agreements;
    }

    public CatalogModel getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogModel catalog) {
        this.catalog = catalog;
    }

    public Set<PrincipalModel> getWritePrincipals() {
        return writePrincipals;
    }

    public void setWritePrincipals(Set<PrincipalModel> writePrincipals) {
        this.writePrincipals = writePrincipals;
    }

    public Set<PrincipalModel> getReadPrincipals() {
        return readPrincipals;
    }

    public void setReadPrincipals(Set<PrincipalModel> readPrincipals) {
        this.readPrincipals = readPrincipals;
    }

    public CurrencyModel getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyModel defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Set<LanguageModel> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<LanguageModel> languages) {
        this.languages = languages;
    }

    public Set<CountryModel> getTerritories() {
        return territories;
    }

    public void setTerritories(Set<CountryModel> territories) {
        this.territories = territories;
    }

    public Set<CategoryModel> getRootCategories() {
        return rootCategories;
    }

    public void setRootCategories(Set<CategoryModel> rootCategories) {
        this.rootCategories = rootCategories;
    }

    public Set<SyncItemJobModel> getSynchronizations() {
        return synchronizations;
    }

    public void setSynchronizations(Set<SyncItemJobModel> synchronizations) {
        this.synchronizations = synchronizations;
    }

    public Set<SyncItemJobModel> getIncomingSynchronizations() {
        return incomingSynchronizations;
    }

    public void setIncomingSynchronizations(Set<SyncItemJobModel> incomingSynchronizations) {
        this.incomingSynchronizations = incomingSynchronizations;
    }

    public Set<CxPersonalizationProcessModel> getCxPersonalizationProcesses() {
        return cxPersonalizationProcesses;
    }

    public void setCxPersonalizationProcesses(Set<CxPersonalizationProcessModel> cxPersonalizationProcesses) {
        this.cxPersonalizationProcesses = cxPersonalizationProcesses;
    }
}
