package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name="CMSProductRestriction")
@PrimaryKeyJoinColumn(name="id")
public class CMSProductRestrictionModel extends AbstractRestrictionModel{
    public CMSProductRestrictionModel()
    {
        super();
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="Products4Restriction",
            joinColumns = {@JoinColumn(name="restriction")},
            inverseJoinColumns = {@JoinColumn(name="product")})
    private Set<ProductModel> products;

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }
}
