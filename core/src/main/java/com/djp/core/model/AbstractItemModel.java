package com.djp.core.model;


import com.djp.service.model.ItemModelContext;
import com.djp.service.model.ItemModelInternalContext;

import java.io.Serializable;

public abstract class AbstractItemModel implements Serializable {
    private final ItemModelInternalContext ctx;

    public AbstractItemModel() {
        this.ctx = AbstractItemModel.EntityContextFactoryHolder.factory.createNew(this.getClass());
    }

    protected AbstractItemModel(ItemModelContext ctx) {
        if (ctx == null) {
            throw new IllegalArgumentException("context must not be null");
        } else if (ctx instanceof ItemModelInternalContext) {
            this.ctx = (ItemModelInternalContext)ctx;
        } else {
            throw new IllegalArgumentException("wrong type of context");
        }
    }

    protected ItemModelInternalContext getPersistenceContext() {
        return this.ctx;
    }


    protected static Boolean toObject(boolean value) {
        return value;
    }

    protected static Integer toObject(int value) {
        return value;
    }

    protected static Double toObject(double value) {
        return value;
    }

    protected static Float toObject(float value) {
        return value;
    }

    protected static Byte toObject(byte value) {
        return value;
    }

    protected static Long toObject(long value) {
        return value;
    }

    protected static Short toObject(short value) {
        return value;
    }

    protected static Character toObject(char value) {
        return value;
    }
    private static NewModelContextFactory bootstrapFromSystemProperty(String prop) {
        NewModelContextFactory tmp = null;
        String className = readClassNameFromProperty(prop, "de.hybris.platform.servicelayer.model.DefaultNewModelContextFactory");

        try {
            ClassLoader ctxLoader = Thread.currentThread().getContextClassLoader();
            tmp = (NewModelContextFactory)(ctxLoader == null ? Class.forName(className) : Class.forName(className, true, ctxLoader)).newInstance();
            return tmp;
        } catch (Exception var4) {
            throw new IllegalStateException("the 'new model' context factory '" + className + "' is illegal", var4);
        }
    }
    private static final class EntityContextFactoryHolder {
        private static final NewModelContextFactory factory = AbstractItemModel.bootstrapFromSystemProperty("model.context.factory.class");

        private EntityContextFactoryHolder() {
        }
    }
    private static String readClassNameFromProperty(String property, String defaultClassName) {
        String ret = System.getProperty(property);
        return ret == null ? defaultClassName : ret;
    }
    public interface NewModelContextFactory {
        ItemModelInternalContext createNew(Class var1);
    }
}
