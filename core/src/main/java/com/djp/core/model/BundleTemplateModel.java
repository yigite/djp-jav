package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "BundleTemplate")
public class BundleTemplateModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long _id;

    @Column(unique = true)
    private String id;

    private String name;
    private String description;
    private String version;

    @OneToMany(mappedBy = "parentTemplate", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<BundleTemplateModel> childTemplates;

    @ManyToOne
    @JoinColumn
    private BundleTemplateModel parentTemplate;

    @ManyToMany(mappedBy = "bundleTemplates", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<ProductModel> products;

    @ManyToMany(mappedBy = "dependentBundleTemplates", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<BundleTemplateModel> requiredBundleTemplates;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE})
    @JoinTable(name = "Req2DependBundleTemplRel",
            joinColumns = {@JoinColumn(name = "requiredBundleTemplate")},
            inverseJoinColumns = {@JoinColumn(name = "dependentBundleTemplate")})
    private Set<BundleTemplateModel> dependentBundleTemplates;

    @ManyToOne
    @JoinColumn
    private BundleTemplateStatusModel status;

    @OneToOne
    private CatalogVersionModel catalogVersion;

    @OneToMany(mappedBy = "bundleTemplate", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<DisableProductBundleRuleModel> disableProductBundleRules;

    @OneToOne
    private BundleSelectionCriteriaModel bundleSelectionCriteria;

    @OneToMany(mappedBy = "bundleTemplate", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<ChangeProductPriceBundleRuleModel> changeProductPriceBundleRules;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Set<BundleTemplateModel> getChildTemplates() {
        return childTemplates;
    }

    public void setChildTemplates(Set<BundleTemplateModel> childTemplates) {
        this.childTemplates = childTemplates;
    }

    public BundleTemplateModel getParentTemplate() {
        return parentTemplate;
    }

    public void setParentTemplate(BundleTemplateModel parentTemplate) {
        this.parentTemplate = parentTemplate;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public Set<BundleTemplateModel> getRequiredBundleTemplates() {
        return requiredBundleTemplates;
    }

    public void setRequiredBundleTemplates(Set<BundleTemplateModel> requiredBundleTemplates) {
        this.requiredBundleTemplates = requiredBundleTemplates;
    }

    public Set<BundleTemplateModel> getDependentBundleTemplates() {
        return dependentBundleTemplates;
    }

    public void setDependentBundleTemplates(Set<BundleTemplateModel> dependentBundleTemplates) {
        this.dependentBundleTemplates = dependentBundleTemplates;
    }

    public BundleTemplateStatusModel getStatus() {
        return status;
    }

    public void setStatus(BundleTemplateStatusModel status) {
        this.status = status;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public Set<DisableProductBundleRuleModel> getDisableProductBundleRules() {
        return disableProductBundleRules;
    }

    public void setDisableProductBundleRules(Set<DisableProductBundleRuleModel> disableProductBundleRules) {
        this.disableProductBundleRules = disableProductBundleRules;
    }

    public BundleSelectionCriteriaModel getBundleSelectionCriteria() {
        return bundleSelectionCriteria;
    }

    public void setBundleSelectionCriteria(BundleSelectionCriteriaModel bundleSelectionCriteria) {
        this.bundleSelectionCriteria = bundleSelectionCriteria;
    }

    public Set<ChangeProductPriceBundleRuleModel> getChangeProductPriceBundleRules() {
        return changeProductPriceBundleRules;
    }

    public void setChangeProductPriceBundleRules(Set<ChangeProductPriceBundleRuleModel> changeProductPriceBundleRules) {
        this.changeProductPriceBundleRules = changeProductPriceBundleRules;
    }
}
