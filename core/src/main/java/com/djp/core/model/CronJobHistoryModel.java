package com.djp.core.model;

import com.djp.core.enums.CronJobResult;
import com.djp.core.enums.CronJobStatus;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CronJobHistory")
public class CronJobHistoryModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String cronJobCode;

    @Column(nullable = false)
    private String jobCode;

    private int nodeID;
    private String userUid;
    private String failureMessage;
    private String statusLine;
    private Date startTime;
    private Date endTime;
    private boolean scheduled;
    private CronJobStatus status;
    private CronJobResult result;
    private double progress;

    @ManyToOne
    @JoinColumn
    private CronJobModel cronJob;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCronJobCode() {
        return cronJobCode;
    }

    public void setCronJobCode(String cronJobCode) {
        this.cronJobCode = cronJobCode;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getStatusLine() {
        return statusLine;
    }

    public void setStatusLine(String statusLine) {
        this.statusLine = statusLine;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public CronJobStatus getStatus() {
        return status;
    }

    public void setStatus(CronJobStatus status) {
        this.status = status;
    }

    public CronJobResult getResult() {
        return result;
    }

    public void setResult(CronJobResult result) {
        this.result = result;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public CronJobModel getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJobModel cronJob) {
        this.cronJob = cronJob;
    }
}
