package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name="DjpAdditionalServiceEntry")
public class DjpAdditionalServiceEntryModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long quantity;

    @OneToOne
    private AbstractOrderEntryModel belongToOrderEntry;

    @OneToOne
    private AbstractOrderEntryModel ownToOrderEntry;

    @OneToOne
    private ProductModel product;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="OrdEnt2AddtiServRel",
            joinColumns = {@JoinColumn(name="additionalService")},
            inverseJoinColumns = {@JoinColumn(name="entry")})
    private Set<AbstractOrderEntryModel> entries;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public AbstractOrderEntryModel getBelongToOrderEntry() {
        return belongToOrderEntry;
    }

    public void setBelongToOrderEntry(AbstractOrderEntryModel belongToOrderEntry) {
        this.belongToOrderEntry = belongToOrderEntry;
    }

    public AbstractOrderEntryModel getOwnToOrderEntry() {
        return ownToOrderEntry;
    }

    public void setOwnToOrderEntry(AbstractOrderEntryModel ownToOrderEntry) {
        this.ownToOrderEntry = ownToOrderEntry;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public Set<AbstractOrderEntryModel> getEntries() {
        return entries;
    }

    public void setEntries(Set<AbstractOrderEntryModel> entries) {
        this.entries = entries;
    }
}
