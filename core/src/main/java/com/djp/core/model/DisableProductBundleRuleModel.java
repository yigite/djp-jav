package com.djp.core.model;

import jakarta.persistence.*;

@Entity
@Table(name="DisableProductBundleRule")
@PrimaryKeyJoinColumn(name = "_id")
public class DisableProductBundleRuleModel extends AbstractBundleRuleModel{

    @ManyToOne
    @JoinColumn
    private BundleTemplateModel bundleTemplate;

    public DisableProductBundleRuleModel() {
        super();
    }

    public BundleTemplateModel getBundleTemplate() {
        return bundleTemplate;
    }

    public void setBundleTemplate(BundleTemplateModel bundleTemplate) {
        this.bundleTemplate = bundleTemplate;
    }
}
