package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Zone")
public class ZoneModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(
            name = "ZoneCountryRelation",
            joinColumns = {@JoinColumn(name = "zone")},
            inverseJoinColumns = {@JoinColumn(name = "country")}
    )
    private Set<CountryModel> countries;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<CountryModel> getCountries() {
        return countries;
    }

    public void setCountries(Set<CountryModel> countries) {
        this.countries = countries;
    }
}
