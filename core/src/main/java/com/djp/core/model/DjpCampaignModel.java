package com.djp.core.model;

import com.djp.core.enums.DjpCampaignCompanyEnum;
import com.djp.core.enums.DjpCampaignRemainingType;
import com.djp.core.enums.DjpCampaignTypeEnum;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "DjpCampaign")
public class DjpCampaignModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String code;

    private boolean absolute;
    private boolean hasFeaturing;
    private boolean hasRemaining;
    private boolean isNotificationCenter;
    private boolean isSpecial;
    private boolean isShowQR;
    private boolean showButton;
    private boolean showPromoCode;
    private boolean showTitle;
    private String buttonAction;
    private String buttonTitle;
    private String detailDescription;
    private Date expireDate;
    private Date startDate;
    private DjpCampaignRemainingType campaignRemainingType;
    private DjpCampaignTypeEnum campaignsType;
    private DjpCampaignCompanyEnum company;
    private BigDecimal discountAmount;

    @OneToOne
    private MediaModel detailImage;

    @OneToOne
    private MediaModel newDetailImage;

    @OneToOne
    private MediaModel newThumbnailImage;

    @ManyToMany(mappedBy = "campaigns", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<DjpCampaignModel> similarCampaigns;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name="DjpCampaign2CampaignRel",
            joinColumns={@JoinColumn(name="campaigns")},
            inverseJoinColumns={@JoinColumn(name="similarCampaigns")})
    private Set<DjpCampaignModel> campaigns;

    @OneToOne
    private DjpCampaignCategoryModel category;

    @ManyToMany(mappedBy = "campaigns", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<UserGroupModel> userGroups;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    public boolean isHasFeaturing() {
        return hasFeaturing;
    }

    public void setHasFeaturing(boolean hasFeaturing) {
        this.hasFeaturing = hasFeaturing;
    }

    public boolean isHasRemaining() {
        return hasRemaining;
    }

    public void setHasRemaining(boolean hasRemaining) {
        this.hasRemaining = hasRemaining;
    }

    public boolean isNotificationCenter() {
        return isNotificationCenter;
    }

    public void setNotificationCenter(boolean notificationCenter) {
        isNotificationCenter = notificationCenter;
    }

    public boolean isSpecial() {
        return isSpecial;
    }

    public void setSpecial(boolean special) {
        isSpecial = special;
    }

    public boolean isShowQR() {
        return isShowQR;
    }

    public void setShowQR(boolean showQR) {
        isShowQR = showQR;
    }

    public boolean isShowButton() {
        return showButton;
    }

    public void setShowButton(boolean showButton) {
        this.showButton = showButton;
    }

    public boolean isShowPromoCode() {
        return showPromoCode;
    }

    public void setShowPromoCode(boolean showPromoCode) {
        this.showPromoCode = showPromoCode;
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public DjpCampaignRemainingType getCampaignRemainingType() {
        return campaignRemainingType;
    }

    public void setCampaignRemainingType(DjpCampaignRemainingType campaignRemainingType) {
        this.campaignRemainingType = campaignRemainingType;
    }

    public DjpCampaignTypeEnum getCampaignsType() {
        return campaignsType;
    }

    public void setCampaignsType(DjpCampaignTypeEnum campaignsType) {
        this.campaignsType = campaignsType;
    }

    public DjpCampaignCompanyEnum getCompany() {
        return company;
    }

    public void setCompany(DjpCampaignCompanyEnum company) {
        this.company = company;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public MediaModel getDetailImage() {
        return detailImage;
    }

    public void setDetailImage(MediaModel detailImage) {
        this.detailImage = detailImage;
    }

    public MediaModel getNewDetailImage() {
        return newDetailImage;
    }

    public void setNewDetailImage(MediaModel newDetailImage) {
        this.newDetailImage = newDetailImage;
    }

    public MediaModel getNewThumbnailImage() {
        return newThumbnailImage;
    }

    public void setNewThumbnailImage(MediaModel newThumbnailImage) {
        this.newThumbnailImage = newThumbnailImage;
    }

    public Set<DjpCampaignModel> getSimilarCampaigns() {
        return similarCampaigns;
    }

    public void setSimilarCampaigns(Set<DjpCampaignModel> similarCampaigns) {
        this.similarCampaigns = similarCampaigns;
    }

    public Set<DjpCampaignModel> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(Set<DjpCampaignModel> campaigns) {
        this.campaigns = campaigns;
    }

    public DjpCampaignCategoryModel getCategory() {
        return category;
    }

    public void setCategory(DjpCampaignCategoryModel category) {
        this.category = category;
    }

    public Set<UserGroupModel> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(Set<UserGroupModel> userGroups) {
        this.userGroups = userGroups;
    }
}
