package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "QrForMeet")
public class QrForMeetModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String userID;
    private String assistantID;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    private MediaModel qr;

    private QrType whereIsValidQr;
    private Boolean usedForCampaign;
    private QrType whereUsed;
    private Date usageDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAssistantID() {
        return assistantID;
    }

    public void setAssistantID(String assistantID) {
        this.assistantID = assistantID;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public QrType getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(QrType whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }
}
