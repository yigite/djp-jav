package com.djp.core.model;

import com.djp.core.enums.QrType;

import java.util.Date;
import java.util.Set;


public class DjpContractedTicketsModel extends ItemModel
{
    /**<i>Generated model type code constant.</i>*/
    public static final String _TYPECODE = "IgaContractedTickets";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.uniqueID</code> attribute defined at extension <code>igacore</code>. */
    public static final String UNIQUEID = "uniqueID";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.contractID</code> attribute defined at extension <code>igacore</code>. */
    public static final String CONTRACTID = "contractID";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.contractedService</code> attribute defined at extension <code>igacore</code>. */
    public static final String CONTRACTEDSERVICE = "contractedService";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.startDate</code> attribute defined at extension <code>igacore</code>. */
    public static final String STARTDATE = "startDate";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.expireDate</code> attribute defined at extension <code>igacore</code>. */
    public static final String EXPIREDATE = "expireDate";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.whereWasTicketUseds</code> attribute defined at extension <code>igacore</code>. */
    public static final String WHEREWASTICKETUSEDS = "whereWasTicketUseds";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.claimed</code> attribute defined at extension <code>igacore</code>. */
    public static final String CLAIMED = "claimed";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.used</code> attribute defined at extension <code>igacore</code>. */
    public static final String USED = "used";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.guestQuantity</code> attribute defined at extension <code>igacore</code>. */
    public static final String GUESTQUANTITY = "guestQuantity";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.companyClientId</code> attribute defined at extension <code>igacore</code>. */
    public static final String COMPANYCLIENTID = "companyClientId";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.personalID</code> attribute defined at extension <code>igacore</code>. */
    public static final String PERSONALID = "personalID";

    /** <i>Generated constant</i> - Attribute key of <code>IgaContractedTickets.personalName</code> attribute defined at extension <code>igacore</code>. */
    public static final String PERSONALNAME = "personalName";


    /**
     * <i>Generated constructor</i> - Default constructor for generic creation.
     */
    public DjpContractedTicketsModel()
    {
        super();
    }

    @Deprecated
    public DjpContractedTicketsModel(final String _companyClientId, final String _contractID, final QrType _contractedService, final Date _expireDate, final Date _startDate, final String _uniqueID)
    {
        super();
        setCompanyClientId(_companyClientId);
        setContractID(_contractID);
        setContractedService(_contractedService);
        setExpireDate(_expireDate);
        setStartDate(_startDate);
        setUniqueID(_uniqueID);
    }

    @Deprecated
    public DjpContractedTicketsModel(final String _companyClientId, final String _contractID, final QrType _contractedService, final Date _expireDate, final ItemModel _owner, final Date _startDate, final String _uniqueID)
    {
        super();
        setCompanyClientId(_companyClientId);
        setContractID(_contractID);
        setContractedService(_contractedService);
        setExpireDate(_expireDate);
        setOwner(_owner);
        setStartDate(_startDate);
        setUniqueID(_uniqueID);
    }
    public Integer getClaimed()
    {
        //return getPersistenceContext().getPropertyValue(CLAIMED);
        return null;
    }

    public String getCompanyClientId()
    {
        //return getPersistenceContext().getPropertyValue(COMPANYCLIENTID);
        return null;
    }

    public QrType getContractedService()
    {
        //return getPersistenceContext().getPropertyValue(CONTRACTEDSERVICE);
        return null;
    }

    public String getContractID()
    {
        //return getPersistenceContext().getPropertyValue(CONTRACTID);
        return null;
    }

    public Date getExpireDate()
    {
        //return getPersistenceContext().getPropertyValue(EXPIREDATE);
        return null;
    }

    public Integer getGuestQuantity()
    {
        //return getPersistenceContext().getPropertyValue(GUESTQUANTITY);
        return null;
    }

    public String getPersonalID()
    {
        //return getPersistenceContext().getPropertyValue(PERSONALID);
        return null;
    }

    public String getPersonalName()
    {
        //return getPersistenceContext().getPropertyValue(PERSONALNAME);
        return null;
    }

    public Date getStartDate()
    {
        //return getPersistenceContext().getPropertyValue(STARTDATE);
        return null;
    }


    public String getUniqueID()
    {
        //return getPersistenceContext().getPropertyValue(UNIQUEID);
        return null;
    }


    public Integer getUsed()
    {
        //return getPersistenceContext().getPropertyValue(USED);
        return null;
    }

    public Set<QrType> getWhereWasTicketUseds()
    {
        //return getPersistenceContext().getPropertyValue(WHEREWASTICKETUSEDS);
        return null;
    }

    public void setClaimed(final Integer value)
    {
        //getPersistenceContext().setPropertyValue(CLAIMED, value);
    }

    public void setCompanyClientId(final String value)
    {
       // getPersistenceContext().setPropertyValue(COMPANYCLIENTID, value);
    }

    public void setContractedService(final QrType value)
    {
        //getPersistenceContext().setPropertyValue(CONTRACTEDSERVICE, value);
    }

    public void setContractID(final String value)
    {
        //getPersistenceContext().setPropertyValue(CONTRACTID, value);
    }

    public void setExpireDate(final Date value)
    {
       // getPersistenceContext().setPropertyValue(EXPIREDATE, value);
    }

    public void setGuestQuantity(final Integer value)
    {
        //getPersistenceContext().setPropertyValue(GUESTQUANTITY, value);
    }
    public void setPersonalID(final String value)
    {
        //getPersistenceContext().setPropertyValue(PERSONALID, value);
    }

    public void setPersonalName(final String value)
    {
        //getPersistenceContext().setPropertyValue(PERSONALNAME, value);
    }

    public void setStartDate(final Date value)
    {
       // getPersistenceContext().setPropertyValue(STARTDATE, value);
    }

    public void setUniqueID(final String value)
    {
       // getPersistenceContext().setPropertyValue(UNIQUEID, value);
    }

    public void setUsed(final Integer value)
    {
       // getPersistenceContext().setPropertyValue(USED, value);
    }

    public void setWhereWasTicketUseds(final Set<QrType> value)
    {
      //  getPersistenceContext().setPropertyValue(WHEREWASTICKETUSEDS, value);
    }

}

