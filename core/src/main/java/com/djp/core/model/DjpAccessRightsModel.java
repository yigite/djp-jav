package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "DjpAccessRights")
public class DjpAccessRightsModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String uid;
    private String airportCode;
    private String airportName;
    private String classCode;
    private String className;
    private String accessRight;
    private String btype;
    private boolean isActive;
    private boolean serviceUsageWithMembership;
    private String airlineCode;
    private String airlineName;

    @ElementCollection(targetClass = QrType.class)
    @CollectionTable(name = "DjpAccessRightsModel_QrType", joinColumns = @JoinColumn(name = "DjpAccessRightsModel_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "QrType_code")
    private Set<QrType> boardingTypes;

    private String whichRange;
    private String charactersToSearch;
    private Integer adultGuestCount;
    private Integer childGuestCount;
    private Integer customerUsageRight;
    private String contractID;
    private Date startDate;
    private Date expireDate;
    private Boolean requiredSameCompanyForGuest;
    private Boolean quickServiceUsage;

    @OneToMany
    private Set<DjpPassLocationsModel> validAtWhichLocation;

    private Integer whichBeforeLastCharacterPlace;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(String accessRight) {
        this.accessRight = accessRight;
    }

    public String getBtype() {
        return btype;
    }

    public void setBtype(String btype) {
        this.btype = btype;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isServiceUsageWithMembership() {
        return serviceUsageWithMembership;
    }

    public void setServiceUsageWithMembership(boolean serviceUsageWithMembership) {
        this.serviceUsageWithMembership = serviceUsageWithMembership;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public Set<QrType> getBoardingTypes() {
        return boardingTypes;
    }

    public void setBoardingTypes(Set<QrType> boardingTypes) {
        this.boardingTypes = boardingTypes;
    }

    public String getWhichRange() {
        return whichRange;
    }

    public void setWhichRange(String whichRange) {
        this.whichRange = whichRange;
    }

    public String getCharactersToSearch() {
        return charactersToSearch;
    }

    public void setCharactersToSearch(String charactersToSearch) {
        this.charactersToSearch = charactersToSearch;
    }

    public Integer getAdultGuestCount() {
        return adultGuestCount;
    }

    public void setAdultGuestCount(Integer adultGuestCount) {
        this.adultGuestCount = adultGuestCount;
    }

    public Integer getChildGuestCount() {
        return childGuestCount;
    }

    public void setChildGuestCount(Integer childGuestCount) {
        this.childGuestCount = childGuestCount;
    }

    public Integer getCustomerUsageRight() {
        return customerUsageRight;
    }

    public void setCustomerUsageRight(Integer customerUsageRight) {
        this.customerUsageRight = customerUsageRight;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Boolean getRequiredSameCompanyForGuest() {
        return requiredSameCompanyForGuest;
    }

    public void setRequiredSameCompanyForGuest(Boolean requiredSameCompanyForGuest) {
        this.requiredSameCompanyForGuest = requiredSameCompanyForGuest;
    }

    public Boolean getQuickServiceUsage() {
        return quickServiceUsage;
    }

    public void setQuickServiceUsage(Boolean quickServiceUsage) {
        this.quickServiceUsage = quickServiceUsage;
    }

    public Set<DjpPassLocationsModel> getValidAtWhichLocation() {
        return validAtWhichLocation;
    }

    public void setValidAtWhichLocation(Set<DjpPassLocationsModel> validAtWhichLocation) {
        this.validAtWhichLocation = validAtWhichLocation;
    }

    public Integer getWhichBeforeLastCharacterPlace() {
        return whichBeforeLastCharacterPlace;
    }

    public void setWhichBeforeLastCharacterPlace(Integer whichBeforeLastCharacterPlace) {
        this.whichBeforeLastCharacterPlace = whichBeforeLastCharacterPlace;
    }
}
