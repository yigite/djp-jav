package com.djp.core.model;

import jakarta.persistence.*;


@Entity
@Table(name = "BaseSite")
public class BaseSiteModel extends GenericItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String uid;

    private String name;

//    private SiteTheme theme;

    @OneToOne(fetch = FetchType.EAGER)
    private LanguageModel defaultLanguage;

    private String locale;

//    private SiteChannel channel;

//    private PromotionGroup defaultPromotionGroup;

    @OneToOne
    private SolrFacetSearchConfigModel solrFacetSearchConfiguration;

    private Integer cartRemovalAge;

    private Integer anonymousCartRemovalAge;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LanguageModel getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageModel defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public SolrFacetSearchConfigModel getSolrFacetSearchConfiguration() {
        return solrFacetSearchConfiguration;
    }

    public void setSolrFacetSearchConfiguration(SolrFacetSearchConfigModel solrFacetSearchConfiguration) {
        this.solrFacetSearchConfiguration = solrFacetSearchConfiguration;
    }

    public Integer getCartRemovalAge() {
        return cartRemovalAge;
    }

    public void setCartRemovalAge(Integer cartRemovalAge) {
        this.cartRemovalAge = cartRemovalAge;
    }

    public Integer getAnonymousCartRemovalAge() {
        return anonymousCartRemovalAge;
    }

    public void setAnonymousCartRemovalAge(Integer anonymousCartRemovalAge) {
        this.anonymousCartRemovalAge = anonymousCartRemovalAge;
    }
}
