package com.djp.core.model;

import com.djp.core.enums.QrType;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name="QrForCabinet")
public class QrForCabinetModel extends ItemModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String macAddress;
    private String nameSurname;
    private String cabinID;
    private String pnrCode;
    private String cabinInfo;
    private String productCode;
    private Date cabinStartTime;
    private Integer rentHour;
    private Date expireDate;

    @OneToOne
    private DjpUniqueTokenModel tokenKey;

    @OneToOne
    private MediaModel qr;
    private QrType whereIsValidQr;
    private QrType whereUsed;
    private Boolean usedForCampaign;

    @OneToOne
    private DjpPassLocationsModel whichLocation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getCabinID() {
        return cabinID;
    }

    public void setCabinID(String cabinID) {
        this.cabinID = cabinID;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public String getCabinInfo() {
        return cabinInfo;
    }

    public void setCabinInfo(String cabinInfo) {
        this.cabinInfo = cabinInfo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Date getCabinStartTime() {
        return cabinStartTime;
    }

    public void setCabinStartTime(Date cabinStartTime) {
        this.cabinStartTime = cabinStartTime;
    }

    public Integer getRentHour() {
        return rentHour;
    }

    public void setRentHour(Integer rentHour) {
        this.rentHour = rentHour;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public DjpUniqueTokenModel getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(DjpUniqueTokenModel tokenKey) {
        this.tokenKey = tokenKey;
    }

    public MediaModel getQr() {
        return qr;
    }

    public void setQr(MediaModel qr) {
        this.qr = qr;
    }

    public QrType getWhereIsValidQr() {
        return whereIsValidQr;
    }

    public void setWhereIsValidQr(QrType whereIsValidQr) {
        this.whereIsValidQr = whereIsValidQr;
    }

    public QrType getWhereUsed() {
        return whereUsed;
    }

    public void setWhereUsed(QrType whereUsed) {
        this.whereUsed = whereUsed;
    }

    public Boolean getUsedForCampaign() {
        return usedForCampaign;
    }

    public void setUsedForCampaign(Boolean usedForCampaign) {
        this.usedForCampaign = usedForCampaign;
    }

    public DjpPassLocationsModel getWhichLocation() {
        return whichLocation;
    }

    public void setWhichLocation(DjpPassLocationsModel whichLocation) {
        this.whichLocation = whichLocation;
    }
}
