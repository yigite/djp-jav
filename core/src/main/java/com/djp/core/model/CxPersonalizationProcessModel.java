package com.djp.core.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "CxPersonalizationProcess")
@PrimaryKeyJoinColumn(name = "id")
public class CxPersonalizationProcessModel extends BusinessProcessModel{

    private String key;

    @ManyToMany(mappedBy = "cxPersonalizationProcesses", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<CatalogVersionModel> catalogVersions;

    public CxPersonalizationProcessModel() {
        super();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Set<CatalogVersionModel> getCatalogVersions() {
        return catalogVersions;
    }

    public void setCatalogVersions(Set<CatalogVersionModel> catalogVersions) {
        this.catalogVersions = catalogVersions;
    }
}
