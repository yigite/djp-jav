package com.djp.core.model.enumeration;

public enum  IDType {

    DUNS("DUNS"),
    ILN("ILN"),
    SUPPLIER_SPECIFIC("SUPPLIER_SPECIFIC"),
    BUYER_SPECIFIC("BUYER_SPECIFIC"),
    UNSPECIFIED("UNSPECIFIED");

    private final String code;

    IDType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
