package com.djp.core.model;

public class EmployeeModel extends UserModel{
    public static final String _TYPECODE = "Employee";

    public EmployeeModel()
    {
        super();
    }


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
