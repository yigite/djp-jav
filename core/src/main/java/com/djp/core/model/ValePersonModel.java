package com.djp.core.model;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ValePerson")
public class ValePersonModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Integer oid;
    private String name;
    private String surname;
    private String phone1;
    private String phone2;
    private String phone3;

    @OneToMany(mappedBy = "valePerson", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.DETACH})
    private Set<PlateMovementWsModel> plateMovementWsModels;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone3() {
        return phone3;
    }

    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }

    public Set<PlateMovementWsModel> getPlateMovementWsModels() {
        return plateMovementWsModels;
    }

    public void setPlateMovementWsModels(Set<PlateMovementWsModel> plateMovementWsModels) {
        this.plateMovementWsModels = plateMovementWsModels;
    }
}
