package com.djp.core.model;

import com.djp.core.enums.SAPOrderStatus;

import jakarta.persistence.*;
import java.util.Set;

@Entity
@Table(name = "SAPOrder")
public class SAPOrderModel extends ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private OrderModel order;

    @OneToMany(mappedBy = "sapOrder", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ConsignmentModel> consignments;

    private SAPOrderStatus sapOrderStatus;

    //private String orderPOS;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public Set<ConsignmentModel> getConsignments() {
        return consignments;
    }

    public void setConsignments(Set<ConsignmentModel> consignments) {
        this.consignments = consignments;
    }

    public SAPOrderStatus getSapOrderStatus() {
        return sapOrderStatus;
    }

    public void setSapOrderStatus(SAPOrderStatus sapOrderStatus) {
        this.sapOrderStatus = sapOrderStatus;
    }
}
