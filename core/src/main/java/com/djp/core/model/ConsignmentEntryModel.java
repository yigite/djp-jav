package com.djp.core.model;

import com.djp.core.enums.ConsignmentEntryStatus;

import jakarta.persistence.*;

@Entity
@Table(name = "ConsignmentEntry")
public class ConsignmentEntryModel extends ItemModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long quantity;
    private Long shippedQuantity;
    private Integer sapOrderEntryRowNumber;
    private ConsignmentEntryStatus status;

    @ManyToOne
    @JoinColumn
    private AbstractOrderEntryModel orderEntry;

    @ManyToOne
    @JoinColumn
    private ConsignmentModel consignment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getShippedQuantity() {
        return shippedQuantity;
    }

    public void setShippedQuantity(Long shippedQuantity) {
        this.shippedQuantity = shippedQuantity;
    }

    public Integer getSapOrderEntryRowNumber() {
        return sapOrderEntryRowNumber;
    }

    public void setSapOrderEntryRowNumber(Integer sapOrderEntryRowNumber) {
        this.sapOrderEntryRowNumber = sapOrderEntryRowNumber;
    }

    public ConsignmentEntryStatus getStatus() {
        return status;
    }

    public void setStatus(ConsignmentEntryStatus status) {
        this.status = status;
    }

    public AbstractOrderEntryModel getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(AbstractOrderEntryModel orderEntry) {
        this.orderEntry = orderEntry;
    }

    public ConsignmentModel getConsignment() {
        return consignment;
    }

    public void setConsignment(ConsignmentModel consignment) {
        this.consignment = consignment;
    }
}
