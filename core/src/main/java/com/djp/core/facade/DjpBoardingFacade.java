package com.djp.core.facade;

import com.djp.data.DjpBoardingPassData;
import com.djp.request.ExtensionOfTimeForBoardingPassRequest;
import com.djp.request.UpdateBoardingPassRequest;
import com.djp.response.BoardingPassResponse;
import com.djp.response.FioriStatusResponse;

import java.text.ParseException;

import java.util.List;

public interface DjpBoardingFacade {

    void extensionOfTimeBoardingPass(ExtensionOfTimeForBoardingPassRequest request, BoardingPassResponse response);

    boolean isChildBoardingPass(String pnrCode,Boolean isManual);

    List<DjpBoardingPassData> getBoardingPasses(String date,String endDateStr,Boolean isNewSale,String whereUsed,String locationID);

    FioriStatusResponse updateBoardingPass(UpdateBoardingPassRequest updateBoardingPassRequest) throws ParseException;
}
