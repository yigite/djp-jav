package com.djp.core.facade.impl;

import com.djp.core.enums.TourniquetBoardingStatus;
import com.djp.core.facade.DjpBoardingFacade;
import com.djp.core.model.*;
import com.djp.data.DjpBoardingPassData;
import com.djp.modelservice.ModelService;
import com.djp.modelservice.util.DateUtil;
import com.djp.populator.Populator;
import com.djp.request.ExtensionOfTimeForBoardingPassRequest;
import com.djp.request.UpdateBoardingPassRequest;
import com.djp.response.BoardingPassResponse;
import com.djp.response.FioriStatusResponse;
import com.djp.service.BaseStoreService;
import com.djp.service.DjpBoardingService;
import com.djp.service.DjpTokenService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import jakarta.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.text.ParseException;
import java.util.Objects;

public class DjpBoardingFacadeImpl implements DjpBoardingFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DjpBoardingFacadeImpl.class);

    @Resource
    private DjpBoardingService djpBoardingService;

    @Resource
    private DjpTokenService djpTokenService;

    @Resource
    private Populator<DjpBoardingPassModel, DjpBoardingPassData> djpBoardingPassPopulator;

    @Resource
    private Populator<DjpAccessBoardingModel, DjpBoardingPassData> djpAccBoardingPassPopulator;

    @Resource
    BaseStoreService baseStoreService;

    @Resource
    ModelService modelService;

    @Override
    public void extensionOfTimeBoardingPass(ExtensionOfTimeForBoardingPassRequest request, BoardingPassResponse response) {
        FioriStatusResponse result = new FioriStatusResponse();
        try {
            if (StringUtils.isNotBlank(request.getPnrCode())){
                if (StringUtils.isNotBlank(request.getBoardingType())){
                    if(CollectionUtils.isNotEmpty(request.getExtensionTimeList())){
                        DjpBoardingPassModel djpBoardingPassModel = djpBoardingService.getCouldReadDjpBoardingPassWithouthDayLimit(request.getPnrCode(), request.getBoardingType());
                        DjpAccessBoardingModel djpAccessBoardingModel = null;
                        if (Objects.isNull(djpBoardingPassModel)) {
                            djpAccessBoardingModel = djpBoardingService.getCouldReadDjpAccessBoardingWithoutDateLimit(request.getPnrCode(),request.getBoardingType());
                            if (Objects.nonNull(djpAccessBoardingModel)){
                                updateExtensionForBoarding(djpBoardingPassModel,djpAccessBoardingModel,request);
                                result.setMessage("Success extension of time");
                                result.setType("Success");
                                response.setServiceResult(result);
                            } else {
                                result.setMessage("PnrCode not found.");
                                result.setType("Error");
                                response.setServiceResult(result);
                            }
                        } else {
                            updateExtensionForBoarding(djpBoardingPassModel,djpAccessBoardingModel,request);
                            result.setMessage("Success extension of time");
                            result.setType("Success");
                            response.setServiceResult(result);
                        }
                    } else {
                        result.setMessage("Time list is mandatory.");
                        result.setType("Error");
                        response.setServiceResult(result);
                    }
                } else {
                    result.setMessage("BoardingType list is mandatory.");
                    result.setType("Error");
                    response.setServiceResult(result);
                }
            } else {
                result.setMessage("PnrCode is mandatory.");
                result.setType("Error");
                response.setServiceResult(result);
            }
        } catch (Exception e){
            LOG.error("extensionOfTimeBoardingPass Error :", e);
            result.setMessage("Error");
            result.setType("Error");
            response.setServiceResult(result);
        }
    }

    @Override
    public boolean isChildBoardingPass(String pnrCode, Boolean isManual) {
        if(isManual){
            return pnrCode.startsWith("NS") || pnrCode.startsWith("INF");
        }else {
            if(StringUtils.isNotBlank(pnrCode)){
                String seatNumber = pnrCode.substring(49, 52);
                return seatNumber.startsWith("NS") || seatNumber.startsWith("INF");
            }
        }
        return false;
    }
    @Override
    public FioriStatusResponse updateBoardingPass(UpdateBoardingPassRequest updateBoardingPassRequest) throws ParseException {
        FioriStatusResponse response = new FioriStatusResponse();
        if(StringUtils.isNotBlank(updateBoardingPassRequest.getTokenKey())){
            DjpBoardingPassModel boardingPassModel = djpBoardingService.getDjpBpByQrOrFiori(updateBoardingPassRequest,null);
            if(Objects.isNull(boardingPassModel)){
                response.setType("E");
                response.setMessage("BOARDING PASS DJP DE BULUNMAMAKTADIR");
                return response;
            }else {
                if(!updateBoardingPassRequest.getTokenKey().equals(boardingPassModel.getToken().getCode())){
                    response.setType("E");
                    response.setMessage("BOARDING PASS DJP DE BULUNMAMAKTADIR");
                    return response;
                }else {
                    djpBoardingService.checkIfValidBoardingPasses(response,updateBoardingPassRequest);
                    if("E".equals(response.getType())){
                        return response;
                    }else {
                        //update
                        return djpBoardingService.updateBoardingPasses(response,updateBoardingPassRequest,boardingPassModel,null);
                    }
                }
            }
        }else{
            DjpAccessBoardingModel accessBoardingModel = djpBoardingService.findDjpAccessBoarding(updateBoardingPassRequest,null);
            if(Objects.nonNull(accessBoardingModel)){
                djpBoardingService.checkIfValidBoardingPasses(response,updateBoardingPassRequest);
                if("E".equals(response.getType())){
                    return response;
                }else {
                    //update
                    return djpBoardingService.updateBoardingPasses(response,updateBoardingPassRequest,null,accessBoardingModel);
                }
            }else {
                response.setType("E");
                response.setMessage("BOARDING PASS DJP DE BULUNMAMAKTADIR");
                return response;
            }
        }
    }

    @Override
    public List<DjpBoardingPassData> getBoardingPasses(String date, String endDateStr, Boolean isNewSale, String whereUsed, String locationID) {
        List<DjpBoardingPassData> passDataList = new ArrayList<>();
        Date startDate = DateUtil.convertStartDate(date);
        Date endDate;
        if (StringUtils.isBlank(endDateStr) || date.equals(endDateStr)){
            endDate = DateUtil.addOneDay(startDate);
        }else{
            endDate = DateUtil.convertStartDate(endDateStr);
            endDate = DateUtil.addOneDay(endDate);
        }
        DjpPassLocationsModel djpPassLocation = djpTokenService.getDjpPassLocationByLocationID(locationID);
        List<Object> dataList = djpBoardingService.getAllDjpBoardingSellList(startDate, endDate, isNewSale,whereUsed,djpPassLocation);

        dataList.forEach(object -> {
            DjpBoardingPassData passData = new DjpBoardingPassData();
            if (object instanceof DjpBoardingPassModel){
                passData =  djpBoardingPassPopulator.populate((DjpBoardingPassModel) object);
            } else if (object instanceof DjpAccessBoardingModel) {
                passData = djpAccBoardingPassPopulator.populate((DjpAccessBoardingModel) object);
            }
            try {
                setContractedCompanyInfo(object, passData);
            } catch (InvocationTargetException | IllegalAccessException e) {
                LOG.error("getBoardingPasses has an error",e);
            }
            passDataList.add(passData);
        });

        return CollectionUtils.isEmpty(passDataList) ? Collections.emptyList() : passDataList;
    }

    private void setContractedCompanyInfo(Object passModel, DjpBoardingPassData passData) throws InvocationTargetException, IllegalAccessException {
        Method getContractedId = passModel instanceof DjpBoardingPassModel? ReflectionUtils.findMethod(DjpBoardingPassModel.class, "getContractID"):
                passModel instanceof DjpAccessBoardingModel? ReflectionUtils.findMethod(DjpAccessBoardingModel.class, "getContractID"): null ;
        if(getContractedId != null){
            String contractId = (String) getContractedId.invoke(passModel);
            if(StringUtils.isNotBlank(contractId)){
                ContractedCompaniesModel contractedCompaniesModel = djpBoardingService.getContractedCompaniesViaPass(contractId);
                if(contractedCompaniesModel != null){
                    passData.setContractedCompanyId(contractedCompaniesModel.getCompanyId());
                    passData.setContractedCompanyName(contractedCompaniesModel.getCompanyName());
                }
            }
        }
    }

    private void updateExtensionForBoarding(DjpBoardingPassModel djpBoardingPassModel, DjpAccessBoardingModel accessBoardingModel, ExtensionOfTimeForBoardingPassRequest extensionRequest){
        BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
        if (Objects.nonNull(baseStoreModel)){
            Integer stayTime = baseStoreModel.getIndividualLengthOfStayHour();
            extensionRequest.getExtensionTimeList().forEach(extensionTimeData -> {
                DjpBoardingPassUsingInfoModel data = new DjpBoardingPassUsingInfoModel();
                data.setCompanyOrIndividual(Boolean.FALSE);
                data.setIndexUsingData(1);
                data.setScheduledOutStartDate(DateUtil.parseExtensionDate(extensionTimeData.getStartDate()));
                data.setScheduledOutEndDate(DateUtil.addHoursToDate(DateUtil.parseExtensionDate(extensionTimeData.getEndDate()), stayTime));
                data.setIsTimeOk(new Date().after(data.getScheduledOutEndDate()));
                data.setStatus(new Date().before(data.getScheduledOutEndDate()) ? TourniquetBoardingStatus.USE : TourniquetBoardingStatus.COMPLETE);
                data.setStayHourLimit(stayTime);
                if(Objects.nonNull(djpBoardingPassModel)){
                    data.setBoardingPass(djpBoardingPassModel);
                    modelService.saveOrUpdate(data);
                    if (data.getIsTimeOk().equals(Boolean.FALSE)){
                        DjpBoardingPassUsingInfoModel currentUsingData = djpBoardingPassModel.getBoardingPassUsingData();
                        currentUsingData.setIsTimeOk(Boolean.TRUE);
                        currentUsingData.setStatus(TourniquetBoardingStatus.COMPLETE);
                        data.setScheduledOutEndDate(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(DateUtil.parseExtensionDate(extensionTimeData.getEndDate()), stayTime)));

                        modelService.saveOrUpdate(currentUsingData);
                        modelService.saveOrUpdate(data);
                        djpBoardingPassModel.setBoardingPassUsingData(data);
                        modelService.saveOrUpdate(djpBoardingPassModel);
                    }
                }
                if (Objects.nonNull(accessBoardingModel)){
                    if (StringUtils.isNotBlank(extensionTimeData.getCode())){
                        List<String> modifableList = CollectionUtils.isEmpty(accessBoardingModel.getRefNumberList()) ? new ArrayList<>() : accessBoardingModel.getRefNumberList();
                        modifableList.add(extensionTimeData.getCode());
                        accessBoardingModel.setRefNumberList(modifableList);
                        modelService.saveOrUpdate(accessBoardingModel);
                    }
                    data.setAccBoardingPass(accessBoardingModel);
                    modelService.saveOrUpdate(data);
                    if (data.getIsTimeOk().equals(Boolean.FALSE)){
                        DjpBoardingPassUsingInfoModel currentUsingData = accessBoardingModel.getBoardingPassUsingData();
                        currentUsingData.setIsTimeOk(Boolean.TRUE);
                        currentUsingData.setStatus(TourniquetBoardingStatus.COMPLETE);
                        data.setScheduledOutEndDate(DateUtil.addMinutesFifteenBufferToDate(DateUtil.addHoursToDate(DateUtil.parseExtensionDate(extensionTimeData.getEndDate()), stayTime)));
                        modelService.saveOrUpdate(currentUsingData);
                        modelService.saveOrUpdate(data);
                        accessBoardingModel.setBoardingPassUsingData(data);
                        modelService.saveOrUpdate(accessBoardingModel);
                    }
                }
            });
        }
    }
}
