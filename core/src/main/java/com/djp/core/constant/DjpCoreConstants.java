package com.djp.core.constant;

import java.util.Locale;

public final class DjpCoreConstants
{


    private DjpCoreConstants()
    {
        //empty
    }

    public static final Locale trLocale = Locale.forLanguageTag(DjpCoreConstants.TURKISH_LANGUAGE_ISO_CODE);

    // implement here constants used by this extension
    public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
    public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
    public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
    public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
    public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
    public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
    public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
    public static final String SEND_SMS_CONSENT_TEMPLATE = "SendSMS_consentTemplate";
    public static final String SEND_EMAIL_CONSENT_TEMPLATE = "SendEMAIL_consentTemplate";
    public static final String SEND_CALL_CONSENT_TEMPLATE = "SendCALL_consentTemplate";
    public static final String SEND_WHATSAPP_CONSENT_TEMPLATE = "SendWHATSAPP_consentTemplate";

    public static final String DJP_STORE = "djpstore";
    public static final String ANONYMOUS = "anonymous";
    public static final String PREMIUM_SERVICE_CATEGORY = "01";
    public static final String MASS_PASSENGER_SERVICE_CATEGORY = "04";
    public static final String PREMIUM_PACKAGE_CATEGORY = "02";
    public static final String PREMIUM_DAILY_PACKAGE_CATEGORY = "0201";
    public static final String PREMIUM_CORPORATE_PACKAGE_CATEGORY = "0203";
    public static final String FAST_TRACK_CATEGORY = "010501";
    public static final String PREMIUM_DAILY_DOMESTIC_CATEGORY = "0202";
    public static final String MEET_CATEGORY = "0101";
    public static final String MEET_AND_GREET_CATEGORY = "010102";
    public static final String MEET_AND_GREET_ARRIVAL = "Arrival";
    public static final String MEET_AND_GREET_DEPARTURE = "Departure";
    public static final String CAR_PARKING_CODE = "3000";
    public static final String MEET_AND_GREET_CODE = "7000";
    public static final String DJP_MEETING_LOUNGE_CATEGORY = "010801";
    public static final String CAR_PARK_CATEGORY = "010401";
    public static final String DJP_SLEEPOD_CATEGORY = "010901";
    public static final String MEETING_LOUNGE_TOURISM_AGENCY = "SD008104";
    public static final String AND = " and ";
    public static final Double ZERO_DOUBLE = 0D;

    public static final String DJP_PERSONNEL_GROUP_CODE = "djppersonnel";
    public static final String CUSTOMER_GROUP_CODE = "customergroup";
    public static final String DJP_UID_REGEX = "^[+][0-9]*$";
    public static final String DJP_MAIL_REGEX = "^[a-z0-9+_.-]+@[a-z0-9.-]+$";
    public static final String OK = "OK";
    public static final String NOK = "NOK";
    public static final String SD = "SD";
    public static final String DJP_BLOCKED_PHONE_NUMBER_PREFIX = "+99";
    public static final String SUCCESS="Success";
    public static final String SN_TITLE_CODE="Sn";
    public static final String IS_FIRST_NAME_READONLY="isFirstNameReadonly";
    public static final String IS_LAST_NAME_READONLY="isLastNameReadonly";
    public static final String MAIL="mail";
    public static final String SMS="sms";

    public static final String BUSINESS_PLUS = "SD001095";
    public static final String BUSINESS_EXTRA = "SD001096";
    public static final String BUSINESS_PREMIUM = "SD001097";

    public static final String DAILY_PASS_CODE = "SD001098";
    public static final String DAILY_PASS_KIDS_CODE = "SD001099";
    public static final String DAILY_DOMESTIC_CODE = "SD001094";
    public static final String IS_PRODUCT_ON_CART_UPGRADED="isProductOnCartUpgraded";
    public static final String DJP_PASS_DAILY_KIDS_CODE="SD001099";


    public static class MeetingLounge{
        public static String REZERVATION="01";
        public static String TOURISM="02";
    }

    public static class VirtualPaymentResponse{
        public static String SUCCESS="success";
    }

    public static class PackageOrProduct{
        public static String PACKAGE="PACKAGE";
        public static String PRODUCT="PRODUCT";
    }

    public static final String TURKISH_NATIONALITY_CODE = "182";
    public static final String TURKISH_LANGUAGE_ISO_CODE = "tr";
    public static final String ENGLISH_LANGUAGE_ISO_CODE = "en";

    public static final String ADDITIONAL_SERVICES_CATEGORY = "03";

    public static final String TOKEN_TYPE_FOR_SOME_BODY = "FOR_SOME_BODY";
    public static final String TOKEN_TYPE_FOR_SINGLE_CUSTOM_CODE = "SINGLE_CUSTOM_CODE";
    public static final String FOREIGN = "FOREIGN";
    public static final String PERSONAL = "personal";
    public static final String COMPANY = "company";

    public static class Updkz{
        public static String INSERT="I";
        public static String DELETE="D";
        public static String CAMPAIGN="1";
        public static String CUSTOM_SINGLE_CAMPAIGN="2";
    }

    public static class RemainingReturns{
        public static String OK="OK";
        public static String SERVICE="service";
        public static String PACKAGE="package";
        public static String NOK="NOK";
        public static String DIVISION_MARK="/";
        public static String PACKAGE_MAIL="PACKAGE";
        public static String SERVICE_MAIL="SERVICE";
        public static String PAST_STATUS="past";
        public static String GIFT_STATUS="gift";
        public static String CURRENT_STATUS="current";
    }

    public static class DjpVariantFeature{
        public static final String ZMMDJP2_STARTWITH="ZMMDJP_002";
        public static final String ADULT="ZMMDJP_002_01";
        public static final String CHILD="ZMMDJP_002_02";
        public static final String BABY="ZMMDJP_002_03";
        public static final String CITY_TRANSFER="SD001902";
        public static final String NANNY_SERVICE="SD001903";
        public static final String AVAILABLE_QUALIFER_CODE = "zmmdjp_001";
        public static final String PASSENGER_QUALIFER_CODE = "zmmdjp_002";
        public static final String SERVICE_TYPE_QUALIFER_CODE = "zmmdjp_003";
    }
    public static class Gtm{
        public static final String ASM="asm";
        public static final String REQUEST_URL_WITHOUT_WWW="https://djppass.com";
        public static final String REQUEST_URL_WITHOUT_LANG_PATH="https://www.djppass.com";
        public static final String REQUEST_URL_WITHOUT_LANG_PATH_END_DIVISION="https://www.djppass.com/";
        public static final String REQUEST_URL_WITH_WWW="https://www.djppass.com";
        public static final String DIVISION="/";
        public static final String QUERY_MARK="?";
        public static final String GOOGLE_TAG_MANAGER="googletagmanager";
        public static final String SITE_URL="project.website.djpstore.https";
        public static final String ROBOTS="/robots.txt";
        public static final String ID_SITE="idsite=";
    }

    public static class DjpPassLocations{
        public static String DJP = "1";
        public static String DALAMAN = "2";
    }
    public static class SpecialCharacters{
        public static String QUERY_MARK="?";
        public static String DIVISION_MARK="/";
        public static String DASH_MARK="-";
        public static String UNDERSCORES="_";
        public static String PERCENT="%";
        public static String COLON=":";
        public static String COMMA=",";
        public static String DOT=".";
    }
    public static class ExternalCompany{
        public static Integer PACKAGE_MAX_QUANTITY_ONE_ORDER= 1;
        public static Integer ZERO= 0;
        public static String QR="qr";
        public static String ENABLED="enabled";
        public static String DISABLED_REFUND="refunds are temporarily closed";
        public static String CANNOT_CONTACT_INTERNAL_SYSTEM="refund failed, internal server error";
        public static String PROCESS_TYPE="processType";
        public static String START_DATE="startDate";
        public static String END_DATE="endDate";
        public static String CREATE_ORDER_CHECK_TYPE="1";
        public static String QR_CHECK_TYPE="2";
        public static String REFUND_ORDER_CHECK_TYPE="3";
        public static String BAGAJ_SARMA_CODE="8030";
        public static String FAST_TRACK_CODE="4000";
        public static String SUCCESS="S";
        public static String BLANK=" ";
        public static String OR="or";
        public static String FOR=" for ";
        public static String BILESIK_ODEME="bilesikodeme";
        public static String BILESIK_ODEME_TYPE="NAKIT";
        public static String TYPE_ORDER="C";
        public static String TYPE_REFUND="H";
        public static String BILESIK_ODEME_APP="bilesikodeme_app";
        public static String ISKYTRIP="iskytrip";
        public static String TRIP="trip";
        public static String DISCONTI="disconti";
        public static String HIGHPASS="highpass";
        public static String DRAGONPASS="dragonpass";
        public static String TRIPBENDER="tripbender";
        public static String ISKYTRIP_APP="iskytrip_app";
        public static String TRIP_APP="trip_app";
        public static String AGENTRU_APP="agentru_app";
        public static String AVIACENTER_APP="aviacenter_app";
        public static String HIGHPASS_APP="highpass_app";
        public static String DRAGONPASS_APP="dragonpass_app";
        public static String NUEVO_APP="nuevo_app";
        public static String DISCONTI_APP="disconti_app";
        public static String QR_TYPE_FOR_DJP_EXTERNAL_CUSTOMER = "FOR_DJP_EXTERNAL_CUSTOMER";
        public static String NAME = " name ";
        public static String LAST_NAME = " lastName ";
        public static String E_MAIL = " email ";
        public static String PHONE_NUMBER = " phoneNumber ";
        public static String BIRTHDATE = " birthDate ";
        public static String CONTACT_NAME = " contactName ";
        public static String CONTACT_PHONE = " contactPhone ";
        public static String ARRIVAL_CUSTOMER_DATE = " arrivalCustomerDate ";
        public static String ARRIVAL_FLIGHT_DATE = " arrivalFlightDate ";
        public static String ARRIVAL_FLIGHT_CODE = " arrivalFlightCode ";
        public static String DEPARTURE_FLIGHT_DATE = " departureFlightDate ";
        public static String DEPARTURE_FLIGHT_CODE = " departureFlightCode ";
        public static String GUEST_INFO = " guestInfo ";
        public static String GUEST_INFO_SIZE_ERROR = "Guest information must be entered as much as the number of products";
        public static Integer SIXTY_FIVE = 65;
        public static Integer CHILD_MAX_AGE = 12;
        public static class InvoiceInfo{
            public static String IS_INVOICE_PERSONAL="isInvoicePersonal";
            public static String IS_TURKISH_CITIZEN="isTurkishCitizen";
            public static String IDENTIFIER_NUMBER="identifierNumber";
            public static String PASSPORT_NUMBER="passportNumber";
            public static String NAME="name";
            public static String LAST_NAME="lastName";
            public static String EMAIL="email";
            public static String ADDRESS="address";
            public static String PHONE_NUMBER="phoneNumber";
            public static String COMPANY_NAME="companyName";
            public static String TAX_NUMBER="taxNumber";
            public static String TAX_OFFICE="taxOffice";
        }
        public static class AvailableFeature{
            public static String DOMESTIC="DOMESTIC";
            public static String INTERNATIONAL="INTERNATIONAL";
        }
        public static class ServiceFloorFeature{
            public static String DEPARTURE="DEPARTURE";
            public static String ARRIVAL="ARRIVAL";
            public static String TRANSFER="TRANSFER";
        }


    }
    public static class SapBillingDetail{
        public static String PARAMETER="PARAMETER";
        public static String BILLING="BILLING";
        public static String TYPE="TYPE";
        public static String ID="ID";
        public static String NUMBER="NUMBER";
        public static String MESSAGE="MESSAGE";
        public static String MESSAGE_V1="MESSAGE_V1";
        public static String MESSAGE_V2="MESSAGE_V2";
        public static String MESSAGE_V3="MESSAGE_V3";
        public static String MESSAGE_V4="MESSAGE_V4";
    }

    public static class StatusForFlightHours{
        public static String DIFF_24_HOURS="checkIfFlightLessDiff24Hours";
        public static String MAX_24_HOURS="checkIfFlightMaxDiff24Hours";
        public static String ARRIVAL_MUST_BE_BEFORE_DEPARTURE="arrivalMustBeBeforeDeparture";
        public static String OP="OP";
        public static String DOMESTIC="DOMESTIC";
        public static String INTERNATIONAL="INTERNATIONAL";
        public static String TRANSFER="TRANSFER";
        public static String PNR_CODE="PNR";
        public static String DEVICE_ID="DeviceId";
        public static String STATUS="Status";
        public static String PASSED="IsPassed";
        public static String OPERATIONAL_STATUS="OPERATIONAL_STATUS";
        public static String INTERNATIONAL_STATUS="INTERNATIONAL_STATUS";
        public static String FAST_TRACK_STARTWITH="FAST_TRACK";
        public static String MIDDLE_WARE_ERROR_STATUS="-2";
        public static String NOT_USED_FOR_DOMESTIC="NOT_USED_FOR_DOMESTIC";
        public static String NOT_USED_FOR_INTERNATIONAL="NOT_USED_FOR_INTERNATIONAL";
        public static String NOT_AVAILABLE_FLIGHT_IN_DJP="NOT_AVAILABLE_FLIGHT_IN_DJP";
        public static String M1="M1";
    }

    public static class ParkingAndValet{
        public static Integer ABONMANLIK_ID=38;
    }

    public static class Payment{
        public static String TRY="TRY";
        public static String EUR="EUR";
        public static Double ONE_DOUBLE= 1D;
        public static String BILL_SERIAL_NO="1111111";
        public static String DOCUMENT_TYPE_RECEIPT="00";
        public static String DOCUMENT_TYPE_INVOICE="09";
        public static String DOCUMENT_TYPE_RECEIPT_MAP_ID="1";
        public static String DEPARTMENT_INDEX="00";
        public static String PROCESS_TYPE="01";
        public static String PAYMENT_TYPE="02";
        public static String AMOUNT="0";
        public static String EXCHANGE_CODE_INDEX="00";
        public static String INSTALLMENT_COUNT="00";
        public static String SUCCESS="200";
        public static String ERROR="400";
        public static String ZERO="0";
        public static class StartReceiptDataModel{
            public static String ID="StartReceiptDataModel";
            public static String DOCUMENT_TYPE="DocumentType";
            public static String TCKN="TCKN";
            public static String VKN="VKN";
            public static String BILL_SERIAL="BillSerialNo";
        }
        public static class SaleItemDataModel{
            public static String ID="SaleItemDataModel";
            public static String DEPARTMENT_INDEX="DepartmentIndex";
            public static String TRANS_ITEMS="TransItems";
        }
        public static class TransItem{
            public static String MATERIAL_NAME="MaterialName";
            public static String FINAL_AMOUNT="FinalAmount";
        }
        public static class SalePaymentDataModel{
            public static String ID="SalePaymentDataModel";
            public static String PROCESS_TYPE="ProcessTypeCode";
            public static String PAYMENT_TYPE="PaymentTypeCode";
            public static String AMOUNT="Amount";
            public static String EXCHANGE="ExchangeCodeIndex";
            public static String INSTALLMENT="InstallmentCnt";
        }
    }
    public static class WebServiceParameters{
        public static final Integer TRY_COUNT=3;
        public static final String DJP = "DJP";
    }
    public static class Register{
        public static final String DEVICE="device";
        public static final String MOBILE_APP = "mobileapp";
        public static final String MOBILE = "Mobile";
        public static final String WEB = "Web";
        public static final String IPHONE = "iphone";
        public static final String ANDROID = "android";
    }
    public static class Sms{
        public static final String ASM="ASM";
        public static final String REGISTER = "REGISTER";
        public static final String REGISTER_SUCCESS = "REGISTER";
        public static final String REGISTER_MOBILE = "REGISTER_MOBILE";
        public static final String RESEND_REGISTER_MOBILE = "RESEND REGISTER MOBILE";
        public static final String FORGOTTEN = "FORGOTTEN";
        public static final String FORGOTTEN_MOBILE = "FORGOTTEN";
        public static final String FORGOTTEN_MOBILE_SMS = "FORGOTTEN SMS";
        public static final String FORGOTTEN_MOBILE_MAIL = "FORGOTTEN MAIL";
        public static final String FORGOTTEN_WEB_SMS = "FORGOTTEN WEB SMS";
        public static final String RESEND_FORGOTTEN_MOBILE_SMS = "RESEND FORGOTTEN MOBILE SMS";
        public static final String RESEND_FORGOTTEN_MOBILE_MAIL = "RESEND FORGOTTEN MOBILE MAIL";
        public static final String QRFORFIORI = "QRFORFIORI";
        public static final String OTP = "OTP";
        public static final String CUSTOM = "CUSTOM";
        public static final String VALE = "VALE";
        public static final String FORSOMEBODY = "FORSOMEBODY";
        public static final String WELCOME_TO_DJP = "WELCOMETODJP";
    }


    public static class SiteMap{
        public static final String LOGO="djp_logo.png";
        public static final String DJP_CONTENT_CATALOG = "djpContentCatalog";
        public static final String ONLINE_CATALOG = "Online";
        public static final String CONTEXT_BEAN_ID = "siteMapContext";
    }
    public static class DjpSapReturns {
        public static String ERP = "ERP";
        public static String KYP = "KYP";
        public static String WP = "WP";
        public static String SUCCESS = "S";
    }

    public static class Bp{
        public static final String BLANK_CONTRACT_ID="400";
        public static final String NOT_VALID_CONTRACT_ID="isNotValidContractIDForAirlineCode";
        public static final String PASSENGER_AND_GUEST_SAME_BP="hasSameBp";
        public static final String USED_PASSENGER_BP="hasBp";
        public static final String USED_GUEST_BP="hasGuestBp";
        public static final String GUEST_AND_GUEST_SAME_BP="hasSameGuestBp";
        public static final String PASSENGER_AND_GUEST_DIFFERENT_COMPANY="nonCompanyBoardingPass";
        public static final String SUCCESS="200";
        public static final String ERROR="202";
    }
    public static class ExceptionMessage{
        public static final String USER_LOGIN_BLOCKED = "User Login Is Blocked";
        public static final String USER_IS_DISABLED = "User is disabled";
    }

    public static class DjpCampaign{
        public static String DJP_PASS_CAMPAIGN_TITLE="DJP PASS";
        public static String DJP_PASS_CAMPAIGN_ID="03";
    }

}

