package com.djp.core.constant;


public final class DjpCommerceservicesConstants
{

    private DjpCommerceservicesConstants()
    {
        //empty to avoid instantiating this constant class
    }



    public static final String ZERO = "0";
    public static final String COMMA = ",";

    public static final String LOUNGE_INTERNATIONAL_POINT_CODE = "1001";

    public static final String MEET_CATEGORY = "0101";
    public static final String ADDITIONAL_SERVICES_CATEGORY = "03";
    public static final String PREMIUM_PACKAGE_CATEGORY = "02";
    public static final String PREMIUM_DAILY_PACKAGE_CATEGORY = "0201";
    public static final String PREMIUM_DAILY_DOMESTIC_CATEGORY = "0202";
    public static final String CAR_PARK_CATEGORY = "010401";
    public static final String LOUNGE_CATEGORY = "010201";
    public static final String MEET_AND_GREET_CATEGORY = "010102";
    public static final String FAST_TRACK_CATEGORY = "010501";
    public static final String MEETING_LOUNGE_CATEGORY = "010801";
    public static final String SLEEPOD_CATEGORY = "010901";
    public static final String BAGAJ_SARMA_CATEGORY = "018030";

    public static final String BUSINESS_PLUS = "SD001095";
    public static final String BUSINESS_EXTRA = "SD001096";
    public static final String BUSINESS_PREMIUM = "SD001097";

    public static final String MEET_AND_ASSIST = "5000";
    public static final String  MEET_AND_GREET = "7000";
    public static final String MEET_AND_GREET_VIP = "8000";

    public static final String MEETING_LOUNGE_TOURISM = "SD008104";

    public static final String ZMMDJP1_STARTWITH = "ZMMDJP_001";
    public static final String ZMMDJP2_STARTWITH = "ZMMDJP_002";
    public static final String ZMMDJP3_STARTWITH = "ZMMDJP_003";

    public static final String DJP_DOMESTIC_FEATURE_CODE = "ZMMDJP_001_01";
    public static final String DJP_INTERNATIONAL_FEATURE_CODE = "ZMMDJP_001_02";

    public static final String DJP_BABY_FEATURE_CODE = "ZMMDJP_002_03";

    public static final String DJP_ARRIVAL_FEATURE_CODE = "ZMMDJP_003_01";
    public static final String DJP_DEPARTURE_FEATURE_CODE = "ZMMDJP_003_02";
    public static final String DJP_TRANSFER_FEATURE_CODE = "ZMMDJP_003_03";

    public static final String DJP_DEFAULT_FEATURE_CODE = "ZMMDJP_004_001";
    public static final String DJP_SPECIAL_FEATURE_CODE = "ZMMDJP_004_006";

    public static final String FAST_TRACK_STARTWITH = "FAST_TRACK";
    public static final String FAST_TRACK_TRANSFER_STARTWITH = "FAST_TRACK_TRANSFER";
    public static final String BUGGY_INTERNATIONAL_STARTWITH = "BUGGY_INTERNATIONAL";
    public static final String BUGGY_DOMESTIC_STARTWITH = "BUGGY_DOMESTIC";
    public static final String FAST_TRACK_DEPARTURE_STARTWITH = "FAST_TRACK_DEPARTURE";
    public static final String FAST_TRACK_ARRIVAL_STARTWITH = "FAST_TRACK_ARRIVAL";
    public static final String BAVUL_KAPLAMA_STARTWITH = "BAVUL_KAPLAMA";
    public static final String EMANET_DOLABI_STARTWITH = "EMANET_DOLABI";

    public static final String QR_FOR_CARD = "QR_FOR_CARD";

    public static final String DJP_IATA = "IST";

    public static final String FAST_TRACK_CODE = "20";
    public static final String BUGGY_CODE = "30";
    public static final String CARPARKING_CODE = "40";

    public static final String BUSINESS_BOARDING_CLASS_CODE = "Business";

    public static final String ALL = "ALL";

    public static class PackageOrProduct{
        public static String PACKAGE="PACKAGE";
        public static String PRODUCT="PRODUCT";
    }


    public static class Gb{
        public static String CLIENT_NAME_ID="ClientName";
        public static String TRANSACTION_ID="TransactionId";
        public static String PROCESS_CODE_ID="ProcessCode";
        public static String CULTURE_INFO_ID="CultureInfo";
        public static String PARAMETERS_ID="Parameters";
        public static String PROCESS_CODE="DJP_GenericStartFlow";
        public static String CLIENT_NAME="DJP";

        public static String CULTURE_INFO="tr-TR";
        public static String CHANNEL="Channel";
        public static String NAME="Name";
        public static String SURNAME="Surname";
        public static String DESC="Description";
        public static String PHONE="Phone";
        public static String MAIL="Email";
        public static String DJPPASS="DJPPASS";
        public static String CONTACTFORM="CONTACTFORM";
        public static String SUCCESS_VALUE="1";
        public static String VALUE_ID="Value";
        public static String FORMAT_ID="Format";
        public static String NAME_ID="Name";
    }

    public static String ATLAS_GLOBAL_CLIENT_ID="atlasglobal_app";
    public static String DENIZ_BANK_CLIENT_ID="denizbank_app";



    public static class Authorization{
        public static String Class="Authorization";
        public static String CLIENT_TYPE="client_type";
        public static String CLIENT_SECRET="client_secret";
        public static String GRANT_TYPE="grant_type";
        public static String USER_NAME="username";
        public static String PASSWORD="password";
        public static String ACCESS_TOKEN="access_token";
        public static String BEARER="bearer ";
    }

    public static class ivr {
        public static String CHARACTER_ENCODING = "UTF-8";
        public static String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
        public static String AES_ENCRYPTION_ALGORITHM = "AES";
    }

    public static class checkCustomerOwnedService {
        public static String NO_MATCH = "NOMATCH";
        public static String DJP_PASS_CARD = "DJP PASS CARD";
        public static String Success = "Success";
        public static String Error = "Error";
    }

    public static class DjpPassLocations{
        public static String DJP = "1";
    }

    public static class StatusForFlightHours{
        public static String DIFF_24_HOURS="checkIfFlightLessDiff24Hours";
        public static String MAX_24_HOURS="checkIfFlightMaxDiff24Hours";
        public static String MIN_NINTY_MINITE="checkIfFlightMinDiffNintyMinute";
        public static String ARRIVAL_MUST_BE_BEFORE_DEPARTURE="arrivalMustBeBeforeDeparture";
        public static String OP="OP";
        public static String DOMESTIC="DOMESTIC";
        public static String INTERNATIONAL="INTERNATIONAL";
        public static String TRANSFER="TRANSFER";
        public static String PNR_CODE="PNR";
        public static String DEVICE_ID="DeviceId";
        public static String STATUS="Status";
        public static String PASSED="IsPassed";
        public static String OPERATIONAL_STATUS="OPERATIONAL_STATUS";
        public static String INTERNATIONAL_STATUS="INTERNATIONAL_STATUS";
        public static String NOT_USED_FOR_DOMESTIC="NOT_USED_FOR_DOMESTIC";
        public static String NOT_USED_FOR_INTERNATIONAL="NOT_USED_FOR_INTERNATIONAL";
        public static String NOT_AVAILABLE_FLIGHT_IN_DJP="NOT_AVAILABLE_FLIGHT_IN_DJP";
        public static String M1="M1";
    }
}
