package com.djp.core.dao.impl;

import com.djp.core.dao.DjpCppDao;
import com.djp.core.model.DjpCppOwnerModel;
import com.djp.core.model.DjpCppTxnModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class DjpCppDaoImpl implements DjpCppDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpCppDaoImpl.class);
    private static final String GET_DJP_CPP_TXN_BY_TOKEN="SELECT dct.id from DjpCppTxnModel dct where dct.cppToken=:cppToken AND (:cppSapStatus IS NULL OR dct.cppSapStatus NOT IN(:cppStatuses)) ";

    private static final String GET_DJP_CPP_OWN_BY_ID = "SELECT dco.id from DjpCppOwnerModel dco where dco.id=:id";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public DjpCppOwnerModel getDjpCppOwnerByID(String id) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpCppOwnerModel> djpCppOwnerModelList = session.createQuery(GET_DJP_CPP_OWN_BY_ID,DjpCppOwnerModel.class)
                    .setParameter("id", id)
                    .getResultList();
            boolean isEmpty = !djpCppOwnerModelList.isEmpty();
            return isEmpty ? djpCppOwnerModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public DjpCppTxnModel getDjpCppTxnDataByToken(String djpCppToken) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpCppTxnModel> djpCppTxnModelList = session.createQuery(GET_DJP_CPP_TXN_BY_TOKEN,DjpCppTxnModel.class)
                    .setParameter("cppToken", djpCppToken)
//                    .setParameter("cppStatuses", Arrays.asList(CppSapStatus.SAP_RETURNED))
                    .setParameter("cppStatuses", null)
                    .getResultList();
            boolean isEmpty = !djpCppTxnModelList.isEmpty();
            return isEmpty ? djpCppTxnModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }
}
