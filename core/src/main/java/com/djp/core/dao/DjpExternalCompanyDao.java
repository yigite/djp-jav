package com.djp.core.dao;

import com.djp.core.model.DjpExternalCustomerModel;
import com.djp.core.model.DjpExternalCompanyOrderTableModel;

public interface DjpExternalCompanyDao {

    DjpExternalCustomerModel findDjpExternalCustomerByID(String djpExternalCustomerID, String djpExternalCompanyClientID);

    DjpExternalCompanyOrderTableModel findDjpExternalOrderByIdAndCustomer(String orderCode, String customerID);
}
