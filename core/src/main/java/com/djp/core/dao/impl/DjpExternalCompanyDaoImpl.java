package com.djp.core.dao.impl;

import com.djp.core.dao.DjpExternalCompanyDao;
import com.djp.core.model.DjpExternalCustomerModel;
import com.djp.core.model.DjpExternalCompanyOrderTableModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Transactional
@Repository
public class DjpExternalCompanyDaoImpl implements DjpExternalCompanyDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DjpExternalCompanyDaoImpl.class);
    private static final String DJP_EXTERNAL_CUSTOMER_BY_ID_QUERY = "SELECT iecu.id FROM DjpExternalCustomerModel as iecu " +
            "JOIN DjpExternalCompanyModel as ieco on ieco.id=iecu.company.id " +
            "WHERE iecu.customerID=:customerID AND ieco.companyID=:companyID";
    private static final String DJP_EXTERNAL_ORDER_BY_ID_AND_CUSTOMER_QUERY = "SELECT iecot.id FROM DjpExternalCompanyOrderTableModel as iecot JOIN DjpExternalCustomerModel as iec on iec.id = iecot.customer.id WHERE iecot.code=:orderCode AND iec.customerID=:customerID";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public DjpExternalCustomerModel findDjpExternalCustomerByID(String djpExternalCustomerID, String djpExternalCompanyClientID) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpExternalCustomerModel> DjpExternalCustomerModels = session.createQuery(DJP_EXTERNAL_CUSTOMER_BY_ID_QUERY, DjpExternalCustomerModel.class)
                    .setParameter("customerID",djpExternalCustomerID)
                    .setParameter("companyID",djpExternalCompanyClientID)
                    .getResultList();
            return DjpExternalCustomerModels.size() > 0 ? DjpExternalCustomerModels.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("findDjpExternalCustomerByID ", e);
            return null;
        }
    }

    @Override
    public DjpExternalCompanyOrderTableModel findDjpExternalOrderByIdAndCustomer(String orderCode, String customerID) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpExternalCompanyOrderTableModel> djpExternalCompanyOrderTableModels = session.createQuery(DJP_EXTERNAL_ORDER_BY_ID_AND_CUSTOMER_QUERY,DjpExternalCompanyOrderTableModel.class)
                    .setParameter("orderCode", orderCode)
                    .setParameter("customerID", customerID)
                    .getResultList();
            boolean getDjpBoarding = !djpExternalCompanyOrderTableModels.isEmpty();
            return getDjpBoarding ? djpExternalCompanyOrderTableModels.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }

    }
}
