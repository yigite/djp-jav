package com.djp.core.dao;

import com.djp.core.model.BaseStoreModel;
import com.djp.core.model.OrderModel;

public interface DjpOrderDao {

    OrderModel getOrderByToken(String token, BaseStoreModel store);
}
