package com.djp.core.dao;

import com.djp.core.model.DjpTelCountriesModel;

import java.util.List;

public interface DjpTelephoneCountriesDao {

    List<DjpTelCountriesModel> findCountriesTelephone();
}
