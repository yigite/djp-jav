package com.djp.core.dao;

import com.djp.core.model.UserModel;

public interface UserDao {

    UserModel getUserForUID(String userUid);
}
