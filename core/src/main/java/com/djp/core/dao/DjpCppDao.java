package com.djp.core.dao;

import com.djp.core.model.DjpCppOwnerModel;
import com.djp.core.model.DjpCppTxnModel;

public interface DjpCppDao {

    DjpCppOwnerModel getDjpCppOwnerByID(final String id);
    DjpCppTxnModel getDjpCppTxnDataByToken(final String djpCppToken);
}
