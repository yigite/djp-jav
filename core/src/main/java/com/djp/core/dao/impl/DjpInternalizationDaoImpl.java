package com.djp.core.dao.impl;

import com.djp.core.dao.DjpInternalizationDao;
import com.djp.core.model.DialingCodeModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class DjpInternalizationDaoImpl implements DjpInternalizationDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpInternalizationDaoImpl.class);
    private static final String FROM_ENTITY_DIALING_CODE= "FROM DialingCodeModel AS dc ";
    private static final String ORDER_BY_ISOCODE= "ORDER BY dc.isoCode";

    @Resource
    private SessionFactory sessionFactory;


    @Override
    public List<DialingCodeModel> getDialogCodes() {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DialingCodeModel> dialingCodes = session.createQuery(FROM_ENTITY_DIALING_CODE + ORDER_BY_ISOCODE, DialingCodeModel.class)
                    .getResultList();
            return dialingCodes.size() > 0 ? dialingCodes : new ArrayList<>();
        } catch (Exception e) {
            LOGGER.error("getDialogCodes ", e);
            return null;
        }
    }
}
