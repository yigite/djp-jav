package com.djp.core.dao.impl;

import com.djp.core.dao.DjpTelephoneCountriesDao;
import com.djp.core.model.DjpTelCountriesModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Transactional
@Repository
public class DjpTelephoneCountriesDaoImpl implements DjpTelephoneCountriesDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DjpTelephoneCountriesDaoImpl.class);

    private static final String FROM_ENTITY_DJPTELCOUNTRIES= "FROM DjpTelCountriesModel";


    @Resource
    private SessionFactory sessionFactory;

    @Override
    public List<DjpTelCountriesModel> findCountriesTelephone() {
        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(FROM_ENTITY_DJPTELCOUNTRIES, DjpTelCountriesModel.class)
                    .getResultList();

        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }
}
