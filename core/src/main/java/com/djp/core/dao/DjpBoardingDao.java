package com.djp.core.dao;



import com.djp.core.enums.QrType;
import com.djp.core.model.*;

import java.util.Date;
import java.util.List;


public interface DjpBoardingDao {

    DjpBoardingPassModel getDjpBoardingPassByQrWithoutDateLimit(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpBoardingPassModel getDjpBoardingPassByFioriWithoutDateLimit(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpBoardingPassModel getDjpBoardingPassWithoutDate(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpAccessBoardingModel getDjpAccessBoardingWithoutDateLimit(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpAccessBoardingModel getDjpAccessBoarding(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpBoardingPassModel getDjpBoardingPass(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpBoardingPassModel getDjpBoardingPassByToken(String token);

    List<DjpBoardingPassModel> getDjpBoardingPassesByTokenAndSapOrderID(String qrKey, String sapOrderID);

    List<DjpBoardingPassModel> getDjpBoardingPassByTokenAndUsage(String qrKey,QrType whereUsed,Date sub48HoursCurrentDate);

    DjpBoardingPassModel getDjpBoardingPassByTokenAndSapOrderID(String token,String sapOrderID);

    DjpBoardingPassModel getDjpBoardingPassByQr(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpBoardingPassModel getDjpBoardingPassByFiori(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpMobilePassModel getDjpMobilePassByQr(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpMobilePassModel getDjpMobilePassByFiori(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);

    DjpMobilePassModel getDjpMobilePass(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed);
    DjpMobilePassModel getDjpMobilePass(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate);

    List<Object> getAllDjpBoardingSellList(Date startDate, Date endDate, Boolean isNewSale, QrType qrType, DjpPassLocationsModel djpPassLocation);

    ContractedCompaniesModel getContractedCompaniesViaPass(String contractedId);
}
