package com.djp.core.dao;

import com.djp.core.model.CustomerModel;
import com.djp.core.model.DjpOtherCustomerModel;


import java.util.List;

public interface CustomerDao {

    List<DjpOtherCustomerModel> getAllOtherCustomer();

    CustomerModel findCustomerByUid(String uid);

}
