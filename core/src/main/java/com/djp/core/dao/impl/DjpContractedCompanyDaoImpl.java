package com.djp.core.dao.impl;

import com.djp.core.dao.DjpContractedCompanyDao;
import com.djp.core.model.ContractedCompaniesModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class DjpContractedCompanyDaoImpl implements DjpContractedCompanyDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DjpContractedCompanyDaoImpl.class);

    private static final String CONTRACTED_COMPANY_QUERY_FOR_TOURNIQUET = "SELECT ccm.id from ContractedCompaniesModel as ccm where ccm.contractId=:contractId";

    @Resource
    private SessionFactory sessionFactory;
    @Override
    public ContractedCompaniesModel getContractedCompany(String contractId) {
        try{
            Session session = sessionFactory.getCurrentSession();
            List<ContractedCompaniesModel> contractedCompanies  = session.createQuery(CONTRACTED_COMPANY_QUERY_FOR_TOURNIQUET,ContractedCompaniesModel.class)
                    .setParameter("contractId",contractId)
                    .getResultList();
            boolean getContractedCompany = !contractedCompanies.isEmpty();
            return getContractedCompany ? contractedCompanies.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getContractedCompany ", e);
            return null;
        }
    }
}
