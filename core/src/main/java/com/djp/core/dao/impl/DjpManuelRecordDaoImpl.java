package com.djp.core.dao.impl;

import com.djp.core.dao.DjpManuelRecordDao;
import com.djp.core.enums.QrType;
import com.djp.core.model.DjpManuelRecordsModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DjpManuelRecordDaoImpl implements DjpManuelRecordDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpManuelRecordDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    private static final String FROM_ENTITY_MANUEL_RECORD= "FROM DjpManuelRecordsModel ";
    private static final String WHERE_USED_AND_CREATION= "WHERE whereUsed=:whereUsed " +
                                           " and creationtime BETWEEN :startDate AND :endDate";

    private final String subquery = "SELECT cc.companyName AS companyName,ct.code AS companyCode, iar.className AS r FROM" +
            "               ContractedCompaniesModel AS cc LEFT JOIN DjpAccessRightsModel AS iar " +
            "               ON cc.companyName = iar.airlineName " +
            "               LEFT JOIN contractedType as ct ON  cc.contractedType=ct.pk "+
            "               where cc.contractedService in (SELECT pk from QrType as qte where code LIKE CONCAT((?qrCode),'%')) " +
            "               GROUP BY cc.companyName,ct.code, iar.className";
    private final String GET_CONTRACTED_COMPANIES_FOR_SERVICE = "SELECT subquery.companyName, subquery.companyCode, STRING_AGG( r, ',') as className FROM "
            + subquery + " AS subquery " + "GROUP BY subquery.companyName,subquery.companyCode  ";



    private static final String GET_MANUEL_RECORD_FOR_SERVICE_BY_FLIGHTNO = "SELECT dmr.id FROM DjpManuelRecordsModel AS dmr WHERE dmr.whereUsed =:whereUsed AND dmr.pnrCode=:pnrCode";
    @Override
    public List<DjpManuelRecordsModel> getManuelRecords(Date startDate, Date endDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(FROM_ENTITY_MANUEL_RECORD + WHERE_USED_AND_CREATION, DjpManuelRecordsModel.class)
                    .getResultList();

        } catch (Exception e) {
            LOGGER.error("Djp Manuel Record ", e);
            return null;
        }
    }

    @Override
    public DjpManuelRecordsModel getManuelRecord(String pnr, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            List<DjpManuelRecordsModel> djpManuelRecordsModelList = session.createQuery(GET_MANUEL_RECORD_FOR_SERVICE_BY_FLIGHTNO, DjpManuelRecordsModel.class)
                    .setParameter("pnrCode",pnr)
                    .setParameter("whereUsed",whereUsed)
                    .getResultList();
           return !djpManuelRecordsModelList.isEmpty()?djpManuelRecordsModelList.get(0):null;
        } catch (Exception e) {
            LOGGER.error("Djp Manuel Record ", e);
            return null;
        }
    }

    @Override
    public ArrayList getContractedCompaniesForService(String qrCode) {

        try{
            Session session = sessionFactory.getCurrentSession();
            return (ArrayList) session.createQuery(GET_CONTRACTED_COMPANIES_FOR_SERVICE, DjpManuelRecordsModel.class)
                    .setParameter("qrCode",qrCode)
                    .getResultList();

        } catch (Exception e) {
            LOGGER.error("executeQuery :: Failed : " , e);
            return null;
        }
    }
}
