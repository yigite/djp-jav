package com.djp.core.dao;

import com.djp.core.model.ContractedCompaniesModel;

public interface DjpContractedCompanyDao {

    ContractedCompaniesModel getContractedCompany(String contractId);

}
