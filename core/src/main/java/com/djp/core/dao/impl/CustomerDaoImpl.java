package com.djp.core.dao.impl;


import com.djp.core.dao.CustomerDao;
import com.djp.core.model.CustomerModel;
import com.djp.core.model.DjpOtherCustomerModel;
import com.djp.core.model.QrForMobileModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDaoImpl.class);
    private static final String FROM_ENTITY_DJPOTHERCUSTOMER= "FROM DjpOtherCustomerModel";
    private static final String FROM_ENTITY_CUSTOMER= "FROM CustomerModel ";
    private static final String WHERE_UID= "WHERE uid=:uid";
    private static final String UIDQUERY="FROM Customer As c WHERE c.uid=:uid";


    @Resource
    private SessionFactory sessionFactory;

    @Override
    public List<DjpOtherCustomerModel> getAllOtherCustomer() {
        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(FROM_ENTITY_DJPOTHERCUSTOMER, DjpOtherCustomerModel.class)
                    .getResultList();

        } catch (Exception e) {
            LOGGER.error("Djp Other customer ", e);
            return null;
        }
    }

    @Override
    public CustomerModel findCustomerByUid(String uid) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<CustomerModel> query = session.createQuery(UIDQUERY, CustomerModel.class)
                    .setParameter("uid",uid);
            List<CustomerModel> customerModelList = query.getResultList();
            boolean getDjpBoarding = !customerModelList.isEmpty();
            return getDjpBoarding ? customerModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp Other customer ", e);
            return null;
        }
    }
}
