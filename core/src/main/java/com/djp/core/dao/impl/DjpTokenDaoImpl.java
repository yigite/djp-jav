package com.djp.core.dao.impl;

import com.djp.core.dao.DjpTokenDao;

import com.djp.core.model.DjpAccessRightsModel;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.core.model.DjpUniqueTokenModel;
import com.djp.core.model.QrForExperienceCenterModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public class DjpTokenDaoImpl implements DjpTokenDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DjpTokenDaoImpl.class);

    private static final String FROM_ENTITY_DJPPASSLOCATION= "FROM DjpPassLocationsModel";
    private static final String WHERE_LOCATIONID = " where locationID=:locationID";
    private static final String KEYQUERY="SELECT dtm.id FROM DjpUniqueTokenModel As dtm WHERE dtm.code=:code";
    private static final String ACCESS_RIGHTS_BY_MEMBERSHIP_QUERY = "SELECT dar.id FROM DjpAccessRightsModel AS dar WHERE dar.serviceUsageWithMembership=:serviceUsageWithMembership AND " +
            "((:now>= dar.startDate AND :now<=dar.expireDate) " +
            "OR (dar.startDate IS NULL AND dar.expireDate IS NULL) " +
            "OR (dar.startDate IS NULL AND :now<=dar.expireDate) " +
            "OR (:now>=dar.startDate AND dar.expireDate IS NULL))";

    private static final String ACCESS_RIGHTS_BY_UID_QUERY = "SELECT dar.id FROM DjpAccessRightsModel as dar " +
            "JOIN dar.validAtWhichLocation as location " +
            "WHERE dar.airlineCode = :airlineCode " +
            "AND location.locationID like CONCAT('%',CONCAT((:djpPassLocation),'%')) AND " +
            "((:now>=dar.startDate AND :now<=dar.expireDate) " +
            "OR (dar.startDate IS NULL AND dar.expireDate IS NULL) " +
            "OR (dar.startDate IS NULL AND :now<=dar.expireDate) " +
            "OR (:now>=dar.startDate AND dar.expireDate IS NULL)) " +
            "AND (dar.serviceUsageWithMembership=:value OR dar.serviceUsageWithMembership IS NULL)";

    private static final String ACCESS_RIGHT_BY_CODE_AND_CLASS_NAME_QUERY="SELECT d.id FROM DjpAccessRightsModel As d JOIN d.validAtWhichLocation as location WHERE d.airlineCode=:airlineCode" +
            " AND d.className=:className AND location.locationID like CONCAT('%',CONCAT((:djpPassLocation),'%'))";

    @Resource
    private SessionFactory sessionFactory;
    private static final String QR_FOR_EXPERIENCE_CENTER_BY_TOKEN_KEY_QUERY="SELECT qrb.id FROM QrForExperienceCenterModel AS qrb JOIN DjpUniqueTokenModel AS tk ON tk.id=qrb.tokenKey.id WHERE tk.code=:tokenKey";;

    @Override
    public DjpPassLocationsModel getDjpPassLocationByLocationID(String locationID) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpPassLocationsModel> query = session.createQuery(FROM_ENTITY_DJPPASSLOCATION + WHERE_LOCATIONID,DjpPassLocationsModel.class)
                    .setParameter("locationID", locationID);
            List<DjpPassLocationsModel> djpPassLocationsModelList = query.getResultList();
            boolean getDjpBoarding = !djpPassLocationsModelList.isEmpty();
            return getDjpBoarding ? djpPassLocationsModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }

    }

    public DjpUniqueTokenModel getTokenFromKey(String key) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpUniqueTokenModel> query = session.createQuery(KEYQUERY,DjpUniqueTokenModel.class)
                    .setParameter("code", key);
            List<DjpUniqueTokenModel> djpUniqueTokenModelList = query.getResultList();
            boolean getDjpBoarding = !djpUniqueTokenModelList.isEmpty();
            return getDjpBoarding ? djpUniqueTokenModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public List<DjpAccessRightsModel> getServiceUsageWithMembership() {
        try {
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(ACCESS_RIGHTS_BY_MEMBERSHIP_QUERY,DjpAccessRightsModel.class)
                    .setParameter("serviceUsageWithMembership", Boolean.TRUE)
                    .setParameter("now",new Date())
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public List<DjpAccessRightsModel> getAccessRightsByAirlineCode(String airlineCode, DjpPassLocationsModel djpPassLocation, Boolean isServiceUsageWithMembership) {
        try {
            Session session = sessionFactory.getCurrentSession();
            airlineCode = airlineCode.contains(" ") ? airlineCode.replace(" ","") : airlineCode;

            List<DjpAccessRightsModel> djpAccessRightsModelList = session.createQuery(ACCESS_RIGHTS_BY_UID_QUERY,DjpAccessRightsModel.class)
                    .setParameter("airlineCode", airlineCode)
                    .setParameter("djpPassLocation", djpPassLocation.getLocationID())
                    .setParameter("value", isServiceUsageWithMembership)
                    .setParameter("now",new Date())
                    .getResultList();
            if (!CollectionUtils.isEmpty(djpAccessRightsModelList)) {
                return djpAccessRightsModelList;
            }
            return Collections.emptyList();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }
    @Override
    public DjpAccessRightsModel getAccessRightsByAirlineCodeAndClassName(String airport, String className, DjpPassLocationsModel djpPassLocation) {
        try {
            Session session = sessionFactory.getCurrentSession();

            List<DjpAccessRightsModel> djpAccessRightsModelList = session.createQuery(ACCESS_RIGHT_BY_CODE_AND_CLASS_NAME_QUERY,DjpAccessRightsModel.class)
                    .setParameter("airlineCode", airport)
                    .setParameter("className", className + "%")
                    .setParameter("djpPassLocation", djpPassLocation)
                    .getResultList();
            if (!CollectionUtils.isEmpty(djpAccessRightsModelList)) {
                return (DjpAccessRightsModel) djpAccessRightsModelList;
            }
            return (DjpAccessRightsModel) Collections.emptyList();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public QrForExperienceCenterModel getQRForExperienceCenterByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForExperienceCenterModel> query = session.createQuery(QR_FOR_EXPERIENCE_CENTER_BY_TOKEN_KEY_QUERY, QrForExperienceCenterModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForExperienceCenterModel> qrForExperienceCenterModelList = query.getResultList();
            boolean noEmpty = !qrForExperienceCenterModelList.isEmpty();
            return noEmpty ? qrForExperienceCenterModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("QrForExperienceCenterModel ", e);
            return null;
        }
    }
}
