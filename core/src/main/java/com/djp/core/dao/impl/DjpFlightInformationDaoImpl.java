package com.djp.core.dao.impl;

import com.djp.core.dao.DjpFlightInformationDao;
import com.djp.core.model.FlightInformationModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.Date;

@Repository
@Transactional
public class DjpFlightInformationDaoImpl implements DjpFlightInformationDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpFlightInformationDaoImpl.class);

    private static final String FROM_ENTITY_FLIGHT = "FROM FlightInformationModel fi";
    private static final String WHERE_FLIGHT_DATE_AND_NUMBER = " WHERE fi.flightNumber = :flightNumber " +
                                                               " AND CONVERT(DATE, fi.flightDate) = :flightDate";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public FlightInformationModel getFlightInformation(Date flightDate, String flightNumber) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<FlightInformationModel> query = session.createQuery(FROM_ENTITY_FLIGHT + WHERE_FLIGHT_DATE_AND_NUMBER, FlightInformationModel.class)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("flightDate", flightDate);
            return query.getResultList().get(0);
        } catch (Exception e) {
            LOGGER.error("Djp flight information", e);
            return null;
        }
    }
}
