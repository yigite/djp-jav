package com.djp.core.dao.impl;

import com.djp.core.dao.DjpBoardingDao;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.service.TypeService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.Collections;
import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Transactional
@Repository
public class DjpBoardingDaoImpl implements DjpBoardingDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DjpBoardingDaoImpl.class);

    private static final String COULD_READ_DJP_BOARDING_PASS_BY_QR_WITHOUT_DATE_LIMIT = "SELECT id from DjpBoardingPassModel AS bp JOIN QrForMobileModel AS qr on qr.tokenKey=bp.token " +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.itemType.id=:itemType ";

    private static final String COULD_READ_DJP_BOARDING_PASS_BY_FR_WITHOUT_DATE_LIMIT = "SELECT id from DjpBoardingPassModel AS bp JOIN QrForFioriModel AS fr on fr.tokenKey=bp.token " +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.itemType.id=:itemType ";

    private static final String COULD_READ_DJP_ACCESS_BOARDING_WITHOUT_DATE_LIMIT = "SELECT id from DjpAccessBoardingModel as apb" +
            "WHERE apb.dateOfFlight=?flightDate " +
            "AND apb.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND apb.flightNumber=:flightNumber " +
            "AND apb.seatNumber=:seatNumber " +
            "AND apb.isCancelled=:isCancelled ";

    private static final String COULD_READ_DJP_BOARDING_PASS = "SELECT id from DjpBoardingPassModel bp" +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.creationtime > :sub48HoursCurrentDate AND bp.isCancelled=:isCancelled " +
            "AND bp.DjpBoardingPass=:itemType ";

    private static final String COULD_READ_DJP_BOARDING_PASS_WITHOUT_DATE_LIMIT = "SELECT bp.id from DjpBoardingPass AS bp" +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.isCancelled=:isCancelled " +
            "AND bp.itemType.id =:itemType ";

    private static final String COULD_READ_DJP_ACCESS_BOARDING = "SELECT ab.id from AccessBoarding AS ab " +
            "WHERE ab.dateOfFlight=:flightDate " +
            "AND ab.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND ab.flightNumber=:flightNumber " +
            "AND ab.seatNumber=:seatNumber " +
            "AND ab.creationtime > :sub48HoursCurrentDate AND bp.isCancelled=:isCancelled ";

    private static final String COULD_READ_DJP_BOARDING_PASS_BY_QR = "SELECT bp.id from DjpBoardingPassModel AS bp JOIN QrForMobileModel AS qr on qr.tokenKey=bp.token " +
            "WHERE bpdateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.creationtime > :sub48HoursCurrentDate AND bp.id=:itemType";

    private static final String COULD_READ_DJP_MOBILE_PASS_BY_FR = "SELECT bp.id from DjpMobilePassModel AS bp JOIN QrForFioriModel AS fr on qr.tokenKey=bp.token " +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.creationtime > :sub48HoursCurrentDate " +
            "AND bp.id=:itemType" ;
    private static final String COULD_READ_DJP_MOBILE_PASS_BY_QR ="SELECT bp.id from DjpMobilePassModel AS bp JOIN QrForMobileModel AS qr on qr.tokenKey=bp.token " +
            "WHERE bp.dateOfFlight=:flightDate " +
            "AND bp.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND bp.flightNumber=:flightNumber " +
            "AND bp.seatNumber=:seatNumber " +
            "AND bp.creationtime > :sub48HoursCurrentDate " +
            "AND bp.id=:itemType "+
            "ORDER BY bp.creationtime desc";

    private static final String COULD_READ_DJP_MOBILE_PASS = "SELECT mp.id from DjpMobilePassModel AS mp " +
            "WHERE dateOfFlight:flightDate " +
            "AND mp.operatingCarrierDesignator:operatingCarrierDesignator " +
            "AND mp.flightNumber:flightNumber " +
            "AND mp.seatNumber:seatNumber " +
            "AND mp.creationtime > :sub48HoursCurrentDate AND mp.isCancelled=:isCancelled " +
            "AND mp.id=:itemType" ;

    private static final String COULD_READ_IGA_MOBILE_PASS_QUERY = "SELECT mpm.id FROM DjpMobilePassModel AS mpm " +
            "WHERE mpm.dateOfFlight=:flightDate " +
            "AND mpm.operatingCarrierDesignator=:operatingCarrierDesignator " +
            "AND mpm.flightNumber=:flightNumber " +
            "AND mpm.seatNumber=:seatNumber " +
            "AND mpm.creationtime > :sub48HoursCurrentDate " +
            "AND mpm.id=:itemType " +
            "ORDER BY creationtime desc";
    private static final String DJP_BOARDING_PASS_BY_TOKEN = "SELECT bp.id FROM DjpBoardingPass AS bp JOIN IgaUniqueToken AS tk ON tk.id=bp.token WHERE tk.code=:token AND bp.itemtype=?itemType";

    private static final String DJP_BOARDING_PASS_BY_TOKEN_AND_WHERE_USED = "SELECT bp.id FROM DjpBoardingPassModel AS bp " +
            "JOIN DjpUniqueTokenModel AS tk ON tk.id=bp.token.id " +
            "WHERE tk.code=:token AND bp.itemtype=:itemType AND bp.boardingUsage=:whereUsed AND bp.creationtime > :sub48HoursCurrentDate";

    private static final String CONTRACTED_COMPANIES_BY_DJP_PASS ="SELECT id FROM ContractedCompaniesModel " +
            "WHERE contractId=:contractedId";

    @Resource
    private SessionFactory sessionFactory;

    @Resource
    private TypeService typeService;


    @Override
    public DjpBoardingPassModel getDjpBoardingPassByQrWithoutDateLimit(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoarding = session.createQuery(generateCouldReadBoardingPassByQrWithoutDateLimitQuery(whereUsed),DjpBoardingPassModel.class)
                    .setParameter("flightDate",flightDate)
                    .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                    .setParameter("flightNumber",flightNumber)
                    .setParameter("seatNumber",seatNumber)
                    .setParameter("itemType",getItemType("DjpBoardingPass"));
            if(Objects.nonNull(whereUsed)){
                couldReadDjpBoarding.setParameter("whereUsed",whereUsed);
            }
            List<DjpBoardingPassModel> getDjpBoardingModels = couldReadDjpBoarding.getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByFioriWithoutDateLimit(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoarding = session.createQuery(generateCouldReadBoardingPassByWithoutDateLimitFioriQuery(whereUsed),DjpBoardingPassModel.class)
                                                                      .setParameter("flightDate",flightDate)
                                                                      .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                                                                      .setParameter("flightNumber",flightNumber)
                                                                      .setParameter("seatNumber",seatNumber)
                                                                      .setParameter("itemType",getItemType("DjpBoardingPass"));

            if(Objects.nonNull(whereUsed)){
                couldReadDjpBoarding.setParameter("whereUsed",whereUsed);
            }
            List<DjpBoardingPassModel> getDjpBoardingModels = couldReadDjpBoarding.getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassByFioriWithoutDateLimit ", e);
            return null;
        }
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassWithoutDate(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadBoardingPassByQueryWithoutDateLimit(whereUsed),DjpBoardingPassModel.class)
                    .setParameter("flightDate",flightDate)
                    .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                    .setParameter("flightNumber",flightNumber)
                    .setParameter("seatNumber",seatNumber)
                    .setParameter("isCancelled",Boolean.FALSE);
            if(Objects.nonNull(whereUsed)){
                couldReadDjpBoardingPass.setParameter("whereUsed",whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType",getItemType("DjpBoardingPassModel"));
            List<DjpBoardingPassModel> getDjpBoardingModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public DjpAccessBoardingModel getDjpAccessBoardingWithoutDateLimit(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
            try{
                Session session = sessionFactory.getCurrentSession();
                Query<DjpAccessBoardingModel> couldReadDjpAccessBoarding = session.createQuery(generateCouldReadBoardingPassQueryWithoutDateLimit(whereUsed),DjpAccessBoardingModel.class)
                .setParameter("flightDate",flightDate)
                .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                .setParameter("flightNumber",flightNumber)
                .setParameter("seatNumber",seatNumber);

                if(Objects.nonNull(whereUsed)){
                    couldReadDjpAccessBoarding.setParameter("whereUsed",whereUsed);
                }
                couldReadDjpAccessBoarding.setParameter("isCancelled",Boolean.FALSE);
                List<DjpAccessBoardingModel> getDjpAccessBoardingModels = couldReadDjpAccessBoarding.getResultList();
                boolean getDjpAccessBoarding = !getDjpAccessBoardingModels.isEmpty();
                return getDjpAccessBoarding ? getDjpAccessBoardingModels.get(0) : null;

            }catch (Exception e){
                LOGGER.error("getDjpAccessBoardingWithoutDateLimit ", e);
                return null;
            }
    }


    @Override
    public DjpAccessBoardingModel getDjpAccessBoarding(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpAccessBoardingModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadBoardingPassQuery(whereUsed),DjpAccessBoardingModel.class)
                    .setParameter("flightDate",flightDate)
                    .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                    .setParameter("flightNumber",flightNumber)
                    .setParameter("seatNumber",seatNumber)
                    .setParameter("isCancelled",Boolean.FALSE);
            if(Objects.nonNull(whereUsed)){
                couldReadDjpBoardingPass.setParameter("whereUsed",whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType",getItemType("DjpBoardingPassModel"));
            List<DjpAccessBoardingModel> getDjpBoardingModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPass(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadBoardingPassByQuery(whereUsed),DjpBoardingPassModel.class)
                                                                      .setParameter("flightDate",flightDate)
                                                                      .setParameter("operatingCarrierDesignator",operatingCarrierDesignator)
                                                                      .setParameter("flightNumber",flightNumber)
                                                                      .setParameter("seatNumber",seatNumber)
                                                                      .setParameter("sub48HoursCurrentDate",sub48HoursCurrentDate)
                                                                      .setParameter("isCancelled",Boolean.FALSE);

            if(Objects.nonNull(whereUsed)) {
                couldReadDjpBoardingPass.setParameter("whereUsed", whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType",getItemType("DjpBoardingPass"));
            List<DjpBoardingPassModel> getDjpBoardingPassModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoardingPass = !getDjpBoardingPassModels.isEmpty();
            return getDjpBoardingPass ? getDjpBoardingPassModels.get(0) : null;

        }catch (Exception e){
            LOGGER.error("getDjpBoardingPass ", e);
            return null;
        }
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByQr(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpMobilePassModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadBoardingPassByQrQuery(whereUsed), DjpMobilePassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate);
            if (Objects.nonNull(whereUsed)) {
                couldReadDjpBoardingPass.setParameter("whereUsed", whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType", getItemType("DjpBoardingPass"));
            List<DjpMobilePassModel> getDjpMobileModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpMobileBoarding = !getDjpMobileModels.isEmpty();
            return getDjpMobileBoarding ? getDjpMobileModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassByQr ", e);
            return null;
        }

    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByFiori(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadMobilePassByFioriQuery(whereUsed), DjpBoardingPassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate);
            if (Objects.nonNull(whereUsed)) {
                couldReadDjpBoardingPass.setParameter("whereUsed", whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType", getItemType("DjpBoardingPass"));
            List<DjpBoardingPassModel> getDjpBoardingModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassByFiori ", e);
            return null;
        }
    }

    @Override
    public DjpMobilePassModel getDjpMobilePassByQr(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpMobilePassModel> getDjpBoardingModels = session.createQuery(COULD_READ_DJP_MOBILE_PASS_BY_QR, DjpMobilePassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate)
                    .setParameter("itemType", getItemType("DjpMobilePass"))
                    .getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpMobilePassByQr ", e);
            return null;
        }
    }

    @Override
    public DjpMobilePassModel getDjpMobilePassByFiori(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<DjpMobilePassModel> getDjpBoardingModels = session.createQuery(generateCouldReadMobilePassByFioriQuery(whereUsed), DjpMobilePassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate)
                    .setParameter("itemType", getItemType("DjpBoardingPass"))
                    .getResultList();
            boolean getDjpBoarding = !getDjpBoardingModels.isEmpty();
            return getDjpBoarding ? getDjpBoardingModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpMobilePassByFiori ", e);
            return null;
        }
    }

    @Override
    public DjpMobilePassModel getDjpMobilePass(Date flightDate,String operatingCarrierDesignator,String flightNumber,String seatNumber, Date sub48HoursCurrentDate, QrType whereUsed) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpMobilePassModel> couldReadDjpBoardingPass = session.createQuery(generateCouldReadMobilePassByQuery(whereUsed), DjpMobilePassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate)
                    .setParameter("isCancelled",Boolean.FALSE);
            if (Objects.nonNull(whereUsed)) {
                couldReadDjpBoardingPass.setParameter("whereUsed", whereUsed);
            }
            couldReadDjpBoardingPass.setParameter("itemType", getItemType("DjpMobilePass"));
            List<DjpMobilePassModel> getDjpMobilePassModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoarding = !getDjpMobilePassModels.isEmpty();
            return getDjpBoarding ? getDjpMobilePassModels.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpMobilePass ", e);
            return null;
        }
    }

    @Override
    public DjpMobilePassModel getDjpMobilePass(Date flightDate, String operatingCarrierDesignator, String flightNumber, String seatNumber, Date sub48HoursCurrentDate) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpMobilePassModel> couldReadDjpBoardingPass = session.createQuery(COULD_READ_IGA_MOBILE_PASS_QUERY, DjpMobilePassModel.class)
                    .setParameter("flightDate", flightDate)
                    .setParameter("operatingCarrierDesignator", operatingCarrierDesignator)
                    .setParameter("flightNumber", flightNumber)
                    .setParameter("seatNumber", seatNumber)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate)
                    .setParameter("itemType", getItemType("DjpMobilePass"));
            List<DjpMobilePassModel> getDjpMobilePasses = couldReadDjpBoardingPass.getResultList();

            boolean getDjpBoardingPass = !getDjpMobilePasses.isEmpty();
            return getDjpBoardingPass ? getDjpMobilePasses.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpMobilePass ", e);
            return null;
        }
    }

    @Override
    public List<Object> getAllDjpBoardingSellList(Date startDate, Date endDate, Boolean isNewSale, QrType whereUsed, DjpPassLocationsModel djpPassLocation) {
        try {
            Session session = sessionFactory.getCurrentSession();
            String query = getQueryForAllBoardingData( whereUsed);
            NativeQuery<Object> hibernateQuery = session.createNativeQuery(query)
                    .setParameter("startDate",startDate)
                    .setParameter("endDate",endDate)
                    .setParameter("location",djpPassLocation.getLocationID())
                    .setParameter("isCancelled",Boolean.FALSE)
                    .setParameter("WhereUsed",whereUsed);

            if(Objects.nonNull(isNewSale)) {
                hibernateQuery.setParameter("isNewSale", isNewSale);
            }
            if("LOUNGE_DOMESTIC".equals(whereUsed.getCode())){
                hibernateQuery.setParameter("whereUsed2",QrType.valueOf("LOUNGE_PLUS_DOMESTIC"));
            }else if("LOUNGE_INTERNATIONAL".equals(whereUsed.getCode())){
                hibernateQuery.setParameter("whereUsed2",QrType.valueOf("LOUNGE_PLUS_INTERNATIONAL"));
            }
            return hibernateQuery.list();
        }catch (Exception e){
            LOGGER.error("getAllDjpBoardingSellList ", e);
            return null;
        }
    }

    @Override
    public ContractedCompaniesModel getContractedCompaniesViaPass(String contractedId) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<ContractedCompaniesModel> result = session.createQuery(CONTRACTED_COMPANIES_BY_DJP_PASS, ContractedCompaniesModel.class)
                    .setParameter("contractedId", contractedId)
                    .getResultList();
            return CollectionUtils.isEmpty(result) ? null : result.get(0);
        }catch (Exception e){
            LOGGER.error("getContractedCompaniesViaPass ", e);
            return null;
        }
    }

    private String getQueryForAllBoardingData(QrType whereUsed){
        StringBuilder query = new StringBuilder();
        query.append("SELECT tb.id FROM  ");

        query.append("( ");
        //BoardingPassModel
        query.append("SELECT id as PK, creationtime as CREATIONTIME FROM DjpBoardingPass ");
        query.append("WHERE (creationtime BETWEEN :startDate AND :endDate) ");
        query.append("AND isCancelled=:isCancelled ");
        query.append("AND whichLocation.id=:location ");
        if("LOUNGE_DOMESTIC".equals(whereUsed.getCode()) || "LOUNGE_INTERNATIONAL".equals(whereUsed.getCode())){
            query.append("AND boardingUsage=:whereUsed OR boardingUsage=:whereUsed2 ");
        } else {
            query.append("AND boardingUsage=:whereUsed ");
        }
        query.append("AND newSale=:isNewSale ");

        query.append(") ");

        query.append("UNION ALL ");
        //MobilePassModel
        query.append("( ");
        query.append("SELECT id as PK, creationtime as CREATIONTIME FROM DjpMobilePass ");
        query.append("WHERE creationtime BETWEEN :startDate AND :endDate ");
        query.append("AND isCancelled=:isCancelled ");
        query.append("AND whichLocation.id=:location ");
        if("LOUNGE_DOMESTIC".equals(whereUsed.getCode()) || "LOUNGE_INTERNATIONAL".equals(whereUsed.getCode())){
            query.append("AND (boardingUsage=:whereUsed OR boardingUsage=:whereUsed2) ");
        } else {
            query.append("AND boardingUsage=:whereUsed ");
        }
        query.append("AND newSale=:isNewSale ");

        query.append(") ");

        query.append("UNION ALL ");
        //DjpAccessBoarding
        query.append("( ");

        query.append("SELECT id as PK, creationtime as CREATIONTIME FROM DjpAccessBoarding ");
        query.append("WHERE creationtime BETWEEN :startDate AND :endDate ");
        query.append("AND isCancelled=:isCancelled ");
        query.append("AND whichLocation.id=:location ");
        query.append("AND boardingUsages LIKE CONCAT('%',CONCAT((?whereUsed),'%')) ");
        query.append("AND newSale=:isNewSale ");

        query.append(") tb ORDER BY tb.CREATIONTIME DESC ");
        return query.toString();
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByToken(String token) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(DJP_BOARDING_PASS_BY_TOKEN,DjpBoardingPassModel.class)
                    .setParameter("token", token)
                    .setParameter("itemType", getItemType("DjpBoardingPass"));
            List<DjpBoardingPassModel> getDjpBoardingPassModels = couldReadDjpBoardingPass.getResultList();
            boolean getDjpBoardingPass = !getDjpBoardingPassModels.isEmpty();
            return getDjpBoardingPass ? getDjpBoardingPassModels.get(0) : null;

        }catch (Exception e){
            LOGGER.error("getDjpBoardingPass ", e);
            return null;
        }
    }

    @Override
    public List<DjpBoardingPassModel> getDjpBoardingPassesByTokenAndSapOrderID(String qrKey, String sapOrderID) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(generateBoardingPassQuery(sapOrderID),DjpBoardingPassModel.class)
                    .setParameter("token", qrKey)
                    .setParameter("itemType", getItemType("DjpBoardingPassModel"))
                    .setParameter("sapOrderID", sapOrderID);
            return couldReadDjpBoardingPass.getResultList();

        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassesByTokenAndSapOrderID ", e);
            return null;
        }
    }

    @Override
    public List<DjpBoardingPassModel> getDjpBoardingPassByTokenAndUsage(String qrKey, QrType whereUsed, Date sub48HoursCurrentDate) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> couldReadDjpBoardingPass = session.createQuery(DJP_BOARDING_PASS_BY_TOKEN_AND_WHERE_USED,DjpBoardingPassModel.class)
                    .setParameter("token", qrKey)
                    .setParameter("itemType", getItemType("DjpBoardingPassModel"))
                    .setParameter("whereUsed", whereUsed)
                    .setParameter("sub48HoursCurrentDate", sub48HoursCurrentDate);
            return couldReadDjpBoardingPass.getResultList();

        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassByTokenAndUsage ", e);
            return null;
        }
    }

    @Override
    public DjpBoardingPassModel getDjpBoardingPassByTokenAndSapOrderID(String token, String sapOrderID) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpBoardingPassModel> query = session.createQuery(generateBoardingPassQuery(sapOrderID),DjpBoardingPassModel.class)
                    .setParameter("token", token)
                    .setParameter("itemType", getItemType("DjpBoardingPassModel"))
                    .setParameter("sapOrderID", sapOrderID);
            List<DjpBoardingPassModel> djpBoardingPassModelList = query.getResultList();
            boolean hasIgaBoardingPassModel = !djpBoardingPassModelList.isEmpty();
            return hasIgaBoardingPassModel ? djpBoardingPassModelList.get(0) : null;
        }catch (Exception e){
            LOGGER.error("getDjpBoardingPassByTokenAndUsage ", e);
            return null;
        }
    }
    public long getItemType(String typeCode) {
        //TODO: need information
//        TypeModel typeForCode = typeService.getTypeForCode(typeCode);
//        return typeForCode.getPk();
        return 1l;
    }
    private String generateBoardingPassQuery(String sapOrderID){
        StringBuilder builder = new StringBuilder();
        builder.append(DJP_BOARDING_PASS_BY_TOKEN);
        if(StringUtils.hasText(sapOrderID)){
            builder.append(" AND bp.sapOrderID=:sapOrderID");
        }
        return builder.toString();
    }
    private String generateCouldReadBoardingPassByQueryWithoutDateLimit(QrType qrTypeEnum){
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_BOARDING_PASS_WITHOUT_DATE_LIMIT);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND bp.boardingUsage=:whereUsed ");
        }
        builder.append("ORDER BY bp.creationtime desc");
        return builder.toString();
    }
    private String generateCouldReadBoardingPassQuery(QrType qrTypeEnum){
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_ACCESS_BOARDING);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND ab.boardingUsages LIKE CONCAT('%',CONCAT((:whereUsed),'%')) ");
        }
        builder.append("ORDER BY ab.creationtime desc");
        return builder.toString();
    }
    private String generateCouldReadBoardingPassByQrWithoutDateLimitQuery(QrType qrTypeEnum){
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_BOARDING_PASS_BY_QR_WITHOUT_DATE_LIMIT);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND qr.whereUsed=:whereUsed ");
        }
        builder.append("ORDER BY bp.creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadBoardingPassByWithoutDateLimitFioriQuery(QrType qrTypeEnum) {

        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_BOARDING_PASS_BY_FR_WITHOUT_DATE_LIMIT);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND fr.whereUsed=:whereUsed ");
        }
        builder.append("ORDER BY bp.creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadBoardingPassQueryWithoutDateLimit(QrType qrTypeEnum) {
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_ACCESS_BOARDING_WITHOUT_DATE_LIMIT);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND abp.boardingUsages LIKE CONCAT('%',CONCAT((:whereUsed),'%')) ");
        }
        builder.append("ORDER BY abp.creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadBoardingPassByQuery(QrType qrTypeEnum) {
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_BOARDING_PASS);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND bp.boardingUsage=:whereUsed ");
        }
        builder.append("ORDER BY bp.creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadBoardingPassByQrQuery(QrType qrTypeEnum){
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_BOARDING_PASS_BY_QR);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND qr.whereUsed=:whereUsed ");
        }
        builder.append("ORDER BY bp.creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadMobilePassByFioriQuery(QrType qrTypeEnum){
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_MOBILE_PASS_BY_FR);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND fr.whereUsed=:whereUsed ");
        }
        builder.append("ORDER BY creationtime desc");
        return builder.toString();
    }

    private String generateCouldReadMobilePassByQuery(QrType qrTypeEnum) {
        StringBuilder builder = new StringBuilder();
        builder.append(COULD_READ_DJP_MOBILE_PASS);
        if(Objects.nonNull(qrTypeEnum)){
            builder.append("AND boardingUsage=:whereUsed ");
        }
        builder.append("ORDER BY creationtime desc");
        return builder.toString();
    }

}
