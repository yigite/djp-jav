package com.djp.core.dao.impl;

import com.djp.core.dao.DjpPosDao;
import com.djp.core.model.DjpPosLogModel;
import com.djp.core.model.DjpRemainingUsageModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.util.Collections;
import java.util.Objects;

public class DjpPosDaoImpl implements DjpPosDao {
    @Resource
    private SessionFactory sessionFactory;
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpPosDaoImpl.class);
    private static final String GET_POS_LOG_MODEL="SELECT pl.id FROM DjpPosLogModel as pl where pl.posRequestId=:posRequestId AND pl.type=:type";
    @Override
    public DjpPosLogModel getPosLogModel(String requestId){

        try{
            Session session = sessionFactory.getCurrentSession();
            return (DjpPosLogModel) session.createQuery(GET_POS_LOG_MODEL, DjpRemainingUsageModel.class)
                    .setParameter("posRequestId",requestId)
                    .setParameter("type","S")
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return (DjpPosLogModel) Collections.emptyList();
        }
    }

}
