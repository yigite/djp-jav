package com.djp.core.dao;


import com.djp.core.enums.QrType;
import com.djp.core.model.*;

import java.util.List;

public interface DjpQRDao {

    QrForMobileModel getQRForMobileByTokenKey(String tokenKey);

    QrForVoucherModel getQRForDjpByTokenKey(String tokenKey);

    QrForFioriModel getQRForFioriByTokenKey(String tokenKey);

    QrForBankModel getQRForBankByTokenKey(String tokenKey);

    QrForMeetModel getQRForMeetByTokenKey(String tokenKey);

    QrForCabinetModel getQrForCabinet(String qrToken);

    QrForExperienceCenterModel getQRForExperienceCenterByTokenKey(String tokenKey);

    QrForCardModel getQRForCardByTokenKey(String tokenKey);

    List<DjpContractedTicketsModel> getContractedTicketByID(String uniqueID);

    DjpVoucherCodeUsagesModel findCampaignCodeUsageBySapOrderIDAndVoucher(String sapOrderID,String qrKey);

    QrForExternalModel getQRForExternalByTokenKey(String tokenKey);

    List<DjpExternalCompanyUsagesModel> getQrExternalCompanyUsagesList(String tokenKey);

    QrForCardModel getQRForCardsByPhoneNumber(String phoneNumber);

    ContractedCompaniesModel getContractedCompany(String companyId, QrType contractedService, DjpPassLocationsModel djpPassLocation);

    List<GateNumberModel> getGates();
}
