package com.djp.core.dao;

import com.djp.core.model.DjpPosLogModel;

public interface DjpPosDao {

    DjpPosLogModel getPosLogModel(String requestId);
}
