package com.djp.core.dao.impl;

import com.djp.core.dao.DjpOrderDao;
import com.djp.core.model.BaseStoreModel;
import com.djp.core.model.OrderModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Transactional
@Repository
public class DjpOrderDaoImpl implements DjpOrderDao {

    private static final String ORDER_BY_TOKEN = "SELECT o.id FROM OrderModel as o join DjpUniqueToken as tk on tk.id=o.activationOrUsageCode WHERE tk.code=:token AND o.store.id=:store";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public OrderModel getOrderByToken(String token, BaseStoreModel store) {
        Session session = sessionFactory.getCurrentSession();
        List<OrderModel> getOrder  = session.createQuery(ORDER_BY_TOKEN,OrderModel.class)
                .setParameter("token",token)
                .setParameter("store",store.getId())
                .getResultList();
        boolean hasOrder= !getOrder.isEmpty();
        return hasOrder ? getOrder.get(0) : null;
    }
}
