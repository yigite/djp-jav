package com.djp.core.dao;

import com.djp.core.model.FlightInformationModel;

import java.util.Date;

public interface DjpFlightInformationDao {
    FlightInformationModel getFlightInformation(final Date flightDate, final String flightNumber);
}
