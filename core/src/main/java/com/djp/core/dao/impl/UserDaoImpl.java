package com.djp.core.dao.impl;


import com.djp.core.dao.UserDao;
import com.djp.core.model.UserModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);
    private static final String FROM = "FROM UserModel ";
    private static final String WHERE_UID = " where uid=:uid";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public UserModel getUserForUID(String userUid) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<UserModel> result = session.createQuery(FROM + WHERE_UID, UserModel.class)
                    .setParameter("uid", userUid)
                    .getResultList();
            return result.size() > 0 ? result.get(0) : null;

        } catch (Exception e) {
            LOGGER.error("getUserForUID Exp: ", e);
            return null;
        }
    }
}
