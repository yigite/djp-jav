package com.djp.core.dao;



import com.djp.core.model.DjpAccessRightsModel;
import com.djp.core.model.DjpPassLocationsModel;
import com.djp.core.model.DjpUniqueTokenModel;
import com.djp.core.model.QrForExperienceCenterModel;

import java.util.List;


public interface DjpTokenDao {

   DjpPassLocationsModel getDjpPassLocationByLocationID(String locationID);

   DjpUniqueTokenModel getTokenFromKey(String key);

   List<DjpAccessRightsModel> getServiceUsageWithMembership();

    List<DjpAccessRightsModel> getAccessRightsByAirlineCode(String airport, DjpPassLocationsModel djpPassLocation, Boolean isServiceUsageWithMembership);

    QrForExperienceCenterModel getQRForExperienceCenterByTokenKey(String tokenKey);

    DjpAccessRightsModel getAccessRightsByAirlineCodeAndClassName(String airport,String className,DjpPassLocationsModel igaPassLocation);

}
