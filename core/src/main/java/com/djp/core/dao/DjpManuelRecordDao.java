package com.djp.core.dao;

import com.djp.core.enums.QrType;
import com.djp.core.model.DjpManuelRecordsModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface DjpManuelRecordDao {
    List<DjpManuelRecordsModel> getManuelRecords(Date startDate, Date endDate, QrType whereUsed);

    ArrayList getContractedCompaniesForService(String qrCode);

    DjpManuelRecordsModel getManuelRecord(String pnr, QrType whereUsed);
}
