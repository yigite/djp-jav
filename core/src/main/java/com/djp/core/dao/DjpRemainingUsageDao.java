package com.djp.core.dao;

import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.core.model.UserModel;

import java.util.Date;
import java.util.List;

public interface DjpRemainingUsageDao {

    List<DjpRemainingUsageModel> getCustomerUsages(String customerUid);

    List<DjpRemainingUsageModel> getCustomerUsages(String customerUid, String productCategory, String productType);

    List<DjpRemainingUsageModel> getExternalCustomerUsages(String externalUid, String productCategory, String productType);

    List<DjpRemainingUsageModel> findDjpRemainingUsageForPackageProductByUser(UserModel userModel);

    List<DjpRemainingUsageModel> getDjpRemainingUsageForOrderCode(String orderCode);

    List<DjpRemainingUsageModel> getDailyPackageProducts(String customerUid, String orderCode, String packageProductCode, Date date,Boolean isExternalCustomer);

    DjpRemainingUsageModel getDjpRemainingByExternal(String customerId, String orderCode);

}