package com.djp.core.dao.impl;

import com.djp.core.dao.DjpRemainingUsageDao;
import com.djp.core.model.DjpRemainingUsageModel;
import com.djp.core.model.QrForMobileModel;
import com.djp.core.model.UserModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.djp.core.constant.DjpCoreConstants;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.util.*;

@Repository
@Transactional
public class DjpRemainingUsageDaoImpl implements DjpRemainingUsageDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpRemainingUsageDaoImpl.class);
    private static final String REMAININGUSAGES="SELECT ru.id FROM DjpRemainingUsage as ru JOIN User as u on ru.user.id=u.id JOIN Product as p on ru.product.id=p.id ";
    private static final String REMAININGUSAGES_WHERECLOSURE=" WHERE u.uid=:uid AND ru.claimed>ru.used ";
    private static final String REMAININGUSAGES_ORDERBY="order by ru.orderCode asc";
    private static final String DJP_REMAINING_USAGE_FOR_PACKAGE_PRODUCT_BY_USER_QUERY = "SELECT ru.id FROM DjpRemainingUsageModel as ru WHERE ru.user.id=:user.id AND ru.packagedProductModel IS NOT NULL AND ru.isCancelled = false ";
    private static final String DJP_USER_FOR_PACKAGE_BY_ORDER_QUERY="SELECT rm.id FROM DjpRemainingUsageModel as rm where rm.orderCode=:orderCode ";
    private static final String REMAININGUSAGES_DAILY="SELECT ru.id FROM DjpRemainingUsageModel as ru JOIN UserModel as u on ru.user.id=u.id JOIN ProductModel as p on ru.packagedProduct.id=p.id";
    private static final String REMAININGUSAGESDAILY_WHERECLOSURE=" WHERE u.uid=:uid ";
    private static final String REMAININGUSAGES_DAILY_EXTERNAL="SELECT ru.id FROM DjpRemainingUsageModel as ru JOIN DjpExternalCustomerModel as iec on ru.externalUser.id=:iec.id JOIN ProductModel as p on ru.packagedProduct.id=p.id";
    private static final String REMAININGUSAGESDAILY_WHERECLOSURE_EXTERNAL_CUSTOMER=" WHERE iec.customerID=:uid ";
    private static final Logger LOG = LoggerFactory.getLogger(DjpRemainingUsageDaoImpl.class);
    private static final String REMAININGUSAGES_EXTERNAL_CUSTOMER = "SELECT ru.id FROM DjpRemainingUsageModel as ru JOIN DjpExternalCustomerModel as iec on ru.externalUser=iec.id JOIN ProductModel as p on ru.product=p.id ";
    private static final String REMAININGUSAGES_CATEGORY = "JOIN CategoryProductRelationModel AS c2p ON p.id=c2p.target JOIN CategoryModel as cat on cat.id=c2p.source ";
    private static final String REMAININGUSAGES_WHERECLOSURE_EXTERNAL_CUSTOMER = "WHERE iec.customerID=:uid AND ru.claimed>ru.used";
    private static final String REMAININGUSAGES_PACKAGETYPE = "AND ru.packagedProductModel IS NOT NULL ";
    private static final String REMAININGUSAGES_PRODUCTTYPE = " AND ru.packagedProductModel IS NULL ";
    private static final String SELECT_DJP_REMAINING_USAGE_FOR_PACKAGE_PRODUCT_BY_USER_QUERY = "SELECT ru.id FROM DjpRemainingUsageModel as ru" +
            " WHERE ru.UserModel.uid=:uid" +
            " AND ru.packagedProduct IS NOT NULL " +
            " AND ru.isCancelled = 0"+
            " AND ((:now >= ru.startDate AND :now <= ru.expireDate) OR (ru.startDate IS NULL AND :now <= ru.expireDate))";
    private static final String DJP_GET_REMAININGUSAGE_BY_EXTERNAL_INFO ="SELECT ru.DjpRemainingUsageModel.id FROM DjpRemainingUsageModel as ru " +
            " JOIN DjpExternalCustomerModel as iec on ru.djpRemainingUsageModel.externalUser.id=iec.djpExternalCustomerModel.id" +
            " WHERE iec.djpExternalCustomerModel.customerId=:customerId AND ru.djpRemainingUsageModel.orderCode=:orderCode";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public List<DjpRemainingUsageModel> getCustomerUsages(String customerUid) {
        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(generateRemaininUsageDateQuery(), DjpRemainingUsageModel.class)
                    .setParameter("uid",customerUid)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<DjpRemainingUsageModel> getCustomerUsages(String customerUid, String productCategory, String productType) {
        try {
            List usageList = getUsagesForQuery(customerUid,null, productCategory, productType);
            return !CollectionUtils.isEmpty(usageList) ? usageList : Collections.emptyList();
        }catch (Exception e){
            LOG.error("getCustomerUsages: ", e);
            return null;
        }
    }

    @Override
    public List<DjpRemainingUsageModel> getExternalCustomerUsages(String externalUid, String productCategory, String productType) {
        try {
            List usageList = getUsagesForQuery(null,externalUid, productCategory, productType);
            return !CollectionUtils.isEmpty(usageList) ? usageList : Collections.emptyList();
        }catch (Exception e){
            LOG.error("getExternalCustomerUsages: ", e);
            return null;
        }
    }

    @Override
    public List<DjpRemainingUsageModel> findDjpRemainingUsageForPackageProductByUser(UserModel userModel) {
        try{
            Session session = sessionFactory.getCurrentSession();

            List<DjpRemainingUsageModel> result = session.createQuery(SELECT_DJP_REMAINING_USAGE_FOR_PACKAGE_PRODUCT_BY_USER_QUERY, DjpRemainingUsageModel.class)
                    .setParameter("uid", userModel.getUid())
                    .setParameter("now", new Date())
                    .getResultList();
            return result.size() > 0 ? result : new ArrayList<>();

        } catch(Exception e){
            LOG.error("findDjpRemainingUsageForPackageProductByUser: ", e);
            return null;
        }
    }

    @Override
    public List<DjpRemainingUsageModel> getDjpRemainingUsageForOrderCode(String orderCode) {
        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(DJP_USER_FOR_PACKAGE_BY_ORDER_QUERY, DjpRemainingUsageModel.class)
                    .setParameter("orderCode",orderCode)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<DjpRemainingUsageModel> getDailyPackageProducts(String customerUid, String orderCode, String packageProductCode, Date date, Boolean isExternalCustomer) {
        List usageList = getDailyUsagesForQuery(customerUid,orderCode, packageProductCode, date,isExternalCustomer);
        if(!CollectionUtils.isEmpty(usageList)){
            return ((List<DjpRemainingUsageModel>) usageList);
        }
        return Collections.emptyList();
    }

    @Override
    public DjpRemainingUsageModel getDjpRemainingByExternal(String customerId, String orderCode) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<DjpRemainingUsageModel> query = session.createQuery(DJP_GET_REMAININGUSAGE_BY_EXTERNAL_INFO, DjpRemainingUsageModel.class)
                    .setParameter("orderCode",orderCode)
                    .setParameter("customerId",customerId);
            List<DjpRemainingUsageModel> djpRemainingUsageModelList = query.getResultList();
            boolean getDjpBoarding = !djpRemainingUsageModelList.isEmpty();
            return getDjpBoarding ? djpRemainingUsageModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("djp Remaining Usage Model List ", e);
            return null;
        }
    }

    private List getDailyUsagesForQuery(String customerUid, String orderCode, String packageProductCode, Date date,Boolean isExternalCustomer) {
        StringBuilder usageQuery = new StringBuilder();
        if (StringUtils.hasText(customerUid) && Boolean.FALSE.equals(isExternalCustomer)) {
            usageQuery.append(REMAININGUSAGES_DAILY);
            usageQuery.append(REMAININGUSAGESDAILY_WHERECLOSURE);
            usageQuery.append(" AND p.code=:packageProductCode AND ru.orderCode=:orderCode AND ru.expireDate=:date ");
        }else if(Boolean.TRUE.equals(isExternalCustomer)){
            usageQuery.append(REMAININGUSAGES_DAILY_EXTERNAL);
            usageQuery.append(REMAININGUSAGESDAILY_WHERECLOSURE_EXTERNAL_CUSTOMER);
            usageQuery.append(" AND p.code=:packageProductCode AND ru.orderCode=:orderCode AND ru.expireDate=:date ");
        }
        usageQuery.append(generateDateQueryForDb());


        usageQuery.append(REMAININGUSAGES_ORDERBY);

        try{
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(usageQuery.toString(), DjpRemainingUsageModel.class)
                    .setParameter("uid", customerUid)
                    .setParameter("orderCode", orderCode)
                    .setParameter("date", date)
                    .setParameter("packageProductCode", packageProductCode)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return Collections.emptyList();
        }
    }

    private  String generateDjpRemainingUsageForPackageProductByUserQuery(){
        StringBuilder query = new StringBuilder(DJP_REMAINING_USAGE_FOR_PACKAGE_PRODUCT_BY_USER_QUERY);
        query.append(generateDateQueryForDb());
        return query.toString();
    }
    private String generateRemaininUsageDateQuery(){
        StringBuilder query = new StringBuilder();
        query.append(REMAININGUSAGES);
        query.append(REMAININGUSAGES_WHERECLOSURE);
        query.append(generateDateQueryForDb());
        query.append(REMAININGUSAGES_ORDERBY);
        return query.toString();
    }
    private List<DjpRemainingUsageModel> getUsagesForQuery(String customerUid, String externalUid, String productCategory, String productType) {
        StringBuilder usageQuery = new StringBuilder();
        if (StringUtils.hasText(productCategory)) {
            if (StringUtils.hasText(externalUid)) {
                usageQuery.append(REMAININGUSAGES_EXTERNAL_CUSTOMER);
                usageQuery.append(REMAININGUSAGES_CATEGORY);
                usageQuery.append(REMAININGUSAGES_WHERECLOSURE_EXTERNAL_CUSTOMER);
            }
            if (StringUtils.hasText(customerUid)) {
                usageQuery.append(REMAININGUSAGES);
                usageQuery.append(REMAININGUSAGES_CATEGORY);
                usageQuery.append(REMAININGUSAGES_WHERECLOSURE);
            }
            usageQuery.append(generateDateQueryForDb());
            usageQuery.append(" AND cat.code IN (:catCode) ");
        } else {
            if (StringUtils.hasText(externalUid)) {
                usageQuery.append(REMAININGUSAGES_EXTERNAL_CUSTOMER);
                usageQuery.append(REMAININGUSAGES_WHERECLOSURE_EXTERNAL_CUSTOMER);
            }
            if (StringUtils.hasText(customerUid)) {
                usageQuery.append(REMAININGUSAGES);
                usageQuery.append(REMAININGUSAGES_WHERECLOSURE);
            }
            usageQuery.append(generateDateQueryForDb());
        }


        if (DjpCoreConstants.PackageOrProduct.PACKAGE.equals(productType)) {
            usageQuery.append(REMAININGUSAGES_PACKAGETYPE);
        } else if (DjpCoreConstants.PackageOrProduct.PRODUCT.equals(productType)) {
            usageQuery.append(REMAININGUSAGES_PRODUCTTYPE);
        }

        usageQuery.append(REMAININGUSAGES_ORDERBY);

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(usageQuery.toString(), DjpRemainingUsageModel.class)
                .setParameter("uid", StringUtils.hasText(externalUid) ? externalUid : customerUid)
                .setParameter("now",new Date());

        if (StringUtils.hasText(productCategory)) {
            query.setParameter("catCode", productCategory.contains(",") ? Arrays.asList(productCategory.split(",")) : productCategory);
        }

        return  query.getResultList();
    }

    private String generateDateQueryForDb() {
        return "AND " +
                "((:now>=ru.startDate AND :now<=ru.expireDate) OR (ru.startDate IS NULL AND :now<=ru.expireDate)) " +
                "AND ru.isCancelled = 0";
    }
}
