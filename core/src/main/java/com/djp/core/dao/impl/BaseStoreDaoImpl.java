package com.djp.core.dao.impl;

import com.djp.core.dao.BaseStoreDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class BaseStoreDaoImpl implements BaseStoreDao {

}
