package com.djp.core.dao;

import com.djp.core.model.DialingCodeModel;
import java.util.List;

public interface DjpInternalizationDao {
    List<DialingCodeModel> getDialogCodes();
}
