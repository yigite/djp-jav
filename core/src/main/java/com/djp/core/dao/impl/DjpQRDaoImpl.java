package com.djp.core.dao.impl;


import com.djp.core.dao.DjpQRDao;
import com.djp.core.enums.QrType;
import com.djp.core.model.*;
import com.djp.core.model.DjpContractedTicketsModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Transactional
@Repository
public class DjpQRDaoImpl implements DjpQRDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DjpQRDaoImpl.class);
    private static final String QR_FOR_MOBILE_BY_TOKEN_KEY_QUERY = "SELECT qrm.id FROM QrForMobileModel qrm JOIN DjpUniqueTokenModel AS tk ON tk.id=qrm.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_DJP_BY_TOKEN_KEY_QUERY = "SELECT qrv.id FROM QrForVoucherModel AS qrv JOIN DjpUniqueTokenModel AS tk ON tk.id=qrv.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_FIORI_BY_TOKEN_KEY_QUERY = "SELECT qrf.id FROM QrForFioriModel AS qrf JOIN DjpUniqueTokenModel AS tk ON tk.id=qrf.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_BANK_BY_TOKEN_KEY_QUERY = "SELECT qrb.id FROM QrForBankModel AS qrb JOIN DjpUniqueTokenModel AS tk ON tk.id=qrb.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_MEET_BY_TOKEN_KEY_QUERY = "SELECT qrm.id FROM QrForMeetModel AS qrm JOIN DjpUniqueTokenModel AS tk ON tk.id=qrm.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_CABINET_BY_TOKEN_KEY_QUERY = "SELECT qr.id FROM QrForCabinetModel AS qr JOIN DjpUniqueTokenModel as ut on qr.tokenKey.id = ut.id WHERE ut.code=:tokenKey";
    private static final String QR_FOR_EXPERIENCE_CENTER_BY_TOKEN_KEY_QUERY = "SELECT qrb.id FROM QrForExperienceCenterModel AS qrb JOIN DjpUniqueTokenModel AS tk ON tk.id=qrb.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_FOR_CARD_BY_TOKEN_KEY_QUERY = "SELECT qcm.id  FROM QrForCardModel as qcm WHERE qcm.cardKey=:tokenKey";
    private static final String FROM_DJP_CONTRACTED_TICKET = "FROM "+ DjpContractedTicketsModel._TYPECODE;
    private static final String WHERE_UNIQUE_ID = " where uniqueID=:uniqueID";
    private static final String CAMPAING_CODE_USAGE_BY_SAP_ORDER_ID_AND_QR_KEY_QUERY = "SELECT ivcu.id FROM DjpVoucherCodeUsagesModel AS ivcu " +
            "JOIN QrForVoucherModel AS qm on qm.id=ivcu.voucher.id " +
            "JOIN DjpUniqueTokenModel AS iut ON iut.id=qm.tokenKey.id " +
            "WHERE ivcu.sapOrderID=:sapOrderID AND iut.code=:qrKey ";
    private static final String QR_FOR_EXTERNAL_BY_TOKEN_KEY_QUERY = "SELECT qfe.id FROM QrForExternalModel AS qfe JOIN DjpUniqueTokenModel AS tk ON tk.id=qfe.tokenKey.id WHERE tk.code=:tokenKey";
    private static final String QR_EXTERNAL_USAGES_LIST = "SELECT iec.id FROM QrForExternalModel AS qr JOIN DjpUniqueTokenModel as ut on qr.tokenKey.id = ut.id JOIN DjpExternalCompanyUsagesModel as iec on qr.tokenKey.id = iec.tokenKey.id WHERE ut.code=:tokenKey";
    private static final String CONTRACTED_COMPANY_QUERY = "SELECT cc.id from ContractedCompaniesModel where cc.companyId=:companyId and cc.contractedService=:contractedService";
    private static final String DJP_GATE_NUMBERS = "SELECT g.id  FROM GateNumberModel as g";
    private static final String QR_FOR_CARD_BY_PHONE_NUMBER_QUERY = "SELECT qrc.id FROM QrForCardModel AS qrc WHERE qrc.phoneNumber LIKE CONCAT('%',CONCAT(:phone,'%'))";
    @Resource
    private SessionFactory sessionFactory;

    @Override
    public QrForMobileModel getQRForMobileByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForMobileModel> query =  session.createQuery(QR_FOR_MOBILE_BY_TOKEN_KEY_QUERY, QrForMobileModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForMobileModel> qrForMobileModelList = query.getResultList();
            boolean getDjpBoarding = !qrForMobileModelList.isEmpty();
            return getDjpBoarding ? qrForMobileModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForVoucherModel getQRForDjpByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForVoucherModel> query =  session.createQuery(QR_FOR_DJP_BY_TOKEN_KEY_QUERY, QrForVoucherModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForVoucherModel> qrForVoucherModelList = query.getResultList();
            boolean noEmpty = !qrForVoucherModelList.isEmpty();
            return noEmpty ? qrForVoucherModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForFioriModel getQRForFioriByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForFioriModel> query =  session.createQuery(QR_FOR_FIORI_BY_TOKEN_KEY_QUERY, QrForFioriModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForFioriModel> qrForFioriModelList = query.getResultList();
            boolean noEmpty = !qrForFioriModelList.isEmpty();
            return noEmpty ? qrForFioriModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForBankModel getQRForBankByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForBankModel> query = session.createQuery(QR_FOR_BANK_BY_TOKEN_KEY_QUERY, QrForBankModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForBankModel> qrForBankModelList = query.getResultList();
            boolean noEmpty = !qrForBankModelList.isEmpty();
            return noEmpty ? qrForBankModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForMeetModel getQRForMeetByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForMeetModel> query =  session.createQuery(QR_FOR_MEET_BY_TOKEN_KEY_QUERY, QrForMeetModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForMeetModel> qrForMeetModelList = query.getResultList();
            boolean noEmpty = !qrForMeetModelList.isEmpty();
            return noEmpty ? qrForMeetModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForCabinetModel getQrForCabinet(String qrToken) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForCabinetModel> query = session.createQuery(QR_FOR_CABINET_BY_TOKEN_KEY_QUERY, QrForCabinetModel.class)
                    .setParameter("tokenKey", qrToken);
            List<QrForCabinetModel> qrForCabinetModelList = query.getResultList();
            boolean noEmpty = !qrForCabinetModelList.isEmpty();
            return noEmpty ? qrForCabinetModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForExperienceCenterModel getQRForExperienceCenterByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForExperienceCenterModel> query = session.createQuery(QR_FOR_EXPERIENCE_CENTER_BY_TOKEN_KEY_QUERY, QrForExperienceCenterModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForExperienceCenterModel> qrForExperienceCenterModelList = query.getResultList();
            boolean noEmpty = !qrForExperienceCenterModelList.isEmpty();
            return noEmpty ? qrForExperienceCenterModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }

    @Override
    public QrForCardModel getQRForCardByTokenKey(String tokenKey) {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<QrForCardModel> query =  session.createQuery(QR_FOR_CARD_BY_TOKEN_KEY_QUERY, QrForCardModel.class)
                    .setParameter("tokenKey", tokenKey);
            List<QrForCardModel> qrForCardModelList = query.getResultList();
            boolean noEmpty = !qrForCardModelList.isEmpty();
            return noEmpty ? qrForCardModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp tel countries ", e);
            return null;
        }
    }
    @Override
    public List<DjpContractedTicketsModel> getContractedTicketByID(String uniqueID) {

        try {
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(FROM_DJP_CONTRACTED_TICKET + WHERE_UNIQUE_ID, DjpContractedTicketsModel.class)
                    .setParameter("uniqueID", uniqueID)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public DjpVoucherCodeUsagesModel findCampaignCodeUsageBySapOrderIDAndVoucher(String sapOrderID, String qrKey) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<DjpVoucherCodeUsagesModel> query = session.createQuery(CAMPAING_CODE_USAGE_BY_SAP_ORDER_ID_AND_QR_KEY_QUERY, DjpVoucherCodeUsagesModel.class)
                    .setParameter("qrKey",qrKey);
            List<DjpVoucherCodeUsagesModel> djpVoucherCodeUsagesModelList = query.getResultList();
            boolean noEmpty = !djpVoucherCodeUsagesModelList.isEmpty();
            return noEmpty ? djpVoucherCodeUsagesModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public QrForExternalModel getQRForExternalByTokenKey(String tokenKey) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<QrForExternalModel> query = session.createQuery(QR_FOR_EXTERNAL_BY_TOKEN_KEY_QUERY, QrForExternalModel.class)
                    .setParameter("tokenKey",tokenKey);
            List<QrForExternalModel> qrForExternalModelList = query.getResultList();
            boolean noEmpty = !qrForExternalModelList.isEmpty();
            return noEmpty ? qrForExternalModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public List<DjpExternalCompanyUsagesModel> getQrExternalCompanyUsagesList(String tokenKey) {
        try {
            Session session = sessionFactory.getCurrentSession();
            return session.createQuery(QR_EXTERNAL_USAGES_LIST, DjpExternalCompanyUsagesModel.class)
                    .setParameter("tokenKey",tokenKey)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public ContractedCompaniesModel getContractedCompany(String companyId, QrType contractedService, DjpPassLocationsModel djpPassLocation) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query<ContractedCompaniesModel> query = session.createQuery(generateContractCompaniesByDjpPassLocation(djpPassLocation), ContractedCompaniesModel.class)
                    .setParameter("companyId",companyId)
                    .setParameter("contractedService", contractedService);
            if(Objects.nonNull(djpPassLocation)) {
                query.setParameter("djpPassLocation", djpPassLocation);
            }
            List<ContractedCompaniesModel> contractedCompaniesModelList = query.getResultList();
            boolean noEmpty = !contractedCompaniesModelList.isEmpty();
            return noEmpty ? contractedCompaniesModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }

    @Override
    public QrForCardModel getQRForCardsByPhoneNumber(String phoneNumber) {
        try {
            Session session = sessionFactory.getCurrentSession();
            List<QrForCardModel> qrForCardModelList = session.createQuery(QR_FOR_CARD_BY_PHONE_NUMBER_QUERY, QrForCardModel.class)
                    .setParameter("phone",phoneNumber)
                    .getResultList();
            return !qrForCardModelList.isEmpty() ? qrForCardModelList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error(e.toString());
            return null;
        }
    }
    private String generateContractCompaniesByDjpPassLocation(DjpPassLocationsModel djpPassLocation){
        StringBuilder builder = new StringBuilder();
        builder.append(CONTRACTED_COMPANY_QUERY);
        if(Objects.nonNull(djpPassLocation)){
            builder.append("AND validLocation=:igaPassLocation");
        }
        return builder.toString();
    }

    @Override
    public List<GateNumberModel> getGates() {
        try{
            Session session = sessionFactory.getCurrentSession();
            Query<GateNumberModel> query =  session.createQuery(DJP_GATE_NUMBERS, GateNumberModel.class);
            List<GateNumberModel> GateNumberList = query.getResultList();
            boolean getGates = !GateNumberList.isEmpty();
            return getGates ? (List<GateNumberModel>) GateNumberList.get(0) : null;
        } catch (Exception e) {
            LOGGER.error("Djp gate numbers ", e);
            return null;
        }
    }
}
