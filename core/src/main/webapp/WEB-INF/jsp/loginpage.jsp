<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;encoding=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="version" value="<%= new java.util.Date().getTime()%>" />

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="<c:url value="/resources/assets/icons/favicon.ico" />" type="image/x-icon"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link href="<c:url value="/resources/assets/plugins/fontawesome-free/css/all.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/daterangepicker/daterangepicker.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/select2/css/select2.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bs-stepper/css/bs-stepper.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/dropzone/min/dropzone.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/dist/css/adminlte.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/customcss/loginpage.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/customcss/homePage.css" />" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="#" class="h1"><b style="color: #007bff;"></b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg"></p>
        <span id="isEmptyToken" data-result="${isEmptyToken}">
        <span id="expiredToken" data-result="${expiredToken}"/>
        <c:url var="loginUrl" value='/spring_security_check'/>
            <form:form action="${loginUrl}" method="POST" modelAttribute="loginForm">
                <div class="input-group mb-3">
                    <form:input path="username" name="username" type="text" class="form-control input-lg"/>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <form:input path="password" name="password" type="password" class="form-control input-lg" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <sec:csrfInput/>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary btn-block"></button>
                </div>
            </form:form>
            <p class="mb-1">
                <!--href="https://www.igacloud.com/home/fioripassreset"-->
                <a href="#" data-toggle="modal" data-target="#forgotPasswordModal"></a>
            </p>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title" id="alertModalTitle">Hata</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="alertModalP"></p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Kapat</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="errorTokenModal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title" id="alertTokenModalTitle">Hata</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="alertTokenModalP"></p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Kapat</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal" id="forgotPasswordModal" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="forgotPasswordModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="max-width: 400px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h3 class="text-center"><i class="fa fa-lock fa-4x"></i></h3>
                <h3 class="text-center">E-IGA Şifre Yenileme</h3>
                <div class="form-group flex-row d-flex col-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-user-circle"></i>
                            </div>
                        </div>
                        <input id="uid" name="uid" placeholder="Kullanıcı adınız" type="text" required="required" class="form-control">
                    </div>
                </div>
                <span class="form-text small text-muted text-center" id="messageText"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary resetPasswordBtn" onclick="return controlInput()">Şifremi Unuttum</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="successSendEmail" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="forgotPasswordModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="max-width: 400px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16">
                    <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>
                </svg>
                <span class="form-text small text-muted text-center" id="successText">Şifre yenileme linki e-posta adresinize gönderilmiştir.</span>
            </div>
        </div>
    </div>
</div>



<button type="button" id="dangerModalBtn" style="display: none;" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger"></button>
<button type="button" id="errorTokenModalBtn" style="display: none;" class="btn btn-danger" data-toggle="modal" data-target="#errorTokenModal"></button>
<button type="button" id="successSendEmailBtn" style="display: none;" class="btn btn-danger" data-toggle="modal" data-target="#successSendEmail"></button>
<script src="../../resources/assets/plugins/jquery/jquery.min.js"></script>
<script src="../../resources/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../resources/assets/dist/js/adminlte.min.js"></script>
<script src="<c:url value="/resources/assets/customjs/login.js?v${version}" />"></script>


</body>
</html>

