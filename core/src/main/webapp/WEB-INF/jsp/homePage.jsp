<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;encoding=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<head>
    <title><spring:theme code="djpservices.head.title1"></spring:theme> | <spring:theme
            code="djpservices.head.title3"></spring:theme></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<c:url value="/resources/assets/icons/favicon.ico" />" type="image/x-icon"/>

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link href="<c:url value="/resources/assets/plugins/fontawesome-free/css/all.min.css" />" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/resources/assets/plugins/ionicons.min.css" />">
    <link href="<c:url value="/resources/assets/plugins/daterangepicker/daterangepicker.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/select2/css/select2.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/bs-stepper/css/bs-stepper.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/plugins/dropzone/min/dropzone.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/dist/css/adminlte.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/assets/customcss/homePage.css" />" rel="stylesheet">

    <tags:javascriptthemetag></tags:javascriptthemetag>

</head>
<body class="hold-transition sidebar-mini">
<sec:authorize access="hasAnyRole('GEMI_ADAM_ASSISTANT','GEMI_ADAM_RESERVATION','GEMI_ADAM_FIRMA')" var="isGemiAdam"/>
<sec:authorize access="hasAnyRole('BUSINESS_CORNER')" var="isBusinessCorner"/>
<sec:authorize access="hasAnyRole('FAST_TRACK')" var="isFastTrack"/>
<sec:authorize access="hasAnyRole('MANAGER')" var="isManager"/>
<sec:authorize access="hasAnyRole('YOUTH_LOUNGE')" var="isYouthLounge"/>
<sec:authorize access="hasAnyRole('BAGAJ_SARMA')" var="isBagajSarma"/>
<sec:authorize access="hasAnyRole('BUSINESS_POD')" var="isBusinessPod"/>
<sec:authorize access="hasAnyRole('LOUNGE')" var="isLounge"/>
<sec:authorize access="hasAnyRole('POPUP_LOUNGE')" var="isPopupLounge"/>
<sec:authorize access="hasAnyRole('ORDER_MENAGEMENT')" var="isOrderManagement"/>
<sec:authorize access="hasAnyRole('BUGGY')" var="isBuggy"/>
<sec:authorize access="hasAnyRole('DALAMANLOUNGE')" var="isDalamanlounge"/>
<sec:authorize access="hasAnyRole('POD_HOTEL')" var="isPodHotel"/>
<sec:authorize access="hasAnyRole('IGA_NOT_SEND_SAP')" var="isIgaNotSendSap"/>
<sec:authorize access="hasAnyRole('IGA_EMANET_DOLABI')" var="isEmanet"/>
<sec:authorize access="hasAnyRole('IGA_SLEEPOD')" var="isSleepod"/>
<sec:authorize access="hasAnyRole('IGA_SHOWER')" var="isShower"/>
<sec:authorize access="hasAnyRole('IGA_MEET_AND_GREET')" var="isMeetAndGreet"/>
<sec:authorize access="hasAnyRole('EXPERIENCE_CENTER_SALE')" var="isExperienceCenter"/>
<sec:authorize access="hasAnyRole('IGA_CONTRACTED')" var="isContracted"/>
<sec:authorize access="hasAnyRole('IGA_PACKET_SALE')" var="isIgaPacketSale"/>

<div class="wrapper">

    <tags:navbar-main></tags:navbar-main>
    <tags:sidebar-left></tags:sidebar-left>
    <div class="content-wrapper">
        <section class="content-header m-0 p-0">
            <div class="container-fluid p-0">
                <div class="row mb-2">
                    <div class="col-md-12 homepage-title">
                        <h1>DJP Services</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <div class="container row justify-content-center text-center m-auto menu-items col-md-12 pt-5">

            <c:if test="${isGemiAdam or isManager}">
                <div class="col-md-3">
                    <a href="/shipcrewpage/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-anchor"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.shipcrew"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>

            <c:if test="${isBusinessCorner or isManager}">
                <div class="col-md-3">
                    <a href="/businessCorner/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-business-time"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.businesscorner"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>

            <c:if test="${isFastTrack or isManager}">
                <div class="col-md-3">
                    <a href="/fasttrackpage/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-chevron-right"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.fasttrack"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isYouthLounge or isManager}">
                <div class="col-md-3">
                    <a href="/youthlounge/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/youth_Lounge.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.youthlounge"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isBagajSarma or isManager}">
                <div class="col-md-3">
                    <a href="/bagajSarma/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-suitcase-rolling"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.bagaj"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isBusinessPod or isManager}">
                <div class="col-md-3">
                    <a href="/bCabinet/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/business_Cabinet.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.businesscabinetToUpperCase"/></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isManager or isExperienceCenter}">
                <div class="col-md-3">
                    <a href="/eCenter/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/experience_center.png"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.experienceCenterToUpperCase"/></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isLounge or isManager}">
                <div class="col-md-3">
                    <a href="/lounge/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-cocktail"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.lounge"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isPopupLounge or isManager}">
                <div class="col-md-3">
                    <a href="/popupLounge/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                    <span class="info-box-icon">
                                        <img alt="" src='<c:url value="/resources/assets/icons/popupLounge.svg"></c:url>'>
                                    </span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.popupLounge"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isOrderManagement or isManager}">
                <div class="col-md-3">
                    <a href="/orderManagement/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-indent"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.order"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isBuggy or isManager}">
                <div class="col-md-3">
                    <a href="/buggy/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/buggy.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.buggy"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isDalamanlounge or isManager}">
                <div class="col-md-3">
                    <a href="/dalamanlounge/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/dalaman_Lounge.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.dalamanlounge"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isPodHotel or isManager}">
                <div class="col-md-3">
                    <a href="/podHotel/usage">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" style="width: 75px" src='<c:url value="/resources/assets/icons/pod_Hotel.svg"></c:url>'>
                                </span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.podHotel"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isIgaNotSendSap or isManager}">
                <div class="col-md-3">
                    <a href="/IgaNotSendSap">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/sendnot.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.notSendSapToUpperCase"/></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isSleepod or isManager}">
                <div class="col-md-3">
                    <a href="/uykukabini/usage">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-bed"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.sleepcabin"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isEmanet or isManager}">
                <div class="col-md-3">
                    <a href="/emanetDolabi/usage">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" style="width: 50px" src='<c:url value="/resources/assets/icons/emanet_Dolabi.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.emanetdolap"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isShower or isManager}">
                <div class="col-md-3">
                    <a href="/shower/usage">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" style="width: 50px" src='<c:url value="/resources/assets/icons/shower.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.showercabin"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isMeetAndGreet or isManager}">
                <div class="col-md-3">
                    <a href="/meetAndGreet/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="far fa-handshake"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.meetAndGreet"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isTest}">
                <div class="col-md-3">
                    <a href="/meetingLounge/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/meeting_Lounge.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.meetinglounge"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/kvkk/menu">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="fas fa-user-shield"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.kvkkonay"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isIgaPacketSale or isManager}">
                <div class="col-md-3">
                    <a href="/igaPacket/newSale">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon">
                                    <img alt="" src='<c:url value="/resources/assets/icons/business_Cabinet.svg"></c:url>'>
                                </span>
                                <span class="info-box-text mt-3"><spring:theme code="djpservices.header.menu.igaPassPaketToUpperCase"/></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
            <c:if test="${isContracted or isManager}">
                <div class="col-md-3">
                    <a href="/contractedInstitutions" target="_blank">
                        <div class="info-box bg-info bg-custom">
                            <div class="info-box-content">
                                <span class="info-box-icon"><i class="far fa-handshake"></i></span>
                                <span class="info-box-text"><spring:theme code="djpservices.header.menu.contractedInstitutions"></spring:theme></span>
                            </div>
                        </div>
                    </a>
                </div>
            </c:if>
        </div>
        <!--a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
            <i class="fas fa-chevron-up"></i>
        </a-->
    </div>
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
        </div>
    </footer>

    <aside class="control-sidebar control-sidebar-dark">
    </aside>
</div>
<script src="<c:url value="/resources/assets/plugins/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/resources/assets/plugins/bootstrap/js/bootstrap.bundle.min.js" />"></script>
<script src="<c:url value="/resources/assets/plugins/dist/adminlte.min.js" />"></script>
<script src="<c:url value="/resources/assets/plugins/dist/demo.js" />"></script>
<script src="<c:url value="/resources/assets/customjs/swal.js?v${version}" />"></script>
<script src="<c:url value="/resources/assets/customjs/sessionInvalidCheck.js?v${version}" />"></script>
</body>
</html>
