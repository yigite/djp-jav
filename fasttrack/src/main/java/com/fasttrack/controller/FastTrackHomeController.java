package com.fasttrack.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
/*
* this is a test controller for fasttrack which calls the controller in the core module
* the fasttrack url is the controller url in the core module
* it calls a function in that controller called /there;
* */

@RequestMapping("/fasttrack")
@RestController
public class FastTrackHomeController {

	/* below function is for test purpose only
	/ if you want it to work just uncomment it
	 and add the specified end point in core module of fasttrack*/
	@GetMapping("/hello")
	public Map<String, String> hello()
	{
		return Collections.singletonMap("text", "Fasttrack");
	}
}
